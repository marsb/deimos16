//////////////////////////////////////////////////////////////////////
//
// File:      TestApp.cc
//
// Purpose:
//    MiniIPS test application
//  
//////////////////////////////////////////////////////////////////////

#define ETHERNET    // Select Ethernet interface
//#undef ETHERNET


#include <stdio.h>
#include <stdlib.h>
#include "ZestET1.h"
#include "Network2.hh"
#include <iostream>
#include <string.h>     /* strcat */
#include <algorithm>
#include <vector>

#ifdef WIN32
#include <Windows.h>
#define usleep(x) Sleep((x)/1000)
#else
#include <unistd.h>
#endif

// Default GPIO direction (0=write, 1=read)
// 0b11001100110111111101110000110000
#define DEFAULT_GPIO_DIRECTION 0xccdfdc30

// Default GPIO value (0=low, 1=high)
// 0b11001111111111111111110111110000
#define DEFAULT_GPIO 0xcffffdf4

// TRIGGER GPIO value (0=low, 1=high)
// 0b1111
#define TRIGGER_GPIO (DEFAULT_GPIO | 0xf)

// ADC SPI GPIO
// 0b1111111111111111111111011111111
#define ADC_SPI_GPIO (DEFAULT_GPIO & 0xfffffeff)

// DAC SPI GPIO (SYNC)
// 0b11111111111111111101111111111111
#define DAC_SPI_GPIO (DEFAULT_GPIO & 0xffffdfff)

// DAC LOAD GPIO (LDAC)
// 0b11111111111111111111111101111111
#define DAC_LOAD_GPIO (DEFAULT_GPIO & 0xffffff7f)

// TEM1 SPI GPIO
// 0b11111111110111111111111111111111
#define TEM1_SPI_GPIO (DEFAULT_GPIO & 0xffdfffff)

// TEM2 SPI GPIO
// 0b11111111110111111111111111111111
#define TEM2_SPI_GPIO (DEFAULT_GPIO & 0xfeffffff)

const int N_DAC_PULSE_CHANNELS=4;
const int DAC_PULSE_CHANNELS[]={32,33,34,35};
const int N_DAC_TRIM_CHANNELS=16;
const int DAC_TRIM_CHANNELS[]={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};
const int N_DAC_HV_CHANNELS=1;
const int DAC_HV_CHANNELS[]={38};

//
// Error handler function
//
void ErrorHandler(const char *Function, 
                  ZESTET1_CARD_INFO *CardInfo,
                  ZESTET1_STATUS Status,
                  const char *Msg)
{
    printf("**** TestApp - Function %s returned an error\n        \"%s\"\n\n", Function, Msg);
    exit(1);
}

std::string binary(unsigned x)
{
    std::string s;
    do
    {
        s.push_back('0' + (x & 1));
    } while (x >>= 1);
    std::reverse(s.begin(), s.end());
    return s;

}

//
// Dump trigger results from FPGA
//
void FPGA_shootCalibrationPulse(ZESTET1_CONNECTION CtrlConn)
{
  //printf("Shoot calibration pulse \n");
  NetSetGPIO(CtrlConn, TRIGGER_GPIO);       // Triggers on
  usleep(100000);
  NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Triggers off
  usleep(100000);
}

//
// Dump trigger results from FPGA
//
int Bin0[16384];
int Areas[16];
ZESTET1_STATUS DumpData(ZESTET1_CONNECTION &DataConn, std::vector<int> &wf, int verbosity)
{
  ZESTET1_STATUS Status;
  unsigned char Buffer[1024];
  
  // ZestET1RegisterErrorHandler(ErrorHandler);
  Status=NetReadTriggerData(DataConn, Buffer, sizeof(Buffer));
  
  unsigned char Overflow;
  unsigned char Channel=Buffer[0];
  //NetReadTriggerReg(CtrlConn, Channel, TRIGGER_OVERFLOW, &Overflow);
  //if (Overflow!=0) {
  //  printf("Channel %d overflow", Channel);
  //  //Status=NetWriteTriggerReg(CtrlConn, Channel, TRIGGER_ENABLE, 1);
  //}
  if (Status==ZESTET1_SUCCESS) {
	unsigned char Channel;
	unsigned long long TimeStamp;
	int PreSamples;
	int PostSamples;
	int i;
	//unsigned char Overflow;
	
	// Received some data
	Channel=Buffer[0];
	TimeStamp = (((unsigned long long)(Buffer[1]))<<56ll) | (((unsigned long long)(Buffer[2]))<<48ll) |
	  (((unsigned long long)(Buffer[3]))<<40ll) | (((unsigned long long)(Buffer[4]))<<32ll) |
	  (((unsigned long long)(Buffer[5]))<<24ll) | (((unsigned long long)(Buffer[6]))<<16ll) |
	  (((unsigned long long)(Buffer[7]))<<8ll) | (((unsigned long long)(Buffer[8]))<<0ll);
	if (verbosity>1){
	  printf("Time: %.2f secs, Channel: %d\n", TimeStamp/100000000.0, Channel);
	}
	//Status=NetReadTriggerReg(CtrlConn, Channel, TRIGGER_OVERFLOW, &Overflow);
	//if (Overflow!=0)
	//{
	//    printf("Channel %d overflow\n", Channel);
	//    Status=NetWriteTriggerReg(CtrlConn, Channel, TRIGGER_ENABLE, 1);
	//}
	PreSamples = (Buffer[9]<<8) | Buffer[10];
	PostSamples = (Buffer[11]<<8) | Buffer[12];
	for (i=0; i<PreSamples; i++)
	  {
	    int Val = (Buffer[13+2*i]<<8) | Buffer[13+2*i+1];
	    if (verbosity>2){
	      printf("%d - 0x%lx - 0b%s\n", Val, Val, (binary(Val)).c_str());
	    }
	    wf.push_back(Val);
	  }
	if (verbosity>2){
	  printf("--Trigger Point--\n");
	}
	for (; i<PreSamples+PostSamples; i++)
	  {
	    int Val = (Buffer[13+2*i]<<8) | Buffer[13+2*i+1];
	    if (verbosity>2){
	      printf("%d - 0x%lx\ - 0b%s\n", Val, Val, (binary(Val)).c_str());
	    }
	    wf.push_back(Val);
	  }
	i = 13+2*(PreSamples+PostSamples-1);
	//printf("Neutron? : %s\n", Buffer[i] ? "Yes" : "No");
	if (verbosity>1){
	  printf("Neutron : %d\n", Buffer[i]);
	  printf("Time over Threshold : %d\n", (Buffer[i+1]<<8) | Buffer[i+2]);
	  printf("Peak : %d\n", (Buffer[i+3]<<8) | Buffer[i+4]);
	  printf("Area : %d\n", (Buffer[i+5]<<16) | (Buffer[i+6]<<8) | Buffer[i+7]);
	  Areas[Channel]+=(Buffer[i+5]<<16) | (Buffer[i+6]<<8) | Buffer[i+7];
	  Bin0[(Buffer[13]<<8) | Buffer[14]]++;
	}
	//printf("Channel : %d\n", Buffer[0]);
      }
  if (verbosity>0){
    printf("Status: %d\n", Status);
  }
  return Status;
}

void DAC_write(ZESTET1_CONNECTION &CtrlConn, unsigned char WriteData[]){
  int clockRate=2;
  int bitWidth=8;
  int length=3;
  unsigned char ReadData[]={0,0,0};
  
  NetSetGPIO(CtrlConn, DEFAULT_GPIO);           // Set GPIOs
  NetSetGPIODirection(CtrlConn, DEFAULT_GPIO_DIRECTION);  // Enable outputs
  
  NetSetGPIO(CtrlConn, DAC_SPI_GPIO);       // Set DAC_SYNCN low
  NetSPIReadWrite(CtrlConn, clockRate, bitWidth, WriteData, ReadData, length); 
  NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set DAC_SYNCN high
}

void DAC_load(ZESTET1_CONNECTION CtrlConn){
  NetSetGPIO(CtrlConn, DAC_LOAD_GPIO);       // Set DAC_LDACN low
  NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set DAC_LDACN high
}

void DAC_initialise(ZESTET1_CONNECTION CtrlConn){
  int trim=16383;
  int calPulse=16383;
  int hv=0; // Corresponds to 67.94 V
  unsigned char WriteData[3];
  //Soft power up
  WriteData[0]=0x09;
  WriteData[1]=0x0;
  WriteData[2]=0x0; 
  DAC_write(CtrlConn, WriteData);
  // Set internal reference to 1v25 (or 2v50)
  WriteData[0]=0x0c; // Control register write
  WriteData[1]=0x24;
  WriteData[2]=0x0; 
  DAC_write(CtrlConn, WriteData);

  
  int ch;
  int j;
  // Set cal channels
  for(j=0; j<N_DAC_PULSE_CHANNELS; j++){
    ch=DAC_PULSE_CHANNELS[j];
    // Set gain to 1 
    WriteData[0]=ch; // Write to A register of channel ch
    WriteData[1]=0x7f; // Set gain to 1
    WriteData[2]=0xff;
    DAC_write(CtrlConn, WriteData);
    // Set offset to 0
    WriteData[0]=ch; // Write to A register of channel ch
    WriteData[1]=0xa0; // Set offset to 0
    WriteData[2]=0x00;
    DAC_write(CtrlConn, WriteData);
    // Set cal pulse
    WriteData[0]=ch; // Write to A register of channel ch
    WriteData[1]=0xC0 | ((calPulse>>8)&0x3f);
    WriteData[2]=calPulse&0xff;
    DAC_write(CtrlConn, WriteData);
  }


  // Set trim channels
  for(j=0; j<N_DAC_TRIM_CHANNELS; j++){
    ch=DAC_TRIM_CHANNELS[j];
    // Set gain to 1 
    WriteData[0]=ch; // Write to A register of channel ch
    WriteData[1]=0x7f; // Set gain to 1
    WriteData[2]=0xff;
    DAC_write(CtrlConn, WriteData);
    // Set offset to 0
    WriteData[0]=ch; // Write to A register of channel ch
    WriteData[1]=0xa0; // Set offset to 0
    WriteData[2]=0x00;
    DAC_write(CtrlConn, WriteData);
    // Set cal pulse
    WriteData[0]=ch; // Write to A register of channel ch
    WriteData[1]=0xC0 | ((trim>>8)&0x3f);
    WriteData[2]=trim&0xff;
    DAC_write(CtrlConn, WriteData);
  }

  // Set hv channels
  for(j=0; j<N_DAC_HV_CHANNELS; j++){
    ch=DAC_HV_CHANNELS[j];
    // Set gain to 1 
    WriteData[0]=ch; // Write to A register of channel ch
    WriteData[1]=0x7f; // Set gain to 1
    WriteData[2]=0xff;
    DAC_write(CtrlConn, WriteData);
    // Set offset to 0
    WriteData[0]=ch; // Write to A register of channel ch
    WriteData[1]=0xa0; // Set offset to 0
    WriteData[2]=0x00;
    DAC_write(CtrlConn, WriteData);
    // Set cal pulse
    WriteData[0]=ch; // Write to A register of channel ch
    WriteData[1]=0xC0 | ((hv>>8)&0x3f);
    WriteData[2]=hv&0xff;
    DAC_write(CtrlConn, WriteData);
  }
    
  DAC_load(CtrlConn);
}


void ADC_initialise(ZESTET1_CONNECTION &CtrlConn){
  unsigned char nEnables[3];
  unsigned char ReadData[3];
  unsigned char WriteData[3];
  

  // Read model number
  //    NetSetGPIO(CtrlConn, 0xfffffcff);   // Set GPIO9, 8 low (powers up ADC and selects SPI port)
  NetSetGPIO(CtrlConn, ADC_SPI_GPIO);   // Set GPIO9, 8 low (powers up ADC and selects SPI port)
  nEnables[0] = 0x00;                 // Output
  nEnables[1] = 0x00;
  nEnables[2] = 0xff;                 // Input
  WriteData[0] = 0x80 | 0x00 | 0x00;  // RnW = 1 so read, W1:0=0 so read 1 byte, A12:8=0
  WriteData[1] = 0x01;                // A7:0 = 0x01 so read from register address 0x001
  WriteData[2] = 0x00;
  ReadData[0] = 0;
  ReadData[1] = 0;
  ReadData[2] = 0;
  NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
  NetSetGPIO(CtrlConn, DEFAULT_GPIO);   // Set GPIO8 high
  printf("ADC model = %02x (should be 0x92)\n", ReadData[2]);
  
  //Set output mode to offset binary mode
  NetSetGPIO(CtrlConn, ADC_SPI_GPIO);   // Set GPIO8 low
  nEnables[0] = 0x00;                 // Output
  nEnables[1] = 0x00;
  nEnables[2] = 0x00;
  WriteData[0] = 0x00 | 0x00 | 0x00;  // RnW = 0 so write, W1:0=0 so read 1 byte, A12:8=0
  WriteData[1] = 0x14;                // A7:0 = 0x14 (write to output mode register)
  WriteData[2] = 0x00;				 // LVDS-ANSI, non inverted, offset binary
  // WriteData[2] = 0x01;				 // LVDS-ANSI, non inverted, twos complement
  ReadData[0] = 0;
  ReadData[1] = 0;
  ReadData[2] = 0;
  NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
  NetSetGPIO(CtrlConn, DEFAULT_GPIO);   // Set GPIO8 high
}

void ADC_setTestPattern(ZESTET1_CONNECTION &CtrlConn, int value){
  // Use ADC test patterns

  unsigned char nEnables[3];
  unsigned char ReadData[3];
  unsigned char WriteData[3];

  // Set output to zero
  NetSetGPIO(CtrlConn, DEFAULT_GPIO);   // Set GPIO8 high
  NetSetGPIO(CtrlConn, ADC_SPI_GPIO);   // Set GPIO8 low
  nEnables[0] = 0x00;                 // Output
  nEnables[1] = 0x00;
  nEnables[2] = 0x00;
  WriteData[0] = 0x00 | 0x00 | 0x00;  // RnW = 0 so write, W1:0=0 so read 1 byte, A12:8=0
  WriteData[1] = 0x0d;                // A7:0 = 0x0d so write to register address 0x00d (test mode)
  WriteData[2]=0b00000011;
  ReadData[0] = 0;
  ReadData[1] = 0;
  ReadData[2] = 0;
  NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
  NetSetGPIO(CtrlConn, DEFAULT_GPIO);   // Set GPIO8 high


  // Write test values to ADC test pattern generator
  NetSetGPIO(CtrlConn, ADC_SPI_GPIO);   // Set GPIO8 low
  nEnables[0] = 0x00;                 // Output
  nEnables[1] = 0x00;
  nEnables[2] = 0x00;
  WriteData[0] = 0x00 | 0x00 | 0x00;  // RnW = 0 so write, W1:0=0 so write 1 byte, A12:8=0
  WriteData[1] = 0x19;                // A7:0 = 0x019 so write to register address 0x019 (user test value)
  WriteData[2] = (value & 0x3F)<<2; // Last 6 bits plus 00
  ReadData[0] = 0;
  ReadData[1] = 0;
  ReadData[2] = 0;
  NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
  NetSetGPIO(CtrlConn, DEFAULT_GPIO);   // Set GPIO8 high
  NetSetGPIO(CtrlConn, ADC_SPI_GPIO);   // Set GPIO8 low
  nEnables[0] = 0x00;                 // Output
  nEnables[1] = 0x00;
  nEnables[2] = 0x00;
  WriteData[0] = 0x00 | 0x00 | 0x00;  // RnW = 0 so write, W1:0=0 so write 1 byte, A12:8=0
  WriteData[1] = 0x1a;                // A7:0 = 0x01a so write to register address 0x01a (user test value)
  WriteData[2] =value>>6; // First 8 bits                
  ReadData[0] = 0;
  ReadData[1] = 0;
  ReadData[2] = 0;
  NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
  NetSetGPIO(CtrlConn, DEFAULT_GPIO);   // Set GPIO8 high
  NetSetGPIO(CtrlConn, ADC_SPI_GPIO);   // Set GPIO8 low
  nEnables[0] = 0x00;                 // Output
  nEnables[1] = 0x00;
  nEnables[2] = 0x00;
  WriteData[0] = 0x00 | 0x00 | 0x00;  // RnW = 0 so write, W1:0=0 so write 1 byte, A12:8=0
  WriteData[1] = 0x1b;                // A7:0 = 0x01b so write to register address 0x01b (user test value)
  WriteData[2] = 0x00;
  ReadData[0] = 0;
  ReadData[1] = 0;
  ReadData[2] = 0;
  NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
  NetSetGPIO(CtrlConn, DEFAULT_GPIO);   // Set GPIO8 high
  NetSetGPIO(CtrlConn, ADC_SPI_GPIO);   // Set GPIO8 low
  nEnables[0] = 0x00;                 // Output
  nEnables[1] = 0x00;
  nEnables[2] = 0x00;
  WriteData[0] = 0x00 | 0x00 | 0x00;  // RnW = 0 so write, W1:0=0 so write 1 byte, A12:8=0
  WriteData[1] = 0x1c;                // A7:0 = 0x01c so write to register address 0x01c (user test value)
  WriteData[2] = 0x00;
  ReadData[0] = 0;
  ReadData[1] = 0;
  ReadData[2] = 0;
  NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
  NetSetGPIO(CtrlConn, DEFAULT_GPIO);   // Set GPIO8 high

  // Write test generator enable register
  NetSetGPIO(CtrlConn, ADC_SPI_GPIO);   // Set GPIO8 low
  nEnables[0] = 0x00;                 // Output
  nEnables[1] = 0x00;
  nEnables[2] = 0x00;
  WriteData[0] = 0x00 | 0x00 | 0x00;  // RnW = 0 so write, W1:0=0 so read 1 byte, A12:8=0
  WriteData[1] = 0x0d;                // A7:0 = 0x0d so write to register address 0x00d (test mode)
  //WriteData[2] = 0x04;               // Checkerboard mode
  //WriteData[2] = 0x06;               // Pseudo-random short mode
  //WriteData[2] = 0x48;                 // Alternating user value mode
  // WriteData[2] = 0x00;				 // Disable test mode	
  //WriteData[2] = 0x0b;                // One bit high
  WriteData[2]=0b00001000;
  //WriteData[2] = 0x2;                // Full scale
  ReadData[0] = 0;
  ReadData[1] = 0;
  ReadData[2] = 0;
  NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
  NetSetGPIO(CtrlConn, DEFAULT_GPIO);   // Set GPIO8 high
}

void FPGA_initialise(ZESTET1_CARD_INFO &CardInfo, ZESTET1_CONNECTION &CtrlConn,
		     ZESTET1_CONNECTION &DataConn, bool flash=false){
  unsigned char Val;
  unsigned long Time;
  int status;
  //unsigned char nEnables[32];
  //unsigned char ReadData[32];
  //unsigned char WriteData[32];
  
  //
  // Install an error handler and initialise library
  //
  ZestET1RegisterErrorHandler(ErrorHandler);
  ZestET1Init();

  // Connect
  printf("Connecting directly (no search)\n");
  //CardInfo[0] = &CardInfoTmp;
  printf("IPAddr %u %u %u %u\n",CardInfo.IPAddr[0],CardInfo.IPAddr[1],
	 CardInfo.IPAddr[2],CardInfo.IPAddr[3]);
  printf("ControlPort: %u \n",CardInfo.ControlPort);
  printf("Get card info\n");
  ZestET1GetCardInfo(&CardInfo);

  // Configure the FPGA directly from a file
  // Wait for user FPGA application to start listening on its connection
  // Open control and data connections to user FPGA
  //
  printf("Configuring FPGA from file \n");
  ZestET1ConfigureFromFile(&CardInfo, "miniips.bit");
  //
  // Write firmware to flash
  if (flash){
    printf("Writing firmware to flash \n");
    ZestET1ProgramFlashFromFile(&CardInfo, "miniips.bit");
    usleep(1000000);
  }
  
  usleep(1000000);
  printf("Open control connection\n");
  status=ZestET1OpenConnection(&CardInfo, ZESTET1_TYPE_TCP, 0x6000, 0, &CtrlConn);
  std::cout << "Connection status: " << status << std::endl; 

  printf("Open data connection\n");
  ZestET1OpenConnection(&CardInfo, ZESTET1_TYPE_TCP, 0x6001, 0, &DataConn);
  
  // 
  // Access registers
  //
  printf("Reading ZestET1 firmware version\n");
  NetReadControlReg(CtrlConn, CTRL_FIRMWARE_VERSION, &Val);
  printf("Firmware version : %x.%x\n", Val>>4, Val&0xf);
  
  // Get Time
  NetWriteControlReg(CtrlConn, CTRL_TIME_MSB, 0); // Latches current time
  NetReadControlReg(CtrlConn, CTRL_TIME_MSB, &Val); // Latches current time
  Time = Val<<24;
  NetReadControlReg(CtrlConn, CTRL_TIME_2, &Val); // Latches current time
  Time |= Val<<16;
  NetReadControlReg(CtrlConn, CTRL_TIME_1, &Val); // Latches current time
  Time |= Val<<8;
  NetReadControlReg(CtrlConn, CTRL_TIME_LSB, &Val); // Latches current time
  Time |= Val;
  printf("Current Time = %lu\n", Time);
  
  // Default GPIOs (Directions are kept fixed)
  NetSetGPIO(CtrlConn, DEFAULT_GPIO);           // Set GPIOs
  NetSetGPIODirection(CtrlConn, DEFAULT_GPIO_DIRECTION);  // Enable outputs
  }
  
  
  void FPGA_defaultTrigger(ZESTET1_CONNECTION &CtrlConn, 
			 int Threshold = 9000, int Peak = 14000, 
			   int ToTDetect = 6, int ToTTrig = 3, int PreSamples=10,
			   int PostSamples=20){
  //
  // Test triggering
  //
  // 8192 is the mid level of the adc input
  // noise starts to trigger at 8615
  int Area = 3000;   // NB: Unused in v2.0 firmware
  int j;

  // Reset timer
  // This will synchronise the timestamps between the master and slave boards
  NetWriteControlReg(CtrlConn, CTRL_TIME_RESET, 1); 
  NetWriteControlReg(CtrlConn, CTRL_TIME_RESET, 0); 
  
  for (j=0; j<16; j++)  // Set all channels
    {
      NetWriteTriggerReg(CtrlConn, j, TRIGGER_TRIG_LEVEL_HIGH, (Threshold>>8)); 
      NetWriteTriggerReg(CtrlConn, j, TRIGGER_TRIG_LEVEL_LOW, (Threshold>>0));
      NetWriteTriggerReg(CtrlConn, j, TRIGGER_PRE_CALC_SAMPLES_HIGH, (PreSamples>>8));
      NetWriteTriggerReg(CtrlConn, j, TRIGGER_PRE_CALC_SAMPLES_LOW, (PreSamples>>0));
      NetWriteTriggerReg(CtrlConn, j, TRIGGER_POST_CALC_SAMPLES_HIGH, (PostSamples>>8));
      NetWriteTriggerReg(CtrlConn, j, TRIGGER_POST_CALC_SAMPLES_LOW, (PostSamples>>0));
      NetWriteTriggerReg(CtrlConn, j, TRIGGER_PRE_NET_SAMPLES_HIGH, (PreSamples>>8));
      NetWriteTriggerReg(CtrlConn, j, TRIGGER_PRE_NET_SAMPLES_LOW, (PreSamples>>0));
      NetWriteTriggerReg(CtrlConn, j, TRIGGER_POST_NET_SAMPLES_HIGH, (PostSamples>>8));
      NetWriteTriggerReg(CtrlConn, j, TRIGGER_POST_NET_SAMPLES_LOW, (PostSamples>>0));
      NetWriteTriggerReg(CtrlConn, j, TRIGGER_PEAK_THRESHOLD_HIGH, (Peak>>8));
      NetWriteTriggerReg(CtrlConn, j, TRIGGER_PEAK_THRESHOLD_LOW, (Peak>>0));
      NetWriteTriggerReg(CtrlConn, j, TRIGGER_AREA_THRESHOLD_HIGH, (Area>>16));   // NB: Unused in v2.0 firmware
      NetWriteTriggerReg(CtrlConn, j, TRIGGER_AREA_THRESHOLD_MED, (Area>>8));
      NetWriteTriggerReg(CtrlConn, j, TRIGGER_AREA_THRESHOLD_LOW, (Area>>0));
      NetWriteTriggerReg(CtrlConn, j, TRIGGER_TOT_DETECT_THRESHOLD_HIGH, (ToTDetect>>8)); // Number of samples over the threshold in the calc window required to set neutron detection flag
      NetWriteTriggerReg(CtrlConn, j, TRIGGER_TOT_DETECT_THRESHOLD_LOW, (ToTDetect>>0));
      NetWriteTriggerReg(CtrlConn, j, TRIGGER_TOT_TRIG_THRESHOLD_HIGH, (ToTTrig>>8)); // Number of samples over the threshold in the calc pre-window required to trigger calculation
      NetWriteTriggerReg(CtrlConn, j, TRIGGER_TOT_TRIG_THRESHOLD_LOW, (ToTTrig>>0));
      NetWriteTriggerReg(CtrlConn, j, TRIGGER_ENABLE, 1);
    }
}
  
  
//
// Main program
//
int main(int argc, char **argv)
{

  // Check the number of parameters
  if (argc < 2) {
    // Tell the user how to run the program
    std::cout << "Usage: " << argv[0] << " test/pattern/read/trigger [rate Hz]" << std::endl;
    return 1;
  }
  std::cout << argv[0] << "is run in mode " << argv[1] << std::endl;

  std::string runMode=argv[1];
  double triggerRate=10; // in Hz
  if (argc == 3){
    triggerRate=atof(argv[2]);
  }
  if (runMode=="trigger"){
    std::cout << "Trigger rate: " << triggerRate << " Hz" << std::endl;
  }

    ZESTET1_CARD_INFO CardInfo;
    ZESTET1_CONNECTION CtrlConn;
    ZESTET1_CONNECTION DataConn;
    ZESTET1_STATUS status;
    //unsigned char nEnables[32];
    //unsigned char ReadData[32];
    //unsigned char WriteData[32];

    CardInfo.IPAddr[0]=192;
    CardInfo.IPAddr[1]=168;
    CardInfo.IPAddr[2]=1;
    CardInfo.IPAddr[3]=100;
    CardInfo.ControlPort=20481;
    CardInfo.Timeout=10000; //100000;

    std::vector<int> wf;

    if (std::string(argv[1]) == "pattern") {
      FPGA_initialise(CardInfo, CtrlConn, DataConn);
      DAC_initialise(CtrlConn);
      ADC_initialise(CtrlConn);
      //ADC_setTestPattern(CtrlConn,0b0);
      FPGA_defaultTrigger(CtrlConn, 2, 14000, 6, 1, 10, 200);
      printf("Trigger on ADC test patterns \n");
      ZestET1RegisterErrorHandler(NULL);
      //ZestET1RegisterErrorHandler(ErrorHandler);

      for( int value = 5; value < 0b11111111111111; value+=1 ) {
	status=ZESTET1_SUCCESS;

	// Empty buffer
	status=ZESTET1_SUCCESS;
	while(status!=32777){
	  status=DumpData(DataConn, wf, 0);
	  wf.clear();
	}

	
	if (value % 100 == 0 || value==5){
	  std::cout<< std::endl;
	  std::cout<<"Testing value " << value << std::endl;
	}
	ADC_setTestPattern(CtrlConn, value);
	//usleep(1e6);

	status=ZESTET1_SUCCESS;
	while(status!=32777){
	  status=DumpData(DataConn, wf, 0);
	  if(status==ZESTET1_SUCCESS){
	    std::cout << "." <<  std::flush;
	    for( int i=20; i<wf.size(); i++){
	      if(wf[i]!=value){
		std::cout<<"***************** Problem in ADC value "<< value << " *******************" << std::endl;
		break;
	      }
	    }
	  }
	  wf.clear();
	}
      }
    }

    if (std::string(argv[1]) == "test") {
      FPGA_initialise(CardInfo, CtrlConn, DataConn);
      DAC_initialise(CtrlConn);
      ADC_initialise(CtrlConn);
      FPGA_defaultTrigger(CtrlConn);
      
      printf("Testing calibration triggers \n");
      ZestET1RegisterErrorHandler(NULL);
      //ZestET1RegisterErrorHandler(ErrorHandler);
      while(1){
	status=DumpData(DataConn, wf, 3);
	if (status==32777){
	  FPGA_shootCalibrationPulse(CtrlConn);
	}
      }
    }

    if (std::string(argv[1]) == "trigger") {
      printf("IPAddr %u %u %u %u\n",CardInfo.IPAddr[0],CardInfo.IPAddr[1],
	     CardInfo.IPAddr[2],CardInfo.IPAddr[3]);
      printf("ControlPort: %u \n",CardInfo.ControlPort);
      printf("Get card info\n");
      status=ZestET1GetCardInfo(&CardInfo);  
      std::cout << "Card info status: " << status << std::endl; 
      usleep(1000000);
      printf("Open control connection\n");
      status=ZestET1OpenConnection(&CardInfo, ZESTET1_TYPE_TCP, 0x6000, 0, &CtrlConn);
      std::cout << "Connection status: " << status << std::endl; 
      
      FPGA_defaultTrigger(CtrlConn);      
      printf("Shooting calibration triggers \n");
      ZestET1RegisterErrorHandler(NULL);
      //ZestET1RegisterErrorHandler(ErrorHandler);
      while(1){
	FPGA_shootCalibrationPulse(CtrlConn);
	usleep(1/triggerRate*1000000.0);
	std::cout << "t" << std::flush;
      }
    }
    
    if (std::string(argv[1]) == "read") {
      FPGA_initialise(CardInfo, CtrlConn, DataConn);
      DAC_initialise(CtrlConn);
      ADC_initialise(CtrlConn);
            
      ZestET1RegisterErrorHandler(NULL);
      ZestET1CloseConnection(CtrlConn);
      //ZestET1RegisterErrorHandler(ErrorHandler);
      while(1){
	status=DumpData(DataConn, wf, 0);
	if (status==32777)
	  std::cout << "-" << std::flush;
	else if (status==0)
	  std::cout << "+" << std::flush;
	else
	  std::cout << "e" << std::flush;
      }
    }
    
    
    //
    // Free the card information structure
    //
    ZestET1CloseConnection(CtrlConn);
    ZestET1CloseConnection(DataConn);
    
    // This does not work
    //ZestET1FreeCards(&CardInfo);
    
    //
    // Close library
    //
    ZestET1Close();
    
    return 0;
}

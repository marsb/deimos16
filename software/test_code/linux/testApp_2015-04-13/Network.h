#ifndef NETWORK_H
#define NETWORK_H

#ifdef __cplusplus
extern "C"
{
#endif

// Network packet constants
#define NET_ESC 0x10
#define NET_SOF 0x01
#define NET_EOF 0x02
#define NET_TRIGGER_READ_CMD 0x00
#define NET_TRIGGER_WRITE_CMD 0x01
#define NET_CONTROL_READ_CMD 0x02
#define NET_CONTROL_WRITE_CMD 0x03

// Trigger unit addresses
#define TRIGGER_TRIG_LEVEL_HIGH         0x000
#define TRIGGER_TRIG_LEVEL_LOW          0x001
#define TRIGGER_PRE_CALC_SAMPLES_HIGH   0x002
#define TRIGGER_PRE_CALC_SAMPLES_LOW    0x003
#define TRIGGER_POST_CALC_SAMPLES_HIGH  0x004
#define TRIGGER_POST_CALC_SAMPLES_LOW   0x005
#define TRIGGER_PRE_NET_SAMPLES_HIGH    0x006
#define TRIGGER_PRE_NET_SAMPLES_LOW     0x007
#define TRIGGER_POST_NET_SAMPLES_HIGH   0x008
#define TRIGGER_POST_NET_SAMPLES_LOW    0x009
#define TRIGGER_PEAK_THRESHOLD_HIGH     0x00a
#define TRIGGER_PEAK_THRESHOLD_LOW      0x00b
#define TRIGGER_AREA_THRESHOLD_HIGH     0x00c
#define TRIGGER_AREA_THRESHOLD_MED      0x00d
#define TRIGGER_AREA_THRESHOLD_LOW      0x00e
#define TRIGGER_ENABLE                  0x00f

// Control unit addresses
#define CTRL_GPIO_MSB                   0x0000
#define CTRL_GPIO_2                     0x0001
#define CTRL_GPIO_1                     0x0002
#define CTRL_GPIO_LSB                   0x0003
#define CTRL_GPIO_nOUTPUT_MSB           0x0004
#define CTRL_GPIO_nOUTPUT_2             0x0005
#define CTRL_GPIO_nOUTPUT_1             0x0006
#define CTRL_GPIO_nOUTPUT_LSB           0x0007

#define CTRL_SPI_DATA                   0x0010
#define CTRL_SPI_ENABLE                 0x0011
#define CTRL_SPI_MUX_LEN                0x0012
#define CTRL_SPI_CLOCK_RATE             0x0013

#define CTRL_TIME_MSB                   0x0020
#define CTRL_TIME_2                     0x0021
#define CTRL_TIME_1                     0x0022
#define CTRL_TIME_LSB                   0x0023

#define CTRL_FIRMWARE_VERSION           0xffff


// API
ZESTET1_STATUS NetWriteTriggerReg(ZESTET1_CONNECTION Handle, int Index, unsigned short Addr, unsigned char Data);
ZESTET1_STATUS NetReadTriggerReg(ZESTET1_CONNECTION Handle, int Index, unsigned short Addr, unsigned char *Data);
ZESTET1_STATUS NetWriteControlReg(ZESTET1_CONNECTION Handle, unsigned short Addr, unsigned char Data);
ZESTET1_STATUS NetReadControlReg(ZESTET1_CONNECTION Handle, unsigned short Addr, unsigned char *Data);
ZESTET1_STATUS NetSetGPIODirection(ZESTET1_CONNECTION Handle, unsigned long nEnables);
ZESTET1_STATUS NetSetGPIO(ZESTET1_CONNECTION Handle, unsigned long DataOut);
ZESTET1_STATUS NetGetGPIO(ZESTET1_CONNECTION Handle, unsigned long *DataIn);
ZESTET1_STATUS NetSPIReadWrite(ZESTET1_CONNECTION Handle, int ClockRate, int BitWidth,
                               unsigned char *WriteData, unsigned char *ReadData, int Length); 
ZESTET1_STATUS NetSDIOReadWrite(ZESTET1_CONNECTION Handle, int ClockRate, int BitWidth,
                                unsigned char *nEnables, unsigned char *WriteData, unsigned char *ReadData, int Length); 
ZESTET1_STATUS NetReadTriggerData(ZESTET1_CONNECTION DataConn, unsigned char *Buffer, int Length);

#ifdef __cplusplus
}
#endif

#endif // NETWORK_H

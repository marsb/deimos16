//////////////////////////////////////////////////////////////////////
//
// File:      TestApp.c
//
// Purpose:
//    MiniIPS test application
//  
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "ZestET1.h"
#include "Network.h"


// Default GPIO direction (0=write, 1=read)
// 0b11001100110111111101110000110000
#define DEFAULT_GPIO_DIRECTION 0xccdfdc30

// Default GPIO value (0=low, 1=high)
// 0b11001111111111111111110111110000
#define DEFAULT_GPIO 0xcffffdf0

// TRIGGER GPIO value (0=low, 1=high)
// 0b1111
#define TRIGGER_GPIO (DEFAULT_GPIO | 0xf)

// ADC SPI GPIO
// 0b1111111111111111111111011111111
#define ADC_SPI_GPIO (DEFAULT_GPIO & 0xfffffeff)

// DAC SPI GPIO (SYNC)
// 0b11111111111111111101111111111111
#define DAC_SPI_GPIO (DEFAULT_GPIO & 0xffffdfff)

// DAC LOAD GPIO (LDAC)
// 0b11111111111111111111111101111111
#define DAC_LOAD_GPIO (DEFAULT_GPIO & 0xffffff7f)

// TEM1 SPI GPIO
// 0b11111111110111111111111111111111
#define TEM1_SPI_GPIO (DEFAULT_GPIO & 0xffdfffff)

// TEM2 SPI GPIO
// 0b11111111110111111111111111111111
#define TEM2_SPI_GPIO (DEFAULT_GPIO & 0xfeffffff)

//
// Error handler function
//
void ErrorHandler(const char *Function, 
                  ZESTET1_CARD_INFO *CardInfo,
                  ZESTET1_STATUS Status,
                  const char *Msg)
{
    printf("**** TestApp - Function %s returned an error\n        \"%s\"\n\n", Function, Msg);
    exit(1);
}

//
// Dump trigger results from FPGA
//
void CalibrationPulse(ZESTET1_CONNECTION CtrlConn)
{
  int value=0x3fff;
  int j;
  unsigned char ReadData[32];
  unsigned char WriteData[32];
  
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);           // Set GPIOs
    NetSetGPIODirection(CtrlConn, DEFAULT_GPIO_DIRECTION);  // Enable outputs

    NetSetGPIO(CtrlConn, DAC_SPI_GPIO);       // Set DAC_SYNCN low
    WriteData[0] = 0x09;                    // Soft power up
    WriteData[1] = 0x0;
    WriteData[2] = 0x0;
    ReadData[0] = 0;
    ReadData[1] = 0;
    ReadData[2] = 0;
    NetSPIReadWrite(CtrlConn, 2, 8, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set DAC_SYNCN high

    NetSetGPIO(CtrlConn, DAC_SPI_GPIO);       // Set DAC_SYNCN low
    WriteData[0] = 0x0c;                    // Write to control register
    WriteData[1] = 0x24;                    // Use internal vref 1v25
    WriteData[2] = 0x00;
    ReadData[0] = 0;
    ReadData[1] = 0;
    ReadData[2] = 0;
    NetSPIReadWrite(CtrlConn, 2, 8, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set DAC_SYNCN high
    



    for (j=32; j<36; j++)  // Set all channels
	{
	  // Set gain to 1
	  NetSetGPIO(CtrlConn, DAC_SPI_GPIO);       // Set DAC_SYNCN low
	  WriteData[0] = j;                    // Write to A register of channel j
	  WriteData[1] = 0x7f;                    // Set gain to 1
	  WriteData[2] = 0xff;
	  ReadData[0] = 0;
	  ReadData[1] = 0;
	  ReadData[2] = 0;
	  NetSPIReadWrite(CtrlConn, 2, 8, WriteData, ReadData, 3); 
	  NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set DAC_SYNCN high
	  
	  // Offset to 0
	  NetSetGPIO(CtrlConn, DAC_SPI_GPIO);       // Set DAC_SYNCN low
	  WriteData[0] = j;                    // Write to A register of channel j
	  WriteData[1] = 0xa0;                  
	  WriteData[2] = 0x00;
	  ReadData[0] = 0;
	  ReadData[1] = 0;
	  ReadData[2] = 0;
	  NetSPIReadWrite(CtrlConn, 2, 8, WriteData, ReadData, 3); 
	  NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set DAC_SYNCN high
	  
	  NetSetGPIO(CtrlConn, DAC_SPI_GPIO);       // Set DAC_SYNCN low
	  WriteData[0] = j;                       // Write to A register of channel
	  WriteData[1] = 0xC0 | ((value>>8)&0x3f);    // Write to input data register (REG1:0 = 11)
	  WriteData[2] = value&0xff;
	  ReadData[0] = 0;
	  ReadData[1] = 0;
	  ReadData[2] = 0;
	  NetSPIReadWrite(CtrlConn, 2, 8, WriteData, ReadData, 3); 
	  NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set DAC_SYNCN high
	  // printf("Channel = %i, Voltage %i \n", j,value);
	  usleep(100000);
	  //Sleep(1); 
	}
    // Toggle LDAC low then high
    NetSetGPIO(CtrlConn, DAC_LOAD_GPIO);       // Set DAC_LDACN low
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set DAC_LDACN high

    //Sleep(1000);
    usleep(100000);
    
    // Test calibration trigger
    printf("Shoot calibration trigger \n");
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);           // Set GPIOs
    NetSetGPIODirection(CtrlConn, DEFAULT_GPIO_DIRECTION);  // Enable trigger inputs
    NetSetGPIO(CtrlConn, TRIGGER_GPIO);       // Triggers on
    //Sleep(1000); 
    usleep(100000);
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Triggers off
    //Sleep(1000); 
    usleep(100000);	
}

//
// Dump trigger results from FPGA
//
void DumpData(ZESTET1_CONNECTION DataConn, ZESTET1_CONNECTION CtrlConn)
{
    ZESTET1_STATUS Status;
    unsigned char Buffer[1024];

    ZestET1RegisterErrorHandler(NULL);
	// ZestET1RegisterErrorHandler(ErrorHandler);

    while(1)
    {
        Status=NetReadTriggerData(DataConn, Buffer, sizeof(Buffer));
        if (Status==ZESTET1_SUCCESS)
        {
	  printf("*");
	  return;
	  unsigned char Channel;
	  unsigned long long TimeStamp;
            int PreSamples;
            int PostSamples;
            int i;

            // Received some data
	    Channel=Buffer[0];
            TimeStamp = (((unsigned long long)(Buffer[1]))<<56ll) | (((unsigned long long)(Buffer[2]))<<48ll) |
                        (((unsigned long long)(Buffer[3]))<<40ll) | (((unsigned long long)(Buffer[4]))<<32ll) |
                        (((unsigned long long)(Buffer[5]))<<24ll) | (((unsigned long long)(Buffer[6]))<<16ll) |
                        (((unsigned long long)(Buffer[7]))<<8ll) | (((unsigned long long)(Buffer[8]))<<0ll);
            printf("Time: %.2f secs\n", TimeStamp/100000000.0);
            PreSamples = (Buffer[9]<<8) | Buffer[10];
            PostSamples = (Buffer[11]<<8) | Buffer[12];
            for (i=0; i<PreSamples; i++)
            {
                int Val = (Buffer[13+2*i]<<8) | Buffer[13+2*i+1];
                printf("%d\n", Val);

            }
            printf("--Trigger Point--\n");
            for (; i<PreSamples+PostSamples; i++)
            {
                printf("%d\n", (Buffer[13+2*i]<<8) | Buffer[13+2*i+1]);
            }
            i = 13+2*i;
            //printf("Neutron? : %s\n", Buffer[i] ? "Yes" : "No");
	    printf("Neutron : %d\n", Buffer[i]);
            printf("Peak : %d\n", (Buffer[i+1]<<8) | Buffer[i+2]);
            printf("Area : %d\n", (Buffer[i+3]<<16) | (Buffer[i+4]<<8) | Buffer[i+5]);

            ZestET1RegisterErrorHandler(ErrorHandler);
	  printf("Channel : %d\n", Buffer[0]);
	  return;
        }
		//else{
	//printf(" Status: %04x\n", Status);
	printf("Status: %d\n", Status);
	if(Status==32777)
	  {
	    CalibrationPulse(CtrlConn);
	  }
	
    }
}

//
// Dump trigger results from FPGA
//
void DumpDataRaw(ZESTET1_CONNECTION DataConn, ZESTET1_CONNECTION CtrlConn)
{
    ZESTET1_STATUS Status;

    ZestET1RegisterErrorHandler(NULL);
	// ZestET1RegisterErrorHandler(ErrorHandler);

    while(1){
      unsigned long lengthRead=0;
      unsigned char Buffer[8*1024]={0};
      Status=NetReadTriggerDataRaw(DataConn, Buffer, sizeof(Buffer), &lengthRead);
      printf("Status: %d\n", Status);
      //if (Status==ZESTET1_SUCCESS){
      // Received some data
      int i;
      printf("\n********** Length read: %d ************\n", lengthRead);
      for (i=0; i<lengthRead+5; i++){
	if (i==lengthRead){
	  printf("|");
	}
	printf("%d ", Buffer[i]); 
      }
      //printf(" Status: %04x\n", Status);
      if(Status==32777 && lengthRead==0){
	CalibrationPulse(CtrlConn);
      } 
    }
}

//
// Main program
//
int main(int argc, char **argv)
{
    unsigned long Count;
    unsigned long NumCards;
    int CardIndex=0;
    ZESTET1_CARD_INFO *CardInfo;
    ZESTET1_CONNECTION CtrlConn;
    ZESTET1_CONNECTION DataConn;
    unsigned char Val;
    unsigned long Time;
    int i,j;
    unsigned char nEnables[32];
    unsigned char ReadData[32];
    unsigned char WriteData[32];

    int TemperatureAdc;
    double TemperatureC;

    //
    // Install an error handler and initialise library
    //
    ZestET1RegisterErrorHandler(ErrorHandler);
    ZestET1Init();

    //
    // Request information about the system
    // Wait for 2 seconds (on each interface) for boards to respond to query
    //
 
    printf("Searching for ZestET1 cards...\n");
    ZestET1CountCards(&NumCards, &CardInfo, 2000);
    printf("%ld available cards in the system\n\n\n", NumCards);
    if (NumCards==0)
    {
        printf("No cards found\n");
        exit(1);
    }

    for (Count=0; Count<NumCards; Count++)
    {
        printf("%ld : SerialNum = %ld, IPAddress = %d.%d.%d.%d\n",
            Count, CardInfo[Count].SerialNumber,
            CardInfo[Count].IPAddr[0], CardInfo[Count].IPAddr[1],
            CardInfo[Count].IPAddr[2], CardInfo[Count].IPAddr[3]);
        //FIXME
        if (CardInfo[Count].IPAddr[3]==101) CardIndex = Count;
    }

    //
    // Configure the FPGA directly from a file
    // Wait for user FPGA application to start listening on its connection
    // Open control and data connections to user FPGA
    //
    // ZestET1ConfigureFromFile(&CardInfo[CardIndex], "miniips.bit");
    //
    // Write firmware to flash
#if 0
    printf("Writing firmware to flash \n");
    ZestET1ProgramFlashFromFile(&CardInfo[CardIndex], "miniips.bit");
    //Sleep(1000);
    usleep(1000000);
#endif


    printf("Open control connection\n");
    ZestET1OpenConnection(&CardInfo[CardIndex], ZESTET1_TYPE_TCP, 0x6000, 0, &CtrlConn);
    printf("Open data connection\n");
    ZestET1OpenConnection(&CardInfo[CardIndex], ZESTET1_TYPE_TCP, 0x6001, 0, &DataConn);


    // 
    // Access registers
    //
    printf("Reading ZestET1 firmware version\n");
    NetReadControlReg(CtrlConn, CTRL_FIRMWARE_VERSION, &Val);
    printf("Firmware version : %x.%x\n", Val>>4, Val&0xf);

    // Get Time
    for (i=0; i<10; i++)
    {
        NetWriteControlReg(CtrlConn, CTRL_TIME_MSB, 0); // Latches current time
        NetReadControlReg(CtrlConn, CTRL_TIME_MSB, &Val); // Latches current time
        Time = Val<<24;
        NetReadControlReg(CtrlConn, CTRL_TIME_2, &Val); // Latches current time
        Time |= Val<<16;
        NetReadControlReg(CtrlConn, CTRL_TIME_1, &Val); // Latches current time
        Time |= Val<<8;
        NetReadControlReg(CtrlConn, CTRL_TIME_LSB, &Val); // Latches current time
        Time |= Val;
        printf("Current Time = %lu\n", Time);
    }

	// Test calibration trigger
	printf("Test calibration trigger \n");
	NetSetGPIO(CtrlConn, DEFAULT_GPIO);           // Set GPIOs
    NetSetGPIODirection(CtrlConn, DEFAULT_GPIO_DIRECTION);  // Enable trigger inputs
    for (i=0; i<10; i++)
      {
	NetSetGPIO(CtrlConn, TRIGGER_GPIO);       // Triggers on
	//Sleep(100); 
	usleep(100000);
	NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Triggers off
	//Sleep(100);
	usleep(100000); 
      }
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Triggers off
    //NetSetGPIODirection(CtrlConn, DEFAULT_GPIO_DIRECTION);  // Enable default outputs
    printf("Calibration trigger test ended \n");
    //Sleep(1000); 
    usleep(1000000);

    // Test SPI
    // Generate (very slow) ramp on DAC output
    
#if 1
    // Set DAC_RESETN high, DAC_LDACN high, DAC_PD low, DAC_DCEN low, DAC_SYNC high, DAC_CLRN high
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);           // Set GPIOs
    NetSetGPIODirection(CtrlConn, DEFAULT_GPIO_DIRECTION);  // Enable outputs

    NetSetGPIO(CtrlConn, DAC_SPI_GPIO);       // Set DAC_SYNCN low
    WriteData[0] = 0x09;                    // Soft power up
    WriteData[1] = 0x0;
    WriteData[2] = 0x0;
    ReadData[0] = 0;
    ReadData[1] = 0;
    ReadData[2] = 0;
    NetSPIReadWrite(CtrlConn, 2, 8, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set DAC_SYNCN high

    NetSetGPIO(CtrlConn, DAC_SPI_GPIO);       // Set DAC_SYNCN low
    WriteData[0] = 0x0c;                    // Write to control register
    WriteData[1] = 0x24;                    // Use internal vref 1v25
    WriteData[2] = 0x00;
    ReadData[0] = 0;
    ReadData[1] = 0;
    ReadData[2] = 0;
    NetSPIReadWrite(CtrlConn, 2, 8, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set DAC_SYNCN high


    // Set gain and offset of all channels
    for (j=0; j<39; j++)  // Set all channels
      //j=35;                   // Cal pulse 4 level
      {
	// Set gain to 1
	NetSetGPIO(CtrlConn, DAC_SPI_GPIO);       // Set DAC_SYNCN low
	WriteData[0] = j;                    // Write to A register of channel j
	WriteData[1] = 0x7f;                    // Set gain to 1
	WriteData[2] = 0xff;
	ReadData[0] = 0;
	ReadData[1] = 0;
	ReadData[2] = 0;
	NetSPIReadWrite(CtrlConn, 2, 8, WriteData, ReadData, 3); 
	NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set DAC_SYNCN high
	
	// Offset to 0
	NetSetGPIO(CtrlConn, DAC_SPI_GPIO);       // Set DAC_SYNCN low
	WriteData[0] = j;                    // Write to A register of channel j
	WriteData[1] = 0xa0;                  
	WriteData[2] = 0x00;
	ReadData[0] = 0;
	ReadData[1] = 0;
	ReadData[2] = 0;
	NetSPIReadWrite(CtrlConn, 2, 8, WriteData, ReadData, 3); 
	NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set DAC_SYNCN high
      }
    
	
    // Toggle LDAC low then high
    //NetSetGPIO(CtrlConn, DAC_LOAD_GPIO);       // Set DAC_LDACN low
    //usleep(100000);
    //NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set DAC_LDACN high
    //usleep(100000);

    
    for (i=0; i<12621; i+=5000)
    {
      //i=12621;
        for (j=0; j<35; j++)  // Set all channels
	  //j=35;                   // Cal pulse 4 level
	  {
            NetSetGPIO(CtrlConn, DAC_SPI_GPIO);       // Set DAC_SYNCN low
            WriteData[0] = j;                       // Write to A register of channel
            WriteData[1] = 0xC0 | ((i>>8)&0x3f);    // Write to input data register (REG1:0 = 11)
            WriteData[2] = i&0xff;
            ReadData[0] = 0;
            ReadData[1] = 0;
            ReadData[2] = 0;
            NetSPIReadWrite(CtrlConn, 2, 8, WriteData, ReadData, 3); 
            NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set DAC_SYNCN high
	    printf("Channel = %i, Voltage %i \n", j,i);
	    // Sleep(1);

	    NetSetGPIO(CtrlConn, DAC_LOAD_GPIO);       // Set DAC_LDACN low
	    //usleep(100000);
	    NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set DAC_LDACN high

	    usleep(100000);
	  }
      }
    // Toggle LDAC low then high
    NetSetGPIO(CtrlConn, DAC_LOAD_GPIO);       // Set DAC_LDACN low
    //usleep(100000);
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set DAC_LDACN high
    //usleep(100000);
    
#endif

    // Test temperature sensor 1
    // NB: The DAC has to be properly powered and out of reset (see above)
    // otherwise it will hold the data out pin of the temp sensor low
#if 1
    // Set DAC_RESETN high, DAC_LDACN high, DAC_PD low, DAC_DCEN low, DAC_SYNC high, DAC_CLRN high
    // Set temp sensor CSN as output
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);           // Set GPIOs
    NetSetGPIODirection(CtrlConn, DEFAULT_GPIO_DIRECTION);  // Enable outputs

	
    NetSetGPIO(CtrlConn, TEM1_SPI_GPIO);       // Set CSN low
    WriteData[0] = 0xff;                    // Reset
    WriteData[1] = 0xff;
    WriteData[2] = 0xff;
    WriteData[3] = 0xff;
    NetSPIReadWrite(CtrlConn, 0xf, 8, WriteData, ReadData, 4); 
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set CSN high
    // Sleep(1000);
    usleep(1000000);
	

    NetSetGPIO(CtrlConn, TEM1_SPI_GPIO);       // Set CSN low
	// printf("TEM1 = 0x%08x \n", TEM1_SPI_GPIO);
    WriteData[0] = 0x58;                    // Read from ID register address 3
    WriteData[1] = 0xff;
    ReadData[0] = 0;
    ReadData[1] = 0;
    NetSPIReadWrite(CtrlConn, 0xf, 8, WriteData, ReadData, 2); 
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set CSN high
    printf("Temperature chip 1 ID = 0x%02x, 0x%02x (should be 0xc3)\n", ReadData[0], ReadData[1]);

    // Set operation mode
    NetSetGPIO(CtrlConn, TEM1_SPI_GPIO);       // Set CSN low
    WriteData[0] = 0x08;                    // Read from ID register address 3
    WriteData[1] = 0xc9;
    ReadData[0] = 0xff;
    ReadData[1] = 0xff;
    NetSPIReadWrite(CtrlConn, 0xf, 8, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set CSN high

    // Check operation mode
    NetSetGPIO(CtrlConn, TEM1_SPI_GPIO);       // Set CSN low
    WriteData[0] = 0x48;                    // Read from ID register address 3
    WriteData[1] = 0xff;
    ReadData[0] = 0xff;
    ReadData[1] = 0xff;
    NetSPIReadWrite(CtrlConn, 0xf, 8, WriteData, ReadData, 2); 
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set CSN high
    printf("Operation mode = 0x%02x  0x%02x (should be 0xc9) \n", ReadData[0],ReadData[1]);

    NetSetGPIO(CtrlConn, TEM1_SPI_GPIO);       // Set CSN low
    WriteData[0] = 0x50;                    // Read from ID register address 3
    WriteData[1] = 0xff;
    WriteData[2] = 0xff;
    ReadData[0] = 0xff;
    ReadData[1] = 0xff;
    ReadData[2] = 0xff;
    NetSPIReadWrite(CtrlConn, 0xf, 8, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set CSN high
    //printf("Temperature = 0x%02x 0x%02x \n", ReadData[1],ReadData[2]);
    TemperatureAdc = (ReadData[1]<<8) | ReadData[2];
    TemperatureC=(double)TemperatureAdc*0.0078; //16 bit readout
    printf("Temperature = %f degrees Celsius \n", TemperatureC);
#endif

    // Test temperature sensor 2
    // NB: The DAC has to be properly powered and out of reset (see above)
    // otherwise it will hold the data out pin of the temp sensor low
#if 1
    // Set DAC_RESETN high, DAC_LDACN high, DAC_PD low, DAC_DCEN low, DAC_SYNC high, DAC_CLRN high
    // Set temp sensor CSN as output
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);           // Set GPIOs
    NetSetGPIODirection(CtrlConn, DEFAULT_GPIO_DIRECTION);  // Enable outputs

	
    NetSetGPIO(CtrlConn, TEM2_SPI_GPIO);       // Set CSN low
    WriteData[0] = 0xff;                    // Reset
    WriteData[1] = 0xff;
    WriteData[2] = 0xff;
    WriteData[3] = 0xff;
    NetSPIReadWrite(CtrlConn, 0xf, 8, WriteData, ReadData, 4); 
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set CSN high
    // Sleep(1000);
    usleep(1000000);
	

    NetSetGPIO(CtrlConn, TEM2_SPI_GPIO);       // Set CSN low
	// printf("TEM1 = 0x%08x \n", TEM1_SPI_GPIO);
    WriteData[0] = 0x58;                    // Read from ID register address 3
    WriteData[1] = 0xff;
    ReadData[0] = 0;
    ReadData[1] = 0;
    NetSPIReadWrite(CtrlConn, 0xf, 8, WriteData, ReadData, 2); 
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set CSN high
    printf("Temperature chip 2 ID = 0x%02x, 0x%02x (should be 0xc3)\n", ReadData[0], ReadData[1]);

    // Set operation mode
    NetSetGPIO(CtrlConn, TEM2_SPI_GPIO);       // Set CSN low
    WriteData[0] = 0x08;                    // Read from ID register address 3
    WriteData[1] = 0xc9;
    ReadData[0] = 0xff;
    ReadData[1] = 0xff;
    NetSPIReadWrite(CtrlConn, 0xf, 8, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set CSN high

    // Check operation mode
    NetSetGPIO(CtrlConn, TEM2_SPI_GPIO);       // Set CSN low
    WriteData[0] = 0x48;                    // Read from ID register address 3
    WriteData[1] = 0xff;
    ReadData[0] = 0xff;
    ReadData[1] = 0xff;
    NetSPIReadWrite(CtrlConn, 0xf, 8, WriteData, ReadData, 2); 
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set CSN high
    printf("Operation mode = 0x%02x  0x%02x (should be 0xc9) \n", ReadData[0],ReadData[1]);

    NetSetGPIO(CtrlConn, TEM2_SPI_GPIO);       // Set CSN low
    WriteData[0] = 0x50;                    // Read from ID register address 3
    WriteData[1] = 0xff;
    WriteData[2] = 0xff;
    ReadData[0] = 0xff;
    ReadData[1] = 0xff;
    ReadData[2] = 0xff;
    NetSPIReadWrite(CtrlConn, 0xf, 8, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);       // Set CSN high
    //printf("Temperature = 0x%02x 0x%02x \n", ReadData[1],ReadData[2]);
    TemperatureAdc = (ReadData[1]<<8) | ReadData[2];
    TemperatureC=(double)TemperatureAdc*0.0078; //16 bit readout
    printf("Temperature = %f degrees Celsius \n", TemperatureC);
#endif


#if 1
    // Test ADC SDIO
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);   // Set GPIO9, 8 high (ADC_PDN, ADC_CSN)
    NetSetGPIODirection(CtrlConn, DEFAULT_GPIO_DIRECTION);      // Set GPIO9, 8 as output

    // Read model number
//    NetSetGPIO(CtrlConn, 0xfffffcff);   // Set GPIO9, 8 low (powers up ADC and selects SPI port)
    NetSetGPIO(CtrlConn, ADC_SPI_GPIO);   // Set GPIO9, 8 low (powers up ADC and selects SPI port)
    nEnables[0] = 0x00;                 // Output
    nEnables[1] = 0x00;
    nEnables[2] = 0xff;                 // Input
    WriteData[0] = 0x80 | 0x00 | 0x00;  // RnW = 1 so read, W1:0=0 so read 1 byte, A12:8=0
    WriteData[1] = 0x01;                // A7:0 = 0x01 so read from register address 0x001
    WriteData[2] = 0x00;
    ReadData[0] = 0;
    ReadData[1] = 0;
    ReadData[2] = 0;
    NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
    //NetSetGPIO(CtrlConn, 0xfffffdff);   // Set GPIO8 high
	NetSetGPIO(CtrlConn, DEFAULT_GPIO);   // Set GPIO8 high
    printf("ADC model = %02x (should be 0x92)\n", ReadData[2]);

	//Set output mode to offset binary mode
    NetSetGPIO(CtrlConn, ADC_SPI_GPIO);   // Set GPIO8 low
    nEnables[0] = 0x00;                 // Output
    nEnables[1] = 0x00;
    nEnables[2] = 0x00;
    WriteData[0] = 0x00 | 0x00 | 0x00;  // RnW = 0 so write, W1:0=0 so read 1 byte, A12:8=0
    WriteData[1] = 0x14;                // A7:0 = 0x14 (write to output mode register)
	WriteData[2] = 0x00;				 // LVDS-ANSI, non inverted, offset binary
	// WriteData[2] = 0x01;				 // LVDS-ANSI, non inverted, twos complement
    ReadData[0] = 0;
    ReadData[1] = 0;
    ReadData[2] = 0;
    NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);   // Set GPIO8 high

# endif

#if 0
	// Use ADC test patterns

    // Write test generator enable register
    NetSetGPIO(CtrlConn, ADC_SPI_GPIO);   // Set GPIO8 low
    nEnables[0] = 0x00;                 // Output
    nEnables[1] = 0x00;
    nEnables[2] = 0x00;
    WriteData[0] = 0x00 | 0x00 | 0x00;  // RnW = 0 so write, W1:0=0 so read 1 byte, A12:8=0
    WriteData[1] = 0x0d;                // A7:0 = 0x0d so write to register address 0x00d (test mode)
    //WriteData[2] = 0x04;               // Checkerboard mode
    //WriteData[2] = 0x06;               // Pseudo-random short mode
    //WriteData[2] = 0x48;                 // Alternating user value mode
	WriteData[2] = 0x00;				 // Disable test mode	
    //WriteData[2] = 0x0b;                // One bit high
    ReadData[0] = 0;
    ReadData[1] = 0;
    ReadData[2] = 0;
    NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);   // Set GPIO8 high


    // Write test values to ADC test pattern generator
    NetSetGPIO(CtrlConn, ADC_SPI_GPIO);   // Set GPIO8 low
    nEnables[0] = 0x00;                 // Output
    nEnables[1] = 0x00;
    nEnables[2] = 0x00;
    WriteData[0] = 0x00 | 0x00 | 0x00;  // RnW = 0 so write, W1:0=0 so write 1 byte, A12:8=0
    WriteData[1] = 0x19;                // A7:0 = 0x019 so write to register address 0x019 (user test value)
    WriteData[2] = 0x34;
    ReadData[0] = 0;
    ReadData[1] = 0;
    ReadData[2] = 0;
    NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);   // Set GPIO8 high
    NetSetGPIO(CtrlConn, ADC_SPI_GPIO);   // Set GPIO8 low
    nEnables[0] = 0x00;                 // Output
    nEnables[1] = 0x00;
    nEnables[2] = 0x00;
    WriteData[0] = 0x00 | 0x00 | 0x00;  // RnW = 0 so write, W1:0=0 so write 1 byte, A12:8=0
    WriteData[1] = 0x1a;                // A7:0 = 0x01a so write to register address 0x01a (user test value)
    WriteData[2] = 0x12;                
    ReadData[0] = 0;
    ReadData[1] = 0;
    ReadData[2] = 0;
    NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);   // Set GPIO8 high
    NetSetGPIO(CtrlConn, ADC_SPI_GPIO);   // Set GPIO8 low
    nEnables[0] = 0x00;                 // Output
    nEnables[1] = 0x00;
    nEnables[2] = 0x00;
    WriteData[0] = 0x00 | 0x00 | 0x00;  // RnW = 0 so write, W1:0=0 so write 1 byte, A12:8=0
    WriteData[1] = 0x1b;                // A7:0 = 0x01b so write to register address 0x01b (user test value)
    WriteData[2] = 0x56;
    ReadData[0] = 0;
    ReadData[1] = 0;
    ReadData[2] = 0;
    NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);   // Set GPIO8 high
    NetSetGPIO(CtrlConn, ADC_SPI_GPIO);   // Set GPIO8 low
    nEnables[0] = 0x00;                 // Output
    nEnables[1] = 0x00;
    nEnables[2] = 0x00;
    WriteData[0] = 0x00 | 0x00 | 0x00;  // RnW = 0 so write, W1:0=0 so write 1 byte, A12:8=0
    WriteData[1] = 0x1c;                // A7:0 = 0x01c so write to register address 0x01c (user test value)
    WriteData[2] = 0x78;
    ReadData[0] = 0;
    ReadData[1] = 0;
    ReadData[2] = 0;
    NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, DEFAULT_GPIO);   // Set GPIO8 high
#endif

#if 1
    //
    // Test triggering
    //
	// 8192 is the mid level of the adc input
	// noise starts to trigger at 8615
    for (j=0; j<16; j++)  // Set all channels
      {
	NetWriteTriggerReg(CtrlConn, j, TRIGGER_TRIG_LEVEL_HIGH, (8500>>8)); 
	NetWriteTriggerReg(CtrlConn, j, TRIGGER_TRIG_LEVEL_LOW, (8500>>0));
	NetWriteTriggerReg(CtrlConn, j, TRIGGER_PRE_CALC_SAMPLES_HIGH, (5>>8));
	NetWriteTriggerReg(CtrlConn, j, TRIGGER_PRE_CALC_SAMPLES_LOW, (5>>0));
	NetWriteTriggerReg(CtrlConn, j, TRIGGER_POST_CALC_SAMPLES_HIGH, (12>>8));
	NetWriteTriggerReg(CtrlConn, j, TRIGGER_POST_CALC_SAMPLES_LOW, (12>>0));
	NetWriteTriggerReg(CtrlConn, j, TRIGGER_PRE_NET_SAMPLES_HIGH, (20>>8));
	NetWriteTriggerReg(CtrlConn, j, TRIGGER_PRE_NET_SAMPLES_LOW, (20>>0));
	NetWriteTriggerReg(CtrlConn, j, TRIGGER_POST_NET_SAMPLES_HIGH, (30>>8));
	NetWriteTriggerReg(CtrlConn, j, TRIGGER_POST_NET_SAMPLES_LOW, (30>>0));
	NetWriteTriggerReg(CtrlConn, j, TRIGGER_PEAK_THRESHOLD_HIGH, (7000>>8));
	NetWriteTriggerReg(CtrlConn, j, TRIGGER_PEAK_THRESHOLD_LOW, (7000>>0));
	NetWriteTriggerReg(CtrlConn, j, TRIGGER_AREA_THRESHOLD_HIGH, (3000>>16));
	NetWriteTriggerReg(CtrlConn, j, TRIGGER_AREA_THRESHOLD_MED, (3000>>8));
	NetWriteTriggerReg(CtrlConn, j, TRIGGER_AREA_THRESHOLD_LOW, (3000>>0));
	NetWriteTriggerReg(CtrlConn, j, TRIGGER_ENABLE, 1);
      }
    CalibrationPulse(CtrlConn);
    CalibrationPulse(CtrlConn);
    for (i=0; i<16000; i++){
      // CalibrationPulse(CtrlConn);
      //DumpData(DataConn,CtrlConn);
      DumpDataRaw(DataConn,CtrlConn);
    }
#endif

    //
    // Free the card information structure
    //
    ZestET1CloseConnection(CtrlConn);
    ZestET1CloseConnection(DataConn);
    ZestET1FreeCards(CardInfo);

    //
    // Close library
    //
    ZestET1Close();

    return 0;
}

//////////////////////////////////////////////////////////////////////
//
// File:      Example6.c
//
// Purpose:
//    ZestET1 Example Programs
//    Ethernet bandwidth test using wrapper code
//  
// Copyright (c) 2011 Orange Tree Technologies.
// May not be reproduced without permission.
//
//////////////////////////////////////////////////////////////////////

#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "ZestET1.h"

//
// Specify length of test
//
#define TRANSFER_LENGTH (5*1024*1024)
#define NUM_TRANSFERS 100

//
// Define this to check the data coming back from the ZestET1
//
//#define CHECK_DATA

//
// Error handler function
//
void ErrorHandler(const char *Function, 
                  ZESTET1_CARD_INFO *CardInfo,
                  ZESTET1_STATUS Status,
                  const char *Msg)
{
    printf("**** Example6 - Function %s returned an error\n        \"%s\"\n\n", Function, Msg);
    exit(1);
}

//
// Main program
//
int main(int argc, char **argv)
{
    unsigned long Count;
    unsigned long NumCards;
    ZESTET1_CARD_INFO *CardInfo;
    ZESTET1_CONNECTION Connection;
    struct timeval Start;
    unsigned long long uSecStart;
    struct timeval End;
    unsigned long long uSecEnd;
    unsigned char *Buffer;
    int i;

    //
    // Install an error handler and initialise library
    //
    ZestET1RegisterErrorHandler(ErrorHandler);
    ZestET1Init();

    //
    // Request information about the system
    // Wait for 2 seconds (on each interface) for boards to respond to query
    //
    printf("Searching for ZestET1 cards...\n");
    ZestET1CountCards(&NumCards, &CardInfo, 2000);
    printf("%ld available cards in the system\n\n\n", NumCards);
    if (NumCards==0)
    {
        printf("No cards found\n");
        exit(1);
    }

    for (Count=0; Count<NumCards; Count++)
    {
        printf("%ld : SerialNum = %ld, IPAddress = %d.%d.%d.%d\n",
            Count, CardInfo[Count].SerialNumber,
            CardInfo[Count].IPAddr[0], CardInfo[Count].IPAddr[1],
            CardInfo[Count].IPAddr[2], CardInfo[Count].IPAddr[3]);
    }

    //
    // Allocate buffer
    //
    Buffer = (unsigned char *)malloc(TRANSFER_LENGTH);

    //
    // Configure the FPGA directly from a file
    // Wait for user FPGA application to start listening on its connection
    // Open connection to user FPGA and start timing
    //
    ZestET1ConfigureFromFile(&CardInfo[0], "example6.bit");
    sleep(2);
    ZestET1OpenConnection(&CardInfo[0], ZESTET1_TYPE_TCP, 0x5002, 0, &Connection);

    //
    // Read data from card
    //
    gettimeofday(&Start, NULL);
    for (Count=0; Count<NUM_TRANSFERS; Count++)
    {
#ifdef CHECK_DATA
        memset(Buffer, 0, TRANSFER_LENGTH);
#endif

        ZestET1ReadData(Connection, Buffer, TRANSFER_LENGTH, NULL, 10000);

#ifdef CHECK_DATA
        for (i=0; i<TRANSFER_LENGTH; i++)
        {
            if (Buffer[i] != (i&0xff))
            {
                printf("Error at 0x%08x: Expected 0x%02x, got 0x%02x\n", 
                    i, i&0xff, Buffer[i]);
            }
        }
#endif
    }
    gettimeofday(&End, NULL);
    uSecStart = (unsigned long long)Start.tv_sec*1000000ull + (unsigned long long)Start.tv_usec;
    uSecEnd = (unsigned long long)End.tv_sec*1000000ull + (unsigned long long)End.tv_usec;
    printf("Transfer rate from ZestET1 to host = %.2fMBytes/sec\n", 
           ((double)NUM_TRANSFERS*(double)TRANSFER_LENGTH) /
		(((double)(uSecEnd-uSecStart)/1000000.)*1024*1024));

    //
    // Write data to card
    //
    for (i=0; i<TRANSFER_LENGTH; i++)
        Buffer[i] = i;
    gettimeofday(&Start, NULL);
    for (Count=0; Count<NUM_TRANSFERS; Count++)
    {
        ZestET1WriteData(Connection, Buffer, TRANSFER_LENGTH, NULL, 10000);
    }
    gettimeofday(&End, NULL);
    uSecStart = (unsigned long long)Start.tv_sec*1000000ull + (unsigned long long)Start.tv_usec;
    uSecEnd = (unsigned long long)End.tv_sec*1000000ull + (unsigned long long)End.tv_usec;
    printf("Transfer rate from host to ZestET1 = %.2fMBytes/sec\n", 
           ((double)NUM_TRANSFERS*(double)TRANSFER_LENGTH) /
		(((double)(uSecEnd-uSecStart)/1000000.)*1024*1024));

    //
    // Free the card information structure
    //
    ZestET1CloseConnection(Connection);
    ZestET1FreeCards(CardInfo);

    //
    // Close library
    //
    ZestET1Close();

    return 0;
}

--   ZestET1 Example 1
--   File name: Example1.vhd
--   Version: 1.00
--   Date: 26/7/2009
--
--   ZestET1 Example 1 - Dummy example file.
--   Empty top level file.  Serves as an example configuration file to
--   demonstarte the User FPGA configuration functions and as a starting
--   point for new user designs.
--
--   Copyright (C) 2009 Orange Tree Technologies Ltd. All rights reserved.
--   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and 
--   modify this logic core in any form such as VHDL source code or EDIF netlist in 
--   FPGA designs that target the ZestET1.
--   Orange Tree Technologies prohibits the use of this logic core in any form such 
--   as VHDL source code or EDIF netlist in FPGA designs that target any other
--   hardware unless the purchaser of the ZestET1 has purchased the appropriate 
--   licence from Orange Tree Technologies. Contact Orange Tree Technologies if you 
--   want to purchase such a licence.
--
--  *****************************************************************************************
--  **
--  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
--  **              provided to you "as is". Orange Tree Technologies and its licensors 
--  **              make and you receive no warranties or conditions, express, implied, 
--  **              statutory or otherwise, and Orange Tree Technologies specifically 
--  **              disclaims any implied warranties of merchantability, non-infringement,
--  **              or fitness for a particular purpose. Orange Tree Technologies does not
--  **              warrant that the functions contained in these designs will meet your 
--  **              requirements, or that the operation of these designs will be 
--  **              uninterrupted or error free, or that defects in the Designs will be 
--  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
--  **              make any representations regarding use or the results of the use of the 
--  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
--  **
--  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
--  **              or its licensors be liable for any loss of data, lost profits, cost or 
--  **              procurement of substitute goods or services, or for any special, 
--  **              incidental, consequential, or indirect damages arising from the use or 
--  **              operation of the designs or accompanying documentation, however caused 
--  **              and on any theory of liability. This limitation will apply even if 
--  **              Orange Tree Technologies has been advised of the possibility of such 
--  **              damage. This limitation shall apply notwithstanding the failure of the 
--  **              essential purpose of any limited remedies herein.
--  **
--  *****************************************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity Example1 is
    port (
        -- Programmable Clock
        CLK : in std_logic;
        
        -- Flash Interface
        Flash_CSOn : out std_logic;
        Flash_CLK : out std_logic;
        Flash_MOSI : out std_logic;
        Flash_MISO : in std_logic;

        -- IO connector
        IO0_In : in std_logic;
        IO0 : inout std_logic_vector(39 downto 0);
        IO3_In : in std_logic_vector(2 downto 0);
        IO3 : inout std_logic_vector(35 downto 0);

        -- Ethernet Interface
        Eth_Clk : out std_logic;
        Eth_CSn : out std_logic;
        Eth_WEn : out std_logic;
        Eth_A : out std_logic_vector(4 downto 0);
        Eth_D : inout std_logic_vector(15 downto 0);
        Eth_BE : out std_logic_vector(1 downto 0);
        Eth_Intn : in std_logic;

        -- DDR SDRAM interface
        DS_CLK_P : out std_logic;
        DS_CLK_N : out std_logic;
        DS_CKE : out std_logic;
        DS_A : out std_logic_vector(12 downto 0);
        DS_BA : out std_logic_vector(1 downto 0);
        DS_CAS_N : out std_logic;
        DS_RAS_N : out std_logic;
        DS_WE_N : out std_logic;
        DS_DQS : inout std_logic_vector(1 downto 0);
        DS_DM : out std_logic_vector(1 downto 0);
        DS_DQ : inout std_logic_vector(15 downto 0)
    );
end Example1;

architecture arch of Example1 is

    --------------------------------------------------------------------------
    -- Declare signals
    
    -- Clocks and reset
    signal RST : std_logic;
    signal RAMRST : std_logic;
    signal RAMClk : std_logic;
    signal RAMClk90 : std_logic;
    signal EthClk : std_logic;

    -- Ethernet interface
    signal EthernetWE : std_logic;
    signal EthernetRE : std_logic;
    signal EthernetOwner : std_logic_vector(7 downto 0);
    signal EthernetAddr : std_logic_vector(4 downto 0);
    signal EthernetBE : std_logic_vector(1 downto 0);
    signal EthernetWriteData : std_logic_vector(15 downto 0);
    signal EthernetReadData : std_logic_vector(15 downto 0);
    signal EthernetReadDataValid : std_logic;
    signal EthernetValidOwner : std_logic_vector(7 downto 0);
    signal EthernetInterrupt : std_logic;

    -- SDRAM interface
    signal SDRAMAddr : std_logic_vector(23 downto 0);
    signal SDRAMRE : std_logic;
    signal SDRAMWE : std_logic;
    signal SDRAMOwner : std_logic_vector(7 downto 0);
    signal SDRAMReadDataValid : std_logic;
    signal SDRAMValidOwner : std_logic_vector(7 downto 0);
    signal SDRAMBE : std_logic_vector(3 downto 0);
    signal SDRAMWriteData : std_logic_vector(31 downto 0);
    signal SDRAMReadData : std_logic_vector(31 downto 0);
    signal SDRAMBusy : std_logic;
    signal SDRAMInitDone : std_logic;

begin

    --------------------------------------------------------------------------
    -- Starting point for new designs
    
    -- Flash
    Flash_CSOn <= '1';
    Flash_CLK <= '1';
    Flash_MOSI <= '1';
    -- xxx <= Flash_MISO;
    
    -- IO connector
    IO0 <= (others=>'Z');
    IO3 <= (others=>'Z');
    -- xxx = IO0_In;
    -- xxx = IO3_In,
    
    -- Ethernet
    EthernetWE <= '0';
    EthernetRE <= '0';
    EthernetAddr <= (others=>'0');
    EthernetBE <= (others=>'0');
    EthernetWriteData <= (others=>'0');
    EthernetOwner <= (others=>'0');
    -- xxx <= EthernetReadData;
    -- xxx <= EthernetReadDataValid;
    -- xxx <= EthernetValidOwner;
    -- xxx <= EthernetInterrupt;
    
    -- SDRAM
    SDRAMAddr <= (others=>'0');
    SDRAMRE <= '0';
    SDRAMWE <= '0';
    SDRAMOwner <= (others=>'0');
    SDRAMBE <= (others=>'0');
    SDRAMWriteData <= (others=>'0');
    -- xxx <= SDRAMReadDataValid;
    -- xxx <= SDRAMValidOwner;
    -- xxx <= SDRAMReadData;
    -- xxx <= SDRAMBusy;
    
    -- Reset controller
    ROCInst : ROC port map (O=>RST);
    
    --------------------------------------------------------------------------
    -- Instantiate clocks
    -- (Assumes default 125MHz reference clock)
    ClockInst : entity work.ZestET1_Clocks
        port map (
            RST => RST,
            RefClk => CLK,
            EthClk => EthClk,
            RAMRST => RAMRST,
            RAMClk => RAMClk,
            RAMClk90 => RAMClk90
        );

    --------------------------------------------------------------------------
    -- Ethernet interface
    EthernetInst : entity work.ZestET1_Ethernet
        port map (
            User_CLK => EthClk,
            User_RST => RST,

            -- User interface
            User_WE => EthernetWE,
            User_RE => EthernetRE,
            User_Addr => EthernetAddr,
            User_WriteData => EthernetWriteData,
            User_BE => EthernetBE,
            User_Owner => EthernetOwner,
            User_ReadData => EthernetReadData,
            User_ReadDataValid => EthernetReadDataValid,
            User_ValidOwner => EthernetValidOwner,
            User_Interrupt => EthernetInterrupt,
            
            -- Interface to GigExpedite
            Eth_Clk => Eth_Clk,
            Eth_CSn => Eth_CSn,
            Eth_WEn => Eth_WEn,
            Eth_A => Eth_A,
            Eth_D => Eth_D,
            Eth_BE => Eth_BE,
            Eth_Intn => Eth_Intn
        );

    --------------------------------------------------------------------------
    -- SDRAM Buffer
    SDRAMInst : entity work.ZestET1_SDRAM
        port map (
            User_CLK => RAMClk,
            User_CLK90 => RAMClk90,
            User_RST => RAMRST,

            User_A => SDRAMAddr,
            User_RE => SDRAMRE,
            User_WE => SDRAMWE,
            User_Owner => SDRAMOwner,
            User_BE => SDRAMBE,
            User_DW => SDRAMWriteData,
            User_DR => SDRAMReadData,
            User_DR_Valid => SDRAMReadDataValid,
            User_ValidOwner => SDRAMValidOwner,
            User_Busy => SDRAMBusy,
            User_InitDone => SDRAMInitDone,

            DS_CLK_P => DS_CLK_P,
            DS_CLK_N => DS_CLK_N,
            DS_A => DS_A,
            DS_BA => DS_BA,
            DS_DQ => DS_DQ,
            DS_DQS => DS_DQS,
            DS_DM => DS_DM,
            DS_CAS_N => DS_CAS_N,
            DS_RAS_N => DS_RAS_N,
            DS_CKE => DS_CKE,
            DS_WE_N => DS_WE_N
        );
        
end arch;

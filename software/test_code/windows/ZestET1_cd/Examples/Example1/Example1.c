//////////////////////////////////////////////////////////////////////
//
// File:      Example1.c
//
// Purpose:
//    ZestET1 Example Programs
//    Simple card open/configure/close example
//  
// Copyright (c) 2009 Orange Tree Technologies.
// May not be reproduced without permission.
//
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include "ZestET1.h"

//
// Declare configuration images
//
extern unsigned long FPGAImageLength;
extern unsigned long FPGAImageImage[];

//
// Error handler function
//
void ErrorHandler(const char *Function, 
                  ZESTET1_CARD_INFO *CardInfo,
                  ZESTET1_STATUS Status,
                  const char *Msg)
{
    printf("**** Example1 - Function %s returned an error\n        \"%s\"\n\n", Function, Msg);
    exit(1);
}

//
// Main program
//
int main(int argc, char **argv)
{
    unsigned long Count;
    unsigned long NumCards;
    ZESTET1_CARD_INFO *CardInfo;
    ZESTET1_IMAGE Image;

    //
    // Install an error handler and initialise library
    //
    ZestET1RegisterErrorHandler(ErrorHandler);
    ZestET1Init();

    //
    // Request information about the system
    // Wait for 2 seconds (on each network interface) for boards to respond
    //
    printf("Searching for ZestET1 cards...\n");
    ZestET1CountCards(&NumCards, &CardInfo, 2000);
    printf("%ld available cards in the system\n\n\n", NumCards);
    if (NumCards==0)
    {
        printf("No cards found\n");
        exit(1);
    }

    for (Count=0; Count<NumCards; Count++)
    {
        printf("%ld : SerialNum = %ld, IPAddress = %d.%d.%d.%d\n",
            Count, CardInfo[Count].SerialNumber, CardInfo[Count].IPAddr[0],
            CardInfo[Count].IPAddr[1], CardInfo[Count].IPAddr[2],
            CardInfo[Count].IPAddr[3]);
    }

    //
    // Method 1: Configure the FPGA directly from a file
    //
    printf("Configuring FPGA directly from BIT file\n");
    ZestET1ConfigureFromFile(&CardInfo[0], "Example1.bit");
    printf("Done LED should be on - Press Enter to continue\n");
    while(getchar()!='\n');

    //
    // Method 2: Pre-load image and configure later
    //
    printf("Configuring FPGA from pre-loaded BIT file\n");
    ZestET1LoadFile("Example1.bit", &Image);
    ZestET1Configure(&CardInfo[0], Image);
    ZestET1FreeImage(Image);
    printf("Done LED should be on - Press Enter to continue\n");
    while(getchar()!='\n');

    //
    // Method 3: configure from a linked image
    // The linked images are created by the Bit2C utility
    //
    printf("Configuring FPGA from embedded BIT file\n");
    ZestET1RegisterImage(FPGAImageImage, FPGAImageLength, &Image);

    ZestET1Configure(&CardInfo[0], Image);
    ZestET1FreeImage(Image);
    printf("Done LED should be on - Press Enter to continue\n");
    while(getchar()!='\n');

    //
    // Free the card information structure
    //
    ZestET1FreeCards(CardInfo);

    //
    // Close library
    //
    ZestET1Close();

    return 0;
}

/*
   ZestET1 Example 1
   File name: Example1.v
   Version: 1.00
   Date: 26/7/2009

   ZestET1 Example 1 - Dummy example file.
   Empty top level file.  Serves as an example configuration file to
   demonstarte the User FPGA configuration functions and as a starting
   point for new user designs.

   Copyright (C) 2009 Orange Tree Technologies Ltd. All rights reserved.
   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and 
   modify this logic core in any form such as Verilog source code or EDIF netlist in 
   FPGA designs that target the ZestET1.
   Orange Tree Technologies prohibits the use of this logic core in any form such 
   as Verilog source code or EDIF netlist in FPGA designs that target any other
   hardware unless the purchaser of the ZestET1 has purchased the appropriate 
   licence from Orange Tree Technologies. Contact Orange Tree Technologies if you 
   want to purchase such a licence.

  *****************************************************************************************
  **
  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
  **              provided to you "as is". Orange Tree Technologies and its licensors 
  **              make and you receive no warranties or conditions, express, implied, 
  **              statutory or otherwise, and Orange Tree Technologies specifically 
  **              disclaims any implied warranties of merchantability, non-infringement,
  **              or fitness for a particular purpose. Orange Tree Technologies does not
  **              warrant that the functions contained in these designs will meet your 
  **              requirements, or that the operation of these designs will be 
  **              uninterrupted or error free, or that defects in the Designs will be 
  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
  **              make any representations regarding use or the results of the use of the 
  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
  **
  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
  **              or its licensors be liable for any loss of data, lost profits, cost or 
  **              procurement of substitute goods or services, or for any special, 
  **              incidental, consequential, or indirect damages arising from the use or 
  **              operation of the designs or accompanying documentation, however caused 
  **              and on any theory of liability. This limitation will apply even if 
  **              Orange Tree Technologies has been advised of the possibility of such 
  **              damage. This limitation shall apply notwithstanding the failure of the 
  **              essential purpose of any limited remedies herein.
  **
  *****************************************************************************************
*/

`timescale 1ns / 1ps

module Example1
(
    // Programmable Clock
    input CLK,
    
    // Flash Interface
    output Flash_CSOn,
    output Flash_CLK,
    output Flash_MOSI,
    input Flash_MISO,

    // IO connector
    input IO0_In,
    inout [39:0] IO0,
    input [2:0] IO3_In,
    inout [35:0] IO3,

    // Ethernet Interface
    output Eth_Clk,
    output Eth_CSn,
    output Eth_WEn,
    output [4:0] Eth_A,
    inout [15:0] Eth_D,
    output [1:0] Eth_BE,
    input Eth_Intn,

    // DDR SDRAM interface
    output DS_CLK_P,
    output DS_CLK_N,
    output DS_CKE,
    output [12:0] DS_A,
    output [1:0] DS_BA,
    output DS_CAS_N,
    output DS_RAS_N,
    output DS_WE_N,
    inout [1:0] DS_DQS,
    output [1:0] DS_DM,
    inout [15:0] DS_DQ
);

    //////////////////////////////////////////////////////////////////////////
    // Declare signals
    
    // Clocks and reset
    wire RST;
    wire RAMRST;
    wire RAMClk;
    wire RAMClk90;
    wire EthClk;

    // Ethernet interface
    wire EthernetWE;
    wire EthernetRE;
    wire [7:0] EthernetOwner;
    wire [4:0] EthernetAddr;
    wire [1:0] EthernetBE;
    wire [15:0] EthernetWriteData;
    wire [15:0] EthernetReadData;
    wire EthernetReadDataValid;
    wire [7:0] EthernetValidOwner;
    wire EthernetInterrupt;

    // SDRAM interface
    wire [23:0] SDRAMAddr;
    wire SDRAMRE;
    wire SDRAMWE;
    wire [7:0] SDRAMOwner;
    wire SDRAMReadDataValid;
    wire [7:0] SDRAMValidOwner;
    wire [3:0] SDRAMBE;
    wire [31:0] SDRAMWriteData;
    wire [31:0] SDRAMReadData;
    wire SDRAMBusy;
    wire SDRAMInitDone;

    //////////////////////////////////////////////////////////////////////////
    // Starting point for new designs
    
    // Flash
    assign Flash_CSOn = 1;
    assign Flash_CLK = 1;
    assign Flash_MOSI = 1;
    //assign xxx = Flash_MISO;
    
    // IO connector
    assign IO0 = 40'hZ;
    assign IO3 = 36'hZ;
    //assign xxx = IO0_In;
    //assign xxx = IO3_In,
    
    // Ethernet
    assign EthernetWE = 0;
    assign EthernetRE = 0;
    assign EthernetAddr = 0;
    assign EthernetBE = 0;
    assign EthernetWriteData = 0;
    assign EthernetOwner = 0;
    //assign xxx = EthernetReadData;
    //assign xxx = EthernetReadDataValid;
    //assign xxx = EthernetValidOwner;
    //assign xxx = EthernetInterrupt;
    
    // SDRAM
    assign SDRAMAddr = 0;
    assign SDRAMRE = 0;
    assign SDRAMWE = 0;
    assign SDRAMOwner = 0;
    assign SDRAMBE = 0;
    assign SDRAMWriteData = 0;
    //assign xxx = SDRAMReadDataValid;
    //assign xxx = SDRAMValidOwner;
    //assign xxx = SDRAMReadData;
    //assign xxx = SDRAMBusy;
    
    //////////////////////////////////////////////////////////////////////////
    // Instantiate clocks
    // (Assumes default 125MHz reference clock)
    ZestET1_Clocks ClockInst (
        .RST(RST),
        .RefClk(CLK),
        .EthClk(EthClk),
        .RAMRST(RAMRST),
        .RAMClk(RAMClk),
        .RAMClk90(RAMClk90)
    );

    //////////////////////////////////////////////////////////////////////////
    // Ethernet interface
    ZestET1_Ethernet EthernetInst (
        .User_CLK(EthClk),
        .User_RST(RST),

        // User interface
        .User_WE(EthernetWE),
        .User_RE(EthernetRE),
        .User_Addr(EthernetAddr),
        .User_WriteData(EthernetWriteData),
        .User_BE(EthernetBE),
        .User_Owner(EthernetOwner),
        .User_ReadData(EthernetReadData),
        .User_ReadDataValid(EthernetReadDataValid),
        .User_ValidOwner(EthernetValidOwner),
        .User_Interrupt(EthernetInterrupt),
        
        // Interface to GigExpedite
        .Eth_Clk(Eth_Clk),
        .Eth_CSn(Eth_CSn),
        .Eth_WEn(Eth_WEn),
        .Eth_A(Eth_A),
        .Eth_D(Eth_D),
        .Eth_BE(Eth_BE),
        .Eth_Intn(Eth_Intn)
    );

    //////////////////////////////////////////////////////////////////////////
    // SDRAM Buffer
    ZestET1_SDRAM SDRAMInst (
        .User_CLK(RAMClk),
        .User_CLK90(RAMClk90),
        .User_RST(RAMRST),

        .User_A(SDRAMAddr),
        .User_RE(SDRAMRE),
        .User_WE(SDRAMWE),
        .User_Owner(SDRAMOwner),
        .User_BE(SDRAMBE),
        .User_DW(SDRAMWriteData),
        .User_DR(SDRAMReadData),
        .User_DR_Valid(SDRAMReadDataValid),
        .User_ValidOwner(SDRAMValidOwner),
        .User_Busy(SDRAMBusy),
        .User_InitDone(SDRAMInitDone),

        .DS_CLK_P(DS_CLK_P),
        .DS_CLK_N(DS_CLK_N),
        .DS_A(DS_A),
        .DS_BA(DS_BA),
        .DS_DQ(DS_DQ),
        .DS_DQS(DS_DQS),
        .DS_DM(DS_DM),
        .DS_CAS_N(DS_CAS_N),
        .DS_RAS_N(DS_RAS_N),
        .DS_CKE(DS_CKE),
        .DS_WE_N(DS_WE_N)
    );
    
endmodule

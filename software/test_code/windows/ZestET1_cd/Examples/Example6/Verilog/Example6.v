/*
   ZestET1 Example 6
   File name: Example6.v
   Version: 1.00
   Date: 27/5/2011

   ZestET1 Example 6 - TCP test code using wrapper IP.
   Copies data to and from the Ethernet interface

   Copyright (C) 2011 Orange Tree Technologies Ltd. All rights reserved.
   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and 
   modify this logic core in any form such as Verilog source code or EDIF netlist in 
   FPGA designs that target the ZestET1.
   Orange Tree Technologies prohibits the use of this logic core in any form such 
   as Verilog source code or EDIF netlist in FPGA designs that target any other
   hardware unless the purchaser of the ZestET1 has purchased the appropriate 
   licence from Orange Tree Technologies. Contact Orange Tree Technologies if you 
   want to purchase such a licence.

  *****************************************************************************************
  **
  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
  **              provided to you "as is". Orange Tree Technologies and its licensors 
  **              make and you receive no warranties or conditions, express, implied, 
  **              statutory or otherwise, and Orange Tree Technologies specifically 
  **              disclaims any implied warranties of merchantability, non-infringement,
  **              or fitness for a particular purpose. Orange Tree Technologies does not
  **              warrant that the functions contained in these designs will meet your 
  **              requirements, or that the operation of these designs will be 
  **              uninterrupted or error free, or that defects in the Designs will be 
  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
  **              make any representations regarding use or the results of the use of the 
  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
  **
  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
  **              or its licensors be liable for any loss of data, lost profits, cost or 
  **              procurement of substitute goods or services, or for any special, 
  **              incidental, consequential, or indirect damages arising from the use or 
  **              operation of the designs or accompanying documentation, however caused 
  **              and on any theory of liability. This limitation will apply even if 
  **              Orange Tree Technologies has been advised of the possibility of such 
  **              damage. This limitation shall apply notwithstanding the failure of the 
  **              essential purpose of any limited remedies herein.
  **
  *****************************************************************************************
*/

`timescale 1ns / 1ps

module Example6
(
    // Programmable Clock
    input CLK,
    
    // Flash Interface
    output Flash_CSOn,
    output Flash_CLK,
    output Flash_MOSI,
    input Flash_MISO,

    // IO connector
    input IO0_In,
    inout [39:0] IO0,
    input [2:0] IO3_In,
    inout [35:0] IO3,

    // Ethernet Interface
    output Eth_Clk,
    output Eth_CSn,
    output Eth_WEn,
    output [4:0] Eth_A,
    inout [15:0] Eth_D,
    output [1:0] Eth_BE,
    input Eth_Intn,

    // DDR SDRAM interface
    output DS_CLK_P,
    output DS_CLK_N,
    output DS_CKE,
    output [12:0] DS_A,
    output [1:0] DS_BA,
    output DS_CAS_N,
    output DS_RAS_N,
    output DS_WE_N,
    inout [1:0] DS_DQS,
    output [1:0] DS_DM,
    inout [15:0] DS_DQ
);

    //////////////////////////////////////////////////////////////////////////
    // Declare constants
    
    // Test transfer length
    localparam WRITE_LENGTH = 500*1024*1024;
    
    //////////////////////////////////////////////////////////////////////////
    // Declare signals
    
    // Clocks and reset
    wire RST;
    wire RAMRST;
    wire RAMClk;
    wire RAMClk90;
    wire EthClk;

    // Link status variables
    wire [31:0] LocalIP;
    wire LinkStatus;
    wire LinkDuplex;
    wire [2:0] LinkSpeed;
    wire [31:0] RemoteIPAddr;
    wire [15:0] RemotePort;
    
    // Connection state signals
    reg EnableConnection;
    wire Connected;
    reg LastConnected;

    // Test data signals
    reg [7:0] RampData;
    reg [31:0] WriteCount;
    wire TxEnable;
    wire TxBusy;
    wire TxBufferEmpty;

    wire RxEnable;
    wire [7:0] RxData;
    wire RxBusy;

    //////////////////////////////////////////////////////////////////////////
    // Tie unused signals
    assign Flash_CSOn = 1;
    assign Flash_CLK = 1;
    assign Flash_MOSI = 1;
    assign IO0 = 40'hZ;
    assign IO3 = 36'hZ;

    //////////////////////////////////////////////////////////////////////////
    // Main example code
    
    // Connection management
    // Open a connection whenever possible
    // If the host closes the connection then we immediately re-open it
    // NB: If the connection isn't closed (e.g. host application crashes)
    // then we would need to close it from this end before we could re-open it
    always @ (posedge RAMRST or posedge EthClk) begin
        if (RAMRST==1) begin
            EnableConnection <= 0;
            LastConnected <= 0;
        end else begin
            LastConnected <= Connected;
            if (EnableConnection==0) begin
                // Rising edge will start listening on connection
                EnableConnection <= 1;
            end else begin
                if (LastConnected==1 && Connected==0) begin
                    // Falling edge indicates connection was closed
                    // We need to force a rising edge on EnableConnection
                    // to start listening again
                    EnableConnection <= 0;
                end
            end
        end
    end
    
    // Generate test ramp data
    always @ (posedge RAMRST or posedge EthClk) begin
        if (RAMRST==1) begin
            RampData <= 8'h00;
            WriteCount <= 0;
        end else begin
            if (Connected==0) begin
                // Reset ramp generator
                WriteCount <= 0;
                RampData <= 8'h00;
            end else if (TxEnable==1) begin
                // Increment ramp generator
                WriteCount <= WriteCount+1;
                RampData <= RampData+1;
            end
        end
    end
    assign TxEnable = Connected==1 &&
                      WriteCount!=WRITE_LENGTH &&
                      TxBusy==0 ? 1 : 0;
    
    // NB: Data from the interface appears on these Rx signals:
    // if (RxEnable==1) begin
    //     ??? <= RxData;
    // end
    assign RxBusy = 0;

    //////////////////////////////////////////////////////////////////////////
    // Instantiate clocks
    // (Assumes default 125MHz reference clock)
    ZestET1_Clocks ClockInst (
        .RST(RST),
        .RefClk(CLK),
        .EthClk(EthClk),
        .RAMRST(RAMRST),
        .RAMClk(RAMClk),
        .RAMClk90(RAMClk90)
    );

    //////////////////////////////////////////////////////////////////////////
    // Ethernet interface wrapper
    ZestET1_EthernetWrapper #(.CLOCK_RATE(125000000))
        EthernetInst (
            .User_CLK(EthClk),
            .User_RST(RST),
            .User_LocalIPAddr(LocalIP),
            .User_LinkStatus(LinkStatus),
            .User_LinkDuplex(LinkDuplex),
            .User_LinkSpeed(LinkSpeed),

            // User interface - only one connection used
            // Control ports
            .User_Enable0(EnableConnection),
            .User_Server0(1'b1),
            .User_TCP0(1'b1),
            .User_TxPause0(1'b0),
            //.User_TxPaused0(),
            .User_Connected0(Connected),
            .User_MTU0(8'h80),
            .User_TTL0(8'h80),
            .User_LocalPort0(16'h5002),
            .User_WriteRemotePort0(16'h0000),
            .User_WriteRemoteIPAddr0(32'h00000000),
            .User_ReadRemotePort0(RemotePort),
            .User_ReadRemoteIPAddr0(RemoteIPAddr),
            
            // Transmit and receive ports
            .User_TxData0(RampData),
            .User_TxEnable0(TxEnable),
            .User_TxBusy0(TxBusy),
            .User_TxBufferEmpty0(TxBufferEmpty),
            .User_RxData0(RxData),
            .User_RxEnable0(RxEnable),
            .User_RxBusy0(RxBusy),
            
            // Disable all other connections
            .User_Enable1(1'b0),
            .User_Enable2(1'b0),
            .User_Enable3(1'b0),
            .User_Enable4(1'b0),
            .User_Enable5(1'b0),
            .User_Enable6(1'b0),
            .User_Enable7(1'b0),
            .User_Enable8(1'b0),
            .User_Enable9(1'b0),
            .User_Enable10(1'b0),
            .User_Enable11(1'b0),
            .User_Enable12(1'b0),
            .User_Enable13(1'b0),
            .User_Enable14(1'b0),
            .User_Enable15(1'b0),
            
            // Interface to GigExpedite
            .Eth_Clk(Eth_Clk),
            .Eth_CSn(Eth_CSn),
            .Eth_WEn(Eth_WEn),
            .Eth_A(Eth_A),
            .Eth_D(Eth_D),
            .Eth_BE(Eth_BE),
            .Eth_Intn(Eth_Intn)
        );

    //////////////////////////////////////////////////////////////////////////
    // SDRAM Buffer
    // Place holder - not used in the example but ties the pins correctly
    ZestET1_SDRAM  #(.CLOCK_RATE(166666666))
        SDRAMInst (
            .User_CLK(RAMClk),
            .User_CLK90(RAMClk90),
            .User_RST(RAMRST),

            .User_A(0),
            .User_RE(0),
            .User_WE(0),
            .User_Owner(8'h00),
            .User_BE(0),
            .User_DW(0),
            //.User_DR(),
            //.User_DR_Valid(),
            //.User_ValidOwner(),
            //.User_Busy(),
            //.User_InitDone(),

            .DS_CLK_P(DS_CLK_P),
            .DS_CLK_N(DS_CLK_N),
            .DS_A(DS_A),
            .DS_BA(DS_BA),
            .DS_DQ(DS_DQ),
            .DS_DQS(DS_DQS),
            .DS_DM(DS_DM),
            .DS_CAS_N(DS_CAS_N),
            .DS_RAS_N(DS_RAS_N),
            .DS_CKE(DS_CKE),
            .DS_WE_N(DS_WE_N)
        );
    
endmodule

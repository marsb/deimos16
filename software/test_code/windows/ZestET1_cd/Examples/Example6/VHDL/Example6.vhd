--   ZestET1 Example 6
--   File name: Example6.vhd
--   Version: 1.00
--   Date: 27/5/2011
--
--   ZestET1 Example 7 - TCP test code using wrapper IP.
--   Copies data to and from the Ethernet interface
--
--   Copyright (C) 2011 Orange Tree Technologies Ltd. All rights reserved.
--   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and 
--   modify this logic core in any form such as VHDL source code or EDIF netlist in 
--   FPGA designs that target the ZestET1.
--   Orange Tree Technologies prohibits the use of this logic core in any form such 
--   as VHDL source code or EDIF netlist in FPGA designs that target any other
--   hardware unless the purchaser of the ZestET1 has purchased the appropriate 
--   licence from Orange Tree Technologies. Contact Orange Tree Technologies if you 
--   want to purchase such a licence.
--
--  *****************************************************************************************
--  **
--  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
--  **              provided to you "as is". Orange Tree Technologies and its licensors 
--  **              make and you receive no warranties or conditions, express, implied, 
--  **              statutory or otherwise, and Orange Tree Technologies specifically 
--  **              disclaims any implied warranties of merchantability, non-infringement,
--  **              or fitness for a particular purpose. Orange Tree Technologies does not
--  **              warrant that the functions contained in these designs will meet your 
--  **              requirements, or that the operation of these designs will be 
--  **              uninterrupted or error free, or that defects in the Designs will be 
--  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
--  **              make any representations regarding use or the results of the use of the 
--  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
--  **
--  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
--  **              or its licensors be liable for any loss of data, lost profits, cost or 
--  **              procurement of substitute goods or services, or for any special, 
--  **              incidental, consequential, or indirect damages arising from the use or 
--  **              operation of the designs or accompanying documentation, however caused 
--  **              and on any theory of liability. This limitation will apply even if 
--  **              Orange Tree Technologies has been advised of the possibility of such 
--  **              damage. This limitation shall apply notwithstanding the failure of the 
--  **              essential purpose of any limited remedies herein.
--  **
--  *****************************************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity Example6 is
    port (
        -- Programmable Clock
        CLK : in std_logic;
        
        -- Flash Interface
        Flash_CSOn : out std_logic;
        Flash_CLK : out std_logic;
        Flash_MOSI : out std_logic;
        Flash_MISO : in std_logic;

        -- IO connector
        IO0_In : in std_logic;
        IO0 : inout std_logic_vector(39 downto 0);
        IO3_In : in std_logic_vector(2 downto 0);
        IO3 : inout std_logic_vector(35 downto 0);

        -- Ethernet Interface
        Eth_Clk : out std_logic;
        Eth_CSn : out std_logic;
        Eth_WEn : out std_logic;
        Eth_A : out std_logic_vector(4 downto 0);
        Eth_D : inout std_logic_vector(15 downto 0);
        Eth_BE : out std_logic_vector(1 downto 0);
        Eth_Intn : in std_logic;

        -- DDR SDRAM interface
        DS_CLK_P : out std_logic;
        DS_CLK_N : out std_logic;
        DS_CKE : out std_logic;
        DS_A : out std_logic_vector(12 downto 0);
        DS_BA : out std_logic_vector(1 downto 0);
        DS_CAS_N : out std_logic;
        DS_RAS_N : out std_logic;
        DS_WE_N : out std_logic;
        DS_DQS : inout std_logic_vector(1 downto 0);
        DS_DM : out std_logic_vector(1 downto 0);
        DS_DQ : inout std_logic_vector(15 downto 0)
    );
end Example6;

architecture arch of Example6 is

    --------------------------------------------------------------------------
    -- Declare constants
    
    -- Test transfer length
    constant WRITE_LENGTH         : std_logic_vector(31 downto 0) := conv_std_logic_vector(500*1024*1024, 32);

    --------------------------------------------------------------------------
    -- Declare signals
    
    -- Clocks and reset
    signal RST : std_logic;
    signal RAMRST : std_logic;
    signal RAMClk : std_logic;
    signal RAMClk90 : std_logic;
    signal EthClk : std_logic;

    -- Link status variables
    signal LocalIP : std_logic_vector(31 downto 0);
    signal LinkStatus : std_logic;
    signal LinkDuplex : std_logic;
    signal LinkSpeed : std_logic_vector(2 downto 0);
    signal RemoteIPAddr : std_logic_vector(31 downto 0);
    signal RemotePort : std_logic_vector(15 downto 0);
    
    -- Connection state signals
    signal EnableConnection : std_logic;
    signal Connected : std_logic;
    signal LastConnected : std_logic;

    -- Test data signals
    signal RampData : std_logic_vector(7 downto 0);
    signal WriteCount : std_logic_vector(31 downto 0);
    signal TxEnable : std_logic;
    signal TxBusy : std_logic;
    signal TxBufferEmpty : std_logic;

    signal RxEnable : std_logic;
    signal RxData : std_logic_vector(7 downto 0);
    signal RxBusy : std_logic;

begin
    
    -- Tie unused signals
    Flash_CSOn <= '1';
    Flash_CLK <= '1';
    Flash_MOSI <= '1';
    IO0 <=  (others=>'Z');
    IO3 <= (others=>'Z');
    
    -- Reset controller
    ROCInst : ROC port map (O=>RST);

    --------------------------------------------------------------------------
    -- Main application code

    -- Connection management
    -- Open a connection whenever possible
    -- If the host closes the connection then we immediately re-open it
    -- NB: If the connection isn't closed (e.g. host application crashes)
    -- then we would need to close it from this end before we could re-open it
    process (RAMRST, EthClk)
    begin
        if (RAMRST='1') then
            EnableConnection <= '0';
            LastConnected <= '0';
        elsif (EthClk'event and EthClk='1') then
            LastConnected <= Connected;
            if (EnableConnection='0') then
                -- Rising edge will start listening on connection
                EnableConnection <= '1';
            else 
                if (LastConnected='1' and Connected='0') then
                    -- Falling edge indicates connection was closed
                    -- We need to force a rising edge on EnableConnection
                    -- to start listening again
                    EnableConnection <= '0';
                end if;
            end if;
        end if;
    end process;
    
    -- Generate test ramp data
    process (RAMRST, EthClk)
    begin
        if (RAMRST='1') then
            RampData <= (others=>'0');
            WriteCount <= (others=>'0');
        elsif (EthClk'event and EthClk='1') then
            if (Connected='0') then
                -- Reset ramp generator
                WriteCount <= (others=>'0');
                RampData <= (others=>'0');
            elsif (TxEnable='1') then
                -- Increment ramp generator
                WriteCount <= WriteCount+1;
                RampData <= RampData+1;
            end if;
        end if;
    end process;
    TxEnable <= '1' when Connected='1' and
                         WriteCount/=WRITE_LENGTH and
                         TxBusy='0' else '0';
    
    -- NB: Data from the interface appears on these Rx signals:
    -- if (RxEnable='1') then
    --     ??? <= RxData;
    -- end if;
    RxBusy <= '0';
    
    --------------------------------------------------------------------------
    -- Instantiate clocks
    -- (Assumes default 125MHz reference clock)
    ClockInst : entity work.ZestET1_Clocks
        port map (
            RST => RST,
            RefClk => CLK,
            EthClk => EthClk,
            RAMRST => RAMRST,
            RAMClk => RAMClk,
            RAMClk90 => RAMClk90    
        );

    --------------------------------------------------------------------------
    -- Ethernet interface wrapper instantiation
    EthernetInst : entity work.ZestET1_EthernetWrapper 
        generic map (
            CLOCK_RATE => 125000000
        )
        port map (
            User_CLK => EthClk,
            User_RST => RST,
            User_LocalIPAddr => LocalIP,
            User_LinkStatus => LinkStatus,
            User_LinkDuplex => LinkDuplex,
            User_LinkSpeed => LinkSpeed,

            -- User interface - only one connection used
            -- Control ports
            User_Enable0 => EnableConnection,
            User_Server0 => '1',
            User_TCP0 => '1',
            User_TxPause0 => '0',
            User_Connected0 => Connected,
            User_MTU0 => X"80",
            User_TTL0 => X"80",
            User_LocalPort0 => X"5002",
            User_WriteRemotePort0 => X"0000",
            User_WriteRemoteIPAddr0 => X"00000000",
            User_ReadRemotePort0 => RemotePort,
            User_ReadRemoteIPAddr0 => RemoteIPAddr,
            
            -- Transmit and receive ports
            User_TxData0 => RampData,
            User_TxEnable0 => TxEnable,
            User_TxBusy0 => TxBusy,
            User_TxBufferEmpty0 => TxBufferEmpty,
            User_RxData0 => RxData,
            User_RxEnable0 => RxEnable,
            User_RxBusy0 => RxBusy,
            
            -- Disable all other connections
            User_Enable1 => '0',
            User_Enable2 => '0',
            User_Enable3 => '0',
            User_Enable4 => '0',
            User_Enable5 => '0',
            User_Enable6 => '0',
            User_Enable7 => '0',
            User_Enable8 => '0',
            User_Enable9 => '0',
            User_Enable10 => '0',
            User_Enable11 => '0',
            User_Enable12 => '0',
            User_Enable13 => '0',
            User_Enable14 => '0',
            User_Enable15 => '0',
            
            -- Interface to GigExpedite
            Eth_Clk => Eth_Clk,
            Eth_CSn => Eth_CSn,
            Eth_WEn => Eth_WEn,
            Eth_A => Eth_A,
            Eth_D => Eth_D,
            Eth_BE => Eth_BE,
            Eth_Intn => Eth_Intn
        );

    --------------------------------------------------------------------------
    -- SDRAM Buffer
    -- Place holder - not used in the example but ties the pins correctly
    
    -- Instantiate SDRAM component
    SDRAMInst : entity work.ZestET1_SDRAM
        generic map (
            CLOCK_RATE => 166666666
        )
        port map (
            User_CLK => RAMClk,
            User_CLK90 => RAMClk90,
            User_RST => RAMRST,

            User_A => (others=>'0'),
            User_RE => '0',
            User_WE => '0',
            User_Owner => X"00",
            User_BE => (others=>'0'),
            User_DW => (others=>'0'),
            --User_DR => ,
            --User_DR_Valid => ,
            --User_ValidOwner => ,
            --User_Busy => ,
            --User_InitDone => ,

            DS_CLK_P => DS_CLK_P,
            DS_CLK_N => DS_CLK_N,
            DS_A => DS_A,
            DS_BA => DS_BA,
            DS_DQ => DS_DQ,
            DS_DQS => DS_DQS,
            DS_DM => DS_DM,
            DS_CAS_N => DS_CAS_N,
            DS_RAS_N => DS_RAS_N,
            DS_CKE => DS_CKE,
            DS_WE_N => DS_WE_N
        );

end arch;


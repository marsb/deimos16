--   ZestET1 Example 3
--   File name: Example3.vhd
--   Version: 1.01
--   Date: 18/1/2010
--
--   ZestET1 Example 3 - UDP test code.
--   Streams data to Ethernet using UDP.
--   Also illustrates UDP reception for 'start/stop' control
--
--   Copyright (C) 2009 Orange Tree Technologies Ltd. All rights reserved.
--   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and 
--   modify this logic core in any form such as VHDL source code or EDIF netlist in 
--   FPGA designs that target the ZestET1.
--   Orange Tree Technologies prohibits the use of this logic core in any form such 
--   as VHDL source code or EDIF netlist in FPGA designs that target any other
--   hardware unless the purchaser of the ZestET1 has purchased the appropriate 
--   licence from Orange Tree Technologies. Contact Orange Tree Technologies if you 
--   want to purchase such a licence.
--
--  *****************************************************************************************
--  **
--  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
--  **              provided to you "as is". Orange Tree Technologies and its licensors 
--  **              make and you receive no warranties or conditions, express, implied, 
--  **              statutory or otherwise, and Orange Tree Technologies specifically 
--  **              disclaims any implied warranties of merchantability, non-infringement,
--  **              or fitness for a particular purpose. Orange Tree Technologies does not
--  **              warrant that the functions contained in these designs will meet your 
--  **              requirements, or that the operation of these designs will be 
--  **              uninterrupted or error free, or that defects in the Designs will be 
--  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
--  **              make any representations regarding use or the results of the use of the 
--  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
--  **
--  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
--  **              or its licensors be liable for any loss of data, lost profits, cost or 
--  **              procurement of substitute goods or services, or for any special, 
--  **              incidental, consequential, or indirect damages arising from the use or 
--  **              operation of the designs or accompanying documentation, however caused 
--  **              and on any theory of liability. This limitation will apply even if 
--  **              Orange Tree Technologies has been advised of the possibility of such 
--  **              damage. This limitation shall apply notwithstanding the failure of the 
--  **              essential purpose of any limited remedies herein.
--  **
--  *****************************************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity Example3 is
    port (
        -- Programmable Clock
        CLK : in std_logic;
        
        -- Flash Interface
        Flash_CSOn : out std_logic;
        Flash_CLK : out std_logic;
        Flash_MOSI : out std_logic;
        Flash_MISO : in std_logic;

        -- IO connector
        IO0_In : in std_logic;
        IO0 : inout std_logic_vector(39 downto 0);
        IO3_In : in std_logic_vector(2 downto 0);
        IO3 : inout std_logic_vector(35 downto 0);

        -- Ethernet Interface
        Eth_Clk : out std_logic;
        Eth_CSn : out std_logic;
        Eth_WEn : out std_logic;
        Eth_A : out std_logic_vector(4 downto 0);
        Eth_D : inout std_logic_vector(15 downto 0);
        Eth_BE : out std_logic_vector(1 downto 0);
        Eth_Intn : in std_logic;

        -- DDR SDRAM interface
        DS_CLK_P : out std_logic;
        DS_CLK_N : out std_logic;
        DS_CKE : out std_logic;
        DS_A : out std_logic_vector(12 downto 0);
        DS_BA : out std_logic_vector(1 downto 0);
        DS_CAS_N : out std_logic;
        DS_RAS_N : out std_logic;
        DS_WE_N : out std_logic;
        DS_DQS : inout std_logic_vector(1 downto 0);
        DS_DM : out std_logic_vector(1 downto 0);
        DS_DQ : inout std_logic_vector(15 downto 0)
    );
end Example3;

architecture arch of Example3 is

    --------------------------------------------------------------------------
    -- Declare constants
    
    -- GigExpedite register addresses
    constant LOCAL_IP_ADDR_LOW          : std_logic_vector(4 downto 0) := "00000";
    constant LOCAL_IP_ADDR_HIGH         : std_logic_vector(4 downto 0) := "00001";
    constant LINK_STATUS                : std_logic_vector(4 downto 0) := "00010";
    constant CONNECTION_PAGE            : std_logic_vector(4 downto 0) := "00111";
    constant LOCAL_PORT                 : std_logic_vector(4 downto 0) := "01000";
    constant REMOTE_IP_ADDR_LOW         : std_logic_vector(4 downto 0) := "01001";
    constant REMOTE_IP_ADDR_HIGH        : std_logic_vector(4 downto 0) := "01010";
    constant REMOTE_PORT                : std_logic_vector(4 downto 0) := "01011";
    constant MTU_TTL                    : std_logic_vector(4 downto 0) := "01100";
    constant INTERRUPT_ENABLE_STATUS    : std_logic_vector(4 downto 0) := "01101";
    constant CONNECTION_STATE           : std_logic_vector(4 downto 0) := "01110";
    constant FRAME_LENGTH               : std_logic_vector(4 downto 0) := "01111";
    constant DATA_FIFO                  : std_logic_vector(4 downto 0) := "10000";
    
    -- Connection states (for CONNECTION_STATE reg)
    constant CLOSED       : std_logic_vector(15 downto 0) := X"0000";
    constant LISTEN       : std_logic_vector(15 downto 0) := X"0001";
    constant CONNECT      : std_logic_vector(15 downto 0) := X"0002";
    constant ESTABLISHED  : std_logic_vector(15 downto 0) := X"0003";
    constant CONN_TCP     : std_logic_vector(15 downto 0) := X"0010";
    constant CONN_ENABLE  : std_logic_vector(15 downto 0) := X"0020";
    
    -- Interrupt enable and status bits (for INTERRUPT_ENABLE_STATUS reg)
    constant IE_INCOMING          : std_logic_vector(15 downto 0) := X"0001";
    constant IE_OUTGOING_EMPTY    : std_logic_vector(15 downto 0) := X"0002";
    constant IE_OUTGOING_NOT_FULL : std_logic_vector(15 downto 0) := X"0004";
    constant IE_STATE             : std_logic_vector(15 downto 0) := X"0008";

    -- Length of burst from GigExpedite
    constant BURST_WORDS          : std_logic_vector(15 downto 0) := X"0060";

    -- Send packet length
    constant PACKET_LENGTH : std_logic_vector(15 downto 0) := conv_std_logic_vector(1400, 16);
    constant PACKET_WORDS : std_logic_vector(15 downto 0) := conv_std_logic_vector(1400/2-1, 16);
    
    --------------------------------------------------------------------------
    -- Declare signals

    -- Clocks and reset
    signal RST : std_logic;
    signal RAMRST : std_logic;
    signal RAMClk : std_logic;
    signal RAMClk90 : std_logic;
    signal EthClk : std_logic;

    -- Ethernet interface state machine
    signal UserWE : std_logic;
    signal UserRE : std_logic;
    signal UserAddr : std_logic_vector(4 downto 0);
    signal UserBE : std_logic_vector(1 downto 0);
    signal UserWriteData : std_logic_vector(15 downto 0);
    signal UserReadData : std_logic_vector(15 downto 0);
    signal UserReadDataValid : std_logic;
    signal UserInterrupt : std_logic;

    signal Delay : std_logic_vector(23 downto 0);
    signal Connection : std_logic;
    signal UserFPGASubState : std_logic_vector(15 downto 0);
    signal UserValidCount : std_logic_vector(15 downto 0);
    type STATE_TYPE is array(0 to 1) of std_logic_vector(5 downto 0);
    signal ConnectionState : STATE_TYPE;
    type INT_STATUS_TYPE is array(0 to 1) of std_logic_vector(15 downto 0);
    signal InterruptStatus : INT_STATUS_TYPE;
    signal FrameLength : std_logic_vector(15 downto 0);
    signal BurstWords : std_logic_vector(15 downto 0);
    signal EnableSend : std_logic;
    signal CLKEnableSend : std_logic;
    signal RAMEnableSend : std_logic;
    attribute tig : string; 
    attribute tig of EnableSend : signal is "TRUE"; 
    signal RemoteIP : std_logic_vector(31 downto 0);
    signal RemotePort : std_logic_vector(15 downto 0);
    signal Clean : std_logic;
    
    type FSM_STATE_TYPE is (
        USER_FPGA_DELAY_INIT,
        USER_FPGA_CLEAN,
        USER_FPGA_CLEAN_CHECK,
        USER_FPGA_CLEAR_INTS,
        USER_FPGA_INIT,
        USER_FPGA_IDLE,
        USER_FPGA_CHECK_STATE,
        USER_FPGA_READ_LENGTH,
        USER_FPGA_READ_HEADER,
        USER_FPGA_READ_DATA,
        USER_FPGA_OPEN_DATA,
        USER_FPGA_CLOSE_DATA,
        USER_FPGA_CHECK_SPACE,
        USER_FPGA_WRITE_LENGTH,
        USER_FPGA_WRITE_DATA,
        USER_FPGA_NEXT_CONNECTION
    );
    signal UserFPGAState : FSM_STATE_TYPE;

    -- SDRAM FIFO declarations
    signal SDRAMWriteCount : std_logic_vector(23 downto 0);
    signal SDRAMReadCount : std_logic_vector(23 downto 0);
    signal NextSDRAMWriteCount : std_logic_vector(23 downto 0);
    signal NextSDRAMReadCount : std_logic_vector(23 downto 0);
    signal SDRAMWordCount : std_logic_vector(23 downto 0);
    signal ArbCounter : std_logic_vector(6 downto 0);
    signal SDRAMEmpty : std_logic;
    signal SDRAMFull : std_logic;
    signal SDRAMAddr : std_logic_vector(23 downto 0);
    signal SDRAMRE : std_logic;
    signal SDRAMWE : std_logic;
    signal SDRAMWriteData : std_logic_vector(31 downto 0);
    signal SDRAMReadData : std_logic_vector(31 downto 0);
    signal SDRAMReadDataValid : std_logic;
    signal SDRAMBusy : std_logic;
    signal SDRAMWriteOK : std_logic;
    
    signal WriteFIFODataIn : std_logic_vector(31 downto 0);
    signal WriteFIFODataOut : std_logic_vector(31 downto 0);
    signal WriteFIFOWE : std_logic;
    signal WriteFIFORE : std_logic;
    signal WriteFIFOEmpty : std_logic;
    signal WriteFIFOFull : std_logic;
    
    signal ReadFIFODataIn : std_logic_vector(31 downto 0);
    signal ReadFIFODataOut : std_logic_vector(15 downto 0);
    signal ReadFIFOWE : std_logic;
    signal ReadFIFORE : std_logic;
    signal ReadFIFOEmpty : std_logic;
    signal ReadFIFOFull : std_logic;

begin
    
    -- Tie unused signals
    Flash_CSOn <= '1';
    Flash_CLK <= '1';
    Flash_MOSI <= '1';
    IO0 <= (others=>'Z');
    IO3 <= (others=>'Z');
    
    -- Reset controller
    ROCInst : ROC port map (O=>RST);
    
    --------------------------------------------------------------------------
    -- Instantiate clocks
    -- (Assumes default 125MHz reference clock)
    ClockInst : entity work.ZestET1_Clocks
        port map (
            RST => RST,
            RefClk => CLK,
            EthClk => EthClk,
            RAMRST => RAMRST,
            RAMClk => RAMClk,
            RAMClk90 => RAMClk90    
        );

    --------------------------------------------------------------------------
    -- Ethernet interface test code
    EthernetInst : entity work.ZestET1_Ethernet
        generic map (
            CLOCK_RATE => 125000000
        )
        port map (
            User_CLK => EthClk,
            User_RST => RST,

            -- User interface
            User_WE => UserWE,
            User_RE => UserRE,
            User_Addr => UserAddr,
            User_Owner => X"00",
            User_WriteData => UserWriteData,
            User_BE => UserBE,
            User_ReadData => UserReadData,
            User_ReadDataValid => UserReadDataValid,
            --User_ValidOwner => ,
            User_Interrupt => UserInterrupt,
            
            -- Interface to GigExpedite
            Eth_Clk => Eth_Clk,
            Eth_CSn => Eth_CSn,
            Eth_WEn => Eth_WEn,
            Eth_A => Eth_A,
            Eth_D => Eth_D,
            Eth_BE => Eth_BE,
            Eth_Intn => Eth_Intn
        );

    -- State machine to read/write Ethernet
    -- Note: use RAMRST to keep the state machine in reset until 
    -- the DCMs are locked
    process (RAMRST, EthClk) begin
        if (RAMRST='1') then
            UserFPGAState <= USER_FPGA_DELAY_INIT;
            Delay <= (others=>'0');
            UserFPGASubState <= (others=>'0');
            UserValidCount <= (others=>'0');
            FrameLength <= (others=>'0');
            BurstWords <= (others=>'0');
            EnableSend <= '0';
            RemoteIP <= (others=>'0');
            RemotePort <= (others=>'0');
            Connection <= '0';
            InterruptStatus(0) <= (others=>'0');
            InterruptStatus(1) <= (others=>'0');
            ConnectionState(0) <= (others=>'0');
            ConnectionState(1) <= (others=>'0');
            Clean <= '1';
        elsif (EthClk'event and EthClk='1') then
            -- Counter of completed register reads
            if (UserReadDataValid='1') then
                UserValidCount <= UserValidCount + 1;
            end if;
            
            case UserFPGAState is
                -- Wait here to ensure the GigExpedite has locked to
                -- our clock
                when USER_FPGA_DELAY_INIT =>
                    Delay <= Delay + 1;
                    if (Delay=X"ffffff") then
                        UserFPGAState <= USER_FPGA_CLEAN;
                    end if;
                
                -- Reset all connections in case the previous
                -- application closed unexpectedly and left some open
                -- Write 0 to each connection state then
                -- read the status for each connection in turn
                -- Loop round until all statuses are zero.
                when USER_FPGA_CLEAN =>
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserFPGASubState(7 downto 0)=X"1f") then
                        UserFPGASubState <= (others=>'0');
                        UserFPGAState <= USER_FPGA_CLEAN_CHECK;
                        Clean <= '1';
                    end if;
                when USER_FPGA_CLEAN_CHECK =>
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserReadDataValid='1' and UserReadData/=X"0000") then
                        Clean <= '0';
                    end if;
                    if (UserFPGASubState(8 downto 0)="111111111") then
                        if (Clean='1') then
                            UserFPGASubState <= (others=>'0');
                            UserValidCount <= (others=>'0');
                            UserFPGAState <= USER_FPGA_CLEAR_INTS;
                        else
                            Clean <= '1';
                            UserFPGASubState <= (others=>'0');
                        end if;
                    end if;
                when USER_FPGA_CLEAR_INTS =>
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserFPGASubState(8 downto 0)="111111111") then
                        UserFPGASubState <= (others=>'0');
                        UserValidCount <= (others=>'0');
                        UserFPGAState <= USER_FPGA_INIT;
                    end if;
                
                -- Initialise register set
                -- Set connection 0 as the control connection listening
                -- for messages on port 5002.
                -- Set connection 1 as the data sending connection.
                -- Connection 1 is opened when a start message is received
                -- and closed when a stop message is received.
                when USER_FPGA_INIT =>
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserFPGASubState=X"0007") then
                        UserFPGAState <= USER_FPGA_IDLE;
                    end if;
                
                -- Wait for interrupt from GigExpedite device
                when USER_FPGA_IDLE =>
                    UserFPGASubState <= (others=>'0');
                    UserValidCount <= (others=>'0');
                    if (UserInterrupt='1') then
                        -- Interrupt has been received
                        UserFPGAState <= USER_FPGA_CHECK_STATE;
                    end if;

                -- Check if the connection state has changed
                when USER_FPGA_CHECK_STATE =>
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserReadDataValid='1' and UserValidCount(3 downto 0)=X"0") then
                        -- Store interrupt status
                        InterruptStatus(conv_integer(Connection)) <= UserReadData;
                    end if;
                    if (UserReadDataValid='1' and UserValidCount(3 downto 0)=X"1") then
                        -- Store the new connection state
                        ConnectionState(conv_integer(Connection)) <= UserReadData(5 downto 0);

                        -- Next, check if there is incoming data available
                        UserFPGASubState <= (others=>'0');
                        UserValidCount <= (others=>'0');
                        if (InterruptStatus(conv_integer(Connection))(0)='0') then
                            -- Loopback FIFO is nearly full or there is no data available
                            -- Next, check if there is outgoing data to send
                            UserFPGAState <= USER_FPGA_CHECK_SPACE;
                        else
                            -- Read frame length or next burst
                            BurstWords <= BURST_WORDS;
                            if (FrameLength=X"0000") then
                                UserFPGAState <= USER_FPGA_READ_LENGTH;
                            else
                                UserFPGAState <= USER_FPGA_READ_DATA;
                            end if;
                        end if;
                    end if;

                -- Check if there is incoming data
                when USER_FPGA_READ_LENGTH =>
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserReadDataValid='1' and UserValidCount(3 downto 0)=X"0") then
                        -- Read frame length from GigExpedite
                        -- Round the number of bytes up to the total number
                        -- of 16 bit reads we will need to do
                        UserFPGASubState <= (others=>'0');
                        FrameLength <= ('0'&UserReadData(15 downto 1)) + UserReadData(0);
                        UserValidCount <= (others=>'0');
                        if (UserReadData=conv_std_logic_vector(0, 16)) then
                            -- Length was zero - skip the read
                            -- Next, check if there is outgoing data to send
                            UserFPGAState <= USER_FPGA_CHECK_SPACE;
                        else
                            -- Got a valid frame length - read the data
                            UserFPGAState <= USER_FPGA_READ_HEADER;
                        end if;
                    end if;
                
                -- Read UDP header from GigExpedite device
                -- The UDP header contains the remote IP address and port
                when USER_FPGA_READ_HEADER => 
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserReadDataValid='1') then
                        -- Extract remote IP address and port from UDP header
                        if (Connection='0') then
                            if (UserValidCount(3 downto 0)=X"0") then
                                RemoteIP(15 downto 0) <= UserReadData;
                            elsif (UserValidCount(3 downto 0)=X"1") then
                                RemoteIP(31 downto 16) <= UserReadData;
                            elsif (UserValidCount(3 downto 0)=X"2") then
                                RemotePort <= UserReadData;
                            end if;
                        end if;
                        if (UserValidCount(3 downto 0)=X"2") then
                            -- End of header
                            UserFPGASubState <= (others=>'0');
                            UserValidCount <= (others=>'0');
                            BurstWords <= BURST_WORDS-3;
                            UserFPGAState <= USER_FPGA_READ_DATA;
                        end if;
                    end if;
                
                -- Read data from GigExpedite device
                when USER_FPGA_READ_DATA =>
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (Connection='0' and UserReadDataValid='1' and
                        UserValidCount=X"0000") then
                        -- Get start/stop bit from first byte of frame
                        EnableSend <= UserReadData(8);
                    end if;
                    if (UserValidCount>=FrameLength) then
                        -- End of frame reached
                        -- If requested, close or open the data streaming
                        -- connection
                        FrameLength <= (others=>'0');
                        UserFPGASubState <= (others=>'0');
                        UserValidCount <= (others=>'0');
                        if (Connection='0' and EnableSend='0') then
                            UserFPGAState <= USER_FPGA_CLOSE_DATA;
                        elsif (Connection='0' and EnableSend='1') then
                            UserFPGAState <= USER_FPGA_OPEN_DATA;
                        else
                            UserFPGAState <= USER_FPGA_CHECK_SPACE;
                        end if;
                    elsif (UserValidCount>=BurstWords) then
                        FrameLength <= FrameLength - BurstWords;
                        UserFPGASubState <= (others=>'0');
                        UserValidCount <= (others=>'0');
                        UserFPGAState <= USER_FPGA_CHECK_STATE;
                    end if;
                
                -- Open a connection for streaming data
                when USER_FPGA_OPEN_DATA =>
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserFPGASubState(7 downto 0)=X"0F") then
                        UserFPGASubState <= (others=>'0');
                        UserValidCount <= (others=>'0');
                        UserFPGAState <= USER_FPGA_CHECK_SPACE;
                    end if;
                
                -- Close a connection and wait for GigExpedite to acknowledge
                when USER_FPGA_CLOSE_DATA =>
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserReadDataValid='1' and
                        UserReadData(3 downto 0)=CLOSED) then
                        UserFPGASubState <= (others=>'0');
                        UserValidCount <= (others=>'0');
                        UserFPGAState <= USER_FPGA_NEXT_CONNECTION;
                    end if;

                -- Check if there is space in the outgoing GigExpedite buffer
                -- and we have data to send back to the host
                when USER_FPGA_CHECK_SPACE =>
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (Connection='0' or EnableSend='0' or
                        ReadFIFOEmpty='1') then
                        -- No data requested or no data to send
                        UserFPGASubState <= (others=>'0');
                        UserValidCount <= (others=>'0');
                        UserFPGAState <= USER_FPGA_NEXT_CONNECTION;
                    elsif (UserReadDataValid='1' and
                           UserValidCount(3 downto 0)=X"0") then
                        if (UserReadData(3 downto 0)/=ESTABLISHED) then
                            -- Connection not established - ignore request
                            UserFPGASubState <= (others=>'0');
                            UserValidCount <= (others=>'0');
                            UserFPGAState <= USER_FPGA_NEXT_CONNECTION;
                        elsif (InterruptStatus(conv_integer(Connection))(2)='1') then
                            -- Space available - write data to GigExpedite
                            UserFPGASubState <= (others=>'0');
                            UserValidCount <= (others=>'0');
                            UserFPGAState <= USER_FPGA_WRITE_LENGTH;
                        else
                            -- No space available in GigExpedite FIFO
                            UserFPGASubState <= (others=>'0');
                            UserValidCount <= (others=>'0');
                            UserFPGAState <= USER_FPGA_NEXT_CONNECTION;
                        end if;
                    end if;
                
                -- Write datagram length to outgoing GigExpedite FIFO
                when USER_FPGA_WRITE_LENGTH =>
                    UserFPGASubState <= (others=>'0');
                    UserValidCount <= (others=>'0');
                    UserFPGAState <= USER_FPGA_WRITE_DATA;
                
                -- Write data to outgoing GigExpedite FIFO
                when USER_FPGA_WRITE_DATA =>
                    if (ReadFIFORE='1') then
                        UserFPGASubState <= UserFPGASubState + 1;
                        if (UserFPGASubState=PACKET_WORDS) then
                            UserFPGASubState <= (others=>'0');
                            UserValidCount <= (others=>'0');
                            UserFPGAState <= USER_FPGA_NEXT_CONNECTION;
                        end if;
                    end if;
                
                -- Check the next connection
                when USER_FPGA_NEXT_CONNECTION =>
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserFPGASubState(7 downto 0)=X"00") then
                        Connection <= not Connection;
                    elsif (UserFPGASubState(7 downto 0)=X"10") then
                        UserFPGASubState <= (others=>'0');
                        UserValidCount <= (others=>'0');
                        if (Connection='1') then
                            UserFPGAState <= USER_FPGA_IDLE;
                        else
                            UserFPGAState <= USER_FPGA_CHECK_STATE;
                        end if;
                    end if;
                
                when others => null;
                
            end case;
        end if;
    end process;
    
    -- Map states to Ethernet register accesses
    process (UserFPGAState, UserFPGASubState, UserReadDataValid,
             UserValidCount, UserReadData, Connection, FrameLength,
             RemotePort, RemoteIP, ReadFIFOEmpty, EnableSend,
             ReadFIFORE, ReadFIFODataOut, InterruptStatus)
    begin
        case UserFPGAState is
            when USER_FPGA_CLEAN =>
                -- Reset all connections
                UserRE <= '0';
                UserWE <= '1';
                UserBE <= "11";
                if (UserFPGASubState(0)='0') then 
                    UserAddr <= CONNECTION_PAGE;
                    UserWriteData <= X"000" & UserFPGASubState(4 downto 1);
                else
                    UserAddr <= CONNECTION_STATE;
                    UserWriteData <= (others=>'0');
                end if;
            
            when USER_FPGA_CLEAN_CHECK =>
                -- Check all connections have been reset
                if (UserFPGASubState(4 downto 0)="10000") then
                    UserRE <= '1';
                else
                    UserRE <= '0';
                end if;
                if (UserFPGASubState(4 downto 0)="00000") then
                    UserWE <= '1';
                    UserAddr <= CONNECTION_PAGE;
                else
                    UserWE <= '0';
                    UserAddr <= CONNECTION_STATE;
                end if;
                UserBE <= "11";
                UserWriteData <= X"000" & UserFPGASubState(8 downto 5);

            when USER_FPGA_CLEAR_INTS =>
                -- Clear state change interrupts
                UserBE <= "11";
                if (UserFPGASubState(4 downto 0)="00000") then
                    UserRE <= '0';
                    UserWE <= '1';
                    UserAddr <= CONNECTION_PAGE;
                elsif (UserFPGASubState(4 downto 0)="10000") then
                    UserRE <= '1';
                    UserWE <= '0';
                    UserAddr <= INTERRUPT_ENABLE_STATUS;
                else
                    UserRE <= '0';
                    UserWE <= '0';
                end if;
                UserWriteData <= X"000" & UserFPGASubState(8 downto 5);
            
            when USER_FPGA_INIT =>
                -- Set up registers to make one connection listen on
                -- port 0x5002 for UDP connections
                UserRE <= '0';
                UserWE <= '1';
                UserBE <= "11";
                case UserFPGASubState(4 downto 0) is
                    when "00000" =>
                        UserAddr <= CONNECTION_PAGE;
                        UserWriteData <= (others=>'0');
                    when "00001" =>
                        UserAddr <= LOCAL_PORT;
                        UserWriteData <= X"5002";
                    when "00010" =>
                        UserAddr <= REMOTE_PORT;
                        UserWriteData <= X"0000";
                    when "00011" =>
                        UserAddr <= REMOTE_IP_ADDR_LOW;
                        UserWriteData <= X"0000";
                    when "00100" =>
                        UserAddr <= REMOTE_IP_ADDR_HIGH;
                        UserWriteData <= X"0000";
                    when "00101" =>
                        -- This can be changed if your network supports
                        -- jumbo frames
                        UserAddr <= MTU_TTL;
                        UserWriteData <= X"2d80";
                    when "00110" =>
                        UserAddr <= INTERRUPT_ENABLE_STATUS;
                        UserWriteData <= (IE_OUTGOING_NOT_FULL or IE_INCOMING or IE_STATE);
                    when others =>
                        UserAddr <= CONNECTION_STATE;
                        UserWriteData <= (CONN_ENABLE or ESTABLISHED);
                end case;
                
            when USER_FPGA_CHECK_STATE =>
                -- Read connection state then update accordingly
                if (UserFPGASubState(3 downto 1)="000") then
                    UserRE <= '1';
                else
                    UserRE <= '0';
                end if;
                UserWE <= '0';
                UserBE <= "11";
                if (UserFPGASubState(3 downto 0)=X"0") then
                    UserAddr <= INTERRUPT_ENABLE_STATUS;
                else
                    UserAddr <= CONNECTION_STATE;
                end if;
                UserWriteData <= (others=>'0');
            
            when USER_FPGA_READ_LENGTH =>
                -- Read from interrupt status and then frame length register
                if (UserFPGASubState(3 downto 0)=X"0") then
                    UserRE <= '1';
                else
                    UserRE <= '0';
                end if;
                UserWE <= '0';
                UserBE <= "11";
                UserAddr <= FRAME_LENGTH;
                UserWriteData <= (others=>'0');
                
            when USER_FPGA_READ_HEADER =>
                -- Read header from connection FIFO
                if (UserFPGASubState(3 downto 0)<X"3") then
                    UserRE <= '1';
                else
                    UserRE <= '0';
                end if;
                UserWE <= '0';
                UserBE <= "11";
                UserAddr <= DATA_FIFO+Connection;
                UserWriteData <= (others=>'0');
            
            when USER_FPGA_READ_DATA =>
                -- Read from connection FIFO
                if (UserFPGASubState<FrameLength and
                    UserFPGASubState<BurstWords) then
                    UserRE <= '1';
                else
                    UserRE <= '0';
                end if;
                UserWE <= '0';
                UserBE <= "11";
                UserAddr <= DATA_FIFO+Connection;
                UserWriteData <= (others=>'0');

            when USER_FPGA_OPEN_DATA =>
                -- Set up registers to make one connection send data
                -- to requested IP address and port
                UserRE <= '0';
                if (UserFPGASubState(4 downto 0)<="01000") then
                    UserWE <= '1';
                else
                    UserWE <= '0';
                end if;
                UserBE <= "11";
                case UserFPGASubState(4 downto 0) is
                    when "00000" =>
                        UserAddr <= CONNECTION_PAGE;
                        UserWriteData <= X"0001";
                    when "00001" =>
                        UserAddr <= LOCAL_PORT;
                        UserWriteData <= X"5003";
                    when "00010" =>
                        UserAddr <= REMOTE_PORT;
                        UserWriteData <= RemotePort;
                    when "00011" =>
                        UserAddr <= REMOTE_IP_ADDR_LOW;
                        UserWriteData <= RemoteIP(15 downto 0);
                    when "00100" =>
                        UserAddr <= REMOTE_IP_ADDR_HIGH;
                        UserWriteData <= RemoteIP(31 downto 16);
                    when "00101" =>
                        -- This can be changed if your network supports
                        -- jumbo frames
                        UserAddr <= MTU_TTL;
                        UserWriteData <= X"2d80";
                    when "00110" =>
                        UserAddr <= INTERRUPT_ENABLE_STATUS;
                        UserWriteData <= (IE_OUTGOING_NOT_FULL or IE_INCOMING or IE_STATE);
                    when "00111" =>
                        UserAddr <= CONNECTION_STATE;
                        UserWriteData <= (CONN_ENABLE or ESTABLISHED);
                    when others =>
                        UserAddr <= CONNECTION_PAGE;
                        UserWriteData <= conv_std_logic_vector(0, 15) & Connection;
                end case;

            when USER_FPGA_CLOSE_DATA =>
                -- Close connection and wait for acknowledgement
                -- to requested IP address and port
                if (UserFPGASubState(7 downto 0)>X"0f") then
                    UserRE <= '1';
                else
                    UserRE <= '0';
                end if;
                if (UserFPGASubState(7 downto 0)<X"02") then
                    UserWE <= '1';
                else
                    UserWE <= '0';
                end if;
                UserBE <= "11";
                if (UserFPGASubState(7 downto 0)=X"00") then
                    UserAddr <= CONNECTION_PAGE;
                    UserWriteData <= X"0001";
                else
                    UserAddr <= CONNECTION_STATE;
                    UserWriteData <= CLOSED;
                end if;

            when USER_FPGA_CHECK_SPACE =>
                -- Read from connection status and then interrupt status
                if (UserFPGASubState(3 downto 0)=X"0" and Connection/='0' and 
                    ReadFIFOEmpty='0' and EnableSend='1') then
                    UserRE <= '1';
                else
                    UserRE <= '0';
                end if;
                UserWE <= '0';
                UserBE <= "11";
                UserAddr <= CONNECTION_STATE;
                UserWriteData <= (others=>'0');
                
            when USER_FPGA_WRITE_LENGTH =>
                -- Write UDP datagram length to GigExpedite
                UserRE <= '0';
                UserWE <= '1';
                UserBE <= "11";
                UserAddr <= FRAME_LENGTH;
                UserWriteData <= PACKET_LENGTH;
            
            when USER_FPGA_WRITE_DATA =>
                -- Write test data to connection FIFO
                UserRE <= '0';
                UserWE <= ReadFIFORE;
                UserBE <= "11";
                UserAddr <= DATA_FIFO or ("0000"&Connection);
                UserWriteData <= ReadFIFODataOut;

            when USER_FPGA_NEXT_CONNECTION =>
                -- Change connection
                UserRE <= '0';
                if (UserFPGASubState(7 downto 0)=X"01") then
                    UserWE <= '1';
                else
                    UserWE <= '0';
                end if;
                UserBE <= "11";
                UserAddr <= CONNECTION_PAGE;
                UserWriteData <= conv_std_logic_vector(0,15) & Connection;
                
            when others =>
                -- Don't do anything
                UserRE <= '0';
                UserWE <= '0';
                UserAddr <= (others=>'0');
                UserBE <= "11";
                UserWriteData <= (others=>'0');
        end case;
    end process;

    --------------------------------------------------------------------------
    -- SDRAM FIFO
    -- Use the SDRAM as a large FIFO for buffering data
    -- Input data is fed into a block RAM FIFO to cross the clock domain
    -- to the SDRAM clock.  The SDRAM is used as a 32Mbyte cicular buffer.
    -- Data is then fed through another FIFO to cross to the Ethernet clock
    -- domain before being sent to the GigExpedite device.
    WriteFIFOInst : entity work.WriteFIFO
        port map (
            rst => RAMRST,
            wr_clk => CLK,
            rd_clk => RAMClk,
            wr_en => WriteFIFOWE,
            din => WriteFIFODataIn,
            rd_en => WriteFIFORE,
            dout => WriteFIFODataOut,
            empty => WriteFIFOEmpty,
            full => WriteFIFOFull
        );
    ReadFIFOInst : entity work.ReadFIFO
        port map (
            rst => RAMRST,
            wr_clk => RAMClk,
            rd_clk => EthClk,
            wr_en => ReadFIFOWE,
            din => ReadFIFODataIn,
            rd_en => ReadFIFORE,
            dout => ReadFIFODataOut,
            empty => ReadFIFOEmpty,
            prog_full => ReadFIFOFull -- Use almost full to handle SDRAM latency
        );
    
    -- Connections for write FIFO
    -- We feed a ramp as test data but this could be connected to the IO pins
    WriteFIFOWE <= CLKEnableSend and not WriteFIFOFull;
    process (RAMRST, CLK) begin
        if (RAMRST='1') then
            WriteFIFODataIn <= (others=>'0');
            CLKEnableSend <= '0';
        elsif (CLK'event and CLK='1') then
            CLKEnableSend <= EnableSend;
            if (WriteFIFOWE='1') then
                WriteFIFODataIn <= WriteFIFODataIn + 1;
            end if;
        end if;
    end process;
    WriteFIFORE <= SDRAMWriteOK and not SDRAMBusy and not WriteFIFOEmpty;
    
    -- Connections for read FIFO
    ReadFIFOWE <= SDRAMReadDataValid;
    ReadFIFODataIn <= SDRAMReadData;
    ReadFIFORE <= '1' when Connection='1' and 
                           UserFPGAState=USER_FPGA_WRITE_DATA and
                           ReadFIFOEmpty='0' else '0';
    
    -- SDRAM Buffer
    process (RAMRST, RAMClk) begin
        if (RAMRST='1') then
            ArbCounter <= (others=>'0');
            SDRAMWriteCount <= (others=>'0');
            SDRAMReadCount <= (others=>'0');
            SDRAMWordCount <= (others=>'0');
            SDRAMEmpty <= '1';
            SDRAMFull <= '0';
            NextSDRAMWriteCount <= conv_std_logic_vector(1, 24);
            NextSDRAMReadCount <= conv_std_logic_vector(1, 24);
            SDRAMWriteOK <= '0';
            RAMEnableSend <= '0';
        elsif (RAMClk'event and RAMClk='1') then
            RAMEnableSend <= EnableSend;
            -- Arbitrate between SDRAM reads and writes
            if (RAMEnableSend='1') then
                ArbCounter <= ArbCounter + 1;
            end if;
            if (ArbCounter(5 downto 0)="111111") then
                if (ArbCounter(6)='0') then
                    SDRAMAddr <= SDRAMWriteCount;
                else
                    SDRAMAddr <= SDRAMReadCount;
                end if;
            elsif (SDRAMWE='1') then
                SDRAMAddr <= NextSDRAMWriteCount;
            elsif (SDRAMRE='1') then
                SDRAMAddr <= NextSDRAMReadCount;
            end if;
            if (ArbCounter(5 downto 0)="111111") then
                SDRAMWriteOK <= not ArbCounter(6) and not SDRAMFull;
            else
                SDRAMWriteOK <= ArbCounter(6) and not SDRAMFull;
            end if;
            
            -- Update read and write counters
            if (SDRAMWE='1') then
                SDRAMWriteCount <= NextSDRAMWriteCount;
                NextSDRAMWriteCount <= NextSDRAMWriteCount+1;
            end if;
            if (SDRAMRE='1') then
                SDRAMReadCount <= NextSDRAMReadCount;
                NextSDRAMReadCount <= NextSDRAMReadCount+1;
            end if;
            
            -- Update SDRAM full and empty flags
            if (SDRAMWE='1' and SDRAMRE='0') then
                SDRAMEmpty <= '0';
            elsif (SDRAMWE='0' and SDRAMRE='1') then
                if (NextSDRAMReadCount=SDRAMWriteCount) then
                    SDRAMEmpty <= '1';
                else
                    SDRAMEmpty <= '0';
                end if;
            end if;
            SDRAMWordCount <= SDRAMWriteCount-SDRAMReadCount;
            if (SDRAMWordCount>=X"ffff00") then
                SDRAMFull <= '1';
            else
                SDRAMFull <= '0';
            end if;
        end if;
    end process;
    
    SDRAMWE <= WriteFIFORE;
    SDRAMWriteData <= WriteFIFODataOut;
    SDRAMRE <= '1' when ArbCounter(6)='0' and SDRAMEmpty='0' and
                        ReadFIFOFull='0' and SDRAMBusy='0' else '0';
    
    --------------------------------------------------------------------------
    -- Instantiate SDRAM component
    SDRAMInst : entity work.ZestET1_SDRAM
        generic map (
            CLOCK_RATE => 166666666
        )
        port map (
            User_CLK => RAMClk,
            User_CLK90 => RAMClk90,
            User_RST => RAMRST,

            User_A => SDRAMAddr,
            User_RE => SDRAMRE,
            User_WE => SDRAMWE,
            User_Owner => X"00",
            User_BE => "1111",
            User_DW => SDRAMWriteData,
            User_DR => SDRAMReadData,
            User_DR_Valid => SDRAMReadDataValid,
            User_ValidOwner => open,
            User_Busy => SDRAMBusy,
            User_InitDone => open,

            DS_CLK_P => DS_CLK_P,
            DS_CLK_N => DS_CLK_N,
            DS_A => DS_A,
            DS_BA => DS_BA,
            DS_DQ => DS_DQ,
            DS_DQS => DS_DQS,
            DS_DM => DS_DM,
            DS_CAS_N => DS_CAS_N,
            DS_RAS_N => DS_RAS_N,
            DS_CKE => DS_CKE,
            DS_WE_N => DS_WE_N
        );    
        
end arch;



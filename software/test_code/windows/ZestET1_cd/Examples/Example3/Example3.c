//////////////////////////////////////////////////////////////////////
//
// File:      Example3.c
//
// Purpose:
//    ZestET1 Example Programs
//    Ethernet UDP test
//  
// Copyright (c) 2009 Orange Tree Technologies.
// May not be reproduced without permission.
//
//////////////////////////////////////////////////////////////////////

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "ZestET1.h"

//
// Specify length of test
//
#define TRANSFER_LENGTH 1400
#define NUM_TRANSFERS 163840

//
// Define this to check the data coming back from the ZestET1
//
#define CHECK_DATA

//
// Endian swap for data checking
// 
#define REVERSE(x) ((((x)&0xff000000)>>24) | (((x)&0xff0000)>>8) | \
                    (((x)&0xff00)<<8) | (((x)&0xff)<<24))
//
// Error handler function
//
void ErrorHandler(const char *Function, 
                  ZESTET1_CARD_INFO *CardInfo,
                  ZESTET1_STATUS Status,
                  const char *Msg)
{
    printf("**** Example3 - Function %s returned an error\n        \"%s\"\n\n", Function, Msg);
    exit(1);
}

//
// Main program
//
int main(int argc, char **argv)
{
    unsigned long Count;
    unsigned long NumCards;
    ZESTET1_CARD_INFO *CardInfo;
    unsigned __int64 Freq;
    unsigned __int64 Start;
    unsigned __int64 End;
    unsigned long *Buffer;
    ZESTET1_CONNECTION Connection;
    unsigned char Val;
    unsigned long i;
    unsigned long Check = 0;
  
    //
    // Install an error handler and initialise library
    //
    ZestET1RegisterErrorHandler(ErrorHandler);
    ZestET1Init();

    //
    // Request information about the system
    // Wait for 2 seconds (per interface) for boards to respond to query
    //
    printf("Searching for ZestET1 cards...\n");
    ZestET1CountCards(&NumCards, &CardInfo, 2000);
    printf("%ld available cards in the system\n\n\n", NumCards);
    if (NumCards==0)
    {
        printf("No cards found\n");
        exit(1);
    }

    for (Count=0; Count<NumCards; Count++)
    {
        printf("%ld : SerialNum = %ld, IPAddress = %d.%d.%d.%d\n",
            Count, CardInfo[Count].SerialNumber,
            CardInfo[Count].IPAddr[0], CardInfo[Count].IPAddr[1],
            CardInfo[Count].IPAddr[2], CardInfo[Count].IPAddr[3]);
    }

    //
    // Configure the FPGA directly from a file
    // Wait for user FPGA application to start listening on its connection
    // Open connection to user FPGA
    //
    ZestET1ConfigureFromFile(&CardInfo[0], "Example3.bit");
    Sleep(2000);
    ZestET1OpenConnection(&CardInfo[0], ZESTET1_TYPE_UDP, 0x5002, 0, &Connection);
    
    Buffer = malloc(TRANSFER_LENGTH);
    memset(Buffer, 0, TRANSFER_LENGTH);

    QueryPerformanceFrequency((LARGE_INTEGER *)&Freq);
    QueryPerformanceCounter((LARGE_INTEGER *)&Start);

    //
    // Start transfers from board
    //
    Val = 1;
    ZestET1WriteData(Connection, &Val, 1, NULL, 5000);

    //
    // Read data from the card
    //
    for (Count=0; Count<NUM_TRANSFERS; Count++)
    {
        unsigned long Read;

        ZestET1ReadData(Connection, Buffer, TRANSFER_LENGTH, &Read, 5000);

        if (Read!=TRANSFER_LENGTH)
        {
            printf("Tried to read %d bytes, read %d\n",TRANSFER_LENGTH, Read); 
        }
        
#ifdef CHECK_DATA
        // Note that data can be dropped because the transfer mechanism is UDP
        // Assume data is not dropped in a burst (i.e. only whole frames
        // from the ET1 are dropped)
        for (i=0; i<TRANSFER_LENGTH/4; i++)
        {
            if (REVERSE(Buffer[i])!=Check)
            {
                if (i!=0)
                {
                    // Fault in middle of frame
                    printf("Transfer: %d, Offset: %d, Received: 0x%08lx \n", 
                           Count, i, REVERSE(Buffer[i]));
                }
                Check = REVERSE(Buffer[i]);
            }
            Check++;
        }
#endif
    }
    QueryPerformanceCounter((LARGE_INTEGER *)&End);
    printf("Overall transfer rate from card = %.2fMBytes/sec\n", 
        ((double)NUM_TRANSFERS*(double)TRANSFER_LENGTH*(double)Freq) /
            (1024.*1024.*(double)(End-Start)));
            
    //
    // Stop transfers from board
    //
    Val = 0;
    ZestET1WriteData(Connection, &Val, 1, NULL, 5000);

    //
    // Free the card information structure
    //
    ZestET1CloseConnection(Connection);
    ZestET1FreeCards(CardInfo);

    //
    // Close library
    //
    ZestET1Close();

    return 0;
}

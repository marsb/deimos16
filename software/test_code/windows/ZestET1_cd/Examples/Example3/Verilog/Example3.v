/*
   ZestET1 Example 3
   File name: Example3.v
   Version: 1.01
   Date: 18/1/2010

   ZestET1 Example 3 - UDP test code.
   Streams data to Ethernet using UDP.
   Also illustrates UDP reception for 'start/stop' control

   Copyright (C) 2009 Orange Tree Technologies Ltd. All rights reserved.
   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and 
   modify this logic core in any form such as Verilog source code or EDIF netlist in 
   FPGA designs that target the ZestET1.
   Orange Tree Technologies prohibits the use of this logic core in any form such 
   as Verilog source code or EDIF netlist in FPGA designs that target any other
   hardware unless the purchaser of the ZestET1 has purchased the appropriate 
   licence from Orange Tree Technologies. Contact Orange Tree Technologies if you 
   want to purchase such a licence.

  *****************************************************************************************
  **
  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
  **              provided to you "as is". Orange Tree Technologies and its licensors 
  **              make and you receive no warranties or conditions, express, implied, 
  **              statutory or otherwise, and Orange Tree Technologies specifically 
  **              disclaims any implied warranties of merchantability, non-infringement,
  **              or fitness for a particular purpose. Orange Tree Technologies does not
  **              warrant that the functions contained in these designs will meet your 
  **              requirements, or that the operation of these designs will be 
  **              uninterrupted or error free, or that defects in the Designs will be 
  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
  **              make any representations regarding use or the results of the use of the 
  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
  **
  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
  **              or its licensors be liable for any loss of data, lost profits, cost or 
  **              procurement of substitute goods or services, or for any special, 
  **              incidental, consequential, or indirect damages arising from the use or 
  **              operation of the designs or accompanying documentation, however caused 
  **              and on any theory of liability. This limitation will apply even if 
  **              Orange Tree Technologies has been advised of the possibility of such 
  **              damage. This limitation shall apply notwithstanding the failure of the 
  **              essential purpose of any limited remedies herein.
  **
  *****************************************************************************************
*/

`timescale 1ns / 1ps

module Example3
(
    // Programmable Clock
    input CLK,
    
    // Flash Interface
    output Flash_CSOn,
    output Flash_CLK,
    output Flash_MOSI,
    input Flash_MISO,

    // IO connector
    input IO0_In,
    inout [39:0] IO0,
    input [2:0] IO3_In,
    inout [35:0] IO3,

    // Ethernet Interface
    output Eth_Clk,
    output Eth_CSn,
    output Eth_WEn,
    output [4:0] Eth_A,
    inout [15:0] Eth_D,
    output [1:0] Eth_BE,
    input Eth_Intn,

    // DDR SDRAM interface
    output DS_CLK_P,
    output DS_CLK_N,
    output DS_CKE,
    output [12:0] DS_A,
    output [1:0] DS_BA,
    output DS_CAS_N,
    output DS_RAS_N,
    output DS_WE_N,
    inout [1:0] DS_DQS,
    output [1:0] DS_DM,
    inout [15:0] DS_DQ
);

    //////////////////////////////////////////////////////////////////////////
    // Declare constants
    
    // GigExpedite register addresses
    localparam LOCAL_IP_ADDR_LOW        = 0;
    localparam LOCAL_IP_ADDR_HIGH       = 1;
    localparam LINK_STATUS              = 2;
    localparam CONNECTION_PAGE          = 7;
    localparam LOCAL_PORT               = 8;
    localparam REMOTE_IP_ADDR_LOW       = 9;
    localparam REMOTE_IP_ADDR_HIGH      = 10;
    localparam REMOTE_PORT              = 11;
    localparam MTU_TTL                  = 12;
    localparam INTERRUPT_ENABLE_STATUS  = 13;
    localparam CONNECTION_STATE         = 14;
    localparam FRAME_LENGTH             = 15;
    localparam DATA_FIFO                = 16;
    
    // Connection states (for CONNECTION_STATE reg)
    localparam CLOSED       = 16'h0000;
    localparam LISTEN       = 16'h0001;
    localparam CONNECT      = 16'h0002;
    localparam ESTABLISHED  = 16'h0003;
    localparam CONN_TCP     = 16'h0010;
    localparam CONN_ENABLE  = 16'h0020;
    
    // Interrupt enable and status bits (for INTERRUPT_ENABLE_STATUS reg)
    localparam IE_INCOMING          = 16'h0001;
    localparam IE_OUTGOING_EMPTY    = 16'h0002;
    localparam IE_OUTGOING_NOT_FULL = 16'h0004;
    localparam IE_STATE             = 16'h0008;

    // Length of burst from GigExpedite
    localparam BURST_WORDS = 96;        // 192 bytes
    
    // Send packet length (could be longer if your network supports
    // jumbo frames or if you want IP fragmentation to occur)
    localparam PACKET_LENGTH = 1400;
    
    //////////////////////////////////////////////////////////////////////////
    // Declare signals
    
    // Clocks and reset
    wire RST;
    wire RAMRST;
    wire RAMClk;
    wire RAMClk90;
    wire EthClk;

    // Ethernet interface state machine
    reg UserWE;
    reg UserRE;
    reg [4:0] UserAddr;
    reg [1:0] UserBE;
    reg [15:0] UserWriteData;
    wire [15:0] UserReadData;
    wire UserReadDataValid;
    wire UserInterrupt;

    reg [23:0] Delay;
    reg Connection;
    reg [15:0] UserFPGAState;
    reg [15:0] UserFPGASubState;
    reg [15:0] UserValidCount;
    reg [5:0] ConnectionState[0:1];
    reg [15:0] InterruptStatus[0:1];
    reg [15:0] FrameLength;
    reg [15:0] BurstWords;
    (* TIG="TRUE" *) reg EnableSend;
    reg CLKEnableSend;
    reg RAMEnableSend;
    reg [31:0] RemoteIP;
    reg [15:0] RemotePort;
    reg Clean;
    
    localparam USER_FPGA_DELAY_INIT         = 16'h0001;
    localparam USER_FPGA_CLEAN              = 16'h0002;
    localparam USER_FPGA_CLEAN_CHECK        = 16'h0004;
    localparam USER_FPGA_CLEAR_INTS         = 16'h0008;
    localparam USER_FPGA_INIT               = 16'h0010;
    localparam USER_FPGA_IDLE               = 16'h0020;
    localparam USER_FPGA_CHECK_STATE        = 16'h0040;
    localparam USER_FPGA_READ_LENGTH        = 16'h0080;
    localparam USER_FPGA_READ_HEADER        = 16'h0100;
    localparam USER_FPGA_READ_DATA          = 16'h0200;
    localparam USER_FPGA_OPEN_DATA          = 16'h0400;
    localparam USER_FPGA_CLOSE_DATA         = 16'h0800;
    localparam USER_FPGA_CHECK_SPACE        = 16'h1000;
    localparam USER_FPGA_WRITE_LENGTH       = 16'h2000;
    localparam USER_FPGA_WRITE_DATA         = 16'h4000;
    localparam USER_FPGA_NEXT_CONNECTION    = 16'h8000;

    // SDRAM FIFO declarations
    reg [23:0] SDRAMWriteCount;
    reg [23:0] SDRAMReadCount;
    reg [23:0] NextSDRAMWriteCount;
    reg [23:0] NextSDRAMReadCount;
    reg [23:0] SDRAMWordCount;
    reg [6:0] ArbCounter;
    reg SDRAMEmpty;
    reg SDRAMFull;
    reg [23:0] SDRAMAddr;
    wire SDRAMRE;
    wire SDRAMWE;
    wire [31:0] SDRAMWriteData;
    wire [31:0] SDRAMReadData;
    wire SDRAMReadDataValid;
    wire SDRAMBusy;
    reg SDRAMWriteOK;
    
    reg [31:0] WriteFIFODataIn;
    wire [31:0] WriteFIFODataOut;
    wire WriteFIFOWE;
    wire WriteFIFORE;
    wire WriteFIFOEmpty;
    wire WriteFIFOFull;
    
    wire [31:0] ReadFIFODataIn;
    wire [15:0] ReadFIFODataOut;
    wire ReadFIFOWE;
    wire ReadFIFORE;
    wire ReadFIFOEmpty;
    wire ReadFIFOFull;
    
    // Tie unused signals
    assign Flash_CSOn = 1;
    assign Flash_CLK = 1;
    assign Flash_MOSI = 1;
    assign IO0 = 40'hZ;
    assign IO3 = 36'hZ;
    
    //////////////////////////////////////////////////////////////////////////
    // Instantiate clocks
    // (Assumes default 125MHz reference clock)
    ZestET1_Clocks ClockInst (
        .RST(RST),
        .RefClk(CLK),
        .EthClk(EthClk),
        .RAMRST(RAMRST),
        .RAMClk(RAMClk),
        .RAMClk90(RAMClk90)
    );

    //////////////////////////////////////////////////////////////////////////
    // Ethernet interface test code
    ZestET1_Ethernet #(.CLOCK_RATE(125000000))
        EthernetInst (
            .User_CLK(EthClk),
            .User_RST(RST),

            // User interface
            .User_WE(UserWE),
            .User_RE(UserRE),
            .User_Addr(UserAddr),
            .User_Owner(8'h00),
            .User_WriteData(UserWriteData),
            .User_BE(UserBE),
            .User_ReadData(UserReadData),
            .User_ReadDataValid(UserReadDataValid),
            .User_ValidOwner(),
            .User_Interrupt(UserInterrupt),
            
            // Interface to GigExpedite
            .Eth_Clk(Eth_Clk),
            .Eth_CSn(Eth_CSn),
            .Eth_WEn(Eth_WEn),
            .Eth_A(Eth_A),
            .Eth_D(Eth_D),
            .Eth_BE(Eth_BE),
            .Eth_Intn(Eth_Intn)
        );

    // State machine to read/write Ethernet
    // Note: use RAMRST to keep the state machine in reset until 
    // the DCMs are locked
    always @ (posedge RAMRST or posedge EthClk) begin
        if (RAMRST==1) begin
            UserFPGAState <= USER_FPGA_DELAY_INIT;
            Delay <= 0;
            UserFPGASubState <= 0;
            UserValidCount <= 0;
            FrameLength <= 0;
            BurstWords <= 0;
            EnableSend <= 0;
            RemoteIP <= 0;
            RemotePort <= 0;
            Connection <= 0;
            InterruptStatus[0] <= 0;
            InterruptStatus[1] <= 0;
            ConnectionState[0] <= 0;
            ConnectionState[1] <= 0;
            Clean <= 1;
        end else begin
            // Counter of completed register reads
            if (UserReadDataValid==1) begin
                UserValidCount <= UserValidCount + 1;
            end
            
            case (UserFPGAState)
                // Wait here to ensure the GigExpedite has locked to
                // our clock
                USER_FPGA_DELAY_INIT: begin
                    Delay <= Delay + 1;
                    if (Delay==24'hffffff) begin
                        UserFPGAState <= USER_FPGA_CLEAN;
                    end
                end
                
                // Reset all connections in case the previous
                // application closed unexpectedly and left some open
                // Write 0 to each connection state then
                // read the status for each connection in turn
                // Loop round until all statuses are zero.
                USER_FPGA_CLEAN: begin
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserFPGASubState==31) begin
                        UserFPGASubState <= 0;
                        UserFPGAState <= USER_FPGA_CLEAN_CHECK;
                        Clean <= 1;
                    end 
                end
                USER_FPGA_CLEAN_CHECK: begin
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserReadDataValid==1 && UserReadData!=0) begin
                        Clean <= 0;
                    end
                    if (UserFPGASubState==511) begin
                        if (Clean==1) begin
                            UserFPGASubState <= 0;
                            UserValidCount <= 0;
                            UserFPGAState <= USER_FPGA_CLEAR_INTS;
                        end else begin
                            Clean <= 1;
                            UserFPGASubState <= 0;
                        end
                    end 
                end
                USER_FPGA_CLEAR_INTS: begin
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserFPGASubState==511) begin
                        UserFPGASubState <= 0;
                        UserFPGAState <= USER_FPGA_INIT;
                    end 
                end
                
                // Initialise register set
                // Set connection 0 as the control connection listening
                // for messages on port 5002.
                // Set connection 1 as the data sending connection.
                // Connection 1 is opened when a start message is received
                // and closed when a stop message is received.
                USER_FPGA_INIT: begin
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserFPGASubState==7) begin
                        UserFPGAState <= USER_FPGA_IDLE;
                    end 
                end
                
                // Wait for interrupt from GigExpedite device
                USER_FPGA_IDLE: begin
                    UserFPGASubState <= 0;
                    UserValidCount <= 0;
                    if (UserInterrupt==1) begin
                        // Interrupt has been received
                        UserFPGAState <= USER_FPGA_CHECK_STATE;
                    end
                end

                // Check if the connection state has changed
                USER_FPGA_CHECK_STATE: begin
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserReadDataValid==1 && UserValidCount[3:0]==0) begin
                        // Store interrupt status
                        InterruptStatus[Connection] <= UserReadData;
                    end
                    if (UserReadDataValid==1 && UserValidCount[3:0]==1) begin
                        // Store the new connection state
                        ConnectionState[Connection] <= UserReadData[5:0];

                        // Next, check if there is incoming data available
                        UserFPGASubState <= 0;
                        UserValidCount <= 0;
                        if (InterruptStatus[Connection][0]==0) begin
                            // There is no data available
                            // Next, check if there is outgoing data to send
                            UserFPGAState <= USER_FPGA_CHECK_SPACE;
                        end else begin
                            // Read frame length or next burst
                            BurstWords <= BURST_WORDS;
                            UserFPGAState <= FrameLength==0 ? USER_FPGA_READ_LENGTH :
                                                              USER_FPGA_READ_DATA;
                        end
                    end
                end

                // Check if there is incoming data
                USER_FPGA_READ_LENGTH: begin
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserReadDataValid==1 && UserValidCount[3:0]==0) begin
                        // Read frame length from GigExpedite
                        // Round the number of bytes up to the total number
                        // of 16 bit reads we will need to do
                        UserFPGASubState <= 0;
                        FrameLength <= {1'b0,UserReadData[15:1]}+UserReadData[0];
                        UserValidCount <= 0;
                        if (UserReadData==0) begin
                            // Length was zero - skip the read
                            // Next, check if there is outgoing data to send
                            UserFPGAState <= USER_FPGA_CHECK_SPACE;
                        end else begin
                            // Got a valid frame length - read the data
                            UserFPGAState <= USER_FPGA_READ_HEADER;
                        end
                    end
                end
                
                // Read UDP header from GigExpedite device
                // The UDP header contains the remote IP address and port
                USER_FPGA_READ_HEADER: begin
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserReadDataValid==1) begin
                        // Extract remote IP address and port from UDP header
                        if (Connection==0) begin
                            if (UserValidCount[3:0]==0) begin
                                RemoteIP[15:0] <= UserReadData;
                            end else if (UserValidCount[3:0]==1) begin
                                RemoteIP[31:16] <= UserReadData;
                            end else if (UserValidCount[3:0]==2) begin
                                RemotePort <= UserReadData;
                            end
                        end
                        if (UserValidCount[3:0]==2) begin
                            // End of header
                            UserFPGASubState <= 0;
                            UserValidCount <= 0;
                            BurstWords <= BURST_WORDS-3;
                            UserFPGAState <= USER_FPGA_READ_DATA;
                        end
                    end
                end
                
                // Read data from GigExpedite device
                USER_FPGA_READ_DATA: begin
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (Connection==0 && UserReadDataValid==1 &&
                        UserValidCount==0) begin
                        // Get start/stop bit from first byte of frame
                        EnableSend <= UserReadData[8];
                    end
                    if (UserValidCount>=FrameLength) begin
                        // End of frame reached
                        // If requested, close or open the data streaming
                        // connection
                        FrameLength <= 0;
                        UserFPGASubState <= 0;
                        UserValidCount <= 0;
                        if (Connection==0 && EnableSend==0) begin
                            UserFPGAState <= USER_FPGA_CLOSE_DATA;
                        end else if (Connection==0 && EnableSend==1) begin
                            UserFPGAState <= USER_FPGA_OPEN_DATA;
                        end else begin
                            UserFPGAState <= USER_FPGA_CHECK_SPACE;
                        end
                    end else if (UserValidCount>=BurstWords) begin
                        // End of burst reached
                        FrameLength <= FrameLength - BurstWords;
                        UserFPGASubState <= 0;
                        UserValidCount <= 0;
                        UserFPGAState <= USER_FPGA_CHECK_STATE;
                    end
                end
                
                // Open a connection for streaming data
                USER_FPGA_OPEN_DATA: begin
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserFPGASubState[7:0]==15) begin
                        UserFPGASubState <= 0;
                        UserValidCount <= 0;
                        UserFPGAState <= USER_FPGA_CHECK_SPACE;
                    end 
                end
                
                // Close a connection and wait for GigExpedite to acknowledge
                USER_FPGA_CLOSE_DATA: begin
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserReadDataValid==1 &&
                        UserReadData[3:0]==CLOSED) begin
                        UserFPGASubState <= 0;
                        UserValidCount <= 0;
                        UserFPGAState <= USER_FPGA_NEXT_CONNECTION;
                    end 
                end
                
                // Check if there is space in the outgoing GigExpedite buffer
                // and we have data to send back to the host
                USER_FPGA_CHECK_SPACE: begin
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (Connection==0 || EnableSend==0 ||
                        ReadFIFOEmpty==1) begin
                        // No data requested or no data to send
                        UserFPGASubState <= 0;
                        UserValidCount <= 0;
                        UserFPGAState <= USER_FPGA_NEXT_CONNECTION;
                    end else if (UserReadDataValid==1 &&
                                 UserValidCount[3:0]==0) begin
                        if (UserReadData[3:0]!=ESTABLISHED) begin
                            // Connection not established - ignore request
                            UserFPGASubState <= 0;
                            UserValidCount <= 0;
                            UserFPGAState <= USER_FPGA_NEXT_CONNECTION;
                        end else if (InterruptStatus[Connection][2]==1) begin
                            // Space available - write data to GigExpedite
                            UserFPGASubState <= 0;
                            UserValidCount <= 0;
                            UserFPGAState <= USER_FPGA_WRITE_LENGTH;
                        end else begin
                            // No space available in GigExpedite FIFO
                            UserFPGASubState <= 0;
                            UserValidCount <= 0;
                            UserFPGAState <= USER_FPGA_NEXT_CONNECTION;
                        end
                    end
                end
                
                // Write datagram length to outgoing GigExpedite FIFO
                USER_FPGA_WRITE_LENGTH: begin
                    UserFPGASubState <= 0;
                    UserValidCount <= 0;
                    UserFPGAState <= USER_FPGA_WRITE_DATA;
                end
                
                // Write data to outgoing GigExpedite FIFO
                USER_FPGA_WRITE_DATA: begin
                    if (ReadFIFORE==1) begin
                        UserFPGASubState <= UserFPGASubState + 1;
                        if (UserFPGASubState==(PACKET_LENGTH/2-1)) begin
                            UserFPGASubState <= 0;
                            UserValidCount <= 0;
                            UserFPGAState <= USER_FPGA_NEXT_CONNECTION;
                        end
                    end
                end
                
                // Check the next connection
                USER_FPGA_NEXT_CONNECTION: begin
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserFPGASubState[7:0]==0) begin
                        Connection <= ~Connection;
                    end else if (UserFPGASubState[7:0]==16) begin
                        UserFPGASubState <= 0;
                        UserValidCount <= 0;
                        UserFPGAState <= Connection ? USER_FPGA_IDLE :
                                                      USER_FPGA_CHECK_STATE;
                    end
                end
                
            endcase
        end
    end
    
    // Map states to Ethernet register accesses
    always @ (UserFPGAState, UserFPGASubState, UserReadDataValid,
              UserValidCount, UserReadData, Connection, FrameLength,
              RemotePort, RemoteIP, ReadFIFOEmpty, EnableSend,
              ReadFIFORE, ReadFIFODataOut) begin
        case (UserFPGAState)
            USER_FPGA_CLEAN: begin
                // Reset all connections
                UserRE <= 0;
                UserWE <= 1;
                UserBE <= 2'b11;
                UserAddr <= UserFPGASubState[0]==0 ? CONNECTION_PAGE :
                                                     CONNECTION_STATE;
                UserWriteData <= UserFPGASubState[0]==0 ?
                                                    UserFPGASubState[4:1] : 0;
            end
            
            USER_FPGA_CLEAN_CHECK: begin
                // Check all connections have been reset
                UserRE <= UserFPGASubState[4:0]==16 ? 1 : 0;
                UserWE <= UserFPGASubState[4:0]==0 ? 1 : 0;
                UserBE <= 2'b11;
                UserAddr <= UserFPGASubState[4:0]==0 ? CONNECTION_PAGE :
                                                       CONNECTION_STATE;
                UserWriteData <= UserFPGASubState[8:5];
            end

            USER_FPGA_CLEAR_INTS: begin
                // Clear all remaining interrupt status bits
                UserRE <= UserFPGASubState[4:0]==16 ? 1 : 0;
                UserWE <= UserFPGASubState[4:0]==0 ? 1 : 0;
                UserBE <= 2'b11;
                UserAddr <= UserFPGASubState[4:0]==0 ? CONNECTION_PAGE :
                                                       INTERRUPT_ENABLE_STATUS;
                UserWriteData <= UserFPGASubState[8:5];
            end
            
            USER_FPGA_INIT: begin
                // Set up registers to make one connection listen on
                // port 0x5002 for UDP connections
                UserRE <= 0;
                UserWE <= 1;
                UserBE <= 2'b11;
                case (UserFPGASubState[4:0])
                    0: begin
                        UserAddr <= CONNECTION_PAGE;
                        UserWriteData <= 0;
                    end
                    1: begin
                        UserAddr <= LOCAL_PORT;
                        UserWriteData <= 16'h5002;
                    end
                    2: begin
                        UserAddr <= REMOTE_PORT;
                        UserWriteData <= 0;
                    end
                    3: begin
                        UserAddr <= REMOTE_IP_ADDR_LOW;
                        UserWriteData <= 0;
                    end
                    4: begin
                        UserAddr <= REMOTE_IP_ADDR_HIGH;
                        UserWriteData <= 0;
                    end
                    5: begin
                        // This can be changed if your network supports
                        // jumbo frames
                        UserAddr <= MTU_TTL;
                        UserWriteData <= 16'h2D80;
                    end
                    6: begin
                        UserAddr <= INTERRUPT_ENABLE_STATUS;
                        UserWriteData <= (IE_OUTGOING_NOT_FULL | IE_INCOMING | IE_STATE);
                    end
                    default: begin
                        UserAddr <= CONNECTION_STATE;
                        UserWriteData <= (CONN_ENABLE | ESTABLISHED);
                    end
                endcase
            end
                
            USER_FPGA_CHECK_STATE: begin
                // Read connection state then update accordingly
                UserRE <= UserFPGASubState[3:1]==0 ? 1 : 0;
                UserWE <= 0;
                UserBE <= 2'b11;
                UserAddr <= UserFPGASubState[3:0]==0 ? INTERRUPT_ENABLE_STATUS :
                                                       CONNECTION_STATE;
                UserWriteData <= 0;
            end
            
            USER_FPGA_READ_LENGTH: begin
                // Read from frame length register
                UserRE <= UserFPGASubState[3:0]==0 ? 1 : 0;
                UserWE <= 1'b0;
                UserBE <= 2'b11;
                UserAddr <= FRAME_LENGTH;
                UserWriteData <= 0;
            end
                
            USER_FPGA_READ_HEADER: begin
                // Read header from connection FIFO
                UserRE <= UserFPGASubState[3:0]<3 ? 1 : 0;
                UserWE <= 0;
                UserBE <= 2'b11;
                UserAddr <= DATA_FIFO+Connection;
                UserWriteData <= 0;
            end
            
            USER_FPGA_READ_DATA: begin
                // Read from connection FIFO
                UserRE <= UserFPGASubState<FrameLength &&
                          UserFPGASubState<BurstWords ? 1 : 0;
                UserWE <= 0;
                UserBE <= 2'b11;
                UserAddr <= DATA_FIFO+Connection;
                UserWriteData <= 0;
            end

            USER_FPGA_OPEN_DATA: begin
                // Set up registers to make one connection send data
                // to requested IP address and port
                UserRE <= 0;
                UserWE <= UserFPGASubState[4:0]<=8 ? 1 : 0;
                UserBE <= 2'b11;
                case (UserFPGASubState[4:0])
                    0: begin
                        UserAddr <= CONNECTION_PAGE;
                        UserWriteData <= 1;
                    end
                    1: begin
                        UserAddr <= LOCAL_PORT;
                        UserWriteData <= 16'h5003;
                    end
                    2: begin
                        UserAddr <= REMOTE_PORT;
                        UserWriteData <= RemotePort;
                    end
                    3: begin
                        UserAddr <= REMOTE_IP_ADDR_LOW;
                        UserWriteData <= RemoteIP[15:0];
                    end
                    4: begin
                        UserAddr <= REMOTE_IP_ADDR_HIGH;
                        UserWriteData <= RemoteIP[31:16];
                    end
                    5: begin
                        // This can be changed if your network supports
                        // jumbo frames
                        UserAddr <= MTU_TTL;
                        UserWriteData <= 16'h2D80;
                    end
                    6: begin
                        UserAddr <= INTERRUPT_ENABLE_STATUS;
                        UserWriteData <= (IE_OUTGOING_NOT_FULL | IE_INCOMING | IE_STATE);
                    end
                    7: begin
                        UserAddr <= CONNECTION_STATE;
                        UserWriteData <= (CONN_ENABLE | ESTABLISHED);
                    end
                    default: begin
                        UserAddr <= CONNECTION_PAGE;
                        UserWriteData <= Connection;
                    end
                endcase
            end

            USER_FPGA_CLOSE_DATA: begin
                // Close connection and wait for acknowledgement
                // to requested IP address and port
                UserRE <= UserFPGASubState[7:0]>15 ? 1 : 0;
                UserWE <= UserFPGASubState[7:0]<2 ? 1 : 0;
                UserBE <= 2'b11;
                UserAddr <= UserFPGASubState[7:0]==0 ? CONNECTION_PAGE :
                                                       CONNECTION_STATE;
                UserWriteData <= UserFPGASubState[7:0]==0 ? 1 : CLOSED;
            end

            USER_FPGA_CHECK_SPACE: begin
                // Read from connection status
                UserRE <= (UserFPGASubState[3:0]==0 && Connection!=0 && 
                           ReadFIFOEmpty==0 && EnableSend==1) ? 1 : 0;
                UserWE <= 0;
                UserBE <= 2'b11;
                UserAddr <= CONNECTION_STATE;
                UserWriteData <= 0;
            end
                
            USER_FPGA_WRITE_LENGTH: begin
                // Write UDP datagram length to GigExpedite
                UserRE <= 0;
                UserWE <= 1;
                UserBE <= 2'b11;
                UserAddr <= FRAME_LENGTH;
                UserWriteData <= PACKET_LENGTH;
            end
            
            USER_FPGA_WRITE_DATA: begin
                // Write test data to connection FIFO
                UserRE <= 0;
                UserWE <= ReadFIFORE;
                UserBE <= 2'b11;
                UserAddr <= DATA_FIFO+Connection;
                UserWriteData <= ReadFIFODataOut;
            end

            USER_FPGA_NEXT_CONNECTION: begin
                // Change connection
                UserRE <= 0;
                UserWE <= UserFPGASubState[7:0]==1 ? 1 : 0;
                UserBE <= 2'b11;
                UserAddr <= CONNECTION_PAGE;
                UserWriteData <= {15'h0000, Connection};
            end
                
            default: begin
                // Don't do anything
                UserRE <= 0;
                UserWE <= 0;
                UserAddr <= 0;
                UserBE <= 2'b11;
                UserWriteData <= 0;
            end
        endcase
    end

    //////////////////////////////////////////////////////////////////////////
    // SDRAM FIFO
    // Use the SDRAM as a large FIFO for buffering data
    // Input data is fed into a block RAM FIFO to cross the clock domain
    // to the SDRAM clock.  The SDRAM is used as a 32Mbyte cicular buffer.
    // Data is then fed through another FIFO to cross to the Ethernet clock
    // domain before being sent to the GigExpedite device.
    WriteFIFO WriteFIFOInst (
        .rst(RAMRST),
        .wr_clk(CLK),
        .rd_clk(RAMClk),
        .wr_en(WriteFIFOWE),
        .din(WriteFIFODataIn),
        .rd_en(WriteFIFORE),
        .dout(WriteFIFODataOut),
        .empty(WriteFIFOEmpty),
        .full(WriteFIFOFull)
    );
    ReadFIFO ReadFIFOInst (
        .rst(RAMRST),
        .wr_clk(RAMClk),
        .rd_clk(EthClk),
        .wr_en(ReadFIFOWE),
        .din(ReadFIFODataIn),
        .rd_en(ReadFIFORE),
        .dout(ReadFIFODataOut),
        .empty(ReadFIFOEmpty),
        .prog_full(ReadFIFOFull) // Use almost full to handle SDRAM latency
    );
    
    // Connections for write FIFO
    // We feed a ramp as test data but this could be connected to the IO pins
    assign WriteFIFOWE = CLKEnableSend & ~WriteFIFOFull;
    always @ (posedge RAMRST or posedge CLK) begin
        if (RAMRST==1) begin
            WriteFIFODataIn <= 0;
            CLKEnableSend <= 0;
        end else begin
            CLKEnableSend <= EnableSend;
            if (WriteFIFOWE==1) begin
                WriteFIFODataIn <= WriteFIFODataIn + 1;
            end
        end
    end
    assign WriteFIFORE = SDRAMWriteOK & ~SDRAMBusy & ~WriteFIFOEmpty;
    
    // Connections for read FIFO
    assign ReadFIFOWE = SDRAMReadDataValid;
    assign ReadFIFODataIn = SDRAMReadData;
    assign ReadFIFORE = Connection==1 && 
                        UserFPGAState==USER_FPGA_WRITE_DATA &&
                        ~ReadFIFOEmpty ? 1 : 0; 
    
    // SDRAM Buffer
    always @ (posedge RAMRST or posedge RAMClk) begin
        if (RAMRST==1) begin
            ArbCounter <= 0;
            SDRAMWriteCount <= 0;
            SDRAMReadCount <= 0;
            SDRAMWordCount <= 0;
            SDRAMEmpty <= 1;
            SDRAMFull <= 0;
            NextSDRAMWriteCount <= 1;
            NextSDRAMReadCount <= 1;
            SDRAMWriteOK <= 0;
            RAMEnableSend <= 0;
        end else begin
            RAMEnableSend <= EnableSend;
            // Arbitrate between SDRAM reads and writes
            if (RAMEnableSend==1) begin
                ArbCounter <= ArbCounter + 1;
            end
            if (ArbCounter[5:0]==6'b111111) begin
                SDRAMAddr <= ArbCounter[6]==0 ? SDRAMWriteCount :
                                                SDRAMReadCount;
            end else if (SDRAMWE==1) begin
                SDRAMAddr <= NextSDRAMWriteCount;
            end else if (SDRAMRE==1) begin
                SDRAMAddr <= NextSDRAMReadCount;
            end
            if (ArbCounter[5:0]==6'b111111) begin
                SDRAMWriteOK <= ArbCounter[6]==0 ? ~SDRAMFull : 0;
            end else begin
                SDRAMWriteOK <= ArbCounter[6] & ~SDRAMFull;
            end
            
            // Update read and write counters
            if (SDRAMWE==1) begin
                SDRAMWriteCount <= NextSDRAMWriteCount;
                NextSDRAMWriteCount <= NextSDRAMWriteCount+1;
            end
            if (SDRAMRE==1) begin
                SDRAMReadCount <= NextSDRAMReadCount;
                NextSDRAMReadCount <= NextSDRAMReadCount+1;
            end
            
            // Update SDRAM full and empty flags
            if (SDRAMWE==1 && SDRAMRE==0) begin
                SDRAMEmpty <= 0;
            end else if (SDRAMWE==0 && SDRAMRE==1) begin
                SDRAMEmpty <= NextSDRAMReadCount==SDRAMWriteCount ? 1 : 0;
            end
            SDRAMWordCount <= SDRAMWriteCount-SDRAMReadCount;
            SDRAMFull <= SDRAMWordCount>=24'hffff00 ? 1 : 0;
        end
    end
    
    assign SDRAMWE = WriteFIFORE;
    assign SDRAMWriteData = WriteFIFODataOut;
    assign SDRAMRE = ArbCounter[6]==0 && ~SDRAMEmpty &&
                     ~ReadFIFOFull && ~SDRAMBusy ? 1 : 0; 
    
    //////////////////////////////////////////////////////////////////////////
    // Instantiate SDRAM component
    ZestET1_SDRAM  #(.CLOCK_RATE(166666666))
        SDRAMInst (
            .User_CLK(RAMClk),
            .User_CLK90(RAMClk90),
            .User_RST(RAMRST),

            .User_A(SDRAMAddr),
            .User_RE(SDRAMRE),
            .User_WE(SDRAMWE),
            .User_Owner(8'h00),
            .User_BE(4'b1111),
            .User_DW(SDRAMWriteData),
            .User_DR(SDRAMReadData),
            .User_DR_Valid(SDRAMReadDataValid),
            .User_ValidOwner(),
            .User_Busy(SDRAMBusy),
            .User_InitDone(),

            .DS_CLK_P(DS_CLK_P),
            .DS_CLK_N(DS_CLK_N),
            .DS_A(DS_A),
            .DS_BA(DS_BA),
            .DS_DQ(DS_DQ),
            .DS_DQS(DS_DQS),
            .DS_DM(DS_DM),
            .DS_CAS_N(DS_CAS_N),
            .DS_RAS_N(DS_RAS_N),
            .DS_CKE(DS_CKE),
            .DS_WE_N(DS_WE_N)
        );

endmodule

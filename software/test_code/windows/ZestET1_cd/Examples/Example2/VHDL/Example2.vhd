--   ZestET1 Example 2
--   File name: Example2.vhd
--   Version: 1.00
--   Date: 26/7/2009
--
--   ZestET1 Example 2 - TCP test code.
--   Copies data to and from the Ethernet interface
--
--   Copyright (C) 2009 Orange Tree Technologies Ltd. All rights reserved.
--   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and 
--   modify this logic core in any form such as VHDL source code or EDIF netlist in 
--   FPGA designs that target the ZestET1.
--   Orange Tree Technologies prohibits the use of this logic core in any form such 
--   as VHDL source code or EDIF netlist in FPGA designs that target any other
--   hardware unless the purchaser of the ZestET1 has purchased the appropriate 
--   licence from Orange Tree Technologies. Contact Orange Tree Technologies if you 
--   want to purchase such a licence.
--
--  *****************************************************************************************
--  **
--  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
--  **              provided to you "as is". Orange Tree Technologies and its licensors 
--  **              make and you receive no warranties or conditions, express, implied, 
--  **              statutory or otherwise, and Orange Tree Technologies specifically 
--  **              disclaims any implied warranties of merchantability, non-infringement,
--  **              or fitness for a particular purpose. Orange Tree Technologies does not
--  **              warrant that the functions contained in these designs will meet your 
--  **              requirements, or that the operation of these designs will be 
--  **              uninterrupted or error free, or that defects in the Designs will be 
--  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
--  **              make any representations regarding use or the results of the use of the 
--  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
--  **
--  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
--  **              or its licensors be liable for any loss of data, lost profits, cost or 
--  **              procurement of substitute goods or services, or for any special, 
--  **              incidental, consequential, or indirect damages arising from the use or 
--  **              operation of the designs or accompanying documentation, however caused 
--  **              and on any theory of liability. This limitation will apply even if 
--  **              Orange Tree Technologies has been advised of the possibility of such 
--  **              damage. This limitation shall apply notwithstanding the failure of the 
--  **              essential purpose of any limited remedies herein.
--  **
--  *****************************************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity Example2 is
    port (
        -- Programmable Clock
        CLK : in std_logic;
        
        -- Flash Interface
        Flash_CSOn : out std_logic;
        Flash_CLK : out std_logic;
        Flash_MOSI : out std_logic;
        Flash_MISO : in std_logic;

        -- IO connector
        IO0_In : in std_logic;
        IO0 : inout std_logic_vector(39 downto 0);
        IO3_In : in std_logic_vector(2 downto 0);
        IO3 : inout std_logic_vector(35 downto 0);

        -- Ethernet Interface
        Eth_Clk : out std_logic;
        Eth_CSn : out std_logic;
        Eth_WEn : out std_logic;
        Eth_A : out std_logic_vector(4 downto 0);
        Eth_D : inout std_logic_vector(15 downto 0);
        Eth_BE : out std_logic_vector(1 downto 0);
        Eth_Intn : in std_logic;

        -- DDR SDRAM interface
        DS_CLK_P : out std_logic;
        DS_CLK_N : out std_logic;
        DS_CKE : out std_logic;
        DS_A : out std_logic_vector(12 downto 0);
        DS_BA : out std_logic_vector(1 downto 0);
        DS_CAS_N : out std_logic;
        DS_RAS_N : out std_logic;
        DS_WE_N : out std_logic;
        DS_DQS : inout std_logic_vector(1 downto 0);
        DS_DM : out std_logic_vector(1 downto 0);
        DS_DQ : inout std_logic_vector(15 downto 0)
    );
end Example2;

architecture arch of Example2 is

    --------------------------------------------------------------------------
    -- Declare constants
    
    -- GigExpedite register addresses
    constant LOCAL_IP_ADDR_LOW          : std_logic_vector(4 downto 0) := "00000";
    constant LOCAL_IP_ADDR_HIGH         : std_logic_vector(4 downto 0) := "00001";
    constant LINK_STATUS                : std_logic_vector(4 downto 0) := "00010";
    constant CONNECTION_PAGE            : std_logic_vector(4 downto 0) := "00111";
    constant LOCAL_PORT                 : std_logic_vector(4 downto 0) := "01000";
    constant REMOTE_IP_ADDR_LOW         : std_logic_vector(4 downto 0) := "01001";
    constant REMOTE_IP_ADDR_HIGH        : std_logic_vector(4 downto 0) := "01010";
    constant REMOTE_PORT                : std_logic_vector(4 downto 0) := "01011";
    constant MTU_TTL                    : std_logic_vector(4 downto 0) := "01100";
    constant INTERRUPT_ENABLE_STATUS    : std_logic_vector(4 downto 0) := "01101";
    constant CONNECTION_STATE           : std_logic_vector(4 downto 0) := "01110";
    constant FRAME_LENGTH               : std_logic_vector(4 downto 0) := "01111";
    constant DATA_FIFO                  : std_logic_vector(4 downto 0) := "10000";
    
    -- Connection states (for CONNECTION_STATE reg)
    constant CLOSED       : std_logic_vector(15 downto 0) := X"0000";
    constant LISTEN       : std_logic_vector(15 downto 0) := X"0001";
    constant CONNECT      : std_logic_vector(15 downto 0) := X"0002";
    constant ESTABLISHED  : std_logic_vector(15 downto 0) := X"0003";
    constant CONN_TCP     : std_logic_vector(15 downto 0) := X"0010";
    constant CONN_ENABLE  : std_logic_vector(15 downto 0) := X"0020";
    
    -- Interrupt enable and status bits (for INTERRUPT_ENABLE_STATUS reg)
    constant IE_INCOMING          : std_logic_vector(15 downto 0) := X"0001";
    constant IE_OUTGOING_EMPTY    : std_logic_vector(15 downto 0) := X"0002";
    constant IE_OUTGOING_NOT_FULL : std_logic_vector(15 downto 0) := X"0004";
    constant IE_STATE             : std_logic_vector(15 downto 0) := X"0008";

    -- Length of burst from GigExpedite (192 bytes)
    constant BURST_WORDS          : std_logic_vector(15 downto 0) := X"0060";

    -- Test transfer length
    constant WRITE_LENGTH         : std_logic_vector(31 downto 0) := conv_std_logic_vector(500*1024*1024, 32);

    --------------------------------------------------------------------------
    -- Declare signals
    
    -- Clocks and reset
    signal RST : std_logic;
    signal RAMRST : std_logic;
    signal RAMClk : std_logic;
    signal RAMClk90 : std_logic;
    signal EthClk : std_logic;

    -- Ethernet interface state machine
    signal UserWE : std_logic;
    signal UserRE : std_logic;
    signal UserAddr : std_logic_vector(4 downto 0);
    signal UserBE : std_logic_vector(1 downto 0);
    signal UserWriteData : std_logic_vector(15 downto 0);
    signal UserReadData : std_logic_vector(15 downto 0);
    signal UserReadDataValid : std_logic;
    signal UserInterrupt : std_logic;

    signal Delay : std_logic_vector(23 downto 0);
    signal UserFPGASubState : std_logic_vector(15 downto 0);
    signal UserValidCount : std_logic_vector(15 downto 0);
    signal ConnectionState : std_logic_vector(5 downto 0);
    signal InterruptStatus : std_logic_vector(15 downto 0);
    signal FrameLength : std_logic_vector(15 downto 0);
    signal Clean : std_logic;
    signal RampData : std_logic_vector(15 downto 0);
    signal WriteCount : std_logic_vector(31 downto 0);
    signal LatencyDelay : std_logic_vector(3 downto 0);

    type STATE_TYPE is (
        USER_FPGA_DELAY_INIT,
        USER_FPGA_CLEAN,
        USER_FPGA_CLEAN_CHECK,
        USER_FPGA_CLEAR_INTS,
        USER_FPGA_INIT,
        USER_FPGA_IDLE,
        USER_FPGA_CHECK_STATE,
        USER_FPGA_CHECK_SPACE,
        USER_FPGA_WRITE_DATA,
        USER_FPGA_READ_LENGTH,
        USER_FPGA_READ_DATA
    );
    signal UserFPGAState : STATE_TYPE;

begin
    
    -- Tie unused signals
    Flash_CSOn <= '1';
    Flash_CLK <= '1';
    Flash_MOSI <= '1';
    IO0 <=  (others=>'Z');
    IO3 <= (others=>'Z');
    
    -- Reset controller
    ROCInst : ROC port map (O=>RST);
    
    --------------------------------------------------------------------------
    -- Instantiate clocks
    -- (Assumes default 125MHz reference clock)
    ClockInst : entity work.ZestET1_Clocks
        port map (
            RST => RST,
            RefClk => CLK,
            EthClk => EthClk,
            RAMRST => RAMRST,
            RAMClk => RAMClk,
            RAMClk90 => RAMClk90    
        );

    --------------------------------------------------------------------------
    -- Ethernet interface test code
    EthernetInst : entity work.ZestET1_Ethernet
        generic map (
            CLOCK_RATE => 125000000
        )
        port map (
            User_CLK => EthClk,
            User_RST => RST,

            -- User interface
            User_WE => UserWE,
            User_RE => UserRE,
            User_Addr => UserAddr,
            User_Owner => X"00",
            User_WriteData => UserWriteData,
            User_BE => UserBE,
            User_ReadData => UserReadData,
            User_ReadDataValid => UserReadDataValid,
            --User_ValidOwner => ,
            User_Interrupt => UserInterrupt,
            
            -- Interface to GigExpedite
            Eth_Clk => Eth_Clk,
            Eth_CSn => Eth_CSn,
            Eth_WEn => Eth_WEn,
            Eth_A => Eth_A,
            Eth_D => Eth_D,
            Eth_BE => Eth_BE,
            Eth_Intn => Eth_Intn
        );

    -- State machine to read/write Ethernet
    -- Note: use RAMRST to keep the state machine in reset until 
    -- the DCMs are locked
    process (RAMRST, EthClk) begin
        if (RAMRST='1') then
            UserFPGAState <= USER_FPGA_DELAY_INIT;
            Delay <= (others=>'0');
            UserFPGASubState <= (others=>'0');
            UserValidCount <= (others=>'0');
            ConnectionState <= (others=>'0');
            InterruptStatus <= (others=>'0');
            FrameLength <= (others=>'0');
            Clean <= '1';
            RampData <= X"0001";
            WriteCount <= (others=>'0');
            LatencyDelay <= (others=>'0');
        elsif (EthClk'event and EthClk='1') then
            -- Counter of completed register reads
            if (UserReadDataValid='1') then
                UserValidCount <= UserValidCount + 1;
            end if;
            
            -- Interrupt status latency counter
            if (LatencyDelay/=X"0") then
                LatencyDelay <= LatencyDelay - 1;
            end if;
            
            case (UserFPGAState) is
                -- Wait here to ensure the GigExpedite has locked to
                -- our clock
                when USER_FPGA_DELAY_INIT =>
                    Delay <= Delay + 1;
                    if (Delay=X"FFFFFF") then
                        UserFPGAState <= USER_FPGA_CLEAN;
                    end if;
                
                -- Reset all connections in case the previous
                -- application closed unexpectedly and left some open
                -- Write 0 to each connection state then
                -- read the status for each connection in turn
                -- Loop round until all statuses are zero.
                when USER_FPGA_CLEAN =>
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserFPGASubState=X"001f") then
                        UserFPGASubState <= (others=>'0');
                        UserFPGAState <= USER_FPGA_CLEAN_CHECK;
                        Clean <= '1';
                    end if;
                when USER_FPGA_CLEAN_CHECK =>
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserReadDataValid='1' and UserReadData/=X"0000") then
                        Clean <= '0';
                    end if;
                    if (UserFPGASubState=X"01ff") then
                        if (Clean='1') then
                            UserFPGASubState <= (others=>'0');
                            UserValidCount <= (others=>'0');
                            UserFPGAState <= USER_FPGA_CLEAR_INTS;
                        else
                            Clean <= '1';
                            UserFPGASubState <= (others=>'0');
                        end if;
                    end if;
                when USER_FPGA_CLEAR_INTS =>
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserFPGASubState=X"01ff") then
                        UserFPGASubState <= (others=>'0');
                        UserValidCount <= (others=>'0');
                        UserFPGAState <= USER_FPGA_INIT;
                    end if;
                
                -- Initialise register set
                -- Configures one connection as a TCP server waiting
                -- for a connection from a client (i.e. the host program)
                when USER_FPGA_INIT => 
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserFPGASubState=X"0004") then
                        UserFPGAState <= USER_FPGA_IDLE;
                    end if;
                
                -- Wait for interrupt from GigExpedite device
                when USER_FPGA_IDLE => 
                    UserFPGASubState <= (others=>'0');
                    UserValidCount <= (others=>'0');
                    if (UserInterrupt='1') then
                        -- Interrupt has been received
                        UserFPGAState <= USER_FPGA_CHECK_STATE;
                    end if;

                -- Check if the connection state has changed
                when USER_FPGA_CHECK_STATE => 
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserReadDataValid='1' and UserValidCount(0)='0') then
                        -- Store the interrupt status bits
                        InterruptStatus <= UserReadData;
                    end if;
                    if (UserReadDataValid='1' and UserValidCount(0)='1') then
                        -- Store the new connection state
                        ConnectionState <= UserReadData(5 downto 0);

                        -- Next, check if there is incoming data available
                        UserFPGASubState <= (others=>'0');
                        UserValidCount <= (others=>'0');
                        if (LatencyDelay/=X"0" or InterruptStatus(0)='0') then
                            -- Loopback FIFO is nearly full or there is no data available
                            -- Next, check if there is outgoing data to send
                            UserFPGAState <= USER_FPGA_CHECK_SPACE;
                        else
                            -- Read frame length or next burst
                            if (FrameLength=X"0000") then
                                UserFPGAState <= USER_FPGA_READ_LENGTH;
                            else
                                UserFPGAState <= USER_FPGA_READ_DATA;
                            end if;
                        end if;
                    end if;

                -- Check if there is incoming data
                when USER_FPGA_READ_LENGTH =>
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserReadDataValid='1' and UserValidCount=X"0000") then
                        -- Read frame length from GigExpedite
                        -- Round the number of bytes up to the total number
                        -- of 16 bit reads we will need to do
                        UserFPGASubState <= (others=>'0');
                        UserValidCount <= (others=>'0');
                        FrameLength <= ('0'&UserReadData(15 downto 1)) + UserReadData(0);
                        if (UserReadData=conv_std_logic_vector(0, 16)) then
                            -- Length was zero - skip the read
                            UserFPGAState <= USER_FPGA_IDLE;
                        else
                            -- Got a valid frame length - read the data
                            UserFPGAState <= USER_FPGA_READ_DATA;
                        end if;
                    end if;
                
                -- Read data from GigExpedite device
                when USER_FPGA_READ_DATA => 
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserValidCount>=FrameLength) then
                        -- End of frame reached
                        -- There is a latency delay on the interrupt
                        -- status at the end of a frame
                        LatencyDelay <= X"F";
                        FrameLength <= (others=>'0');
                        UserFPGASubState <= (others=>'0');
                        UserValidCount <= (others=>'0');
                        UserFPGAState <= USER_FPGA_CHECK_SPACE;
                    elsif (UserValidCount>=BURST_WORDS) then
                        -- End of burst reached
                        FrameLength <= FrameLength - BURST_WORDS;
                        UserFPGASubState <= (others=>'0');
                        UserValidCount <= (others=>'0');
                        UserFPGAState <= USER_FPGA_IDLE;
                    end if;
                
                -- Check if there is space in the outgoing GigExpedite buffer
                -- and we have data to send back to the host
                when USER_FPGA_CHECK_SPACE => 
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (WriteCount=WRITE_LENGTH or
                        ConnectionState(3 downto 0)/=ESTABLISHED(3 downto 0) or
                        InterruptStatus(2)='0') then
                        -- Already sent all our data or the 
                        -- connection from the host has not been made
                        -- or there is no space in the GigExpedite buffer
                        UserFPGASubState <= (others=>'0');
                        UserValidCount <= (others=>'0');
                        UserFPGAState <= USER_FPGA_IDLE;
                    else
                        -- OK to write data to GigExpedite FIFO
                        UserFPGASubState <= (others=>'0');
                        UserValidCount <= (others=>'0');
                        UserFPGAState <= USER_FPGA_WRITE_DATA;
                    end if;
                
                -- Write 1kbyte data to outgoing FIFO
                when USER_FPGA_WRITE_DATA =>
                    UserFPGASubState <= UserFPGASubState + 1;
                    RampData(7 downto 0) <= RampData(7 downto 0) + 2;
                    RampData(15 downto 8) <= RampData(15 downto 8) + 2;
                    WriteCount <= WriteCount + 2;
                    if (UserFPGASubState=conv_std_logic_vector(511, 16)) then
                        UserFPGASubState <= (others=>'0');
                        UserValidCount <= (others=>'0');
                        UserFPGAState <= USER_FPGA_IDLE;
                    end if;
                
                when others => null;
            end case;
        end if;
    end process;
    
    -- Map states to Ethernet register accesses
    process (UserFPGAState, UserFPGASubState, UserReadDataValid,
             UserValidCount, UserReadData, ConnectionState,
             FrameLength) begin
        case UserFPGAState is
            when USER_FPGA_CLEAN =>
                -- Reset all connections
                UserRE <= '0';
                UserWE <= '1';
                UserBE <= "11";
                if (UserFPGASubState(0)='0') then 
                    UserAddr <= CONNECTION_PAGE;
                    UserWriteData <= X"000" & UserFPGASubState(4 downto 1);
                else
                    UserAddr <= CONNECTION_STATE;
                    UserWriteData <= (others=>'0');
                end if;
            
            when USER_FPGA_CLEAN_CHECK =>
                -- Check all connections have been reset
                if (UserFPGASubState(4 downto 0)="10000") then
                    UserRE <= '1';
                else
                    UserRE <= '0';
                end if;
                if (UserFPGASubState(4 downto 0)="00000") then
                    UserWE <= '1';
                    UserAddr <= CONNECTION_PAGE;
                else
                    UserWE <= '0';
                    UserAddr <= CONNECTION_STATE;
                end if;
                UserBE <= "11";
                UserWriteData <= X"000" & UserFPGASubState(8 downto 5);

            when USER_FPGA_CLEAR_INTS =>
                -- Clear state change interrupts
                UserBE <= "11";
                if (UserFPGASubState(4 downto 0)="00000") then
                    UserRE <= '0';
                    UserWE <= '1';
                    UserAddr <= CONNECTION_PAGE;
                elsif (UserFPGASubState(4 downto 0)="10000") then
                    UserRE <= '1';
                    UserWE <= '0';
                    UserAddr <= INTERRUPT_ENABLE_STATUS;
                else
                    UserRE <= '0';
                    UserWE <= '0';
                end if;
                UserWriteData <= X"000" & UserFPGASubState(8 downto 5);
            
            when USER_FPGA_INIT => 
                -- Set up registers to make one connection listen on
                -- port 0x5002 for TCP connections
                UserRE <= '0';
                UserWE <= '1';
                UserBE <= "11";
                case UserFPGASubState(4 downto 0) is
                    when "00000" =>
                        UserAddr <= CONNECTION_PAGE;
                        UserWriteData <= (others=>'0');
                    when "00001" =>
                        UserAddr <= LOCAL_PORT;
                        UserWriteData <= X"5002";
                    when "00010" => 
                        UserAddr <= MTU_TTL;
                        UserWriteData <= X"8080";
                    when "00011" =>
                        UserAddr <= INTERRUPT_ENABLE_STATUS;
                        UserWriteData <= (IE_OUTGOING_NOT_FULL or IE_INCOMING or IE_STATE);
                    when others =>
                        UserAddr <= CONNECTION_STATE;
                        UserWriteData <= (CONN_TCP or CONN_ENABLE or LISTEN);
                end case;
                
            when USER_FPGA_CHECK_STATE => 
                -- Read connection state then update accordingly
                -- The only important state change is to ESTABLISHED which
                -- we must acknowledge by changing our state to ESTABLISHED.
                -- All other state changes result in us returning to LISTEN
                -- to wait for another connection from a new client.
                if (UserFPGASubState(3 downto 1)="000") then 
                    UserRE <= '1';
                else
                    UserRE <= '0';
                end if;
                if (UserReadDataValid='1' and UserValidCount(0)='1' and
                    UserReadData(5 downto 0)/=ConnectionState) then
                    UserWE <= '1';
                else
                    UserWE <= '0';
                end if;
                UserBE <= "11";
                if (UserFPGASubState(3 downto 0)=X"0") then
                    UserAddr <= INTERRUPT_ENABLE_STATUS;
                else
                    UserAddr <= CONNECTION_STATE;
                end if;
                if (UserReadData(3 downto 0)=ESTABLISHED(3 downto 0)) then
                    UserWriteData <= (CONN_TCP or CONN_ENABLE or ESTABLISHED);
                else
                    UserWriteData <= (CONN_TCP or CONN_ENABLE or LISTEN);
                end if;
            
            when USER_FPGA_READ_LENGTH => 
                -- Read from interrupt status and then frame length register
                if (UserFPGASubState(3 downto 0)=X"0") then
                    UserRE <= '1';
                else
                    UserRE <= '0';
                end if;
                UserWE <= '0';
                UserBE <= "11";
                UserAddr <= FRAME_LENGTH;
                UserWriteData <= (others=>'0');
                
            when USER_FPGA_READ_DATA => 
                -- Read from connection FIFO
                if (UserFPGASubState<FrameLength and
                    UserFPGASubState<BURST_WORDS) then
                    UserRE <= '1';
                else
                    UserRE <= '0';
                end if;
                UserWE <= '0';
                UserBE <= "11";
                UserAddr <= DATA_FIFO;
                UserWriteData <= (others=>'0');

            when USER_FPGA_CHECK_SPACE =>
                -- No access necessary
                UserRE <= '0';
                UserWE <= '0';
                UserBE <= "11";
                UserAddr <= (others=>'0');
                UserWriteData <= (others=>'0');
                
            when USER_FPGA_WRITE_DATA => 
                -- Write test data to connection FIFO
                UserRE <= '0';
                UserWE <= '1';
                UserBE <= "11";
                UserAddr <= DATA_FIFO;
                UserWriteData <= RampData;
                
            when others =>
                -- Don't do anything
                UserRE <= '0';
                UserWE <= '0';
                UserAddr <= (others=>'0');
                UserBE <= "11";
                UserWriteData <= (others=>'0');
            
        end case;
    end process;

    --------------------------------------------------------------------------
    -- SDRAM Buffer
    -- Place holder - not used in the example but ties the pins correctly
    
    -- Instantiate SDRAM component
    SDRAMInst : entity work.ZestET1_SDRAM
        generic map (
            CLOCK_RATE => 166666666
        )
        port map (
            User_CLK => RAMClk,
            User_CLK90 => RAMClk90,
            User_RST => RAMRST,

            User_A => (others=>'0'),
            User_RE => '0',
            User_WE => '0',
            User_Owner => X"00",
            User_BE => (others=>'0'),
            User_DW => (others=>'0'),
            --User_DR => ,
            --User_DR_Valid => ,
            --User_ValidOwner => ,
            --User_Busy => ,
            --User_InitDone => ,

            DS_CLK_P => DS_CLK_P,
            DS_CLK_N => DS_CLK_N,
            DS_A => DS_A,
            DS_BA => DS_BA,
            DS_DQ => DS_DQ,
            DS_DQS => DS_DQS,
            DS_DM => DS_DM,
            DS_CAS_N => DS_CAS_N,
            DS_RAS_N => DS_RAS_N,
            DS_CKE => DS_CKE,
            DS_WE_N => DS_WE_N
        );
    
end arch;


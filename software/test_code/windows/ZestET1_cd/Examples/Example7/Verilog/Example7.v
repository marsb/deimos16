/*
   ZestET1 Example 7
   File name: Example7.v
   Version: 1.0
   Date: 31/5/2011

   ZestET1 Example 7 - UDP test code.
   Streams data to Ethernet using UDP and wrapper interface.
   Also illustrates UDP reception for 'start/stop' control

   Copyright (C) 2011 Orange Tree Technologies Ltd. All rights reserved.
   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and 
   modify this logic core in any form such as Verilog source code or EDIF netlist in 
   FPGA designs that target the ZestET1.
   Orange Tree Technologies prohibits the use of this logic core in any form such 
   as Verilog source code or EDIF netlist in FPGA designs that target any other
   hardware unless the purchaser of the ZestET1 has purchased the appropriate 
   licence from Orange Tree Technologies. Contact Orange Tree Technologies if you 
   want to purchase such a licence.

  *****************************************************************************************
  **
  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
  **              provided to you "as is". Orange Tree Technologies and its licensors 
  **              make and you receive no warranties or conditions, express, implied, 
  **              statutory or otherwise, and Orange Tree Technologies specifically 
  **              disclaims any implied warranties of merchantability, non-infringement,
  **              or fitness for a particular purpose. Orange Tree Technologies does not
  **              warrant that the functions contained in these designs will meet your 
  **              requirements, or that the operation of these designs will be 
  **              uninterrupted or error free, or that defects in the Designs will be 
  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
  **              make any representations regarding use or the results of the use of the 
  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
  **
  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
  **              or its licensors be liable for any loss of data, lost profits, cost or 
  **              procurement of substitute goods or services, or for any special, 
  **              incidental, consequential, or indirect damages arising from the use or 
  **              operation of the designs or accompanying documentation, however caused 
  **              and on any theory of liability. This limitation will apply even if 
  **              Orange Tree Technologies has been advised of the possibility of such 
  **              damage. This limitation shall apply notwithstanding the failure of the 
  **              essential purpose of any limited remedies herein.
  **
  *****************************************************************************************
*/

`timescale 1ns / 1ps

module Example7
(
    // Programmable Clock
    input CLK,
    
    // Flash Interface
    output Flash_CSOn,
    output Flash_CLK,
    output Flash_MOSI,
    input Flash_MISO,

    // IO connector
    input IO0_In,
    inout [39:0] IO0,
    input [2:0] IO3_In,
    inout [35:0] IO3,

    // Ethernet Interface
    output Eth_Clk,
    output Eth_CSn,
    output Eth_WEn,
    output [4:0] Eth_A,
    inout [15:0] Eth_D,
    output [1:0] Eth_BE,
    input Eth_Intn,

    // DDR SDRAM interface
    output DS_CLK_P,
    output DS_CLK_N,
    output DS_CKE,
    output [12:0] DS_A,
    output [1:0] DS_BA,
    output DS_CAS_N,
    output DS_RAS_N,
    output DS_WE_N,
    inout [1:0] DS_DQS,
    output [1:0] DS_DM,
    inout [15:0] DS_DQ
);

    //////////////////////////////////////////////////////////////////////////
    // Declare signals
    
    // Clocks and reset
    wire RST;
    wire RAMRST;
    wire RAMClk;
    wire RAMClk90;
    wire EthClk;

    // Connection state signals
    wire CtrlConnected;
    reg EnableDataConnection;
    wire DataConnected;
    reg [15:0] RxPayloadLength;
    reg [15:0] RxFrameCount;
    reg [31:0] RemoteIPAddr;
    reg [15:0] RemotePort;

    wire CtrlRxEnable;
    wire [7:0] CtrlRxData;
    wire CtrlRxBusy = 0;        // Could use this to hold off data

    // Test data signals
    reg [31:0] RampData;
    reg [15:0] TxFrameCount;
    wire [7:0] TxData;
    wire TxEnable;
    wire TxBusy;
    wire TxBufferEmpty;

    // Send packet length (could be longer if your network supports
    // jumbo frames or if you want IP fragmentation to occur)
    localparam PACKET_LENGTH = 1400;


    // Tie unused signals
    assign Flash_CSOn = 1;
    assign Flash_CLK = 1;
    assign Flash_MOSI = 1;
    assign IO0 = 40'hZ;
    assign IO3 = 36'hZ;

    // Connection management
    // The control connection is permanently open
    // It is used to enable/disable transmission on the data connection
    always @ (posedge RAMRST or posedge EthClk) begin
        if (RAMRST==1) begin
            EnableDataConnection <= 0;
            RxFrameCount <= 0;
            RxPayloadLength <= 16'hffff;
            RemoteIPAddr <= 0;
            RemotePort <= 0;
        end else begin
            if (CtrlRxEnable==1) begin
                RxFrameCount <= RxFrameCount + 1;
                if (RxFrameCount>=2 && RxFrameCount==RxPayloadLength) begin
                    RxFrameCount <= 0;
                end
                case (RxFrameCount) 
                    // Read frame header
                    16'h0000 : RxPayloadLength[15:8] <= CtrlRxData;
                    16'h0001 : RxPayloadLength[7:0] <= CtrlRxData;
                    16'h0002 : begin
                                   RemoteIPAddr[15:8] <= CtrlRxData;
                                   RxPayloadLength <= RxPayloadLength + 7;
                               end
                    16'h0003 : RemoteIPAddr[7:0] <= CtrlRxData;
                    16'h0004 : RemoteIPAddr[31:24] <= CtrlRxData;
                    16'h0005 : RemoteIPAddr[23:16] <= CtrlRxData;
                    16'h0006 : RemotePort[15:8] <= CtrlRxData;
                    16'h0007 : RemotePort[7:0] <= CtrlRxData;
                    // Frame data payload - 1 byte used as enable/disable
                    // to start/stop data transmission
                    16'h0008 : EnableDataConnection <= CtrlRxData[0];
                endcase
            end
        end
    end
    
    // Generate test ramp data
    always @ (posedge RAMRST or posedge EthClk) begin
        if (RAMRST==1) begin
            TxFrameCount <= 0;
            RampData <= 0;
        end else begin
            if (EnableDataConnection==0 || DataConnected==0) begin
                // Reset ramp generator
                TxFrameCount <= 0;
                RampData <= 0;
            end else if (TxEnable==1) begin
                // Increment ramp generator
                if (TxFrameCount==PACKET_LENGTH+1) begin
                    TxFrameCount <= 0;
                end else begin
                    TxFrameCount <= TxFrameCount+1;
                end
                if (TxFrameCount>=2 && TxFrameCount[1:0]==1) begin
                    RampData <= RampData+1;
                end
            end
        end
    end
    assign TxEnable = EnableDataConnection==1 && DataConnected==1 &&
                      TxBusy==0 ? 1 : 0;
    assign TxData = TxFrameCount==0 ? PACKET_LENGTH[15:8] :
                    TxFrameCount==1 ? PACKET_LENGTH[7:0] :
                    TxFrameCount[1:0]==2 ? RampData[31:24] :
                    TxFrameCount[1:0]==3 ? RampData[23:16] :
                    TxFrameCount[1:0]==0 ? RampData[15:8] :
                                           RampData[7:0];
                    
    //////////////////////////////////////////////////////////////////////////
    // Instantiate clocks
    // (Assumes default 125MHz reference clock)
    ZestET1_Clocks ClockInst (
        .RST(RST),
        .RefClk(CLK),
        .EthClk(EthClk),
        .RAMRST(RAMRST),
        .RAMClk(RAMClk),
        .RAMClk90(RAMClk90)
    );

    //////////////////////////////////////////////////////////////////////////
    // Ethernet interface test code
    ZestET1_EthernetWrapper #(.CLOCK_RATE(125000000))
        EthernetInst (
            .User_CLK(EthClk),
            .User_RST(RST),
            //.User_LocalIPAddr(),
            //.User_LinkStatus(),
            //.User_LinkDuplex(),
            //.User_LinkSpeed(),

            // User interface - connection 0 is control, connection 1 is data
            .User_Enable0(1'b1),
            .User_Server0(1'b1),
            .User_TCP0(1'b0),
            .User_TxPause0(1'b0),
            .User_Connected0(CtrlConnected),
            .User_MTU0(8'h2d),
            .User_TTL0(8'h80),
            .User_LocalPort0(16'h5002),
            .User_WriteRemotePort0(16'h0000),
            .User_WriteRemoteIPAddr0(32'h00000000),
            //.User_ReadRemotePort0(),
            //.User_ReadRemoteIPAddr0(),
            .User_TxData0(8'h00),
            .User_TxEnable0(1'b0),
            //.User_TxBusy0(),
            //.User_TxBufferEmpty0(),
            .User_RxData0(CtrlRxData),
            .User_RxEnable0(CtrlRxEnable),
            .User_RxBusy0(CtrlRxBusy),

            .User_Enable1(EnableDataConnection),
            .User_Server1(1'b0),
            .User_TCP1(1'b0),
            .User_TxPause1(1'b0),
            .User_Connected1(DataConnected),
            .User_MTU1(8'h2d),
            .User_TTL1(8'h80),
            .User_LocalPort1(16'h5003),
            .User_WriteRemotePort1(RemotePort),
            .User_WriteRemoteIPAddr1(RemoteIPAddr),
            //.User_ReadRemotePort1(),
            //.User_ReadRemoteIPAddr1(),
            .User_TxData1(TxData),
            .User_TxEnable1(TxEnable),
            .User_TxBusy1(TxBusy),
            .User_TxBufferEmpty1(TxBufferEmpty),
            //.User_RxData1(),
            //.User_RxEnable1(),
            .User_RxBusy1(1'b1),
            
            // Disable all other connections
            .User_Enable2(1'b0),
            .User_Enable3(1'b0),
            .User_Enable4(1'b0),
            .User_Enable5(1'b0),
            .User_Enable6(1'b0),
            .User_Enable7(1'b0),
            .User_Enable8(1'b0),
            .User_Enable9(1'b0),
            .User_Enable10(1'b0),
            .User_Enable11(1'b0),
            .User_Enable12(1'b0),
            .User_Enable13(1'b0),
            .User_Enable14(1'b0),
            .User_Enable15(1'b0),
            
            // Interface to GigExpedite
            .Eth_Clk(Eth_Clk),
            .Eth_CSn(Eth_CSn),
            .Eth_WEn(Eth_WEn),
            .Eth_A(Eth_A),
            .Eth_D(Eth_D),
            .Eth_BE(Eth_BE),
            .Eth_Intn(Eth_Intn)
        );

    //////////////////////////////////////////////////////////////////////////
    // SDRAM Buffer
    // Place holder - not used in the example but ties the pins correctly
    
    // Instantiate SDRAM component
    wire [31:0] DR;
    wire DRValid;
    wire [7:0] ValidOwner;
    wire Busy;
    wire InitDone;
    ZestET1_SDRAM SDRAMInst (
        .User_CLK(RAMClk),
        .User_CLK90(RAMClk90),
        .User_RST(RAMRST),

        .User_A(24'h000000),
        .User_RE(1'b0),
        .User_WE(1'b0),
        .User_Owner(8'h00),
        .User_BE(4'b0000),
        .User_DW(32'h00000000),
        .User_DR(DR),
        .User_DR_Valid(DRValid),
        .User_ValidOwner(ValidOwner),
        .User_Busy(Busy),
        .User_InitDone(InitDone),

        .DS_CLK_P(DS_CLK_P),
        .DS_CLK_N(DS_CLK_N),
        .DS_A(DS_A),
        .DS_BA(DS_BA),
        .DS_DQ(DS_DQ),
        .DS_DQS(DS_DQS),
        .DS_DM(DS_DM),
        .DS_CAS_N(DS_CAS_N),
        .DS_RAS_N(DS_RAS_N),
        .DS_CKE(DS_CKE),
        .DS_WE_N(DS_WE_N)
    );

endmodule

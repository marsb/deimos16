--   ZestET1 Example 7
--   File name: Example7.vhd
--   Version: 1.00
--   Date: 27/5/2011
--
--   ZestET1 Example 7 - UDP test code using wrapper IP.
--   Sends data to the Ethernet interface at maximum rate
--
--   Copyright (C) 2011 Orange Tree Technologies Ltd. All rights reserved.
--   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and 
--   modify this logic core in any form such as VHDL source code or EDIF netlist in 
--   FPGA designs that target the ZestET1.
--   Orange Tree Technologies prohibits the use of this logic core in any form such 
--   as VHDL source code or EDIF netlist in FPGA designs that target any other
--   hardware unless the purchaser of the ZestET1 has purchased the appropriate 
--   licence from Orange Tree Technologies. Contact Orange Tree Technologies if you 
--   want to purchase such a licence.
--
--  *****************************************************************************************
--  **
--  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
--  **              provided to you "as is". Orange Tree Technologies and its licensors 
--  **              make and you receive no warranties or conditions, express, implied, 
--  **              statutory or otherwise, and Orange Tree Technologies specifically 
--  **              disclaims any implied warranties of merchantability, non-infringement,
--  **              or fitness for a particular purpose. Orange Tree Technologies does not
--  **              warrant that the functions contained in these designs will meet your 
--  **              requirements, or that the operation of these designs will be 
--  **              uninterrupted or error free, or that defects in the Designs will be 
--  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
--  **              make any representations regarding use or the results of the use of the 
--  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
--  **
--  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
--  **              or its licensors be liable for any loss of data, lost profits, cost or 
--  **              procurement of substitute goods or services, or for any special, 
--  **              incidental, consequential, or indirect damages arising from the use or 
--  **              operation of the designs or accompanying documentation, however caused 
--  **              and on any theory of liability. This limitation will apply even if 
--  **              Orange Tree Technologies has been advised of the possibility of such 
--  **              damage. This limitation shall apply notwithstanding the failure of the 
--  **              essential purpose of any limited remedies herein.
--  **
--  *****************************************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity Example7 is
    port (
        -- Programmable Clock
        CLK : in std_logic;
        
        -- Flash Interface
        Flash_CSOn : out std_logic;
        Flash_CLK : out std_logic;
        Flash_MOSI : out std_logic;
        Flash_MISO : in std_logic;

        -- IO connector
        IO0_In : in std_logic;
        IO0 : inout std_logic_vector(39 downto 0);
        IO3_In : in std_logic_vector(2 downto 0);
        IO3 : inout std_logic_vector(35 downto 0);

        -- Ethernet Interface
        Eth_Clk : out std_logic;
        Eth_CSn : out std_logic;
        Eth_WEn : out std_logic;
        Eth_A : out std_logic_vector(4 downto 0);
        Eth_D : inout std_logic_vector(15 downto 0);
        Eth_BE : out std_logic_vector(1 downto 0);
        Eth_Intn : in std_logic;

        -- DDR SDRAM interface
        DS_CLK_P : out std_logic;
        DS_CLK_N : out std_logic;
        DS_CKE : out std_logic;
        DS_A : out std_logic_vector(12 downto 0);
        DS_BA : out std_logic_vector(1 downto 0);
        DS_CAS_N : out std_logic;
        DS_RAS_N : out std_logic;
        DS_WE_N : out std_logic;
        DS_DQS : inout std_logic_vector(1 downto 0);
        DS_DM : out std_logic_vector(1 downto 0);
        DS_DQ : inout std_logic_vector(15 downto 0)
    );
end Example7;

architecture arch of Example7 is

    --------------------------------------------------------------------------
    -- Declare constants
    
    -- Test transfer length
    constant WRITE_LENGTH         : std_logic_vector(31 downto 0) := conv_std_logic_vector(500*1024*1024, 32);

    --------------------------------------------------------------------------
    -- Declare signals
    
    -- Clocks and reset
    signal RST : std_logic;
    signal RAMRST : std_logic;
    signal RAMClk : std_logic;
    signal RAMClk90 : std_logic;
    signal EthClk : std_logic;

    -- Link status variables
    signal LocalIP : std_logic_vector(31 downto 0);
    signal LinkStatus : std_logic;
    signal LinkDuplex : std_logic;
    signal LinkSpeed : std_logic_vector(2 downto 0);
    signal RemoteIPAddr : std_logic_vector(31 downto 0);
    signal RemotePort : std_logic_vector(15 downto 0);
    
    -- Connection state signals
    signal CtrlConnected : std_logic;
    signal EnableDataConnection : std_logic;
    signal DataConnected : std_logic;
    signal RxPayloadLength : std_logic_vector(15 downto 0);
    signal RxFrameCount : std_logic_vector(15 downto 0);

    signal CtrlRxEnable : std_logic;
    signal CtrlRxData : std_logic_vector(7 downto 0);
    signal CtrlRxBusy : std_logic;        

    -- Test data signals
    signal RampData : std_logic_vector(31 downto 0);
    signal TxFrameCount : std_logic_vector(15 downto 0);
    signal TxData : std_logic_vector(7 downto 0);
    signal TxEnable : std_logic;
    signal TxBusy : std_logic;
    signal TxBufferEmpty : std_logic;

    -- Send packet length (could be longer if your network supports
    -- jumbo frames or if you want IP fragmentation to occur)
    constant PACKET_LENGTH : std_logic_vector(15 downto 0) := conv_std_logic_vector(1400, 16);

begin
    
    -- Tie unused signals
    Flash_CSOn <= '1';
    Flash_CLK <= '1';
    Flash_MOSI <= '1';
    IO0 <=  (others=>'Z');
    IO3 <= (others=>'Z');
    
    -- Reset controller
    ROCInst : ROC port map (O=>RST);

    --------------------------------------------------------------------------
    -- Main application code

    -- Connection management
    -- The control connection is permanently open
    -- It is used to enable/disable transmission on the data connection
    process (RAMRST, EthClk)
    begin
        if (RAMRST='1') then
            EnableDataConnection <= '0';
            RxFrameCount <= (others=>'0');
            RxPayloadLength <= (others=>'1');
            RemoteIPAddr <= (others=>'0');
            RemotePort <= (others=>'0');
        elsif (EthClk'event and EthClk='1') then
            if (CtrlRxEnable='1') then
                RxFrameCount <= RxFrameCount + 1;
                if (RxFrameCount>=X"0002" and RxFrameCount=RxPayloadLength) then
                    RxFrameCount <= (others=>'0');
                end if;
                case RxFrameCount is
                    -- Read frame header
                    when X"0000" => RxPayloadLength(15 downto 8) <= CtrlRxData;
                    when X"0001" => RxPayloadLength(7 downto 0) <= CtrlRxData;
                    when X"0002" => RemoteIPAddr(15 downto 8) <= CtrlRxData;
                                    RxPayloadLength <= RxPayloadLength + 7;
                    when X"0003" => RemoteIPAddr(7 downto 0) <= CtrlRxData;
                    when X"0004" => RemoteIPAddr(31 downto 24) <= CtrlRxData;
                    when X"0005" => RemoteIPAddr(23 downto 16) <= CtrlRxData;
                    when X"0006" => RemotePort(15 downto 8) <= CtrlRxData;
                    when X"0007" => RemotePort(7 downto 0) <= CtrlRxData;
                    -- Frame data payload - 1 byte used as enable/disable
                    -- to start/stop data transmission
                    when X"0008" => EnableDataConnection <= CtrlRxData(0);
                    when others => null;
                end case;
            end if;
        end if;
    end process;
    CtrlRxBusy <= '0';  -- Could use this to hold off data
    
    -- Generate test ramp data
    process (RAMRST, EthClk)
    begin
        if (RAMRST='1') then
            TxFrameCount <= (others=>'0');
            RampData <= (others=>'0');
        elsif (EthClk'event and EthClk='1') then
            if (EnableDataConnection='0' or DataConnected='0') then
                -- Reset ramp generator
                TxFrameCount <= (others=>'0');
                RampData <= (others=>'0');
            elsif (TxEnable='1') then
                -- Increment ramp generator
                if (TxFrameCount=PACKET_LENGTH+1) then
                    TxFrameCount <= (others=>'0');
                else
                    TxFrameCount <= TxFrameCount+1;
                end if;
                if (TxFrameCount>=X"0002" and TxFrameCount(1 downto 0)="01") then
                    RampData <= RampData+1;
                end if;
            end if;
        end if;
    end process;
    TxEnable <= EnableDataConnection and DataConnected and not TxBusy;
    TxData <= PACKET_LENGTH(15 downto 8) when TxFrameCount=X"0000" else
              PACKET_LENGTH(7 downto 0) when TxFrameCount=X"0001" else
              RampData(31 downto 24) when TxFrameCount(1 downto 0)="10" else
              RampData(23 downto 16) when TxFrameCount(1 downto 0)="11" else
              RampData(15 downto 8) when TxFrameCount(1 downto 0)="00" else
              RampData(7 downto 0);

    --------------------------------------------------------------------------
    -- Instantiate clocks
    -- (Assumes default 125MHz reference clock)
    ClockInst : entity work.ZestET1_Clocks
        port map (
            RST => RST,
            RefClk => CLK,
            EthClk => EthClk,
            RAMRST => RAMRST,
            RAMClk => RAMClk,
            RAMClk90 => RAMClk90    
        );

    --------------------------------------------------------------------------
    -- Ethernet interface wrapper instantiation
    EthernetInst : entity work.ZestET1_EthernetWrapper 
        generic map (
            CLOCK_RATE => 125000000
        )
        port map (
            User_CLK => EthClk,
            User_RST => RST,
            User_LocalIPAddr => LocalIP,
            User_LinkStatus => LinkStatus,
            User_LinkDuplex => LinkDuplex,
            User_LinkSpeed => LinkSpeed,

            -- User interface - connection 0 is control, connection 1 is data
            User_Enable0 => '1',
            User_Server0 => '1',
            User_TCP0 => '0',
            User_TxPause0 => '0',
            User_Connected0 => CtrlConnected,
            User_MTU0 => X"2d",
            User_TTL0 => X"80",
            User_LocalPort0 => X"5002",
            User_WriteRemotePort0 => X"0000",
            User_WriteRemoteIPAddr0 => X"00000000",
            User_ReadRemotePort0 => open,
            User_ReadRemoteIPAddr0 => open,
            User_TxData0 => X"00",
            User_TxEnable0 => '0',
            User_TxBusy0 => open,
            User_TxBufferEmpty0 => open,
            User_RxData0 => CtrlRxData,
            User_RxEnable0 => CtrlRxEnable,
            User_RxBusy0 => CtrlRxBusy,

            User_Enable1 => EnableDataConnection,
            User_Server1 => '0',
            User_TCP1 => '0',
            User_TxPause1 => '0',
            User_Connected1 => DataConnected,
            User_MTU1 => X"2d", -- Could increase this for Jumbo frames
            User_TTL1 => X"80",
            User_LocalPort1 => X"5003",
            User_WriteRemotePort1 => RemotePort,
            User_WriteRemoteIPAddr1 => RemoteIPAddr,
            User_ReadRemotePort1 => open,
            User_ReadRemoteIPAddr1 => open,
            User_TxData1 => TxData,
            User_TxEnable1 => TxEnable,
            User_TxBusy1 => TxBusy,
            User_TxBufferEmpty1 => TxBufferEmpty,
            User_RxData1 => open,
            User_RxEnable1 => open,
            User_RxBusy1 => '1',
            
            -- Disable all other connections
            User_Enable2 => '0',
            User_Enable3 => '0',
            User_Enable4 => '0',
            User_Enable5 => '0',
            User_Enable6 => '0',
            User_Enable7 => '0',
            User_Enable8 => '0',
            User_Enable9 => '0',
            User_Enable10 => '0',
            User_Enable11 => '0',
            User_Enable12 => '0',
            User_Enable13 => '0',
            User_Enable14 => '0',
            User_Enable15 => '0',
            
            -- Interface to GigExpedite
            Eth_Clk => Eth_Clk,
            Eth_CSn => Eth_CSn,
            Eth_WEn => Eth_WEn,
            Eth_A => Eth_A,
            Eth_D => Eth_D,
            Eth_BE => Eth_BE,
            Eth_Intn => Eth_Intn
        );

    --------------------------------------------------------------------------
    -- SDRAM Buffer
    -- Place holder - not used in the example but ties the pins correctly
    
    -- Instantiate SDRAM component
    SDRAMInst : entity work.ZestET1_SDRAM
        generic map (
            CLOCK_RATE => 166666666
        )
        port map (
            User_CLK => RAMClk,
            User_CLK90 => RAMClk90,
            User_RST => RAMRST,

            User_A => (others=>'0'),
            User_RE => '0',
            User_WE => '0',
            User_Owner => X"00",
            User_BE => (others=>'0'),
            User_DW => (others=>'0'),
            --User_DR => ,
            --User_DR_Valid => ,
            --User_ValidOwner => ,
            --User_Busy => ,
            --User_InitDone => ,

            DS_CLK_P => DS_CLK_P,
            DS_CLK_N => DS_CLK_N,
            DS_A => DS_A,
            DS_BA => DS_BA,
            DS_DQ => DS_DQ,
            DS_DQS => DS_DQS,
            DS_DM => DS_DM,
            DS_CAS_N => DS_CAS_N,
            DS_RAS_N => DS_RAS_N,
            DS_CKE => DS_CKE,
            DS_WE_N => DS_WE_N
        );
    
end arch;


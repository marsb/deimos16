--
-- FIFO.vhd
--
-- Asynchronous FIFO using distributed RAM for storage
--

------------------------------------------------------------------------------
-- Grey code and decode functions

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

package FIFOGreyCode is
    function GreyCode (Input : std_logic_vector) return std_logic_vector;
    function GreyDecode (Input : std_logic_vector) return std_logic_vector;
end FIFOGreyCode;

package body FIFOGreyCode is

    -- Declare functions
    function GreyCode (Input : std_logic_vector) return std_logic_vector is
        variable Result : std_logic_vector(Input'high downto 0);
    begin
        Result(Input'high) := Input(Input'high);
        for i in Input'high downto 1 loop
            Result(i-1) := Input(i) xor Input(i-1);
        end loop;
        return Result;
    end GreyCode;

    function GreyDecode (Input : std_logic_vector) return std_logic_vector is
        variable Result : std_logic_vector(Input'high downto 0);
        variable LastBit : std_logic;
    begin
        LastBit := '0';
        for i in Input'high downto 0 loop
            LastBit := LastBit xor Input(i);
            Result(i) := LastBit;
        end loop;
        return Result;
    end GreyDecode;

end FIFOGreyCode;


------------------------------------------------------------------------------
-- Distributed RAM FIFO with fallthrough
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

use sdram_v1_00_a.FIFOGreyCode.all;

entity DistFIFO is
    generic
    (
        FIFO_WIDTH : integer := 8;
        FIFO_LOG_DEPTH : integer := 4
    );
    port (
        rst: in std_logic;
        wr_clk: in std_logic;
        wr_en: in std_logic;
        din: in std_logic_vector(FIFO_WIDTH-1 downto 0);
        rd_clk: in std_logic;
        rd_en: in std_logic;
        dout: out std_logic_vector(FIFO_WIDTH-1 downto 0);
        empty: out std_logic;
        full: out std_logic;
        wr_data_count: out std_logic_vector(FIFO_LOG_DEPTH-1 downto 0);
        rd_data_count: out std_logic_vector(FIFO_LOG_DEPTH-1 downto 0)
    );
end DistFIFO;

architecture arch of DistFIFO is

    -- Declare signals
    attribute ram_style : string;
    type DIST_RAM is array(0 to 2**FIFO_LOG_DEPTH-1) of std_logic_vector(FIFO_WIDTH-1 downto 0);
    signal StorageArray : DIST_RAM;
    attribute ram_style of StorageArray : signal is "distributed";
    signal StorageArrayVal : std_logic_vector(FIFO_WIDTH-1 downto 0);
    
    signal WriteCount : std_logic_vector(FIFO_LOG_DEPTH-1 downto 0);
    signal NextWriteCount : std_logic_vector(FIFO_LOG_DEPTH-1 downto 0);
    signal ReadCount : std_logic_vector(FIFO_LOG_DEPTH-1 downto 0);
    signal NextReadCount : std_logic_vector(FIFO_LOG_DEPTH-1 downto 0);
    
    signal WriteCountGreyCoded : std_logic_vector(FIFO_LOG_DEPTH-1 downto 0);
    signal WriteCountGreyInReadDomain : std_logic_vector(FIFO_LOG_DEPTH-1 downto 0);
    signal WriteCountInReadDomain : std_logic_vector(FIFO_LOG_DEPTH-1 downto 0);
    
    signal ReadCountGreyCoded : std_logic_vector(FIFO_LOG_DEPTH-1 downto 0);
    signal ReadCountGreyInWriteDomain : std_logic_vector(FIFO_LOG_DEPTH-1 downto 0);
    signal ReadCountInWriteDomain : std_logic_vector(FIFO_LOG_DEPTH-1 downto 0);

    attribute tig: string; 
    attribute tig of WriteCountGreyCoded : signal is "TRUE"; 
    attribute tig of ReadCountGreyCoded : signal is "TRUE"; 
    attribute tig of StorageArrayVal : signal is "TRUE"; 

begin

    -- Write storage array such that it can be inferred as a ram
    process (wr_clk)
    begin
        if (wr_clk'event and wr_clk='1') then
            StorageArray(conv_integer(WriteCount)) <= din;
        end if;
    end process;
    StorageArrayVal <= StorageArray(conv_integer(ReadCount));
    dout <= StorageArrayVal;
    
    -- Manage read and write counters
    process (rst, wr_clk)
    begin
        if (rst='1') then
            WriteCount <= (others=>'0');
            WriteCountGreyCoded <= (others=>'0');
            ReadCountGreyInWriteDomain <= (others=>'0');
            full <= '0';
        elsif (wr_clk'event and wr_clk='1') then
            if (wr_en='1') then
                WriteCount <= NextWriteCount;
                WriteCountGreyCoded <= GreyCode(NextWriteCount);
                if (NextWriteCount=ReadCountInWriteDomain-1) then
                    full <= '1';
                else
                    full <= '0';
                end if;
            else
                if (WriteCount=ReadCountInWriteDomain-1) then
                    full <= '1';
                else
                    full <= '0';
                end if;
            end if;
            ReadCountGreyInWriteDomain <= ReadCountGreyCoded;
        end if;
    end process;
    NextWriteCount <= WriteCount + 1;
    
    process (rst, rd_clk)
    begin
        if (rst='1') then
            ReadCount <= (others=>'0');
            ReadCountGreyCoded <= (others=>'0');
            WriteCountGreyInReadDomain <= (others=>'0');
            empty <= '1';
        elsif (rd_clk'event and rd_clk='1') then
            if (rd_en='1') then
                ReadCount <= NextReadCount;
                ReadCountGreyCoded <= GreyCode(NextReadCount);
                if (NextReadCount=WriteCountInReadDomain) then
                    empty <= '1';
                else
                    empty <= '0';
                end if;
            else
                if (ReadCount=WriteCountInReadDomain) then
                    empty <= '1';
                else
                    empty <= '0';
                end if;
            end if;
            WriteCountGreyInReadDomain <= WriteCountGreyCoded;
        end if;
    end process;
    NextReadCount <= ReadCount + 1;
    
    -- Calculate flags and read/write counts
    WriteCountInReadDomain <= GreyDecode(WriteCountGreyInReadDomain);
    ReadCountInWriteDomain <= GreyDecode(ReadCountGreyInWriteDomain);

--    empty <= '1' when ReadCount=WriteCountInReadDomain else '0';
--    full <= '1' when WriteCount=ReadCountInWriteDomain-1 else '0';
    
    rd_data_count <= WriteCountInReadDomain-ReadCount;
    wr_data_count <= WriteCount-ReadCountInWriteDomain;
    
end arch;

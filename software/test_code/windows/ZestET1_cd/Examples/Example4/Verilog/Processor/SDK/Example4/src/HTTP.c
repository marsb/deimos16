//////////////////////////////////////////////////////////////////////
//
// File:      HTTP.c
//
// Purpose:
//    ZestET1 Example Programs
//    Very simple multi-threaded web server.
//
// Copyright (c) 2011 Orange Tree Technologies.
// May not be reproduced without permission.
//
//////////////////////////////////////////////////////////////////////

#include <string.h>
#include <malloc.h>
#include <stdio.h>
#include <xparameters.h>
#include <pthread.h>
#include "socket.h"

// Port to listen on
// Choose something other than the default port 80 used for
// the GigExpedite configuration interface.
// Alternatively, change the configuration interface port.
// To display the example web page, browse to:
//		http://192.168.1.100:8080
// or similar depending on current GigExpedite IP address.
#define SERVER_PORT 8080

// Length of incoming data buffer and thread stack
#define HTTP_BUFFER_SIZE 300000
#define HTTP_STACKSIZE 32768

// File not found error
static char *HTTP_Error404 =
"HTTP/1.1 404 Not found"
"\r\n\r\n";

// Normal response string
// This should be replaced by an HTML page
static char *IndexPage =
"HTTP/1.1 200 OK\r\n"
"Content-type: text/html\r\n"
"Content-length: 255\r\n"
"Connection: Keep-Alive\r\n\r\n"
"<HTML>\r\n"
"<HEAD>\r\n"
"<META HTTP-EQUIV=\"Expires\" CONTENT=\"Tue, 01 Jan 1980 1:00:00 GMT\">"
"</HEAD>\r\n"
"<BODY>\r\n"
"ZestET1 Example4 Web Server\r\n"
"</BODY>\r\n"
"</HTML>\r\n";


//
// Socket service routine
//
static void HTTP_ServiceSocket(SOCKET s)
{
    char *Ptr = NULL;
    char *HTTP_Buffer = malloc(HTTP_BUFFER_SIZE);
    int HTTP_BufferPtr = 0;

    // Receive a complte message from browser
    do
    {
        if (recv(s, HTTP_Buffer+HTTP_BufferPtr, 1, 0)!=1)
            break;
        HTTP_BufferPtr++;
    } while (HTTP_BufferPtr<4 || HTTP_Buffer[HTTP_BufferPtr-4]!='\r' ||
             HTTP_Buffer[HTTP_BufferPtr-3]!='\n' ||
             HTTP_Buffer[HTTP_BufferPtr-2]!='\r' ||
             HTTP_Buffer[HTTP_BufferPtr-1]!='\n');

    // Determine what message type is is
    if (strncmp(HTTP_Buffer, "GET ", 4) == 0)
        Ptr = (char *)HTTP_Buffer + 4;
    else if (strncmp(HTTP_Buffer, "HEAD ", 4) == 0)
        Ptr = (char *)HTTP_Buffer + 5;     // Should only return the header but this is fine
    else if (strncmp(HTTP_Buffer, "POST ", 5) == 0)
    {
        // Could handle POST body here
    	// The various options can be extracted from the HTTP_Buffer string
    	// to update server state here

        Ptr = (char *)HTTP_Buffer + 5;
    }
    if (Ptr!=NULL)
    {
        char *FilePtr = NULL;
        int FileSize;
        int i;

        // Find the end of the header
        for (i=0; ; i++)
        {
            if (Ptr[i] == ' ' || Ptr[i] == '\r' || Ptr[i] == '\n')
            {
                Ptr[i] = 0;
                break;
            }
        }

        // Find the requested file name
        // For a more sophisticated site this is where the
        // requested file is located ready for returning
        if (Ptr[0]=='/' && (Ptr[1]==0 || stricmp("index.html", Ptr+1)==0))
        {
        	// Request for index home page
			FilePtr = IndexPage;
        }
        else
        {
            // File not found - return 404 error
            FilePtr = HTTP_Error404;
        }
    	FileSize = strlen(FilePtr)+1;

    	// Send response
        send(s, FilePtr, FileSize, 0);
    }
    free(HTTP_Buffer);
}


//
// Main HTTP service loop
//
static void *HTTP_Loop(void *Arg)
{
	SOCKET s;
	struct sockaddr_in ServerAddr;
	struct sockaddr_in ClientAddr;
	int ClientAddrLen;

	// Initialise local address/port
	memset(&ServerAddr, 0, sizeof(ServerAddr));
    ServerAddr.sin_family      = AF_INET;
    ServerAddr.sin_addr.s_addr = htonl(INADDR_ANY);
    ServerAddr.sin_port        = htons(SERVER_PORT);

	while(1)
	{
		// Create socket
		s = socket(AF_INET, SOCK_DGRAM, IPPROTO_TCP);

		// Bind to local address/port
	    bind(s, (struct sockaddr *)&ServerAddr, sizeof(ServerAddr));

		// Listen on connection
		listen(s, LISTENQ);

		// Wait for connection
		ClientAddrLen = sizeof(ClientAddr);
		s = accept(s, (struct sockaddr *)&ClientAddr, &ClientAddrLen);

		// Service HTTP request
		HTTP_ServiceSocket(s);

		// Close socket
		close(s);
	}

    return NULL;
}


//////////////////////////////////////////////////////////////////////////////
// External API
//
// Main web server loop
//
void HTTPInit(void)
{
	char *HTTP_Stack[4];
    pthread_t Thread;
    pthread_attr_t Attr;
    struct sched_param SchedPar;
    int i;

    // Launch 4 server threads to allow 4 HTTP connections
    // A minimum of 2 threads are required to allow browsers to
    // access two file simultaneously.
    for (i=0; i<4; i++)
    {
    	HTTP_Stack[i] = malloc(HTTP_STACKSIZE+4);
    	HTTP_Stack[i] = (char *)((((unsigned long)HTTP_Stack[i])+3)&~3);

		SchedPar.sched_priority = 1;    // Thread priority
		pthread_attr_init(&Attr);
		pthread_attr_setschedparam(&Attr, &SchedPar);
		pthread_attr_setstack(&Attr, HTTP_Stack[i], HTTP_STACKSIZE);
		pthread_create(&Thread, &Attr, HTTP_Loop, NULL);
    }
}

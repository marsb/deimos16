------------------------------------------------------------------------------
-- user_logic.vhd - entity/architecture pair
------------------------------------------------------------------------------
--
-- ***************************************************************************
-- ** Copyright (c) 1995-2008 Xilinx, Inc.  All rights reserved.            **
-- **                                                                       **
-- ** Xilinx, Inc.                                                          **
-- ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
-- ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
-- ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
-- ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
-- ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
-- ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
-- ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
-- ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
-- ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
-- ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
-- ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
-- ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
-- ** FOR A PARTICULAR PURPOSE.                                             **
-- **                                                                       **
-- ***************************************************************************
--
------------------------------------------------------------------------------
-- Filename:          user_logic.vhd
-- Version:           1.00.a
-- Description:       User logic.
-- Date:              Tue Oct 20 15:29:52 2009 (by Create and Import Peripheral Wizard)
-- VHDL Standard:     VHDL'93
------------------------------------------------------------------------------
-- Naming Conventions:
--   active low signals:                    "*_n"
--   clock signals:                         "clk", "clk_div#", "clk_#x"
--   reset signals:                         "rst", "rst_n"
--   generics:                              "C_*"
--   user defined types:                    "*_TYPE"
--   state machine next state:              "*_ns"
--   state machine current state:           "*_cs"
--   combinatorial signals:                 "*_com"
--   pipelined or register delay signals:   "*_d#"
--   counter signals:                       "*cnt*"
--   clock enable signals:                  "*_ce"
--   internal version of output port:       "*_i"
--   device pins:                           "*_pin"
--   ports:                                 "- Names begin with Uppercase"
--   processes:                             "*_PROCESS"
--   component instantiations:              "<ENTITY_>I_<#|FUNC>"
------------------------------------------------------------------------------

-- DO NOT EDIT BELOW THIS LINE --------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library proc_common_v3_00_a;
use proc_common_v3_00_a.proc_common_pkg.all;

-- DO NOT EDIT ABOVE THIS LINE --------------------

--USER libraries added here

------------------------------------------------------------------------------
-- Entity section
------------------------------------------------------------------------------
-- Definition of Generics:
--   C_SLV_AWIDTH                 -- Slave interface address bus width
--   C_SLV_DWIDTH                 -- Slave interface data bus width
--   C_NUM_MEM                    -- Number of memory spaces
--
-- Definition of Ports:
--   Bus2IP_Clk                   -- Bus to IP clock
--   Bus2IP_Reset                 -- Bus to IP reset
--   Bus2IP_Addr                  -- Bus to IP address bus
--   Bus2IP_CS                    -- Bus to IP chip select for user logic memory selection
--   Bus2IP_RNW                   -- Bus to IP read/not write
--   Bus2IP_Data                  -- Bus to IP data bus
--   Bus2IP_BE                    -- Bus to IP byte enables
--   IP2Bus_Data                  -- IP to Bus data bus
--   IP2Bus_RdAck                 -- IP to Bus read transfer acknowledgement
--   IP2Bus_WrAck                 -- IP to Bus write transfer acknowledgement
--   IP2Bus_Error                 -- IP to Bus error response
------------------------------------------------------------------------------

entity user_logic is
  generic
  (
    -- ADD USER GENERICS BELOW THIS LINE ---------------
    --USER generics added here
    -- ADD USER GENERICS ABOVE THIS LINE ---------------

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol parameters, do not add to or delete
    C_SLV_AWIDTH                   : integer              := 32;
    C_SLV_DWIDTH                   : integer              := 32;
    C_NUM_MEM                      : integer              := 1
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );
  port
  (
    -- ADD USER PORTS BELOW THIS LINE ------------------
    RAMClk                 : in std_logic;
    WE                     : out std_logic;
    RE                     : out std_logic;
    Owner                  : out std_logic_vector(7 downto 0);
    Addr                   : out std_logic_vector(23 downto 0);
    BE                     : out std_logic_vector(3 downto 0);
    WriteData              : out std_logic_vector(31 downto 0);
    ReadData               : in std_logic_vector(31 downto 0);
    ReadDataValid          : in std_logic;
    ValidOwner             : in std_logic_vector(7 downto 0);
    Busy                   : in std_logic;
    -- ADD USER PORTS ABOVE THIS LINE ------------------

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol ports, do not add to or delete
    Bus2IP_Clk                     : in  std_logic;
    Bus2IP_Reset                   : in  std_logic;
    Bus2IP_Addr                    : in  std_logic_vector(0 to C_SLV_AWIDTH-1);
    Bus2IP_CS                      : in  std_logic_vector(0 to C_NUM_MEM-1);
    Bus2IP_RNW                     : in  std_logic;
    Bus2IP_Data                    : in  std_logic_vector(0 to C_SLV_DWIDTH-1);
    Bus2IP_BE                      : in  std_logic_vector(0 to C_SLV_DWIDTH/8-1);
    IP2Bus_Data                    : out std_logic_vector(0 to C_SLV_DWIDTH-1);
    IP2Bus_RdAck                   : out std_logic;
    IP2Bus_WrAck                   : out std_logic;
    IP2Bus_Error                   : out std_logic;

    FSL0_M_Clk                     : in std_logic;
    FSL0_M_Write                   : in std_logic;
    FSL0_M_Data                    : in std_logic_vector(0 to 31);
    FSL0_M_Control                 : in std_logic;
    FSL0_M_Full                    : out std_logic;
    FSL0_S_Clk                     : in std_logic;
    FSL0_S_Read                    : in std_logic;
    FSL0_S_Data                    : out std_logic_vector(0 to 31);
    FSL0_S_Control                 : out std_logic;
    FSL0_S_Exists                  : out std_logic;
    FSL1_M_Clk                     : in std_logic;
    FSL1_M_Write                   : in std_logic;
    FSL1_M_Data                    : in std_logic_vector(0 to 31);
    FSL1_M_Control                 : in std_logic;
    FSL1_M_Full                    : out std_logic;
    FSL1_S_Clk                     : in std_logic;
    FSL1_S_Read                    : in std_logic;
    FSL1_S_Data                    : out std_logic_vector(0 to 31);
    FSL1_S_Control                 : out std_logic;
    FSL1_S_Exists                  : out std_logic
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );

  attribute SIGIS : string;
  attribute SIGIS of Bus2IP_Clk    : signal is "CLK";
  attribute SIGIS of Bus2IP_Reset  : signal is "RST";

end entity user_logic;

------------------------------------------------------------------------------
-- Architecture section
------------------------------------------------------------------------------

architecture IMP of user_logic is

    -- PLB bus signals
    signal LastRE : std_logic;
    signal ReqWE : std_logic;
    signal ReqRE : std_logic;
    signal ReqDataIn : std_logic_vector(60 downto 0);
    signal ReqDataOut : std_logic_vector(60 downto 0);
    signal ReqEmpty : std_logic;
    signal RespWE : std_logic;
    signal RespRE : std_logic;
    signal RespDataIn : std_logic_vector(31 downto 0);
    signal RespDataOut : std_logic_vector(31 downto 0);
    signal RespEmpty : std_logic;
    signal PLBRE : std_logic;
    signal PLBWE : std_logic;
    signal PLBAddr : std_logic_vector(23 downto 0);
    signal PLBBE : std_logic_vector(3 downto 0);
    signal PLBWriteData : std_logic_vector(31 downto 0);

    -- Cache signals
    signal DXCLReq : std_logic;
    signal DXCLArb : std_logic;
    signal DXCLReadDataValid : std_logic;
    signal DXCLRE : std_logic;
    signal DXCLWE : std_logic;
    signal DXCLAddr : std_logic_vector(23 downto 0);
    signal DXCLBE : std_logic_vector(3 downto 0);
    signal DXCLWriteData : std_logic_vector(31 downto 0);

    signal IXCLReq : std_logic;
    signal IXCLArb : std_logic;
    signal IXCLReadDataValid : std_logic;
    signal IXCLRE : std_logic;
    signal IXCLWE : std_logic;
    signal IXCLAddr : std_logic_vector(23 downto 0);
    signal IXCLBE : std_logic_vector(3 downto 0);
    signal IXCLWriteData : std_logic_vector(31 downto 0);

    -- Arbiter and multiplexer signals
    signal Arb : std_logic_vector(1 downto 0);
    signal MuxWE : std_logic;
    signal MuxDataIn : std_logic_vector(69 downto 0);
    signal MuxRE : std_logic;
    signal MuxDataOut : std_logic_vector(69 downto 0);
    signal MuxEmpty : std_logic;
    signal MuxFull : std_logic;

begin

    --------------------------------------------------------------------------
    -- PLB bus interface
    process (Bus2IP_Reset, Bus2IP_Clk) 
    begin
        if (Bus2IP_Reset='1') then
            LastRE <= '0';
        elsif (Bus2IP_Clk'event and Bus2IP_Clk='1') then
            if (RespRE='1') then
                LastRE <= '0';
            elsif (Bus2IP_CS(0)='1' and Bus2IP_RNW='1' and LastRE='0') then
                LastRE <= '1';
            end if;
        end if;
    end process;

    IP2Bus_Data            <= RespDataOut;
    IP2Bus_WrAck           <= Bus2IP_CS(0) and not Bus2IP_RNW;
    IP2Bus_RdAck           <= Bus2IP_CS(0) and Bus2IP_RNW and RespRE;
    IP2Bus_Error           <= '0';

    -- Cross clock domain FIFOs to bridge PLB to RAM clock domain
    ReqWE <= Bus2IP_CS(0) and ((not Bus2IP_RNW) or (Bus2IP_RNW and not LastRE));
    ReqRE <= Arb(1) and not MuxFull and not ReqEmpty;
    ReqDataIn <= Bus2IP_RNW &                                      -- RnW
                 Bus2IP_BE(C_SLV_DWIDTH/8-4 to C_SLV_DWIDTH/8-1) & -- BE
                 Bus2IP_Addr(C_SLV_AWIDTH-26 to C_SLV_AWIDTH-3) &  -- Address
                 Bus2IP_Data(C_SLV_DWIDTH-32 to C_SLV_DWIDTH-1);   -- Write data
               
    RespWE <= ReadDataValid and ValidOwner(1);
    RespRE <= not RespEmpty;
    RespDataIn <= ReadData;

    PLBWE        <= not ReqDataOut(60) and ReqRE;
    PLBRE        <= ReqDataOut(60) and ReqRE;
    PLBAddr      <= ReqDataOut(55 downto 32);
    PLBBE        <= ReqDataOut(59 downto 56);
    PLBWriteData <= ReqDataOut(31 downto 0);
  
    ReqFIFOInst : entity sdram_v1_00_a.DistFIFO 
        generic map (
            FIFO_WIDTH => 61
        )
        port map (
            rst => Bus2IP_Reset,
            wr_clk => Bus2IP_Clk,
            wr_en => ReqWE,
            din => ReqDataIn,
            rd_clk => RAMClk,
            rd_en => ReqRE,
            dout => ReqDataOut,
            empty => ReqEmpty,
            full => open,
            wr_data_count => open,
            rd_data_count => open
        );
    RespFIFOInst : entity sdram_v1_00_a.DistFIFO 
        generic map (
            FIFO_WIDTH => 32
        )
        port map (
            rst => Bus2IP_Reset,
            wr_clk => RAMClk,
            wr_en => RespWE,
            din => RespDataIn,
            rd_clk => Bus2IP_Clk,
            rd_en => RespRE,
            dout => RespDataOut,
            empty => RespEmpty,
            full => open,
            wr_data_count => open,
            rd_data_count => open
        );

    --------------------------------------------------------------------------
    -- Data cache accesses
    DXCLArb <= '1' when Arb="00" and MuxFull='0' else '0';
    DXCLReadDataValid <= '1' when ValidOwner="00" and ReadDataValid='1' else '0';
    DXCLInst : entity sdram_v1_00_a.xcl
        port map (
            RST => Bus2IP_Reset,
            FSL_M_Clk => FSL0_M_Clk,
            FSL_M_Write => FSL0_M_Write,
            FSL_M_Data => FSL0_M_Data,
            FSL_M_Control => FSL0_M_Control,
            FSL_M_Full => FSL0_M_Full,
            FSL_S_Clk => FSL0_S_Clk,
            FSL_S_Read => FSL0_S_Read,
            FSL_S_Data => FSL0_S_Data,
            FSL_S_Control => FSL0_S_Control,
            FSL_S_Exists => FSL0_S_Exists,
            
            RAMClk => RAMClk,
            ArbCS => DXCLArb,
            Req => DXCLReq,
            RE => DXCLRE,
            WE => DXCLWE,
            Addr => DXCLAddr,
            BE => DXCLBE,
            WriteData => DXCLWriteData,
            ReadData => ReadData,
            ReadDataValid => DXCLReadDataValid
        );

    --------------------------------------------------------------------------
    -- Instruction cache accesses
    IXCLArb <= '1' when Arb="01" and MuxFull='0' else '0';
    IXCLReadDataValid <= '1' when ValidOwner="01" and ReadDataValid='1' else '0';
    IXCLInst : entity sdram_v1_00_a.xcl
        port map (
            RST => Bus2IP_Reset,
            FSL_M_Clk => FSL1_M_Clk,
            FSL_M_Write => FSL1_M_Write,
            FSL_M_Data => FSL1_M_Data,
            FSL_M_Control => FSL1_M_Control,
            FSL_M_Full => FSL1_M_Full,
            FSL_S_Clk => FSL1_S_Clk,
            FSL_S_Read => FSL1_S_Read,
            FSL_S_Data => FSL1_S_Data,
            FSL_S_Control => FSL1_S_Control,
            FSL_S_Exists => FSL1_S_Exists,
            
            RAMClk => RAMClk,
            ArbCS => IXCLArb,
            Req => IXCLReq,
            RE => IXCLRE,
            WE => IXCLWE,
            Addr => IXCLAddr,
            BE => IXCLBE,
            WriteData => IXCLWriteData,
            ReadData => ReadData,
            ReadDataValid => IXCLReadDataValid
        );

    --------------------------------------------------------------------------
    -- Arbiter
    -- Very simple cycle sharing arbiter
    process (Bus2IP_Reset, RAMClk)
    begin
        if (Bus2IP_Reset='1') then
            Arb <= "00";
        elsif (RAMClk'event and RAMClk='1') then
            if (Arb="10") then
                Arb <= "00";
            else
                Arb <= Arb + 1;
            end if;
        end if;
    end process;

    -- Multiplex connections to RAM controller
    -- Use an additional FIFO here to improve timing
    MuxDataIn(69 downto 62) <= "000000" & Arb; -- Owner
    MuxDataIn(61) <= PLBWE when Arb(1)='1' else DXCLWE when Arb(0)='0' else IXCLWE;
    MuxDataIn(60) <= PLBRE when Arb(1)='1' else DXCLRE when Arb(0)='0' else IXCLRE;
    MuxDataIn(59 downto 56) <= PLBBE when Arb(1)='1' else DXCLBE when Arb(0)='0' else IXCLBE;
    MuxDataIn(55 downto 32) <= PLBAddr when Arb(1)='1' else DXCLAddr when Arb(0)='0' else IXCLAddr;
    MuxDataIn(31 downto 0) <= PLBWriteData when Arb(1)='1' else DXCLWriteData when Arb(0)='0' else IXCLWriteData;
    MuxWE <= MuxDataIn(61) or MuxDataIn(60);
    
    MuxFIFOInst : entity sdram_v1_00_a.DistFIFO 
        generic map (
            FIFO_WIDTH => 70
        )
        port map (
            rst => Bus2IP_Reset,
            wr_clk => RAMClk,
            wr_en => MuxWE,
            din => MuxDataIn,
            rd_clk => RAMClk,
            rd_en => MuxRE,
            dout => MuxDataOut,
            empty => MuxEmpty,
            full => MuxFull,
            wr_data_count => open,
            rd_data_count => open
        );

    Owner <= MuxDataOut(69 downto 62);
    WE <= not MuxEmpty and not Busy and MuxDataOut(61);
    RE <= not MuxEmpty and not Busy and MuxDataOut(60);
    BE <= MuxDataOut(59 downto 56);
    Addr <= MuxDataOut(55 downto 32);
    WriteData <= MuxDataOut(31 downto 0);
    MuxRE <= not MuxEmpty and not Busy;

end IMP;

//////////////////////////////////////////////////////////////////////
//
// File:      Example4.c
//
// Purpose:
//    ZestET1 Example Programs
//    Example Microblaze program.  Very simple web server.
//  
// Copyright (c) 2011 Orange Tree Technologies.
// May not be reproduced without permission.
//
//////////////////////////////////////////////////////////////////////

#include <xintc.h>
#include <xmk.h>
#include <mb_interface.h>
#include <string.h>
#include "socket.h"
#include "HTTP.h"

// Interrupt controller state
XIntc IntController;

int main(int argc, char **argv)
{
	// Enable caches
	microblaze_enable_icache();
	microblaze_enable_dcache();

	// Initialise interrupts
    XIntc_Initialize(&IntController, XPAR_INTC_0_DEVICE_ID);
    XIntc_Start(&IntController, XIN_REAL_MODE);

    microblaze_enable_interrupts();

	xilkernel_main();

	return 0;
}
void MainThread(void *Arg)
{
    // Open socket library to connect ISR and enable interrupts
    socket_init();

    // Enable interrupts
    XIntc_Enable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);

    // Enter main control loop
    HTTPInit();

    while(1);
}


//////////////////////////////////////////////////////////////////////
//
// File:      Socket.c
//
// Purpose:
//    ZestET1 Example Programs
//    Socket layer to interface to GigExpedite device.
//  
// Copyright (c) 2011 Orange Tree Technologies.
// May not be reproduced without permission.
//
//////////////////////////////////////////////////////////////////////

#include <string.h>
#include <xparameters.h>
#include <xio.h>
#include <xintc.h>
#include <sys/timer.h>
#include <xmk.h>
#include <semaphore.h>
#include <sys/process.h>
#include "socket.h"

// Interrupt controller state
extern XIntc IntController;

// Local structure for socket state
#define NUM_SOCKETS 16
static volatile struct {
	char Used;
	int Protocol;
	struct sockaddr LocalAddr;
	unsigned short RemotePort;
	unsigned long RemoteAddr;
	int State;
	int Accept;
	int Connect;
	int IE;
	char *RxBuffer;
	int RxBufferLen;
	int RxBufferPtr;
    int RxBurst;
    int RxFrameLen;
    unsigned char ExcessByte;
	const char *TxBuffer;
	int TxBufferLen;
} Sockets[NUM_SOCKETS];

// GigExpedite register map
#define LOCAL_IP_ADDR_LOW       (XPAR_GIGEXPEDITE_0_MEM0_BASEADDR + 0*4)
#define LOCAL_IP_ADDR_HIGH      (XPAR_GIGEXPEDITE_0_MEM0_BASEADDR + 1*4)
#define LINK_STATUS             (XPAR_GIGEXPEDITE_0_MEM0_BASEADDR + 2*4)
#define CONNECTION_PAGE         (XPAR_GIGEXPEDITE_0_MEM0_BASEADDR + 7*4)
#define LOCAL_PORT              (XPAR_GIGEXPEDITE_0_MEM0_BASEADDR + 8*4)
#define REMOTE_IP_ADDR_LOW      (XPAR_GIGEXPEDITE_0_MEM0_BASEADDR + 9*4)
#define REMOTE_IP_ADDR_HIGH     (XPAR_GIGEXPEDITE_0_MEM0_BASEADDR + 10*4)
#define REMOTE_PORT             (XPAR_GIGEXPEDITE_0_MEM0_BASEADDR + 11*4)
#define MTU_TTL                 (XPAR_GIGEXPEDITE_0_MEM0_BASEADDR + 12*4)
#define INTERRUPT_ENABLE_STATUS (XPAR_GIGEXPEDITE_0_MEM0_BASEADDR + 13*4)
#define CONNECTION_STATE        (XPAR_GIGEXPEDITE_0_MEM0_BASEADDR + 14*4)
#define FRAME_LENGTH            (XPAR_GIGEXPEDITE_0_MEM0_BASEADDR + 15*4)
#define DATA_FIFO               (XPAR_GIGEXPEDITE_0_MEM0_BASEADDR + 16*4)

// GigExpedite interrupt bits
#define IE_INCOMING          	0x0001
#define IE_OUTGOING_EMPTY    	0x0002
#define IE_OUTGOING_NOT_FULL 	0x0004
#define IE_STATE             	0x0008

// Connection states (for GigExpedite CONNECTION_STATE register)
#define CLOSED       			0x0000
#define LISTEN       			0x0001
#define CONNECT      			0x0002
#define ESTABLISHED  			0x0003
#define STATE_MASK   			0x000f
#define CONN_TCP     			0x0010
#define CONN_ENABLE  			0x0020
#define CONN_PAUSE  			0x0040

// Burst length for reading frames
#define BURST_LENGTH 192 //bytes

// Thread safety semaphore
static sem_t socket_Sem;

//////////////////////////////////////////////////////////////////////////////
// Local functions

//
// Read data from GigExpedite
//
static void socket_ReadFrame(int s, unsigned long *IntStatus)
{
    unsigned short Data;
    int First = 1;

    do
    {
        if (Sockets[s].RxFrameLen==0 || Sockets[s].RxBurst==BURST_LENGTH)
        {
            if (IntStatus==NULL) return;
            
            if (First==0)
            {
                int Start = xget_clock_ticks();
                
                // Wait for incoming interrupt
                do
                {
                    XIo_Out32(CONNECTION_PAGE, s);
                    *IntStatus |= XIo_In32(INTERRUPT_ENABLE_STATUS);
                } while (((*IntStatus)&IE_INCOMING)==0 && (xget_clock_ticks()-Start)<100);
                if (((*IntStatus)&IE_INCOMING)==0)
                    return;
            }
            (*IntStatus) &= ~IE_INCOMING;
            if (Sockets[s].RxFrameLen==0)
            {
                Sockets[s].RxFrameLen = XIo_In32(FRAME_LENGTH);
                if (Sockets[s].Protocol==IPPROTO_UDP)
                {
                    unsigned long RemoteIP1 = XIo_In32(DATA_FIFO+s*4);
                    unsigned long RemoteIP2 = XIo_In32(DATA_FIFO+s*4);
                    unsigned long RemotePort = XIo_In32(DATA_FIFO+s*4);
                    // This information could be used in an implementation
                    // of the recvfrom function for UDP communications
                }
            }
            First = 0;
            Sockets[s].RxBurst = 0;
        }

        // Read data (normally 2 bytes unless at end of frame)
        Data = XIo_In32(DATA_FIFO+s*4);
        *(Sockets[s].RxBuffer + Sockets[s].RxBufferPtr) = Data>>8;
        Sockets[s].RxBufferPtr++;
        Sockets[s].RxFrameLen--;
        Sockets[s].RxBurst++;
        if (Sockets[s].RxBufferPtr==Sockets[s].RxBufferLen)
        {
            if (Sockets[s].RxFrameLen!=0)
            {
                // Store excess byte for next recv
                Sockets[s].ExcessByte = Data&0xff;
            }
            break;
        }
        if (Sockets[s].RxFrameLen!=0)
        {
            *(Sockets[s].RxBuffer + Sockets[s].RxBufferPtr) = Data&0xff;
            Sockets[s].RxBufferPtr++;
            Sockets[s].RxFrameLen--;
            Sockets[s].RxBurst++;
        }
    } while (Sockets[s].RxBufferPtr<Sockets[s].RxBufferLen);
}

//
// GigExpedite interrupt service routine
//
static void socket_ISR(void *baseaddr_p)
{
	unsigned long State;
	unsigned long IntStatus;
	int s;

    for (s=0; s<NUM_SOCKETS; s++)
    {
        if (Sockets[s].IE==0)
            continue;
            
    	// Select connection and read interrupt status bits
    	// This will clear any state change interrupts
		XIo_Out32(CONNECTION_PAGE, s);
		IntStatus = XIo_In32(INTERRUPT_ENABLE_STATUS);

        do 
        {
	        // Read connection state
	        State = XIo_In32(CONNECTION_STATE);

		    // Handle changes in connection state
    	    if ((Sockets[s].State&STATE_MASK)==LISTEN &&
	    	    (State&STATE_MASK)==ESTABLISHED)
    	    {
    		    // Connection established
    		    // Read connected address and acknowledge state change
    		    unsigned long RemoteAddr;
        		
    		    Sockets[s].Accept++;
    		    Sockets[s].RemotePort = XIo_In32(REMOTE_PORT);
    		    RemoteAddr = XIo_In32(REMOTE_IP_ADDR_HIGH);
    		    RemoteAddr <<= 16;
    		    RemoteAddr |= XIo_In32(REMOTE_IP_ADDR_LOW);
    		    Sockets[s].RemoteAddr = RemoteAddr;
                Sockets[s].IE &= ~IE_STATE;
                XIo_Out32(INTERRUPT_ENABLE_STATUS, Sockets[s].IE);
			    XIo_Out32(CONNECTION_STATE, State);
			    Sockets[s].State = State;
    	    }
    	    if ((Sockets[s].State&STATE_MASK)==CONNECT &&
	    	    (State&STATE_MASK)==ESTABLISHED)
    	    {
    		    // Connection established
    		    // Acknowledge state change
    		    Sockets[s].Connect++;
                Sockets[s].IE &= ~IE_STATE;
                XIo_Out32(INTERRUPT_ENABLE_STATUS, Sockets[s].IE);
			    XIo_Out32(CONNECTION_STATE, State);
			    Sockets[s].State = State;
    	    }
    	    if (Sockets[s].Used==1 &&
    		    (Sockets[s].State&STATE_MASK)==CLOSED &&
	    	    (State&STATE_MASK)==CLOSED)
    	    {
    		    // Connection closed - free socket and acknowledge state change
			    Sockets[s].IE = 0;
                Sockets[s].State = 0;
			    XIo_Out32(INTERRUPT_ENABLE_STATUS, Sockets[s].IE);
			    XIo_Out32(CONNECTION_STATE, Sockets[s].State);
                Sockets[s].Used = 0;
	        }
	        IntStatus &= ~IE_STATE;
    		
		    // Service incoming data
		    if ((Sockets[s].IE&IE_INCOMING) &&
			    (IntStatus&IE_INCOMING))
		    {
			    // Data available
                socket_ReadFrame(s, &IntStatus);
			    Sockets[s].IE &= ~IE_INCOMING;
			    XIo_Out32(INTERRUPT_ENABLE_STATUS, Sockets[s].IE);
            }
		} while (IntStatus&IE_STATE);

		// Service outgoing data
		if ((Sockets[s].IE&IE_OUTGOING_EMPTY) &&
			(IntStatus&IE_OUTGOING_EMPTY))
		{
			// Outgoing connection empty - all data sent
			Sockets[s].IE &= ~IE_OUTGOING_EMPTY;
			XIo_Out32(INTERRUPT_ENABLE_STATUS, Sockets[s].IE);
		}

        if ((Sockets[s].IE&IE_OUTGOING_NOT_FULL) &&
			(IntStatus&IE_OUTGOING_NOT_FULL))
		{
			// Space available - send data (up to 64k)
			unsigned long Count;
			unsigned short *Ptr = (unsigned short *)(Sockets[s].TxBuffer);

            Sockets[s].State |= CONN_PAUSE;
            XIo_Out32(CONNECTION_STATE, Sockets[s].State);
			for (Count=0; Count<Sockets[s].TxBufferLen; Count+=2)
			{
                if ((Sockets[s].TxBufferLen-Count)==1)
                    XIo_Out8(DATA_FIFO+s*4+3, *((unsigned char *)Ptr));
                else
    				XIo_Out32(DATA_FIFO+s*4, *Ptr++);
			}
            Sockets[s].State &= ~CONN_PAUSE;
            XIo_Out32(CONNECTION_STATE, Sockets[s].State);
			Sockets[s].IE &= ~IE_OUTGOING_NOT_FULL;
			Sockets[s].IE |= IE_OUTGOING_EMPTY;
			XIo_Out32(INTERRUPT_ENABLE_STATUS, Sockets[s].IE);
		}
    }

	XIntc_Acknowledge(&IntController,
					  XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
}

//////////////////////////////////////////////////////////////////////////////
// External API

//
// Initialise library
//
void socket_init(void)
{
	int s;
	volatile int i;
	int Done;
	
	// Wait for GigExpedite initialisation
	for (i=0; i<100000; i++);
    
    // Initialise protection semaphore
    sem_init(&socket_Sem, 0, 1);
	
	// Clean GigExpedite registers
	for (s=0; s<NUM_SOCKETS; s++)
	{
		XIo_Out32(CONNECTION_PAGE, s);
		XIo_Out32(INTERRUPT_ENABLE_STATUS, 0);
		XIo_Out32(CONNECTION_STATE, 0);
	}
	do
	{
		Done = 1;
		for (s=0; s<NUM_SOCKETS; s++)
		{
			XIo_Out32(CONNECTION_PAGE, s);
			for (i=0; i<10; i++);
			if (XIo_In32(CONNECTION_STATE)!=0)
				Done = 0;
		}
	} while (Done==0);
	for (s=0; s<NUM_SOCKETS; s++)
	{
		XIo_Out32(CONNECTION_PAGE, s);
		for (i=0; i<10; i++);
		XIo_In32(INTERRUPT_ENABLE_STATUS);
	}
    
	// Initialise connection state and semaphore
	memset((void *)Sockets, 0, sizeof(Sockets));
	
	// Initialise interrupts
	XIntc_Connect(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR,
		  		  socket_ISR, (void*)XPAR_GIGEXPEDITE_0_MEM0_BASEADDR);
}

//
// Allocate new socket
// af and type are ignored
//
SOCKET socket(int af, int type, int protocol)
{
	int s;
    static int SockStart = 0; // Used to rotate socket use
	
    sem_wait(&socket_Sem);
    
	// Look for free socket
    s = SockStart;
    do
    {
		if (Sockets[s].Used==0) break;
        s = (s==NUM_SOCKETS-1) ? 0 : (s+1);
	} while (s!=SockStart);
    SockStart = (SockStart==NUM_SOCKETS-1) ? 0 : (SockStart+1);
    if (Sockets[s].Used!=0)
    {
        sem_post(&socket_Sem);
        return INVALID_SOCKET;
    }
    
	// Initialise connection state
    XIntc_Disable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
	Sockets[s].Used = 1;
	Sockets[s].Accept = 0;
	Sockets[s].Connect = 0;
	Sockets[s].IE = 0;
	Sockets[s].State = 0;
	Sockets[s].Protocol = protocol;
    Sockets[s].RxBufferLen = 0;
    Sockets[s].RxBufferPtr = 0;
    Sockets[s].RxBurst = 0;
    Sockets[s].RxFrameLen = 0;
    Sockets[s].RemotePort = 0;
    Sockets[s].RemoteAddr = 0;
    Sockets[s].TxBufferLen = 0;
	XIo_Out32(CONNECTION_PAGE, s);
	XIo_Out32(INTERRUPT_ENABLE_STATUS, 0);
	XIo_Out32(CONNECTION_STATE, 0);
    XIntc_Enable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
    sem_post(&socket_Sem);

	return s;
}

//
// Bind socket to an address
//
int bind(SOCKET s, const struct sockaddr *name, int namelen)
{
	struct sockaddr_in *Addr = (struct sockaddr_in *)name;
	memcpy((void *)&Sockets[s].LocalAddr, name, namelen);
	Addr = (struct sockaddr_in *)&Sockets[s].LocalAddr;
	
	return 0;
}

//
// Listen for incoming connections
// backlog is ignored
//
int listen(SOCKET s, int backlog)
{
	struct sockaddr_in *Addr = (struct sockaddr_in *)&Sockets[s].LocalAddr;
	unsigned long State = CONN_ENABLE | LISTEN;
	
	if (Sockets[s].Protocol==IPPROTO_TCP)
		State |= CONN_TCP;

	// Set up listening connection		
    sem_wait(&socket_Sem);
    XIntc_Disable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
    Sockets[s].RemoteAddr = 0;
    Sockets[s].RemotePort = 0;
	Sockets[s].IE |= IE_STATE;
	Sockets[s].State = State;
	Sockets[s].Accept = 0;
	XIo_Out32(CONNECTION_PAGE, s);
	XIo_Out32(LOCAL_PORT, Addr->sin_port);
	XIo_Out32(MTU_TTL, 0x8080);
	XIo_Out32(INTERRUPT_ENABLE_STATUS, Sockets[s].IE);
	XIo_Out32(CONNECTION_STATE, Sockets[s].State);
    XIntc_Enable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
    sem_post(&socket_Sem);
        
    return 0;
}

//
// Wait for connection from remote device
// addrlen is ignored
//
SOCKET accept(SOCKET s, struct sockaddr *addr, int *addrlen)
{
	int Done = 0;
	
	// Busy wait loop until connection is made
	// This code be changed for a semaphore from the ISR if available
	while (!Done)
	{
        XIntc_Disable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
		if (Sockets[s].Accept!=0)
		{
			Sockets[s].Accept--;
			Done = 1;
			if (addr!=NULL)
			{
				struct sockaddr_in *Addr = (struct sockaddr_in *)addr;
				Addr->sin_port = Sockets[s].RemotePort;
				Addr->sin_addr.s_addr = Sockets[s].RemoteAddr;
				*addrlen = sizeof(struct sockaddr_in);
			}
		}
        XIntc_Enable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
        yield();
	}
	
	return s;
}

//
// Connect to remote device
//
int connect(SOCKET s, const struct sockaddr *addr, int addrlen)
{
    struct sockaddr_in *Target = (struct sockaddr_in *)addr;
    struct sockaddr_in *Addr = (struct sockaddr_in *)&Sockets[s].LocalAddr;
    unsigned long State = CONN_ENABLE | CONNECT;
    int Done = 0;
    int Start = xget_clock_ticks();

    if (Sockets[s].Protocol==IPPROTO_TCP)
        State |= CONN_TCP;

    // Set up connection     
    sem_wait(&socket_Sem);
    XIntc_Disable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
    Sockets[s].IE |= IE_STATE;
    Sockets[s].State = State;
    Sockets[s].Connect = 0;
    XIo_Out32(CONNECTION_PAGE, s);
    XIo_Out32(REMOTE_IP_ADDR_LOW, Target->sin_addr.s_addr&0xffff);
    XIo_Out32(REMOTE_IP_ADDR_HIGH, (Target->sin_addr.s_addr>>16)&0xffff);
    XIo_Out32(REMOTE_PORT, Target->sin_port);
    XIo_Out32(LOCAL_PORT, Addr->sin_port);
    XIo_Out32(MTU_TTL, 0x8080);
    XIo_Out32(INTERRUPT_ENABLE_STATUS, Sockets[s].IE);
    XIo_Out32(CONNECTION_STATE, Sockets[s].State);
    XIntc_Enable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
    sem_post(&socket_Sem);

    // Busy wait loop until connection is made
    // This code be changed for a semaphore from the ISR if available
    while (!Done && (xget_clock_ticks()-Start)<1000)
    {
        XIntc_Disable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
        if (Sockets[s].Connect!=0)
        {
            Sockets[s].Connect--;
            Done = 1;
        }
        XIntc_Enable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
        yield();
    }
    if (!Done)
    {
        sem_wait(&socket_Sem);
        XIntc_Disable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
        Sockets[s].IE &= ~IE_STATE;
        Sockets[s].State = 0;
        Sockets[s].Connect = 0;
        XIo_Out32(CONNECTION_PAGE, s);
        XIo_Out32(INTERRUPT_ENABLE_STATUS, Sockets[s].IE);
        XIo_Out32(CONNECTION_STATE, Sockets[s].State);
        XIntc_Enable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
        sem_post(&socket_Sem);
        return SOCKET_ERROR;
    }
    
    return 0;
}

//
// Send data to remote device
// flags is ignored
// NB: Max send length is 64kbytes
//
int send(SOCKET s, const char *buf, int len, int flags)
{
    int Start = xget_clock_ticks();
    
	// Initiate transfer
    sem_wait(&socket_Sem);
	Sockets[s].TxBuffer = buf;
	Sockets[s].TxBufferLen = len > 65536 ? 65536 : len;
    XIntc_Disable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
	XIo_Out32(CONNECTION_PAGE, s);
	Sockets[s].IE |= IE_OUTGOING_NOT_FULL;
	XIo_Out32(INTERRUPT_ENABLE_STATUS, Sockets[s].IE);
    XIntc_Enable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
    sem_post(&socket_Sem);
    
    // Wait for completion
    // Could use semaphore from ISR if available
	while (((Sockets[s].IE&IE_OUTGOING_NOT_FULL) ||
		    (Sockets[s].IE&IE_OUTGOING_EMPTY)) &&
           (xget_clock_ticks()-Start)<100)
        yield();
    if ((Sockets[s].IE&IE_OUTGOING_NOT_FULL) ||
        (Sockets[s].IE&IE_OUTGOING_EMPTY))
    {
        sem_wait(&socket_Sem);
        Sockets[s].TxBufferLen = 0;
        XIntc_Disable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
        XIo_Out32(CONNECTION_PAGE, s);
        Sockets[s].IE &= ~(IE_OUTGOING_NOT_FULL|IE_OUTGOING_EMPTY);
        XIo_Out32(INTERRUPT_ENABLE_STATUS, Sockets[s].IE);
        XIntc_Enable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
        sem_post(&socket_Sem);
        return 0;
    }
		   
	return len;
}

//
// Receive data from remote device
// flags is ignored
//
int recv(SOCKET s, char *buf, int len, int flags)
{
    int Start = xget_clock_ticks();
    
    if (len==0) return 0;
    
    Sockets[s].RxBuffer = buf;
    Sockets[s].RxBufferLen = len;
    Sockets[s].RxBufferPtr = 0;
    
    // Check for an odd byte already read from GigExpedite
    if (Sockets[s].RxFrameLen!=0 && (Sockets[s].RxBurst&1)!=0)
    {
        // There is an odd byte remaining to be read
        *(Sockets[s].RxBuffer + Sockets[s].RxBufferPtr) = Sockets[s].ExcessByte;
        Sockets[s].RxBufferPtr++;
        Sockets[s].RxFrameLen--;
        Sockets[s].RxBurst++;
    }
    if (Sockets[s].RxBufferPtr>=Sockets[s].RxBufferLen)
    {
        // Complete
        return Sockets[s].RxBufferPtr;
    }

    // Check for end of previous frame
    sem_wait(&socket_Sem);
    XIntc_Disable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
    XIo_Out32(CONNECTION_PAGE, s);
    socket_ReadFrame(s, NULL);
    XIntc_Enable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
    sem_post(&socket_Sem);

    if (Sockets[s].RxBufferPtr>=Sockets[s].RxBufferLen)
    {
        // Complete
        return Sockets[s].RxBufferPtr;
    }

	// Initiate receive
    sem_wait(&socket_Sem);
    XIntc_Disable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
	XIo_Out32(CONNECTION_PAGE, s);
	Sockets[s].IE |= IE_INCOMING;
	XIo_Out32(INTERRUPT_ENABLE_STATUS, Sockets[s].IE);
    XIntc_Enable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
    sem_post(&socket_Sem);

    // Wait for completion
    // Could use semaphore from ISR if available
	while ((Sockets[s].State&STATE_MASK)==ESTABLISHED &&
           Sockets[s].IE&IE_INCOMING &&
           (xget_clock_ticks()-Start)<1000)
        yield();
    if ((Sockets[s].State&STATE_MASK)==ESTABLISHED &&
        Sockets[s].IE&IE_INCOMING)
    {
        // Timeout
        sem_wait(&socket_Sem);
        XIntc_Disable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
        XIo_Out32(CONNECTION_PAGE, s);
        Sockets[s].IE &= ~IE_INCOMING;
        XIo_Out32(INTERRUPT_ENABLE_STATUS, Sockets[s].IE);
        XIntc_Enable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
        sem_post(&socket_Sem);
    }
    
	return Sockets[s].RxBufferPtr;
}

int sendto(SOCKET s, const char *buf, int len, int flags,
           const struct sockaddr *toaddr, int tolen)
{
    // UDP not used
	return 0;
}
           
int recvfrom(SOCKET s, char *buf, int len, int flags,
             const struct sockaddr *fromaddr, int *fromlen)
{
    // UDP not used
	return 0;
}

//
// Close and free socket
//
int close(SOCKET s)
{
    sem_wait(&socket_Sem);
    XIntc_Disable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
	XIo_Out32(CONNECTION_PAGE, s);
    Sockets[s].State &= ~STATE_MASK;
    Sockets[s].State |= CLOSED;
	Sockets[s].IE |= IE_STATE;
	XIo_Out32(CONNECTION_STATE, Sockets[s].State);
	XIo_Out32(INTERRUPT_ENABLE_STATUS, Sockets[s].IE);
    XIntc_Enable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
    sem_post(&socket_Sem);
        
    // The socket may still be open here - the ISR will
    // clean up when GigExpedite acknowledges close
    
    return 0;
}

//
// Pause/unpause transmission on a socket
//
void SocketPause(SOCKET s, int p)
{
    sem_wait(&socket_Sem);
    XIntc_Disable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
	XIo_Out32(CONNECTION_PAGE, s);
	if (p)
		Sockets[s].State |= CONN_PAUSE;
	else
		Sockets[s].State &= ~CONN_PAUSE;
	XIo_Out32(CONNECTION_STATE, Sockets[s].State);
    XIntc_Enable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
    sem_post(&socket_Sem);
}

//
// Get local IP address from GigEx
//
unsigned long SocketGetLocalIPAddr(void)
{
    unsigned long Res = 0;
    
    sem_wait(&socket_Sem);
    XIntc_Disable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
    Res = XIo_In32(LOCAL_IP_ADDR_HIGH);
    Res<<=16;
    Res |= XIo_In32(LOCAL_IP_ADDR_LOW);
    XIntc_Enable(&IntController, XPAR_XPS_INTC_0_GIGEXPEDITE_0_ETHERNETIRQ_INTR);
    sem_post(&socket_Sem);
    
    return Res;
}

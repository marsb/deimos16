//////////////////////////////////////////////////////////////////////
//
// File:      HTTP.h
//
// Purpose:
//    ZestET1 Example Programs
//    Very simple multi-threaded web server.
//
// Copyright (c) 2011 Orange Tree Technologies.
// May not be reproduced without permission.
//
//////////////////////////////////////////////////////////////////////

#ifndef HTTP_H_
#define HTTP_H_

void HTTPInit(void);

#endif /* HTTP_H_ */

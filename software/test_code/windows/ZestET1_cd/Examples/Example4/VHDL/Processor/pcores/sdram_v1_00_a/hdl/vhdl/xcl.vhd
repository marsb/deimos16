--
-- xcl.vhd
--
-- XCL interface code
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity xcl is
    port (
        RST                           : in std_logic;
        FSL_M_Clk                     : in std_logic;
        FSL_M_Write                   : in std_logic;
        FSL_M_Data                    : in std_logic_vector(0 to 31);
        FSL_M_Control                 : in std_logic;
        FSL_M_Full                    : out std_logic;
        FSL_S_Clk                     : in std_logic;
        FSL_S_Read                    : in std_logic;
        FSL_S_Data                    : out std_logic_vector(0 to 31);
        FSL_S_Control                 : out std_logic;
        FSL_S_Exists                  : out std_logic;

        RAMClk                        : in std_logic;
        ArbCS                         : in std_logic;
        Req                           : out std_logic;
        RE                            : out std_logic;
        WE                            : out std_logic;
        Addr                          : out std_logic_vector(23 downto 0);
        BE                            : out std_logic_vector(3 downto 0);
        WriteData                     : out std_logic_vector(31 downto 0);
        ReadData                      : in std_logic_vector(31 downto 0);
        ReadDataValid                 : in std_logic
    );
end xcl;

architecture arch of xcl is

    -- Declare signals
    constant LOG_CACHE_LINE_SIZE : integer := 3; -- cache line of 8 words

    signal BurstCount : std_logic_vector(LOG_CACHE_LINE_SIZE downto 0);
    signal RegRnW : std_logic;
    signal RegType : std_logic_vector(1 downto 0);
    signal RegAddr : std_logic_vector(31 downto 0);
    signal RegBE : std_logic_vector(3 downto 0);

    signal ReqWE : std_logic;
    signal ReqRE : std_logic;
    signal ReqDataIn : std_logic_vector(60 downto 0);
    signal ReqDataOut : std_logic_vector(60 downto 0);
    signal ReqFull : std_logic;
    signal ReqEmpty : std_logic;
    signal RespWE : std_logic;
    signal RespRE : std_logic;
    signal RespDataIn : std_logic_vector(31 downto 0);
    signal RespDataOut : std_logic_vector(31 downto 0);
    signal RespEmpty : std_logic;

begin

    -- Process access requests and place entries
    -- in the request FIFO to the RAM
    process (RST, FSL_M_Clk)
    begin
        if (RST='1') then
            BurstCount <= '1' & conv_std_logic_vector(0, LOG_CACHE_LINE_SIZE);
            RegRnW <= '0';
            RegAddr <= (others=>'0');
            RegType <= (others=>'0');
        elsif (FSL_M_Clk'event and FSL_M_Clk='1') then
            if (FSL_M_Write='1') then
                if (BurstCount=('1'&conv_std_logic_vector(0, LOG_CACHE_LINE_SIZE))) then
                    -- Address phase
                    BurstCount <= (others=>'0');
                    RegRnW <= not FSL_M_Control;
                    RegAddr <= FSL_M_Data(0 to 29)&"00";
                    RegType <= FSL_M_Data(30 to 31);
                else
                    -- Writes are non-bursting
                    BurstCount <= '1' & conv_std_logic_vector(0, LOG_CACHE_LINE_SIZE);
                end if;
            elsif (ReqFull='0' and RegRnW='1' and BurstCount(LOG_CACHE_LINE_SIZE)='0') then
                -- Read burst
                BurstCount <= BurstCount + 1;
            end if;
        end if;
    end process;
    
    process (RegRnW, RegType, FSL_M_Control)
    begin
        if (RegRnW='1') then
            RegBE <= "1111";
        else
            case (FSL_M_Control & RegType) is
                when "000" => RegBE <= "1111";
                when "001" => RegBE <= "1100";
                when "010" => RegBE <= "1111";
                when "011" => RegBE <= "0011";
                when "100" => RegBE <= "1000";
                when "101" => RegBE <= "0100";
                when "110" => RegBE <= "0010";
                when others => RegBE <= "0001";
            end case;
        end if;
    end process;
                 
    ReqWE <= '1' when ReqFull='0' and BurstCount(LOG_CACHE_LINE_SIZE)='0' and
                      (RegRnW='1' or FSL_M_Write='1') else '0';

    ReqDataIn(60) <= RegRNW;
    ReqDataIn(59 downto 56) <= RegBE;
    ReqDataIn(55 downto 32+LOG_CACHE_LINE_SIZE) <= RegAddr(25 downto 2+LOG_CACHE_LINE_SIZE);
    ReqDataIn(32+LOG_CACHE_LINE_SIZE-1 downto 32) <= RegAddr(2+LOG_CACHE_LINE_SIZE-1 downto 2) +
                                                        BurstCount(LOG_CACHE_LINE_SIZE-1 downto 0);
    ReqDataIn(31 downto 0) <= FSL_M_Data;
    FSL_M_Full <= '1' when ReqFull='1' or
                           (RegRnW='1' and BurstCount(LOG_CACHE_LINE_SIZE)='0') else '0';
    
    -- Send data to SDRAM
    Req <= not ReqEmpty;
    ReqRE <= ArbCS and not ReqEmpty;
    WE <= not ReqDataOut(60) and ReqRE;
    RE <= ReqDataOut(60) and ReqRE;
    BE <= ReqDataOut(59 downto 56);
    Addr <= ReqDataOut(55 downto 32);
    WriteData <= ReqDataOut(31 downto 0);

    -- Get response from SDRAM
    RespWE <= ReadDataValid;
    RespDataIn <= ReadData;
    
    -- Send response to CPU
    FSL_S_Control <= '1';
    FSL_S_Exists <= not RespEmpty;
    FSL_S_Data <= RespDataOut;
    RespRE <= FSL_S_Read;

    -- Instantiate cross clock domain FIFOs between CPU and RAM
    -- The cache controller can issue multiple queued read accesses so the
    -- response FIFO must be longer than the request FIFO
    ReqFIFOInst : entity sdram_v1_00_a.DistFIFO 
        generic map (
            FIFO_WIDTH => 61,
            FIFO_LOG_DEPTH => 4
        )
        port map (
            rst => RST,
            wr_clk => FSL_M_Clk,
            wr_en => ReqWE,
            din => ReqDataIn,
            rd_clk => RAMClk,
            rd_en => ReqRE,
            dout => ReqDataOut,
            empty => ReqEmpty,
            full => ReqFull,
            wr_data_count => open,
            rd_data_count => open
        );
    RespFIFOInst : entity sdram_v1_00_a.DistFIFO 
        generic map (
            FIFO_WIDTH => 32,
            FIFO_LOG_DEPTH => 6
        )
        port map (
            rst => RST,
            wr_clk => RAMClk,
            wr_en => RespWE,
            din => RespDataIn,
            rd_clk => FSL_S_Clk,
            rd_en => RespRE,
            dout => RespDataOut,
            empty => RespEmpty,
            full => open,
            wr_data_count => open,
            rd_data_count => open
        );
        
end arch;
--   ZestET1 Example 4
--   File name: Example4.vhd
--   Version: 1.10
--   Date: 01/06/2011
--
--   ZestET1 Example 4 - Microblaze example
--   Connects Microblaze to Ethernet.
--   Demonstrates use of GigExpedite from processor without
--   TCP/IP presence of library.
--
--   Copyright (C) 2011 Orange Tree Technologies Ltd. All rights reserved.
--   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and 
--   modify this logic core in any form such as VHDL source code or EDIF netlist in 
--   FPGA designs that target the ZestET1.
--   Orange Tree Technologies prohibits the use of this logic core in any form such 
--   as VHDL source code or EDIF netlist in FPGA designs that target any other
--   hardware unless the purchaser of the ZestET1 has purchased the appropriate 
--   licence from Orange Tree Technologies. Contact Orange Tree Technologies if you 
--   want to purchase such a licence.
--
--  *****************************************************************************************
--  **
--  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
--  **              provided to you "as is". Orange Tree Technologies and its licensors 
--  **              make and you receive no warranties or conditions, express, implied, 
--  **              statutory or otherwise, and Orange Tree Technologies specifically 
--  **              disclaims any implied warranties of merchantability, non-infringement,
--  **              or fitness for a particular purpose. Orange Tree Technologies does not
--  **              warrant that the functions contained in these designs will meet your 
--  **              requirements, or that the operation of these designs will be 
--  **              uninterrupted or error free, or that defects in the Designs will be 
--  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
--  **              make any representations regarding use or the results of the use of the 
--  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
--  **
--  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
--  **              or its licensors be liable for any loss of data, lost profits, cost or 
--  **              procurement of substitute goods or services, or for any special, 
--  **              incidental, consequential, or indirect damages arising from the use or 
--  **              operation of the designs or accompanying documentation, however caused 
--  **              and on any theory of liability. This limitation will apply even if 
--  **              Orange Tree Technologies has been advised of the possibility of such 
--  **              damage. This limitation shall apply notwithstanding the failure of the 
--  **              essential purpose of any limited remedies herein.
--  **
--  *****************************************************************************************


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity Example4 is
    port (
        -- Programmable Clock
        CLK : in std_logic;
        
        -- Flash Interface
        Flash_CSOn : out std_logic;
        Flash_CLK : out std_logic;
        Flash_MOSI : out std_logic;
        Flash_MISO : in std_logic;

        -- IO connector
        IO0_In : in std_logic;
        IO0 : inout std_logic_vector(39 downto 0);
        IO3_In : in std_logic_vector(2 downto 0);
        IO3 : inout std_logic_vector(35 downto 0);

        -- Ethernet Interface
        Eth_Clk : out std_logic;
        Eth_CSn : out std_logic;
        Eth_WEn : out std_logic;
        Eth_A : out std_logic_vector(4 downto 0);
        Eth_D : inout std_logic_vector(15 downto 0);
        Eth_BE : out std_logic_vector(1 downto 0);
        Eth_Intn : in std_logic;

        -- DDR SDRAM interface
        DS_CLK_P : out std_logic;
        DS_CLK_N : out std_logic;
        DS_CKE : out std_logic;
        DS_A : out std_logic_vector(12 downto 0);
        DS_BA : out std_logic_vector(1 downto 0);
        DS_CAS_N : out std_logic;
        DS_RAS_N : out std_logic;
        DS_WE_N : out std_logic;
        DS_DQS : inout std_logic_vector(1 downto 0);
        DS_DM : out std_logic_vector(1 downto 0);
        DS_DQ : inout std_logic_vector(15 downto 0)
    );
end Example4;

architecture arch of Example4 is

    --------------------------------------------------------------------------
    -- Declare signals
    
    -- Clocks and reset
    signal RST : std_logic;
    signal RAMRST : std_logic;
    signal RAMClk : std_logic;
    signal RAMClk90 : std_logic;
    signal EthClk : std_logic;

    -- Ethernet interface
    signal EthernetClk : std_logic;
    signal EthernetWE : std_logic;
    signal EthernetRE : std_logic;
    signal EthernetOwner : std_logic_vector(7 downto 0);
    signal EthernetAddr : std_logic_vector(4 downto 0);
    signal EthernetBE : std_logic_vector(1 downto 0);
    signal EthernetWriteData : std_logic_vector(15 downto 0);
    signal EthernetReadData : std_logic_vector(15 downto 0);
    signal EthernetReadDataValid : std_logic;
    signal EthernetValidOwner : std_logic_vector(7 downto 0);
    signal EthernetInterrupt : std_logic;

    -- SDRAM interface
    signal SDRAMAddr : std_logic_vector(23 downto 0);
    signal SDRAMRE : std_logic;
    signal SDRAMWE : std_logic;
    signal SDRAMOwner : std_logic_vector(7 downto 0);
    signal SDRAMReadDataValid : std_logic;
    signal SDRAMValidOwner : std_logic_vector(7 downto 0);
    signal SDRAMBE : std_logic_vector(3 downto 0);
    signal SDRAMWriteData : std_logic_vector(31 downto 0);
    signal SDRAMReadData : std_logic_vector(31 downto 0);
    signal SDRAMBusy : std_logic;
    signal SDRAMInitDone : std_logic;
    attribute TIG : string;
    attribute TIG of SDRAMInitDone : signal is "TRUE";

    -- Processor
    signal ProcClk : std_logic;
    signal ProcRST : std_logic;

begin

    --------------------------------------------------------------------------
    -- Connect signals
    
    -- Flash
    Flash_CSOn <= '1';
    Flash_CLK <= '1';
    Flash_MOSI <= '1';
    --xxx <= Flash_MISO;
    
    -- IO connector
    IO0 <= (others=>'Z');
    IO3 <= (others=>'Z');
    --xxx <= IO0_In;
    --xxx <= IO3_In,
    
    -- Clocks and resets
    ProcClk <= EthClk;
    process (ProcClk)
    begin
        if (ProcClk'event and ProcClk='1') then
            ProcRST <= not SDRAMInitDone;
        end if;
    end process;
    ROCInst : ROC port map (O=>RST);

    --------------------------------------------------------------------------
    -- Instantiate Microblaze
    ProcessorInst : entity work.Processor 
        port map (
            sys_clk_s => ProcClk,
            sys_rst_s => ProcRST,

            -- Ethernet connections
            EthernetClk => EthernetClk,
            EthernetWE => EthernetWE,
            EthernetRE => EthernetRE,
            EthernetAddr => EthernetAddr,
            EthernetBE => EthernetBE,
            EthernetWriteData => EthernetWriteData,
            EthernetReadData => EthernetReadData,
            EthernetReadDataValid => EthernetReadDataValid,
            EthernetInterrupt => EthernetInterrupt,
            
            -- SDRAM connections
            RAMClk => RAMClk,
            RAMBusy => SDRAMBusy,
            RAMWE => SDRAMWE,
            RAMRE => SDRAMRE,
            RAMAddr => SDRAMAddr,
            RAMBE => SDRAMBE,
            RAMOwner => SDRAMOwner,
            RAMWriteData => SDRAMWriteData,
            RAMReadData => SDRAMReadData,
            RAMReadDataValid => SDRAMReadDataValid,
            RAMValidOwner => SDRAMValidOwner
        );
    
    --------------------------------------------------------------------------
    -- Instantiate clocks
    -- (Assumes default 125MHz reference clock)
    ClockInst : entity work.ZestET1_Clocks
        port map (
            RST => RST,
            RefClk => CLK,
            EthClk => EthClk,
            RAMRST => RAMRST,
            RAMClk => RAMClk,
            RAMClk90 => RAMClk90
        );

    --------------------------------------------------------------------------
    -- Ethernet interface
    EthernetInst : entity work.ZestET1_Ethernet
        generic map (
            CLOCK_RATE => 75000000
        )
        port map (
            User_CLK => EthernetClk,
            User_RST => RST,

            -- User interface
            User_WE => EthernetWE,
            User_RE => EthernetRE,
            User_Addr => EthernetAddr,
            User_WriteData => EthernetWriteData,
            User_BE => EthernetBE,
            User_Owner => EthernetOwner,
            User_ReadData => EthernetReadData,
            User_ReadDataValid => EthernetReadDataValid,
            User_ValidOwner => EthernetValidOwner,
            User_Interrupt => EthernetInterrupt,
            
            -- Interface to GigExpedite
            Eth_Clk => Eth_Clk,
            Eth_CSn => Eth_CSn,
            Eth_WEn => Eth_WEn,
            Eth_A => Eth_A,
            Eth_D => Eth_D,
            Eth_BE => Eth_BE,
            Eth_Intn => Eth_Intn
        );

    --------------------------------------------------------------------------
    -- SDRAM Buffer
    SDRAMInst : entity work.ZestET1_SDRAM
        generic map (
            CLOCK_RATE => 166666666
        )
        port map (
            User_CLK => RAMClk,
            User_CLK90 => RAMClk90,
            User_RST => RAMRST,

            User_A => SDRAMAddr,
            User_RE => SDRAMRE,
            User_WE => SDRAMWE,
            User_Owner => SDRAMOwner,
            User_BE => SDRAMBE,
            User_DW => SDRAMWriteData,
            User_DR => SDRAMReadData,
            User_DR_Valid => SDRAMReadDataValid,
            User_ValidOwner => SDRAMValidOwner,
            User_Busy => SDRAMBusy,
            User_InitDone => SDRAMInitDone,

            DS_CLK_P => DS_CLK_P,
            DS_CLK_N => DS_CLK_N,
            DS_A => DS_A,
            DS_BA => DS_BA,
            DS_DQ => DS_DQ,
            DS_DQS => DS_DQS,
            DS_DM => DS_DM,
            DS_CAS_N => DS_CAS_N,
            DS_RAS_N => DS_RAS_N,
            DS_CKE => DS_CKE,
            DS_WE_N => DS_WE_N
        );

end arch;


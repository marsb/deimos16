/*
   ZestET1 Example 5
   File name: Example5.v
   Version: 1.00
   Date: 26/7/2009

   ZestET1 Example 5 - UDP multicast test code.
   Multicasts periodic counter to the network

   Copyright (C) 2009 Orange Tree Technologies Ltd. All rights reserved.
   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and 
   modify this logic core in any form such as Verilog source code or EDIF netlist in 
   FPGA designs that target the ZestET1.
   Orange Tree Technologies prohibits the use of this logic core in any form such 
   as Verilog source code or EDIF netlist in FPGA designs that target any other
   hardware unless the purchaser of the ZestET1 has purchased the appropriate 
   licence from Orange Tree Technologies. Contact Orange Tree Technologies if you 
   want to purchase such a licence.

  *****************************************************************************************
  **
  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
  **              provided to you "as is". Orange Tree Technologies and its licensors 
  **              make and you receive no warranties or conditions, express, implied, 
  **              statutory or otherwise, and Orange Tree Technologies specifically 
  **              disclaims any implied warranties of merchantability, non-infringement,
  **              or fitness for a particular purpose. Orange Tree Technologies does not
  **              warrant that the functions contained in these designs will meet your 
  **              requirements, or that the operation of these designs will be 
  **              uninterrupted or error free, or that defects in the Designs will be 
  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
  **              make any representations regarding use or the results of the use of the 
  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
  **
  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
  **              or its licensors be liable for any loss of data, lost profits, cost or 
  **              procurement of substitute goods or services, or for any special, 
  **              incidental, consequential, or indirect damages arising from the use or 
  **              operation of the designs or accompanying documentation, however caused 
  **              and on any theory of liability. This limitation will apply even if 
  **              Orange Tree Technologies has been advised of the possibility of such 
  **              damage. This limitation shall apply notwithstanding the failure of the 
  **              essential purpose of any limited remedies herein.
  **
  *****************************************************************************************
*/

`timescale 1ns / 1ps

module Example5
(
    // Programmable Clock
    input CLK,
    
    // Flash Interface
    output Flash_CSOn,
    output Flash_CLK,
    output Flash_MOSI,
    input Flash_MISO,

    // IO connector
    input IO0_In,
    inout [39:0] IO0,
    input [2:0] IO3_In,
    inout [35:0] IO3,

    // Ethernet Interface
    output Eth_Clk,
    output Eth_CSn,
    output Eth_WEn,
    output [4:0] Eth_A,
    inout [15:0] Eth_D,
    output [1:0] Eth_BE,
    input Eth_Intn,

    // DDR SDRAM interface
    output DS_CLK_P,
    output DS_CLK_N,
    output DS_CKE,
    output [12:0] DS_A,
    output [1:0] DS_BA,
    output DS_CAS_N,
    output DS_RAS_N,
    output DS_WE_N,
    inout [1:0] DS_DQS,
    output [1:0] DS_DM,
    inout [15:0] DS_DQ
);

    //////////////////////////////////////////////////////////////////////////
    // Declare constants
    
    // GigExpedite register addresses
    localparam LOCAL_IP_ADDR_LOW        = 0;
    localparam LOCAL_IP_ADDR_HIGH       = 1;
    localparam LINK_STATUS              = 2;
    localparam CONNECTION_PAGE          = 7;
    localparam LOCAL_PORT               = 8;
    localparam REMOTE_IP_ADDR_LOW       = 9;
    localparam REMOTE_IP_ADDR_HIGH      = 10;
    localparam REMOTE_PORT              = 11;
    localparam MTU_TTL                  = 12;
    localparam INTERRUPT_ENABLE_STATUS  = 13;
    localparam CONNECTION_STATE         = 14;
    localparam FRAME_LENGTH             = 15;
    localparam DATA_FIFO                = 16;
    
    // Connection states (for CONNECTION_STATE reg)
    localparam CLOSED       = 16'h0000;
    localparam LISTEN       = 16'h0001;
    localparam CONNECT      = 16'h0002;
    localparam ESTABLISHED  = 16'h0003;
    localparam CONN_UDP     = 16'h0000;
    localparam CONN_TCP     = 16'h0010;
    localparam CONN_ENABLE  = 16'h0020;
    
    // Interrupt enable and status bits (for INTERRUPT_ENABLE_STATUS reg)
    localparam IE_INCOMING          = 16'h0001;
    localparam IE_OUTGOING_EMPTY    = 16'h0002;
    localparam IE_OUTGOING_NOT_FULL = 16'h0004;
    localparam IE_STATE             = 16'h0008;

    //////////////////////////////////////////////////////////////////////////
    // Declare signals
    
    // Clocks and reset
    wire RST;
    wire RAMRST;
    wire RAMClk;
    wire RAMClk90;
    wire EthClk;

    // Ethernet interface state machine
    reg UserWE;
    reg UserRE;
    reg [4:0] UserAddr;
    reg [1:0] UserBE;
    reg [15:0] UserWriteData;
    wire [15:0] UserReadData;
    wire UserReadDataValid;
    wire UserInterrupt;

    reg [23:0] Delay;
    reg [31:0] Timer;
    reg [15:0] Counter;
    reg [8:0] UserFPGAState;
    reg [15:0] UserFPGASubState;
    reg [15:0] UserValidCount;
    reg [5:0] ConnectionState;
    reg [15:0] InterruptStatus;
    reg Clean;
    
    localparam USER_FPGA_DELAY_INIT  = 9'h001;
    localparam USER_FPGA_CLEAN       = 9'h002;
    localparam USER_FPGA_CLEAN_CHECK = 9'h004;
    localparam USER_FPGA_CLEAR_INTS  = 9'h008;
    localparam USER_FPGA_INIT        = 9'h010;
    localparam USER_FPGA_IDLE        = 9'h020;
    localparam USER_FPGA_CHECK_STATE = 9'h040;
    localparam USER_FPGA_CHECK_SPACE = 9'h080;
    localparam USER_FPGA_WRITE_DATA  = 9'h100;

    // Tie unused signals
    assign Flash_CSOn = 1;
    assign Flash_CLK = 1;
    assign Flash_MOSI = 1;
    assign IO0 = 40'hZ;
    assign IO3 = 36'hZ;
    
    //////////////////////////////////////////////////////////////////////////
    // Instantiate clocks
    // (Assumes default 125MHz reference clock)
    ZestET1_Clocks ClockInst (
        .RST(RST),
        .RefClk(CLK),
        .EthClk(EthClk),
        .RAMRST(RAMRST),
        .RAMClk(RAMClk),
        .RAMClk90(RAMClk90)
    );

    //////////////////////////////////////////////////////////////////////////
    // Ethernet interface test code
    ZestET1_Ethernet #(.CLOCK_RATE(125000000))
        EthernetInst (
            .User_CLK(EthClk),
            .User_RST(RST),

            // User interface
            .User_WE(UserWE),
            .User_RE(UserRE),
            .User_Addr(UserAddr),
            .User_Owner(0),
            .User_WriteData(UserWriteData),
            .User_BE(UserBE),
            .User_ReadData(UserReadData),
            .User_ReadDataValid(UserReadDataValid),
            //.User_ValidOwner(),
            .User_Interrupt(UserInterrupt),
            
            // Interface to GigExpedite
            .Eth_Clk(Eth_Clk),
            .Eth_CSn(Eth_CSn),
            .Eth_WEn(Eth_WEn),
            .Eth_A(Eth_A),
            .Eth_D(Eth_D),
            .Eth_BE(Eth_BE),
            .Eth_Intn(Eth_Intn)
        );

    // State machine to read/write Ethernet
    // Note: use RAMRST to keep the state machine in reset until 
    // the DCMs are locked
    always @ (posedge RAMRST or posedge EthClk) begin
        if (RAMRST==1) begin
            UserFPGAState <= USER_FPGA_DELAY_INIT;
            Delay <= 0;
            UserFPGASubState <= 0;
            UserValidCount <= 0;
            ConnectionState <= 0;
            InterruptStatus <= 0;
            Timer <= 0;
            Counter <= 0;
            Clean <= 1;
        end else begin
            // Timer for sending periodic frames
            if (Timer!=0) begin
                Timer <= Timer - 1;
            end
            
            // Counter of completed register reads
            if (UserReadDataValid==1) begin
                UserValidCount <= UserValidCount + 1;
            end
            
            case (UserFPGAState)
                // Wait here to ensure the GigExpedite has locked to
                // our clock
                USER_FPGA_DELAY_INIT: begin
                    Delay <= Delay + 1;
                    if (Delay==24'hffffff) begin
                        UserFPGAState <= USER_FPGA_CLEAN;
                    end
                end
                
                // Reset all connections in case the previous
                // application closed unexpectedly and left some open
                // Write 0 to each connection state then
                // read the status for each connection in turn
                // Loop round until all statuses are zero.
                USER_FPGA_CLEAN: begin
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserFPGASubState==31) begin
                        UserFPGASubState <= 0;
                        UserFPGAState <= USER_FPGA_CLEAN_CHECK;
                        Clean <= 1;
                    end 
                end
                USER_FPGA_CLEAN_CHECK: begin
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserReadDataValid==1 && UserReadData!=0) begin
                        Clean <= 0;
                    end
                    if (UserFPGASubState==511) begin
                        if (Clean==1) begin
                            UserFPGASubState <= 0;
                            UserValidCount <= 0;
                            UserFPGAState <= USER_FPGA_CLEAR_INTS;
                        end else begin
                            Clean <= 1;
                            UserFPGASubState <= 0;
                        end
                    end 
                end
                USER_FPGA_CLEAR_INTS: begin
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserFPGASubState==511) begin
                        UserFPGASubState <= 0;
                        UserFPGAState <= USER_FPGA_INIT;
                    end 
                end
                
                // Initialise register set
                // Configures one connection as a UDP client
                USER_FPGA_INIT: begin
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserFPGASubState==7) begin
                        UserFPGAState <= USER_FPGA_IDLE;
                    end 
                end
                
                // Wait for interrupt from GigExpedite device
                USER_FPGA_IDLE: begin
                    UserFPGASubState <= 0;
                    UserValidCount <= 0;
                    if (UserInterrupt==1) begin
                        // Interrupt has been received
                        UserFPGAState <= USER_FPGA_CHECK_STATE;
                    end
                end

                // Check if the connection state has changed
                USER_FPGA_CHECK_STATE: begin
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserReadDataValid==1 && UserValidCount==0) begin
                        // Store interrupt status
                        InterruptStatus <= UserReadData;
                    end
                    if (UserReadDataValid==1 && UserValidCount==1) begin
                        // Store the new connection state
                        ConnectionState <= UserReadData[5:0];

                        // Next, check if there is outgoing space
                        UserFPGASubState <= 0;
                        UserValidCount <= 0;
                        UserFPGAState <= USER_FPGA_CHECK_SPACE;
                    end
                end

                // Check if there is space in the outgoing GigExpedite buffer
                // and we have data to send back to the host
                USER_FPGA_CHECK_SPACE: begin
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (Timer!=0) begin
                        // Not ready to send data
                        UserFPGASubState <= 0;
                        UserValidCount <= 0;
                        UserFPGAState <= USER_FPGA_IDLE;
                    end else if (UserReadDataValid==1 && UserValidCount==0) begin
                        if (UserReadData[3:0]!=ESTABLISHED) begin
                            // Connection not established
                            UserFPGASubState <= 0;
                            UserValidCount <= 0;
                            UserFPGAState <= USER_FPGA_IDLE;
                        end else if (InterruptStatus[2]==1) begin
                            // Space available - write data to GigExpedite
                            UserFPGASubState <= 0;
                            UserValidCount <= 0;
                            UserFPGAState <= USER_FPGA_WRITE_DATA;
                        end else begin
                            // No space available in GigExpedite FIFO
                            UserFPGASubState <= 0;
                            UserValidCount <= 0;
                            UserFPGAState <= USER_FPGA_IDLE;
                        end
                    end
                end
                
                // Write data to outgoing FIFO
                USER_FPGA_WRITE_DATA: begin
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserFPGASubState==1) begin
                        Timer <= 124999999; // Clock is 125MHz
                        Counter <= Counter+1;
                        UserFPGASubState <= 0;
                        UserValidCount <= 0;
                        UserFPGAState <= USER_FPGA_IDLE;
                    end
                end
                
            endcase
        end
    end
    
    // Map states to Ethernet register accesses
    always @ * begin
        case (UserFPGAState)
            USER_FPGA_CLEAN: begin
                // Reset all connections
                UserRE <= 0;
                UserWE <= 1;
                UserBE <= 2'b11;
                UserAddr <= UserFPGASubState[0]==0 ? CONNECTION_PAGE :
                                                     CONNECTION_STATE;
                UserWriteData <= UserFPGASubState[0]==0 ? UserFPGASubState[4:1] : 0;
            end
            
            USER_FPGA_CLEAN_CHECK: begin
                // Check all connections have been reset
                UserRE <= UserFPGASubState[4:0]==16 ? 1 : 0;
                UserWE <= UserFPGASubState[4:0]==0 ? 1 : 0;
                UserBE <= 2'b11;
                UserAddr <= UserFPGASubState[4:0]==0 ? CONNECTION_PAGE :
                                                       CONNECTION_STATE;
                UserWriteData <= UserFPGASubState[8:5];
            end
            
            USER_FPGA_CLEAR_INTS: begin
                // Clear all remaining interrupt status bits
                UserRE <= UserFPGASubState[4:0]==16 ? 1 : 0;
                UserWE <= UserFPGASubState[4:0]==0 ? 1 : 0;
                UserBE <= 2'b11;
                UserAddr <= UserFPGASubState[4:0]==0 ? CONNECTION_PAGE :
                                                       INTERRUPT_ENABLE_STATUS;
                UserWriteData <= UserFPGASubState[8:5];
            end

            USER_FPGA_INIT: begin
                // Set up registers to make one multicast connection
                // to address 239.0.0.1
                UserRE <= 0;
                UserWE <= 1;
                UserBE <= 2'b11;
                case (UserFPGASubState[4:0])
                    0: begin
                        UserAddr <= CONNECTION_PAGE;
                        UserWriteData <= 0;
                    end
                    1: begin
                        UserAddr <= LOCAL_PORT;
                        UserWriteData <= 16'hd000;
                    end
                    2: begin
                        UserAddr <= REMOTE_IP_ADDR_HIGH;
                        UserWriteData <= 16'hef00;
                    end
                    3: begin
                        UserAddr <= REMOTE_IP_ADDR_LOW;
                        UserWriteData <= 16'h0001;
                    end
                    4: begin
                        UserAddr <= REMOTE_PORT;
                        UserWriteData <= 16'h5002;
                    end
                    5: begin
                        UserAddr <= MTU_TTL;
                        UserWriteData <= 16'h8080;
                    end
                    6: begin
                        UserAddr <= INTERRUPT_ENABLE_STATUS;
                        UserWriteData <= (IE_OUTGOING_NOT_FULL | IE_STATE);
                    end
                    default: begin
                        UserAddr <= CONNECTION_STATE;
                        UserWriteData <= (CONN_UDP | CONN_ENABLE | ESTABLISHED);
                    end
                endcase
            end
                
            USER_FPGA_CHECK_STATE: begin
                // Read connection state then update accordingly
                // The only important state change is to ESTABLISHED which
                // we must acknowledge by changing our state to ESTABLISHED.
                // All other state changes result in us returning to CONNECT
                UserRE <= UserFPGASubState[3:1]==0 ? 1 : 0;
                UserWE <= UserReadDataValid==1 && UserValidCount==1 &&
                          UserReadData[5:0]!=ConnectionState ? 1 : 0;
                UserBE <= 2'b11;
                UserAddr <= UserFPGASubState[3:0]==0 ? INTERRUPT_ENABLE_STATUS :
                                                       CONNECTION_STATE;
                UserWriteData <= (CONN_UDP | CONN_ENABLE | ESTABLISHED);
            end
            
            USER_FPGA_CHECK_SPACE: begin
                // Read from connection status
                UserRE <= UserFPGASubState[3:0]==0 && Timer==0 ? 1 : 0;
                UserWE <= 0;
                UserBE <= 2'b11;
                UserAddr <= CONNECTION_STATE;
                UserWriteData <= 0;
            end
                
            USER_FPGA_WRITE_DATA: begin
                // Write test data to connection FIFO
                UserRE <= 0;
                UserWE <= 1;
                UserBE <= 2'b11;
                UserAddr <= DATA_FIFO;
                UserWriteData <= UserFPGASubState==0 ? 16'h0002 : // Frame length
                                                       Counter;
            end
                
            default: begin
                // Don't do anything
                UserRE <= 0;
                UserWE <= 0;
                UserAddr <= 0;
                UserBE <= 2'b11;
                UserWriteData <= 0;
            end
        endcase
    end

    //////////////////////////////////////////////////////////////////////////
    // SDRAM Buffer
    // Place holder - not used in the example but ties the pins correctly
    
    // Instantiate SDRAM component
    ZestET1_SDRAM  #(.CLOCK_RATE(166666666))
        SDRAMInst (
            .User_CLK(RAMClk),
            .User_CLK90(RAMClk90),
            .User_RST(RAMRST),

            .User_A(0),
            .User_RE(0),
            .User_WE(0),
            .User_Owner(8'h00),
            .User_BE(0),
            .User_DW(0),
            //.User_DR(),
            //.User_DR_Valid(),
            //.User_ValidOwner(),
            //.User_Busy(),
            //.User_InitDone(),

            .DS_CLK_P(DS_CLK_P),
            .DS_CLK_N(DS_CLK_N),
            .DS_A(DS_A),
            .DS_BA(DS_BA),
            .DS_DQ(DS_DQ),
            .DS_DQS(DS_DQS),
            .DS_DM(DS_DM),
            .DS_CAS_N(DS_CAS_N),
            .DS_RAS_N(DS_RAS_N),
            .DS_CKE(DS_CKE),
            .DS_WE_N(DS_WE_N)
        );
    
endmodule


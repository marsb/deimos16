--   ZestET1 Example 5
--   File name: Example5.vhd
--   Version: 1.00
--   Date: 26/7/2009
--
--   ZestET1 Example 5 - UDP multicast test code.
--   Multicasts periodic counter to the network
--
--   Copyright (C) 2009 Orange Tree Technologies Ltd. All rights reserved.
--   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and 
--   modify this logic core in any form such as VHDL source code or EDIF netlist in 
--   FPGA designs that target the ZestET1.
--   Orange Tree Technologies prohibits the use of this logic core in any form such 
--   as VHDL source code or EDIF netlist in FPGA designs that target any other
--   hardware unless the purchaser of the ZestET1 has purchased the appropriate 
--   licence from Orange Tree Technologies. Contact Orange Tree Technologies if you 
--   want to purchase such a licence.
--
--  *****************************************************************************************
--  **
--  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
--  **              provided to you "as is". Orange Tree Technologies and its licensors 
--  **              make and you receive no warranties or conditions, express, implied, 
--  **              statutory or otherwise, and Orange Tree Technologies specifically 
--  **              disclaims any implied warranties of merchantability, non-infringement,
--  **              or fitness for a particular purpose. Orange Tree Technologies does not
--  **              warrant that the functions contained in these designs will meet your 
--  **              requirements, or that the operation of these designs will be 
--  **              uninterrupted or error free, or that defects in the Designs will be 
--  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
--  **              make any representations regarding use or the results of the use of the 
--  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
--  **
--  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
--  **              or its licensors be liable for any loss of data, lost profits, cost or 
--  **              procurement of substitute goods or services, or for any special, 
--  **              incidental, consequential, or indirect damages arising from the use or 
--  **              operation of the designs or accompanying documentation, however caused 
--  **              and on any theory of liability. This limitation will apply even if 
--  **              Orange Tree Technologies has been advised of the possibility of such 
--  **              damage. This limitation shall apply notwithstanding the failure of the 
--  **              essential purpose of any limited remedies herein.
--  **
--  *****************************************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity Example5 is
    port (
        -- Programmable Clock
        CLK : in std_logic;
        
        -- Flash Interface
        Flash_CSOn : out std_logic;
        Flash_CLK : out std_logic;
        Flash_MOSI : out std_logic;
        Flash_MISO : in std_logic;

        -- IO connector
        IO0_In : in std_logic;
        IO0 : inout std_logic_vector(39 downto 0);
        IO3_In : in std_logic_vector(2 downto 0);
        IO3 : inout std_logic_vector(35 downto 0);

        -- Ethernet Interface
        Eth_Clk : out std_logic;
        Eth_CSn : out std_logic;
        Eth_WEn : out std_logic;
        Eth_A : out std_logic_vector(4 downto 0);
        Eth_D : inout std_logic_vector(15 downto 0);
        Eth_BE : out std_logic_vector(1 downto 0);
        Eth_Intn : in std_logic;

        -- DDR SDRAM interface
        DS_CLK_P : out std_logic;
        DS_CLK_N : out std_logic;
        DS_CKE : out std_logic;
        DS_A : out std_logic_vector(12 downto 0);
        DS_BA : out std_logic_vector(1 downto 0);
        DS_CAS_N : out std_logic;
        DS_RAS_N : out std_logic;
        DS_WE_N : out std_logic;
        DS_DQS : inout std_logic_vector(1 downto 0);
        DS_DM : out std_logic_vector(1 downto 0);
        DS_DQ : inout std_logic_vector(15 downto 0)
    );
end Example5;

architecture arch of Example5 is

    --------------------------------------------------------------------------
    -- Declare constants
    
    -- GigExpedite register addresses
    constant LOCAL_IP_ADDR_LOW          : std_logic_vector(4 downto 0) := "00000";
    constant LOCAL_IP_ADDR_HIGH         : std_logic_vector(4 downto 0) := "00001";
    constant LINK_STATUS                : std_logic_vector(4 downto 0) := "00010";
    constant CONNECTION_PAGE            : std_logic_vector(4 downto 0) := "00111";
    constant LOCAL_PORT                 : std_logic_vector(4 downto 0) := "01000";
    constant REMOTE_IP_ADDR_LOW         : std_logic_vector(4 downto 0) := "01001";
    constant REMOTE_IP_ADDR_HIGH        : std_logic_vector(4 downto 0) := "01010";
    constant REMOTE_PORT                : std_logic_vector(4 downto 0) := "01011";
    constant MTU_TTL                    : std_logic_vector(4 downto 0) := "01100";
    constant INTERRUPT_ENABLE_STATUS    : std_logic_vector(4 downto 0) := "01101";
    constant CONNECTION_STATE           : std_logic_vector(4 downto 0) := "01110";
    constant FRAME_LENGTH               : std_logic_vector(4 downto 0) := "01111";
    constant DATA_FIFO                  : std_logic_vector(4 downto 0) := "10000";
    
    -- Connection states (for CONNECTION_STATE reg)
    constant CLOSED       : std_logic_vector(15 downto 0) := X"0000";
    constant LISTEN       : std_logic_vector(15 downto 0) := X"0001";
    constant CONNECT      : std_logic_vector(15 downto 0) := X"0002";
    constant ESTABLISHED  : std_logic_vector(15 downto 0) := X"0003";
    constant CONN_UDP     : std_logic_vector(15 downto 0) := X"0000";
    constant CONN_TCP     : std_logic_vector(15 downto 0) := X"0010";
    constant CONN_ENABLE  : std_logic_vector(15 downto 0) := X"0020";
    
    -- Interrupt enable and status bits (for INTERRUPT_ENABLE_STATUS reg)
    constant IE_INCOMING          : std_logic_vector(15 downto 0) := X"0001";
    constant IE_OUTGOING_EMPTY    : std_logic_vector(15 downto 0) := X"0002";
    constant IE_OUTGOING_NOT_FULL : std_logic_vector(15 downto 0) := X"0004";
    constant IE_STATE             : std_logic_vector(15 downto 0) := X"0008";

    --------------------------------------------------------------------------
    -- Declare signals
    
    -- Clocks and reset
    signal RST : std_logic;
    signal RAMRST : std_logic;
    signal RAMClk : std_logic;
    signal RAMClk90 : std_logic;
    signal EthClk : std_logic;

    -- Ethernet interface state machine
    signal UserWE : std_logic;
    signal UserRE : std_logic;
    signal UserAddr : std_logic_vector(4 downto 0);
    signal UserBE : std_logic_vector(1 downto 0);
    signal UserWriteData : std_logic_vector(15 downto 0);
    signal UserReadData : std_logic_vector(15 downto 0);
    signal UserReadDataValid : std_logic;
    signal UserInterrupt : std_logic;

    signal Delay : std_logic_vector(23 downto 0);
    signal Timer : std_logic_vector(31 downto 0);
    signal Counter : std_logic_vector(15 downto 0);
    signal UserFPGASubState : std_logic_vector(15 downto 0);
    signal UserValidCount : std_logic_vector(15 downto 0);
    signal ConnectionState : std_logic_vector(5 downto 0);
    signal InterruptStatus : std_logic_vector(15 downto 0);
    signal Clean : std_logic;
    
    type STATE_TYPE is (
        USER_FPGA_DELAY_INIT,
        USER_FPGA_CLEAN,
        USER_FPGA_CLEAN_CHECK,
        USER_FPGA_CLEAR_INTS,
        USER_FPGA_INIT,
        USER_FPGA_IDLE,
        USER_FPGA_CHECK_STATE,
        USER_FPGA_CHECK_SPACE,
        USER_FPGA_WRITE_DATA
    );
    signal UserFPGAState : STATE_TYPE;

begin
    
    -- Tie unused signals
    Flash_CSOn <= '1';
    Flash_CLK <= '1';
    Flash_MOSI <= '1';
    IO0 <=  (others=>'Z');
    IO3 <= (others=>'Z');
    
    -- Reset block
    ROCInst : ROC port map (O=>RST);
    
    --------------------------------------------------------------------------
    -- Instantiate clocks
    -- (Assumes default 125MHz reference clock)
    ClockInst : entity work.ZestET1_Clocks
        port map (
            RST => RST,
            RefClk => CLK,
            EthClk => EthClk,
            RAMRST => RAMRST,
            RAMClk => RAMClk,
            RAMClk90 => RAMClk90    
        );

    --------------------------------------------------------------------------
    -- Ethernet interface test code
    EthernetInst : entity work.ZestET1_Ethernet
        port map (
            User_CLK => EthClk,
            User_RST => RST,

            -- User interface
            User_WE => UserWE,
            User_RE => UserRE,
            User_Addr => UserAddr,
            User_Owner => X"00",
            User_WriteData => UserWriteData,
            User_BE => UserBE,
            User_ReadData => UserReadData,
            User_ReadDataValid => UserReadDataValid,
            --User_ValidOwner => ,
            User_Interrupt => UserInterrupt,
            
            -- Interface to GigExpedite
            Eth_Clk => Eth_Clk,
            Eth_CSn => Eth_CSn,
            Eth_WEn => Eth_WEn,
            Eth_A => Eth_A,
            Eth_D => Eth_D,
            Eth_BE => Eth_BE,
            Eth_Intn => Eth_Intn
        );

    -- State machine to read/write Ethernet
    -- Note: use RAMRST to keep the state machine in reset until 
    -- the DCMs are locked
    process (RAMRST, EthClk) begin
        if (RAMRST='1') then
            UserFPGAState <= USER_FPGA_DELAY_INIT;
            Delay <= (others=>'0');
            UserFPGASubState <= (others=>'0');
            UserValidCount <= (others=>'0');
            ConnectionState <= (others=>'0');
            InterruptStatus <= (others=>'0');
            Timer <= (others=>'0');
            Counter <= (others=>'0');
            Clean <= '1';
        elsif (EthClk'event and EthClk='1') then
            -- Timer for sending periodic frames
            if (Timer/=X"00000000") then
                Timer <= Timer - 1;
            end if;
            
            -- Counter of completed register reads
            if (UserReadDataValid='1') then
                UserValidCount <= UserValidCount + 1;
            end if;
            
            case (UserFPGAState) is
                -- Wait here to ensure the GigExpedite has locked to
                -- our clock
                when USER_FPGA_DELAY_INIT =>
                    Delay <= Delay + 1;
                    if (Delay=X"FFFFFF") then
                        UserFPGAState <= USER_FPGA_CLEAN;
                    end if;
                
                -- Reset all connections in case the previous
                -- application closed unexpectedly and left some open
                -- Write 0 to each connection state then
                -- read the status for each connection in turn
                -- Loop round until all statuses are zero.
                when USER_FPGA_CLEAN =>
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserFPGASubState=X"001f") then
                        UserFPGASubState <= (others=>'0');
                        UserFPGAState <= USER_FPGA_CLEAN_CHECK;
                        Clean <= '1';
                    end if;
                when USER_FPGA_CLEAN_CHECK =>
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserReadDataValid='1' and UserReadData/=X"0000") then
                        Clean <= '0';
                    end if;
                    if (UserFPGASubState=X"001ff") then
                        if (Clean='1') then
                            UserFPGASubState <= (others=>'0');
                            UserValidCount <= (others=>'0');
                            UserFPGAState <= USER_FPGA_CLEAR_INTS;
                        else
                            Clean <= '1';
                            UserFPGASubState <= (others=>'0');
                        end if;
                    end if;
                when USER_FPGA_CLEAR_INTS =>
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserFPGASubState=X"01ff") then
                        UserFPGASubState <= (others=>'0');
                        UserValidCount <= (others=>'0');
                        UserFPGAState <= USER_FPGA_INIT;
                    end if;
                
                -- Initialise register set
                -- Configures one connection as a TCP server waiting
                -- for a connection from a client (i.e. the host program)
                when USER_FPGA_INIT => 
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserFPGASubState=X"0007") then
                        UserFPGAState <= USER_FPGA_IDLE;
                    end if;
                
                -- Wait for interrupt from GigExpedite device
                when USER_FPGA_IDLE => 
                    UserFPGASubState <= (others=>'0');
                    UserValidCount <= (others=>'0');
                    if (UserInterrupt='1') then
                        -- Interrupt has been received
                        UserFPGAState <= USER_FPGA_CHECK_STATE;
                    end if;

                -- Check if the connection state has changed
                when USER_FPGA_CHECK_STATE => 
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserReadDataValid='1' and UserValidCount=X"0000") then
                        -- Store interrupt status
                        InterruptStatus <= UserReadData;
                    end if;
                    if (UserReadDataValid='1' and UserValidCount=X"0001") then
                        -- Store the new connection state
                        ConnectionState <= UserReadData(5 downto 0);

                        -- Next, check if there is outgoing space
                        UserFPGASubState <= (others=>'0');
                        UserValidCount <= (others=>'0');
                        UserFPGAState <= USER_FPGA_CHECK_SPACE;
                    end if;

                -- Check if there is space in the outgoing GigExpedite buffer
                -- and we have data to send back to the host
                when USER_FPGA_CHECK_SPACE => 
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (Timer/=X"00000000") then 
                        -- Not ready to send data
                        UserFPGASubState <= (others=>'0');
                        UserValidCount <= (others=>'0');
                        UserFPGAState <= USER_FPGA_IDLE;
                    elsif (UserReadDataValid='1' and UserValidCount=X"0000") then
                        if (UserReadData(3 downto 0)/=ESTABLISHED(3 downto 0)) then
                            -- Connection not established
                            UserFPGASubState <= (others=>'0');
                            UserValidCount <= (others=>'0');
                            UserFPGAState <= USER_FPGA_IDLE;
                        elsif (InterruptStatus(2)='1') then
                            -- Space available - write data to GigExpedite
                            UserFPGASubState <= (others=>'0');
                            UserValidCount <= (others=>'0');
                            UserFPGAState <= USER_FPGA_WRITE_DATA;
                        else
                            -- No space available in GigExpedite FIFO
                            UserFPGASubState <= (others=>'0');
                            UserValidCount <= (others=>'0');
                            UserFPGAState <= USER_FPGA_IDLE;
                        end if;
                    end if;
                
                -- Write data to outgoing FIFO
                when USER_FPGA_WRITE_DATA =>
                    UserFPGASubState <= UserFPGASubState + 1;
                    if (UserFPGASubState=X"0001") then
                        Timer <= conv_std_logic_vector(124999999, 32); -- Clock is 125MHz
                        Counter <= Counter+1;
                        UserFPGASubState <= (others=>'0');
                        UserValidCount <= (others=>'0');
                        UserFPGAState <= USER_FPGA_IDLE;
                    end if;
                
                when others => null;
            end case;
        end if;
    end process;
    
    -- Map states to Ethernet register accesses
    process (UserFPGAState, UserFPGASubState, UserReadDataValid,
             UserValidCount, UserReadData, ConnectionState, InterruptStatus,
             Counter, Timer) begin
        case UserFPGAState is
            when USER_FPGA_CLEAN =>
                -- Reset all connections
                UserRE <= '0';
                UserWE <= '1';
                UserBE <= "11";
                if (UserFPGASubState(0)='0') then 
                    UserAddr <= CONNECTION_PAGE;
                    UserWriteData <= X"000" & UserFPGASubState(4 downto 1);
                else
                    UserAddr <= CONNECTION_STATE;
                    UserWriteData <= (others=>'0');
                end if;
            
            when USER_FPGA_CLEAN_CHECK =>
                -- Check all connections have been reset
                if (UserFPGASubState(4 downto 0)="10000") then
                    UserRE <= '1';
                else
                    UserRE <= '0';
                end if;
                if (UserFPGASubState(4 downto 0)="00000") then
                    UserWE <= '1';
                    UserAddr <= CONNECTION_PAGE;
                else
                    UserWE <= '0';
                    UserAddr <= CONNECTION_STATE;
                end if;
                UserBE <= "11";
                UserWriteData <= X"000" & UserFPGASubState(8 downto 5);
            
            when USER_FPGA_CLEAR_INTS =>
                -- Clear state change interrupts
                UserBE <= "11";
                if (UserFPGASubState(4 downto 0)="00000") then
                    UserRE <= '0';
                    UserWE <= '1';
                    UserAddr <= CONNECTION_PAGE;
                elsif (UserFPGASubState(4 downto 0)="10000") then
                    UserRE <= '1';
                    UserWE <= '0';
                    UserAddr <= INTERRUPT_ENABLE_STATUS;
                else
                    UserRE <= '0';
                    UserWE <= '0';
                end if;
                UserWriteData <= X"000" & UserFPGASubState(8 downto 5);
            
            when USER_FPGA_INIT => 
                -- Set up registers to make one multicast connection
                -- to address 239.0.0.1
                UserRE <= '0';
                UserWE <= '1';
                UserBE <= "11";
                case UserFPGASubState(4 downto 0) is
                    when "00000" =>
                        UserAddr <= CONNECTION_PAGE;
                        UserWriteData <= (others=>'0');
                    when "00001" =>
                        UserAddr <= LOCAL_PORT;
                        UserWriteData <= X"d000";
                    when "00010" =>
                        UserAddr <= REMOTE_IP_ADDR_HIGH;
                        UserWriteData <= X"ef00";
                    when "00011" =>
                        UserAddr <= REMOTE_IP_ADDR_LOW;
                        UserWriteData <= X"0001";
                    when "00100" =>
                        UserAddr <= REMOTE_PORT;
                        UserWriteData <= X"5002";
                    when "00101" => 
                        UserAddr <= MTU_TTL;
                        UserWriteData <= X"8080";
                    when "00110" =>
                        UserAddr <= INTERRUPT_ENABLE_STATUS;
                        UserWriteData <= (IE_OUTGOING_NOT_FULL or IE_STATE);
                    when others =>
                        UserAddr <= CONNECTION_STATE;
                        UserWriteData <= (CONN_UDP or CONN_ENABLE or ESTABLISHED);
                end case;
                
            when USER_FPGA_CHECK_STATE => 
                -- Read connection state then update accordingly
                -- The only important state change is to ESTABLISHED which
                -- we must acknowledge by changing our state to ESTABLISHED.
                -- All other state changes result in us returning to LISTEN
                -- to wait for another connection from a new client.
                if (UserFPGASubState(3 downto 1)="000") then 
                    UserRE <= '1';
                else
                    UserRE <= '0';
                end if;
                if (UserReadDataValid='1' and UserValidCount=X"0001" and
                    UserReadData(5 downto 0)/=ConnectionState) then
                    UserWE <= '1';
                else
                    UserWE <= '0';
                end if;
                UserBE <= "11";
                if (UserFPGASubState(3 downto 0)=X"0") then
                    UserAddr <= INTERRUPT_ENABLE_STATUS;
                else
                    UserAddr <= CONNECTION_STATE;
                end if;
                UserWriteData <= (CONN_UDP or CONN_ENABLE or ESTABLISHED);
            
            when USER_FPGA_CHECK_SPACE =>
                -- Read from connection status and then interrupt status
                if (UserFPGASubState(3 downto 0)=X"0" and
                    Timer=X"00000000") then
                    UserRE <= '1';
                else
                    UserRE <= '0';
                end if;
                UserWE <= '0';
                UserBE <= "11";
                UserAddr <= CONNECTION_STATE;
                UserWriteData <= (others=>'0');
                
            when USER_FPGA_WRITE_DATA => 
                -- Write test data to connection FIFO
                UserRE <= '0';
                UserWE <= '1';
                UserBE <= "11";
                UserAddr <= DATA_FIFO;
                if (UserFPGASubState=X"0000") then
                    UserWriteData <= X"0002";   -- Frame length
                else
                    UserWriteData <= Counter;
                end if;
                
            when others =>
                -- Don't do anything
                UserRE <= '0';
                UserWE <= '0';
                UserAddr <= (others=>'0');
                UserBE <= "11";
                UserWriteData <= (others=>'0');
            
        end case;
    end process;

    --------------------------------------------------------------------------
    -- SDRAM Buffer
    -- Place holder - not used in the example but ties the pins correctly
    
    -- Instantiate SDRAM component
    SDRAMInst : entity work.ZestET1_SDRAM
        port map (
            User_CLK => RAMClk,
            User_CLK90 => RAMClk90,
            User_RST => RAMRST,

            User_A => (others=>'0'),
            User_RE => '0',
            User_WE => '0',
            User_Owner => X"00",
            User_BE => (others=>'0'),
            User_DW => (others=>'0'),
            --User_DR => ,
            --User_DR_Valid => ,
            --User_ValidOwner => ,
            --User_Busy => ,
            --User_InitDone => ,

            DS_CLK_P => DS_CLK_P,
            DS_CLK_N => DS_CLK_N,
            DS_A => DS_A,
            DS_BA => DS_BA,
            DS_DQ => DS_DQ,
            DS_DQS => DS_DQS,
            DS_DM => DS_DM,
            DS_CAS_N => DS_CAS_N,
            DS_RAS_N => DS_RAS_N,
            DS_CKE => DS_CKE,
            DS_WE_N => DS_WE_N
        );
    
end arch;


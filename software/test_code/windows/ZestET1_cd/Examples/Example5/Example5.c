//////////////////////////////////////////////////////////////////////
//
// File:      Example5.c
//
// Purpose:
//    ZestET1 Example Programs
//    UDP Multicast example
//  
// Copyright (c) 2009 Orange Tree Technologies.
// May not be reproduced without permission.
//
//////////////////////////////////////////////////////////////////////

#include <Winsock2.h>
#include <Ws2tcpip.h>
#include <stdio.h>
#include <stdlib.h>
#include "ZestET1.h"

//  Broadcast address for example
#define ZESTET1_MULTICAST_ADDR "239.0.0.1"
#define ZESTET1_MULTICAST_PORT 0x5002

//
// Multicasting functions to join and leave a group
//
static int JoinGroup(SOCKET sd, unsigned long grpaddr,
                     unsigned long iaddr)
{
    struct ip_mreq imr; 

    imr.imr_multiaddr.s_addr  = grpaddr;
    imr.imr_interface.s_addr  = iaddr;
    return setsockopt(sd, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                      (const char *)&imr, sizeof(imr));  
}
static int LeaveGroup(SOCKET sd, unsigned long grpaddr, 
                      unsigned long iaddr)
{
    struct ip_mreq imr;

    imr.imr_multiaddr.s_addr  = grpaddr;
    imr.imr_interface.s_addr  = iaddr;
    return setsockopt(sd, IPPROTO_IP, IP_DROP_MEMBERSHIP, 
                      (const char *)&imr, sizeof(imr));
}


//
// Error handler function
//
void ErrorHandler(const char *Function, 
                  ZESTET1_CARD_INFO *CardInfo,
                  ZESTET1_STATUS Status,
                  const char *Msg)
{
    printf("**** Example5 - Function %s returned an error\n        \"%s\"\n\n", Function, Msg);
    exit(1);
}

//
// Main program
//
int main(int argc, char **argv)
{
    unsigned long Count;
    unsigned long NumCards;
    ZESTET1_CARD_INFO *CardInfo;
    SOCKET Socket;
    int Flag = 1;
    struct sockaddr_in LocalIP;
    struct sockaddr_in TargetIP;
    int TargetLen;
    int Result;
    unsigned short Value;

    //
    // Install an error handler and initialise library
    //
    ZestET1RegisterErrorHandler(ErrorHandler);
    ZestET1Init();

    //
    // Request information about the system
    // Wait for 2 seconds (per interface) for boards to respond to query
    //
    printf("Searching for ZestET1 cards...\n");
    ZestET1CountCards(&NumCards, &CardInfo, 2000);
    printf("%ld available cards in the system\n\n\n", NumCards);
    if (NumCards==0)
    {
        printf("No cards found\n");
        exit(1);
    }

    for (Count=0; Count<NumCards; Count++)
    {
        printf("%ld : SerialNum = %ld, IPAddress = %d.%d.%d.%d\n",
            Count, CardInfo[Count].SerialNumber,
            CardInfo[Count].IPAddr[0], CardInfo[Count].IPAddr[1],
            CardInfo[Count].IPAddr[2], CardInfo[Count].IPAddr[3]);
    }

    //
    // Configure the FPGA directly from a file
    //
    ZestET1ConfigureFromFile(&CardInfo[0], "Example5.bit");

    //
    // Open socket to receive multicast data
    //
    Socket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (Socket<0)
    {
        printf("Could not open socket to receive data\n");
        exit(1);
    }

    //
    // Set reuse port to on to allow multiple binds per host
    //
    if (setsockopt(Socket, SOL_SOCKET, SO_REUSEADDR, (char*)&Flag,
                   sizeof(Flag))<0)
    {
        closesocket(Socket);
        printf("Could not configure socket to receive data\n");
        exit(1);
    }

    //
    // Bind to port for receiving responses
    // NB: This uses the Windows routing tables to receive packets when multiple
    // interfaces are present.  To set the routing tables, type this at the command line:
    //   route add 224.0.0.0 mask 240.0.0.0 192.168.1.1 metric 1
    // where 192.168.1.1 is the IP address of the port connected to the ZestET1
    //
    LocalIP.sin_family = AF_INET;
    LocalIP.sin_addr.s_addr = htonl(INADDR_ANY);
    LocalIP.sin_port = htons(ZESTET1_MULTICAST_PORT);
    Result = bind(Socket, (const struct sockaddr *)&LocalIP,
                  sizeof(struct sockaddr_in));
    if (Result<0)
    {
        closesocket(Socket);
        printf("Could not configure socket to receive data\n");
        exit(1);
    }

    //
    // Join multicast group
    //
    if (JoinGroup(Socket, inet_addr(ZESTET1_MULTICAST_ADDR), 
                  htonl(INADDR_ANY))<0)
    {
        closesocket(Socket);
        printf("Could not join multicast group\n");
        exit(1);
    }

    //
    // Receive data from card
    //
    while (1)
    {
        printf("Waiting for data from ZestET1\n");
        TargetLen = sizeof(TargetIP);
        Result = recvfrom(Socket, (char *)&Value, sizeof(Value), 0,
                          (struct sockaddr *)&TargetIP, &TargetLen);
        if (Result==2)
        {
            printf("Received %d\n", ((Value>>8)&0xff) | ((Value<<8)&0xff00));
        }
    }

    LeaveGroup(Socket, inet_addr(ZESTET1_MULTICAST_ADDR), 
               htonl(INADDR_ANY));
    shutdown(Socket, SD_BOTH);
    closesocket(Socket);

    //
    // Free the card information structure
    //
    ZestET1FreeCards(CardInfo);

    //
    // Close library
    //
    ZestET1Close();

    return 0;
}

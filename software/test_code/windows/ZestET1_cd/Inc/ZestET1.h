/**********************************************************
*                                                         *
* (c) 2009 Orange Tree Technologies Ltd                   *
*                                                         *
* ZestET1.h                                               *
* Version 1.1                                             *
*                                                         *
* Header file for ZestET1 Ethernet FPGA card              *
*                                                         *
**********************************************************/

#ifndef __ZESTET1_H__
#define __ZESTET1_H__

#ifdef __cplusplus
extern "C"
{
#endif


/**********************************************
* Handle for referencing configuration images *
**********************************************/
typedef void *ZESTET1_HANDLE;
typedef void *ZESTET1_IMAGE;


/*******************
* Valid FPGA types *
*******************/
typedef enum
{
    ZESTET1_FPGA_UNKNOWN,
    ZESTET1_XC3S1400A,
} ZESTET1_FPGA_TYPE;


/*****************************
* Card information structure *
*****************************/
typedef struct
{
    // These must be filled in before calling functions
    unsigned char IPAddr[4];
    unsigned short ControlPort;
    unsigned long Timeout;

    // These are for information purposes only
    unsigned short HTTPPort;
    unsigned char MACAddr[6];
    unsigned char SubNet[4];
    unsigned char Gateway[4];
    unsigned long SerialNumber;
    ZESTET1_FPGA_TYPE FPGAType;
    unsigned long MemorySize;
    unsigned long FirmwareVersion;
    unsigned long HardwareVersion;
} ZESTET1_CARD_INFO;

/****************************
* Data transfer definitions *
****************************/
typedef void *ZESTET1_CONNECTION;
typedef enum
{
    ZESTET1_TYPE_TCP,
    ZESTET1_TYPE_UDP
} ZESTET1_CONNECTION_TYPE;

/************************
* Function return codes *
************************/
#define ZESTET1_INFO_BASE 0
#define ZESTET1_WARNING_BASE 0x4000
#define ZESTET1_ERROR_BASE 0x8000
typedef enum
{
    ZESTET1_SUCCESS = ZESTET1_INFO_BASE,
    ZESTET1_MAX_INFO,

    ZESTET1_MAX_WARNING = ZESTET1_WARNING_BASE,

    ZESTET1_SOCKET_ERROR = ZESTET1_ERROR_BASE,
    ZESTET1_INTERNAL_ERROR,
    ZESTET1_ILLEGAL_STATUS_CODE,
    ZESTET1_NULL_PARAMETER,
    ZESTET1_ILLEGAL_CLOCK_RATE,
    ZESTET1_OUT_OF_MEMORY,
    ZESTET1_INVALID_CONNECTION_TYPE,
    ZESTET1_ILLEGAL_CONNECTION,
    ZESTET1_SOCKET_CLOSED,
    ZESTET1_TIMEOUT,
    ZESTET1_ILLEGAL_IMAGE_HANDLE,
    ZESTET1_ILLEGAL_FILE,
    ZESTET1_FILE_NOT_FOUND,
    ZESTET1_FILE_ERROR,
    ZESTET1_INVALID_PART_TYPE,

    ZESTET1_MAX_ERROR
} ZESTET1_STATUS;
typedef void (*ZESTET1_ERROR_FUNC)(const char *Function, 
                                   ZESTET1_CARD_INFO *CardInfo,
                                   ZESTET1_STATUS Status,
                                   const char *Msg);


/**********************
* Function prototypes *
**********************/
ZESTET1_STATUS ZestET1Init(void);
ZESTET1_STATUS ZestET1Close(void);
ZESTET1_STATUS ZestET1CountCards(unsigned long *NumCards,
                                 ZESTET1_CARD_INFO **CardInfo,
                                 unsigned long Wait);
ZESTET1_STATUS ZestET1GetCardInfo(ZESTET1_CARD_INFO *CardInfo);
ZESTET1_STATUS ZestET1FreeCards(ZESTET1_CARD_INFO *CardInfo);

ZESTET1_STATUS ZestET1RegisterErrorHandler(ZESTET1_ERROR_FUNC Function);
ZESTET1_STATUS ZestET1GetErrorMessage(ZESTET1_STATUS Status,
                                      char **Buffer);

ZESTET1_STATUS ZestET1ConfigureFromFile(ZESTET1_CARD_INFO *CardInfo,
                                        char *FileName);
ZESTET1_STATUS ZestET1LoadFile(char *FileName,
                               ZESTET1_IMAGE *Image);
ZESTET1_STATUS ZestET1Configure(ZESTET1_CARD_INFO *CardInfo,
                                ZESTET1_IMAGE Image);
ZESTET1_STATUS ZestET1ProgramFlashFromFile(ZESTET1_CARD_INFO *CardInfo,
                                           char *FileName);
ZESTET1_STATUS ZestET1ProgramFlash(ZESTET1_CARD_INFO *CardInfo,
                                   ZESTET1_IMAGE Image);
ZESTET1_STATUS ZestET1EraseFlash(ZESTET1_CARD_INFO *CardInfo);
ZESTET1_STATUS ZestET1RegisterImage(void *Buffer,
                                    unsigned long BufferLength,
                                    ZESTET1_IMAGE *Image);
ZESTET1_STATUS ZestET1FreeImage(ZESTET1_IMAGE Image);

ZESTET1_STATUS ZestET1SetClockRate(ZESTET1_CARD_INFO *CardInfo,
                                   unsigned long Rate,
                                   unsigned long *Actual);

ZESTET1_STATUS ZestET1OpenConnection(ZESTET1_CARD_INFO *CardInfo,
                                     ZESTET1_CONNECTION_TYPE Type,
                                     unsigned short Port,
                                     unsigned short LocalPort,
                                     ZESTET1_CONNECTION *Connection);
ZESTET1_STATUS ZestET1CloseConnection(ZESTET1_CONNECTION Connection);
ZESTET1_STATUS ZestET1WriteData(ZESTET1_CONNECTION Connection,
                                void *Buffer,
                                unsigned long Length,
                                unsigned long *Written,
                                unsigned long Timeout);
ZESTET1_STATUS ZestET1ReadData(ZESTET1_CONNECTION Connection,
                               void *Buffer,
                               unsigned long Length,
                               unsigned long *Read,
                               unsigned long Timeout);
ZESTET1_STATUS ZestET1WriteFlash(ZESTET1_CARD_INFO *CardInfo,
                                 unsigned long Address,
                                 void *Buffer,
                                 unsigned long Length);
ZESTET1_STATUS ZestET1ReadFlash(ZESTET1_CARD_INFO *CardInfo,
                                unsigned long Address,
                                void *Buffer,
                                unsigned long Length);


#ifdef __cplusplus
}
#endif

#endif // __ZESTET1_H__


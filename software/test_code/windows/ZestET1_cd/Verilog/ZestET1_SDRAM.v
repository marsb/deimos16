/* ZestET1 DDR SDRAM
   File name: ZestET1_SDRAM.v
   Version: 1.20
   Date: 11/5/2011

   DDR SDRAM controller for user FPGA on ZestET1 boards
    
   Copyright (C) 2011 Orange Tree Technologies Ltd. All rights reserved.
   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and
   modify this logic core in any form including but not limited to Verilog source code or
   EDIF netlist in FPGA designs that target the ZestET1.
   Orange Tree Technologies prohibits the use of this logic core or any modification of
   it in any form including but not limited to Verilog source code or EDIF netlist in
   FPGA or ASIC designs that target any other hardware unless the purchaser of the
   ZestET1 has purchased the appropriate licence from Orange Tree Technologies.
   Contact Orange Tree Technologies if you want to purchase such a licence.

  *****************************************************************************************
  **
  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
  **              provided to you "as is". Orange Tree Technologies and its licensors 
  **              make and you receive no warranties or conditions, express, implied, 
  **              statutory or otherwise, and Orange Tree Technologies specifically 
  **              disclaims any implied warranties of merchantability, non-infringement,
  **              or fitness for a particular purpose. Orange Tree Technologies does not
  **              warrant that the functions contained in these designs will meet your 
  **              requirements, or that the operation of these designs will be 
  **              uninterrupted or error free, or that defects in the Designs will be 
  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
  **              make any representations regarding use or the results of the use of the 
  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
  **
  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
  **              or its licensors be liable for any loss of data, lost profits, cost or 
  **              procurement of substitute goods or services, or for any special, 
  **              incidental, consequential, or indirect damages arising from the use or 
  **              operation of the designs or accompanying documentation, however caused 
  **              and on any theory of liability. This limitation will apply even if 
  **              Orange Tree Technologies has been advised of the possibility of such 
  **              damage. This limitation shall apply notwithstanding the failure of the 
  **              essential purpose of any limited remedies herein.
  **
  *****************************************************************************************
*/

`timescale 1ns / 1ps

module ZestET1_SDRAM
(
    // User interface
    input User_CLK,                 // clock from user logic
    input User_CLK90,               // 90 degree shifted clock from user logic
    input User_RST,                 // reset
    input [23:0] User_A,            // word address
    input User_RE,                  // read strobe
    input User_WE,                  // write strobe
    input [7:0] User_Owner,         // access owner
    output User_DR_Valid,           // read data valid strobe active high
    output [7:0] User_ValidOwner,   // read data owner
    input [3:0] User_BE,            // byte enables for write
    input [31:0] User_DW,           // 32-bit data bus for writing to SDRAM
    output [31:0] User_DR,          // 32-bit data bus for reading from SDRAM
    output User_Busy,               // SDRAM busy signal (active high)
    output reg User_InitDone,       // SDRAM controller initialisation complete
    
    // SDRAM interface
    output DS_CLK_P,
    output DS_CLK_N,
    output [12:0] DS_A,
    output [1:0] DS_BA,
    inout [15:0] DS_DQ,
    output [1:0] DS_DM,
    inout [1:0] DS_DQS,
    output DS_CAS_N,
    output DS_RAS_N,
    output DS_CKE,
    output DS_WE_N
);

    parameter CLOCK_RATE = 166666666;

    // Declare constants
    // The column size changes with the size of the device
    localparam integer ROW_BITS = 13;
    localparam integer COLUMN_BITS = 10;

    localparam integer CLOCK_RATE_1000 = CLOCK_RATE/1000; // Clock divided by 1000
    
    localparam integer tREF = 57;           // ms
    localparam integer tRP = 15;            // ns
    localparam integer tRFC = 72;           // ns
    localparam integer tMRD = 2;            // cycles
    localparam integer tRCD = 15;           // ns
    localparam integer tRC = 60;            // ns
    localparam integer tRRD = 13;           // ns (at least 1 cycle)
    localparam integer tWR = 3;             // cycles
    localparam integer tRAS = 42;           // ns
    localparam integer tCAS = 3;            // cycles
    localparam integer tWTR = 1;            // cycles

    // NB: For simulation, it may be useful to drop the initialisation time
    localparam integer INIT_CKE_TIME = (200*CLOCK_RATE_1000)/1000;    // 200 us
    //localparam integer INIT_CKE_TIME = (12*CLOCK_RATE_1000)/1000;    // 12 us for simulation
    localparam integer INIT_PRECHARGE1_TIME = INIT_CKE_TIME + (1+(10*CLOCK_RATE_1000)/1000000);
    localparam integer INIT_LOAD_EXTENDED_MODE_TIME = INIT_PRECHARGE1_TIME + (1+(tRP*CLOCK_RATE_1000)/1000000);
    localparam integer INIT_LOAD_MODE1_TIME = INIT_LOAD_EXTENDED_MODE_TIME + tMRD;
    localparam integer INIT_PRECHARGE2_TIME = INIT_LOAD_MODE1_TIME + tMRD;
    localparam integer INIT_REFRESH1_TIME = INIT_PRECHARGE2_TIME + (1+(tRP*CLOCK_RATE_1000)/1000000);
    localparam integer INIT_REFRESH2_TIME = INIT_REFRESH1_TIME + (1+(tRFC*CLOCK_RATE_1000)/1000000);
    localparam integer INIT_LOAD_MODE2_TIME = INIT_REFRESH2_TIME + tRFC;
    localparam integer INIT_LENGTH = INIT_LOAD_MODE2_TIME + 200;
    
    localparam integer REFRESH_CYCLES = (tREF*CLOCK_RATE_1000)/8192;

    // Mode word.  Select CAS latency of 2.5, burst of 2
    localparam integer MODE1_WORD = 15'b000000101100001;
    localparam integer MODE2_WORD = 15'b000000001100001;
    localparam integer EXTENDED_MODE_WORD = 15'b010000000000010;
    
    // Delays following each activity
    localparam integer ACTIVE_WAIT_CYCLES = (tRCD*CLOCK_RATE_1000)/1000000;
    localparam integer PRECHARGE_WAIT_CYCLES = (tRP*CLOCK_RATE_1000)/1000000;
    localparam integer REFRESH_WAIT_CYCLES = (tRFC*CLOCK_RATE_1000)/1000000;
    localparam integer tRAS_CYCLES = (tRAS*CLOCK_RATE_1000)/1000000-1;
    localparam integer tCAS_CYCLES = tCAS;
    localparam integer tWTR_CYCLES = tWTR+1;
    localparam integer tWR_CYCLES = tWR+1;
    localparam integer tRRD_CYCLES = (tRRD*CLOCK_RATE_1000)/1000000-1;
    localparam integer tRC_CYCLES = (tRC*CLOCK_RATE_1000)/1000000-1;
    
    // Declare signals
    wire CLK;
    wire CLK90;
    wire RST;
    
    // Input register
    reg [23:0] InputRegAddr;
    reg [31:0] InputRegData;
    reg [7:0] InputRegOwner;
    reg [3:0] InputRegBE;
    reg InputRegRE;
    reg InputRegWE;
    reg InputRegFull;
    reg InputRegPrechargeNeeded;
    reg InputRegReadReady;
    reg InputRegWriteReady;
    wire InputRegRead;

    // Commands are in priority order
    localparam integer PRECHARGE_BIT    = 0;
    localparam integer AUTO_REFRESH_BIT = 1;
    localparam integer ACTIVE_BIT       = 2;
    localparam integer READ_BIT         = 3;
    localparam integer WRITE_BIT        = 4;
    localparam integer LOAD_MODE_BIT    = 5;
    localparam integer PRECHARGE    = 6'b000001;
    localparam integer AUTO_REFRESH = 6'b000010;
    localparam integer ACTIVE       = 6'b000100;
    localparam integer READ         = 6'b001000;
    localparam integer WRITE        = 6'b010000;
    localparam integer LOAD_MODE    = 6'b100000;
    reg [5:0] NextCommand;
    reg [5:0] Command;
    reg LastCommandWrite;
    reg [31:0] DataOut;
    reg [1:0] RegBankAddr;
    wire [1:0] InputRegBankAddr;
    wire [1:0] NewBankAddr;
    reg [1:0] LastActiveBank;
    reg [12:0] RegRowAddr;
    wire [12:0] InputRegRowAddr;
    wire [ROW_BITS-1:0] NewRowAddr;
    reg [12:0] RegColAddr;
    wire [12:0] InputRegColAddr;
    reg [12:0] PreChargeAddr;
    reg [12:0] RegPreChargeAddr;
    reg [31:0] RegData;
    reg [7:0] RegOwner;
    reg [3:0] RegBE;
    reg [14:0] RegModeWord;

    reg [3:0] OpenBanks;
    reg OpenBanksInputReg;
    reg [ROW_BITS-1:0] BankAddrs[0:3];
    wire NewBankMatch;
    wire NewOpenBank;
    
    // Initialisation signals
    reg CKEVal;
    reg [15:0] InitCounter;
    reg InitCounterCKE;
    reg InitCounterPreCharge;
    reg InitCounterRefresh;
    reg InitCounterLoad1;
    reg InitCounterLoad2;
    reg InitCounterLoad3;

    // Read input data signals
    wire [31:0] DataIn;
    wire DataInValid;

    // Timers
    localparam integer TIMER_WIDTH = 13;
    reg tRCTimerZero;
    wire [TIMER_WIDTH-1:0] ExpandtRC_CYCLES = expand(tRC_CYCLES);
    reg [TIMER_WIDTH-1:0] tRCTimer [0:3];
    reg [TIMER_WIDTH-1:0] tRASTimer [0:3];
    reg [TIMER_WIDTH-1:0] tRRDTimer;
    reg [TIMER_WIDTH-1:0] tWRTimer;
    reg [TIMER_WIDTH-1:0] tCASTimer;
    reg [TIMER_WIDTH-1:0] tWTRTimer;
    reg [TIMER_WIDTH-1:0] ActiveWaitTimer;
    reg [TIMER_WIDTH-1:0] RefreshWaitTimer;
    reg [TIMER_WIDTH-1:0] PrechargeWaitTimer;
    reg [10:0] RefreshCounter;
    reg RefreshNeeded;
    wire PreChargeOK;
    wire ActiveOK;

    // DDR alignment signals
    reg [12:0] DS_AVal;
    reg [1:0] DS_BAVal;
    reg DS_WE_NVal;
    reg DS_CAS_NVal;
    reg DS_RAS_NVal;
    reg [3:0] DS_DMVal;
    reg [3:0] DS_DMVal1;
    wire Tristate;
    reg Tristate1;
    wire ExtendedTristate;
    wire nTristate;
    wire nTristate1;
    wire nExtendedTristate;
    reg [31:0] DataOut1;

    // Owner FIFO
    reg [8:0] OwnerFIFO[0:15] /* synthesis syn_ramstyle="distributed" */;
    wire [8:0] OwnerFIFODataVal;
    reg [3:0] OwnerFIFOWriteCount;
    reg [3:0] OwnerFIFOReadCount;
    reg [3:0] OutstandingRead;
    
    // Decoder function
    function  [TIMER_WIDTH-1:0] expand;
        input integer Val;
        begin
            expand = 0;
            expand[Val] = 1'b1;
        end
    endfunction
		
    // Connect clocks and reset
    assign RST = User_RST;
    assign CLK = User_CLK;
    assign CLK90 = User_CLK90;
    
    //////////////////////////////////////////////////////////////////////////
    // If the user issues a command and we can't process it immediately,
    // register data for when the read/write can be issued
    always @ (posedge CLK) begin
        if (InputRegFull==0 || InputRegRead==1) begin
            InputRegAddr <= User_A;
            InputRegData <= User_DW;
            InputRegOwner <= User_Owner;
            InputRegBE <= User_BE;
            InputRegRE <= User_RE;
            InputRegWE <= User_WE;
        end
    end
    assign User_Busy = InputRegFull & ~InputRegRead;
    always @ (posedge RST or posedge CLK) begin
        if (RST==1) begin
            InputRegFull <= 0;
            InputRegPrechargeNeeded <= 0;
            InputRegReadReady <= 0;
            InputRegWriteReady <= 0;
        end else begin
            if (User_RE==1 || User_WE==1) begin
                InputRegFull <= 1;
            end else if (InputRegRead==1) begin
                InputRegFull <= 0;
            end
            if (User_RE==1 || User_WE==1) begin
                InputRegPrechargeNeeded <= NewOpenBank==1 && NewBankMatch==0 ? 1 : 0;
            end else if (NextCommand[PRECHARGE_BIT]==1) begin
                InputRegPrechargeNeeded <= 0;
            end else if (InputRegRead==1) begin
                InputRegPrechargeNeeded <= 0;
            end
            if (User_RE==1 || User_WE==1) begin
                InputRegReadReady <= NewOpenBank==1 && NewBankMatch==1 && User_RE==1 ? 1 : 0;
                InputRegWriteReady <= NewOpenBank==1 && NewBankMatch==1 && User_WE==1 ? 1 : 0;
            end else if (NextCommand[ACTIVE_BIT]==1) begin
                InputRegReadReady <= InputRegFull==1 && InputRegRE==1 ? 1 : 0;
                InputRegWriteReady <= InputRegFull==1 && InputRegWE==1 ? 1 : 0;
            end else if (InputRegRead==1) begin
                InputRegReadReady <= 0;
                InputRegWriteReady <= 0;
            end
        end
    end
    assign NewBankMatch = BankAddrs[NewBankAddr]==NewRowAddr ? 1 : 0;
    assign NewOpenBank = OpenBanks[NewBankAddr];
    
    assign InputRegRead = NextCommand[READ_BIT] | NextCommand[WRITE_BIT];
            
    // Record the current row address of each bank
    // This is used to check if a PRECHARGE - ACTIVE pair is needed
    always @ (posedge CLK) begin
        if (User_RE==1 || User_WE==1) begin
            BankAddrs[NewBankAddr] <= NewRowAddr;
        end
    end
    
    // Organise bits as BA:Row:Column to try to make sure as many banks as
    // possible remain open
    assign InputRegBankAddr = InputRegAddr[23:22];
    assign NewBankAddr = User_A[23:22];
    assign InputRegRowAddr = { 0, InputRegAddr[21:COLUMN_BITS-1] };
    assign NewRowAddr = { 0, User_A[21:COLUMN_BITS-1] };

    // On smaller devices, User_Addr(9) may form part of the row address instead
    // but if so then they are 'don't cares' on the column address.
    // A10 is tied low to disable auto precharge.
    assign InputRegColAddr = { 2'b0, InputRegAddr[9], 1'b0, InputRegAddr[8:0], 1'b0 };
                    
    //////////////////////////////////////////////////////////////////////////
    // Initialisation counters
    // This ensures the startup procedure is followed
    always @ (posedge CLK) begin
        if (RST==1) begin
            InitCounter <= 0;
            InitCounterCKE <= 0;
            InitCounterPreCharge <= 0;
            InitCounterRefresh <= 0;
            InitCounterLoad1 <= 0;
            InitCounterLoad2 <= 0;
            InitCounterLoad3 <= 0;
            User_InitDone <= 0;
        end else begin
            if (InitCounter!=16'hFFFF) begin
                InitCounter <= InitCounter + 1;
            end
            
            // Count steps of initialisation
            if (InitCounter==INIT_CKE_TIME) begin
                InitCounterCKE <= 1;
            end else begin
                InitCounterCKE <= 0;
            end
            if (InitCounter==INIT_PRECHARGE1_TIME ||
                InitCounter==INIT_PRECHARGE2_TIME) begin
                InitCounterPreCharge <= 1;
            end else begin
                InitCounterPreCharge <= 0;
            end
            if (InitCounter==INIT_REFRESH1_TIME || 
                InitCounter==INIT_REFRESH2_TIME) begin
                InitCounterRefresh <= 1;
            end else begin
                InitCounterRefresh <= 0;
            end
            if (InitCounter==INIT_LOAD_MODE1_TIME) begin
                InitCounterLoad1 <= 1;
            end else begin
                InitCounterLoad1 <= 0;
            end  
            if (InitCounter==INIT_LOAD_MODE2_TIME) begin
                InitCounterLoad2 <= 1;
            end else begin
                InitCounterLoad2 <= 0;
            end  
            if (InitCounter==INIT_LOAD_EXTENDED_MODE_TIME) begin
                InitCounterLoad3 <= 1;
            end else begin
                InitCounterLoad3 <= 0;
            end   
            if (InitCounter==INIT_LENGTH) begin
                // Initialisation complete
                User_InitDone <= 1;
            end
        end
    end
    
    //////////////////////////////////////////////////////////////////////////
    // Determine the next command to issue
    always @ (RefreshNeeded or OpenBanks or InputRegPrechargeNeeded or InputRegBankAddr or
              PreChargeOK or InputRegFull or OpenBanksInputReg) begin
        
        // Check for precharge
        if ((RefreshNeeded==1 && OpenBanks!=0) ||
            (InputRegFull==1 && OpenBanksInputReg==1 && InputRegPrechargeNeeded==1)) begin
            NextCommand[PRECHARGE_BIT] = PreChargeOK;
        end else begin
            NextCommand[PRECHARGE_BIT] = 0;
        end

        if (RefreshNeeded==1 && OpenBanks!=0) begin
            // Issue all banks precharge
            PreChargeAddr = 13'b0010000000000;
        end else begin
            // Issue single bank precharge
            PreChargeAddr = 0;
        end
    end
    
    always @ (RefreshNeeded or OpenBanks or PrechargeWaitTimer) begin
        if (RefreshNeeded==1 && OpenBanks==0) begin
            // Auto refresh needed
            NextCommand[AUTO_REFRESH_BIT] = PrechargeWaitTimer[0];
        end else begin
            NextCommand[AUTO_REFRESH_BIT] = 0;
        end
    end
    
    always @ (OpenBanksInputReg or InputRegFull or ActiveOK or PrechargeWaitTimer or
              RefreshWaitTimer or RefreshNeeded) begin
        if (RefreshNeeded==0 && 
            InputRegFull==1 && OpenBanksInputReg==0 &&
            ActiveOK==1 && PrechargeWaitTimer[0]==1 && RefreshWaitTimer[0]==1) begin
            // Active needed to open bank
            NextCommand[ACTIVE_BIT] = 1;
        end else begin
            NextCommand[ACTIVE_BIT] = 0;
        end
    end
    
    always @ (RefreshNeeded or InputRegReadReady or OpenBanksInputReg or
              ActiveWaitTimer or tWTRTimer) begin
        if (RefreshNeeded==0 && InputRegReadReady==1 && OpenBanksInputReg==1 &&
            ActiveWaitTimer[0]==1 && tWTRTimer[0]==1) begin
            // Issue Read
            NextCommand[READ_BIT] = 1;
        end else begin
            NextCommand[READ_BIT] = 0;
        end
    end
    
    always @ (RefreshNeeded or InputRegWriteReady or OpenBanksInputReg or
              ActiveWaitTimer or tCASTimer) begin
        if (RefreshNeeded==0 && InputRegWriteReady==1 && OpenBanksInputReg==1 &&
            ActiveWaitTimer[0]==1 && tCASTimer[0]==1) begin
            // Issue Write
            NextCommand[WRITE_BIT] = 1;
        end else begin
            NextCommand[WRITE_BIT] = 0;
        end
    end
    
    // NextCommand[LOAD_MODE_BIT] = 0;
    
    // Keep a record of open rows
    // There are 4 banks so 4 rows can be open simultaneously
    // Note that the banks should not be kept open for more than 120us but 
    // this is not a problem because the refresh forces an all-banks precharge
    // much more frequently than this.
    always @ (posedge RST or posedge CLK) begin
        if (RST==1) begin
            OpenBanks <= 4'b0;
            OpenBanksInputReg <= 0;
        end else begin
            if (NextCommand[PRECHARGE_BIT]==1 && PreChargeAddr[10]==1) begin
                // All banks precharge
                OpenBanks <= 0;
                OpenBanksInputReg <= 0;
            end else if (NextCommand[ACTIVE_BIT]==1 || NextCommand[PRECHARGE_BIT]==1) begin
                // Bank status changed
                OpenBanks[InputRegBankAddr] <= NextCommand[ACTIVE_BIT];
                OpenBanksInputReg <= NextCommand[ACTIVE_BIT];
            end else if (User_RE==1 || User_WE==1) begin
                OpenBanksInputReg <= OpenBanks[NewBankAddr];
            end
        end
    end

    //////////////////////////////////////////////////////////////////////////
    // Manage control signals
    assign DS_CKE = CKEVal;
    always @ (posedge RST or posedge CLK) begin
        if (RST==1) begin
            Command <= 0;
            RegPreChargeAddr <= 0;
            RegRowAddr <= 0;
            RegColAddr <= 0;
            RegBankAddr <= 0;
            RegModeWord <= 0;
            RegData <= 0;
            RegOwner <= 0;
            RegBE <= 0;
            LastCommandWrite <= 0;
            CKEVal <= 0;
        end else begin
            // Override command with the initialisation commands
            // Note that the user cannot issue commands during initialisation anyway
            // as busy signal to user logic will be high
            if (InitCounterCKE==1) begin
                CKEVal <= 1;
            end else if (InitCounterPreCharge==1) begin
                Command <= PRECHARGE;
                RegPreChargeAddr <= 13'b0010000000000;
            end else if (InitCounterRefresh==1) begin
                Command <= AUTO_REFRESH;
                RegPreChargeAddr <= PreChargeAddr;
            end else if (InitCounterLoad1==1 || InitCounterLoad2==1 ||
                         InitCounterLoad3==1) begin
                Command <= LOAD_MODE;
                RegPreChargeAddr <= PreChargeAddr;
            end else begin
                Command <= NextCommand;
                RegPreChargeAddr <= PreChargeAddr;
            end
            if (InitCounterLoad1==1) begin
                RegModeWord <= MODE1_WORD;
            end else if (InitCounterLoad2==1) begin
                RegModeWord <= MODE2_WORD;
            end else begin
                RegModeWord <= EXTENDED_MODE_WORD;
            end
            if (NextCommand[ACTIVE_BIT]==1) begin
                RegColAddr <= InputRegRowAddr;
            end else begin
                RegColAddr <= InputRegColAddr;
            end
            //RegRowAddr <= InputRegRowAddr;
            //RegColAddr <= InputRegColAddr;
            RegBankAddr <= InputRegBankAddr;
            if (InputRegRead==1) begin
                RegData <= InputRegData;
                RegBE <= InputRegBE;
            end
            RegOwner <= InputRegOwner;
    
            // Generate delayed and extended versions of write command
            // for tristate and DQS enables
            LastCommandWrite <= Command[WRITE_BIT];
        end
    end
    always @ (posedge CLK) begin
        DataOut <= RegData;
    end
        
    always @ (Command, RegPreChargeAddr, RegModeWord, RegRowAddr, RegColAddr,
              RegBankAddr) begin
        // Encode commands to RAS, CAS and WE signals
        if (Command[PRECHARGE_BIT]==1 ||
            Command[AUTO_REFRESH_BIT]==1 ||
            Command[ACTIVE_BIT]==1 ||
            Command[LOAD_MODE_BIT]==1) begin
            DS_RAS_NVal <= 0;
        end else begin
            DS_RAS_NVal <= 1;
        end
        if (Command[AUTO_REFRESH_BIT]==1 ||
            Command[READ_BIT]==1 ||
            Command[WRITE_BIT]==1 ||
            Command[LOAD_MODE_BIT]==1) begin
            DS_CAS_NVal <= 0;
        end else begin
            DS_CAS_NVal <= 1;
        end
        if (Command[PRECHARGE_BIT]==1 ||
            Command[WRITE_BIT]==1 ||
            Command[LOAD_MODE_BIT]==1) begin
            DS_WE_NVal <= 0;
        end else begin
            DS_WE_NVal <= 1;
        end

        // Multiplex address
        if (Command[PRECHARGE_BIT]==1) begin
            DS_AVal <= RegPreChargeAddr;
        end else if (Command[LOAD_MODE_BIT]==1) begin
            DS_AVal <= {1'b0, RegModeWord[12:0]};
        //end else if (Command[ACTIVE_BIT]==1) begin
        //    DS_AVal <= RegRowAddr;
        end else begin
            DS_AVal <= RegColAddr;
        end
        if (Command[LOAD_MODE_BIT]==1) begin
            DS_BAVal <= RegModeWord[14:13];
        end else begin
            DS_BAVal <= RegBankAddr;
        end
    end
    always @ (posedge CLK) begin
        // Multiplex data and byte enables
        if (Command[WRITE_BIT]==1) begin
            DS_DMVal <= ~RegBE;
        end else begin
            DS_DMVal <= 4'b1111;
        end
    end
    
    // Delay data to meet RAM timing requirements
    assign Tristate = LastCommandWrite;
    
    //////////////////////////////////////////////////////////////////////////
    // Refresh counter
    // Issues a refresh when the counter reaches zero (when the current
    // command is complete)
    always @ (posedge RST or posedge CLK)
        if (RST==1) begin
            RefreshCounter <= 0;
            RefreshNeeded <= 0;
        end else begin
            if (Command[AUTO_REFRESH_BIT]==1) begin
                RefreshCounter <= REFRESH_CYCLES-1;
                RefreshNeeded <= 0;
            end else if (RefreshCounter!=0) begin
                RefreshCounter <= RefreshCounter - 1;
                if (RefreshCounter==1) begin
                    RefreshNeeded <= 1;
                end
            end
        end
    
    //////////////////////////////////////////////////////////////////////////
    // Timers to avoid conflicts of operations
    // Cannot issue successive ACTIVEs to the same bank faster than tRC
    // Cannot issue successive ACTIVEs to different banks faster than tRRD
    // Cannot issue precharge after write until tWR expires
    // Cannot issue precharge after active until tRAS expires
    // Cannot issue write until tCAS after a read
    // Cannot issue read until tWTR expires
    // Cannot issue commands after ACTIVE until ActiveWaitTimer expires
    // Cannot issue commands after AUTO_REFRESH until RefreshWaitTimer expires
    // Cannot issue commands after PRECHARGE until PrechargeWaitTimer expires
    always @ (posedge CLK)
        if (RST==1) begin
            tRCTimer[0] <= 1;
            tRASTimer[0] <= 1;
            tRCTimer[1] <= 1;
            tRASTimer[1] <= 1;
            tRCTimer[2] <= 1;
            tRASTimer[2] <= 1;
            tRCTimer[3] <= 1;
            tRASTimer[3] <= 1;
            tRRDTimer <= 1;
            tWRTimer <= 1;
            tWTRTimer <= 1;
            tCASTimer <= 1;
            ActiveWaitTimer <= 1;
            PrechargeWaitTimer <= 1;
            RefreshWaitTimer <= 1;
            LastActiveBank <= 0;
        end else begin
            if (NextCommand[ACTIVE_BIT]==1 && InputRegBankAddr==2'b00) begin
                tRCTimer[0] <= expand(tRC_CYCLES);
            end else if (tRCTimer[0][0]==0) begin
                tRCTimer[0] <= {1'b0, tRCTimer[0][TIMER_WIDTH-1:1]};
            end
            if (NextCommand[ACTIVE_BIT]==1 && InputRegBankAddr==2'b01) begin
                tRCTimer[1] <= expand(tRC_CYCLES);
            end else if (tRCTimer[1][0]==0) begin
                tRCTimer[1] <= {1'b0, tRCTimer[1][TIMER_WIDTH-1:1]};
            end
            if (NextCommand[ACTIVE_BIT]==1 && InputRegBankAddr==2'b10) begin
                tRCTimer[2] <= expand(tRC_CYCLES);
            end else if (tRCTimer[2][0]==0) begin
                tRCTimer[2] <= {1'b0, tRCTimer[2][TIMER_WIDTH-1:1]};
            end
            if (NextCommand[ACTIVE_BIT]==1 && InputRegBankAddr==2'b11) begin
                tRCTimer[3] <= expand(tRC_CYCLES);
            end else if (tRCTimer[3][0]==0) begin
                tRCTimer[3] <= {1'b0, tRCTimer[3][TIMER_WIDTH-1:1]};
            end
            
            if (NextCommand[ACTIVE_BIT]==1 && InputRegBankAddr==2'b00) begin
                tRASTimer[0] <= expand(tRAS_CYCLES);
            end else if (tRASTimer[0][0]==0) begin
                tRASTimer[0] <= {1'b0, tRASTimer[0][TIMER_WIDTH-1:1]};
            end
            if (NextCommand[ACTIVE_BIT]==1 && InputRegBankAddr==2'b01) begin
                tRASTimer[1] <= expand(tRAS_CYCLES);
            end else if (tRASTimer[1][0]==0) begin
                tRASTimer[1] <= {1'b0, tRASTimer[1][TIMER_WIDTH-1:1]};
            end
            if (NextCommand[ACTIVE_BIT]==1 && InputRegBankAddr==2'b10) begin
                tRASTimer[2] <= expand(tRAS_CYCLES);
            end else if (tRASTimer[2][0]==0) begin
                tRASTimer[2] <= {1'b0, tRASTimer[2][TIMER_WIDTH-1:1]};
            end
            if (NextCommand[ACTIVE_BIT]==1 && InputRegBankAddr==2'b11) begin
                tRASTimer[3] <= expand(tRAS_CYCLES);
            end else if (tRASTimer[3][0]==0) begin
                tRASTimer[3] <= {1'b0, tRASTimer[3][TIMER_WIDTH-1:1]};
            end

            if (NextCommand[ACTIVE_BIT]==1) begin
                LastActiveBank <= InputRegBankAddr;
                tRRDTimer <= expand(tRRD_CYCLES);
            end else if (tRRDTimer[0]==0) begin
                tRRDTimer <= {1'b0, tRRDTimer[TIMER_WIDTH-1:1]};
            end
            if (NextCommand[WRITE_BIT]==1) begin
                tWRTimer <= expand(tWR_CYCLES);
            end else if (tWRTimer[0]==0) begin
                tWRTimer <= {1'b0, tWRTimer[TIMER_WIDTH-1:1]};
            end

            if (NextCommand[READ_BIT]==1) begin
                tCASTimer <= expand(tCAS_CYCLES);
            end else if (tCASTimer[0]==0) begin
                tCASTimer <= {1'b0, tCASTimer[TIMER_WIDTH-1:1]};
            end
            if (NextCommand[WRITE_BIT]==1) begin
                tWTRTimer <= expand(tWTR_CYCLES);
            end else if (tWTRTimer[0]==0) begin
                tWTRTimer <= {1'b0, tWTRTimer[TIMER_WIDTH-1:1]};
            end
            if (NextCommand[ACTIVE_BIT]==1) begin
                ActiveWaitTimer <= expand(ACTIVE_WAIT_CYCLES);
            end else if (ActiveWaitTimer[0]==0) begin
                ActiveWaitTimer <= {1'b0, ActiveWaitTimer[TIMER_WIDTH-1:1]};
            end
            if (NextCommand[AUTO_REFRESH_BIT]==1) begin
                RefreshWaitTimer <= expand(REFRESH_WAIT_CYCLES);
            end else if (RefreshWaitTimer[0]==0) begin
                RefreshWaitTimer <= {1'b0, RefreshWaitTimer[TIMER_WIDTH-1:1]};
            end
            if (NextCommand[AUTO_REFRESH_BIT]==1 || NextCommand[PRECHARGE_BIT]==1) begin
                PrechargeWaitTimer <= expand(PRECHARGE_WAIT_CYCLES);
            end else if (PrechargeWaitTimer[0]==0) begin
                PrechargeWaitTimer <= {1'b0, PrechargeWaitTimer[TIMER_WIDTH-1:1]};
            end

            if (NextCommand[ACTIVE_BIT]==1) begin
                tRCTimerZero <= ExpandtRC_CYCLES[0];
            end else begin
                tRCTimerZero <= tRCTimer[LastActiveBank][0]|tRCTimer[LastActiveBank][1];
            end

        end
    
    assign PreChargeOK = (tRASTimer[0][0]==1 && tRASTimer[1][0]==1 &&
                          tRASTimer[2][0]==1 && tRASTimer[3][0]==1 &&
                          tWRTimer[0]==1 && tCASTimer[0]==1) ? 1 : 0;
                          
    assign ActiveOK = (LastActiveBank!=InputRegBankAddr) ? tRRDTimer[0] : tRCTimerZero;

    //////////////////////////////////////////////////////////////////////////
    // Instantiate physical layer
    always @ (posedge CLK) begin
        Tristate1 <= Tristate;
    end
    always @ (posedge CLK) begin
        DataOut1[15:0] <= DataOut[15:0];
        DS_DMVal1[1:0] <= DS_DMVal[1:0];
    end
    always @ (negedge CLK) begin
        DataOut1[31:16] <= DataOut[31:16];
        DS_DMVal1[3:2] <= DS_DMVal[3:2];
    end
    ZestET1_SDRAMPhy PhyInst (
        .CLK(CLK),
        .CLK90(CLK90),
        .RST(RST),
        .DS_DQ(DS_DQ),
        .DS_DQS(DS_DQS),
        .DS_CLK_P(DS_CLK_P),
        .DS_CLK_N(DS_CLK_N),
        .DS_DM(DS_DM),
        .DS_A(DS_A),
        .DS_BA(DS_BA),
        .DS_WE_N(DS_WE_N),
        .DS_CAS_N(DS_CAS_N),
        .DS_RAS_N(DS_RAS_N),
        
        .DataOut(DataOut1),
        .DataIn(DataIn),
        .DataTri_P(nTristate),
        .DataTri_N(nTristate),
        .DQSTri_P(nTristate1),
        .DQSTri_N(nExtendedTristate),
        .DM_P(DS_DMVal1[3:2]),
        .DM_N(DS_DMVal1[1:0]),
        .DataValid(DataInValid),
        .Addr(DS_AVal),
        .BankAddr(DS_BAVal),
        .WE(DS_WE_NVal),
        .RAS(DS_RAS_NVal),
        .CAS(DS_CAS_NVal)
    );
    assign nTristate = ~Tristate;
    assign nTristate1 = ~Tristate1;
    assign ExtendedTristate = Tristate | Tristate1;
    assign nExtendedTristate = ~ExtendedTristate;
    
    //////////////////////////////////////////////////////////////////////////
    // Mark valid read data in user domain
    always @ (posedge RST or posedge CLK) begin
        if (RST==1) begin
            OwnerFIFOReadCount <= 0;
            OwnerFIFOWriteCount <= 0;
        end else begin
            if (Command[READ_BIT]==1 || Command[WRITE_BIT]==1) begin
                OwnerFIFOWriteCount <= OwnerFIFOWriteCount + 1;
            end
            if (DataInValid==1) begin
                OwnerFIFOReadCount <= OwnerFIFOReadCount + 1;
            end
        end
    end
    always @ (posedge CLK) begin
        OwnerFIFO[OwnerFIFOWriteCount] <= {Command[READ_BIT], RegOwner};
    end
    assign OwnerFIFODataVal = OwnerFIFO[OwnerFIFOReadCount];
    assign User_DR = DataIn;
    assign User_DR_Valid = OwnerFIFODataVal[8] & DataInValid;
    assign User_ValidOwner = OwnerFIFODataVal[7:0];

endmodule


/* ZestET1 DDR SDRAM Physical layer
   File name: ZestET1_SDRAMPhy.v
   Version: 1.20
   Date: 19/5/2011

   DDR SDRAM controller for user FPGA on ZestET1 boards
   Physical layer.  Directly instantiates components on time critical sections
   of the controller logic.
    
   Copyright (C) 2011 Orange Tree Technologies Ltd. All rights reserved.
   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and
   modify this logic core in any form including but not limited to Verilog source code or
   EDIF netlist in FPGA designs that target the ZestET1.
   Orange Tree Technologies prohibits the use of this logic core or any modification of
   it in any form including but not limited to Verilog source code or EDIF netlist in
   FPGA or ASIC designs that target any other hardware unless the purchaser of the
   ZestET1 has purchased the appropriate licence from Orange Tree Technologies.
   Contact Orange Tree Technologies if you want to purchase such a licence.

  *****************************************************************************************
  **
  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
  **              provided to you "as is". Orange Tree Technologies and its licensors 
  **              make and you receive no warranties or conditions, express, implied, 
  **              statutory or otherwise, and Orange Tree Technologies specifically 
  **              disclaims any implied warranties of merchantability, non-infringement,
  **              or fitness for a particular purpose. Orange Tree Technologies does not
  **              warrant that the functions contained in these designs will meet your 
  **              requirements, or that the operation of these designs will be 
  **              uninterrupted or error free, or that defects in the Designs will be 
  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
  **              make any representations regarding use or the results of the use of the 
  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
  **
  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
  **              or its licensors be liable for any loss of data, lost profits, cost or 
  **              procurement of substitute goods or services, or for any special, 
  **              incidental, consequential, or indirect damages arising from the use or 
  **              operation of the designs or accompanying documentation, however caused 
  **              and on any theory of liability. This limitation will apply even if 
  **              Orange Tree Technologies has been advised of the possibility of such 
  **              damage. This limitation shall apply notwithstanding the failure of the 
  **              essential purpose of any limited remedies herein.
  **
  *****************************************************************************************
*/

`timescale 1 ns / 1 ps

module ZestET1_SDRAMPhy
(
    input CLK,
    input CLK90,
    input RST,
    
    // Data path
    input [31:0] DataOut,
    input DataTri_P,
    input DataTri_N,
    input DQSTri_P,
    input DQSTri_N,
    input [1:0] DM_P,
    input [1:0] DM_N,
    output DS_CLK_P,
    output DS_CLK_N,
    output [31:0] DataIn,
    output [1:0] DS_DM,
    output DataValid,
    inout [15:0] DS_DQ,
    (* BUFFER_TYPE="NONE" *) inout [1:0] DS_DQS,
    
    // Control path
    output [12:0] DS_A,
    output [1:0] DS_BA,
    output DS_CAS_N,
    output DS_RAS_N,
    output DS_WE_N,
    input [12:0] Addr,
    input [1:0] BankAddr,
    input CAS,
    input RAS,
    input WE
);

    //////////////////////////////////////////////////////////////////////////
    // Declare signals
    wire nCLK;
    wire nCLK90;
    (* BUFFER_TYPE="NONE" *) wire DQSBuf1 /* synthesis syn_keep=1 */;
    (* BUFFER_TYPE="NONE" *) wire DQSBuf0 /* synthesis syn_keep=1 */;
    
    wire DDRIn15 /* synthesis syn_keep=1 */;
    wire DDRIn14 /* synthesis syn_keep=1 */;
    wire DDRIn13 /* synthesis syn_keep=1 */;
    wire DDRIn12 /* synthesis syn_keep=1 */;
    wire DDRIn11 /* synthesis syn_keep=1 */;
    wire DDRIn10 /* synthesis syn_keep=1 */;
    wire DDRIn9 /* synthesis syn_keep=1 */;
    wire DDRIn8 /* synthesis syn_keep=1 */;
    wire DDRIn7 /* synthesis syn_keep=1 */;
    wire DDRIn6 /* synthesis syn_keep=1 */;
    wire DDRIn5 /* synthesis syn_keep=1 */;
    wire DDRIn4 /* synthesis syn_keep=1 */;
    wire DDRIn3 /* synthesis syn_keep=1 */;
    wire DDRIn2 /* synthesis syn_keep=1 */;
    wire DDRIn1 /* synthesis syn_keep=1 */;
    wire DDRIn0 /* synthesis syn_keep=1 */;
    
    wire [1:0] DQSOut;
    wire [1:0] DQSTri;
    wire [1:0] DDRMask;
    wire [15:0] DDROut;
    wire [15:0] DDRTri;
    wire [15:0] nTristate_p;
    wire [15:0] nTristate_n;

    wire DQSDelayed0 /* synthesis syn_keep=1 */;
    wire DQSDelayed1 /* synthesis syn_keep=1 */;

    reg [1:0] DataInWriteCount;
    reg [1:0] DataInWriteCountInReadDomain;
    reg DataInReadCount_0;
    reg DataInReadCount_1;
    wire [3:0] DataInRC;

    reg DataInBuf0[0:15];
    reg DataInBuf1[0:15];
    reg DataInBuf2[0:15];
    reg DataInBuf3[0:15];
    reg DataInBuf4[0:15];
    reg DataInBuf5[0:15];
    reg DataInBuf6[0:15];
    reg DataInBuf7[0:15];
    reg DataInBuf8[0:15];
    reg DataInBuf9[0:15];
    reg DataInBuf10[0:15];
    reg DataInBuf11[0:15];
    reg DataInBuf12[0:15];
    reg DataInBuf13[0:15];
    reg DataInBuf14[0:15];
    reg DataInBuf15[0:15];
    reg DataInBuf16[0:15];
    reg DataInBuf17[0:15];
    reg DataInBuf18[0:15];
    reg DataInBuf19[0:15];
    reg DataInBuf20[0:15];
    reg DataInBuf21[0:15];
    reg DataInBuf22[0:15];
    reg DataInBuf23[0:15];
    reg DataInBuf24[0:15];
    reg DataInBuf25[0:15];
    reg DataInBuf26[0:15];
    reg DataInBuf27[0:15];
    reg DataInBuf28[0:15];
    reg DataInBuf29[0:15];
    reg DataInBuf30[0:15];
    reg DataInBuf31[0:15];
    wire [31:0] DataInBuf /* synthesis syn_keep=1 */;

    reg DataInWriteCount0_0 /* synthesis syn_keep=1 */;
    reg DataInWriteCount0_1 /* synthesis syn_keep=1 */;
    wire [3:0] DataInWC0;
    reg DataInWriteCount1_0 /* synthesis syn_keep=1 */;
    reg DataInWriteCount1_1 /* synthesis syn_keep=1 */;
    wire [3:0] DataInWC1;
    reg DataInWriteCount2_0 /* synthesis syn_keep=1 */;
    reg DataInWriteCount2_1 /* synthesis syn_keep=1 */;
    wire [3:0] DataInWC2;
    reg DataInWriteCount3_0 /* synthesis syn_keep=1 */;
    reg DataInWriteCount3_1 /* synthesis syn_keep=1 */;
    wire [3:0] DataInWC3;
    reg DataInWriteCount4_0 /* synthesis syn_keep=1 */;
    reg DataInWriteCount4_1 /* synthesis syn_keep=1 */;
    wire [3:0] DataInWC4;
    reg DataInWriteCount5_0 /* synthesis syn_keep=1 */;
    reg DataInWriteCount5_1 /* synthesis syn_keep=1 */;
    wire [3:0] DataInWC5;
    reg DataInWriteCount6_0 /* synthesis syn_keep=1 */;
    reg DataInWriteCount6_1 /* synthesis syn_keep=1 */;
    wire [3:0] DataInWC6;
    reg DataInWriteCount7_0 /* synthesis syn_keep=1 */;
    reg DataInWriteCount7_1 /* synthesis syn_keep=1 */;
    wire [3:0] DataInWC7;
    reg DataInWriteCount8_0 /* synthesis syn_keep=1 */;
    reg DataInWriteCount8_1 /* synthesis syn_keep=1 */;
    wire [3:0] DataInWC8;
    reg DataInWriteCount9_0 /* synthesis syn_keep=1 */;
    reg DataInWriteCount9_1 /* synthesis syn_keep=1 */;
    wire [3:0] DataInWC9;
    reg DataInWriteCount10_0 /* synthesis syn_keep=1 */;
    reg DataInWriteCount10_1 /* synthesis syn_keep=1 */;
    wire [3:0] DataInWC10;
    reg DataInWriteCount11_0 /* synthesis syn_keep=1 */;
    reg DataInWriteCount11_1 /* synthesis syn_keep=1 */;
    wire [3:0] DataInWC11;
    reg DataInWriteCount12_0 /* synthesis syn_keep=1 */;
    reg DataInWriteCount12_1 /* synthesis syn_keep=1 */;
    wire [3:0] DataInWC12;
    reg DataInWriteCount13_0 /* synthesis syn_keep=1 */;
    reg DataInWriteCount13_1 /* synthesis syn_keep=1 */;
    wire [3:0] DataInWC13;
    reg DataInWriteCount14_0 /* synthesis syn_keep=1 */;
    reg DataInWriteCount14_1 /* synthesis syn_keep=1 */;
    wire [3:0] DataInWC14;
    reg DataInWriteCount15_0 /* synthesis syn_keep=1 */;
    reg DataInWriteCount15_1 /* synthesis syn_keep=1 */;
    wire [3:0] DataInWC15;

    reg [31:0] DataInVal;
    reg DataValidVal;

    // Instantiate DDR FFs for clock output to RAM
    assign nCLK = ~CLK;
    assign nCLK90 = ~CLK90;
    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        CLK_PInst (.Q(DS_CLK_P), .C0(nCLK), .C1(CLK), .CE(1),
                   .D0(1), .D1(0), .R(0), .S(0));

    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        CLK_NInst (.Q(DS_CLK_N), .C0(nCLK), .C1(CLK), .CE(1),
                   .D0(0), .D1(1), .R(0), .S(0));

    // Instantiate FFs for DQS signals
    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        DQSOut1Inst (.Q(DQSOut[1]), .C0(nCLK), .C1(CLK), .CE(1),
                     .D0(1), .D1(0), .R(0), .S(0));

    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        DQSOut0Inst (.Q(DQSOut[0]), .C0(nCLK), .C1(CLK), .CE(1),
                     .D0(1), .D1(0), .R(0), .S(0));

    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        DQSTri1Inst (.Q(DQSTri[1]), .C0(nCLK), .C1(CLK), .CE(1),
                     .D0(DQSTri_P), .D1(DQSTri_N), .R(0), .S(0));

    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        DQSTri0Inst (.Q(DQSTri[0]), .C0(nCLK), .C1(CLK), .CE(1),
                     .D0(DQSTri_P), .D1(DQSTri_N), .R(0), .S(0));

    // Instantiate FFs for DM signals
    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        DM1Inst (.Q(DDRMask[1]), .C0(CLK90), .C1(nCLK90), .CE(1),
                 .D0(DM_P[1]), .D1(DM_N[1]), .R(0), .S(0));
                 
    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        DM0Inst (.Q(DDRMask[0]), .C0(CLK90), .C1(nCLK90), .CE(1),
                 .D0(DM_P[0]), .D1(DM_N[0]), .R(0), .S(0));

    // Instantiate FFs for DQ signals
    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        DQ15Inst (.Q(DDROut[15]), .C0(CLK90), .C1(nCLK90), .CE(1),
                  .D0(DataOut[31]), .D1(DataOut[15]), .R(0), .S(0));
    
    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        DQ14Inst (.Q(DDROut[14]), .C0(CLK90), .C1(nCLK90), .CE(1),
                  .D0(DataOut[30]), .D1(DataOut[14]), .R(0), .S(0));
    
    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        DQ13Inst (.Q(DDROut[13]), .C0(CLK90), .C1(nCLK90), .CE(1),
                  .D0(DataOut[29]), .D1(DataOut[13]), .R(0), .S(0));
    
    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        DQ12Inst (.Q(DDROut[12]), .C0(CLK90), .C1(nCLK90), .CE(1),
                  .D0(DataOut[28]), .D1(DataOut[12]), .R(0), .S(0));
    
    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        DQ11Inst (.Q(DDROut[11]), .C0(CLK90), .C1(nCLK90), .CE(1),
                  .D0(DataOut[27]), .D1(DataOut[11]), .R(0), .S(0));
    
    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        DQ10Inst (.Q(DDROut[10]), .C0(CLK90), .C1(nCLK90), .CE(1),
                  .D0(DataOut[26]), .D1(DataOut[10]), .R(0), .S(0));
    
    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        DQ9Inst (.Q(DDROut[9]), .C0(CLK90), .C1(nCLK90), .CE(1),
                  .D0(DataOut[25]), .D1(DataOut[9]), .R(0), .S(0));
    
    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        DQ8Inst (.Q(DDROut[8]), .C0(CLK90), .C1(nCLK90), .CE(1),
                  .D0(DataOut[24]), .D1(DataOut[8]), .R(0), .S(0));
    
    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        DQ7Inst (.Q(DDROut[7]), .C0(CLK90), .C1(nCLK90), .CE(1),
                  .D0(DataOut[23]), .D1(DataOut[7]), .R(0), .S(0));
    
    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        DQ6Inst (.Q(DDROut[6]), .C0(CLK90), .C1(nCLK90), .CE(1),
                  .D0(DataOut[22]), .D1(DataOut[6]), .R(0), .S(0));
    
    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        DQ5Inst (.Q(DDROut[5]), .C0(CLK90), .C1(nCLK90), .CE(1),
                  .D0(DataOut[21]), .D1(DataOut[5]), .R(0), .S(0));
    
    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        DQ4Inst (.Q(DDROut[4]), .C0(CLK90), .C1(nCLK90), .CE(1),
                  .D0(DataOut[20]), .D1(DataOut[4]), .R(0), .S(0));
    
    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        DQ3Inst (.Q(DDROut[3]), .C0(CLK90), .C1(nCLK90), .CE(1),
                  .D0(DataOut[19]), .D1(DataOut[3]), .R(0), .S(0));
    
    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        DQ2Inst (.Q(DDROut[2]), .C0(CLK90), .C1(nCLK90), .CE(1),
                  .D0(DataOut[18]), .D1(DataOut[2]), .R(0), .S(0));
    
    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        DQ1Inst (.Q(DDROut[1]), .C0(CLK90), .C1(nCLK90), .CE(1),
                  .D0(DataOut[17]), .D1(DataOut[1]), .R(0), .S(0));
    
    ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
        DQ0Inst (.Q(DDROut[0]), .C0(CLK90), .C1(nCLK90), .CE(1),
                  .D0(DataOut[16]), .D1(DataOut[0]), .R(0), .S(0));
    
    // Copy tristate signal and make sure its preserved
    genvar i;
    generate
        for (i=0; i<16; i=i+1) begin: TristateCopyp
            FDCE #(
                .INIT(1'b0)
            ) FDCE_inst (
                .Q(nTristate_p[i]),
                .C(nCLK),
                .CE(1'b1),
                .CLR(1'b0),
                .D(DataTri_P)
            );            
        end
    endgenerate
    generate
        for (i=0; i<16; i=i+1) begin: TristateCopyn
            FDCE #(
                .INIT(1'b0)
            ) FDCE_inst (
                .Q(nTristate_n[i]),
                .C(CLK),
                .CE(1'b1),
                .CLR(1'b0),
                .D(DataTri_N)
            );            
        end
    endgenerate

    // Instantiate FFs for tristate signals
    generate
        for (i=0; i<16; i=i+1) begin: TristateDDR
            ODDR2 #(.DDR_ALIGNMENT("NONE"), .INIT(1'b0), .SRTYPE("SYNC"))
                TristateDDRInst (.Q(DDRTri[i]), .C0(CLK90), .C1(nCLK90), .CE(1),
                                 .D0(nTristate_p[i]), .D1(nTristate_n[i]), .R(0), .S(0));
        end
    endgenerate
    
    // Clock incoming data using DQS strobe
    assign DataInWC0 = {2'b00, DataInWriteCount0_1, DataInWriteCount0_0};
    assign DataInWC1 = {2'b00, DataInWriteCount1_1, DataInWriteCount1_0};
    assign DataInWC2 = {2'b00, DataInWriteCount2_1, DataInWriteCount2_0};
    assign DataInWC3 = {2'b00, DataInWriteCount3_1, DataInWriteCount3_0};
    assign DataInWC4 = {2'b00, DataInWriteCount4_1, DataInWriteCount4_0};
    assign DataInWC5 = {2'b00, DataInWriteCount5_1, DataInWriteCount5_0};
    assign DataInWC6 = {2'b00, DataInWriteCount6_1, DataInWriteCount6_0};
    assign DataInWC7 = {2'b00, DataInWriteCount7_1, DataInWriteCount7_0};
    assign DataInWC8 = {2'b00, DataInWriteCount8_1, DataInWriteCount8_0};
    assign DataInWC9 = {2'b00, DataInWriteCount9_1, DataInWriteCount9_0};
    assign DataInWC10 = {2'b00, DataInWriteCount10_1, DataInWriteCount10_0};
    assign DataInWC11 = {2'b00, DataInWriteCount11_1, DataInWriteCount11_0};
    assign DataInWC12 = {2'b00, DataInWriteCount12_1, DataInWriteCount12_0};
    assign DataInWC13 = {2'b00, DataInWriteCount13_1, DataInWriteCount13_0};
    assign DataInWC14 = {2'b00, DataInWriteCount14_1, DataInWriteCount14_0};
    assign DataInWC15 = {2'b00, DataInWriteCount15_1, DataInWriteCount15_0};
    always @ (negedge DQSBuf0) begin
        DataInBuf0[DataInWC0] <= DDRIn0;
        DataInBuf1[DataInWC1] <= DDRIn1;
        DataInBuf2[DataInWC2] <= DDRIn2;
        DataInBuf3[DataInWC3] <= DDRIn3;
        DataInBuf4[DataInWC4] <= DDRIn4;
        DataInBuf5[DataInWC5] <= DDRIn5;
        DataInBuf6[DataInWC6] <= DDRIn6;
        DataInBuf7[DataInWC7] <= DDRIn7;
    end
    always @ (posedge DQSBuf0) begin
        DataInBuf16[DataInWC0] <= DDRIn0;
        DataInBuf17[DataInWC1] <= DDRIn1;
        DataInBuf18[DataInWC2] <= DDRIn2;
        DataInBuf19[DataInWC3] <= DDRIn3;
        DataInBuf20[DataInWC4] <= DDRIn4;
        DataInBuf21[DataInWC5] <= DDRIn5;
        DataInBuf22[DataInWC6] <= DDRIn6;
        DataInBuf23[DataInWC7] <= DDRIn7;
    end
    always @ (negedge DQSBuf1) begin
        DataInBuf8[DataInWC8] <= DDRIn8;
        DataInBuf9[DataInWC9] <= DDRIn9;
        DataInBuf10[DataInWC10] <= DDRIn10;
        DataInBuf11[DataInWC11] <= DDRIn11;
        DataInBuf12[DataInWC12] <= DDRIn12;
        DataInBuf13[DataInWC13] <= DDRIn13;
        DataInBuf14[DataInWC14] <= DDRIn14;
        DataInBuf15[DataInWC15] <= DDRIn15;
    end
    always @ (posedge DQSBuf1) begin
        DataInBuf24[DataInWC8] <= DDRIn8;
        DataInBuf25[DataInWC9] <= DDRIn9;
        DataInBuf26[DataInWC10] <= DDRIn10;
        DataInBuf27[DataInWC11] <= DDRIn11;
        DataInBuf28[DataInWC12] <= DDRIn12;
        DataInBuf29[DataInWC13] <= DDRIn13;
        DataInBuf30[DataInWC14] <= DDRIn14;
        DataInBuf31[DataInWC15] <= DDRIn15;
    end

    // Manage write counter
    always @ (posedge RST or negedge DQSBuf0) begin
        if (RST==1) begin
            DataInWriteCount0_0 <= 0;
            DataInWriteCount0_1 <= 0;
            DataInWriteCount1_0 <= 0;
            DataInWriteCount1_1 <= 0;
            DataInWriteCount2_0 <= 0;
            DataInWriteCount2_1 <= 0;
            DataInWriteCount3_0 <= 0;
            DataInWriteCount3_1 <= 0;
            DataInWriteCount4_0 <= 0;
            DataInWriteCount4_1 <= 0;
            DataInWriteCount5_0 <= 0;
            DataInWriteCount5_1 <= 0;
            DataInWriteCount6_0 <= 0;
            DataInWriteCount6_1 <= 0;
            DataInWriteCount7_0 <= 0;
            DataInWriteCount7_1 <= 0;
            DataInWriteCount <= "00";
        end else begin
            case (DataInWC0[1:0])
                2'b00:   begin DataInWriteCount0_1 <= 0; DataInWriteCount0_0 <= 1; end
                2'b01:   begin DataInWriteCount0_1 <= 1; DataInWriteCount0_0 <= 1; end
                2'b11:   begin DataInWriteCount0_1 <= 1; DataInWriteCount0_0 <= 0; end
                default: begin DataInWriteCount0_1 <= 0; DataInWriteCount0_0 <= 0; end
            endcase
            case (DataInWC1[1:0])
                2'b00:   begin DataInWriteCount1_1 <= 0; DataInWriteCount1_0 <= 1; end
                2'b01:   begin DataInWriteCount1_1 <= 1; DataInWriteCount1_0 <= 1; end
                2'b11:   begin DataInWriteCount1_1 <= 1; DataInWriteCount1_0 <= 0; end
                default: begin DataInWriteCount1_1 <= 0; DataInWriteCount1_0 <= 0; end
            endcase
            case (DataInWC2[1:0])
                2'b00:   begin DataInWriteCount2_1 <= 0; DataInWriteCount2_0 <= 1; end
                2'b01:   begin DataInWriteCount2_1 <= 1; DataInWriteCount2_0 <= 1; end
                2'b11:   begin DataInWriteCount2_1 <= 1; DataInWriteCount2_0 <= 0; end
                default: begin DataInWriteCount2_1 <= 0; DataInWriteCount2_0 <= 0; end
            endcase
            case (DataInWC3[1:0])
                2'b00:   begin DataInWriteCount3_1 <= 0; DataInWriteCount3_0 <= 1; end
                2'b01:   begin DataInWriteCount3_1 <= 1; DataInWriteCount3_0 <= 1; end
                2'b11:   begin DataInWriteCount3_1 <= 1; DataInWriteCount3_0 <= 0; end
                default: begin DataInWriteCount3_1 <= 0; DataInWriteCount3_0 <= 0; end
            endcase
            case (DataInWC4[1:0])
                2'b00:   begin DataInWriteCount4_1 <= 0; DataInWriteCount4_0 <= 1; end
                2'b01:   begin DataInWriteCount4_1 <= 1; DataInWriteCount4_0 <= 1; end
                2'b11:   begin DataInWriteCount4_1 <= 1; DataInWriteCount4_0 <= 0; end
                default: begin DataInWriteCount4_1 <= 0; DataInWriteCount4_0 <= 0; end
            endcase
            case (DataInWC5[1:0])
                2'b00:   begin DataInWriteCount5_1 <= 0; DataInWriteCount5_0 <= 1; end
                2'b01:   begin DataInWriteCount5_1 <= 1; DataInWriteCount5_0 <= 1; end
                2'b11:   begin DataInWriteCount5_1 <= 1; DataInWriteCount5_0 <= 0; end
                default: begin DataInWriteCount5_1 <= 0; DataInWriteCount5_0 <= 0; end
            endcase
            case (DataInWC6[1:0])
                2'b00:   begin DataInWriteCount6_1 <= 0; DataInWriteCount6_0 <= 1; end
                2'b01:   begin DataInWriteCount6_1 <= 1; DataInWriteCount6_0 <= 1; end
                2'b11:   begin DataInWriteCount6_1 <= 1; DataInWriteCount6_0 <= 0; end
                default: begin DataInWriteCount6_1 <= 0; DataInWriteCount6_0 <= 0; end
            endcase
            case (DataInWC7[1:0])
                2'b00:   begin DataInWriteCount7_1 <= 0; DataInWriteCount7_0 <= 1; end
                2'b01:   begin DataInWriteCount7_1 <= 1; DataInWriteCount7_0 <= 1; end
                2'b11:   begin DataInWriteCount7_1 <= 1; DataInWriteCount7_0 <= 0; end
                default: begin DataInWriteCount7_1 <= 0; DataInWriteCount7_0 <= 0; end
            endcase
            case (DataInWriteCount)
                2'b00:   begin DataInWriteCount <= 2'b01; end
                2'b01:   begin DataInWriteCount <= 2'b11; end
                2'b11:   begin DataInWriteCount <= 2'b10; end
                default: begin DataInWriteCount <= 2'b00; end
            endcase
        end
    end
    always @ (posedge RST or negedge DQSBuf1) begin
        if (RST==1) begin
            DataInWriteCount8_0 <= 0;
            DataInWriteCount8_1 <= 0;
            DataInWriteCount9_0 <= 0;
            DataInWriteCount9_1 <= 0;
            DataInWriteCount10_0 <= 0;
            DataInWriteCount10_1 <= 0;
            DataInWriteCount11_0 <= 0;
            DataInWriteCount11_1 <= 0;
            DataInWriteCount12_0 <= 0;
            DataInWriteCount12_1 <= 0;
            DataInWriteCount13_0 <= 0;
            DataInWriteCount13_1 <= 0;
            DataInWriteCount14_0 <= 0;
            DataInWriteCount14_1 <= 0;
            DataInWriteCount15_0 <= 0;
            DataInWriteCount15_1 <= 0;
        end else begin
            case (DataInWC8[1:0])
                2'b00:   begin DataInWriteCount8_1 <= 0; DataInWriteCount8_0 <= 1; end
                2'b01:   begin DataInWriteCount8_1 <= 1; DataInWriteCount8_0 <= 1; end
                2'b11:   begin DataInWriteCount8_1 <= 1; DataInWriteCount8_0 <= 0; end
                default: begin DataInWriteCount8_1 <= 0; DataInWriteCount8_0 <= 0; end
            endcase
            case (DataInWC9[1:0])
                2'b00:   begin DataInWriteCount9_1 <= 0; DataInWriteCount9_0 <= 1; end
                2'b01:   begin DataInWriteCount9_1 <= 1; DataInWriteCount9_0 <= 1; end
                2'b11:   begin DataInWriteCount9_1 <= 1; DataInWriteCount9_0 <= 0; end
                default: begin DataInWriteCount9_1 <= 0; DataInWriteCount9_0 <= 0; end
            endcase
            case (DataInWC10[1:0])
                2'b00:   begin DataInWriteCount10_1 <= 0; DataInWriteCount10_0 <= 1; end
                2'b01:   begin DataInWriteCount10_1 <= 1; DataInWriteCount10_0 <= 1; end
                2'b11:   begin DataInWriteCount10_1 <= 1; DataInWriteCount10_0 <= 0; end
                default: begin DataInWriteCount10_1 <= 0; DataInWriteCount10_0 <= 0; end
            endcase
            case (DataInWC11[1:0])
                2'b00:   begin DataInWriteCount11_1 <= 0; DataInWriteCount11_0 <= 1; end
                2'b01:   begin DataInWriteCount11_1 <= 1; DataInWriteCount11_0 <= 1; end
                2'b11:   begin DataInWriteCount11_1 <= 1; DataInWriteCount11_0 <= 0; end
                default: begin DataInWriteCount11_1 <= 0; DataInWriteCount11_0 <= 0; end
            endcase
            case (DataInWC12[1:0])
                2'b00:   begin DataInWriteCount12_1 <= 0; DataInWriteCount12_0 <= 1; end
                2'b01:   begin DataInWriteCount12_1 <= 1; DataInWriteCount12_0 <= 1; end
                2'b11:   begin DataInWriteCount12_1 <= 1; DataInWriteCount12_0 <= 0; end
                default: begin DataInWriteCount12_1 <= 0; DataInWriteCount12_0 <= 0; end
            endcase
            case (DataInWC13[1:0])
                2'b00:   begin DataInWriteCount13_1 <= 0; DataInWriteCount13_0 <= 1; end
                2'b01:   begin DataInWriteCount13_1 <= 1; DataInWriteCount13_0 <= 1; end
                2'b11:   begin DataInWriteCount13_1 <= 1; DataInWriteCount13_0 <= 0; end
                default: begin DataInWriteCount13_1 <= 0; DataInWriteCount13_0 <= 0; end
            endcase
            case (DataInWC14[1:0])
                2'b00:   begin DataInWriteCount14_1 <= 0; DataInWriteCount14_0 <= 1; end
                2'b01:   begin DataInWriteCount14_1 <= 1; DataInWriteCount14_0 <= 1; end
                2'b11:   begin DataInWriteCount14_1 <= 1; DataInWriteCount14_0 <= 0; end
                default: begin DataInWriteCount14_1 <= 0; DataInWriteCount14_0 <= 0; end
            endcase
            case (DataInWC15[1:0])
                2'b00:   begin DataInWriteCount15_1 <= 0; DataInWriteCount15_0 <= 1; end
                2'b01:   begin DataInWriteCount15_1 <= 1; DataInWriteCount15_0 <= 1; end
                2'b11:   begin DataInWriteCount15_1 <= 1; DataInWriteCount15_0 <= 0; end
                default: begin DataInWriteCount15_1 <= 0; DataInWriteCount15_0 <= 0; end
            endcase
        end
    end
    
    assign DataInRC = {2'b00, DataInReadCount_1, DataInReadCount_0};
    assign DataInBuf = {DataInBuf31[DataInRC], DataInBuf30[DataInRC],
                        DataInBuf29[DataInRC], DataInBuf28[DataInRC],
                        DataInBuf27[DataInRC], DataInBuf26[DataInRC],
                        DataInBuf25[DataInRC], DataInBuf24[DataInRC],
                        DataInBuf23[DataInRC], DataInBuf22[DataInRC],
                        DataInBuf21[DataInRC], DataInBuf20[DataInRC],
                        DataInBuf19[DataInRC], DataInBuf18[DataInRC],
                        DataInBuf17[DataInRC], DataInBuf16[DataInRC],
                        DataInBuf15[DataInRC], DataInBuf14[DataInRC],
                        DataInBuf13[DataInRC], DataInBuf12[DataInRC],
                        DataInBuf11[DataInRC], DataInBuf10[DataInRC],
                        DataInBuf9[DataInRC], DataInBuf8[DataInRC],
                        DataInBuf7[DataInRC], DataInBuf6[DataInRC],
                        DataInBuf5[DataInRC], DataInBuf4[DataInRC],
                        DataInBuf3[DataInRC], DataInBuf2[DataInRC],
                        DataInBuf1[DataInRC], DataInBuf0[DataInRC]};
    always @ (posedge RST or posedge CLK) begin
        if (RST==1) begin
            DataInWriteCountInReadDomain <= 0;
            DataInReadCount_0 <= 0;
            DataInReadCount_1 <= 0;
            DataValidVal <= 0;
            DataInVal <= 0;
        end else begin
            DataInWriteCountInReadDomain <= DataInWC0[1:0];
            
            if (DataInRC[1:0]!=DataInWriteCountInReadDomain) begin
                DataValidVal <= 1;
                DataInVal <= DataInBuf;
                case (DataInRC[1:0])
                    2'b00:   begin DataInReadCount_1 <= 0; DataInReadCount_0 <= 1; end
                    2'b01:   begin DataInReadCount_1 <= 1; DataInReadCount_0 <= 1; end
                    2'b11:   begin DataInReadCount_1 <= 1; DataInReadCount_0 <= 0; end
                    default: begin DataInReadCount_1 <= 0; DataInReadCount_0 <= 0; end
                endcase
            end else begin
                DataValidVal <= 0;
            end
        end
    end
    assign DataValid = DataValidVal;
    assign DataIn = DataInVal;

    // Introduce phase shift on DQS here
    IBUF #(
      .IOSTANDARD("DEFAULT")
    ) IBUF_DLY_ADJ_inst0 (
      .O(DQSDelayed0),
      .I(DS_DQS[0])
    );
    LUT1 #(
      .INIT(2'b10)
    ) DQS0Buffer0 (
      .I0(DQSDelayed0),
      .O(DQSBuf0)
    );
    OBUFT Inst2_BB0 (.I(DQSOut[0]), .T(DQSTri[0]), .O(DS_DQS[0]));
    
    IBUF #(
      .IOSTANDARD("DEFAULT")
    ) IBUF_DLY_ADJ_inst1 (
      .I(DS_DQS[1]),
      .O(DQSDelayed1)
    );
    LUT1 #(
      .INIT(2'b10)
    ) DQS1Buffer0 (
      .I0(DQSDelayed1),
      .O(DQSBuf1)
    );
    OBUFT DQSOBuf1 (.I(DQSOut[1]), .T(DQSTri[1]), .O(DS_DQS[1]));

    // Data bus buffers
    OBUF DMBuf1 (.I(DDRMask[1]), .O(DS_DM[1]));
    OBUF DMBuf0 (.I(DDRMask[0]), .O(DS_DM[0]));
    IBUF DQIBuf15 (.O(DDRIn15), .I(DS_DQ[15]));
    IBUF DQIBuf14 (.O(DDRIn14), .I(DS_DQ[14]));
    IBUF DQIBuf13 (.O(DDRIn13), .I(DS_DQ[13]));
    IBUF DQIBuf12 (.O(DDRIn12), .I(DS_DQ[12]));
    IBUF DQIBuf11 (.O(DDRIn11), .I(DS_DQ[11]));
    IBUF DQIBuf10 (.O(DDRIn10), .I(DS_DQ[10]));
    IBUF DQIBuf9 (.O(DDRIn9), .I(DS_DQ[9]));
    IBUF DQIBuf8 (.O(DDRIn8), .I(DS_DQ[8]));
    IBUF DQIBuf7 (.O(DDRIn7), .I(DS_DQ[7]));
    IBUF DQIBuf6 (.O(DDRIn6), .I(DS_DQ[6]));
    IBUF DQIBuf5 (.O(DDRIn5), .I(DS_DQ[5]));
    IBUF DQIBuf4 (.O(DDRIn4), .I(DS_DQ[4]));
    IBUF DQIBuf3 (.O(DDRIn3), .I(DS_DQ[3]));
    IBUF DQIBuf2 (.O(DDRIn2), .I(DS_DQ[2]));
    IBUF DQIBuf1 (.O(DDRIn1), .I(DS_DQ[1]));
    IBUF DQIBuf0 (.O(DDRIn0), .I(DS_DQ[0]));
    
    generate
        for (i=0; i<16; i=i+1) begin: DOutBuf
            OBUFT DQOBuf (.I(DDROut[i]), .T(DDRTri[i]), .O(DS_DQ[i]));
        end
    endgenerate

    // Instantiate FFs for address and control strobes
    (* IOB="TRUE" *) FDCE #(.INIT(1)) AddrInst0 (.Q(DS_A[0]), .C(CLK), .CE(1), .CLR(0), .D(Addr[0]));
    (* IOB="TRUE" *) FDCE #(.INIT(1)) AddrInst1 (.Q(DS_A[1]), .C(CLK), .CE(1), .CLR(0), .D(Addr[1]));
    (* IOB="TRUE" *) FDCE #(.INIT(1)) AddrInst2 (.Q(DS_A[2]), .C(CLK), .CE(1), .CLR(0), .D(Addr[2]));
    (* IOB="TRUE" *) FDCE #(.INIT(1)) AddrInst3 (.Q(DS_A[3]), .C(CLK), .CE(1), .CLR(0), .D(Addr[3]));
    (* IOB="TRUE" *) FDCE #(.INIT(1)) AddrInst4 (.Q(DS_A[4]), .C(CLK), .CE(1), .CLR(0), .D(Addr[4]));
    (* IOB="TRUE" *) FDCE #(.INIT(1)) AddrInst5 (.Q(DS_A[5]), .C(CLK), .CE(1), .CLR(0), .D(Addr[5]));
    (* IOB="TRUE" *) FDCE #(.INIT(1)) AddrInst6 (.Q(DS_A[6]), .C(CLK), .CE(1), .CLR(0), .D(Addr[6]));
    (* IOB="TRUE" *) FDCE #(.INIT(1)) AddrInst7 (.Q(DS_A[7]), .C(CLK), .CE(1), .CLR(0), .D(Addr[7]));
    (* IOB="TRUE" *) FDCE #(.INIT(1)) AddrInst8 (.Q(DS_A[8]), .C(CLK), .CE(1), .CLR(0), .D(Addr[8]));
    (* IOB="TRUE" *) FDCE #(.INIT(1)) AddrInst9 (.Q(DS_A[9]), .C(CLK), .CE(1), .CLR(0), .D(Addr[9]));
    (* IOB="TRUE" *) FDCE #(.INIT(1)) AddrInst10 (.Q(DS_A[10]), .C(CLK), .CE(1), .CLR(0), .D(Addr[10]));
    (* IOB="TRUE" *) FDCE #(.INIT(1)) AddrInst11 (.Q(DS_A[11]), .C(CLK), .CE(1), .CLR(0), .D(Addr[11]));
    (* IOB="TRUE" *) FDCE #(.INIT(1)) AddrInst12 (.Q(DS_A[12]), .C(CLK), .CE(1), .CLR(0), .D(Addr[12]));
    (* IOB="TRUE" *) FDCE #(.INIT(1)) BankAddrInst0 (.Q(DS_BA[0]), .C(CLK), .CE(1), .CLR(0), .D(BankAddr[0]));
    (* IOB="TRUE" *) FDCE #(.INIT(1)) BankAddrInst1 (.Q(DS_BA[1]), .C(CLK), .CE(1), .CLR(0), .D(BankAddr[1]));
    (* IOB="TRUE" *) FDCE #(.INIT(1)) CASInst (.Q(DS_CAS_N), .C(CLK), .CE(1), .CLR(0), .D(CAS));
    (* IOB="TRUE" *) FDCE #(.INIT(1)) RASInst (.Q(DS_RAS_N), .C(CLK), .CE(1), .CLR(0), .D(RAS));
    (* IOB="TRUE" *) FDCE #(.INIT(1)) WEInst (.Q(DS_WE_N), .C(CLK), .CE(1), .CLR(0), .D(WE));
    
endmodule

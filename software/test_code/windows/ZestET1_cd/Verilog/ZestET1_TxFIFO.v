/* ZestET1 Ethernet Transmit FIFO
   File name: ZestET1_TxFIFO.v
   Version: 1.00
   Date: 10/3/2011

   FIFO to bridge from 8 bit user interface to 16 bit GigExpedite interface for data
   sent to the network.  This will write either one or two bytes of data to the 
   GigExpedite.
   
   Copyright (C) 2011 Orange Tree Technologies Ltd. All rights reserved.
   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and
   modify this logic core in any form including but not limited to Verilog source code or
   EDIF netlist in FPGA designs that target the ZestET1.
   Orange Tree Technologies prohibits the use of this logic core or any modification of
   it in any form including but not limited to Verilog source code or EDIF netlist in
   FPGA or ASIC designs that target any other hardware unless the purchaser of the
   ZestET1 has purchased the appropriate licence from Orange Tree Technologies.
   Contact Orange Tree Technologies if you want to purchase such a licence.

  *****************************************************************************************
  **
  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
  **              provided to you "as is". Orange Tree Technologies and its licensors 
  **              make and you receive no warranties or conditions, express, implied, 
  **              statutory or otherwise, and Orange Tree Technologies specifically 
  **              disclaims any implied warranties of merchantability, non-infringement,
  **              or fitness for a particular purpose. Orange Tree Technologies does not
  **              warrant that the functions contained in these designs will meet your 
  **              requirements, or that the operation of these designs will be 
  **              uninterrupted or error free, or that defects in the Designs will be 
  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
  **              make any representations regarding use or the results of the use of the 
  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
  **
  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
  **              or its licensors be liable for any loss of data, lost profits, cost or 
  **              procurement of substitute goods or services, or for any special, 
  **              incidental, consequential, or indirect damages arising from the use or 
  **              operation of the designs or accompanying documentation, however caused 
  **              and on any theory of liability. This limitation will apply even if 
  **              Orange Tree Technologies has been advised of the possibility of such 
  **              damage. This limitation shall apply notwithstanding the failure of the 
  **              essential purpose of any limited remedies herein.
  **
  *****************************************************************************************
*/

`timescale 1ns / 1ps

module ZestET1_TxFIFO (
        input RST,
        input CLK,
        input WE,
        input [7:0] DataIn,
        input RE,
        output reg [17:0] DataOut,
        output Empty,
        output NextEmpty,
        output reg Full
    );

    // Declare constants
    localparam integer LOG2_FIFO_DEPTH = 4;
    localparam integer FIFO_DEPTH = 1<<LOG2_FIFO_DEPTH;
    localparam integer ALMOST_FULL = FIFO_DEPTH-4;

    // Declare signals
    reg [17:0] StorageArray[0:(FIFO_DEPTH-1)] /* synthesis syn_ramstyle="distributed" */ ;
    wire [17:0] StorageArrayVal;
    
    reg EmptyVal;
    wire NextEmptyVal;
    wire DataInWE;
    wire [17:0] DataInVal;
    reg [7:0] RegDataIn;
    reg RegDataInValid;
    reg [(LOG2_FIFO_DEPTH-1):0] WriteCount;
    reg [(LOG2_FIFO_DEPTH-1):0] RegWriteCount;
    wire [(LOG2_FIFO_DEPTH-1):0] NextWriteCount;
    reg [(LOG2_FIFO_DEPTH-1):0] ReadCount;
    reg [(LOG2_FIFO_DEPTH-1):0] ReadCountPlus1;
    wire [(LOG2_FIFO_DEPTH-1):0] NextReadCountPlus1;
    wire [(LOG2_FIFO_DEPTH-1):0] SelReadCount;
    wire [(LOG2_FIFO_DEPTH-1):0] Count;
    
    // Storage array - inferred as a distributed ram
    always @ (posedge CLK) begin
        if (DataInWE==1) begin
            StorageArray[WriteCount] <= DataInVal;
        end
        DataOut <= StorageArrayVal;
    end
    assign StorageArrayVal = StorageArray[SelReadCount];
    assign DataInWE = RegDataInValid;
    assign DataInVal[17] = RegDataInValid;
    assign DataInVal[16] = WE;
    assign DataInVal[15:8] = RegDataIn;
    assign DataInVal[7:0] = DataIn;
    assign SelReadCount = RE==0 ? ReadCount : ReadCountPlus1;
    
    // Manage read and write counters
    always @ (posedge CLK) begin
        if (RST==1) begin
            RegDataIn <= 0;
            RegDataInValid <= 0;
            WriteCount <= 0;
            RegWriteCount <= 0;
            ReadCount <= 0;
            ReadCountPlus1 <= 1;
            EmptyVal <= 1;
            Full <= 0;
        end else begin
            if (DataInWE==1) begin
                RegDataInValid <= 0;
            end else if (WE==1) begin
                RegDataIn <= DataIn;
                RegDataInValid <= 1;
            end
            if (DataInWE==1) begin
                WriteCount <= NextWriteCount;
            end
            RegWriteCount <= WriteCount;
            
            if (RE==1) begin
                ReadCount <= ReadCountPlus1;
                ReadCountPlus1 <= NextReadCountPlus1;
            end

            EmptyVal <= NextEmptyVal;
            if (Count>=ALMOST_FULL) begin
                Full <= 1;
            end else begin
                Full <= 0;
            end
        end
    end

    assign NextReadCountPlus1 = ReadCountPlus1 + 1;
    assign NextWriteCount = WriteCount + 1;

    assign Count = WriteCount-ReadCount;
    assign NextEmptyVal = RegWriteCount==SelReadCount ? 1 : 0;
    assign NextEmpty = NextEmptyVal;
    assign Empty = EmptyVal;
    
endmodule

/* ZestET1 DCM instantiation code
   File name: ZestET1_Clocks.v
   Version: 1.20
   Date: 11/5/2011

   By default, the input reference clock is assumed to be 125MHz
   The generated clocks are:
        EthClk      : 125MHz clock for GigExpedite interface
        RAMClk      : 166MHz clock for SDRAM
        RAMClk90    : 166MHz clock with 90 degree shift for SDRAM
    
   Copyright (C) 2011 Orange Tree Technologies Ltd. All rights reserved.
   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and
   modify this logic core in any form including but not limited to Verilog source code or
   EDIF netlist in FPGA designs that target the ZestET1.
   Orange Tree Technologies prohibits the use of this logic core or any modification of
   it in any form including but not limited to Verilog source code or EDIF netlist in
   FPGA or ASIC designs that target any other hardware unless the purchaser of the
   ZestET1 has purchased the appropriate licence from Orange Tree Technologies.
   Contact Orange Tree Technologies if you want to purchase such a licence.

  *****************************************************************************************
  **
  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
  **              provided to you "as is". Orange Tree Technologies and its licensors 
  **              make and you receive no warranties or conditions, express, implied, 
  **              statutory or otherwise, and Orange Tree Technologies specifically 
  **              disclaims any implied warranties of merchantability, non-infringement,
  **              or fitness for a particular purpose. Orange Tree Technologies does not
  **              warrant that the functions contained in these designs will meet your 
  **              requirements, or that the operation of these designs will be 
  **              uninterrupted or error free, or that defects in the Designs will be 
  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
  **              make any representations regarding use or the results of the use of the 
  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
  **
  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
  **              or its licensors be liable for any loss of data, lost profits, cost or 
  **              procurement of substitute goods or services, or for any special, 
  **              incidental, consequential, or indirect damages arising from the use or 
  **              operation of the designs or accompanying documentation, however caused 
  **              and on any theory of liability. This limitation will apply even if 
  **              Orange Tree Technologies has been advised of the possibility of such 
  **              damage. This limitation shall apply notwithstanding the failure of the 
  **              essential purpose of any limited remedies herein.
  **
  *****************************************************************************************
*/

module ZestET1_Clocks
(
    input RST,
    input RefClk,       // Reference input clock
    output EthClk,      // Ethernet interface clock
    output RAMRST,      // Reset for RAM controller
    output RAMClk,      // RAM clock
    output RAMClk90     // RAM clock with 90 degree phase shift
);

    reg RefRST;
    wire RefClkBuf;
    wire RefClkFBUnBuf;
    wire RefClkFx;
    wire RefClkLocked;
    wire RefClkFB;
    wire [7:0] RefStatus;
    reg [15:0] RefResetCount;
    
    wire RAMClkVal;
    wire RAMClkUnBuf;
    wire RAMClk90UnBuf;
    wire RAMClkLocked;
    wire [7:0] RAMStatus;
    reg [15:0] RAMResetCount;
    reg RAMDCMRST;
    (* TIG="TRUE" *) reg RAMResetTimer;

    reg RegRAMResetTimer;
    reg RAMRSTVal;
    
    // Generate Ethernet interface clocks
    DCM_SP # (
        .CLKDV_DIVIDE(2.0),
        .CLKFX_DIVIDE(3),     // For 166MHz RAM clock
        .CLKFX_MULTIPLY(4),
        .CLKIN_DIVIDE_BY_2("FALSE"),
        .CLKIN_PERIOD(8.0),
        .CLKOUT_PHASE_SHIFT("NONE"),
        .CLK_FEEDBACK("1X"),
        .DESKEW_ADJUST("SYSTEM_SYNCHRONOUS"),
        .DLL_FREQUENCY_MODE("LOW"),
        .DUTY_CYCLE_CORRECTION("TRUE"),
        .PHASE_SHIFT(0),
        .STARTUP_WAIT("FALSE")
    ) RefClkDCMInst (
        .CLK0(RefClkFBUnBuf),
        .CLKFX(RefClkFx),
        .LOCKED(RefClkLocked),
        .STATUS(RefStatus),
        .CLKFB(RefClkFB),
        .CLKIN(RefClkBuf),
        .PSCLK(1'b0),
        .PSEN(1'b0),
        .PSINCDEC(1'b0),
        .RST(RefRST)
    );
    BUFG RefClkBuffer (.I(RefClk), .O(RefClkBuf));
    BUFG RefClkFBInst (.I(RefClkFBUnBuf), .O(RefClkFB));
    assign EthClk = RefClkFB;

    // Generate 90 degree phase shifted RAM clock
    DCM_SP # (
        .CLKDV_DIVIDE(2.0),
        .CLKFX_DIVIDE(2),
        .CLKFX_MULTIPLY(2),
        .CLKIN_DIVIDE_BY_2("FALSE"),
        .CLKIN_PERIOD(6.0),
        .CLKOUT_PHASE_SHIFT("NONE"),
        .CLK_FEEDBACK("1X"),
        .DESKEW_ADJUST("SYSTEM_SYNCHRONOUS"),
        .DLL_FREQUENCY_MODE("LOW"),
        .DUTY_CYCLE_CORRECTION("TRUE"),
        .PHASE_SHIFT(0),
        .STARTUP_WAIT("FALSE")
    ) RAMClkDCMInst (
        .CLK0(RAMClkUnBuf),
        .CLK90(RAMClk90UnBuf),
        .LOCKED(RAMClkLocked),
        .STATUS(RAMStatus),
        .CLKFB(RAMClkVal),
        .CLKIN(RefClkFx),
        .PSCLK(0),
        .PSEN(0),
        .PSINCDEC(0),
        .RST(RAMDCMRST)
    );
    
    // Generate resets
    always @ (posedge RST or posedge RefClkBuf) begin
        if (RST==1) begin
            RefRST <= 1;
            RefResetCount <= 0;
        end else begin
            if (RefResetCount!=16'hFFFF) begin
                RefResetCount <= RefResetCount + 1;
                if (RefResetCount==16'h00FF) begin
                    RefRST <= 0;
                end
            end else if (RefClkLocked==0 || RefStatus[2:1]!=2'b00) begin
                RefResetCount <= 0;
                RefRST <= 1;
            end
        end
    end
    always @ (posedge RST or posedge RefClkBuf) begin
        if (RST==1) begin
            RAMDCMRST <= 1;
            RAMResetCount <= 0;
            RAMResetTimer <= 1;
        end else begin
            if (RefClkLocked==0) begin
                RAMResetCount <= 0;
                RAMDCMRST <= 1;
                RAMResetTimer <= 1;
            end else if (RAMResetCount!=16'hFFFF) begin
                RAMResetCount <= RAMResetCount + 1;
                if (RAMResetCount==16'h00FF) begin
                    RAMDCMRST <= 0;
                end
            end else if (RAMClkLocked==0 || RAMStatus[1]==1) begin
                RAMResetCount <= 0;
                RAMDCMRST <= 1;
                RAMResetTimer <= 1;
            end else begin
                RAMResetTimer <= 0;
            end
        end
    end
    
    always @ (posedge RST or posedge RAMClkVal) begin
        if (RST==1) begin
            RegRAMResetTimer <= 1;
            RAMRSTVal <= 1;
        end else begin
            RegRAMResetTimer <= RAMResetTimer;
            RAMRSTVal <= RegRAMResetTimer;
        end
    end
    assign RAMRST = RAMRSTVal;
    
    // Buffer RAM clocks
    BUFG RAMClkInst (.I(RAMClkUnBuf), .O(RAMClkVal));
    BUFG RAMClk90Inst (.I(RAMClk90UnBuf), .O(RAMClk90));
    assign RAMClk = RAMClkVal;

endmodule

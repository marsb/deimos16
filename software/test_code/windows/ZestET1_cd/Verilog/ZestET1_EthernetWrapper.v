/* ZestET1 GigExpedite Wrapper
   File name: ZestET1_EthernetWrapper.v
   Version: 1.50
   Date: 09/2/2012

   Wrapper around GigExpedite interface to simplfy user access.
   Contains the state machine needed to open/close connections and
   transmit/receive data over Ethernet.
   
   Copyright (C) 2012 Orange Tree Technologies Ltd. All rights reserved.
   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and
   modify this logic core in any form including but not limited to Verilog source code or
   EDIF netlist in FPGA designs that target the ZestET1.
   Orange Tree Technologies prohibits the use of this logic core or any modification of
   it in any form including but not limited to Verilog source code or EDIF netlist in
   FPGA or ASIC designs that target any other hardware unless the purchaser of the
   ZestET1 has purchased the appropriate licence from Orange Tree Technologies.
   Contact Orange Tree Technologies if you want to purchase such a licence.

  *****************************************************************************************
  **
  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
  **              provided to you "as is". Orange Tree Technologies and its licensors 
  **              make and you receive no warranties or conditions, express, implied, 
  **              statutory or otherwise, and Orange Tree Technologies specifically 
  **              disclaims any implied warranties of merchantability, non-infringement,
  **              or fitness for a particular purpose. Orange Tree Technologies does not
  **              warrant that the functions contained in these designs will meet your 
  **              requirements, or that the operation of these designs will be 
  **              uninterrupted or error free, or that defects in the Designs will be 
  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
  **              make any representations regarding use or the results of the use of the 
  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
  **
  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
  **              or its licensors be liable for any loss of data, lost profits, cost or 
  **              procurement of substitute goods or services, or for any special, 
  **              incidental, consequential, or indirect damages arising from the use or 
  **              operation of the designs or accompanying documentation, however caused 
  **              and on any theory of liability. This limitation will apply even if 
  **              Orange Tree Technologies has been advised of the possibility of such 
  **              damage. This limitation shall apply notwithstanding the failure of the 
  **              essential purpose of any limited remedies herein.
  **
  *****************************************************************************************
*/

`timescale 1ns / 1ps
`unconnected_drive pull0

module ZestET1_EthernetWrapper
(
    // Clock and reset
    input User_CLK,
    input User_RST,
    
    // Network status
    output User_LinkStatus,
    output reg [2:0] User_LinkSpeed,
    output reg User_LinkDuplex,
    output [31:0] User_LocalIPAddr,
    
    // User connection interfaces
    input User_Enable0,
    input User_Server0,
    input User_TCP0,
    output User_Connected0,
    input [7:0] User_MTU0,
    input [7:0] User_TTL0,
    input [15:0] User_LocalPort0,
    input [15:0] User_WriteRemotePort0,
    input [31:0] User_WriteRemoteIPAddr0,
    output [15:0] User_ReadRemotePort0,
    output [31:0] User_ReadRemoteIPAddr0,
    input User_TxPause0,
    output User_TxPaused0,
    input [7:0] User_TxData0,
    input User_TxEnable0,
    output User_TxBusy0,
    output User_TxBufferEmpty0,
    output [7:0] User_RxData0,
    output User_RxEnable0,
    input User_RxBusy0,
    
    input User_Enable1,
    input User_Server1,
    input User_TCP1,
    output User_Connected1,
    input [7:0] User_MTU1,
    input [7:0] User_TTL1,
    input [15:0] User_LocalPort1,
    input [15:0] User_WriteRemotePort1,
    input [31:0] User_WriteRemoteIPAddr1,
    output [15:0] User_ReadRemotePort1,
    output [31:0] User_ReadRemoteIPAddr1,
    input User_TxPause1,
    output User_TxPaused1,
    input [7:0] User_TxData1,
    input User_TxEnable1,
    output User_TxBusy1,
    output User_TxBufferEmpty1,
    output [7:0] User_RxData1,
    output User_RxEnable1,
    input User_RxBusy1,

    input User_Enable2,
    input User_Server2,
    input User_TCP2,
    output User_Connected2,
    input [7:0] User_MTU2,
    input [7:0] User_TTL2,
    input [15:0] User_LocalPort2,
    input [15:0] User_WriteRemotePort2,
    input [31:0] User_WriteRemoteIPAddr2,
    output [15:0] User_ReadRemotePort2,
    output [31:0] User_ReadRemoteIPAddr2,
    input User_TxPause2,
    output User_TxPaused2,
    input [7:0] User_TxData2,
    input User_TxEnable2,
    output User_TxBusy2,
    output User_TxBufferEmpty2,
    output [7:0] User_RxData2,
    output User_RxEnable2,
    input User_RxBusy2,
    
    input User_Enable3,
    input User_Server3,
    input User_TCP3,
    output User_Connected3,
    input [7:0] User_MTU3,
    input [7:0] User_TTL3,
    input [15:0] User_LocalPort3,
    input [15:0] User_WriteRemotePort3,
    input [31:0] User_WriteRemoteIPAddr3,
    output [15:0] User_ReadRemotePort3,
    output [31:0] User_ReadRemoteIPAddr3,
    input User_TxPause3,
    output User_TxPaused3,
    input [7:0] User_TxData3,
    input User_TxEnable3,
    output User_TxBusy3,
    output User_TxBufferEmpty3,
    output [7:0] User_RxData3,
    output User_RxEnable3,
    input User_RxBusy3,
    
    input User_Enable4,
    input User_Server4,
    input User_TCP4,
    output User_Connected4,
    input [7:0] User_MTU4,
    input [7:0] User_TTL4,
    input [15:0] User_LocalPort4,
    input [15:0] User_WriteRemotePort4,
    input [31:0] User_WriteRemoteIPAddr4,
    output [15:0] User_ReadRemotePort4,
    output [31:0] User_ReadRemoteIPAddr4,
    input User_TxPause4,
    output User_TxPaused4,
    input [7:0] User_TxData4,
    input User_TxEnable4,
    output User_TxBusy4,
    output User_TxBufferEmpty4,
    output [7:0] User_RxData4,
    output User_RxEnable4,
    input User_RxBusy4,
    
    input User_Enable5,
    input User_Server5,
    input User_TCP5,
    output User_Connected5,
    input [7:0] User_MTU5,
    input [7:0] User_TTL5,
    input [15:0] User_LocalPort5,
    input [15:0] User_WriteRemotePort5,
    input [31:0] User_WriteRemoteIPAddr5,
    output [15:0] User_ReadRemotePort5,
    output [31:0] User_ReadRemoteIPAddr5,
    input User_TxPause5,
    output User_TxPaused5,
    input [7:0] User_TxData5,
    input User_TxEnable5,
    output User_TxBusy5,
    output User_TxBufferEmpty5,
    output [7:0] User_RxData5,
    output User_RxEnable5,
    input User_RxBusy5,
    
    input User_Enable6,
    input User_Server6,
    input User_TCP6,
    output User_Connected6,
    input [7:0] User_MTU6,
    input [7:0] User_TTL6,
    input [15:0] User_LocalPort6,
    input [15:0] User_WriteRemotePort6,
    input [31:0] User_WriteRemoteIPAddr6,
    output [15:0] User_ReadRemotePort6,
    output [31:0] User_ReadRemoteIPAddr6,
    input User_TxPause6,
    output User_TxPaused6,
    input [7:0] User_TxData6,
    input User_TxEnable6,
    output User_TxBusy6,
    output User_TxBufferEmpty6,
    output [7:0] User_RxData6,
    output User_RxEnable6,
    input User_RxBusy6,
    
    input User_Enable7,
    input User_Server7,
    input User_TCP7,
    output User_Connected7,
    input [7:0] User_MTU7,
    input [7:0] User_TTL7,
    input [15:0] User_LocalPort7,
    input [15:0] User_WriteRemotePort7,
    input [31:0] User_WriteRemoteIPAddr7,
    output [15:0] User_ReadRemotePort7,
    output [31:0] User_ReadRemoteIPAddr7,
    input User_TxPause7,
    output User_TxPaused7,
    input [7:0] User_TxData7,
    input User_TxEnable7,
    output User_TxBusy7,
    output User_TxBufferEmpty7,
    output [7:0] User_RxData7,
    output User_RxEnable7,
    input User_RxBusy7,
    
    input User_Enable8,
    input User_Server8,
    input User_TCP8,
    output User_Connected8,
    input [7:0] User_MTU8,
    input [7:0] User_TTL8,
    input [15:0] User_LocalPort8,
    input [15:0] User_WriteRemotePort8,
    input [31:0] User_WriteRemoteIPAddr8,
    output [15:0] User_ReadRemotePort8,
    output [31:0] User_ReadRemoteIPAddr8,
    input User_TxPause8,
    output User_TxPaused8,
    input [7:0] User_TxData8,
    input User_TxEnable8,
    output User_TxBusy8,
    output User_TxBufferEmpty8,
    output [7:0] User_RxData8,
    output User_RxEnable8,
    input User_RxBusy8,
    
    input User_Enable9,
    input User_Server9,
    input User_TCP9,
    output User_Connected9,
    input [7:0] User_MTU9,
    input [7:0] User_TTL9,
    input [15:0] User_LocalPort9,
    input [15:0] User_WriteRemotePort9,
    input [31:0] User_WriteRemoteIPAddr9,
    output [15:0] User_ReadRemotePort9,
    output [31:0] User_ReadRemoteIPAddr9,
    input User_TxPause9,
    output User_TxPaused9,
    input [7:0] User_TxData9,
    input User_TxEnable9,
    output User_TxBusy9,
    output User_TxBufferEmpty9,
    output [7:0] User_RxData9,
    output User_RxEnable9,
    input User_RxBusy9,
    
    input User_Enable10,
    input User_Server10,
    input User_TCP10,
    output User_Connected10,
    input [7:0] User_MTU10,
    input [7:0] User_TTL10,
    input [15:0] User_LocalPort10,
    input [15:0] User_WriteRemotePort10,
    input [31:0] User_WriteRemoteIPAddr10,
    output [15:0] User_ReadRemotePort10,
    output [31:0] User_ReadRemoteIPAddr10,
    input User_TxPause10,
    output User_TxPaused10,
    input [7:0] User_TxData10,
    input User_TxEnable10,
    output User_TxBusy10,
    output User_TxBufferEmpty10,
    output [7:0] User_RxData10,
    output User_RxEnable10,
    input User_RxBusy10,
    
    input User_Enable11,
    input User_Server11,
    input User_TCP11,
    output User_Connected11,
    input [7:0] User_MTU11,
    input [7:0] User_TTL11,
    input [15:0] User_LocalPort11,
    input [15:0] User_WriteRemotePort11,
    input [31:0] User_WriteRemoteIPAddr11,
    output [15:0] User_ReadRemotePort11,
    output [31:0] User_ReadRemoteIPAddr11,
    input User_TxPause11,
    output User_TxPaused11,
    input [7:0] User_TxData11,
    input User_TxEnable11,
    output User_TxBusy11,
    output User_TxBufferEmpty11,
    output [7:0] User_RxData11,
    output User_RxEnable11,
    input User_RxBusy11,
    
    input User_Enable12,
    input User_Server12,
    input User_TCP12,
    output User_Connected12,
    input [7:0] User_MTU12,
    input [7:0] User_TTL12,
    input [15:0] User_LocalPort12,
    input [15:0] User_WriteRemotePort12,
    input [31:0] User_WriteRemoteIPAddr12,
    output [15:0] User_ReadRemotePort12,
    output [31:0] User_ReadRemoteIPAddr12,
    input User_TxPause12,
    output User_TxPaused12,
    input [7:0] User_TxData12,
    input User_TxEnable12,
    output User_TxBusy12,
    output User_TxBufferEmpty12,
    output [7:0] User_RxData12,
    output User_RxEnable12,
    input User_RxBusy12,
    
    input User_Enable13,
    input User_Server13,
    input User_TCP13,
    output User_Connected13,
    input [7:0] User_MTU13,
    input [7:0] User_TTL13,
    input [15:0] User_LocalPort13,
    input [15:0] User_WriteRemotePort13,
    input [31:0] User_WriteRemoteIPAddr13,
    output [15:0] User_ReadRemotePort13,
    output [31:0] User_ReadRemoteIPAddr13,
    input User_TxPause13,
    output User_TxPaused13,
    input [7:0] User_TxData13,
    input User_TxEnable13,
    output User_TxBusy13,
    output User_TxBufferEmpty13,
    output [7:0] User_RxData13,
    output User_RxEnable13,
    input User_RxBusy13,
    
    input User_Enable14,
    input User_Server14,
    input User_TCP14,
    output User_Connected14,
    input [7:0] User_MTU14,
    input [7:0] User_TTL14,
    input [15:0] User_LocalPort14,
    input [15:0] User_WriteRemotePort14,
    input [31:0] User_WriteRemoteIPAddr14,
    output [15:0] User_ReadRemotePort14,
    output [31:0] User_ReadRemoteIPAddr14,
    input User_TxPause14,
    output User_TxPaused14,
    input [7:0] User_TxData14,
    input User_TxEnable14,
    output User_TxBusy14,
    output User_TxBufferEmpty14,
    output [7:0] User_RxData14,
    output User_RxEnable14,
    input User_RxBusy14,
    
    input User_Enable15,
    input User_Server15,
    input User_TCP15,
    output User_Connected15,
    input [7:0] User_MTU15,
    input [7:0] User_TTL15,
    input [15:0] User_LocalPort15,
    input [15:0] User_WriteRemotePort15,
    input [31:0] User_WriteRemoteIPAddr15,
    output [15:0] User_ReadRemotePort15,
    output [31:0] User_ReadRemoteIPAddr15,
    input User_TxPause15,
    output User_TxPaused15,
    input [7:0] User_TxData15,
    input User_TxEnable15,
    output User_TxBusy15,
    output User_TxBufferEmpty15,
    output [7:0] User_RxData15,
    output User_RxEnable15,
    input User_RxBusy15,
    
    // Interface to GigExpedite
    output Eth_Clk,
    output Eth_CSn,
    output Eth_WEn,
    output [4:0] Eth_A,
    inout [15:0] Eth_D,
    output [1:0] Eth_BE,
    input Eth_Intn
);

    parameter CLOCK_RATE = 125000000;

    //////////////////////////////////////////////////////////////////////////
    // Convert individual signals to arrays and vice versa
    reg User_LinkStatusVal;
    assign User_LinkStatus = User_LinkStatusVal;
    reg [31:0] User_LocalIPAddrVal;
    assign User_LocalIPAddr = User_LocalIPAddrVal;
    wire [15:0] User_Enable = {User_Enable15, User_Enable14,
                               User_Enable13, User_Enable12, 
                               User_Enable11, User_Enable10, 
                               User_Enable9, User_Enable8, 
                               User_Enable7, User_Enable6, 
                               User_Enable5, User_Enable4, 
                               User_Enable3, User_Enable2, 
                               User_Enable1, User_Enable0};
    wire [15:0] User_Server = {User_Server15, User_Server14,
                               User_Server13, User_Server12, 
                               User_Server11, User_Server10, 
                               User_Server9, User_Server8, 
                               User_Server7, User_Server6, 
                               User_Server5, User_Server4, 
                               User_Server3, User_Server2, 
                               User_Server1, User_Server0};
    wire [15:0] User_TCP = {User_TCP15, User_TCP14,
                            User_TCP13, User_TCP12, 
                            User_TCP11, User_TCP10, 
                            User_TCP9, User_TCP8, 
                            User_TCP7, User_TCP6, 
                            User_TCP5, User_TCP4, 
                            User_TCP3, User_TCP2, 
                            User_TCP1, User_TCP0};
    reg [15:0] User_Connected;
    assign User_Connected0 = User_Connected[0];
    assign User_Connected1 = User_Connected[1];
    assign User_Connected2 = User_Connected[2];
    assign User_Connected3 = User_Connected[3];
    assign User_Connected4 = User_Connected[4];
    assign User_Connected5 = User_Connected[5];
    assign User_Connected6 = User_Connected[6];
    assign User_Connected7 = User_Connected[7];
    assign User_Connected8 = User_Connected[8];
    assign User_Connected9 = User_Connected[9];
    assign User_Connected10 = User_Connected[10];
    assign User_Connected11 = User_Connected[11];
    assign User_Connected12 = User_Connected[12];
    assign User_Connected13 = User_Connected[13];
    assign User_Connected14 = User_Connected[14];
    assign User_Connected15 = User_Connected[15];
    wire [15:0] User_MTUTTL[0:15];
    assign User_MTUTTL[0] = {User_MTU0, User_TTL0};
    assign User_MTUTTL[1] = {User_MTU1, User_TTL1};
    assign User_MTUTTL[2] = {User_MTU2, User_TTL2};
    assign User_MTUTTL[3] = {User_MTU3, User_TTL3};
    assign User_MTUTTL[4] = {User_MTU4, User_TTL4};
    assign User_MTUTTL[5] = {User_MTU5, User_TTL5};
    assign User_MTUTTL[6] = {User_MTU6, User_TTL6};
    assign User_MTUTTL[7] = {User_MTU7, User_TTL7};
    assign User_MTUTTL[8] = {User_MTU8, User_TTL8};
    assign User_MTUTTL[9] = {User_MTU9, User_TTL9};
    assign User_MTUTTL[10] = {User_MTU10, User_TTL10};
    assign User_MTUTTL[11] = {User_MTU11, User_TTL11};
    assign User_MTUTTL[12] = {User_MTU12, User_TTL12};
    assign User_MTUTTL[13] = {User_MTU13, User_TTL13};
    assign User_MTUTTL[14] = {User_MTU14, User_TTL14};
    assign User_MTUTTL[15] = {User_MTU15, User_TTL15};
    wire [15:0] User_LocalPort[0:15];
    assign User_LocalPort[0] = User_LocalPort0;
    assign User_LocalPort[1] = User_LocalPort1;
    assign User_LocalPort[2] = User_LocalPort2;
    assign User_LocalPort[3] = User_LocalPort3;
    assign User_LocalPort[4] = User_LocalPort4;
    assign User_LocalPort[5] = User_LocalPort5;
    assign User_LocalPort[6] = User_LocalPort6;
    assign User_LocalPort[7] = User_LocalPort7;
    assign User_LocalPort[8] = User_LocalPort8;
    assign User_LocalPort[9] = User_LocalPort9;
    assign User_LocalPort[10] = User_LocalPort10;
    assign User_LocalPort[11] = User_LocalPort11;
    assign User_LocalPort[12] = User_LocalPort12;
    assign User_LocalPort[13] = User_LocalPort13;
    assign User_LocalPort[14] = User_LocalPort14;
    assign User_LocalPort[15] = User_LocalPort15;
    wire [15:0] User_WriteRemotePort[0:15];
    assign User_WriteRemotePort[0] = User_WriteRemotePort0;
    assign User_WriteRemotePort[1] = User_WriteRemotePort1;
    assign User_WriteRemotePort[2] = User_WriteRemotePort2;
    assign User_WriteRemotePort[3] = User_WriteRemotePort3;
    assign User_WriteRemotePort[4] = User_WriteRemotePort4;
    assign User_WriteRemotePort[5] = User_WriteRemotePort5;
    assign User_WriteRemotePort[6] = User_WriteRemotePort6;
    assign User_WriteRemotePort[7] = User_WriteRemotePort7;
    assign User_WriteRemotePort[8] = User_WriteRemotePort8;
    assign User_WriteRemotePort[9] = User_WriteRemotePort9;
    assign User_WriteRemotePort[10] = User_WriteRemotePort10;
    assign User_WriteRemotePort[11] = User_WriteRemotePort11;
    assign User_WriteRemotePort[12] = User_WriteRemotePort12;
    assign User_WriteRemotePort[13] = User_WriteRemotePort13;
    assign User_WriteRemotePort[14] = User_WriteRemotePort14;
    assign User_WriteRemotePort[15] = User_WriteRemotePort15;
    wire [31:0] User_WriteRemoteIPAddr[0:15];
    assign User_WriteRemoteIPAddr[0] = User_WriteRemoteIPAddr0;
    assign User_WriteRemoteIPAddr[1] = User_WriteRemoteIPAddr1;
    assign User_WriteRemoteIPAddr[2] = User_WriteRemoteIPAddr2;
    assign User_WriteRemoteIPAddr[3] = User_WriteRemoteIPAddr3;
    assign User_WriteRemoteIPAddr[4] = User_WriteRemoteIPAddr4;
    assign User_WriteRemoteIPAddr[5] = User_WriteRemoteIPAddr5;
    assign User_WriteRemoteIPAddr[6] = User_WriteRemoteIPAddr6;
    assign User_WriteRemoteIPAddr[7] = User_WriteRemoteIPAddr7;
    assign User_WriteRemoteIPAddr[8] = User_WriteRemoteIPAddr8;
    assign User_WriteRemoteIPAddr[9] = User_WriteRemoteIPAddr9;
    assign User_WriteRemoteIPAddr[10] = User_WriteRemoteIPAddr10;
    assign User_WriteRemoteIPAddr[11] = User_WriteRemoteIPAddr11;
    assign User_WriteRemoteIPAddr[12] = User_WriteRemoteIPAddr12;
    assign User_WriteRemoteIPAddr[13] = User_WriteRemoteIPAddr13;
    assign User_WriteRemoteIPAddr[14] = User_WriteRemoteIPAddr14;
    assign User_WriteRemoteIPAddr[15] = User_WriteRemoteIPAddr15;
    reg [15:0] User_ReadRemotePort[0:15];
    assign User_ReadRemotePort0 = User_ReadRemotePort[0];
    assign User_ReadRemotePort1 = User_ReadRemotePort[1];
    assign User_ReadRemotePort2 = User_ReadRemotePort[2];
    assign User_ReadRemotePort3 = User_ReadRemotePort[3];
    assign User_ReadRemotePort4 = User_ReadRemotePort[4];
    assign User_ReadRemotePort5 = User_ReadRemotePort[5];
    assign User_ReadRemotePort6 = User_ReadRemotePort[6];
    assign User_ReadRemotePort7 = User_ReadRemotePort[7];
    assign User_ReadRemotePort8 = User_ReadRemotePort[8];
    assign User_ReadRemotePort9 = User_ReadRemotePort[9];
    assign User_ReadRemotePort10 = User_ReadRemotePort[10];
    assign User_ReadRemotePort11 = User_ReadRemotePort[11];
    assign User_ReadRemotePort12 = User_ReadRemotePort[12];
    assign User_ReadRemotePort13 = User_ReadRemotePort[13];
    assign User_ReadRemotePort14 = User_ReadRemotePort[14];
    assign User_ReadRemotePort15 = User_ReadRemotePort[15];
    reg [31:0] User_ReadRemoteIPAddr[0:15];
    assign User_ReadRemoteIPAddr0 = User_ReadRemoteIPAddr[0];
    assign User_ReadRemoteIPAddr1 = User_ReadRemoteIPAddr[1];
    assign User_ReadRemoteIPAddr2 = User_ReadRemoteIPAddr[2];
    assign User_ReadRemoteIPAddr3 = User_ReadRemoteIPAddr[3];
    assign User_ReadRemoteIPAddr4 = User_ReadRemoteIPAddr[4];
    assign User_ReadRemoteIPAddr5 = User_ReadRemoteIPAddr[5];
    assign User_ReadRemoteIPAddr6 = User_ReadRemoteIPAddr[6];
    assign User_ReadRemoteIPAddr7 = User_ReadRemoteIPAddr[7];
    assign User_ReadRemoteIPAddr8 = User_ReadRemoteIPAddr[8];
    assign User_ReadRemoteIPAddr9 = User_ReadRemoteIPAddr[9];
    assign User_ReadRemoteIPAddr10 = User_ReadRemoteIPAddr[10];
    assign User_ReadRemoteIPAddr11 = User_ReadRemoteIPAddr[11];
    assign User_ReadRemoteIPAddr12 = User_ReadRemoteIPAddr[12];
    assign User_ReadRemoteIPAddr13 = User_ReadRemoteIPAddr[13];
    assign User_ReadRemoteIPAddr14 = User_ReadRemoteIPAddr[14];
    assign User_ReadRemoteIPAddr15 = User_ReadRemoteIPAddr[15];
    wire [15:0] User_TxPause = {User_TxPause15, User_TxPause14,
                                User_TxPause13, User_TxPause12,
                                User_TxPause11, User_TxPause10,
                                User_TxPause9, User_TxPause8,
                                User_TxPause7, User_TxPause6,
                                User_TxPause5, User_TxPause4,
                                User_TxPause3, User_TxPause2,
                                User_TxPause1, User_TxPause0};
    reg [15:0] User_TxPaused;
    assign User_TxPaused0 = User_TxPaused[0];
    assign User_TxPaused1 = User_TxPaused[1];
    assign User_TxPaused2 = User_TxPaused[2];
    assign User_TxPaused3 = User_TxPaused[3];
    assign User_TxPaused4 = User_TxPaused[4];
    assign User_TxPaused5 = User_TxPaused[5];
    assign User_TxPaused6 = User_TxPaused[6];
    assign User_TxPaused7 = User_TxPaused[7];
    assign User_TxPaused8 = User_TxPaused[8];
    assign User_TxPaused9 = User_TxPaused[9];
    assign User_TxPaused10 = User_TxPaused[10];
    assign User_TxPaused11 = User_TxPaused[11];
    assign User_TxPaused12 = User_TxPaused[12];
    assign User_TxPaused13 = User_TxPaused[13];
    assign User_TxPaused14 = User_TxPaused[14];
    assign User_TxPaused15 = User_TxPaused[15];
    wire [7:0] User_TxData[0:15];
    assign User_TxData[0] = User_TxData0;
    assign User_TxData[1] = User_TxData1;
    assign User_TxData[2] = User_TxData2;
    assign User_TxData[3] = User_TxData3;
    assign User_TxData[4] = User_TxData4;
    assign User_TxData[5] = User_TxData5;
    assign User_TxData[6] = User_TxData6;
    assign User_TxData[7] = User_TxData7;
    assign User_TxData[8] = User_TxData8;
    assign User_TxData[9] = User_TxData9;
    assign User_TxData[10] = User_TxData10;
    assign User_TxData[11] = User_TxData11;
    assign User_TxData[12] = User_TxData12;
    assign User_TxData[13] = User_TxData13;
    assign User_TxData[14] = User_TxData14;
    assign User_TxData[15] = User_TxData15;
    wire [15:0] User_TxEnable = {User_TxEnable15, User_TxEnable14,
                                 User_TxEnable13, User_TxEnable12,
                                 User_TxEnable11, User_TxEnable10,
                                 User_TxEnable9, User_TxEnable8,
                                 User_TxEnable7, User_TxEnable6,
                                 User_TxEnable5, User_TxEnable4,
                                 User_TxEnable3, User_TxEnable2,
                                 User_TxEnable1, User_TxEnable0
                                };
    wire [15:0] User_TxBusy;
    assign User_TxBusy0 = User_TxBusy[0];
    assign User_TxBusy1 = User_TxBusy[1];
    assign User_TxBusy2 = User_TxBusy[2];
    assign User_TxBusy3 = User_TxBusy[3];
    assign User_TxBusy4 = User_TxBusy[4];
    assign User_TxBusy5 = User_TxBusy[5];
    assign User_TxBusy6 = User_TxBusy[6];
    assign User_TxBusy7 = User_TxBusy[7];
    assign User_TxBusy8 = User_TxBusy[8];
    assign User_TxBusy9 = User_TxBusy[9];
    assign User_TxBusy10 = User_TxBusy[10];
    assign User_TxBusy11 = User_TxBusy[11];
    assign User_TxBusy12 = User_TxBusy[12];
    assign User_TxBusy13 = User_TxBusy[13];
    assign User_TxBusy14 = User_TxBusy[14];
    assign User_TxBusy15 = User_TxBusy[15];
    reg [15:0] User_TxBufferEmpty;
    assign User_TxBufferEmpty0 = User_TxBufferEmpty[0];
    assign User_TxBufferEmpty1 = User_TxBufferEmpty[1];
    assign User_TxBufferEmpty2 = User_TxBufferEmpty[2];
    assign User_TxBufferEmpty3 = User_TxBufferEmpty[3];
    assign User_TxBufferEmpty4 = User_TxBufferEmpty[4];
    assign User_TxBufferEmpty5 = User_TxBufferEmpty[5];
    assign User_TxBufferEmpty6 = User_TxBufferEmpty[6];
    assign User_TxBufferEmpty7 = User_TxBufferEmpty[7];
    assign User_TxBufferEmpty8 = User_TxBufferEmpty[8];
    assign User_TxBufferEmpty9 = User_TxBufferEmpty[9];
    assign User_TxBufferEmpty10 = User_TxBufferEmpty[10];
    assign User_TxBufferEmpty11 = User_TxBufferEmpty[11];
    assign User_TxBufferEmpty12 = User_TxBufferEmpty[12];
    assign User_TxBufferEmpty13 = User_TxBufferEmpty[13];
    assign User_TxBufferEmpty14 = User_TxBufferEmpty[14];
    assign User_TxBufferEmpty15 = User_TxBufferEmpty[15];
    wire [7:0] User_RxData[0:15];
    assign User_RxData0 = User_RxData[0];
    assign User_RxData1 = User_RxData[1];
    assign User_RxData2 = User_RxData[2];
    assign User_RxData3 = User_RxData[3];
    assign User_RxData4 = User_RxData[4];
    assign User_RxData5 = User_RxData[5];
    assign User_RxData6 = User_RxData[6];
    assign User_RxData7 = User_RxData[7];
    assign User_RxData8 = User_RxData[8];
    assign User_RxData9 = User_RxData[9];
    assign User_RxData10 = User_RxData[10];
    assign User_RxData11 = User_RxData[11];
    assign User_RxData12 = User_RxData[12];
    assign User_RxData13 = User_RxData[13];
    assign User_RxData14 = User_RxData[14];
    assign User_RxData15 = User_RxData[15];
    wire [15:0] User_RxEnable;
    assign User_RxEnable0 = User_RxEnable[0];
    assign User_RxEnable1 = User_RxEnable[1];
    assign User_RxEnable2 = User_RxEnable[2];
    assign User_RxEnable3 = User_RxEnable[3];
    assign User_RxEnable4 = User_RxEnable[4];
    assign User_RxEnable5 = User_RxEnable[5];
    assign User_RxEnable6 = User_RxEnable[6];
    assign User_RxEnable7 = User_RxEnable[7];
    assign User_RxEnable8 = User_RxEnable[8];
    assign User_RxEnable9 = User_RxEnable[9];
    assign User_RxEnable10 = User_RxEnable[10];
    assign User_RxEnable11 = User_RxEnable[11];
    assign User_RxEnable12 = User_RxEnable[12];
    assign User_RxEnable13 = User_RxEnable[13];
    assign User_RxEnable14 = User_RxEnable[14];
    assign User_RxEnable15 = User_RxEnable[15];
    wire [15:0] User_RxBusy = {User_RxBusy15, User_RxBusy14,
                               User_RxBusy13, User_RxBusy12,
                               User_RxBusy11, User_RxBusy10,
                               User_RxBusy9, User_RxBusy8,
                               User_RxBusy7, User_RxBusy6,
                               User_RxBusy5, User_RxBusy4,
                               User_RxBusy3, User_RxBusy2,
                               User_RxBusy1, User_RxBusy0
                              };

    //////////////////////////////////////////////////////////////////////////
    // Declare constants
    
    // GigExpedite register addresses
    localparam LOCAL_IP_ADDR_LOW        = 0;
    localparam LOCAL_IP_ADDR_HIGH       = 1;
    localparam LINK_STATUS              = 2;
    localparam CONNECTION_PAGE          = 7;
    localparam LOCAL_PORT               = 8;
    localparam REMOTE_IP_ADDR_LOW       = 9;
    localparam REMOTE_IP_ADDR_HIGH      = 10;
    localparam REMOTE_PORT              = 11;
    localparam MTU_TTL                  = 12;
    localparam INTERRUPT_ENABLE_STATUS  = 13;
    localparam CONNECTION_STATE         = 14;
    localparam FRAME_LENGTH             = 15;
    localparam DATA_FIFO                = 16;
    
    // Connection states (for CONNECTION_STATE reg)
    localparam CLOSED       = 16'h0000;
    localparam LISTEN       = 16'h0001;
    localparam CONNECT      = 16'h0002;
    localparam ESTABLISHED  = 16'h0003;
    localparam CONN_TCP     = 16'h0010;
    localparam CONN_ENABLE  = 16'h0020;
    localparam CONN_TCP_BIT = 4;
    localparam CONN_ENABLE_BIT = 5;
    
    // Interrupt enable and status bits (for INTERRUPT_ENABLE_STATUS reg)
    localparam IE_INCOMING          = 16'h0001;
    localparam IE_OUTGOING_EMPTY    = 16'h0002;
    localparam IE_OUTGOING_NOT_FULL = 16'h0004;
    localparam IE_STATE             = 16'h0008;
    localparam IE_INCOMING_BIT          = 0;
    localparam IE_OUTGOING_EMPTY_BIT    = 1;
    localparam IE_OUTGOING_NOT_FULL_BIT = 2;
    localparam IE_STATE_BIT             = 3;

    // Max burst transfer length when reading from GigExpedite
    localparam USER_BURST_LENGTH_WORDS = 96;

    // Control state machine states
    localparam DELAY_INIT               = 15'h0001;
    localparam CLEAN                    = 15'h0002;
    localparam CLEAN_CHECK              = 15'h0004;
    localparam CLEAR_INTS               = 15'h0008;
    localparam IDLE                     = 15'h0010;
    localparam GET_STATUS               = 15'h0020;
    localparam USER_STATE_CHANGE_REQ    = 15'h0040;
    localparam OPEN_CONNECTION          = 15'h0080;
    localparam CLOSE_CONNECTION         = 15'h0100;
    localparam PAUSE_UNPAUSE_CONNECTION = 15'h0200;
    localparam CHECK_STATE              = 15'h0400;
    localparam PROCESS_INTERRUPTS       = 15'h0800;
    localparam STATE_CHANGE             = 15'h1000;
    localparam READ_LENGTH              = 15'h2000;
    localparam WRITE_LENGTH             = 15'h4000;

    // Connection control states
    localparam CS_DISABLED              = 3'h0;
    localparam CS_WAIT_ESTAB            = 3'h1;
    localparam CS_ESTAB                 = 3'h2;
    localparam CS_WAIT_CLOSE            = 3'h3;
    localparam CS_WAIT_DISABLED         = 3'h4;
    
    //////////////////////////////////////////////////////////////////////////
    // Declare signals
    
    // GigExpedite interface wires
    // There are two types of access - control and data
    // Multiplex between the two here
    reg CtrlWE;
    reg CtrlRE;
    reg [4:0] CtrlAddr;
    reg [1:0] CtrlBE;
    reg [15:0] CtrlWriteData;
    reg [7:0] CtrlOwner;
    reg DataWE;
    reg DataWEReq;
    reg DataRE;
    reg [4:0] DataAddr;
    reg [1:0] DataBE;
    reg [15:0] DataWriteData;
    reg [7:0] DataOwner;
    wire UserWE = CtrlWE | DataWE;
    wire UserRE = CtrlRE | DataRE;
    wire [4:0] UserAddr = (CtrlWE | CtrlRE) ? CtrlAddr : DataAddr;
    wire [1:0] UserBE = (CtrlWE | CtrlRE) ? CtrlBE : DataBE;
    wire [15:0] UserWriteData = (CtrlWE | CtrlRE) ? CtrlWriteData : DataWriteData;
    wire [15:0] UserReadData;
    wire UserReadDataValid;
    wire UserInterrupt;
    wire [7:0] UserOwner = CtrlRE ? CtrlOwner : DataOwner;
    wire [7:0] UserValidOwner;
    reg [7:0] LinkUpCount;
    integer i;

    // Control state machine
    reg [23:0] Delay;
    reg DelayDone;
    reg CleanDone;
    reg [15:0] SinceStatusUpdate;
    reg SinceStatusUpdateDone;
    reg [15:0] User_EnableEdge;
    reg User_EnableEdgeVal;
    reg [15:0] User_TxPauseEdge;
    reg [15:0] LastUser_Enable;
    reg [15:0] LastUser_TxPause;
    reg [15:0] User_RegEnable;
    reg [2:0] ConnState[0:15];
    reg [3:0] CtrlConnection;
    wire User_EnableVal = User_Enable[CtrlConnection];
    wire User_RegEnableVal = User_RegEnable[CtrlConnection];
    reg [15:0] CtrlConnectionMask;
    reg NextCtrlConnectionValid;
    reg [3:0] NextCtrlConnection;
    wire [3:0] NextCtrlConnectionPlus1 = NextCtrlConnection+1;
    reg [15:0] NextCtrlConnectionMask;
    reg [14:0] CtrlState;
    reg [15:0] CtrlSubState;
    reg [15:0] UserValidCount;
    reg [5:0] CurrentState;
    reg [3:0] InterruptState;
    reg [4:0] BlockDataConnection;
    reg BlockDataRE;
    reg BlockDataWE;
    reg BlockTxMergeFIFO;
    
    // Data state machine
    reg [4:0] DataConnection;
    reg [4:0] NextDataConnection;
    reg ReadOK;
    reg WriteOK;
    reg NextTransferOK;
    reg [7:0] ArbCounter;
    
    // Intermediate Tx FIFO
    reg [3:0] TxConnection;
    reg [15:0] TxConnectionMask;
    reg [3:0] NextTxConnection;
    reg [15:0] NextTxConnectionMask;
    reg [3:0] TxMergeFIFOWriteCount;
    reg [3:0] RegTxMergeFIFOWriteCount;
    reg [3:0] TxMergeFIFOReadCount;
    wire [3:0] TxMergeFIFOReadAddr;
    reg [21:0] TxMergeFIFODataOut;
    wire [3:0] TxMergeFIFOCount;
    reg TxMergeFIFOFull;
    reg TxMergeFIFOEmpty;
    reg [21:0] TxMergeFIFO[0:15];
    wire TxMergeFIFOWE;
    wire TxMergeFIFORE;
    wire [3:0] TxMergeFIFOConn;
    wire [17:0] TxMergeFIFODataIn;
    wire [17:0] TxMergeFIFODataOutVal;

    // Per-connection state RAMs
    reg [5:0] ConnectionState[0:15];
    wire [5:0] ConnectionStateVal = ConnectionState[CtrlConnection];
    wire [5:0] ConnectionStateDataVal = ConnectionState[DataConnection[3:0]];
    wire [5:0] ConnectionStateTxDataVal = ConnectionState[TxConnection];
    wire [5:0] ConnectionStateTxMergeDataVal = ConnectionState[TxMergeFIFOConn];
    
    reg [15:0] RxFrameLength[0:15];
    reg RxFrameLengthOdd[0:15];
    reg RxFrameLengthNotZero[0:15];
    wire RxFrameLengthOddDataVal = RxFrameLengthOdd[DataConnection[3:0]];
    wire [15:0] RxFrameLengthDataVal = RxFrameLength[DataConnection[3:0]];
    wire RxFrameLengthNotZeroVal = RxFrameLengthNotZero[CtrlConnection];
    wire RxFrameLengthNotZeroDataVal = RxFrameLengthNotZero[DataConnection[3:0]];
    wire RxFrameLengthNotZeroNextDataVal = RxFrameLengthNotZero[NextDataConnection[3:0]];
    reg RxFrameLengthWE;
    reg [3:0] RxFrameLengthAddr;
    reg [15:0] RxFrameLengthData;
    reg RxFrameLengthNotZeroData;
    reg LastRxFrameLengthWE;
    reg [3:0] LastRxFrameLengthAddr;
    reg [15:0] LastRxFrameLengthData;
    reg RxFrameLengthOddData;
    
    reg [7:0] RxBurstLength[0:15];
    reg RxBurstLengthNotZero[0:15];
    wire [7:0] RxBurstLengthDataVal = RxBurstLength[DataConnection[3:0]];
    wire RxBurstLengthNotZeroVal = RxBurstLengthNotZero[CtrlConnection];
    wire RxBurstLengthNotZeroDataVal = RxBurstLengthNotZero[DataConnection[3:0]];
    wire RxBurstLengthNotZeroNextDataVal = RxBurstLengthNotZero[NextDataConnection[3:0]];
    reg RxBurstLengthWE;
    reg [3:0] RxBurstLengthAddr;
    reg [7:0] RxBurstLengthData;
    reg RxBurstLengthNotZeroData;
    
    reg [15:0] TxFrameLength[0:15];
    reg TxFrameLengthNotZero[0:15];
    wire [15:0] TxFrameLengthTxDataVal = TxFrameLength[TxConnection];
    wire TxFrameLengthNotZeroVal = TxFrameLengthNotZero[CtrlConnection];
    wire TxFrameLengthNotZeroTxDataVal = TxFrameLengthNotZero[TxConnection];
    wire TxFrameLengthNotZeroNextTxDataVal = TxFrameLengthNotZero[NextTxConnection];
    reg TxFrameLengthWE;
    reg [3:0] TxFrameLengthAddr;
    reg [15:0] TxFrameLengthData;
    reg TxFrameLengthNotZeroData;

    reg [15:0] TxBurstLength[0:15];
    reg TxBurstLengthNotZero[0:15];
    wire [15:0] TxBurstLengthTxDataVal = TxBurstLength[TxConnection];
    wire TxBurstLengthNotZeroTxDataVal = TxBurstLengthNotZero[TxConnection];
    wire TxBurstLengthNotZeroNextTxDataVal = TxBurstLengthNotZero[NextTxConnection];
    reg TxBurstLengthWE;
    reg [3:0] TxBurstLengthAddr;
    reg [15:0] TxBurstLengthData;
    reg TxBurstLengthDataNotZeroData;
    reg [7:0] TxLastByte[0:15];
    reg TxLastByteValid[0:15];
    wire [7:0] TxLastByteVal = TxLastByte[CtrlConnection];
    wire TxLastByteValidVal = TxLastByteValid[CtrlConnection];
    reg [15:0] FrameLength;
    reg [7:0] DataByte;
    reg DataByteValid;
    
    reg [15:0] User_LocalPortVal;
    reg [15:0] User_WriteRemotePortVal;
    reg [31:0] User_WriteRemoteIPAddrVal;
    reg [15:0] User_MTUTTLVal;
    
    reg [15:0] TxBufferEmpty;

    reg [4:0] InterruptLatency[0:15];
    reg InterruptLatencyNotZero[0:15];
    wire InterruptLatencyNotZeroVal = InterruptLatencyNotZero[CtrlConnection];
    
    // FIFOs to/from user code
    wire [15:0] FIFORST;
    wire [17:0] TxFIFODataOut[0:15];
    wire [15:0] TxFIFORE;
    wire [15:0] TxFIFOREData;
    wire [15:0] TxFIFORECtrl;
    wire [15:0] TxFIFOEmpty;
    wire [15:0] TxFIFONextEmpty;
    wire [17:0] TxFIFODataOutVal = TxFIFODataOut[CtrlConnection];
    wire [17:0] TxFIFODataOutTxDataVal = TxFIFODataOut[TxConnection];
    reg TxFIFOEmptyVal;
    reg TxFIFOEmptyTxVal;
    wire TxFIFOEmptyNextTxVal = TxFIFOEmpty[NextTxConnection];
    
    wire [17:0] RxFIFODataIn;
    reg [15:0] RxFIFOWE;
    wire [15:0] RxFIFOEmpty;
    wire [15:0] RxFIFOFull;
    reg RxFIFOFullDataVal;
    wire RxFIFOFullVal = RxFIFOFull[CtrlConnection];
    wire RxFIFOFullNextVal = RxFIFOFull[NextDataConnection[3:0]];

    //////////////////////////////////////////////////////////////////////////
    // State machine to control GigExpedite
    always @ (posedge User_RST or posedge User_CLK) begin
        if (User_RST==1) begin
            CtrlState <= DELAY_INIT;
            Delay <= 0;
            DelayDone <= 0;
            CtrlSubState <= 0;
            UserValidCount <= 0;
            CtrlConnection <= 0;
            CtrlConnectionMask <= 16'h0001;
            CleanDone <= 0;
            User_Connected <= 0;
            User_RegEnable <= 0;
            User_EnableEdge <= 0;
            User_EnableEdgeVal <= 0;
            LastUser_Enable <= 0;
            User_TxPauseEdge <= 0;
            LastUser_TxPause <= 0;
            SinceStatusUpdate <= 0;
            SinceStatusUpdateDone <= 0;
            User_LinkStatusVal <= 0;
            User_LinkSpeed <= 0;
            User_LinkDuplex <= 0;
            User_LocalIPAddrVal <= 0;
            LinkUpCount <= 0;
            BlockDataConnection <= 0;
            BlockTxMergeFIFO <= 0;
            TxFIFOEmptyVal <= 1;
            for (i=0; i<16; i=i+1) begin
                ConnState[i] <= CS_DISABLED;
            end
        end else begin
            // Timers
            Delay <= Delay + 1;
            if (Delay==24'hffffff) begin
                DelayDone <= 1;
            end
            if (SinceStatusUpdate!=16'hffff) begin
                SinceStatusUpdate <= SinceStatusUpdate + 1;
                SinceStatusUpdateDone <= 0;
            end else begin
                SinceStatusUpdateDone <= 1;
            end

            // Counter of completed control register reads
            if (UserReadDataValid==1 && UserValidOwner[7]==0) begin
                UserValidCount <= UserValidCount + 1;
            end
            
            // Detect edges on enable signal
            // Delay until link is detected so that the GigEx is known to
            // be booted.  This will hold off connections if the Xilinx FPGA
            // boots fom flash.
            if (DelayDone==1 && LinkUpCount==8'hff) begin
                LastUser_Enable <= User_Enable;
                User_EnableEdge <= User_EnableEdge | (LastUser_Enable ^ User_Enable);
                LastUser_TxPause <= User_TxPause;
                User_TxPauseEdge <= User_TxPauseEdge | (LastUser_TxPause ^ User_TxPause);
            end
            
            // Register empty signal from Tx FIFOs
            TxFIFOEmptyVal <= TxFIFONextEmpty[CtrlConnection];

            CtrlSubState <= CtrlSubState + 1;
            case (CtrlState)
                // Wait here to ensure the GigExpedite has locked to
                // our clock
                DELAY_INIT: begin
                    if (DelayDone==1) begin
                        CtrlSubState <= 0;
                        CtrlState <= CLEAN;
                    end
                end

                // Reset all connections in case the previous
                // application closed unexpectedly and left some open
                // Write 0 to each connection state then
                // read the status for each connection in turn
                // Loop round until all statuses are zero.
                CLEAN: begin
                    if (CtrlSubState[4:0]==31) begin
                        CtrlSubState <= 0;
                        UserValidCount <= 0;
                        CtrlState <= CLEAN_CHECK;
                        CleanDone <= 1;
                    end 
                end
                CLEAN_CHECK: begin
                    if (UserReadDataValid==1 && UserReadData!=0) begin
                        CleanDone <= 0;
                    end
                    if (CtrlSubState[8:0]==511) begin
                        if (CleanDone==1) begin
                            CtrlSubState <= 0;
                            CtrlState <= CLEAR_INTS;
                        end else begin
                            CleanDone <= 1;
                            CtrlSubState <= 0;
                        end
                    end 
                end
                CLEAR_INTS: begin
                    if (CtrlSubState[8:0]==511) begin
                        CtrlSubState <= 0;
                        CtrlState <= IDLE;
                    end 
                end
                
                // Wait for interrupt from GigExpedite device
                // or request from user interface
                IDLE: begin
                    CtrlSubState <= 0;
                    UserValidCount <= 0;
                    if (SinceStatusUpdateDone==1) begin
                        SinceStatusUpdate <= 0;
                        SinceStatusUpdateDone <= 0;
                        CtrlState <= GET_STATUS;
                    end else if (User_TxPauseEdge!=0 || User_EnableEdge!=0) begin
                        // User has requested a connection open/close or pause/unpause
                        for (i=0; i<15; i=i+1) begin
                            if (User_TxPauseEdge[i]!=0 || User_EnableEdge[i]!=0) begin
                                CtrlConnection <= i;
                                CtrlConnectionMask <= 1<<i;
                                User_EnableEdgeVal <= User_EnableEdge[i];
                            end
                        end
                        CtrlState <= USER_STATE_CHANGE_REQ;
                    end else if (UserInterrupt==1) begin
                        // Interrupt has been received
                        CtrlConnection <= 0;
                        CtrlConnectionMask <= 16'h0001;
                        CtrlState <= CHECK_STATE;
                    end
                end

                // Update link status
                GET_STATUS: begin
                    if (UserReadDataValid==1 && UserValidOwner[7]==0) begin
                        if (UserValidCount[1:0]==2'b00) begin
                            User_LocalIPAddrVal[15:0] <= UserReadData;
                        end else if (UserValidCount[1:0]==2'b01) begin
                            User_LocalIPAddrVal[31:16] <= UserReadData;
                        end else if (UserValidCount[1:0]==2'b10) begin
                            if (User_LocalIPAddrVal!=32'h00000000) begin
                                if (LinkUpCount!=8'hff) begin
                                    LinkUpCount <= LinkUpCount + 1;
                                end
                            end else begin
                                LinkUpCount <= 8'h00;
                            end
                            User_LinkStatusVal <= UserReadData[4];
                            User_LinkDuplex <= UserReadData[3];
                            User_LinkSpeed <= UserReadData[2:0];
                            CtrlSubState <= 0;
                            CtrlState <= IDLE;
                        end
                    end
                end
                    
                // User request to open/close a connection
                USER_STATE_CHANGE_REQ: begin
                    // Change the connection state
                    CtrlSubState <= 0;
                    UserValidCount <= 0;
                    if (User_EnableEdgeVal!=0) begin
                        if (UserInterrupt==1) begin
                            // Default state if can't service state change - check interrupt
                            CtrlConnection <= 0;
                            CtrlConnectionMask <= 16'h0001;
                            CtrlState <= CHECK_STATE;
                        end else begin
                            CtrlState <= IDLE;
                        end
                        if (User_EnableVal==0 ||
                            (User_RegEnableVal==1 && User_EnableVal==1)) begin
                            // Close or close then re-open connection
                            User_EnableEdge[CtrlConnection] <= User_EnableVal;
                            User_RegEnable[CtrlConnection] <= 0;
                            if (ConnState[CtrlConnection]!=CS_WAIT_DISABLED &&
                                ConnState[CtrlConnection]!=CS_DISABLED) begin
                                // OK to service close req
                                CtrlConnection <= CtrlConnection;
                                CtrlState <= CLOSE_CONNECTION;
                            end
                        end else begin
                            if (ConnState[CtrlConnection]==CS_DISABLED) begin
                                // Open closed connection
                                CtrlConnection <= CtrlConnection;
                                User_EnableEdge[CtrlConnection] <= 0;
                                User_RegEnable[CtrlConnection] <= 1;
                                User_LocalPortVal <= User_LocalPort[CtrlConnection];
                                User_WriteRemoteIPAddrVal <= User_WriteRemoteIPAddr[CtrlConnection];
                                User_WriteRemotePortVal <= User_WriteRemotePort[CtrlConnection];
                                User_MTUTTLVal <= User_MTUTTL[CtrlConnection];
                                CtrlState <= OPEN_CONNECTION;
                            end
                        end
                    end else begin
                        // Pause/unpause
                        User_TxPauseEdge[CtrlConnection] <= 0;
                        CtrlState <= PAUSE_UNPAUSE_CONNECTION;
                    end
                end
                OPEN_CONNECTION: begin
                    if (CtrlSubState[2:0]==6) begin
                        CtrlSubState <= 0;
                        CtrlState <= IDLE;
                        ConnState[CtrlConnection] <= CS_WAIT_ESTAB;
                    end
                end
                CLOSE_CONNECTION: begin
                    if (CtrlSubState[0]==1) begin
                        CtrlSubState <= 0;
                        CtrlState <= IDLE;
                        ConnState[CtrlConnection] <= CS_WAIT_CLOSE;
                    end
                end
                PAUSE_UNPAUSE_CONNECTION: begin
                    CtrlSubState <= 0;
                    User_TxPaused[CtrlConnection] <= User_TxPause[CtrlConnection];
                    CtrlState <= IDLE;
                end
                
                // Read state and interrupt status bits
                CHECK_STATE: begin
                    if (UserReadDataValid==1 && UserValidOwner[7]==0) begin
                        if (UserValidCount[0]==0) begin
                            // Store connection state
                            CurrentState <= UserReadData[5:0];
                        end else begin
                            // Store interrupt state
                            InterruptState <= UserReadData[3:0];
                            CtrlSubState <= 0;
                            CtrlState <= PROCESS_INTERRUPTS;
                            if (UserReadData[IE_OUTGOING_NOT_FULL_BIT]==1) begin
                                BlockTxMergeFIFO <= 1;
                            end
                        end
                    end
                    if (CtrlSubState[4:0]==16) begin
                        BlockDataConnection <= {1'b1, CtrlConnection};
                    end
                end
                
                // Process interrupt from GigExpedite
                PROCESS_INTERRUPTS: begin
                    CtrlSubState <= 0;
                    UserValidCount <= 0;
                    BlockTxMergeFIFO <= 0;
                    if (InterruptState[IE_OUTGOING_NOT_FULL_BIT]==1) begin
                        // Buffer not full interrupt
                        InterruptState[IE_OUTGOING_NOT_FULL_BIT] <= 0;
                        if (ConnectionStateVal[CONN_ENABLE_BIT]==1 &&
                            ConnectionStateVal[CONN_TCP_BIT]==0 &&
                            TxFrameLengthNotZeroVal==0 &&
                            TxFIFOEmptyVal==0) begin
                            BlockTxMergeFIFO <= 1;
                            BlockDataConnection <= 0;
                            CtrlState <= WRITE_LENGTH;
                        end
                    end else if (InterruptState[IE_INCOMING_BIT]==1) begin
                        // Data available interrupt
                        InterruptState[IE_INCOMING_BIT] <= 0;
                        if (ConnectionStateVal[CONN_ENABLE_BIT]==1 &&
                            InterruptLatencyNotZeroVal==0 &&
                            (ConnectionStateVal[CONN_TCP_BIT]==1 ||
                             RxFIFOFullVal==0) &&
                            (RxBurstLengthNotZeroVal==0 ||
                             RxFrameLengthNotZeroVal==0)) begin
                            CtrlState <= READ_LENGTH;
                        end
                    end else if (InterruptState[IE_OUTGOING_EMPTY_BIT]==1) begin
                        // Buffer empty interrupt
                        InterruptState[IE_OUTGOING_EMPTY_BIT] <= 0;
                    end else if (InterruptState[IE_STATE_BIT]==1) begin
                        // State change interrupt
                        InterruptState[IE_STATE_BIT] <= 0;
                        BlockTxMergeFIFO <= 1;
                        CtrlState <= STATE_CHANGE;
                    end else if (NextCtrlConnectionValid==1) begin
                        // Move to next connection
                        CtrlConnection <= NextCtrlConnection;
                        CtrlConnectionMask <= NextCtrlConnectionMask;
                        if (NextCtrlConnection==0) begin
                            CtrlState <= IDLE;
                        end else begin
                            CtrlState <= CHECK_STATE;
                        end
                        BlockDataConnection <= 0;
                    end
                end

                // Handle state change interrupt
                STATE_CHANGE: begin
                    if (CtrlSubState[3:0]!=0 ||
                        !(UserReadDataValid==1 && UserValidOwner[6]==1)) begin
                        CtrlSubState <= CtrlSubState + 1;
                        BlockTxMergeFIFO <= 0;
                    end

                    if (CurrentState[3:0]==ESTABLISHED &&
                        ConnState[CtrlConnection]==CS_WAIT_ESTAB) begin
                        // Connection made
                        if (UserReadDataValid==1 && UserValidOwner[7]==0) begin
                            if (UserValidCount[2:0]==0) begin
                                User_ReadRemotePort[CtrlConnection] <= UserReadData;
                            end
                            if (UserValidCount[2:0]==1) begin
                                User_ReadRemoteIPAddr[CtrlConnection][31:16] <= UserReadData;
                            end
                            if (UserValidCount[2:0]==2) begin
                                User_ReadRemoteIPAddr[CtrlConnection][15:0] <= UserReadData;
                            end
                        end
                        if (UserValidCount[2:0]==3'b010 &&
                            CtrlSubState[3:0]>=8) begin
                            CtrlSubState <= 0;
                            CtrlState <= PROCESS_INTERRUPTS;
                            User_Connected[CtrlConnection] <= 1;
                            ConnState[CtrlConnection] <= CS_ESTAB;
                        end
                    end else if (CurrentState[3:0]==CLOSED[3:0] && 
                                 CurrentState[5]==1 && 
                                 (ConnState[CtrlConnection]==CS_ESTAB ||
                                  ConnState[CtrlConnection]==CS_WAIT_CLOSE)) begin
                        // Connection closed by remote end
                        if (CtrlSubState[1:0]==2'b10) begin
                            CtrlSubState <= 0;
                            CtrlState <= PROCESS_INTERRUPTS;
                            ConnState[CtrlConnection] <= CS_WAIT_DISABLED;
                        end
                    end else if (CurrentState[5]==0) begin
                        // Connection disabled
                        if (CtrlSubState[1:0]==2'b10) begin
                            CtrlSubState <= 0;
                            CtrlState <= PROCESS_INTERRUPTS;
                            User_Connected[CtrlConnection] <= 0;
                            ConnState[CtrlConnection] <= CS_DISABLED;
                        end
                    end else begin
                        // Listen or connect state acknowledgement
                        CtrlSubState <= 0;
                        CtrlState <= PROCESS_INTERRUPTS;
                    end
                end
                
                // Handle incoming frame length
                READ_LENGTH: begin
                    if (RxFrameLengthNotZeroVal==1) begin
                        // Partially read frame - new burst available
                        CtrlSubState <= 0;
                        CtrlState <= PROCESS_INTERRUPTS;
                    end else if (UserReadDataValid==1 && UserValidOwner[6]==1 &&
                                 UserValidOwner[3:0]==CtrlConnection) begin
                        // Read frame length from GigExpedite
                        CtrlSubState <= 0;
                        CtrlState <= PROCESS_INTERRUPTS;
                    end
                end

                // Copy datagram length from user FIFO to GigExpedite
                // This complicated by the length being 2 bytes
                // The first byte may be the lower half of the last
                // data word or the upper byte from the TxFIFO
                // The second byte may be the upper byte from the TxFIFO
                // or the lower byte from the TxFIFO
                // or the upper byte of the next word in the TxFIFO
                // The lower byte in the TxFIFO may be data that has to
                // be written to the GigExpedite data FIFO
                WRITE_LENGTH: begin
                    if (TxMergeFIFOEmpty==0) begin
                        // Wait for the merge FIFO to empty to prevent
                        // writing the frame length before the last data
                        // BlockTxMergeFIFO=1 prevents writes to TxMergeFIFO, but the registering
                        // of TxMergeFIFOEmpty means it could go high then low, before going high and
                        // staying high for duration of this state.
                        // To make sure TxMergeFIFO really is empty, start rest of this state at
                        // CtrlSubState=2
                        CtrlSubState <= 0;
                    end else if (CtrlSubState[2:0]==2) begin
                        BlockTxMergeFIFO <= 0;
                        DataByteValid <= 0;
                        if (TxLastByteValidVal==1) begin
                            // There is a byte left over from the previous
                            // data word that will form the top 8 bits of the
                            // frame length
                            FrameLength[15:8] <= TxLastByteVal;
                            FrameLength[7:0] <= TxFIFODataOutVal[15:8];
                            DataByteValid <= TxFIFODataOutVal[16];
                            DataByte <= TxFIFODataOutVal[7:0];
                            CtrlSubState <= 4;
                        end else if (TxFIFODataOutVal[17:16]==2'b11) begin
                            // Complete frame length in 2 byte word
                            FrameLength <= TxFIFODataOutVal[15:0];
                            CtrlSubState <= 4;
                        end else begin
                            // Half of frame length from top byte
                            FrameLength[15:8] <= TxFIFODataOutVal[15:8];
                        end
                    end else if (CtrlSubState[2:0]==3) begin
                        if (TxFIFOEmptyVal==1) begin
                            CtrlSubState <= 3;
                        end else begin
                            // Fill in bottom of frame length
                            DataByteValid <= TxFIFODataOutVal[16];
                            DataByte <= TxFIFODataOutVal[7:0];
                        end
                        FrameLength[7:0] <= TxFIFODataOutVal[15:8];
                    end else if (CtrlSubState[2:0]==4) begin
                        // Write frame length to GigExpedite here
                        if (DataByteValid==0) begin
                            BlockDataConnection <= {1'b1, CtrlConnection};
                            CtrlSubState <= 0;
                            CtrlState <= PROCESS_INTERRUPTS;
                        end else begin
                            // We will be writing an extra byte from this word
                            // to the data FIFO
                            FrameLength <= FrameLength - 1;
                        end
                    end else if (CtrlSubState[2:0]==5) begin
                        // Write extra byte from length word to data FIFO here
                        BlockDataConnection <= {1'b1, CtrlConnection};
                        CtrlSubState <= 0;
                        CtrlState <= PROCESS_INTERRUPTS;
                    end
                end
            endcase
        end
    end
    
    // Map control states to GigExpedite register accesses
    always @ (posedge User_CLK) begin
        CtrlOwner[7] <= 0;                  // Used to indicate a data read
        CtrlOwner[6] <= 0;                  // Used to block data accesses
        CtrlOwner[5:4] <= CtrlBE;           // Byte valid flags
        CtrlOwner[3:0] <= CtrlConnection;
        
        case (CtrlState)
            CLEAN: begin
                // Reset all connections
                CtrlRE <= 0;
                CtrlWE <= 1;
                CtrlBE <= 2'b11;
                CtrlAddr <= CtrlSubState[0]==0 ? CONNECTION_PAGE :
                                                 CONNECTION_STATE;
                CtrlWriteData <= CtrlSubState[0]==0 ? CtrlSubState[4:1] :
                                                      0;
            end
            
            CLEAN_CHECK: begin
                // Check all connections have been reset
                CtrlRE <= CtrlSubState[4:0]==16 ? 1 : 0;
                CtrlWE <= CtrlSubState[4:0]==0 ? 1 : 0;
                CtrlBE <= 2'b11;
                CtrlAddr <= CtrlSubState[4:0]==0 ?  CONNECTION_PAGE :
                                                    CONNECTION_STATE;
                CtrlWriteData <= CtrlSubState[8:5];
            end

            CLEAR_INTS: begin
                // Clear all remaining interrupt status bits
                CtrlRE <= CtrlSubState[4:0]==16 ? 1 : 0;
                CtrlWE <= CtrlSubState[4:0]==0 ? 1 : 0;
                CtrlBE <= 2'b11;
                CtrlAddr <= CtrlSubState[4:0]==0 ?  CONNECTION_PAGE :
                                                    INTERRUPT_ENABLE_STATUS;
                CtrlWriteData <= CtrlSubState[8:5];
            end
            
            GET_STATUS: begin
                // Read link status
                CtrlRE <= CtrlSubState[3:0]<=4'h2 ? 1 : 0;
                CtrlAddr <= CtrlSubState[1:0]==2'b00 ? LOCAL_IP_ADDR_LOW :
                            CtrlSubState[1:0]==2'b01 ? LOCAL_IP_ADDR_HIGH :
                                                       LINK_STATUS;
                CtrlWE <= 0;
                CtrlBE <= 2'b11;
                CtrlWriteData <= 0;
            end

            USER_STATE_CHANGE_REQ: begin
                // Change connection to the requested one for state change
                CtrlRE <= 0;
                CtrlWE <= 1;
                CtrlBE <= 2'b11;
                CtrlAddr <= CONNECTION_PAGE;
                CtrlWriteData <= {12'h000, CtrlConnection};
            end
            
            OPEN_CONNECTION: begin
                // Write settings to GigExpedite connection registers
                CtrlRE <= 0;
                CtrlWE <= 1;
                CtrlBE <= 2'b11;
                CtrlAddr <= CtrlSubState[2:0]==0 ? LOCAL_PORT :
                            CtrlSubState[2:0]==1 ? REMOTE_IP_ADDR_LOW :
                            CtrlSubState[2:0]==2 ? REMOTE_IP_ADDR_HIGH :
                            CtrlSubState[2:0]==3 ? REMOTE_PORT :
                            CtrlSubState[2:0]==4 ? MTU_TTL :
                            CtrlSubState[2:0]==5 ? INTERRUPT_ENABLE_STATUS :
                                                   CONNECTION_STATE;
                CtrlWriteData <= 
                            CtrlSubState[2:0]==0 ? User_LocalPortVal :
                            CtrlSubState[2:0]==1 ? User_WriteRemoteIPAddrVal[15:0] :
                            CtrlSubState[2:0]==2 ? User_WriteRemoteIPAddrVal[31:16] :
                            CtrlSubState[2:0]==3 ? User_WriteRemotePortVal :
                            CtrlSubState[2:0]==4 ? User_MTUTTLVal :
                            CtrlSubState[2:0]==5 ? IE_STATE :
                                                   (CONN_ENABLE |
                                                    (User_TCP[CtrlConnection]==1 ? CONN_TCP : 0) |
                                                    (User_TCP[CtrlConnection]==0 ? ESTABLISHED :          // UDP connection
                                                     User_Server[CtrlConnection]==1 ? LISTEN : CONNECT)   // TCP server/client
                                                   );
            end

            CLOSE_CONNECTION: begin
                // Write settings to GigExpedite connection registers
                CtrlRE <= 0;
                CtrlWE <= ConnectionStateVal!="000000" ? 1 : 0;
                CtrlBE <= 2'b11;
                CtrlAddr <= CtrlSubState[0]==0 ? CONNECTION_STATE :
                                                 INTERRUPT_ENABLE_STATUS;
                CtrlWriteData <=
                            CtrlSubState[0]==0 ? {12'h000, CLOSED[3:0]} :
                                                 {12'h000, (IE_INCOMING | IE_OUTGOING_EMPTY |
                                                            IE_OUTGOING_NOT_FULL | IE_STATE)};
            end
            
            PAUSE_UNPAUSE_CONNECTION: begin
                // Write settings to GigExpedite connection registers
                CtrlRE <= 0;
                CtrlWE <= 1;
                CtrlBE <= 2'b11;
                CtrlAddr <= CONNECTION_STATE;
                CtrlWriteData <= {9'b000000000, 
                                  User_TxPause[CtrlConnection],
                                  ConnectionStateVal};
            end
                            
            CHECK_STATE: begin
                // Read connection state and interrupt state
                CtrlRE <= CtrlSubState[4:0]==16 || CtrlSubState[4:0]==17 ? 1 : 0;
                CtrlWE <= CtrlSubState[4:0]==0 ? 1 : 0;
                CtrlBE <= 2'b11;
                CtrlAddr <= CtrlSubState[4:0]==0 ?  CONNECTION_PAGE :
                            CtrlSubState[4:0]==16 ? CONNECTION_STATE :
                                                    INTERRUPT_ENABLE_STATUS;
                CtrlWriteData <= {12'h000, CtrlConnection};
            end

            STATE_CHANGE: begin
                // Handle state change interrupt
                if (CurrentState[3:0]==ESTABLISHED &&
                    ConnState[CtrlConnection]==CS_WAIT_ESTAB) begin
                    CtrlRE <= CtrlSubState[3:0]!=0 &&
                              CtrlSubState[3:0]<4 ? 1 : 0;
                    CtrlWE <= CtrlSubState[3:0]==7 || CtrlSubState[3:0]==8 ? 1 : 0;
                    CtrlAddr <= CtrlSubState[2:0]==1 ? REMOTE_PORT : 
                                CtrlSubState[2:0]==2 ? REMOTE_IP_ADDR_HIGH :
                                CtrlSubState[2:0]==3 ? REMOTE_IP_ADDR_LOW :
                                CtrlSubState[2:0]==7 ? INTERRUPT_ENABLE_STATUS :
                                                       CONNECTION_STATE;
                    CtrlWriteData <= CtrlSubState[2:0]==7 ? (IE_STATE | IE_INCOMING |
                                                             IE_OUTGOING_NOT_FULL | IE_OUTGOING_EMPTY) :
                                                            {9'b000000000, User_TxPause[CtrlConnection], CurrentState};
                end else if (CurrentState[3:0]==CLOSED[3:0] &&
                             CurrentState[5]==1 &&
                             (ConnState[CtrlConnection]==CS_ESTAB ||
                              ConnState[CtrlConnection]==CS_WAIT_CLOSE)) begin
                    // Received closed but not disabled
                    CtrlRE <= 0;
                    CtrlWE <= CtrlSubState[3:0]!=0 ? 1 : 0;
                    CtrlAddr <= CtrlSubState[0]==1 ? INTERRUPT_ENABLE_STATUS :
                                                     CONNECTION_STATE;
                    CtrlWriteData <= CtrlSubState[0]==1 ? IE_STATE :
                                                          {12'h000, CLOSED[3:0]};
                end else if (CurrentState[5]==0) begin
                    // Received disabled
                    CtrlRE <= 0;
                    CtrlWE <= CtrlSubState[3:0]!=0 ? 1 : 0;
                    CtrlAddr <= CtrlSubState[0]==1 ? INTERRUPT_ENABLE_STATUS :
                                                     CONNECTION_STATE;
                    CtrlWriteData <= CtrlSubState[0]==1 ? 16'h0000 :
                                                          {12'h000, CLOSED[3:0]};
                end else begin
                    // Ignore other states
                    CtrlRE <= 0;
                    CtrlWE <= 0;
                end
                CtrlBE <= 2'b11;
            end
            
            READ_LENGTH: begin
                // Read from frame length register
                CtrlRE <= RxFrameLengthNotZeroVal==0 && CtrlSubState[3:0]==0 ? 1 : 0;
                CtrlWE <= 1'b0;
                CtrlBE <= 2'b11;
                CtrlAddr <= FRAME_LENGTH;
                CtrlWriteData <= 0;
                CtrlOwner[7] <= ~ConnectionStateVal[CONN_TCP_BIT];
                CtrlOwner[6] <= 1;  // Block data access to prevent conflict on 
                                    // RxFrameLength and BurstLength RAMs
            end
                
            WRITE_LENGTH: begin
                // Write to frame length register
                // Also writes additional byte to data FIFO
                CtrlRE <= 0;
                CtrlWE <= CtrlSubState[2:0]==4 || 
                          CtrlSubState[2:0]==5 ? 1 : 0;
                CtrlBE <= CtrlSubState[2:0]==4 ? 2'b11 : 2'b10;
                CtrlAddr <= CtrlSubState[2:0]==4 ? FRAME_LENGTH :
                                                   (DATA_FIFO + CtrlConnection);
                CtrlWriteData <= CtrlSubState[2:0]==4 ? FrameLength :
                                                        {DataByte, 8'h00};
                CtrlOwner[7] <= 0;
                CtrlOwner[6] <= 0;
            end
                
            default: begin
                // Don't do anything
                CtrlRE <= 0;
                CtrlWE <= 0;
                CtrlAddr <= 0;
                CtrlBE <= 2'b11;
                CtrlWriteData <= 0;
            end
        endcase
    end
    
    // Find the next connection to process
    // Search takes place in the background to reduce cycles in 
    // main control state machine
    always @ (posedge User_CLK) begin
        if (CtrlState==CHECK_STATE && CtrlSubState==0) begin
            NextCtrlConnection <= CtrlConnection;
            NextCtrlConnectionMask <= CtrlConnectionMask;
            NextCtrlConnectionValid <= 0;
        end else if (NextCtrlConnectionValid==0) begin
            if (NextCtrlConnectionPlus1==0 ||
                User_RegEnable[NextCtrlConnectionPlus1]==1 ||
                User_Connected[NextCtrlConnectionPlus1]==1) begin
                NextCtrlConnectionValid <= 1;
            end else begin
                NextCtrlConnectionValid <= 0;
            end
            NextCtrlConnection <= NextCtrlConnectionPlus1;
            NextCtrlConnectionMask <= {NextCtrlConnectionMask[14:0],
                                       NextCtrlConnectionMask[15]};
        end
    end
    
    // Keep shadow copy of registers
    always @ (posedge User_CLK) begin
        if (CtrlWE==1 && CtrlAddr==CONNECTION_STATE) begin
            ConnectionState[CtrlConnection] <= UserWriteData[5:0];
        end
    end

    //////////////////////////////////////////////////////////////////////////
    // Feed data from 16 connection Tx FIFOs to single intermediate FIFO
    // to improve timing
    always @ (posedge User_RST or posedge User_CLK) begin
        if (User_RST==1) begin
            TxConnection <= 0;
            TxConnectionMask <= 1;
            NextTxConnection <= 0;
            NextTxConnectionMask <= 1;
            TxMergeFIFOWriteCount <= 0;
            RegTxMergeFIFOWriteCount <= 0;
            TxMergeFIFOReadCount <= 0;
            TxMergeFIFODataOut <= 0;
            TxMergeFIFOFull <= 0;
            TxMergeFIFOEmpty <= 1;
        end else begin

            // Search for the next data connection that can be serviced
            TxFIFOEmptyTxVal <= TxFIFONextEmpty[TxConnection];
            if (TxFIFOEmptyNextTxVal==1 ||
                TxFrameLengthNotZeroNextTxDataVal==0 ||
                TxBurstLengthNotZeroNextTxDataVal==0) begin
                NextTxConnection <= NextTxConnection + 1;
                NextTxConnectionMask <= {NextTxConnectionMask[14:0],
                                         NextTxConnectionMask[15]};
            end else if (TxFIFOEmptyTxVal==1 ||
                         TxFrameLengthNotZeroTxDataVal==0 ||
                         TxBurstLengthNotZeroTxDataVal==0) begin
                TxConnection <= NextTxConnection;
                TxConnectionMask <= NextTxConnectionMask;
                TxFIFOEmptyTxVal <= TxFIFONextEmpty[NextTxConnection];
            end
            
            // Manage FIFO write/read counts
            if (TxMergeFIFOWE==1) begin
                TxMergeFIFOWriteCount <= TxMergeFIFOWriteCount+1;
            end
            RegTxMergeFIFOWriteCount <= TxMergeFIFOWriteCount;
            if (TxMergeFIFORE==1) begin
                TxMergeFIFOReadCount <= TxMergeFIFOReadCount+1;
            end
            TxMergeFIFODataOut <= TxMergeFIFO[TxMergeFIFOReadAddr];
            
            // Manage FIFO full/empty flags
            if (TxMergeFIFOCount>11) begin
                TxMergeFIFOFull <= 1;
            end else begin
                TxMergeFIFOFull <= 0;
            end
            if (RegTxMergeFIFOWriteCount==TxMergeFIFOReadAddr) begin
                TxMergeFIFOEmpty <= 1;
            end else begin
                TxMergeFIFOEmpty <= 0;
            end
        end
    end
    
    // Main FIFO storage
    always @ (posedge User_CLK) begin
        TxMergeFIFO[TxMergeFIFOWriteCount] <= {TxConnection, TxMergeFIFODataIn};
    end
    assign TxMergeFIFODataIn[17] = TxFIFODataOutTxDataVal[17];
    assign TxMergeFIFODataIn[16] = ConnectionStateTxDataVal[CONN_TCP_BIT]==0 && 
                                   TxFrameLengthTxDataVal==1 ? 0 :
                                                         TxFIFODataOutTxDataVal[16];
    assign TxMergeFIFODataIn[15:0] = TxFIFODataOutTxDataVal[15:0];

    assign TxMergeFIFOReadAddr = TxMergeFIFORE==0 ? TxMergeFIFOReadCount :
                                                    TxMergeFIFOReadCount+1;
    assign TxMergeFIFOCount = TxMergeFIFOWriteCount-TxMergeFIFOReadCount;

    assign TxMergeFIFOWE = BlockTxMergeFIFO==0 && 
                           TxFIFOEmptyTxVal==0 &&
                           TxMergeFIFOFull==0 &&
                           TxBurstLengthNotZeroTxDataVal==1 &&
                           TxFrameLengthNotZeroTxDataVal==1 ? 1 : 0;
    assign TxMergeFIFORE = DataWEReq;
    assign TxMergeFIFOConn = TxMergeFIFODataOut[21:18];
    assign TxMergeFIFODataOutVal = TxMergeFIFODataOut[17:0];

    // Feed data between GigExpedite and Rx/Tx FIFOs
    always @ (posedge User_RST or posedge User_CLK) begin
        if (User_RST==1) begin
            DataConnection <= 0;
            NextDataConnection <= 0;
            RxFIFOFullDataVal <= 0;
            ArbCounter <= 0;
        end else begin
            // Move on when not able to continue transfer
            RxFIFOFullDataVal <= RxFIFOFull[DataConnection[3:0]];
            ArbCounter <= ArbCounter + 1;
            if (NextTransferOK==0) begin
                if (NextDataConnection[4]==0) begin
                    NextDataConnection <= 5'b10000;
                end else begin
                    NextDataConnection <= NextDataConnection + 1;
                end
            end else if ((DataConnection[4]==0 && (WriteOK==0 || ArbCounter==8'hff)) ||
                         (DataConnection[4]==1 && ReadOK==0)) begin
                if (NextDataConnection[4]==0) begin
                    NextDataConnection <= 5'b10000;
                end else begin
                    NextDataConnection <= NextDataConnection + 1;
                end
                DataConnection <= NextDataConnection;
                ArbCounter <= 0;
                RxFIFOFullDataVal <= RxFIFOFull[NextDataConnection[3:0]];
            end
        end
    end

    always @ (CtrlState, NextDataConnection, User_Connected,
              ConnectionStateTxMergeDataVal,
              RxFIFOFullNextVal, RxFrameLengthNotZeroNextDataVal,
              RxBurstLengthNotZeroNextDataVal, DataConnection, 
              ConnectionStateDataVal, RxFrameLengthNotZeroVal,
              RxFIFOFullDataVal, WriteOK, ReadOK, 
              RxFrameLengthNotZeroDataVal, RxBurstLengthNotZeroDataVal,
              CtrlWE, CtrlRE, UserReadDataValid, UserValidOwner,
              RxFrameLengthOddDataVal, DataBE, RxFrameLengthDataVal,
              TxMergeFIFOEmpty, BlockDataConnection, TxMergeFIFOConn, 
              TxMergeFIFODataOutVal, BlockDataRE, BlockDataWE) begin
        // Find the next connection that's ready to transfer
        if (NextDataConnection[4]==0) begin
            if (TxMergeFIFOEmpty==0) begin
                NextTransferOK <= 1;
            end else begin
                NextTransferOK <= 0;
            end
        end else begin
            if (RxFIFOFullNextVal==0 &&
                RxFrameLengthNotZeroNextDataVal==1 &&
                RxBurstLengthNotZeroNextDataVal==1) begin
                NextTransferOK <= 1;
            end else begin
                NextTransferOK <= 0;
            end
        end
        
        // Check if a transfer is possible
        if (TxMergeFIFOEmpty==0) begin
            WriteOK <= 1;
        end else begin
            WriteOK <= 0;
        end
        if (RxFIFOFullDataVal==0 &&
            RxFrameLengthNotZeroDataVal==1 &&
            RxBurstLengthNotZeroDataVal==1) begin
            ReadOK <= 1;
        end else begin
            ReadOK <= 0;
        end
        
        // Data read or write transfer
        if (DataConnection[4]==0) begin
            // Write data to GigExpedite FIFO
            DataWEReq <= CtrlWE==0 && CtrlRE==0 && BlockDataWE==0 &&
                         !(BlockDataConnection[4]==1 && BlockDataConnection[3:0]==TxMergeFIFOConn) &&
                         WriteOK==1 ? 1 : 0;
            DataRE <= 0;
            DataBE <= TxMergeFIFODataOutVal[17:16];
            DataAddr <= DATA_FIFO + TxMergeFIFOConn;
        end else begin
            // Read data from GigExpedite FIFO
            DataWEReq <= 0;
            DataRE <= CtrlWE==0 && CtrlRE==0 && BlockDataRE==0 &&
                      !(BlockDataConnection[4]==1 && BlockDataConnection[3:0]==DataConnection[3:0]) &&
                      ReadOK==1 ? 1 : 0;
            DataBE <= RxFrameLengthDataVal==2 ? {1'b1, ~RxFrameLengthOddDataVal} :
                                                2'b11;
            DataAddr <= DATA_FIFO + DataConnection[3:0];
        end

        // Flush outgoing data when not connected
        if (ConnectionStateTxMergeDataVal[CONN_ENABLE_BIT]==1 &&
            User_Connected[TxMergeFIFOConn]==1) begin
            DataWE <= DataWEReq;
        end else begin
            DataWE <= 0;
        end
        DataWriteData <= TxMergeFIFODataOutVal[15:0];
        DataOwner <= {1'b1, 1'b0, DataBE, DataConnection[3:0]};
    end
    
    //////////////////////////////////////////////////////////////////////////
    // Data transfer state RAMs
    // Contains current frame lengths and positions in bursts for each
    // connection
    always @ * begin
        RxFrameLengthWE <= 0;
        RxFrameLengthAddr <= CtrlConnection;
        RxFrameLengthData[15:1] <= UserReadData[15:1] + UserReadData[0] +
                                   (ConnectionStateVal[CONN_TCP_BIT]==1 ? 0 : 3);
        RxFrameLengthData[0] <= 0;
        RxFrameLengthNotZeroData <= UserReadData==0 &&
                                    ConnectionStateVal[CONN_TCP_BIT]==1 ? 0 : 1;
        RxFrameLengthOddData <= UserReadData[0];
        RxBurstLengthWE <= 0;
        RxBurstLengthAddr <= CtrlConnection;
        RxBurstLengthData <= USER_BURST_LENGTH_WORDS;
        RxBurstLengthNotZeroData <= 1;
        TxFrameLengthWE <= 0;
        TxFrameLengthAddr <= CtrlConnection;
        TxFrameLengthData <= TxFIFODataOutTxDataVal[17:16]!=2'b11 ? 
                                        TxFrameLengthTxDataVal - 1 :
                                        TxFrameLengthTxDataVal - 2;
        TxFrameLengthNotZeroData <= TxFrameLengthTxDataVal==1 ||
                                    (TxFrameLengthTxDataVal==2 &&
                                     TxFIFODataOutTxDataVal[17:16]==2'b11) ? 0 : 1;
        TxBurstLengthWE <= 0;
        TxBurstLengthAddr <= CtrlConnection;
        TxBurstLengthData <= TxBurstLengthTxDataVal - 2;
        BlockDataRE <= 0;
        BlockDataWE <= 0;

        // Initialise variables on connection open or close
        if (!(UserReadDataValid==1 && UserValidOwner[6]==1) &&
            CtrlState==STATE_CHANGE && CtrlSubState[3:0]==0) begin
            BlockDataRE <= 1;
            BlockDataWE <= 1;
            RxFrameLengthWE <= 1;
            RxFrameLengthData <= 0;
            RxFrameLengthNotZeroData <= 0;
            RxBurstLengthWE <= 1;
            RxBurstLengthData <= 0;
            RxBurstLengthNotZeroData <= 0;
            TxFrameLengthWE <= 1;
            TxFrameLengthAddr <= CtrlConnection;
            TxFrameLengthData <= 0;
            TxFrameLengthNotZeroData <= CurrentState[CONN_TCP_BIT];
            TxBurstLengthWE <= 1;
            TxBurstLengthData <= 0;
        end
        
        // Update receive frame length counter
        if (UserReadDataValid==1 && UserValidOwner[6]==1) begin
            // New frame length fom GigExpedite
            RxFrameLengthWE <= 1;
            BlockDataRE <= 1;
        end
        if (DataRE==1) begin
            // Decrement frame length as data is read
            RxFrameLengthWE <= 1;
            RxFrameLengthAddr <= DataConnection[3:0];
            RxFrameLengthData <= RxFrameLengthDataVal-2;
            RxFrameLengthNotZeroData <= RxFrameLengthDataVal==2 ? 0 : 1;
            RxFrameLengthOddData <= RxFrameLengthOddDataVal;
        end

        // Update read burst counter
        if ((CtrlState==READ_LENGTH && RxFrameLengthNotZeroVal==1) ||
            (UserReadDataValid==1 && UserValidOwner[6]==1)) begin
            // New burst available
            RxBurstLengthWE <= 1;
            BlockDataRE <= 1;
        end
        if (DataRE==1) begin
            // Decrement burst length as data is read
            RxBurstLengthWE <= 1;
            RxBurstLengthAddr <= DataConnection[3:0];
            RxBurstLengthData <= RxBurstLengthDataVal - 1;
            RxBurstLengthNotZeroData <= RxBurstLengthDataVal==1 ? 0 : 1;
        end

        // Update transmit frame length counter
        if (CtrlWE==1 && CtrlAddr==FRAME_LENGTH) begin
            // New UDP datagram length from user
            TxFrameLengthWE <= ~ConnectionStateVal[CONN_TCP_BIT];
            TxFrameLengthData <= FrameLength;
            TxFrameLengthNotZeroData <= FrameLength!=0 ? 1 : 0;
        end else if (TxMergeFIFOWE==1) begin
            // Decrement frame length as its written to GigExpedite
            TxFrameLengthWE <= ~ConnectionStateTxDataVal[CONN_TCP_BIT];
            TxFrameLengthAddr <= TxConnection;
        end

        // Update transmit burst length counter
        if (CtrlState==PROCESS_INTERRUPTS &&
            InterruptState[IE_OUTGOING_NOT_FULL_BIT]==1) begin
            // New space available in the GigExpedite FIFO
            TxBurstLengthWE <= 1;
            TxBurstLengthData <= 16'hff00;
        end else if (TxMergeFIFOWE==1) begin
            // Decrement space available as its filled
            TxBurstLengthWE <= 1;
            TxBurstLengthAddr <= TxConnection;
        end
    end
    
    // Perform writes to state RAMs
    assign TxBurstLengthNotZeroData = TxBurstLengthData!=0 ? 1 : 0;
    always @ (posedge User_CLK) begin
        if (RxFrameLengthWE==1) begin
            RxFrameLength[RxFrameLengthAddr] <= RxFrameLengthData;
            RxFrameLengthOdd[RxFrameLengthAddr] <= RxFrameLengthOddData;
            RxFrameLengthNotZero[RxFrameLengthAddr] <= RxFrameLengthNotZeroData;
        end
        if (RxBurstLengthWE==1) begin
            RxBurstLength[RxBurstLengthAddr] <= RxBurstLengthData;
            RxBurstLengthNotZero[RxBurstLengthAddr] <= RxBurstLengthNotZeroData;
        end
        if (TxFrameLengthWE==1) begin
            TxFrameLength[TxFrameLengthAddr] <= TxFrameLengthData;
            TxFrameLengthNotZero[TxFrameLengthAddr] <=
                                        TxFrameLengthNotZeroData;
        end
        if (TxBurstLengthWE==1) begin
            TxBurstLength[TxBurstLengthAddr] <= TxBurstLengthData;
            TxBurstLengthNotZero[TxBurstLengthAddr] <=
                                        TxBurstLengthNotZeroData;
        end
        if (TxMergeFIFOWE==1) begin
            TxLastByte[TxConnection] <= TxFIFODataOutTxDataVal[7:0];
            TxLastByteValid[TxConnection] <= TxFIFODataOutTxDataVal[16] &
                                                    ~TxMergeFIFODataIn[16];
        end
    end

    // Update buffer empty flags
    always @ (posedge User_RST or posedge User_CLK) begin
        if (User_RST==1) begin
            User_TxBufferEmpty <= 16'hFFFF;
            TxBufferEmpty <= 16'hFFFF;
        end else begin
            if (CtrlState==PROCESS_INTERRUPTS &&
                InterruptState[IE_OUTGOING_NOT_FULL_BIT]==0 &&
                InterruptState[IE_INCOMING_BIT]==0 &&
                InterruptState[IE_OUTGOING_EMPTY_BIT]==1) begin
                TxBufferEmpty[CtrlConnection] <= 1;
            end
            if (DataWE==1) begin
                TxBufferEmpty[DataConnection[3:0]] <= 0;
            end
            User_TxBufferEmpty <= TxBufferEmpty & TxFIFOEmpty;
        end
    end

    //////////////////////////////////////////////////////////////////////////
    // Mask interrupts during latency periods
    always @ (posedge User_RST or posedge User_CLK) begin
        if (User_RST==1) begin
            for (i=0; i<16; i=i+1) begin
                InterruptLatency[i] <= 0;
                InterruptLatencyNotZero[i] <= 0;
            end
            LastRxFrameLengthWE <= 0;
            LastRxFrameLengthData <= 0;
            LastRxFrameLengthAddr <= 0;
        end else begin
            for (i=0; i<16; i=i+1) begin
                if (InterruptLatency[i]!=0) begin
                    InterruptLatency[i] <= InterruptLatency[i] - 1;
                    InterruptLatencyNotZero[i] <= 1;
                end else begin
                    InterruptLatencyNotZero[i] <= 0;
                end
            end
            LastRxFrameLengthWE <= RxFrameLengthWE;
            LastRxFrameLengthData <= RxFrameLengthData;
            LastRxFrameLengthAddr <= RxFrameLengthAddr;
            if ((LastRxFrameLengthWE==1 && LastRxFrameLengthData==0) ||
                (CtrlWE==1 && CtrlAddr==CONNECTION_STATE)) begin
                InterruptLatency[LastRxFrameLengthAddr] <= 30;
                InterruptLatencyNotZero[LastRxFrameLengthAddr] <= 1;
            end
        end
    end

    //////////////////////////////////////////////////////////////////////////
    // Instantiate FIFOs to/from user code
    genvar g;
    generate
        for (g=0; g<16; g=g+1) begin:
            RxFIFOLoop
                ZestET1_RxFIFO RxFIFOInst (
                    .CLK(User_CLK),
                    .RST(FIFORST[g]),
                    .DataIn(RxFIFODataIn),
                    .WE(RxFIFOWE[g]),
                    .DataOut(User_RxData[g]),
                    .RE(User_RxEnable[g]),
                    .Empty(RxFIFOEmpty[g]),
                    .Full(RxFIFOFull[g])
                );
        end
    endgenerate
    assign FIFORST = User_RST ? 16'hFFFF : ~User_Connected;
    assign RxFIFODataIn = {UserValidOwner[5:4], UserReadData};
    assign User_RxEnable = ~RxFIFOEmpty & ~User_RxBusy;
    always @ * begin
        RxFIFOWE <= 0;
        RxFIFOWE[UserValidOwner[3:0]] <= UserReadDataValid & UserValidOwner[7];
    end

    generate
        for (g=0; g<16; g=g+1) begin:
            TxFIFOLoop
                ZestET1_TxFIFO TxFIFOInst (
                    .CLK(User_CLK),
                    .RST(FIFORST[g]),
                    .DataIn(User_TxData[g]),
                    .WE(User_TxEnable[g]),
                    .DataOut(TxFIFODataOut[g]),
                    .RE(TxFIFORE[g]),
                    .Empty(TxFIFOEmpty[g]),
                    .NextEmpty(TxFIFONextEmpty[g]),
                    .Full(User_TxBusy[g])
                );
        end
    endgenerate        
    assign TxFIFOREData = TxMergeFIFOWE==1 ? TxConnectionMask : 0;
    assign TxFIFORECtrl = CtrlState==WRITE_LENGTH &&
                          TxMergeFIFOEmpty==1 &&
                          CtrlSubState[2:1]==2'b01 &&
                          TxFIFOEmptyVal==0 ? CtrlConnectionMask : 0;
    assign TxFIFORE = TxFIFOREData | TxFIFORECtrl;

    //////////////////////////////////////////////////////////////////////////
    // Ethernet interface physical layer
    ZestET1_Ethernet #(.CLOCK_RATE(CLOCK_RATE)) EthernetInst (
        .User_CLK(User_CLK),
        .User_RST(User_RST),

        // User interface
        .User_WE(UserWE),
        .User_RE(UserRE),
        .User_Addr(UserAddr),
        .User_Owner(UserOwner),
        .User_WriteData(UserWriteData),
        .User_BE(UserBE),
        .User_ReadData(UserReadData),
        .User_ReadDataValid(UserReadDataValid),
        .User_ValidOwner(UserValidOwner),
        .User_Interrupt(UserInterrupt),
        
        // Interface to GigExpedite
        .Eth_Clk(Eth_Clk),
        .Eth_CSn(Eth_CSn),
        .Eth_WEn(Eth_WEn),
        .Eth_A(Eth_A),
        .Eth_D(Eth_D),
        .Eth_BE(Eth_BE),
        .Eth_Intn(Eth_Intn)
    );

endmodule


/* ZestET1 Ethernet interface physical layer
   File name: ZestET1_Ethernet.v
   Version: 1.50
   Date: 25/11/2013

   Copyright (C) 2013 Orange Tree Technologies Ltd. All rights reserved.
   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and
   modify this logic core in any form including but not limited to Verilog source code or
   EDIF netlist in FPGA designs that target the ZestET1.
   Orange Tree Technologies prohibits the use of this logic core or any modification of
   it in any form including but not limited to Verilog source code or EDIF netlist in
   FPGA or ASIC designs that target any other hardware unless the purchaser of the
   ZestET1 has purchased the appropriate licence from Orange Tree Technologies.
   Contact Orange Tree Technologies if you want to purchase such a licence.

  *****************************************************************************************
  **
  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
  **              provided to you "as is". Orange Tree Technologies and its licensors 
  **              make and you receive no warranties or conditions, express, implied, 
  **              statutory or otherwise, and Orange Tree Technologies specifically 
  **              disclaims any implied warranties of merchantability, non-infringement,
  **              or fitness for a particular purpose. Orange Tree Technologies does not
  **              warrant that the functions contained in these designs will meet your 
  **              requirements, or that the operation of these designs will be 
  **              uninterrupted or error free, or that defects in the Designs will be 
  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
  **              make any representations regarding use or the results of the use of the 
  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
  **
  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
  **              or its licensors be liable for any loss of data, lost profits, cost or 
  **              procurement of substitute goods or services, or for any special, 
  **              incidental, consequential, or indirect damages arising from the use or 
  **              operation of the designs or accompanying documentation, however caused 
  **              and on any theory of liability. This limitation will apply even if 
  **              Orange Tree Technologies has been advised of the possibility of such 
  **              damage. This limitation shall apply notwithstanding the failure of the 
  **              essential purpose of any limited remedies herein.
  **
  *****************************************************************************************
*/


`timescale 1ns / 1ps

module ZestET1_Ethernet
(
    input User_CLK,
    input User_RST,

    // User interface
    input User_WE,
    input User_RE,
    input [4:0] User_Addr,
    input [15:0] User_WriteData,
    input [7:0] User_Owner,
    input [1:0] User_BE,
    output reg [15:0] User_ReadData,
    output reg User_ReadDataValid,
    output reg [7:0] User_ValidOwner,
    output reg User_Interrupt,
    
    // Interface to GigExpedite
    output Eth_Clk,
    output Eth_CSn,
    output Eth_WEn,
    output [4:0] Eth_A,
    inout [15:0] Eth_D,
    output [1:0] Eth_BE,
    input Eth_Intn
);

    parameter CLOCK_RATE = 125000000;

    // Declare signals
    reg [15:0] UserDataDelay3;
    reg [15:0] UserDataDelay2;
    reg [15:0] UserDataDelay1;
    reg UserWEDelay3;
    reg UserWEDelay2;
    reg UserWEDelay1;

    reg [4:0] RegUser_Addr;
    reg RegUser_CS;
    reg RegUser_WE;
    reg RegUser_RE;
    reg [1:0] RegUser_BE;
    
    wire [15:0] Eth_DVal;
    wire [15:0] Eth_TSVal;
    wire [15:0] Eth_DTSVal;

    wire Eth_CSnVal;
    wire Eth_WEnVal;
    wire [4:0] Eth_AVal;
    wire [1:0] Eth_BEVal;

    wire nUser_CLK;
    wire [15:0] RegEth_DP;
    wire [15:0] RegEth_DN;
    wire [15:0] User_ReadDataP;
    wire [15:0] User_ReadDataN;

    reg [4:0] ReadDelay;
    reg [7:0] Owner[0:5];

    genvar g;

    // Output clock with DDR FF to give predictable timing
    assign nUser_CLK = ~User_CLK;
    ODDR2 #(
      .DDR_ALIGNMENT("NONE"),
      .INIT(1'b0),
      .SRTYPE("SYNC")
    ) ClockOutInst (
        .Q(Eth_Clk),
        .C0(User_CLK),
        .C1(nUser_CLK),
        .CE(1'b1),
        .D0(1'b0),
        .D1(1'b1),
        .R(1'b0),
        .S(1'b0)
    );

    // Data outputs
    // This makes the data/control change on the falling edge of the clock
    // at the GigExpedite to ensure setup/hold times
    always @ (posedge User_CLK) begin
        UserDataDelay3 <= UserDataDelay2;
        UserDataDelay2 <= UserDataDelay1;
        UserDataDelay1 <= User_WriteData;
        UserWEDelay3 <= UserWEDelay2;
        UserWEDelay2 <= UserWEDelay1;
        UserWEDelay1 <= ~User_WE;
    end

    generate
        for (g=0; g < 16; g=g+1) begin: DataOutCopy
            (* IOB = "FORCE" *) FDCE #(
                .INIT(1'b0)
            ) DataOutCopyInst (
                .Q(Eth_DVal[g]),
                .C(User_CLK),
                .CE(1'b1),
                .D(UserDataDelay3[g]),
                .CLR(1'b0)
            );
        end
    endgenerate

    generate
        for (g=0; g < 16; g=g+1) begin: DataTSCopy
            (* IOB = "FORCE" *) FDCE #(
                .INIT(1'b0)
            ) DataTSCopyInst (
                .Q(Eth_TSVal[g]),
                .C(User_CLK),
                .CE(1'b1),
                .D(UserWEDelay3),
                .CLR(1'b0)
            );
        end
    endgenerate

    assign Eth_DTSVal[0] = Eth_TSVal[0]==0 ? Eth_DVal[0] : 1'bZ;
    assign Eth_DTSVal[1] = Eth_TSVal[1]==0 ? Eth_DVal[1] : 1'bZ;
    assign Eth_DTSVal[2] = Eth_TSVal[2]==0 ? Eth_DVal[2] : 1'bZ;
    assign Eth_DTSVal[3] = Eth_TSVal[3]==0 ? Eth_DVal[3] : 1'bZ;
    assign Eth_DTSVal[4] = Eth_TSVal[4]==0 ? Eth_DVal[4] : 1'bZ;
    assign Eth_DTSVal[5] = Eth_TSVal[5]==0 ? Eth_DVal[5] : 1'bZ;
    assign Eth_DTSVal[6] = Eth_TSVal[6]==0 ? Eth_DVal[6] : 1'bZ;
    assign Eth_DTSVal[7] = Eth_TSVal[7]==0 ? Eth_DVal[7] : 1'bZ;
    assign Eth_DTSVal[8] = Eth_TSVal[8]==0 ? Eth_DVal[8] : 1'bZ;
    assign Eth_DTSVal[9] = Eth_TSVal[9]==0 ? Eth_DVal[9] : 1'bZ;
    assign Eth_DTSVal[10] = Eth_TSVal[10]==0 ? Eth_DVal[10] : 1'bZ;
    assign Eth_DTSVal[11] = Eth_TSVal[11]==0 ? Eth_DVal[11] : 1'bZ;
    assign Eth_DTSVal[12] = Eth_TSVal[12]==0 ? Eth_DVal[12] : 1'bZ;
    assign Eth_DTSVal[13] = Eth_TSVal[13]==0 ? Eth_DVal[13] : 1'bZ;
    assign Eth_DTSVal[14] = Eth_TSVal[14]==0 ? Eth_DVal[14] : 1'bZ;
    assign Eth_DTSVal[15] = Eth_TSVal[15]==0 ? Eth_DVal[15] : 1'bZ;
    assign Eth_D = Eth_DTSVal;
    
    // Address and control outputs
    // This makes the data/control change on the falling edge of the clock
    // at the GigExpedite to ensure setup/hold times
    always @ (posedge User_CLK) begin
        RegUser_CS <= ~(User_WE | User_RE);
        RegUser_BE <= ~User_BE;
        RegUser_WE <= ~User_WE;
        RegUser_RE <= ~User_RE;
        RegUser_Addr <= User_Addr;
    end
    
    (* IOB = "FORCE" *) FDCE #(
        .INIT(1'b1)
    ) CSnInst (
        .Q(Eth_CSnVal),
        .C(User_CLK),
        .CE(1'b1),
        .D(RegUser_CS),
        .CLR(1'b0)
    );

    (* IOB = "FORCE" *) FDCE #(
        .INIT(1'b1)
    ) WEnInst (
        .Q(Eth_WEnVal),
        .C(User_CLK),
        .CE(1'b1),
        .D(RegUser_WE),
        .CLR(1'b0)
    );

    generate
        for (g=0; g < 5; g=g+1) begin: AddrCopy
            (* IOB = "FORCE" *) FDCE #(
                .INIT(1'b0)
            ) AddrCopyInst (
                .Q(Eth_AVal[g]),
                .C(User_CLK),
                .CE(1'b1),
                .D(RegUser_Addr[g]),
                .CLR(1'b0)
            );
        end
    endgenerate

    generate
        for (g=0; g < 2; g=g+1) begin: BECopy
            (* IOB = "FORCE" *) FDCE #(
                .INIT(1'b0)
            ) BECopyInst (
                .Q(Eth_BEVal[g]),
                .C(User_CLK),
                .CE(1'b1),
                .D(RegUser_BE[g]),
                .CLR(1'b0)
            );
        end
    endgenerate

    assign Eth_CSn = Eth_CSnVal;
    assign Eth_WEn = Eth_WEnVal;
    assign Eth_A = Eth_AVal;
    assign Eth_BE = Eth_BEVal;
    
    // Read path
    // Use DDR FF to get rising and falling edge registers
    generate
        for (g=0; g < 16; g=g+1) begin: DataInCopy
            IDDR2 #(
              .DDR_ALIGNMENT("NONE"),
              .INIT_Q0(1'b0),
              .INIT_Q1(1'b0),
              .SRTYPE("SYNC")
            ) DataInCopyInst (
                .Q0(RegEth_DP[g]),
                .Q1(RegEth_DN[g]),
                .C0(User_CLK),
                .C1(nUser_CLK),
                .CE(1'b1),
                .D(Eth_D[g]),
                .R(1'b0),
                .S(1'b0)
            );
        end
    endgenerate

    // Re-register to user clock
    generate
        for (g=0; g < 16; g=g+1) begin: DataInPCopy
            FD DataInPCopyInst (
                .Q(User_ReadDataP[g]),
                .C(User_CLK),
                .D(RegEth_DP[g])
            );
        end
    endgenerate
    generate
        for (g=0; g < 16; g=g+1) begin: DataInNCopy
            FD DataInNCopyInst (
                .Q(User_ReadDataN[g]),
                .C(User_CLK),
                .D(RegEth_DN[g])
            );
        end
    endgenerate

    // Generate delayed valid and owner signals to match read data
    always @ (posedge User_CLK) begin
        ReadDelay <= {ReadDelay[3:0], ~RegUser_RE};
        Owner[5] <= Owner[4];
        Owner[4] <= Owner[3];
        Owner[3] <= Owner[2];
        Owner[2] <= Owner[1];
        Owner[1] <= Owner[0];
        Owner[0] <= User_Owner;
    end

    // Select read data path on clock rate
    // Since the delays to/from the GigExpedite are fixed, the read sample
    // clock edge will change with the clock rate changing
    always @ (User_ReadDataN, User_ReadDataP, ReadDelay) begin
        if (CLOCK_RATE<40000000) begin
            User_ReadData <= User_ReadDataP;
            User_ReadDataValid <= ReadDelay[3];
            User_ValidOwner <= Owner[4];
        end else if (CLOCK_RATE>=40000000 && CLOCK_RATE<80000000) begin
            User_ReadData <= User_ReadDataN;
            User_ReadDataValid <= ReadDelay[3];
            User_ValidOwner <= Owner[4];
        end else begin
            User_ReadData <= User_ReadDataP;
            User_ReadDataValid <= ReadDelay[4];
            User_ValidOwner <= Owner[5];
        end
    end
		
    // Register interrupt line from GigExpedite
    always @ (posedge User_CLK) begin
        User_Interrupt <= ~Eth_Intn;
    end

endmodule

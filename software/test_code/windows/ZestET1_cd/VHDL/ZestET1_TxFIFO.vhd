-- ZestET1 Ethernet Transmit FIFO
--   File name: ZestET1_TxFIFO.vhd
--   Version: 1.00
--   Date: 10/3/2011
--
--   FIFO to bridge from 8 bit user interface to 16 bit GigExpedite interface for data
--   sent to the network.  This will write either one or two bytes of data to the 
--   GigExpedite.
--
--   Copyright (C) 2011 Orange Tree Technologies Ltd. All rights reserved.
--   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and
--   modify this logic core in any form including but not limited to VHDL source code or
--   EDIF netlist in FPGA designs that target the ZestET1.
--   Orange Tree Technologies prohibits the use of this logic core or any modification of
--   it in any form including but not limited to VHDL source code or EDIF netlist in
--   FPGA or ASIC designs that target any other hardware unless the purchaser of the
--   ZestET1 has purchased the appropriate licence from Orange Tree Technologies.
--   Contact Orange Tree Technologies if you want to purchase such a licence.
--
--  *****************************************************************************************
--  **
--  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
--  **              provided to you "as is". Orange Tree Technologies and its licensors 
--  **              make and you receive no warranties or conditions, express, implied, 
--  **              statutory or otherwise, and Orange Tree Technologies specifically 
--  **              disclaims any implied warranties of merchantability, non-infringement,
--  **              or fitness for a particular purpose. Orange Tree Technologies does not
--  **              warrant that the functions contained in these designs will meet your 
--  **              requirements, or that the operation of these designs will be 
--  **              uninterrupted or error free, or that defects in the Designs will be 
--  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
--  **              make any representations regarding use or the results of the use of the 
--  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
--  **
--  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
--  **              or its licensors be liable for any loss of data, lost profits, cost or 
--  **              procurement of substitute goods or services, or for any special, 
--  **              incidental, consequential, or indirect damages arising from the use or 
--  **              operation of the designs or accompanying documentation, however caused 
--  **              and on any theory of liability. This limitation will apply even if 
--  **              Orange Tree Technologies has been advised of the possibility of such 
--  **              damage. This limitation shall apply notwithstanding the failure of the 
--  **              essential purpose of any limited remedies herein.
--  **
--  *****************************************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;

library UNISIM;
use UNISIM.VComponents.all;

entity ZestET1_TxFIFO is
    port (
        RST : in std_logic;
        CLK : in std_logic;
        WE : in std_logic;
        DataIn : in std_logic_vector(7 downto 0);
        RE : in std_logic;
        DataOut : out std_logic_vector(17 downto 0);
        Empty : out std_logic;
        NextEmpty : out std_logic;
        Full : out std_logic
    );
end ZestET1_TxFIFO;

architecture arch of ZestET1_TxFIFO is

    -- Declare constants
    constant LOG2_FIFO_DEPTH : integer := 4;
    constant FIFO_DEPTH : integer := 2**LOG2_FIFO_DEPTH;
    constant ALMOST_FULL : std_logic_vector(LOG2_FIFO_DEPTH-1 downto 0) :=
                            conv_std_logic_vector(FIFO_DEPTH-4, LOG2_FIFO_DEPTH);

    -- Declare signals
    attribute ram_style : string;
    type ARRAY_TYPE is array(0 to FIFO_DEPTH-1) of std_logic_vector(17 downto 0);
    signal StorageArray : ARRAY_TYPE;
    attribute ram_style of StorageArray : signal is "distributed";
    signal StorageArrayVal : std_logic_vector(17 downto 0);

    signal EmptyVal : std_logic;
    signal NextEmptyVal : std_logic;
    signal DataInWE : std_logic;
    signal DataInVal : std_logic_vector(17 downto 0);
    signal RegDataIn : std_logic_vector(7 downto 0);
    signal RegDataInValid : std_logic;
    signal WriteCount : std_logic_vector(LOG2_FIFO_DEPTH-1 downto 0);
    signal RegWriteCount : std_logic_vector(LOG2_FIFO_DEPTH-1 downto 0);
    signal ReadCount : std_logic_vector(LOG2_FIFO_DEPTH-1 downto 0);
    signal ReadCountPlus1 : std_logic_vector(LOG2_FIFO_DEPTH-1 downto 0);
    signal NextReadCountPlus1 : std_logic_vector(LOG2_FIFO_DEPTH-1 downto 0);
    signal NextWriteCount : std_logic_vector(LOG2_FIFO_DEPTH-1 downto 0);
    signal SelReadCount : std_logic_vector(LOG2_FIFO_DEPTH-1 downto 0);
    signal Count : std_logic_vector(LOG2_FIFO_DEPTH-1 downto 0);

begin
    
    -- Storage array - inferred as a distributed ram
    process (CLK)
    begin
        if (CLK'event and CLK='1') then
            if (DataInWE='1') then
                StorageArray(conv_integer(WriteCount)) <= DataInVal;
            end if;
            DataOut <= StorageArrayVal;
        end if;
    end process;
    StorageArrayVal <= StorageArray(conv_integer(SelReadCount));
    DataInWE <= RegDataInValid;
    DataInVal(17) <= RegDataInValid;
    DataInVal(16) <= WE;
    DataInVal(15 downto 8) <= RegDataIn;
    DataInVal(7 downto 0) <= DataIn;
    SelReadCount <= ReadCount when RE='0' else ReadCountPlus1;
    
    -- Manage read and write counters
    process (RST, CLK)
    begin
        if (CLK'event and CLK='1') then
            if (RST='1') then
                RegDataIn <= (others=>'0');
                RegDataInValid <= '0';
                WriteCount <= (others=>'0');
                RegWriteCount <= (others=>'0');
                ReadCount <= (others=>'0');
                ReadCountPlus1 <= conv_std_logic_vector(1, LOG2_FIFO_DEPTH);
                EmptyVal <= '1';
                Full <= '0';
            else
                if (DataInWE='1') then
                    RegDataInValid <= '0';
                elsif (WE='1') then
                    RegDataIn <= DataIn;
                    RegDataInValid <= '1';
                end if;
                if (DataInWE='1') then
                    WriteCount <= NextWriteCount;
                end if;
                RegWriteCount <= WriteCount;
                
                if (RE='1') then
                    ReadCount <= ReadCountPlus1;
                    ReadCountPlus1 <= NextReadCountPlus1;
                end if;

                EmptyVal <= NextEmptyVal;
                if (Count>=ALMOST_FULL) then
                    Full <= '1';
                else
                    Full <= '0';
                end if;
            end if;
        end if;
    end process;

    NextReadCountPlus1 <= ReadCountPlus1 + 1;
    NextWriteCount <= WriteCount + 1;

    Count <= WriteCount-ReadCount;
    NextEmptyVal <= '1' when RegWriteCount=SelReadCount else '0';
    NextEmpty <= NextEmptyVal;
    Empty <= EmptyVal;
    
end arch;


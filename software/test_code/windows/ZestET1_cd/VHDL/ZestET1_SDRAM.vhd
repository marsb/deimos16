-- ZestET1 DDR SDRAM
--   File name: ZestET1_SDRAM.vhd
--   Version: 1.20
--   Date: 16/5/2011
--
--   DDR SDRAM controller for user FPGA on ZestET1 boards
--    
--   Copyright (C) 2011 Orange Tree Technologies Ltd. All rights reserved.
--   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and
--   modify this logic core in any form including but not limited to VHDL source code or
--   EDIF netlist in FPGA designs that target the ZestET1.
--   Orange Tree Technologies prohibits the use of this logic core or any modification of
--   it in any form including but not limited to VHDL source code or EDIF netlist in
--   FPGA or ASIC designs that target any other hardware unless the purchaser of the
--   ZestET1 has purchased the appropriate licence from Orange Tree Technologies.
--   Contact Orange Tree Technologies if you want to purchase such a licence.
--
--  *****************************************************************************************
--  **
--  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
--  **              provided to you "as is". Orange Tree Technologies and its licensors 
--  **              make and you receive no warranties or conditions, express, implied, 
--  **              statutory or otherwise, and Orange Tree Technologies specifically 
--  **              disclaims any implied warranties of merchantability, non-infringement,
--  **              or fitness for a particular purpose. Orange Tree Technologies does not
--  **              warrant that the functions contained in these designs will meet your 
--  **              requirements, or that the operation of these designs will be 
--  **              uninterrupted or error free, or that defects in the Designs will be 
--  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
--  **              make any representations regarding use or the results of the use of the 
--  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
--  **
--  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
--  **              or its licensors be liable for any loss of data, lost profits, cost or 
--  **              procurement of substitute goods or services, or for any special, 
--  **              incidental, consequential, or indirect damages arising from the use or 
--  **              operation of the designs or accompanying documentation, however caused 
--  **              and on any theory of liability. This limitation will apply even if 
--  **              Orange Tree Technologies has been advised of the possibility of such 
--  **              damage. This limitation shall apply notwithstanding the failure of the 
--  **              essential purpose of any limited remedies herein.
--  **
--  *****************************************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity ZestET1_SDRAM is
    generic (
        CLOCK_RATE : integer := 166666666
    );
    port (
        -- User interface
        User_CLK : in std_logic;                        -- clock from user logic
        User_CLK90 : in std_logic;                      -- 90 degree shifted clock from user logic
        User_RST : in std_logic;                        -- reset
        User_A : in std_logic_vector(23 downto 0);      -- word address
        User_RE : in std_logic;                         -- read strobe
        User_WE : in std_logic;                         -- write strobe
        User_Owner : in std_logic_vector(7 downto 0);   -- access owner
        User_DR_Valid : out std_logic;                  -- read data valid strobe active high
        User_ValidOwner : out std_logic_vector(7 downto 0); -- read data owner
        User_BE : in std_logic_vector(3 downto 0);      -- byte enables for write
        User_DW : in std_logic_vector(31 downto 0);     -- 32-bit data bus for writing to SDRAM
        User_DR : out std_logic_vector(31 downto 0);    -- 32-bit data bus for reading from SDRAM
        User_Busy : out std_logic;                      -- SDRAM busy signal (active high)
        User_InitDone : out std_logic;                  -- SDRAM initialisation complete
        
        -- SDRAM interface
        DS_CLK_P : out std_logic;
        DS_CLK_N : out std_logic;
        DS_A : out std_logic_vector(12 downto 0);
        DS_BA : out std_logic_vector(1 downto 0);
        DS_DQ : inout std_logic_vector(15 downto 0);
        DS_DM : out std_logic_vector(1 downto 0);
        DS_DQS : inout std_logic_vector(1 downto 0);
        DS_CAS_N : out std_logic;
        DS_RAS_N : out std_logic;
        DS_CKE : out std_logic;
        DS_WE_N : out std_logic
    );
end ZestET1_SDRAM;

architecture arch of ZestET1_SDRAM is

    -- Declare constants
    -- The column size changes with the size of the device
    constant ROW_BITS : integer := 13;
    constant COLUMN_BITS : integer := 10;
    
    constant CLOCK_RATE_1000 : integer := CLOCK_RATE/1000; -- Clock divided by 1000

    constant tREF : integer := 57;           -- ms
    constant tRP : integer := 15;            -- ns
    constant tRFC : integer := 72;           -- ns
    constant tMRD : integer := 2;            -- cycles
    constant tRCD : integer := 15;           -- ns
    constant tRC : integer := 60;            -- ns
    constant tRRD : integer := 13;           -- ns (at least 1 cycle)
    constant tWR : integer := 3;             -- cycles
    constant tRAS : integer := 42;           -- ns
    constant tCAS : integer := 3;            -- cycles
    constant tWTR : integer := 1;            -- cycles

    -- NB: For simulation, it may be useful to drop the initialisation time
    constant INIT_CKE_TIME : integer := (200*CLOCK_RATE_1000)/1000;    -- 200 us
    --constant INIT_CKE_TIME : integer := (12*CLOCK_RATE_1000)/1000;    -- 12 us for simulation
    constant INIT_PRECHARGE1_TIME : integer := INIT_CKE_TIME + (1+(10*CLOCK_RATE_1000)/1000000);
    constant INIT_LOAD_EXTENDED_MODE_TIME : integer := INIT_PRECHARGE1_TIME + (1+(tRP*CLOCK_RATE_1000)/1000000);
    constant INIT_LOAD_MODE1_TIME : integer := INIT_LOAD_EXTENDED_MODE_TIME + tMRD;
    constant INIT_PRECHARGE2_TIME : integer := INIT_LOAD_MODE1_TIME + tMRD;
    constant INIT_REFRESH1_TIME : integer := INIT_PRECHARGE2_TIME + (1+(tRP*CLOCK_RATE_1000)/1000000);
    constant INIT_REFRESH2_TIME : integer := INIT_REFRESH1_TIME + (1+(tRFC*CLOCK_RATE_1000)/1000000);
    constant INIT_LOAD_MODE2_TIME : integer := INIT_REFRESH2_TIME + tRFC;
    constant INIT_LENGTH : integer := INIT_LOAD_MODE2_TIME + 200;
    
    constant REFRESH_CYCLES : integer := (tREF*CLOCK_RATE_1000)/8192;

    -- Mode word.  Select CAS latency of 2.5, burst of 2
    constant MODE1_WORD : std_logic_vector(14 downto 0) := "000000101100001";
    constant MODE2_WORD : std_logic_vector(14 downto 0) := "000000001100001";
    constant EXTENDED_MODE_WORD : std_logic_vector(14 downto 0) := "010000000000010";
    
    -- Delays following each activity
    constant ACTIVE_WAIT_CYCLES : integer := (tRCD*CLOCK_RATE_1000)/1000000;
    constant PRECHARGE_WAIT_CYCLES : integer := (tRP*CLOCK_RATE_1000)/1000000;
    constant REFRESH_WAIT_CYCLES : integer := (tRFC*CLOCK_RATE_1000)/1000000;
    constant tRAS_CYCLES : integer := (tRAS*CLOCK_RATE_1000)/1000000-1;
    constant tCAS_CYCLES : integer := tCAS;
    constant tWTR_CYCLES : integer := tWTR+1;
    constant tWR_CYCLES : integer := tWR+1;
    constant tRRD_CYCLES : integer := (tRRD*CLOCK_RATE_1000)/1000000-1;
    constant tRC_CYCLES : integer := (tRC*CLOCK_RATE_1000)/1000000-1;
    
    -- Declare signals
    signal CLK : std_logic;
    signal CLK90 : std_logic;
    signal RST : std_logic;
    
    -- Input register
    signal InputRegAddr : std_logic_vector(23 downto 0);
    signal InputRegData : std_logic_vector(31 downto 0);
    signal InputRegOwner : std_logic_vector(7 downto 0);
    signal InputRegBE : std_logic_vector(3 downto 0);
    signal InputRegRE : std_logic;
    signal InputRegWE : std_logic;
    signal InputRegFull : std_logic;
    signal InputRegPrechargeNeeded : std_logic;
    signal InputRegReadReady : std_logic;
    signal InputRegWriteReady : std_logic;
    signal InputRegRead : std_logic;

    -- Commands are in priority order
    constant PRECHARGE_BIT    : integer := 0;
    constant AUTO_REFRESH_BIT : integer := 1;
    constant ACTIVE_BIT       : integer := 2;
    constant READ_BIT         : integer := 3;
    constant WRITE_BIT        : integer := 4;
    constant LOAD_MODE_BIT    : integer := 5;
    constant PRECHARGE    : std_logic_vector(5 downto 0) := "000001";
    constant AUTO_REFRESH : std_logic_vector(5 downto 0) := "000010";
    constant ACTIVE       : std_logic_vector(5 downto 0) := "000100";
    constant READ         : std_logic_vector(5 downto 0) := "001000";
    constant WRITE        : std_logic_vector(5 downto 0) := "010000";
    constant LOAD_MODE    : std_logic_vector(5 downto 0) := "100000";
    signal NextCommand : std_logic_vector(5 downto 0);
    signal Command : std_logic_vector(5 downto 0);
    signal LastCommandWrite : std_logic;
    signal DataOut : std_logic_vector(31 downto 0);
    signal RegBankAddr : std_logic_vector(1 downto 0);
    signal InputRegBankAddr : std_logic_vector(1 downto 0);
    signal NewBankAddr : std_logic_vector(1 downto 0);
    signal LastActiveBank : std_logic_vector(1 downto 0);
    signal RegRowAddr : std_logic_vector(12 downto 0);
    signal InputRegRowAddr : std_logic_vector(12 downto 0);
    signal NewRowAddr : std_logic_vector(ROW_BITS-1 downto 0);
    signal RegColAddr : std_logic_vector(12 downto 0);
    signal InputRegColAddr : std_logic_vector(12 downto 0);
    signal PreChargeAddr : std_logic_vector(12 downto 0);
    signal RegPreChargeAddr : std_logic_vector(12 downto 0);
    signal RegData : std_logic_vector(31 downto 0);
    signal RegOwner : std_logic_vector(7 downto 0);
    signal RegBE : std_logic_vector(3 downto 0);
    signal RegModeWord : std_logic_vector(14 downto 0);

    signal OpenBanks : std_logic_vector(3 downto 0);
    signal OpenBanksInputReg : std_logic;
    type BANK_ADDR_TYPE is array(0 to 3) of std_logic_vector(ROW_BITS-1 downto 0);
    signal BankAddrs : BANK_ADDR_TYPE;
    signal NewBankMatch : std_logic;
    signal NewOpenBank : std_logic;
    
    -- Initialisation signals
    signal CKEVal : std_logic;
    signal InitCounter : std_logic_vector(15 downto 0);
    signal InitCounterCKE : std_logic;
    signal InitCounterPreCharge : std_logic;
    signal InitCounterRefresh : std_logic;
    signal InitCounterLoad1 : std_logic;
    signal InitCounterLoad2 : std_logic;
    signal InitCounterLoad3 : std_logic;
    signal User_InitDoneVal : std_logic;

    -- Read input data signals
    signal DataIn : std_logic_vector(31 downto 0);
    signal DataInValid : std_logic;

    -- Decoder function
    constant TIMER_WIDTH : integer := 13;
    function expand (Val : integer) return std_logic_vector is 
        variable Result : std_logic_vector(TIMER_WIDTH-1 downto 0);
    begin
        Result := (others=>'0');
        Result(Val) := '1';
        return Result;
    end expand;
    
    -- Timers
    signal tRCTimerZero : std_logic;
    constant ExpandtRC_CYCLES : std_logic_vector(TIMER_WIDTH-1 downto 0) := expand(tRC_CYCLES);
    type BANK_TIMERS_TYPE is array(0 to 3) of std_logic_vector(TIMER_WIDTH-1 downto 0);
    signal tRCTimer : BANK_TIMERS_TYPE;
    signal tRASTimer : BANK_TIMERS_TYPE;
    signal tRRDTimer : std_logic_vector(TIMER_WIDTH-1 downto 0);
    signal tWRTimer : std_logic_vector(TIMER_WIDTH-1 downto 0);
    signal tCASTimer : std_logic_vector(TIMER_WIDTH-1 downto 0);
    signal tWTRTimer : std_logic_vector(TIMER_WIDTH-1 downto 0);
    signal ActiveWaitTimer : std_logic_vector(TIMER_WIDTH-1 downto 0);
    signal RefreshWaitTimer : std_logic_vector(TIMER_WIDTH-1 downto 0);
    signal PrechargeWaitTimer : std_logic_vector(TIMER_WIDTH-1 downto 0);
    signal RefreshCounter : std_logic_vector(10 downto 0);
    signal RefreshNeeded : std_logic;
    signal PreChargeOK : std_logic;
    signal ActiveOK : std_logic;

    -- DDR alignment signals
    signal DS_AVal : std_logic_vector(12 downto 0);
    signal DS_BAVal : std_logic_vector(1 downto 0);
    signal DS_WE_NVal : std_logic;
    signal DS_CAS_NVal : std_logic;
    signal DS_RAS_NVal : std_logic;
    signal DS_DMVal : std_logic_vector(3 downto 0);
    signal DS_DMVal1 : std_logic_vector(3 downto 0);
    signal Tristate : std_logic;
    signal Tristate1 : std_logic;
    signal ExtendedTristate : std_logic;
    signal nTristate : std_logic;
    signal nTristate1 : std_logic;
    signal nExtendedTristate : std_logic;
    signal DataOut1 : std_logic_vector(31 downto 0);

    -- Owner FIFO
    type OWNER_FIFO_TYPE is array(0 to 15) of std_logic_vector(8 downto 0);
    signal OwnerFIFO : OWNER_FIFO_TYPE;
    attribute ram_style : string;
    attribute ram_style of OwnerFIFO : signal is "distributed";
    signal OwnerFIFODataVal : std_logic_vector(8 downto 0);
    signal OwnerFIFOWriteCount : std_logic_vector(3 downto 0);
    signal OwnerFIFOReadCount : std_logic_vector(3 downto 0);
    
begin
		
    -- Connect clocks and reset
    RST <= User_RST;
    CLK <= User_CLK;
    CLK90 <= User_CLK90;
    
    --------------------------------------------------------------------------
    -- If the user issues a command and we can't process it immediately,
    -- register data for when the read/write can be issued
    process (CLK) begin
        if (CLK'event and CLK='1') then
            if (InputRegFull='0' or InputRegRead='1') then
                InputRegAddr <= User_A;
                InputRegData <= User_DW;
                InputRegOwner <= User_Owner;
                InputRegBE <= User_BE;
                InputRegRE <= User_RE;
                InputRegWE <= User_WE;
            end if;
        end if;
    end process;
    User_Busy <= InputRegFull and not InputRegRead;
    process (RST, CLK) begin
        if (RST='1') then
            InputRegFull <= '0';
            InputRegPrechargeNeeded <= '0';
            InputRegReadReady <= '0';
            InputRegWriteReady <= '0';
        elsif (CLK'event and CLK='1') then
            if (User_RE='1' or User_WE='1') then
                InputRegFull <= '1';
            elsif (InputRegRead='1') then
                InputRegFull <= '0';
            end if;
            if (User_RE='1' or User_WE='1') then
                InputRegPrechargeNeeded <= NewOpenBank and not NewBankMatch;
            elsif (NextCommand(PRECHARGE_BIT)='1') then
                InputRegPrechargeNeeded <= '0';
            elsif (InputRegRead='1') then
                InputRegPrechargeNeeded <= '0';
            end if;
            if (User_RE='1' or User_WE='1') then
                InputRegReadReady <= NewOpenBank and NewBankMatch and User_RE;
                InputRegWriteReady <= NewOpenBank and NewBankMatch and User_WE;
            elsif (NextCommand(ACTIVE_BIT)='1') then
                InputRegReadReady <= InputRegFull and InputRegRE;
                InputRegWriteReady <= InputRegFull and InputRegWE;
            elsif (InputRegRead='1') then
                InputRegReadReady <= '0';
                InputRegWriteReady <= '0';
            end if;
        end if;
    end process;
    NewBankMatch <= '1' when BankAddrs(conv_integer(NewBankAddr))=NewRowAddr else '0';
    NewOpenBank <= OpenBanks(conv_integer(NewBankAddr));
    
    InputRegRead <= NextCommand(READ_BIT) or NextCommand(WRITE_BIT);
            
    -- Record the current row address of each bank
    -- This is used to check if a PRECHARGE - ACTIVE pair is needed
    process (CLK) begin
        if (CLK'event and CLK='1') then
            if (User_RE='1' or User_WE='1') then
                BankAddrs(conv_integer(NewBankAddr)) <= NewRowAddr;
            end if;
        end if;
    end process;
    
    -- Organise bits as BA:Row:Column to try to make sure as many banks as
    -- possible remain open
    InputRegBankAddr <= InputRegAddr(23 downto 22);
    NewBankAddr <= User_A(23 downto 22);
    InputRegRowAddr <= ext(InputRegAddr(21 downto COLUMN_BITS-1), InputRegRowAddr'length);
    NewRowAddr <= ext(User_A(21 downto COLUMN_BITS-1), NewRowAddr'length);

    -- On smaller devices, User_Addr(9) may form part of the row address instead
    -- but if so then they are 'don't cares' on the column address.
    -- A10 is tied low to disable auto precharge.
    InputRegColAddr <= ext(InputRegAddr(9) & '0' & InputRegAddr(8 downto 0) & '0', InputRegColAddr'length);
                    
    --------------------------------------------------------------------------
    -- Initialisation counters
    -- This ensures the startup procedure is followed
    process (RST, CLK) begin
        if (RST='1') then
            InitCounter <= (others=>'0');
            InitCounterCKE <= '0';
            InitCounterPreCharge <= '0';
            InitCounterRefresh <= '0';
            InitCounterLoad1 <= '0';
            InitCounterLoad2 <= '0';
            InitCounterLoad3 <= '0';
            User_InitDoneVal <= '0';
        elsif (CLK'event and CLK='1') then
            if (InitCounter/=X"FFFF") then
                InitCounter <= InitCounter + 1;
            end if;
            
            -- Count steps of initialisation
            if (InitCounter=INIT_CKE_TIME) then
                InitCounterCKE <= '1';
            else
                InitCounterCKE <= '0';
            end if;
            if (InitCounter=INIT_PRECHARGE1_TIME or
                InitCounter=INIT_PRECHARGE2_TIME) then
                InitCounterPreCharge <= '1';
            else
                InitCounterPreCharge <= '0';
            end if;
            if (InitCounter=INIT_REFRESH1_TIME or
                InitCounter=INIT_REFRESH2_TIME) then
                InitCounterRefresh <= '1';
            else
                InitCounterRefresh <= '0';
            end if;
            if (InitCounter=INIT_LOAD_MODE1_TIME) then
                InitCounterLoad1 <= '1';
            else
                InitCounterLoad1 <= '0';
            end if;
            if (InitCounter=INIT_LOAD_MODE2_TIME) then
                InitCounterLoad2 <= '1';
            else
                InitCounterLoad2 <= '0';
            end if;
            if (InitCounter=INIT_LOAD_EXTENDED_MODE_TIME) then
                InitCounterLoad3 <= '1';
            else
                InitCounterLoad3 <= '0';
            end if;
            if (InitCounter=INIT_LENGTH) then
                -- Initialisation complete
                User_InitDoneVal <= '1';
            end if;
        end if;
    end process;
    User_InitDone <= User_InitDoneVal;
    
    --------------------------------------------------------------------------
    -- Determine the next command to issue
    process (RefreshNeeded, OpenBanks, InputRegPrechargeNeeded, InputRegBankAddr,
             PreChargeOK, InputRegFull, OpenBanksInputReg) begin
        
        -- Check for precharge
        if ((RefreshNeeded='1' and OpenBanks/="0000") or
            (InputRegFull='1' and OpenBanksInputReg='1' and
             InputRegPrechargeNeeded='1')) then
            NextCommand(PRECHARGE_BIT) <= PreChargeOK;
        else
            NextCommand(PRECHARGE_BIT) <= '0';
        end if;

        if (RefreshNeeded='1' and OpenBanks/="0000") then
            -- Issue all banks precharge
            PreChargeAddr <= "0010000000000";
        else
            -- Issue single bank precharge
            PreChargeAddr <= (others=>'0');
        end if;
    end process;
    
    process (RefreshNeeded, OpenBanks, PrechargeWaitTimer) begin
        if (RefreshNeeded='1' and OpenBanks="0000") then
            -- Auto refresh needed
            NextCommand(AUTO_REFRESH_BIT) <= PrechargeWaitTimer(0);
        else
            NextCommand(AUTO_REFRESH_BIT) <= '0';
        end if;
    end process;
    
    process (OpenBanksInputReg, InputRegFull, ActiveOK, PrechargeWaitTimer,
             RefreshWaitTimer, RefreshNeeded) begin
        if (RefreshNeeded='0' and
            InputRegFull='1' and OpenBanksInputReg='0' and
            ActiveOK='1' and PrechargeWaitTimer(0)='1' and RefreshWaitTimer(0)='1') then
            -- Active needed to open bank
            NextCommand(ACTIVE_BIT) <= '1';
        else
            NextCommand(ACTIVE_BIT) <= '0';
        end if;
    end process;
    
    process (RefreshNeeded, InputRegReadReady, OpenBanksInputReg,
              ActiveWaitTimer, tWTRTimer) begin
        if (RefreshNeeded='0' and InputRegReadReady='1' and OpenBanksInputReg='1' and
            ActiveWaitTimer(0)='1' and tWTRTimer(0)='1') then
            -- Issue Read
            NextCommand(READ_BIT) <= '1';
        else
            NextCommand(READ_BIT) <= '0';
        end if;
    end process;
    
    process (RefreshNeeded, InputRegWriteReady, OpenBanksInputReg,
             ActiveWaitTimer, tCASTimer) begin
        if (RefreshNeeded='0' and InputRegWriteReady='1' and OpenBanksInputReg='1' and
            ActiveWaitTimer(0)='1' and tCASTimer(0)='1') then
            -- Issue Write
            NextCommand(WRITE_BIT) <= '1';
        else
            NextCommand(WRITE_BIT) <= '0';
        end if;
    end process;
    
    -- NextCommand(LOAD_MODE_BIT) <= '0';
    
    -- Keep a record of open rows
    -- There are 4 banks so 4 rows can be open simultaneously
    -- Note that the banks should not be kept open for more than 120us but 
    -- this is not a problem because the refresh forces an all-banks precharge
    -- much more frequently than this.
    process (RST, CLK) begin
        if (RST='1') then
            OpenBanks <= (others=>'0');
            OpenBanksInputReg <= '0';
        elsif (CLK'event and CLK='1') then
            if (NextCommand(PRECHARGE_BIT)='1' and PreChargeAddr(10)='1') then
                -- All banks precharge
                OpenBanks <= (others=>'0');
                OpenBanksInputReg <= '0';
            elsif (NextCommand(ACTIVE_BIT)='1' or NextCommand(PRECHARGE_BIT)='1') then
                -- Bank status changed
                OpenBanks(conv_integer(InputRegBankAddr)) <= NextCommand(ACTIVE_BIT);
                OpenBanksInputReg <= NextCommand(ACTIVE_BIT);
            elsif (User_RE='1' or User_WE='1') then
                OpenBanksInputReg <= OpenBanks(conv_integer(NewBankAddr));
            end if;
        end if;
    end process;

    --------------------------------------------------------------------------
    -- Manage control signals
    DS_CKE <= CKEVal;
    process (RST, CLK) begin
        if (RST='1') then
            Command <= (others=>'0');
            RegPreChargeAddr <= (others=>'0');
            RegRowAddr <= (others=>'0');
            RegColAddr <= (others=>'0');
            RegBankAddr <= (others=>'0');
            RegModeWord <= (others=>'0');
            RegData <= (others=>'0');
            RegOwner <= (others=>'0');
            RegBE <= (others=>'0');
            LastCommandWrite <= '0';
            CKEVal <= '0';
        elsif (CLK'event and CLK='1') then
            -- Override command with the initialisation commands
            -- Note that the user cannot issue commands during initialisation anyway
            -- as busy signal to user logic will be high
            if (InitCounterCKE='1') then
                CKEVal <= '1';
            elsif (InitCounterPreCharge='1') then
                Command <= PRECHARGE;
                RegPreChargeAddr <= "0010000000000";
            elsif (InitCounterRefresh='1') then
                Command <= AUTO_REFRESH;
                RegPreChargeAddr <= PreChargeAddr;
            elsif (InitCounterLoad1='1' or InitCounterLoad2='1' or
                         InitCounterLoad3='1') then
                Command <= LOAD_MODE;
                RegPreChargeAddr <= PreChargeAddr;
            else
                Command <= NextCommand;
                RegPreChargeAddr <= PreChargeAddr;
            end if;
            if (InitCounterLoad1='1') then
                RegModeWord <= MODE1_WORD;
            elsif (InitCounterLoad2='1') then
                RegModeWord <= MODE2_WORD;
            else
                RegModeWord <= EXTENDED_MODE_WORD;
            end if;
            if (NextCommand(ACTIVE_BIT)='1') then
                RegColAddr <= InputRegRowAddr;
            else
                RegColAddr <= InputRegColAddr;
            end if;
--            RegRowAddr <= InputRegRowAddr;
--            RegColAddr <= InputRegColAddr;
            RegBankAddr <= InputRegBankAddr;
            if (InputRegRead='1') then
                RegData <= InputRegData;
                RegBE <= InputRegBE;
            end if;
            RegOwner <= InputRegOwner;
    
            -- Generate delayed and extended versions of write command
            -- for tristate and DQS enables
            LastCommandWrite <= Command(WRITE_BIT);
        end if;
    end process;
    process (CLK) begin
        if (CLK'event and CLK='1') then
            DataOut <= RegData;
        end if;
    end process;
        
    process (Command, RegPreChargeAddr, RegModeWord, RegRowAddr, RegColAddr,
             RegBankAddr) begin
        -- Encode commands to RAS, CAS and WE signals
        if (Command(PRECHARGE_BIT)='1' or
            Command(AUTO_REFRESH_BIT)='1' or
            Command(ACTIVE_BIT)='1' or
            Command(LOAD_MODE_BIT)='1') then
            DS_RAS_NVal <= '0';
        else
            DS_RAS_NVal <= '1';
        end if;
        if (Command(AUTO_REFRESH_BIT)='1' or
            Command(READ_BIT)='1' or
            Command(WRITE_BIT)='1' or
            Command(LOAD_MODE_BIT)='1') then
            DS_CAS_NVal <= '0';
        else
            DS_CAS_NVal <= '1';
        end if;
        if (Command(PRECHARGE_BIT)='1' or
            Command(WRITE_BIT)='1' or
            Command(LOAD_MODE_BIT)='1') then
            DS_WE_NVal <= '0';
        else
            DS_WE_NVal <= '1';
        end if;

        -- Multiplex address
        if (Command(PRECHARGE_BIT)='1') then
            DS_AVal <= RegPreChargeAddr;
        elsif (Command(LOAD_MODE_BIT)='1') then
            DS_AVal <= RegModeWord(12 downto 0);
        --elsif (Command(ACTIVE_BIT)='1') then
        --    DS_AVal <= RegRowAddr;
        else
            DS_AVal <= RegColAddr;
        end if;

        if (Command(LOAD_MODE_BIT)='1') then
            DS_BAVal <= RegModeWord(14 downto 13);
        else
            DS_BAVal <= RegBankAddr;
        end if;
    end process;
    process (CLK) begin
        if (CLK'event and CLK='1') then
            -- Multiplex data and byte enables
            if (Command(WRITE_BIT)='1') then
                DS_DMVal <= not RegBE;
            else
                DS_DMVal <= "1111";
            end if;
        end if;
    end process;
    Tristate <= LastCommandWrite;
    
    --------------------------------------------------------------------------
    -- Refresh counter
    -- Issues a refresh when the counter reaches zero (when the current
    -- command is complete)
    process (RST, CLK) begin
        if (RST='1') then
            RefreshCounter <= (others=>'0');
            RefreshNeeded <= '0';
        elsif (CLK'event and CLK='1') then
            if (Command(AUTO_REFRESH_BIT)='1') then
                RefreshCounter <= conv_std_logic_vector(REFRESH_CYCLES-1, RefreshCounter'length);
                RefreshNeeded <= '0';
            elsif (RefreshCounter/=conv_std_logic_vector(0, RefreshCounter'length)) then
                RefreshCounter <= RefreshCounter - 1;
                if (RefreshCounter=conv_std_logic_vector(1, RefreshCounter'length)) then
                    RefreshNeeded <= '1';
                end if;
            end if;
        end if;
    end process;
    
    --------------------------------------------------------------------------
    -- Timers to avoid conflicts of operations
    -- Cannot issue successive ACTIVEs to the same bank faster than tRC
    -- Cannot issue successive ACTIVEs to different banks faster than tRRD
    -- Cannot issue precharge after write until tWR expires
    -- Cannot issue precharge after active until tRAS expires
    -- Cannot issue write until tCAS after a read
    -- Cannot issue read until tWTR expires
    -- Cannot issue commands after ACTIVE until ActiveWaitTimer expires
    -- Cannot issue commands after AUTO_REFRESH until RefreshWaitTimer expires
    -- Cannot issue commands after PRECHARGE until PrechargeWaitTimer expires
    process (RST, CLK) begin
        if (RST='1') then
            tRCTimer(0) <= conv_std_logic_vector(1, TIMER_WIDTH);
            tRASTimer(0) <= conv_std_logic_vector(1, TIMER_WIDTH);
            tRCTimer(1) <= conv_std_logic_vector(1, TIMER_WIDTH);
            tRASTimer(1) <= conv_std_logic_vector(1, TIMER_WIDTH);
            tRCTimer(2) <= conv_std_logic_vector(1, TIMER_WIDTH);
            tRASTimer(2) <= conv_std_logic_vector(1, TIMER_WIDTH);
            tRCTimer(3) <= conv_std_logic_vector(1, TIMER_WIDTH);
            tRASTimer(3) <= conv_std_logic_vector(1, TIMER_WIDTH);
            tRRDTimer <= conv_std_logic_vector(1, TIMER_WIDTH);
            tWRTimer <= conv_std_logic_vector(1, TIMER_WIDTH);
            tWTRTimer <= conv_std_logic_vector(1, TIMER_WIDTH);
            tCASTimer <= conv_std_logic_vector(1, TIMER_WIDTH);
            ActiveWaitTimer <= conv_std_logic_vector(1, TIMER_WIDTH);
            PrechargeWaitTimer <= conv_std_logic_vector(1, TIMER_WIDTH);
            RefreshWaitTimer <= conv_std_logic_vector(1, TIMER_WIDTH);
            LastActiveBank <= (others=>'0');
        elsif (CLK'event and CLK='1') then
            if (NextCommand(ACTIVE_BIT)='1' and InputRegBankAddr="00") then
                tRCTimer(0) <= expand(tRC_CYCLES);
            elsif (tRCTimer(0)(0)='0') then
                tRCTimer(0) <= '0' & tRCTimer(0)(TIMER_WIDTH-1 downto 1);
            end if;
            if (NextCommand(ACTIVE_BIT)='1' and InputRegBankAddr="01") then
                tRCTimer(1) <= expand(tRC_CYCLES);
            elsif (tRCTimer(1)(0)='0') then
                tRCTimer(1) <= '0' & tRCTimer(1)(TIMER_WIDTH-1 downto 1);
            end if;
            if (NextCommand(ACTIVE_BIT)='1' and InputRegBankAddr="10") then
                tRCTimer(2) <= expand(tRC_CYCLES);
            elsif (tRCTimer(2)(0)='0') then
                tRCTimer(2) <= '0' & tRCTimer(2)(TIMER_WIDTH-1 downto 1);
            end if;
            if (NextCommand(ACTIVE_BIT)='1' and InputRegBankAddr="11") then
                tRCTimer(3) <= expand(tRC_CYCLES);
            elsif (tRCTimer(3)(0)='0') then
                tRCTimer(3) <= '0' & tRCTimer(3)(TIMER_WIDTH-1 downto 1);
            end if;
            
            if (NextCommand(ACTIVE_BIT)='1' and InputRegBankAddr="00") then
                tRASTimer(0) <= expand(tRAS_CYCLES);
            elsif (tRASTimer(0)(0)='0') then
                tRASTimer(0) <= '0' & tRASTimer(0)(TIMER_WIDTH-1 downto 1);
            end if;
            if (NextCommand(ACTIVE_BIT)='1' and InputRegBankAddr="01") then
                tRASTimer(1) <= expand(tRAS_CYCLES);
            elsif (tRASTimer(1)(0)='0') then
                tRASTimer(1) <= '0' & tRASTimer(1)(TIMER_WIDTH-1 downto 1);
            end if;
            if (NextCommand(ACTIVE_BIT)='1' and InputRegBankAddr="10") then
                tRASTimer(2) <= expand(tRAS_CYCLES);
            elsif (tRASTimer(2)(0)='0') then
                tRASTimer(2) <= '0' & tRASTimer(2)(TIMER_WIDTH-1 downto 1);
            end if;
            if (NextCommand(ACTIVE_BIT)='1' and InputRegBankAddr="11") then
                tRASTimer(3) <= expand(tRAS_CYCLES);
            elsif (tRASTimer(3)(0)='0') then
                tRASTimer(3) <= '0' & tRASTimer(3)(TIMER_WIDTH-1 downto 1);
            end if;

            if (NextCommand(ACTIVE_BIT)='1') then
                LastActiveBank <= InputRegBankAddr;
                tRRDTimer <= expand(tRRD_CYCLES);
            elsif (tRRDTimer(0)='0') then
                tRRDTimer <= '0' & tRRDTimer(TIMER_WIDTH-1 downto 1);
            end if;
            if (NextCommand(WRITE_BIT)='1') then
                tWRTimer <= expand(tWR_CYCLES);
            elsif (tWRTimer(0)='0') then
                tWRTimer <= '0' & tWRTimer(TIMER_WIDTH-1 downto 1);
            end if;

            if (NextCommand(READ_BIT)='1') then
                tCASTimer <= expand(tCAS_CYCLES);
            elsif (tCASTimer(0)='0') then
                tCASTimer <= '0' & tCASTimer(TIMER_WIDTH-1 downto 1);
            end if;
            if (NextCommand(WRITE_BIT)='1') then
                tWTRTimer <= expand(tWTR_CYCLES);
            elsif (tWTRTimer(0)='0') then
                tWTRTimer <= '0' & tWTRTimer(TIMER_WIDTH-1 downto 1);
            end if;
            if (NextCommand(ACTIVE_BIT)='1') then
                ActiveWaitTimer <= expand(ACTIVE_WAIT_CYCLES);
            elsif (ActiveWaitTimer(0)='0') then
                ActiveWaitTimer <= '0' & ActiveWaitTimer(TIMER_WIDTH-1 downto 1);
            end if;
            if (NextCommand(AUTO_REFRESH_BIT)='1') then
                RefreshWaitTimer <= expand(REFRESH_WAIT_CYCLES);
            elsif (RefreshWaitTimer(0)='0') then
                RefreshWaitTimer <= '0' & RefreshWaitTimer(TIMER_WIDTH-1 downto 1);
            end if;
            if (NextCommand(AUTO_REFRESH_BIT)='1' or NextCommand(PRECHARGE_BIT)='1') then
                PrechargeWaitTimer <= expand(PRECHARGE_WAIT_CYCLES);
            elsif (PrechargeWaitTimer(0)='0') then
                PrechargeWaitTimer <= '0' & PrechargeWaitTimer(TIMER_WIDTH-1 downto 1);
            end if;

            if (NextCommand(ACTIVE_BIT)='1') then
                tRCTimerZero <= ExpandtRC_CYCLES(0);
            else
                tRCTimerZero <= tRCTimer(conv_integer(LastActiveBank))(0) or
                                tRCTimer(conv_integer(LastActiveBank))(1);
            end if;

        end if;
    end process;
    
    PreChargeOK <= '1' when (tRASTimer(0)(0)='1' and tRASTimer(1)(0)='1' and
                             tRASTimer(2)(0)='1' and tRASTimer(3)(0)='1' and
                             tWRTimer(0)='1' and tCASTimer(0)='1') else '0';
                          
    ActiveOK <= tRRDTimer(0) when (LastActiveBank/=InputRegBankAddr) else tRCTimerZero;

    --------------------------------------------------------------------------
    -- Instantiate physical layer
    process (CLK) begin
        if (CLK'event and CLK='1') then
            Tristate1 <= Tristate;
        end if;
    end process;
    process (CLK) begin
        if (CLK'event and CLK='1') then
            DataOut1(15 downto 0) <= DataOut(15 downto 0);
            DS_DMVal1(1 downto 0) <= DS_DMVal(1 downto 0);
        end if;
    end process;
    process (CLK) begin
        if (CLK'event and CLK='0') then
            DataOut1(31 downto 16) <= DataOut(31 downto 16);
            DS_DMVal1(3 downto 2) <= DS_DMVal(3 downto 2);
        end if;
    end process;
    PhyInst : entity work.ZestET1_SDRAMPhy
        port map (
            CLK => CLK,
            CLK90 => CLK90,
            RST => RST,
            DS_DQ => DS_DQ,
            DS_DQS => DS_DQS,
            DS_CLK_P => DS_CLK_P,
            DS_CLK_N => DS_CLK_N,
            DS_DM => DS_DM,
            DS_A => DS_A,
            DS_BA => DS_BA,
            DS_WE_N => DS_WE_N,
            DS_RAS_N => DS_RAS_N,
            DS_CAS_N => DS_CAS_N,
            
            DataOut => DataOut1,
            DataIn => DataIn,
            DataTri_P => nTristate,
            DataTri_N => nTristate,
            DQSTri_P => nTristate1,
            DQSTri_N => nExtendedTristate,
            DM_P => DS_DMVal1(3 downto 2),
            DM_N => DS_DMVal1(1 downto 0),
            DataValid => DataInValid,
            Addr => DS_AVal,
            BankAddr => DS_BAVal,
            WE => DS_WE_NVal,
            RAS => DS_RAS_NVal,
            CAS => DS_CAS_NVal
        );
    nTristate <= not Tristate;
    nTristate1 <= not Tristate1;
    ExtendedTristate <= Tristate or Tristate1;
    nExtendedTristate <= not ExtendedTristate;
    
    --------------------------------------------------------------------------
    -- Mark valid read data in user domain
    process(RST, CLK) begin
        if (RST='1') then
            OwnerFIFOReadCount <= (others=>'0');
            OwnerFIFOWriteCount <= (others=>'0');
        elsif (CLK'event and CLK='1') then
            if (Command(READ_BIT)='1' or Command(WRITE_BIT)='1') then
                OwnerFIFOWriteCount <= OwnerFIFOWriteCount + 1;
            end if;
            if (DataInValid='1') then
                OwnerFIFOReadCount <= OwnerFIFOReadCount + 1;
            end if;
        end if;
    end process;
    process (CLK) begin
        if (CLK'event and CLK='1') then
            OwnerFIFO(conv_integer(OwnerFIFOWriteCount)) <= Command(READ_BIT) &
                                                            RegOwner;
        end if;
    end process;
    OwnerFIFODataVal <= OwnerFIFO(conv_integer(OwnerFIFOReadCount));
    User_DR <= DataIn;
    User_DR_Valid <= OwnerFIFODataVal(8) and DataInValid;
    User_ValidOwner <= OwnerFIFODataVal(7 downto 0);
    
end arch;


//////////////////////////////////////////////////////////////////////
//
// File:      TestApp.c
//
// Purpose:
//    MiniIPS test application
//  
//////////////////////////////////////////////////////////////////////

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "ZestET1.h"
#include "Network.h"

//
// Error handler function
//
void ErrorHandler(const char *Function, 
                  ZESTET1_CARD_INFO *CardInfo,
                  ZESTET1_STATUS Status,
                  const char *Msg)
{
    printf("**** TestApp - Function %s returned an error\n        \"%s\"\n\n", Function, Msg);
    exit(1);
}

//
// Dump trigger results from FPGA
//
void DumpData(ZESTET1_CONNECTION DataConn)
{
    ZESTET1_STATUS Status;

    unsigned char Buffer[1024];

    ZestET1RegisterErrorHandler(NULL);

	printf("funtion DumpData \n");
    while(1)
    {
        Status=NetReadTriggerData(DataConn, Buffer, sizeof(Buffer));
        if (Status==ZESTET1_SUCCESS)
        {
            unsigned __int64 TimeStamp;
            int PreSamples;
            int PostSamples;
            int i;

            // Received some data
            TimeStamp = (((unsigned long long)(Buffer[0]))<<56ll) | (((unsigned long long)(Buffer[1]))<<48ll) |
                        (((unsigned long long)(Buffer[2]))<<40ll) | (((unsigned long long)(Buffer[3]))<<32ll) |
                        (((unsigned long long)(Buffer[4]))<<24ll) | (((unsigned long long)(Buffer[5]))<<16ll) |
                        (((unsigned long long)(Buffer[6]))<<8ll) | (((unsigned long long)(Buffer[7]))<<0ll);
            printf("Time: %.2f secs\n", TimeStamp/100000000.0);
            PreSamples = (Buffer[8]<<8) | Buffer[9];
            PostSamples = (Buffer[10]<<8) | Buffer[11];
            for (i=0; i<PreSamples; i++)
            {
                int Val = (Buffer[12+2*i]<<8) | Buffer[12+2*i+1];
                printf("%d\n", Val);

            }
            printf("--Trigger Point--\n");
            for (; i<PreSamples+PostSamples; i++)
            {
                printf("%d\n", (Buffer[12+2*i]<<8) | Buffer[12+2*i+1]);
            }
            i = 12+2*i;
            printf("Neutron? : %s\n", Buffer[i] ? "Yes" : "No");
            printf("Peak : %d\n", (Buffer[i+1]<<8) | Buffer[i+2]);
            printf("Area : %d\n", (Buffer[i+3]<<16) | (Buffer[i+4]<<8) | Buffer[i+5]);

            ZestET1RegisterErrorHandler(ErrorHandler);
            return;
        }
		//else{
		//	printf("Status : %04x\n", Status);
		//}
    }
}

//
// Main program
//
int main(int argc, char **argv)
{
    unsigned long Count;
    unsigned long NumCards;
    int CardIndex=0;
    ZESTET1_CARD_INFO *CardInfo;
    ZESTET1_CONNECTION CtrlConn;
    ZESTET1_CONNECTION DataConn;
    unsigned char Val;
    unsigned long Time;
    int i,j;
    unsigned char nEnables[32];
    unsigned char ReadData[32];
    unsigned char WriteData[32];

    //
    // Install an error handler and initialise library
    //
    ZestET1RegisterErrorHandler(ErrorHandler);
    ZestET1Init();

    //
    // Request information about the system
    // Wait for 2 seconds (on each interface) for boards to respond to query
    //
    printf("Searching for ZestET1 cards...\n");
    ZestET1CountCards(&NumCards, &CardInfo, 2000);
    printf("%ld available cards in the system\n\n\n", NumCards);
    if (NumCards==0)
    {
        printf("No cards found\n");
        exit(1);
    }

    for (Count=0; Count<NumCards; Count++)
    {
        printf("%ld : SerialNum = %ld, IPAddress = %d.%d.%d.%d\n",
            Count, CardInfo[Count].SerialNumber,
            CardInfo[Count].IPAddr[0], CardInfo[Count].IPAddr[1],
            CardInfo[Count].IPAddr[2], CardInfo[Count].IPAddr[3]);
        //FIXME
        if (CardInfo[Count].IPAddr[3]==101) CardIndex = Count;
    }

    //
    // Configure the FPGA directly from a file
    // Wait for user FPGA application to start listening on its connection
    // Open control and data connections to user FPGA
    //
    ZestET1ConfigureFromFile(&CardInfo[CardIndex], "../miniips.bit");
    Sleep(1000);
    ZestET1OpenConnection(&CardInfo[CardIndex], ZESTET1_TYPE_TCP, 0x6000, 0, &CtrlConn);
    ZestET1OpenConnection(&CardInfo[CardIndex], ZESTET1_TYPE_TCP, 0x6001, 0, &DataConn);

    // 
    // Access registers
    //
    NetReadControlReg(CtrlConn, CTRL_FIRMWARE_VERSION, &Val);
    printf("Firmware version : %x.%x\n", Val>>4, Val&0xf);

    // Get Time
    for (i=0; i<10; i++)
    {
        NetWriteControlReg(CtrlConn, CTRL_TIME_MSB, 0); // Latches current time
        NetReadControlReg(CtrlConn, CTRL_TIME_MSB, &Val); // Latches current time
        Time = Val<<24;
        NetReadControlReg(CtrlConn, CTRL_TIME_2, &Val); // Latches current time
        Time |= Val<<16;
        NetReadControlReg(CtrlConn, CTRL_TIME_1, &Val); // Latches current time
        Time |= Val<<8;
        NetReadControlReg(CtrlConn, CTRL_TIME_LSB, &Val); // Latches current time
        Time |= Val;
        printf("Current Time = %d\n", Time);
    }

    // Test SPI
    // Generate (very slow) ramp on DAC output
#if 1
    // Set DAC_RESETN high, DAC_LDACN high, DAC_PD low, DAC_DCEN low, DAC_SYNC high, DAC_CLRN high
    NetSetGPIO(CtrlConn, 0xcfffffff);           // Set GPIOs
    NetSetGPIODirection(CtrlConn, 0xcdffdf3f);  // Enable outputs

    NetSetGPIO(CtrlConn, 0xcfffdfff);       // Set DAC_SYNCN low
    WriteData[0] = 0x09;                    // Soft power up
    WriteData[1] = 0x0;
    WriteData[2] = 0x0;
    ReadData[0] = 0;
    ReadData[1] = 0;
    ReadData[2] = 0;
    NetSPIReadWrite(CtrlConn, 2, 8, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, 0xcfffffff);       // Set DAC_SYNCN high

    NetSetGPIO(CtrlConn, 0xcfffdfff);       // Set DAC_SYNCN low
    WriteData[0] = 0x0c;                    // Write to control register
    WriteData[1] = 0x24;                    // Use internal vref 1v25
    WriteData[2] = 0x00;
    ReadData[0] = 0;
    ReadData[1] = 0;
    ReadData[2] = 0;
    NetSPIReadWrite(CtrlConn, 2, 8, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, 0xcfffffff);       // Set DAC_SYNCN high

    for (i=0; i<1 /*i<0x10000000*/; i++)
    {
        //for (j=0; j<40; j++)  // Set all channels
        j=35;                   // Cal pulse 4 level
        {
            NetSetGPIO(CtrlConn, 0xcfffdfff);       // Set DAC_SYNCN low
            WriteData[0] = j;                       // Write to A register of channel
            WriteData[1] = 0xC0 | ((i>>8)&0x3f);    // Write to input data register (REG1:0 = 11)
            WriteData[2] = i&0xff;
            ReadData[0] = 0;
            ReadData[1] = 0;
            ReadData[2] = 0;
            NetSPIReadWrite(CtrlConn, 2, 8, WriteData, ReadData, 3); 
            NetSetGPIO(CtrlConn, 0xcfffffff);       // Set DAC_SYNCN high
            Sleep(1);
        }

        // Toggle LDAC low then high
        NetSetGPIO(CtrlConn, 0xcfffff7f);       // Set DAC_LDACN low
        NetSetGPIO(CtrlConn, 0xcfffffff);       // Set DAC_LDACN high
    }

#endif

    // Test temperature sensor
    // NB: The DAC has to be properly powered and out of reset (see above)
    // otherwise it will hold the data out pin of the temp sensor low
#if 1
    // Set DAC_RESETN high, DAC_LDACN high, DAC_PD low, DAC_DCEN low, DAC_SYNC high, DAC_CLRN high
    // Set temp sensor CSN as output
    NetSetGPIO(CtrlConn, 0xcfffffff);           // Set GPIOs
    NetSetGPIODirection(CtrlConn, 0xcddfdf3f);  // Enable outputs

    NetSetGPIO(CtrlConn, 0xcfdfffff);       // Set CSN low
    WriteData[0] = 0xff;                    // Reset
    WriteData[1] = 0xff;
    WriteData[2] = 0xff;
    WriteData[3] = 0xff;
    NetSPIReadWrite(CtrlConn, 0xf, 8, WriteData, ReadData, 4); 
    NetSetGPIO(CtrlConn, 0xcfffffff);       // Set CSN high
    Sleep(1);

    NetSetGPIO(CtrlConn, 0xcfdfffff);       // Set CSN low
    WriteData[0] = 0x58;                    // Read from ID register address 3
    WriteData[1] = 0xff;
    ReadData[0] = 0;
    ReadData[1] = 0;
    NetSPIReadWrite(CtrlConn, 0xf, 8, WriteData, ReadData, 2); 
    NetSetGPIO(CtrlConn, 0xcfffffff);       // Set CSN high
    printf("Temperature chip ID = 0x%02x (should be 0xc3)\n", ReadData[1]);
#endif


#if 1
    // Test ADC SDIO
    NetSetGPIO(CtrlConn, 0xffffffff);   // Set GPIO9, 8 high (ADC_PDN, ADC_CSN)
    NetSetGPIODirection(CtrlConn, 0xfffffcff);      // Set GPIO9, 8 as output

    // Read model number
    NetSetGPIO(CtrlConn, 0xfffffcff);   // Set GPIO9, 8 low (powers up ADC and selects SPI port)
    nEnables[0] = 0x00;                 // Output
    nEnables[1] = 0x00;
    nEnables[2] = 0xff;                 // Input
    WriteData[0] = 0x80 | 0x00 | 0x00;  // RnW = 1 so read, W1:0=0 so read 1 byte, A12:8=0
    WriteData[1] = 0x01;                // A7:0 = 0x01 so read from register address 0x001
    WriteData[2] = 0x00;
    ReadData[0] = 0;
    ReadData[1] = 0;
    ReadData[2] = 0;
    NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, 0xfffffdff);   // Set GPIO8 high
    printf("ADC model = %02x (should be 0x92)\n", ReadData[2]);

    // Write test generator enable register
    NetSetGPIO(CtrlConn, 0xfffffcff);   // Set GPIO8 low
    nEnables[0] = 0x00;                 // Output
    nEnables[1] = 0x00;
    nEnables[2] = 0x00;
    WriteData[0] = 0x00 | 0x00 | 0x00;  // RnW = 0 so write, W1:0=0 so read 1 byte, A12:8=0
    WriteData[1] = 0x0d;                // A7:0 = 0x0d so write to register address 0x00d (test mode)
    //WriteData[2] = 0x04;                // Checkerboard mode
    //WriteData[2] = 0x06;                // Pseudo-random short mode
    WriteData[2] = 0x48;                // Alternating user value mode
    //WriteData[2] = 0x0b;                // One bit high
    ReadData[0] = 0;
    ReadData[1] = 0;
    ReadData[2] = 0;
    NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, 0xfffffdff);   // Set GPIO8 high

    // Write test values to ADC test pattern generator
    NetSetGPIO(CtrlConn, 0xfffffcff);   // Set GPIO8 low
    nEnables[0] = 0x00;                 // Output
    nEnables[1] = 0x00;
    nEnables[2] = 0x00;
    WriteData[0] = 0x00 | 0x00 | 0x00;  // RnW = 0 so write, W1:0=0 so write 1 byte, A12:8=0
    WriteData[1] = 0x19;                // A7:0 = 0x019 so write to register address 0x019 (user test value)
    WriteData[2] = 0x34;
    ReadData[0] = 0;
    ReadData[1] = 0;
    ReadData[2] = 0;
    NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, 0xfffffdff);   // Set GPIO8 high
    NetSetGPIO(CtrlConn, 0xfffffcff);   // Set GPIO8 low
    nEnables[0] = 0x00;                 // Output
    nEnables[1] = 0x00;
    nEnables[2] = 0x00;
    WriteData[0] = 0x00 | 0x00 | 0x00;  // RnW = 0 so write, W1:0=0 so write 1 byte, A12:8=0
    WriteData[1] = 0x1a;                // A7:0 = 0x01a so write to register address 0x01a (user test value)
    WriteData[2] = 0x12;                
    ReadData[0] = 0;
    ReadData[1] = 0;
    ReadData[2] = 0;
    NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, 0xfffffdff);   // Set GPIO8 high
    NetSetGPIO(CtrlConn, 0xfffffcff);   // Set GPIO8 low
    nEnables[0] = 0x00;                 // Output
    nEnables[1] = 0x00;
    nEnables[2] = 0x00;
    WriteData[0] = 0x00 | 0x00 | 0x00;  // RnW = 0 so write, W1:0=0 so write 1 byte, A12:8=0
    WriteData[1] = 0x1b;                // A7:0 = 0x01b so write to register address 0x01b (user test value)
    WriteData[2] = 0x56;
    ReadData[0] = 0;
    ReadData[1] = 0;
    ReadData[2] = 0;
    NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, 0xfffffdff);   // Set GPIO8 high
    NetSetGPIO(CtrlConn, 0xfffffcff);   // Set GPIO8 low
    nEnables[0] = 0x00;                 // Output
    nEnables[1] = 0x00;
    nEnables[2] = 0x00;
    WriteData[0] = 0x00 | 0x00 | 0x00;  // RnW = 0 so write, W1:0=0 so write 1 byte, A12:8=0
    WriteData[1] = 0x1c;                // A7:0 = 0x01c so write to register address 0x01c (user test value)
    WriteData[2] = 0x78;
    ReadData[0] = 0;
    ReadData[1] = 0;
    ReadData[2] = 0;
    NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, 0xfffffdff);   // Set GPIO8 high
#endif

#if 1
    //
    // Test triggering
    //
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_TRIG_LEVEL_HIGH, (1000>>8));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_TRIG_LEVEL_LOW, (1000>>0));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_PRE_CALC_SAMPLES_HIGH, (5>>8));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_PRE_CALC_SAMPLES_LOW, (5>>0));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_POST_CALC_SAMPLES_HIGH, (12>>8));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_POST_CALC_SAMPLES_LOW, (12>>0));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_PRE_NET_SAMPLES_HIGH, (7>>8));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_PRE_NET_SAMPLES_LOW, (7>>0));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_POST_NET_SAMPLES_HIGH, (17>>8));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_POST_NET_SAMPLES_LOW, (17>>0));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_PEAK_THRESHOLD_HIGH, (7000>>8));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_PEAK_THRESHOLD_LOW, (7000>>0));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_AREA_THRESHOLD_HIGH, (3000>>16));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_AREA_THRESHOLD_MED, (3000>>8));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_AREA_THRESHOLD_LOW, (3000>>0));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_ENABLE, 1);
    for (i=0; i<10; i++)
        DumpData(DataConn);
#endif

    //
    // Free the card information structure
    //
    ZestET1CloseConnection(CtrlConn);
    ZestET1CloseConnection(DataConn);
    ZestET1FreeCards(CardInfo);

    //
    // Close library
    //
    ZestET1Close();

    return 0;
}

-- ZestET1 Ethernet Receive FIFO
--   File name: ZestET1_RxFIFO.vhd
--   Version: 1.00
--   Date: 10/3/2011
--
--   FIFO to bridge from 16 bit GigExpedite interface to 8 bit user interface for data
--   received from the network.  This handles cases where either 1 or 2 bytes of data
--   are valid from the GigExpedite.
--
--   Copyright (C) 2011 Orange Tree Technologies Ltd. All rights reserved.
--   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and
--   modify this logic core in any form including but not limited to VHDL source code or
--   EDIF netlist in FPGA designs that target the ZestET1.
--   Orange Tree Technologies prohibits the use of this logic core or any modification of
--   it in any form including but not limited to VHDL source code or EDIF netlist in
--   FPGA or ASIC designs that target any other hardware unless the purchaser of the
--   ZestET1 has purchased the appropriate licence from Orange Tree Technologies.
--   Contact Orange Tree Technologies if you want to purchase such a licence.
--
--  *****************************************************************************************
--  **
--  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
--  **              provided to you "as is". Orange Tree Technologies and its licensors 
--  **              make and you receive no warranties or conditions, express, implied, 
--  **              statutory or otherwise, and Orange Tree Technologies specifically 
--  **              disclaims any implied warranties of merchantability, non-infringement,
--  **              or fitness for a particular purpose. Orange Tree Technologies does not
--  **              warrant that the functions contained in these designs will meet your 
--  **              requirements, or that the operation of these designs will be 
--  **              uninterrupted or error free, or that defects in the Designs will be 
--  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
--  **              make any representations regarding use or the results of the use of the 
--  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
--  **
--  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
--  **              or its licensors be liable for any loss of data, lost profits, cost or 
--  **              procurement of substitute goods or services, or for any special, 
--  **              incidental, consequential, or indirect damages arising from the use or 
--  **              operation of the designs or accompanying documentation, however caused 
--  **              and on any theory of liability. This limitation will apply even if 
--  **              Orange Tree Technologies has been advised of the possibility of such 
--  **              damage. This limitation shall apply notwithstanding the failure of the 
--  **              essential purpose of any limited remedies herein.
--  **
--  *****************************************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;

library UNISIM;
use UNISIM.VComponents.all;

entity ZestET1_RxFIFO is
    port (
        RST : in std_logic;
        CLK : in std_logic;
        WE : in std_logic;
        DataIn : in std_logic_vector(17 downto 0);
        RE : in std_logic;
        DataOut : out std_logic_vector(7 downto 0);
        Empty : out std_logic;
        Full : out std_logic
    );
end ZestET1_RxFIFO;

architecture arch of ZestET1_RxFIFO is

    -- Declare constants
    constant LOG2_FIFO_DEPTH : integer := 4;
    constant FIFO_DEPTH : integer := 2**LOG2_FIFO_DEPTH;
    constant ALMOST_FULL : std_logic_vector(LOG2_FIFO_DEPTH-1 downto 0) :=
                            conv_std_logic_vector(FIFO_DEPTH-8, LOG2_FIFO_DEPTH);
    
    -- Declare signals
    type ARRAY_TYPE is array(0 to FIFO_DEPTH-1) of std_logic_vector(17 downto 0);
    signal StorageArray : ARRAY_TYPE;
    attribute ram_style : string;
    attribute ram_style of StorageArray : signal is "distributed";
    signal StorageArrayReadData : std_logic_vector(17 downto 0);
    signal StorageArrayVal : std_logic_vector(17 downto 0);
    signal WriteCount : std_logic_vector(LOG2_FIFO_DEPTH-1 downto 0);
    signal RegWriteCount : std_logic_vector(LOG2_FIFO_DEPTH-1 downto 0);
    signal ReadCount : std_logic_vector(LOG2_FIFO_DEPTH-1 downto 0);
    signal NextWriteCount : std_logic_vector(LOG2_FIFO_DEPTH-1 downto 0);
    signal NextReadCount : std_logic_vector(LOG2_FIFO_DEPTH-1 downto 0);
    signal SelReadCount : std_logic_vector(LOG2_FIFO_DEPTH-1 downto 0);
    signal Count : std_logic_vector(LOG2_FIFO_DEPTH-1 downto 0);
    signal ReadToggle : std_logic;
    signal IncReadCount : std_logic;

begin
    
    -- Storage array - inferred as a distributed ram
    process (CLK)
    begin
        if (CLK'event and CLK='1') then
            if (WE='1') then
                StorageArray(conv_integer(WriteCount)) <= DataIn;
            end if;
            StorageArrayVal <= StorageArrayReadData;
        end if;
    end process;
    StorageArrayReadData <= StorageArray(conv_integer(SelReadCount));
    SelReadCount <= NextReadCount when IncReadCount='1' else ReadCount;
    DataOut <= StorageArrayVal(15 downto 8) when ReadToggle='0' and
                                                 StorageArrayVal(17)='1' else
               StorageArrayVal(7 downto 0);
    
    -- Manage read and write counters
    process (RST, CLK)
    begin
        if (CLK'event and CLK='1') then
            if (RST='1') then
                WriteCount <= (others=>'0');
                RegWriteCount <= (others=>'0');
                ReadCount <= (others=>'0');
                ReadToggle <= '0';
                Empty <= '1';
                Full <= '0';
            else
                if (WE='1') then
                    WriteCount <= NextWriteCount;
                end if;
                RegWriteCount <= WriteCount;
                if (RE='1') then
                    if (IncReadCount='0') then
                        ReadToggle <= not ReadToggle;
                    else
                        ReadToggle <= '0';
                        ReadCount <= NextReadCount;
                    end if;
                end if;
                
                -- Full and empty flags
                -- Full flag is actually nearly full to allow for GigExpedite
                -- read latency
                if (Count>=ALMOST_FULL) then
                    Full <= '1';
                else
                    Full <= '0';
                end if;
                if (SelReadCount=RegWriteCount) then
                    Empty <= '1';
                else
                    Empty <= '0';
                end if;
            end if;
        end if;
    end process;
    IncReadCount <= '0' when ReadToggle='0' and StorageArrayVal(17 downto 16)="11" else
                    RE;
    NextWriteCount <= WriteCount + 1;
    NextReadCount <= ReadCount + 1;
    Count <= WriteCount-ReadCount;
    
end arch;

--
-- Module containing control and status registers
-- Also implements SPI interfaces
--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity Control is
    port (
        CLK : in std_logic;
        RST : in std_logic;

        -- Network connections
        TxData : out std_logic_vector(7 downto 0);
        TxEnable : out std_logic;
        TxBusy : in std_logic;
        TxBufferEmpty : in std_logic;
        RxEnable : in std_logic;
        RxData : in std_logic_vector(7 downto 0);
        RxBusy : out std_logic;
        
        -- Register interface
        RegWE : out std_logic;
        RegRE : out std_logic;
        RegAddr : out std_logic_vector(15 downto 0);
        RegWriteData : out std_logic_vector(7 downto 0);
        RegReadValid : in std_logic;
        RegReadData : in std_logic_vector(7 downto 0);
        
        -- Timestamp
        TimeStamp : in std_logic_vector(63 downto 0);
        
        -- GPIO and SPI interfaces
        GPIODataOut : out std_logic_vector(31 downto 0);
        GPIODataIn : in std_logic_vector(31 downto 0);
        GPIOnEnable : out std_logic_vector(31 downto 0);
        SPICLK : out std_logic;
        SPIMOSI : out std_logic;
        SPIMISO : in std_logic;
        SDIOnEnable : out std_logic;
        SDIODataOut : out std_logic;
        SDIODataIn : in std_logic
    );
end Control;

architecture arch of Control is

    -- Declare constants
    constant FIRMWARE_VERSION : std_logic_vector(7 downto 0) := X"10";  -- Version 1.0
    
    constant SOF : std_logic_vector(8 downto 0) := '1' & X"01";
    constant EOF : std_logic_vector(8 downto 0) := '1' & X"02";

    constant TRIGGER_READ_CMD : std_logic_vector(7 downto 0) := X"00";
    constant TRIGGER_WRITE_CMD : std_logic_vector(7 downto 0) := X"01";
    constant CONTROL_READ_CMD : std_logic_vector(7 downto 0) := X"02";
    constant CONTROL_WRITE_CMD : std_logic_vector(7 downto 0) := X"03";

    -- Declare signals
	signal LastCtrl : std_logic;
	signal RxDataVal : std_logic_vector(8 downto 0);
	signal RxDataValValid : std_logic;

    type RX_STATE_TYPE is (RX_IDLE, RX_CMD, RX_ADDRH, RX_ADDRL, RX_DATA, RX_WAIT_EOF);
    signal RxCurrentState : RX_STATE_TYPE;
    signal Command : std_logic_vector(7 downto 0);
    signal RegAddrVal : std_logic_vector(15 downto 0);
    signal RegWrDataVal : std_logic_vector(7 downto 0);
    signal RegWEVal : std_logic;
    signal RegREVal : std_logic;
    
    type TX_STATE_TYPE is (TX_IDLE, TX_SOF_1, TX_SOF_2, TX_CMD, TX_DATA, TX_PADDING, TX_EOF_1, TX_EOF_2);
    signal TxCurrentState : TX_STATE_TYPE;
    signal TxCommand : std_logic_vector(7 downto 0);
    signal TxEnableVal : std_logic;
    signal TxDataVal : std_logic_vector(7 downto 0);
    
    signal RegReadDataStoreFull : std_logic;
    signal RegReadDataStore : std_logic_vector(7 downto 0);
    
    signal CmdReadValid : std_logic;
    signal CmdReadDataStoreFull : std_logic;
    signal CmdReadDataStore : std_logic_vector(7 downto 0);
    
    signal GPIODataOutVal : std_logic_vector(31 downto 0);
    signal GPIODataInVal : std_logic_vector(31 downto 0);
    signal GPIOnEnableVal : std_logic_vector(31 downto 0);
    
    signal LatchTimeStamp : std_logic_vector(63 downto 0);

    signal ShiftLoad : std_logic;
    signal ShiftEnable : std_logic;
    signal NextCount : std_logic_vector(7 downto 0);
    signal ClockCount : std_logic_vector(2 downto 0);
    signal SPI_BitCount : std_logic_vector(7 downto 0);
    signal SPI_SCKVal : std_logic;
    signal SPI_ClockRate : std_logic_vector(2 downto 0);
    signal SPI_DataInReg : std_logic_vector(1 downto 0);
    signal SPI_ShiftReg : std_logic_vector(7 downto 0);
    signal SPI_DataOutVal : std_logic;
    signal SPI_nEnableReg : std_logic_vector(7 downto 0);
    signal SPInEnableReg : std_logic_vector(7 downto 0);
    signal SPI_nEnableVal : std_logic;
    signal SPI_InMux : std_logic;
    signal SPI_DataLen : std_logic_vector(5 downto 0);
    
    
    -- Chipscope
    component icon is
        port (
            control0 : inout std_logic_vector(35 downto 0)
        );
    end component;
    component ila is
        port (
            control : inout std_logic_vector(35 downto 0);
            clk : in std_logic;
            trig0 : in std_logic_vector(127 downto 0)
        );
    end component;
    signal control0 : std_logic_vector(35 downto 0);
    signal trig0 : std_logic_vector(127 downto 0);
    
begin

    --------------------------------------------------------------------------
    -- Remove byte stuffing on incoming data
	process (RST, CLK) begin
		if (RST='1') then
			LastCtrl <= '0';
			RxDataVal <= (others=>'0');
			RxDataValValid <= '0';
		elsif (CLK'event and CLK='1') then
			RxDataValValid <= '0';
			if (RxEnable='1') then
				if (RxData=X"10") then
					if (LastCtrl='1') then
						RxDataVal <= '0'&X"10";
						RxDataValValid <= '1';
						LastCtrl <= '0';
					else
						LastCtrl <= '1';
					end if;
				else
					LastCtrl <= '0';
					RxDataVal <= LastCtrl & RxData;
					RxDataValValid <= '1';
				end if;
			end if;
		end if;
	end process;
	
    --------------------------------------------------------------------------
	-- Parse incoming commands
	process (RST, CLK) begin
		if (RST='1') then
			RxCurrentState <= RX_IDLE;
			Command <= (others=>'0');
			RegAddrVal <= (others=>'0');
			RegWrDataVal <= (others=>'0');
			RegWEVal <= '0';
			RegREVal <= '0';
		elsif (CLK'event and CLK='1') then
			RegWEVal <= '0';
			RegREVal <= '0';
			
			if (RxDataValValid='1') then
				case RxCurrentState is
					when RX_IDLE =>
                        if (RxDataVal=SOF) then
                            RxCurrentState <= RX_CMD;
                        end if;
						
					when RX_CMD =>
                        if (RxDataVal(8)='1') then
                            RxCurrentState <= RX_IDLE;
                        else
                            Command <= RxDataVal(7 downto 0);
                            RxCurrentState <= RX_ADDRH;
                        end if;
						
					when RX_ADDRH =>
                        if (RxDataVal(8)='1') then
                            RxCurrentState <= RX_IDLE;
                        else
                            RegAddrVal(15 downto 8) <= RxDataVal(7 downto 0);
                            RxCurrentState <= RX_ADDRL;
                        end if;
						
					when RX_ADDRL =>
                        if (RxDataVal(8)='1') then
                            RxCurrentState <= RX_IDLE;
                        else
                            RegAddrVal(7 downto 0) <= RxDataVal(7 downto 0);
                            if (Command=TRIGGER_READ_CMD or Command=CONTROL_READ_CMD) then
                                RegREVal <= '1';
                                RxCurrentState <= RX_WAIT_EOF;
                            else
                                RxCurrentState <= RX_DATA;
                            end if;
                        end if;
						
					when RX_DATA =>
                        if (RxDataVal(8)='1') then
                            RxCurrentState <= RX_IDLE;
                        else
                            RegWEVal <= '1';
                            RegWrDataVal <= RxDataVal(7 downto 0);
                            RxCurrentState <= RX_DATA;
                        end if;
						
					when RX_WAIT_EOF =>
                        if (RxDataVal(8)='1') then
                            RxCurrentState <= RX_IDLE;
                        end if;

				end case;
			end if;
		end if;
	end process;
	
    --------------------------------------------------------------------------
	-- Send responses
	process (RST, CLK) begin
		if (RST='1') then
			TxCurrentState <= TX_IDLE;
			TxCommand <= CONTROL_READ_CMD;
		elsif (CLK'event and CLK='1') then
			case TxCurrentState is
				when TX_IDLE =>
                    if (RegReadValid='1') then
                        TxCommand <= TRIGGER_READ_CMD;
                        TxCurrentState <= TX_SOF_1;
                    elsif (CmdReadValid='1') then
                        TxCommand <= CONTROL_READ_CMD;
                        TxCurrentState <= TX_SOF_1;
					end if;
					
				when TX_SOF_1 =>
                    if (TxEnableVal='1') then
                        TxCurrentState <= TX_SOF_2;
                    end if;
				when TX_SOF_2 =>
                    if (TxEnableVal='1') then
                        TxCurrentState <= TX_CMD;
                    end if;
					
				when TX_CMD =>
                    if (TxEnableVal='1') then
                        TxCurrentState <= TX_DATA;
                    end if;
					
				when TX_DATA => 
                    if (TxEnableVal='1' and (TxCommand=CONTROL_READ_CMD or TxCommand=TRIGGER_READ_CMD)) then
                        if (TxDataVal=X"10") then
                            TxCurrentState <= TX_PADDING;
                        else
                            TxCurrentState <= TX_EOF_1;
                        end if;
                    end if;

				when TX_PADDING =>
                    if (TxEnableVal='1') then
                        if (TxCommand=CONTROL_READ_CMD or TxCommand=TRIGGER_READ_CMD) then
							TxCurrentState <= TX_EOF_1;
                        else
                            TxCurrentState <= TX_DATA;
						end if;
					end if;

				when TX_EOF_1 =>
                    if (TxEnableVal='1') then
                        TxCurrentState <= TX_EOF_2;
                    end if;
				when TX_EOF_2 =>
                    if (TxEnableVal='1') then
                        TxCurrentState <= TX_IDLE;
					end if;
					
			end case;
			
		end if;
	end process;
    RxBusy <= '0';

    process (TxCurrentState, TxBusy, TxCommand, RegReadDataStoreFull, RegReadDataStore) begin
        TxEnableVal <= '0';
        TxDataVal <= (others=>'0');
        
        case TxCurrentState is
            when TX_SOF_1 =>
                TxEnableVal <= not TxBusy;
                TxDataVal <= X"10";
            when TX_SOF_2 =>
                TxEnableVal <= not TxBusy;
                TxDataVal <= SOF(7 downto 0);
                
            when TX_CMD =>
                TxEnableVal <= not TxBusy;
                TxDataVal <= TxCommand;
            when TX_DATA =>
                if (TxCommand=TRIGGER_READ_CMD) then
                    TxEnableVal <= not TxBusy and RegReadDataStoreFull;
                    TxDataVal <= RegReadDataStore;
                elsif (TxCommand=CONTROL_READ_CMD) then
                    TxEnableVal <= not TxBusy and CmdReadDataStoreFull;
                    TxDataVal <= CmdReadDataStore;
                end if;
            when TX_PADDING =>
                TxEnableVal <= not TxBusy;
                TxDataVal <= X"10";
                
            when TX_EOF_1 =>
                TxEnableVal <= not TxBusy;
                TxDataVal <= X"10";
            when TX_EOF_2 =>
                TxEnableVal <= not TxBusy;
                TxDataVal <= EOF(7 downto 0);
                
            when others =>
                TxEnableVal <= '0';
                TxDataVal <= (others=>'0');
        end case;
    end process;
    TxEnable <= TxEnableVal;
    TxData <= TxDataVal;
    
    --------------------------------------------------------------------------
    -- Register returned read data from trigger units
    process (RST, CLK) begin
        if (RST='1') then
            RegReadDataStoreFull <= '0';
            RegReadDataStore <= (others=>'0');
        elsif (CLK'event and CLK='1') then
            if (RegReadValid='1') then
                RegReadDataStoreFull <= '1';
                RegReadDataStore <= RegReadData;
            elsif (TxEnableVal='1' and TxCurrentState=TX_DATA and TxCommand=TRIGGER_READ_CMD) then
                RegReadDataStoreFull <= '0';
            end if;
        end if;
    end process;
    
    -- Connect trigger unit register accesses
    RegAddr <= RegAddrVal;
    RegWriteData <= RegWrDataVal;
    RegWE <= RegWEVal when Command=TRIGGER_WRITE_CMD else '0';
    RegRE <= RegREVal when Command=TRIGGER_READ_CMD else '0';

    --------------------------------------------------------------------------
    -- GPIO registers
    process (RST, CLK) begin
        if (RST='1') then
            GPIOnEnableVal <= (others=>'1');
            GPIODataOutVal <= (others=>'0');
            GPIODataInVal <= (others=>'0');
            CmdReadDataStoreFull <= '0';
            CmdReadDataStore <= (others=>'0');
            LatchTimeStamp <= (others=>'0');
        elsif (CLK'event and CLK='1') then
            GPIODataInVal <= GPIODataIn;
            
            if (RegWEVal='1' and Command=CONTROL_WRITE_CMD) then
                case RegAddrVal is
                    when X"0000" => GPIODataOutVal(31 downto 24) <= RegWrDataVal;
                    when X"0001" => GPIODataOutVal(23 downto 16) <= RegWrDataVal;
                    when X"0002" => GPIODataOutVal(15 downto 8) <= RegWrDataVal;
                    when X"0003" => GPIODataOutVal(7 downto 0) <= RegWrDataVal;
                    when X"0004" => GPIOnEnableVal(31 downto 24) <= RegWrDataVal;
                    when X"0005" => GPIOnEnableVal(23 downto 16) <= RegWrDataVal;
                    when X"0006" => GPIOnEnableVal(15 downto 8) <= RegWrDataVal;
                    when X"0007" => GPIOnEnableVal(7 downto 0) <= RegWrDataVal;
                    
                    when X"0011" => SPInEnableReg <= RegWrDataVal;
                    when X"0012" => SPI_InMux <= RegWrDataVal(7); SPI_DataLen <= RegWrDataVal(5 downto 0);
                    when X"0013" => SPI_ClockRate <= RegWrDataVal(2 downto 0);
                    
                    when X"0020" => LatchTimeStamp <= TimeStamp;
                    
                    when others => null;
                end case;
            end if;
            
            if (RegREVal='1' and Command=CONTROL_READ_CMD) then
                CmdReadDataStoreFull <= '1';
                case RegAddrVal is
                    when X"0000" => CmdReadDataStore <= GPIODataIn(31 downto 24);
                    when X"0001" => CmdReadDataStore <= GPIODataIn(23 downto 16);
                    when X"0002" => CmdReadDataStore <= GPIODataIn(15 downto 8);
                    when X"0003" => CmdReadDataStore <= GPIODataIn(7 downto 0);
                    
                    when X"0010" => CmdReadDataStore <= SPI_ShiftReg(6 downto 0) & SPI_DataInReg(conv_integer(SPI_InMux));
                    
                    when X"0020" => CmdReadDataStore <= LatchTimeStamp(31 downto 24);
                    when X"0021" => CmdReadDataStore <= LatchTimeStamp(23 downto 16);
                    when X"0022" => CmdReadDataStore <= LatchTimeStamp(15 downto 8);
                    when X"0023" => CmdReadDataStore <= LatchTimeStamp(7 downto 0);
                    
                    when X"ffff" => CmdReadDataStore <= FIRMWARE_VERSION;
                    when others => null;
                end case;
            end if;
                
            if (TxEnableVal='1' and TxCurrentState=TX_DATA and TxCommand=CONTROL_READ_CMD) then
                CmdReadDataStoreFull <= '0';
            end if;
        end if;
    end process;
    CmdReadValid <= '1' when RegREVal='1' and Command=CONTROL_READ_CMD else '0';
    GPIODataOut <= GPIODataOutVal;
    GPIOnEnable <= GPIOnEnableVal;
    
    --------------------------------------------------------------------------
    -- SPI controller
    --FIXME: SPI clock modes?
    ShiftLoad <= '1' when RegWEVal='1' and Command=CONTROL_WRITE_CMD and RegAddrVal=X"0010" else '0';
    ShiftEnable <= '1' when SPI_BitCount(7)='0' and SPI_BitCount(0)='1' else '0';
    NextCount <= SPI_BitCount - 1;
    process (RST, CLK) begin
        if (RST='1') then
            SPI_BitCount <= X"ff";
            SPI_SCKVal <= '1';
            ClockCount <= (others=>'0');
        elsif (CLK'event and CLK='1') then
            if (ClockCount="000" or ShiftLoad='1') then
                ClockCount <= SPI_ClockRate;
            else
                ClockCount <= ClockCount - 1;
            end if;
            if (ShiftLoad='1') then
                SPI_BitCount(7) <= '0';
                SPI_BitCount(6 downto 1) <= SPI_DataLen;
                SPI_BitCount(0) <= '0';
                SPI_SCKVal <= '0';
            elsif (ClockCount="000") then
                if (SPI_BitCount(7)='0') then
                    SPI_BitCount <= NextCount;
                    SPI_SCKVal <= NextCount(0);
                end if;
            end if;
        end if;
    end process;
    process (CLK) begin
        if (CLK'event and CLK='1') then
            if (ShiftLoad='1') then
                SPI_ShiftReg <= RegWrDataVal;
                SPI_DataOutVal <= RegWrDataVal(7);
                SPI_nEnableReg <= SPInEnableReg;
                SPI_nEnableVal <= SPInEnableReg(7);
            elsif (ClockCount="000") then
                if (ShiftEnable='1') then
                    SPI_ShiftReg <= SPI_ShiftReg(6 downto 0) & SPI_DataInReg(conv_integer(SPI_InMux));
                    SPI_DataOutVal <= SPI_ShiftReg(6);
                    SPI_nEnableReg <= SPI_nEnableReg(6 downto 0) & '1';
                    SPI_nEnableVal <= SPI_nEnableReg(6);
                end if;
            end if;
            if (SPI_BitCount(7)='0' and SPI_BitCount(0)='0') then
                SPI_DataInReg <= SPIMISO & SDIODataIn;
            end if;
        end if;
    end process;
    SPICLK <= SPI_SCKVal;
    SPIMOSI <= SPI_DataOutVal;
    SDIODataOut <= SPI_DataOutVal;
    SDIOnEnable <= SPI_nEnableVal;
    
    
    --------------------------------------------------------------------------
    -- Chipscope
--    i_icon : icon
--        port map (
--            control0 => control0
--        );
--    i_ila : ila
--        port map (
--            control => control0,
--            clk => CLK,
--            trig0 => trig0
--        );
--    trig0(88 downto 0) <=
--        
--            RxEnable &          -- 1
--            RxData &            -- 8
--            TxEnableVal &       -- 1
--            TxDataVal &         -- 8
--            
--            RegWEVal &          -- 1
--            RegREVal &          -- 1
--            RegAddrVal &        -- 16
--            Command &           -- 8
--            CmdReadDataStoreFull & -- 1
--            CmdReadDataStore &  -- 8
--            
--            SPI_BitCount &      -- 8
--            ShiftLoad &         -- 1
--            ClockCount &        -- 3
--            
--            ShiftEnable &       -- 1
--            SPI_nEnableReg &    -- 8
--            SPI_ShiftReg &      -- 8
--            SPI_DataInReg &     -- 2
--            
--            SPI_SCKVal &        -- 1
--            SPIMISO &           -- 1
--            SDIODataIn &        -- 1
--            SPI_nEnableVal &    -- 1
--            SPI_DataOutVal      -- 1
--        ;
    
end arch;


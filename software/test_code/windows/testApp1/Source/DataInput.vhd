library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity DataInput is
    port (
        CLK : in std_logic;
        RST : in std_logic;
        
        TxData : out std_logic_vector(7 downto 0);
        TxEnable : out std_logic;
        TxBusy : in std_logic;
        TxBufferEmpty : in std_logic;
        RxEnable : in std_logic;
        RxData : in std_logic_vector(7 downto 0);
        RxBusy : out std_logic;
        
        ADCClockIn : in std_logic_vector(1 downto 0);
        ADCDataIn : in std_logic_vector(15 downto 0);
        ADCFrameIn : in std_logic_vector(1 downto 0);
        
        TimeStamp : in std_logic_vector(63 downto 0);
        
        Detection : out std_logic;
        DetectionStretched : out std_logic;
        
        RegWE : in std_logic;
        RegRE : in std_logic;
        RegAddr : in std_logic_vector(15 downto 0);
        RegWriteData : in std_logic_vector(7 downto 0);
        RegReadValid : out std_logic;
        RegReadData : out std_logic_vector(7 downto 0)
    );
end DataInput;

architecture arch of DataInput is

    -- Declare signals
    signal nADCClockIn : std_logic_vector(1 downto 0);
    signal ADCFrameInRegP : std_logic_vector(1 downto 0);
    signal ADCFrameInRegN : std_logic_vector(1 downto 0);
    signal LastFrameIn : std_logic_vector(1 downto 0);
    type ADC_DATA_OUT_TYPE is array (0 to 15) of std_logic_vector(13 downto 0);
    signal ADCFIFODataOut : ADC_DATA_OUT_TYPE;
    signal ADCFIFOEmpty : std_logic_vector(15 downto 0);
    signal ADCFIFORE : std_logic_vector(15 downto 0);
    signal ADCWordValid : std_logic_vector(1 downto 0);
    
    signal RegWEVal : std_logic_vector(15 downto 0);
    signal RegREVal : std_logic_vector(15 downto 0);
    type READ_DATA_TYPE is array (0 to 15) of std_logic_vector(7 downto 0);
    signal RegReadDataVal : READ_DATA_TYPE;
    signal RegReadValidVal : std_logic_vector(15 downto 0);
    
    type STATE_TYPE is (IDLE, TX_HEADER, TX_PAYLOAD, TX_FOOTER);
    signal CurrentState : STATE_TYPE;
    signal ArbChannel : std_logic_vector(3 downto 0);
    signal OffsetCount : std_logic_vector(3 downto 0);
    signal LastEscape : std_logic;
    signal TxEnableVal : std_logic;
    signal TxDataVal : std_logic_vector(7 downto 0);
    
    signal NetReq : std_logic_vector(15 downto 0);
    signal NetBusy : std_logic_vector(15 downto 0);
    type NET_DATA_TYPE is array (0 to 15) of std_logic_vector(7 downto 0);
    signal NetData : NET_DATA_TYPE;
    signal NetDataWE : std_logic_vector(15 downto 0);
    signal NetRegFull : std_logic;
    signal NetRegRE : std_logic;
    signal NetReg : std_logic_vector(7 downto 0);
    
    signal DetectionVal : std_logic_vector(15 downto 0);
    signal DetectionStretchCount : std_logic_vector(26 downto 0);

    -- Chipscope
    component icon is
        port (
            control0 : inout std_logic_vector(35 downto 0);
            control1 : inout std_logic_vector(35 downto 0)
        );
    end component;
    component ila is
        port (
            control : inout std_logic_vector(35 downto 0);
            clk : in std_logic;
            trig0 : in std_logic_vector(127 downto 0)
        );
    end component;
    component vio is
        port (
            control : inout std_logic_vector(35 downto 0);
            clk : in std_logic;
            sync_out : out std_logic_vector(7 downto 0)
        );
    end component;
    signal control0 : std_logic_vector(35 downto 0);
    signal control1 : std_logic_vector(35 downto 0);
    signal trig0 : std_logic_vector(127 downto 0);
    signal DebugChan : std_logic_vector(7 downto 0);
--    type Debug_type is array(0 to 15) of std_logic_vector(1 downto 0);
--    signal Debug : Debug_type;
--    signal SRF : std_logic_vector(31 downto 0);
--    signal SRD : std_logic_vector(31 downto 0);
--    signal SRFVal : std_logic_vector(31 downto 0);
--    signal SRDVal : std_logic_vector(31 downto 0);
--    signal FWE : std_logic;
--    signal FRE : std_logic;
--    signal DWE : std_logic;
--    signal DRE : std_logic;
--    signal FEmpty : std_logic;
--    signal DEmpty : std_logic;
    
begin

    -- Register frame inputs
    nADCClockIn <= not ADCClockIn;
    FrameCopy:
        for g in 0 to 1 generate
        begin
            FramePNInst: IDDR2
                generic map(
                    DDR_ALIGNMENT => "NONE",
                    INIT_Q0 => '0',
                    INIT_Q1 => '0',
                    SRTYPE => "ASYNC")
                port map (
                    Q0 => ADCFrameInRegP(g),
                    Q1 => ADCFrameInRegN(g),
                    C0 => ADCClockIn(g),
                    C1 => nADCClockIn(g),
                    CE => '1',
                    D => ADCFrameIn(g),
                    R => RST,
                    S => '0'
                );
        end generate;
        
    -- Instantiate FIFOs from ADC to main clock domain
    DataInCopy:
        for g in 0 to 15 generate
        begin
            DataInFIFOInst : entity work.DataInFIFO
                port map (
                    RST => RST,
                    
                    ADCClockIn => ADCClockIn(g/8),
                    ADCDataIn => ADCDataIn(g),
                    ADCWordValid => ADCWordValid(g/8),
                    
                    CLK => CLK,
                    DataOutEmpty => ADCFIFOEmpty(g),
                    DataOutRE => ADCFIFORE(g),
                    DataOut => ADCFIFODataOut(g)
                    
                    --, Debug => Debug(g)
                );
        end generate;

    -- Use frame signal to mark when complete word is received from ADC
    process (ADCClockIn(0)) begin
        if (ADCClockIn(0)'event and ADCClockIn(0)='1') then
            LastFrameIn(0) <= ADCFrameInRegN(0);
            ADCWordValid(0) <= not LastFrameIn(0) and ADCFrameInRegP(0);
        end if;
    end process;
    process (ADCClockIn(1)) begin
        if (ADCClockIn(1)'event and ADCClockIn(1)='1') then
            LastFrameIn(1) <= ADCFrameInRegN(1);
            ADCWordValid(1) <= not LastFrameIn(1) and ADCFrameInRegP(1);
        end if;
    end process;
        
    -- Instantiate trigger processing units    
    TriggerUnitCopy:
        for g in 0 to 15 generate
        begin
            TriggerUnitInst : entity work.TriggerUnit
                generic map (
                    INDEX => g
                )
                port map (
                    RST => RST,
                    CLK => CLK,
                    
                    -- Data from ADC
                    DataIn => ADCFIFODataOut(g),
                    DataInEmpty => ADCFIFOEmpty(g),
                    DataInRE => ADCFIFORE(g),
                    
                    -- Control register accesss
                    RegWE => RegWEVal(g),
                    RegRE => RegREVal(g),
                    RegAddr => RegAddr(15 downto 4),
                    RegWriteData => RegWriteData,
                    RegReadValid => RegReadValidVal(g),
                    RegReadData => RegReadDataVal(g),

                    -- Current time
                    TimeStamp => TimeStamp,
                    
                    -- Detection output
                    Detection => DetectionVal(g),

                    -- Network output
                    NetReq => NetReq(g),
                    NetBusy => NetBusy(g),
                    NetData => NetData(g),
                    NetDataWE => NetDataWE(g)
                );
        end generate;
    Detection <= '1' when DetectionVal/=X"0000" else '0';
    process (RST, CLK) begin
        if (RST='1') then
            DetectionStretched <= '0';
            DetectionStretchCount <= (others=>'1');
        elsif (CLK'event and CLK='1') then
            if (DetectionVal/=X"0000") then
                DetectionStretchCount <= (others=>'0');
            elsif (DetectionStretchCount(26)/='1') then
                DetectionStretchCount <= DetectionStretchCount + 1;
            end if;
            DetectionStretched <= not DetectionStretchCount(26);
        end if;
    end process;
        
    -- Mux register accesses to trigger units
    process (RegWE, RegAddr, RegWE, RegRE, RegReadValidVal, RegReadDataVal) begin
        RegReadData <= (others=>'0');
        for i in 0 to 15 loop
            if (RegAddr(3 downto 0)=conv_std_logic_vector(i, 4)) then
                RegWEVal(i) <= RegWE;
                RegREVal(i) <= RegRE;
            else 
                RegWEVal(i) <= '0';
                RegREVal(i) <= '0';
            end if;
            if (RegReadValidVal(i)='1') then
                RegReadData <= RegReadDataVal(i);
            end if;
        end loop;
    end process;
    RegReadValid <= '1' when RegReadValidVal/=X"0000" else '0';

    -- Multiplex trigger unit outputs to single register
    -- This is used to improve timing across the 16-1 mux
    process (RST, CLK) begin
        if (RST='1') then
            NetRegFull <= '0';
            NetReg <= (others=>'0');
        elsif (CLK'event and CLK='1') then
            if (NetDataWE/=X"0000") then
                NetRegFull <= '1';
                NetReg <= NetData(conv_integer(ArbChannel));
            elsif (NetRegRE='1') then
                NetRegFull <= '0';
            end if;
        end if;
    end process;
    
    -- Multiplex trigger unit outputs to network
    process (RST, CLK) begin
        if (RST='1') then
            CurrentState <= IDLE;
            ArbChannel <= (others=>'0');
            OffsetCount <= (others=>'0');
            LastEscape <= '0';
        elsif (CLK'event and CLK='1') then
            case CurrentState is
                when IDLE =>
                    if (NetReq(conv_integer(ArbChannel))='1') then
                        CurrentState <= TX_HEADER;
                        OffsetCount <= (others=>'0');
                    else
                        ArbChannel <= ArbChannel + 1;
                    end if;
    
                when TX_HEADER =>
                    if (TxBusy='0') then
                        OffsetCount <= OffsetCount + 1;
                        if (OffsetCount=X"1") then
                            CurrentState <= TX_PAYLOAD;
                        end if;
                    end if;
                    
                when TX_PAYLOAD =>
                    if (TxEnableVal='1') then
                        -- Byte stuffing
                        if (LastEscape='1') then
                            LastEscape <= '0';
                        elsif (TxDataVal=X"10") then
                            LastEscape <= '1';
                        end if;
                    end if;
                    if (NetReq(conv_integer(ArbChannel))='0' and NetRegFull='0') then
                        OffsetCount <= (others=>'0');
                        CurrentState <= TX_FOOTER;
                    end if;
                    
                when TX_FOOTER =>
                    if (TxBusy='0') then
                        OffsetCount <= OffsetCount + 1;
                        if (OffsetCount=X"1") then
                            CurrentState <= IDLE;
                        end if;
                    end if;
                    
                when others => null;
            end case;
        end if;
    end process;

    -- Generate combinatorial signals from state machine
    process (CurrentState, OffsetCount, TxBusy, NetData, NetDataWE) begin
        TxDataVal <= (others=>'0');
        TxEnableVal <= '0';
        NetBusy <= X"FFFF";
        NetRegRE <= '0';
        
        case CurrentState is
            when TX_HEADER =>
                case OffsetCount is
                    when X"0" => TxDataVal <= X"10";   -- SOF
                    when others => TxDataVal <= X"01";
                end case;
                TxEnableVal <= not TxBusy;
            
            when TX_PAYLOAD =>
                NetBusy(conv_integer(ArbChannel)) <= NetRegFull and (TxBusy or LastEscape);
                if (LastEscape='1') then
                    -- Byte stuffing
                    TxDataVal <= X"10";
                    TxEnableVal <= not TxBusy;
                else
                    TxDataVal <= NetReg;
                    TxEnableVal <= not TxBusy and NetRegFull;
                    NetRegRE <= not TxBusy and NetRegFull;
                end if;
                
            when TX_FOOTER =>
                case OffsetCount is
                    when X"0" => TxDataVal <= X"10";   -- EOF
                    when others => TxDataVal <= X"02";
                end case;
                TxEnableVal <= not TxBusy;
                
            when others => null;
        end case;
    end process;
    TxEnable <= TxEnableVal;
    TxData <= TxDataVal;
    
    -- We don't receive any data from the network
    --RxEnable : in std_logic;
    --RxData : in std_logic_vector(7 downto 0);
    RxBusy <= '1';


    
    --------------------------------------------------------------------------
    -- Chipscope
    i_icon : icon
        port map (
            control0 => control0,
            control1 => control1
        );
    i_ila : ila
        port map (
            control => control0,
            clk => CLK,
            trig0 => trig0
        );
    i_vio : vio
        port map (
            control => control1,
            clk => CLK,
            sync_out => DebugChan
        );
--    trig0(90 downto 0) <=
--    
--            FRE &   -- 1
--            DRE &   -- 1
--            SRFVal &   -- 32
--            SRDVal &    -- 32
--            
    trig0(24 downto 0) <=
            DebugChan(3 downto 0) & --4
            DetectionVal(conv_integer(DebugChan(3 downto 0))) &      -- 1
            
            ADCFIFOEmpty(conv_integer(DebugChan(3 downto 0))) &   -- 1
            ADCFIFORE(conv_integer(DebugChan(3 downto 0))) &      -- 1
            ADCFIFODataOut(conv_integer(DebugChan(3 downto 0))) & -- 14
        
            ADCWordValid(conv_integer(DebugChan(3 downto 0))) &   -- 1
            ADCClockIn(conv_integer(DebugChan(3))) &   -- 1
            ADCFrameIn(conv_integer(DebugChan(3))) &   -- 1
            ADCDataIn(conv_integer(DebugChan(3 downto 0)))      -- 1
        ;
--    process (ADCClockIn(0)) begin
--        if (ADCClockIn(0)'event and ADCClockIn(0)='1') then
--            SRF <= SRF(29 downto 0) & ADCFrameInRegP(0) & ADCFrameInRegN(0);
--            SRD <= SRD(29 downto 0) & Debug(0);
--        end if;
--    end process;
--    FWE <= (not SRF(14) and SRF(13)) or
--           (not SRF(15) and SRF(14));
--    FIFOFInst : entity work.FIFO
--        port map (
--            wr_clk => ADCClockIn(0),
--            wr_en => FWE,
--            din => SRF,
--            rd_clk => CLK,
--            rd_en => FRE,
--            empty => FEmpty,
--            dout => SRFVal
--        );
--    FRE <= not FEmpty;
--    FIFODInst : entity work.FIFO
--        port map (
--            wr_clk => ADCClockIn(0),
--            wr_en => FWE,
--            din => SRD,
--            rd_clk => CLK,
--            rd_en => DRE,
--            empty => DEmpty,
--            dout => SRDVal
--        );
--    DRE <= not DEmpty;
    
end arch;


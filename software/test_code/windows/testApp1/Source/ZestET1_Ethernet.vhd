-- ZestET1 Ethernet interface physical layer
--   File name: ZestET1_Ethernet.vhd
--   Version: 1.50
--   Date: 25/11/2013
--
--   Copyright (C) 2013 Orange Tree Technologies Ltd. All rights reserved.
--   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and
--   modify this logic core in any form including but not limited to VHDL source code or
--   EDIF netlist in FPGA designs that target the ZestET1.
--   Orange Tree Technologies prohibits the use of this logic core or any modification of
--   it in any form including but not limited to VHDL source code or EDIF netlist in
--   FPGA or ASIC designs that target any other hardware unless the purchaser of the
--   ZestET1 has purchased the appropriate licence from Orange Tree Technologies.
--   Contact Orange Tree Technologies if you want to purchase such a licence.
--
--  *****************************************************************************************
--  **
--  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
--  **              provided to you "as is". Orange Tree Technologies and its licensors 
--  **              make and you receive no warranties or conditions, express, implied, 
--  **              statutory or otherwise, and Orange Tree Technologies specifically 
--  **              disclaims any implied warranties of merchantability, non-infringement,
--  **              or fitness for a particular purpose. Orange Tree Technologies does not
--  **              warrant that the functions contained in these designs will meet your 
--  **              requirements, or that the operation of these designs will be 
--  **              uninterrupted or error free, or that defects in the Designs will be 
--  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
--  **              make any representations regarding use or the results of the use of the 
--  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
--  **
--  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
--  **              or its licensors be liable for any loss of data, lost profits, cost or 
--  **              procurement of substitute goods or services, or for any special, 
--  **              incidental, consequential, or indirect damages arising from the use or 
--  **              operation of the designs or accompanying documentation, however caused 
--  **              and on any theory of liability. This limitation will apply even if 
--  **              Orange Tree Technologies has been advised of the possibility of such 
--  **              damage. This limitation shall apply notwithstanding the failure of the 
--  **              essential purpose of any limited remedies herein.
--  **
--  *****************************************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity ZestET1_Ethernet is
    generic (
        CLOCK_RATE : integer := 125000000
    );
    port (
        User_CLK : in std_logic;
        User_RST : in std_logic;

        -- User interface
        User_WE : in std_logic;
        User_RE : in std_logic;
        User_Addr : in std_logic_vector(4 downto 0);
        User_WriteData : in std_logic_vector(15 downto 0);
        User_Owner : in std_logic_vector(7 downto 0);
        User_BE : in std_logic_vector(1 downto 0);
        User_ReadData : out std_logic_vector(15 downto 0);
        User_ReadDataValid : out std_logic;
        User_ValidOwner : out std_logic_vector(7 downto 0);
        User_Interrupt : out std_logic;
        
        -- Interface to GigExpedite
        Eth_Clk : out std_logic;
        Eth_CSn : out std_logic;
        Eth_WEn : out std_logic;
        Eth_A : out std_logic_vector(4 downto 0);
        Eth_D : inout std_logic_vector(15 downto 0);
        Eth_BE : out std_logic_vector(1 downto 0);
        Eth_Intn : in std_logic
    );
end ZestET1_Ethernet;

architecture arch of ZestET1_Ethernet is

    -- Declare signals
    signal UserDataDelay3 : std_logic_vector(15 downto 0);
    signal UserDataDelay2 : std_logic_vector(15 downto 0);
    signal UserDataDelay1 : std_logic_vector(15 downto 0);
    signal UserWEDelay3 : std_logic;
    signal UserWEDelay2 : std_logic;
    signal UserWEDelay1 : std_logic;

    signal RegUser_Addr : std_logic_vector(4 downto 0);
    signal RegUser_CS : std_logic;
    signal RegUser_WE : std_logic;
    signal RegUser_RE : std_logic;
    signal RegUser_BE : std_logic_vector(1 downto 0);
    
    signal Eth_DVal : std_logic_vector(15 downto 0);
    signal Eth_TSVal : std_logic_vector(15 downto 0);
    signal Eth_DTSVal : std_logic_vector(15 downto 0);

    signal Eth_CSnVal : std_logic;
    signal Eth_WEnVal : std_logic;
    signal Eth_AVal : std_logic_vector(4 downto 0);
    signal Eth_BEVal : std_logic_vector(1 downto 0);

    signal nUser_Clk : std_logic;
    signal RegEth_DP : std_logic_vector(15 downto 0);
    signal RegEth_DN : std_logic_vector(15 downto 0);
    signal User_ReadDataP : std_logic_vector(15 downto 0);
    signal User_ReadDataN : std_logic_vector(15 downto 0);

    signal ReadDelay : std_logic_vector(4 downto 0);
    type OWNER_TYPE is array(0 to 5) of std_logic_vector(7 downto 0);
    signal Owner : OWNER_TYPE;

    attribute IOB : string;
    attribute IOB of CSnInst : label is "FORCE";
    attribute IOB of WEnInst : label is "FORCE";

begin

    -- Output clock with DDR FF to give predictable timing
    nUser_CLK <= not User_CLK;
    ClockOutInst : ODDR2
        generic map (
            DDR_ALIGNMENT => "NONE",
            INIT => '0',
            SRTYPE => "SYNC"
        )
        port map (
            Q => Eth_Clk,
            C0 => User_CLK,
            C1 => nUser_CLK,
            CE => '1',
            D0 => '0',
            D1 => '1',
            R => '0',
            S => '0'
        );

    -- Data outputs
    -- This makes the data/control change on the falling edge of the clock
    -- at the GigExpedite to ensure setup/hold times
    process (User_CLK)
    begin
        if (User_CLK'event and User_CLK='1') then
            UserDataDelay3 <= UserDataDelay2;
            UserDataDelay2 <= UserDataDelay1;
            UserDataDelay1 <= User_WriteData;
            UserWEDelay3 <= UserWEDelay2;
            UserWEDelay2 <= UserWEDelay1;
            UserWEDelay1 <= not User_WE;
        end if;
    end process;

    DataOutCopy:
        for g in 0 to 15 generate
            attribute IOB of DataOutCopyInst : label is "FORCE";
        begin
            DataOutCopyInst : FDCE
                generic map (
                    INIT => '0'
                )
                port map (
                    Q => Eth_DVal(g),
                    C => User_CLK,
                    CE => '1',
                    D => UserDataDelay3(g),
                    CLR => '0'
                );
        end generate;
			
    DataTSCopy:
        for g in 0 to 15 generate
            attribute IOB of DataTSCopyInst : label is "FORCE";
        begin
            DataTSCopyInst : FDCE
                generic map (
                    INIT => '0'
                )
                port map (
                    Q => Eth_TSVal(g),
                    C => User_CLK,
                    CE => '1',
                    D => UserWEDelay3,
                    CLR => '0'
                );
        end generate;
	
    -- Tristate buffers on data output
    DataTS:
        for g in 0 to 15 generate
        begin
            Eth_DTSVal(g) <= Eth_DVal(g) when Eth_TSVal(g)='0' else 'Z';
        end generate;
    Eth_D <= Eth_DTSVal;
    
    -- Address and control outputs
    -- This makes the data/control change on the falling edge of the clock
    -- at the GigExpedite to ensure setup/hold times
    process (User_CLK)
    begin
        if (User_CLK'event and User_CLK='1') then
            RegUser_CS <= not (User_WE or User_RE);
            RegUser_BE <= not User_BE;
            RegUser_WE <= not User_WE;
            RegUser_RE <= not User_RE;
            RegUser_Addr <= User_Addr;
        end if;
    end process;
    
    CSnInst : FDCE
        generic map (
            INIT => '1'
        )
        port map (
            Q => Eth_CSnVal,
            C => User_CLK,
            CE => '1',
            D => RegUser_CS,
            CLR => '0'
        );

    WEnInst : FDCE
        generic map (
            INIT => '1'
        )
        port map (
            Q => Eth_WEnVal,
            C => User_CLK,
            CE => '1',
            D => RegUser_WE,
            CLR => '0'
        );

    AddrCopy:
        for g in 0 to 4 generate
            attribute IOB of AddrCopyInst : label is "FORCE";
        begin
            AddrCopyInst : FDCE
                generic map (
                    INIT => '0'
                )
                port map (
                    Q => Eth_AVal(g),
                    C => User_CLK,
                    CE => '1',
                    D => RegUser_Addr(g),
                    CLR => '0'
                );
        end generate;

    BECopy:
        for g in 0 to 1 generate
            attribute IOB of BECopyInst : label is "FORCE";
        begin
            BECopyInst : FDCE
                generic map (
                    INIT => '0'
                )
                port map (
                    Q => Eth_BEVal(g),
                    C => User_CLK,
                    CE => '1',
                    D => RegUser_BE(g),
                    CLR => '0'
                );
        end generate;

    Eth_CSn <= Eth_CSnVal;
    Eth_WEn <= Eth_WEnVal;
    Eth_A <= Eth_AVal;
    Eth_BE <= Eth_BEVal;
    
    -- Read path
    -- Use DDR FF to get rising and falling edge registers
    DataInCopy:
        for g in 0 to 15 generate
        begin
            DataInCopyInst : IDDR2
                generic map(
                    DDR_ALIGNMENT => "NONE",
                    INIT_Q0 => '0',
                    INIT_Q1 => '0',
                    SRTYPE => "SYNC"
                )
                port map (
                    Q0 => RegEth_DP(g),
                    Q1 => RegEth_DN(g),
                    C0 => User_Clk,
                    C1 => nUser_Clk,
                    CE => '1',
                    D => Eth_D(g),
                    R => '0',
                    S => '0'
                );
        end generate;

    -- Re-register to user clock
    DataInPCopy:
        for g in 0 to 15 generate
        begin
            DataInPCopyInst : FD
                port map (
                    D => RegEth_DP(g),
                    C => User_CLK,
                    Q => User_ReadDataP(g)
                );
        end generate;
    DataInNCopy:
        for g in 0 to 15 generate
        begin
            DataInPCopyInst : FD
                port map (
                    D => RegEth_DN(g),
                    C => User_CLK,
                    Q => User_ReadDataN(g)
                );
        end generate;

    -- Generate delayed valid and owner signals to match read data
    process (User_CLK)
    begin
        if (User_CLK'event and User_CLK='1') then
            ReadDelay <= ReadDelay(3 downto 0) & not RegUser_RE;
            Owner(5) <= Owner(4);
            Owner(4) <= Owner(3);
            Owner(3) <= Owner(2);
            Owner(2) <= Owner(1);
            Owner(1) <= Owner(0);
            Owner(0) <= User_Owner;
        end if;
    end process;
    
    -- Select read data path on clock rate
    -- Since the delays to/from the GigExpedite are fixed, the read sample
    -- clock edge will change with the clock rate changing
    LowClockRate: if (CLOCK_RATE<40000000) generate
        begin
            User_ReadData <= User_ReadDataP;
            User_ReadDataValid <= ReadDelay(3);
            User_ValidOwner <= Owner(4);
        end generate;
    MidClockRate: if (CLOCK_RATE>=40000000 and CLOCK_RATE<80000000) generate
        begin
            User_ReadData <= User_ReadDataN;
            User_ReadDataValid <= ReadDelay(3);
            User_ValidOwner <= Owner(4);
        end generate;
    HighClockRate: if (CLOCK_RATE>=80000000) generate
        begin
            User_ReadData <= User_ReadDataP;
            User_ReadDataValid <= ReadDelay(4);
            User_ValidOwner <= Owner(5);
        end generate;

    -- Register interrupt line from GigExpedite
    process (User_CLK)
    begin
        if (User_CLK'event and User_CLK='1') then
            User_Interrupt <= not Eth_Intn;
        end if;
    end process;

end arch;

--
-- Data trigger and processing unit for one channel
--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity TriggerUnit is
    generic (
        INDEX : integer := 0
    );
    port (
        RST : in std_logic;
        CLK : in std_logic;
        
        -- ADC input data
        DataIn : in std_logic_vector(13 downto 0);
        DataInEmpty : in std_logic;
        DataInRE : out std_logic;
        
        -- Control register accesss
        RegWE : in std_logic;
        RegRE : in std_logic;
        RegAddr : in std_logic_vector(11 downto 0);
        RegWriteData : in std_logic_vector(7 downto 0);
        RegReadValid : out std_logic;
        RegReadData : out std_logic_vector(7 downto 0);

        -- Current time
        TimeStamp : in std_logic_vector(63 downto 0);
        
        -- Detector output
        Detection : out std_logic;
        
        -- Network output
        NetReq : out std_logic;
        NetBusy : in std_logic;
        NetData : out std_logic_vector(7 downto 0);
        NetDataWE : out std_logic
        
    );
end TriggerUnit;

architecture arch of TriggerUnit is

    -- Declare signals
    type BUF_TYPE is array (0 to 1023) of std_logic_vector(13 downto 0);
    signal DataBuffer : BUF_TYPE;
    signal PortAAddr : std_logic_vector(9 downto 0);
    signal BufferFull : std_logic;
    signal DataInREVal : std_logic;
    
    signal PortToggle : std_logic;
    signal WriteCount : std_logic_vector(9 downto 0);
    signal RegWriteCount : std_logic_vector(9 downto 0);
    signal NextWriteCount : std_logic_vector(9 downto 0);
    signal ReadCount : std_logic_vector(9 downto 0);

    signal CalcAddr : std_logic_vector(9 downto 0);
    signal CalcReadData : std_logic_vector(13 downto 0);
    signal CalcLength : std_logic_vector(9 downto 0);
    signal CalcPeak : std_logic_vector(13 downto 0);
    signal CalcArea : std_logic_vector(23 downto 0);
    signal PeakThreshold : std_logic_vector(13 downto 0);
    signal AreaThreshold : std_logic_vector(23 downto 0);
    signal CalcBufferEmpty : std_logic;
    signal DetectionVal : std_logic;
    
    signal NetAddr : std_logic_vector(9 downto 0);
    signal NetReadData : std_logic_vector(13 downto 0);
    signal NetReqVal : std_logic;
    signal NetBufferEmpty : std_logic;
    
    signal EnableTrigger : std_logic;
    type TRIG_FIFO_TYPE is array (0 to 15) of std_logic_vector(9 downto 0);
    signal TrigFIFO : TRIG_FIFO_TYPE;
    type TRIG_TIME_FIFO_TYPE is array (0 to 15) of std_logic_vector(63 downto 0);
    signal TrigTimeFIFO : TRIG_TIME_FIFO_TYPE;
    signal TrigFIFONetDataOut : std_logic_vector(9 downto 0);
    signal TrigFIFOCalcDataOut : std_logic_vector(9 downto 0);
    signal TrigTimeFIFONetDataOut : std_logic_vector(63 downto 0);
    signal TrigFIFOWriteCount : std_logic_vector(3 downto 0);
    signal TrigFIFONextWriteCount : std_logic_vector(3 downto 0);
    signal TrigFIFONetReadCount : std_logic_vector(3 downto 0);
    signal TrigFIFOCalcReadCount : std_logic_vector(3 downto 0);
    signal TrigFIFONetEmpty : std_logic;
    signal TrigFIFOCalcEmpty : std_logic;
    signal TrigFIFOFull : std_logic;
    signal TrigFIFONetRE : std_logic;
    signal TrigFIFOCalcRE : std_logic;
    signal Trigger : std_logic;
    signal TrigLevel : std_logic_vector(13 downto 0);
    signal TrigHoldOff : std_logic;
    signal TrigHoldOffEnd : std_logic_vector(9 downto 0);
    signal TrigHoldOffLength : std_logic_vector(9 downto 0);
    
    signal PreCalcSamples : std_logic_vector(9 downto 0);
    signal PostCalcSamples : std_logic_vector(9 downto 0);
    signal TotalCalcSamples : std_logic_vector(9 downto 0);
    signal PreNetSamples : std_logic_vector(9 downto 0);
    signal PostNetSamples : std_logic_vector(9 downto 0);
    signal TotalNetSamples : std_logic_vector(9 downto 0);

    signal TxLength : std_logic_vector(10 downto 0);
    signal TxAddr : std_logic_vector(9 downto 0);
    signal TxAddrPlus1 : std_logic_vector(9 downto 0);
    signal TxOffset : std_logic_vector(3 downto 0);
    type TX_STATE_TYPE is (IDLE, TX_HEADER, TX_PAYLOAD, TX_FOOTER);
    signal TxState : TX_STATE_TYPE;
    
    type CALC_FIFO_TYPE is array (0 to 15) of std_logic_vector(38 downto 0);
    signal CalcFIFO : CALC_FIFO_TYPE;
    attribute RAM_STYLE : string;
    attribute RAM_STYLE of CalcFIFO : signal is "DISTRIBUTED";
    signal CalcFIFODataIn : std_logic_vector(38 downto 0);
    signal CalcFIFODataOut : std_logic_vector(38 downto 0);
    signal CalcFIFOWriteCount : std_logic_vector(3 downto 0);
    signal CalcFIFOReadCount : std_logic_vector(3 downto 0);
    signal NextCalcFIFOReadCount : std_logic_vector(3 downto 0);
    signal CalcFIFOAddr : std_logic_vector(3 downto 0);
    signal CalcFIFORE : std_logic;
    signal CalcFIFOWE : std_logic;
    signal CalcFIFOEmpty : std_logic;
    
    type OUT_FIFO_TYPE is array(0 to 15) of std_logic_vector(7 downto 0);
    signal OutFIFO : OUT_FIFO_TYPE;
    signal OutFIFOBytes : std_logic_vector(3 downto 0);
    signal OutFIFOWriteCount : std_logic_vector(3 downto 0);
    signal NextOutFIFOWriteCount : std_logic_vector(3 downto 0);
    signal OutFIFOReadCount : std_logic_vector(3 downto 0);
    signal OutFIFODataIn : std_logic_vector(7 downto 0);
    signal OutFIFOWE : std_logic;
    signal OutFIFODataOut : std_logic_vector(7 downto 0);
    signal OutFIFORE : std_logic;
    signal OutFIFOFull : std_logic;
    signal OutFIFOEmpty : std_logic;

    -- Chipscope
    component icon is
        port (
            control0 : inout std_logic_vector(35 downto 0)
        );
    end component;
    component ila is
        port (
            control : inout std_logic_vector(35 downto 0);
            clk : in std_logic;
            trig0 : in std_logic_vector(127 downto 0)
        );
    end component;
    signal control0 : std_logic_vector(35 downto 0);
    signal trig0 : std_logic_vector(127 downto 0);
    
begin

    --------------------------------------------------------------------------
    -- Infer block RAM buffer
    -- Implements a circular buffer of samples from the ADC
    process (CLK) begin
        if (CLK'event and CLK='1') then
            if (DataInREVal='1') then
                DataBuffer(conv_integer(PortAAddr)) <= DataIn;
            end if;
            CalcReadData <= DataBuffer(conv_integer(PortAAddr));
            NetReadData <= DataBuffer(conv_integer(NetAddr));
        end if;
    end process;
    DataInRE <= DataInREVal;
        
    --------------------------------------------------------------------------
    -- Buffer has a one port shared between the ADC and the calculation port
    -- Write incoming data to the buffer on alternating cycles
    -- (Main clock is faster than twice sample rate)
    process (RST, CLK) begin
        if (RST='1') then
            PortToggle <= '0';
        elsif (CLK'event and CLK='1') then
            PortToggle <= not PortToggle;
        end if;
    end process;
    PortAAddr <= WriteCount when PortToggle='0' else CalcAddr;
    
    --------------------------------------------------------------------------
    -- Copy from ADC into circular buffer
    -- FIXME: Think about when buffer is full or when trigger fifo is full how to recover cleanly
    BufferFull <= '1' when NextWriteCount=ReadCount else '0';
    DataInREVal <= '1' when PortToggle='0' and DataInEmpty='0' and BufferFull='0' and TrigFIFOFull='0' else '0';
    process (RST, CLK) begin
        if (RST='1') then
            WriteCount <= (others=>'0');
            NextWriteCount <= "0000000001";
            RegWriteCount <= (others=>'0');
            ReadCount <= (others=>'0');
        elsif (CLK'event and CLK='1') then
            if (DataInREVal='1') then
                WriteCount <= NextWriteCount;
                NextWriteCount <= NextWriteCount + 1;
            end if;
            RegWriteCount <= WriteCount;
            
            if (PreCalcSamples>PreNetSamples) then
                ReadCount <= WriteCount-PreCalcSamples;
            else
                ReadCount <= WriteCount-PreNetSamples;
            end if;
        end if;
    end process;
    
    --------------------------------------------------------------------------
    -- FIFO of triggers detected
    -- The FIFO allows multiple triggers to be queued for processing in case the network is busy
    process (RST, CLK) begin
        if (RST='1') then
            TrigFIFOWriteCount <= (others=>'0');
            TrigFIFONetReadCount <= (others=>'0');
            TrigFIFOCalcReadCount <= (others=>'0');
        elsif (CLK'event and CLK='1') then
            if (Trigger='1') then
                TrigFIFOWriteCount <= TrigFIFONextWriteCount;
            end if;
            if (TrigFIFONetRE='1') then
                TrigFIFONetReadCount <= TrigFIFONetReadCount + 1;
            end if;
            if (TrigFIFOCalcRE='1') then
                TrigFIFOCalcReadCount <= TrigFIFOCalcReadCount + 1;
            end if;
        end if;
    end process;
    process (CLK) begin
        if (CLK'event and CLK='1') then
            if (Trigger='1') then
                TrigFIFO(conv_integer(TrigFIFOWriteCount)) <= WriteCount;
                TrigTimeFIFO(conv_integer(TrigFIFOWriteCount)) <= TimeStamp;
            end if;
        end if;
    end process;
    TrigFIFONextWriteCount <= TrigFIFOWriteCount + 1;
    TrigFIFOFull <= '1' when TrigFIFONextWriteCount=TrigFIFONetReadCount or 
                             TrigFIFONextWriteCount=TrigFIFOCalcReadCount else '0';
    TrigFIFONetEmpty <= '1' when TrigFIFONetReadCount=TrigFIFOWriteCount else '0';
    TrigFIFOCalcEmpty <= '1' when TrigFIFOCalcReadCount=TrigFIFOWriteCount else '0';
    TrigFIFONetDataOut <= TrigFIFO(conv_integer(TrigFIFONetReadCount));
    TrigFIFOCalcDataOut <= TrigFIFO(conv_integer(TrigFIFOCalcReadCount));
    TrigTimeFIFONetDataOut <= TrigTimeFIFO(conv_integer(TrigFIFONetReadCount));
    
    --------------------------------------------------------------------------
    -- Trigger detection
    -- Also, hold off triggering while capturing post-trigger samples
    Trigger <= '1' when EnableTrigger='1' and DataInREVal='1' and DataIn>=TrigLevel and TrigHoldOff='0' else '0';
    process (RST, CLK) begin
        if (RST='1') then
            TrigHoldOff <= '0';
            TrigHoldOffEnd <= (others=>'0');
            TrigHoldOffLength <= (others=>'0');
        elsif (CLK'event and CLK='1') then
            if (PostCalcSamples>PostNetSamples) then
                TrigHoldOffLength <= PostCalcSamples - 1;
            else
                TrigHoldOffLength <= PostNetSamples - 1;
            end if;
            
            if (Trigger='1') then
                TrigHoldOff <= '1';
                TrigHoldOffEnd <= WriteCount + TrigHoldOffLength;
            elsif (DataInREVal='1' and WriteCount=TrigHoldOffEnd) then
                TrigHoldOff <= '0';
            end if;
        end if;
    end process;
    
    --------------------------------------------------------------------------
    -- Perform calculations to determine whether neutron detected
    -- This finds the peak value in the window and the sum of the samples in the window
    process (RST, CLK) begin
        if (RST='1') then
            CalcLength <= (others=>'0');
            CalcAddr <= (others=>'0');
            CalcArea <= (others=>'0');
            CalcPeak <= (others=>'0');
            Detection <= '0';
        elsif (CLK'event and CLK='1') then
            Detection <= '0';
            if (CalcLength="00000000000") then
                if (PortToggle='0' and TrigFIFOCalcEmpty='0' and TotalCalcSamples/="0000000000") then
                    CalcAddr <= TrigFIFOCalcDataOut-PreCalcSamples;
                    CalcLength <= TotalCalcSamples+1;
                    CalcPeak <= (others=>'0');
                    CalcArea <= (others=>'0');
                end if;
            elsif (PortToggle='0' and CalcBufferEmpty='0') then
                CalcLength <= CalcLength - 1;
                CalcAddr <= CalcAddr + 1;
                CalcArea <= CalcArea + CalcReadData;
                if (CalcReadData > CalcPeak) then
                    CalcPeak <= CalcReadData;
                end if;
                if (CalcLength="00000000001") then
                    Detection <= DetectionVal;
                end if;
            end if;
        end if;
    end process;
    TotalCalcSamples <= PreCalcSamples+PostCalcSamples;
    CalcBufferEmpty <= '1' when RegWriteCount=CalcAddr else '0';
    TrigFIFOCalcRE <= '1' when PortToggle='0' and CalcBufferEmpty='0' and CalcLength="0000000001" else '0';
    
    DetectionVal <= '1' when CalcPeak<PeakThreshold and CalcArea>AreaThreshold else '0';    -- Neutron detection condition
    
    --------------------------------------------------------------------------
    -- FIFO of calculation results to hold values until network is ready
    process (RST, CLK) begin
        if (RST='1') then
            CalcFIFOWriteCount <= (others=>'0');
            CalcFIFOReadCount <= (others=>'0');
            CalcFIFOEmpty <= '1';
        elsif (CLK'event and CLK='1') then
            if (CalcFIFOWE='1') then
                CalcFIFOWriteCount <= CalcFIFOWriteCount + 1;
            end if;
            if (CalcFIFORE='1') then
                CalcFIFOReadCount <= NextCalcFIFOReadCount;
            end if;
            if (CalcFIFOWE='1') then
                CalcFIFOEmpty <= '0';
            elsif (CalcFIFORE='1' and NextCalcFIFOReadCount=CalcFIFOWriteCount) then
                CalcFIFOEmpty <= '1';
            end if;
        end if;
    end process;
    process (CLK) begin
        if (CLK'event and CLK='1') then
            if (CalcFIFOWE='1') then
                CalcFIFO(conv_integer(CalcFIFOWriteCount)) <= CalcFIFODataIn;
            end if;
            CalcFIFODataOut <= CalcFIFO(conv_integer(CalcFIFOAddr));
        end if;
    end process;
    CalcFIFOAddr <= CalcFIFOReadCount when CalcFIFORE='0' else CalcFIFOReadCount + 1;
    CalcFIFOWE <= '1' when PortToggle='0' and CalcBufferEmpty='0' and CalcLength="00000000001" else '0';
    CalcFIFODataIn <= DetectionVal & CalcPeak & CalcArea;
    NextCalcFIFOReadCount <= CalcFIFOReadCount + 1;
        
    --------------------------------------------------------------------------
    -- Send data to network in a formatted packet
    process (RST, CLK) begin
        if (RST='1') then
            TxLength <= (others=>'1');
            TxAddr <= (others=>'0');
            TxAddrPlus1 <= "0000000001";
            NetReqVal <= '0';
            TxOffset <= (others=>'0');
            TxState <= IDLE;
        elsif (CLK'event and CLK='1') then
            
            case TxState is
                when IDLE =>
                    -- Wait for trigger
                    if (OutFIFOEmpty='1' and TrigFIFONetEmpty='0') then
                        TxOffset <= (others=>'0');
                        NetReqVal <= '1';
                        TxState <= TX_HEADER;
                    end if;
                
                when TX_HEADER =>
                    -- Send packet header
                    if (OutFIFOWE='1') then
                        TxOffset <= TxOffset+1;
                        if (TxOffset=X"b") then
                            if (TotalNetSamples/="0000000000") then
                                TxAddr <= TrigFIFONetDataOut-PreNetSamples;
                                TxAddrPlus1 <= TrigFIFONetDataOut-PreNetSamples+1;
                                TxLength(10 downto 1) <= TotalNetSamples-1;
                                TxLength(0) <= '1';
                                TxOffset <= (others=>'0');
                                TxState <= TX_PAYLOAD;
                            else
                                TxOffset <= (others=>'0');
                                TxState <= TX_FOOTER;
                            end if;
                        end if;
                    end if;
                
                when TX_PAYLOAD =>
                    -- Send packet payload
                    if (OutFIFOWE='1') then
                        TxLength <= TxLength - 1;
                        if (TxLength(0)='0') then
                            TxAddr <= TxAddrPlus1;
                            TxAddrPlus1 <= TxAddrPlus1 + 1;
                        end if;
                        if (TxLength="00000000000") then
                            TxState <= TX_FOOTER;
                        end if;
                    end if;
                    
                when TX_FOOTER =>
                    -- Send packet footer
                    if (OutFIFOWE='1') then
                        TxOffset <= TxOffset+1;
                        if (TxOffset=X"5") then
                            NetReqVal <= '0';
                            TxState <= IDLE;
                        end if;
                    end if;
                    
                when others => null;
            end case;
        end if;
    end process;
    
    NetBufferEmpty <= '1' when RegWriteCount=TxAddr else '0';
    TotalNetSamples <= PreNetSamples + PostNetSamples;
    NetReq <= NetReqVal or not OutFIFOEmpty;
    OutFIFOWE <= not OutFIFOFull and NetReqVal when TxState=TX_HEADER else
                 not OutFIFOFull and NetReqVal and (not TxLength(0) or not NetBufferEmpty) when TxState=TX_PAYLOAD else
                 not OutFIFOFull and NetReqVal and not CalcFIFOEmpty;
    NetAddr <= TxAddrPlus1 when OutFIFOWE='1' and TxLength(0)='0' and TxState=TX_PAYLOAD else TxAddr;
    TrigFIFONetRE <= '1' when TxState=TX_PAYLOAD and OutFIFOWE='1' and TxLength="00000000001" else '0';

    CalcFIFORE <= '1' when TxState=TX_FOOTER and OutFIFOWE='1' and TxOffset=X"5" else '0';
    
    process (NetReadData, TxLength, TxState, TxOffset, PreNetSamples, PostNetSamples, CalcFIFODataOut) begin
        OutFIFODataIn <= X"00";
        
        case TxState is
            when TX_HEADER =>
                -- Send timestamp and number of samples as the header
                case TxOffset is
                    when X"0" => OutFIFODataIn <= TrigTimeFIFONetDataOut(63 downto 56);
                    when X"1" => OutFIFODataIn <= TrigTimeFIFONetDataOut(55 downto 48);
                    when X"2" => OutFIFODataIn <= TrigTimeFIFONetDataOut(47 downto 40);
                    when X"3" => OutFIFODataIn <= TrigTimeFIFONetDataOut(39 downto 32);
                    when X"4" => OutFIFODataIn <= TrigTimeFIFONetDataOut(31 downto 24);
                    when X"5" => OutFIFODataIn <= TrigTimeFIFONetDataOut(23 downto 16);
                    when X"6" => OutFIFODataIn <= TrigTimeFIFONetDataOut(15 downto 8);
                    when X"7" => OutFIFODataIn <= TrigTimeFIFONetDataOut(7 downto 0);
                    when X"8" => OutFIFODataIn <= "000000" & PreNetSamples(9 downto 8);
                    when X"9" => OutFIFODataIn <= PreNetSamples(7 downto 0);
                    when X"a" => OutFIFODataIn <= "000000" & PostNetSamples(9 downto 8);
                    when others => OutFIFODataIn <= PostNetSamples(7 downto 0);
                end case;
                    
            when TX_PAYLOAD =>
                -- Send sample values as payload data
                if (TxLength(0)='0') then
                    OutFIFODataIn <= NetReadData(7 downto 0);
                else
                    OutFIFODataIn <= "00"&NetReadData(13 downto 8);
                end if;
                
            when others =>  --TX_FOOTER =>
                -- Send calculated values as footer
                case TxOffset is
                    when X"0" => OutFIFODataIn <= "0000000" & CalcFIFODataOut(38);
                    when X"1" => OutFIFODataIn <= "00" & CalcFIFODataOut(37 downto 32);
                    when X"2" => OutFIFODataIn <= CalcFIFODataOut(31 downto 24);
                    when X"3" => OutFIFODataIn <= CalcFIFODataOut(23 downto 16);
                    when X"4" => OutFIFODataIn <= CalcFIFODataOut(15 downto 8);
                    when others => OutFIFODataIn <= CalcFIFODataOut(7 downto 0);
                end case;
                
        end case;
    end process;
    
    --------------------------------------------------------------------------
    -- Feed output data through a short FIFO to the network
    -- This disconnects us from the downstream logic and improves timing
    process (RST, CLK) begin
        if (RST='1') then
            OutFIFOWriteCount <= (others=>'0');
            OutFIFOReadCount <= (others=>'0');
            OutFIFOFull <= '0';
        elsif (CLK'event and CLK='1') then
            if (OutFIFOWE='1') then
                OutFIFOWriteCount <= NextOutFIFOWriteCount;
            end if;
            if (OutFIFORE='1') then
                OutFIFOReadCount <= OutFIFOReadCount + 1;
            end if;
            if (OutFIFOBytes>=X"C") then
                OutFIFOFull <= '1';
            else 
                OutFIFOFull <= '0';
            end if;
        end if;
    end process;
    process (CLK) begin
        if (CLK'event and CLK='1') then
            if (OutFIFOWE='1') then
                OutFIFO(conv_integer(OutFIFOWriteCount)) <= OutFIFODataIn;
            end if;
        end if;
    end process;
    NextOutFIFOWriteCount <= OutFIFOWriteCount + 1;
    OutFIFOBytes <= OutFIFOWriteCount-OutFIFOReadCount;
    OutFIFODataOut <= OutFIFO(conv_integer(OutFIFOReadCount));
    OutFIFOEmpty <= '1' when OutFIFOWriteCount=OutFIFOReadCount else '0';
    
    -- Output to network
    NetData <= OutFIFODataOut;
    OutFIFORE <= not OutFIFOEmpty and not NetBusy;
    NetDataWE <= OutFIFORE;
    
    --------------------------------------------------------------------------
    -- Control register map
    process (RST, CLK) begin
        if (RST='1') then
            PeakThreshold <= (others=>'0');
            AreaThreshold <= (others=>'0');
            TrigLevel <= (others=>'0');
            PreCalcSamples <= (others=>'0');
            PostCalcSamples <= (others=>'0');
            PreNetSamples <= (others=>'0');
            PostNetSamples <= (others=>'0');
            EnableTrigger <= '0';
        elsif (CLK'event and CLK='1') then
            if (RegWE='1') then
                case RegAddr is
                    when X"000" => TrigLevel(13 downto 8) <= RegWriteData(5 downto 0);
                    when X"001" => TrigLevel(7 downto 0) <= RegWriteData;
                    when X"002" => PreCalcSamples(9 downto 8) <= RegWriteData(1 downto 0);
                    when X"003" => PreCalcSamples(7 downto 0) <= RegWriteData;
                    when X"004" => PostCalcSamples(9 downto 8) <= RegWriteData(1 downto 0);
                    when X"005" => PostCalcSamples(7 downto 0) <= RegWriteData;
                    when X"006" => PreNetSamples(9 downto 8) <= RegWriteData(1 downto 0);
                    when X"007" => PreNetSamples(7 downto 0) <= RegWriteData;
                    when X"008" => PostNetSamples(9 downto 8) <= RegWriteData(1 downto 0);
                    when X"009" => PostNetSamples(7 downto 0) <= RegWriteData;
                    when X"00A" => PeakThreshold(13 downto 8) <= RegWriteData(5 downto 0);
                    when X"00B" => PeakThreshold(7 downto 0) <= RegWriteData;
                    when X"00C" => AreaThreshold(23 downto 16) <= RegWriteData;
                    when X"00D" => AreaThreshold(15 downto 8) <= RegWriteData;
                    when X"00E" => AreaThreshold(7 downto 0) <= RegWriteData;
                    when X"00F" => EnableTrigger <= RegWriteData(0);
                    when others => null;
                end case;
            end if;
        end if;
    end process;
    
    -- FIXME: No register read values?
    RegReadData <= (others=>'0');
    RegReadValid <= '0';
    

    --------------------------------------------------------------------------
    -- Chipscope
--    i_icon : 
--        if INDEX=0 generate
--            begin
--                i_iconi : icon
--                    port map (
--                        control0 => control0
--                );
--        end generate;
--    i_ila : 
--        if INDEX=0 generate
--            begin
--                i_ilai : ila
--                    port map (
--                        control => control0,
--                        clk => CLK,
--                        trig0 => trig0
--                    );
--        end generate;
--    trig0(123 downto 0) <=
--            CalcArea(15 downto 0) & -- 16
--            CalcLength &        -- 10
--            CalcAddr &          -- 10
--            --CalcPeak &          -- 14
--            CalcReadData &      -- 14
--            PortToggle &        -- 1
--            CalcBufferEmpty &   -- 1
--            
--            OutFIFOFull &       -- 1
--            
--            RegWriteCount &     -- 10
--            TxAddr &            -- 10
--            NetBufferEmpty &    -- 1
--            TxLength &          -- 11
--            
--            OutFIFODataOut &    -- 8
--            OutFIFORE &         -- 1
--            NetBusy &           -- 1
--            
--            NetReqVal &         -- 1
--            OutFIFOWE &         -- 1
--            OutFIFODataIn &     -- 8
--            
--            TrigFIFOFull &      -- 1
--            TrigFIFONetEmpty &  -- 1
--            BufferFull &         -- 1
--            
--            DataInEmpty &   -- 1
--            DataInREVal &      -- 1        
--            DataIn          -- 14
--        ;
--    
end arch;


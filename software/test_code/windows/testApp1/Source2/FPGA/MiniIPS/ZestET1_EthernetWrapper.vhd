--   File name: ZestET1_EthernetWrapper.vhd
--   Version: 1.50
--   Date: 09/2/2012
--
--   Wrapper around GigExpedite interface to simplfy user access.
--   Contains the state machine needed to open/close connections and
--   transmit/receive data over Ethernet.
--
--   Copyright (C) 2012 Orange Tree Technologies Ltd. All rights reserved.
--   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and
--   modify this logic core in any form including but not limited to VHDL source code or
--   EDIF netlist in FPGA designs that target the ZestET1.
--   Orange Tree Technologies prohibits the use of this logic core or any modification of
--   it in any form including but not limited to VHDL source code or EDIF netlist in
--   FPGA or ASIC designs that target any other hardware unless the purchaser of the
--   ZestET1 has purchased the appropriate licence from Orange Tree Technologies.
--   Contact Orange Tree Technologies if you want to purchase such a licence.
--
--  *****************************************************************************************
--  **
--  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
--  **              provided to you "as is". Orange Tree Technologies and its licensors 
--  **              make and you receive no warranties or conditions, express, implied, 
--  **              statutory or otherwise, and Orange Tree Technologies specifically 
--  **              disclaims any implied warranties of merchantability, non-infringement,
--  **              or fitness for a particular purpose. Orange Tree Technologies does not
--  **              warrant that the functions contained in these designs will meet your 
--  **              requirements, or that the operation of these designs will be 
--  **              uninterrupted or error free, or that defects in the Designs will be 
--  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
--  **              make any representations regarding use or the results of the use of the 
--  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
--  **
--  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
--  **              or its licensors be liable for any loss of data, lost profits, cost or 
--  **              procurement of substitute goods or services, or for any special, 
--  **              incidental, consequential, or indirect damages arising from the use or 
--  **              operation of the designs or accompanying documentation, however caused 
--  **              and on any theory of liability. This limitation will apply even if 
--  **              Orange Tree Technologies has been advised of the possibility of such 
--  **              damage. This limitation shall apply notwithstanding the failure of the 
--  **              essential purpose of any limited remedies herein.
--  **
--  *****************************************************************************************

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_unsigned.all;
use IEEE.std_logic_arith.all;

library UNISIM;
use UNISIM.VComponents.all;

entity ZestET1_EthernetWrapper is
    generic (
        CLOCK_RATE : integer := 125000000
    );
    port (
        -- Clock and reset
        User_CLK : in std_logic;
        User_RST : in std_logic;
        
        -- Network status
        User_LinkStatus : out std_logic;
        User_LinkSpeed : out std_logic_vector(2 downto 0);
        User_LinkDuplex : out std_logic;
        User_LocalIPAddr : out std_logic_vector(31 downto 0);
        
        -- User connection interfaces
        User_Enable0 : in std_logic := '0';
        User_Server0 : in std_logic := '0';
        User_TCP0 : in std_logic := '0';
        User_Connected0 : out std_logic;
        User_MTU0 : in std_logic_vector(7 downto 0) := X"2d";
        User_TTL0 : in std_logic_vector(7 downto 0) := X"80";
        User_LocalPort0 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemotePort0 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemoteIPAddr0 : in std_logic_vector(31 downto 0) := X"00000000";
        User_ReadRemotePort0 : out std_logic_vector(15 downto 0);
        User_ReadRemoteIPAddr0 : out std_logic_vector(31 downto 0);
        User_TxPause0 : in std_logic := '0';
        User_TxPaused0 : out std_logic;
        User_TxData0 : in std_logic_vector(7 downto 0) := X"00";
        User_TxEnable0 : in std_logic := '0';
        User_TxBusy0 : out std_logic;
        User_TxBufferEmpty0 : out std_logic;
        User_RxData0 : out std_logic_vector(7 downto 0);
        User_RxEnable0 : out std_logic;
        User_RxBusy0 : in std_logic := '1';
        
        User_Enable1 : in std_logic := '0';
        User_Server1 : in std_logic := '0';
        User_TCP1 : in std_logic := '0';
        User_Connected1 : out std_logic;
        User_MTU1 : in std_logic_vector(7 downto 0) := X"2d";
        User_TTL1 : in std_logic_vector(7 downto 0) := X"80";
        User_LocalPort1 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemotePort1 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemoteIPAddr1 : in std_logic_vector(31 downto 0) := X"00000000";
        User_ReadRemotePort1 : out std_logic_vector(15 downto 0);
        User_ReadRemoteIPAddr1 : out std_logic_vector(31 downto 0);
        User_TxPause1 : in std_logic := '0';
        User_TxPaused1 : out std_logic;
        User_TxData1 : in std_logic_vector(7 downto 0) := X"00";
        User_TxEnable1 : in std_logic := '0';
        User_TxBusy1 : out std_logic;
        User_TxBufferEmpty1 : out std_logic;
        User_RxData1 : out std_logic_vector(7 downto 0);
        User_RxEnable1 : out std_logic;
        User_RxBusy1 : in std_logic := '1';

        User_Enable2 : in std_logic := '0';
        User_Server2 : in std_logic := '0';
        User_TCP2 : in std_logic := '0';
        User_Connected2 : out std_logic;
        User_MTU2 : in std_logic_vector(7 downto 0) := X"2d";
        User_TTL2 : in std_logic_vector(7 downto 0) := X"80";
        User_LocalPort2 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemotePort2 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemoteIPAddr2 : in std_logic_vector(31 downto 0) := X"00000000";
        User_ReadRemotePort2 : out std_logic_vector(15 downto 0);
        User_ReadRemoteIPAddr2 : out std_logic_vector(31 downto 0);
        User_TxPause2 : in std_logic := '0';
        User_TxPaused2 : out std_logic;
        User_TxData2 : in std_logic_vector(7 downto 0) := X"00";
        User_TxEnable2 : in std_logic := '0';
        User_TxBusy2 : out std_logic;
        User_TxBufferEmpty2 : out std_logic;
        User_RxData2 : out std_logic_vector(7 downto 0);
        User_RxEnable2 : out std_logic;
        User_RxBusy2 : in std_logic := '1';
        
        User_Enable3 : in std_logic := '0';
        User_Server3 : in std_logic := '0';
        User_TCP3 : in std_logic := '0';
        User_Connected3 : out std_logic;
        User_MTU3 : in std_logic_vector(7 downto 0) := X"2d";
        User_TTL3 : in std_logic_vector(7 downto 0) := X"80";
        User_LocalPort3 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemotePort3 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemoteIPAddr3 : in std_logic_vector(31 downto 0) := X"00000000";
        User_ReadRemotePort3 : out std_logic_vector(15 downto 0);
        User_ReadRemoteIPAddr3 : out std_logic_vector(31 downto 0);
        User_TxPause3 : in std_logic := '0';
        User_TxPaused3 : out std_logic;
        User_TxData3 : in std_logic_vector(7 downto 0) := X"00";
        User_TxEnable3 : in std_logic := '0';
        User_TxBusy3 : out std_logic;
        User_TxBufferEmpty3 : out std_logic;
        User_RxData3 : out std_logic_vector(7 downto 0);
        User_RxEnable3 : out std_logic;
        User_RxBusy3 : in std_logic := '1';
        
        User_Enable4 : in std_logic := '0';
        User_Server4 : in std_logic := '0';
        User_TCP4 : in std_logic := '0';
        User_Connected4 : out std_logic;
        User_MTU4 : in std_logic_vector(7 downto 0) := X"2d";
        User_TTL4 : in std_logic_vector(7 downto 0) := X"80";
        User_LocalPort4 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemotePort4 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemoteIPAddr4 : in std_logic_vector(31 downto 0) := X"00000000";
        User_ReadRemotePort4 : out std_logic_vector(15 downto 0);
        User_ReadRemoteIPAddr4 : out std_logic_vector(31 downto 0);
        User_TxPause4 : in std_logic := '0';
        User_TxPaused4 : out std_logic;
        User_TxData4 : in std_logic_vector(7 downto 0) := X"00";
        User_TxEnable4 : in std_logic := '0';
        User_TxBusy4 : out std_logic;
        User_TxBufferEmpty4 : out std_logic;
        User_RxData4 : out std_logic_vector(7 downto 0);
        User_RxEnable4 : out std_logic;
        User_RxBusy4 : in std_logic := '1';
        
        User_Enable5 : in std_logic := '0';
        User_Server5 : in std_logic := '0';
        User_TCP5 : in std_logic := '0';
        User_Connected5 : out std_logic;
        User_MTU5 : in std_logic_vector(7 downto 0) := X"2d";
        User_TTL5 : in std_logic_vector(7 downto 0) := X"80";
        User_LocalPort5 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemotePort5 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemoteIPAddr5 : in std_logic_vector(31 downto 0) := X"00000000";
        User_ReadRemotePort5 : out std_logic_vector(15 downto 0);
        User_ReadRemoteIPAddr5 : out std_logic_vector(31 downto 0);
        User_TxPause5 : in std_logic := '0';
        User_TxPaused5 : out std_logic;
        User_TxData5 : in std_logic_vector(7 downto 0) := X"00";
        User_TxEnable5 : in std_logic := '0';
        User_TxBusy5 : out std_logic;
        User_TxBufferEmpty5 : out std_logic;
        User_RxData5 : out std_logic_vector(7 downto 0);
        User_RxEnable5 : out std_logic;
        User_RxBusy5 : in std_logic := '1';
        
        User_Enable6 : in std_logic := '0';
        User_Server6 : in std_logic := '0';
        User_TCP6 : in std_logic := '0';
        User_Connected6 : out std_logic;
        User_MTU6 : in std_logic_vector(7 downto 0) := X"2d";
        User_TTL6 : in std_logic_vector(7 downto 0) := X"80";
        User_LocalPort6 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemotePort6 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemoteIPAddr6 : in std_logic_vector(31 downto 0) := X"00000000";
        User_ReadRemotePort6 : out std_logic_vector(15 downto 0);
        User_ReadRemoteIPAddr6 : out std_logic_vector(31 downto 0);
        User_TxPause6 : in std_logic := '0';
        User_TxPaused6 : out std_logic;
        User_TxData6 : in std_logic_vector(7 downto 0) := X"00";
        User_TxEnable6 : in std_logic := '0';
        User_TxBusy6 : out std_logic;
        User_TxBufferEmpty6 : out std_logic;
        User_RxData6 : out std_logic_vector(7 downto 0);
        User_RxEnable6 : out std_logic;
        User_RxBusy6 : in std_logic := '1';
        
        User_Enable7 : in std_logic := '0';
        User_Server7 : in std_logic := '0';
        User_TCP7 : in std_logic := '0';
        User_Connected7 : out std_logic;
        User_MTU7 : in std_logic_vector(7 downto 0) := X"2d";
        User_TTL7 : in std_logic_vector(7 downto 0) := X"80";
        User_LocalPort7 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemotePort7 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemoteIPAddr7 : in std_logic_vector(31 downto 0) := X"00000000";
        User_ReadRemotePort7 : out std_logic_vector(15 downto 0);
        User_ReadRemoteIPAddr7 : out std_logic_vector(31 downto 0);
        User_TxPause7 : in std_logic := '0';
        User_TxPaused7 : out std_logic;
        User_TxData7 : in std_logic_vector(7 downto 0) := X"00";
        User_TxEnable7 : in std_logic := '0';
        User_TxBusy7 : out std_logic;
        User_TxBufferEmpty7 : out std_logic;
        User_RxData7 : out std_logic_vector(7 downto 0);
        User_RxEnable7 : out std_logic;
        User_RxBusy7 : in std_logic := '1';
        
        User_Enable8 : in std_logic := '0';
        User_Server8 : in std_logic := '0';
        User_TCP8 : in std_logic := '0';
        User_Connected8 : out std_logic;
        User_MTU8 : in std_logic_vector(7 downto 0) := X"2d";
        User_TTL8 : in std_logic_vector(7 downto 0) := X"80";
        User_LocalPort8 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemotePort8 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemoteIPAddr8 : in std_logic_vector(31 downto 0) := X"00000000";
        User_ReadRemotePort8 : out std_logic_vector(15 downto 0);
        User_ReadRemoteIPAddr8 : out std_logic_vector(31 downto 0);
        User_TxPause8 : in std_logic := '0';
        User_TxPaused8 : out std_logic;
        User_TxData8 : in std_logic_vector(7 downto 0) := X"00";
        User_TxEnable8 : in std_logic := '0';
        User_TxBusy8 : out std_logic;
        User_TxBufferEmpty8 : out std_logic;
        User_RxData8 : out std_logic_vector(7 downto 0);
        User_RxEnable8 : out std_logic;
        User_RxBusy8 : in std_logic := '1';
        
        User_Enable9 : in std_logic := '0';
        User_Server9 : in std_logic := '0';
        User_TCP9 : in std_logic := '0';
        User_Connected9 : out std_logic;
        User_MTU9 : in std_logic_vector(7 downto 0) := X"2d";
        User_TTL9 : in std_logic_vector(7 downto 0) := X"80";
        User_LocalPort9 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemotePort9 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemoteIPAddr9 : in std_logic_vector(31 downto 0) := X"00000000";
        User_ReadRemotePort9 : out std_logic_vector(15 downto 0);
        User_ReadRemoteIPAddr9 : out std_logic_vector(31 downto 0);
        User_TxPause9 : in std_logic := '0';
        User_TxPaused9 : out std_logic;
        User_TxData9 : in std_logic_vector(7 downto 0) := X"00";
        User_TxEnable9 : in std_logic := '0';
        User_TxBusy9 : out std_logic;
        User_TxBufferEmpty9 : out std_logic;
        User_RxData9 : out std_logic_vector(7 downto 0);
        User_RxEnable9 : out std_logic;
        User_RxBusy9 : in std_logic := '1';
        
        User_Enable10 : in std_logic := '0';
        User_Server10 : in std_logic := '0';
        User_TCP10 : in std_logic := '0';
        User_Connected10 : out std_logic;
        User_MTU10 : in std_logic_vector(7 downto 0) := X"2d";
        User_TTL10 : in std_logic_vector(7 downto 0) := X"80";
        User_LocalPort10 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemotePort10 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemoteIPAddr10 : in std_logic_vector(31 downto 0) := X"00000000";
        User_ReadRemotePort10 : out std_logic_vector(15 downto 0);
        User_ReadRemoteIPAddr10 : out std_logic_vector(31 downto 0);
        User_TxPause10 : in std_logic := '0';
        User_TxPaused10 : out std_logic;
        User_TxData10 : in std_logic_vector(7 downto 0) := X"00";
        User_TxEnable10 : in std_logic := '0';
        User_TxBusy10 : out std_logic;
        User_TxBufferEmpty10 : out std_logic;
        User_RxData10 : out std_logic_vector(7 downto 0);
        User_RxEnable10 : out std_logic;
        User_RxBusy10 : in std_logic := '1';
        
        User_Enable11 : in std_logic := '0';
        User_Server11 : in std_logic := '0';
        User_TCP11 : in std_logic := '0';
        User_Connected11 : out std_logic;
        User_MTU11 : in std_logic_vector(7 downto 0) := X"2d";
        User_TTL11 : in std_logic_vector(7 downto 0) := X"80";
        User_LocalPort11 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemotePort11 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemoteIPAddr11 : in std_logic_vector(31 downto 0) := X"00000000";
        User_ReadRemotePort11 : out std_logic_vector(15 downto 0);
        User_ReadRemoteIPAddr11 : out std_logic_vector(31 downto 0);
        User_TxPause11 : in std_logic := '0';
        User_TxPaused11 : out std_logic;
        User_TxData11 : in std_logic_vector(7 downto 0) := X"00";
        User_TxEnable11 : in std_logic := '0';
        User_TxBusy11 : out std_logic;
        User_TxBufferEmpty11 : out std_logic;
        User_RxData11 : out std_logic_vector(7 downto 0);
        User_RxEnable11 : out std_logic;
        User_RxBusy11 : in std_logic := '1';
        
        User_Enable12 : in std_logic := '0';
        User_Server12 : in std_logic := '0';
        User_TCP12 : in std_logic := '0';
        User_Connected12 : out std_logic;
        User_MTU12 : in std_logic_vector(7 downto 0) := X"2d";
        User_TTL12 : in std_logic_vector(7 downto 0) := X"80";
        User_LocalPort12 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemotePort12 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemoteIPAddr12 : in std_logic_vector(31 downto 0) := X"00000000";
        User_ReadRemotePort12 : out std_logic_vector(15 downto 0);
        User_ReadRemoteIPAddr12 : out std_logic_vector(31 downto 0);
        User_TxPause12 : in std_logic := '0';
        User_TxPaused12 : out std_logic;
        User_TxData12 : in std_logic_vector(7 downto 0) := X"00";
        User_TxEnable12 : in std_logic := '0';
        User_TxBusy12 : out std_logic;
        User_TxBufferEmpty12 : out std_logic;
        User_RxData12 : out std_logic_vector(7 downto 0);
        User_RxEnable12 : out std_logic;
        User_RxBusy12 : in std_logic := '1';
        
        User_Enable13 : in std_logic := '0';
        User_Server13 : in std_logic := '0';
        User_TCP13 : in std_logic := '0';
        User_Connected13 : out std_logic;
        User_MTU13 : in std_logic_vector(7 downto 0) := X"2d";
        User_TTL13 : in std_logic_vector(7 downto 0) := X"80";
        User_LocalPort13 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemotePort13 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemoteIPAddr13 : in std_logic_vector(31 downto 0) := X"00000000";
        User_ReadRemotePort13 : out std_logic_vector(15 downto 0);
        User_ReadRemoteIPAddr13 : out std_logic_vector(31 downto 0);
        User_TxPause13 : in std_logic := '0';
        User_TxPaused13 : out std_logic;
        User_TxData13 : in std_logic_vector(7 downto 0) := X"00";
        User_TxEnable13 : in std_logic := '0';
        User_TxBusy13 : out std_logic;
        User_TxBufferEmpty13 : out std_logic;
        User_RxData13 : out std_logic_vector(7 downto 0);
        User_RxEnable13 : out std_logic;
        User_RxBusy13 : in std_logic := '1';
        
        User_Enable14 : in std_logic := '0';
        User_Server14 : in std_logic := '0';
        User_TCP14 : in std_logic := '0';
        User_Connected14 : out std_logic;
        User_MTU14 : in std_logic_vector(7 downto 0) := X"2d";
        User_TTL14 : in std_logic_vector(7 downto 0) := X"80";
        User_LocalPort14 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemotePort14 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemoteIPAddr14 : in std_logic_vector(31 downto 0) := X"00000000";
        User_ReadRemotePort14 : out std_logic_vector(15 downto 0);
        User_ReadRemoteIPAddr14 : out std_logic_vector(31 downto 0);
        User_TxPause14 : in std_logic := '0';
        User_TxPaused14 : out std_logic;
        User_TxData14 : in std_logic_vector(7 downto 0) := X"00";
        User_TxEnable14 : in std_logic := '0';
        User_TxBusy14 : out std_logic;
        User_TxBufferEmpty14 : out std_logic;
        User_RxData14 : out std_logic_vector(7 downto 0);
        User_RxEnable14 : out std_logic;
        User_RxBusy14 : in std_logic := '1';
        
        User_Enable15 : in std_logic := '0';
        User_Server15 : in std_logic := '0';
        User_TCP15 : in std_logic := '0';
        User_Connected15 : out std_logic;
        User_MTU15 : in std_logic_vector(7 downto 0) := X"2d";
        User_TTL15 : in std_logic_vector(7 downto 0) := X"80";
        User_LocalPort15 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemotePort15 : in std_logic_vector(15 downto 0) := X"0000";
        User_WriteRemoteIPAddr15 : in std_logic_vector(31 downto 0) := X"00000000";
        User_ReadRemotePort15 : out std_logic_vector(15 downto 0);
        User_ReadRemoteIPAddr15 : out std_logic_vector(31 downto 0);
        User_TxPause15 : in std_logic := '0';
        User_TxPaused15 : out std_logic;
        User_TxData15 : in std_logic_vector(7 downto 0) := X"00";
        User_TxEnable15 : in std_logic := '0';
        User_TxBusy15 : out std_logic;
        User_TxBufferEmpty15 : out std_logic;
        User_RxData15 : out std_logic_vector(7 downto 0);
        User_RxEnable15 : out std_logic;
        User_RxBusy15 : in std_logic := '1';
        
        -- Interface to GigExpedite
        Eth_Clk : out std_logic;
        Eth_CSn : out std_logic;
        Eth_WEn : out std_logic;
        Eth_A : out std_logic_vector(4 downto 0);
        Eth_D : inout std_logic_vector(15 downto 0);
        Eth_BE : out std_logic_vector(1 downto 0);
        Eth_Intn : in std_logic
    );
end ZestET1_EthernetWrapper;

architecture arch of ZestET1_EthernetWrapper is

    --------------------------------------------------------------------------
    -- Declare constants
    -- GigExpedite register addresses
    constant LOCAL_IP_ADDR_LOW          : std_logic_vector(4 downto 0) := "00000";
    constant LOCAL_IP_ADDR_HIGH         : std_logic_vector(4 downto 0) := "00001";
    constant LINK_STATUS                : std_logic_vector(4 downto 0) := "00010";
    constant CONNECTION_PAGE            : std_logic_vector(4 downto 0) := "00111";
    constant LOCAL_PORT                 : std_logic_vector(4 downto 0) := "01000";
    constant REMOTE_IP_ADDR_LOW         : std_logic_vector(4 downto 0) := "01001";
    constant REMOTE_IP_ADDR_HIGH        : std_logic_vector(4 downto 0) := "01010";
    constant REMOTE_PORT                : std_logic_vector(4 downto 0) := "01011";
    constant MTU_TTL                    : std_logic_vector(4 downto 0) := "01100";
    constant INTERRUPT_ENABLE_STATUS    : std_logic_vector(4 downto 0) := "01101";
    constant CONNECTION_STATE           : std_logic_vector(4 downto 0) := "01110";
    constant FRAME_LENGTH               : std_logic_vector(4 downto 0) := "01111";
    constant DATA_FIFO                  : std_logic_vector(4 downto 0) := "10000";
    
    -- Connection states (for CONNECTION_STATE reg)
    constant CLOSED       : std_logic_vector(15 downto 0) := X"0000";
    constant LISTEN       : std_logic_vector(15 downto 0) := X"0001";
    constant CONNECT      : std_logic_vector(15 downto 0) := X"0002";
    constant ESTABLISHED  : std_logic_vector(15 downto 0) := X"0003";
    constant CONN_TCP     : std_logic_vector(15 downto 0) := X"0010";
    constant CONN_ENABLE  : std_logic_vector(15 downto 0) := X"0020";
    constant CONN_TCP_BIT    : integer := 4;
    constant CONN_ENABLE_BIT : integer := 5;
    
    -- Interrupt enable and status bits (for INTERRUPT_ENABLE_STATUS reg)
    constant IE_INCOMING              : std_logic_vector(15 downto 0) := X"0001";
    constant IE_OUTGOING_EMPTY        : std_logic_vector(15 downto 0) := X"0002";
    constant IE_OUTGOING_NOT_FULL     : std_logic_vector(15 downto 0) := X"0004";
    constant IE_STATE                 : std_logic_vector(15 downto 0) := X"0008";
    constant IE_INCOMING_BIT          : integer := 0;
    constant IE_OUTGOING_EMPTY_BIT    : integer := 1;
    constant IE_OUTGOING_NOT_FULL_BIT : integer := 2;
    constant IE_STATE_BIT             : integer := 3;

    -- Length of burst from GigExpedite (192 bytes)
    constant USER_BURST_LENGTH_WORDS  : std_logic_vector(7 downto 0) := X"60";

    --------------------------------------------------------------------------
    -- Declare types
    type ARRAY1 is array(0 to 15) of std_logic;
    type ARRAY2 is array(0 to 15) of std_logic_vector(1 downto 0);
    type ARRAY5 is array(0 to 15) of std_logic_vector(4 downto 0);
    type ARRAY6 is array(0 to 15) of std_logic_vector(5 downto 0);
    type ARRAY8 is array(0 to 15) of std_logic_vector(7 downto 0);
    type ARRAY16 is array(0 to 15) of std_logic_vector(15 downto 0);
    type ARRAY18 is array(0 to 15) of std_logic_vector(17 downto 0);
    type ARRAY22 is array(0 to 15) of std_logic_vector(21 downto 0);
    type ARRAY32 is array(0 to 15) of std_logic_vector(31 downto 0);
    
    -- Control state machine states
    type STATE_TYPE is (
            DELAY_INIT,
            CLEAN,
            CLEAN_CHECK,
            CLEAR_INTS,
            IDLE,
            GET_STATUS,
            USER_STATE_CHANGE_REQ,
            OPEN_CONNECTION,
            CLOSE_CONNECTION,
            PAUSE_UNPAUSE_CONNECTION,
            CHECK_STATE,
            PROCESS_INTERRUPTS,
            STATE_CHANGE,
            READ_LENGTH,
            WRITE_LENGTH
        );

    -- Connection control states
    type STATE_CONN is (
            CS_DISABLED,
            CS_WAIT_ESTAB,
            CS_ESTAB,
            CS_WAIT_CLOSE,
            CS_WAIT_DISABLED
        );
    type STATE_CONN_ARRAY is array(0 to 15) of STATE_CONN;

    --------------------------------------------------------------------------
    -- Declare signals
    signal User_Enable : std_logic_vector(15 downto 0);
    signal User_Server : std_logic_vector(15 downto 0);
    signal User_TCP : std_logic_vector(15 downto 0);
    signal User_Connected : std_logic_vector(15 downto 0);
    signal User_MTUTTL : ARRAY16;
    signal User_LocalPort : ARRAY16;
    signal User_WriteRemotePort : ARRAY16;
    signal User_WriteRemoteIPAddr : ARRAY32;
    signal User_ReadRemotePort : ARRAY16;
    signal User_ReadRemoteIPAddr : ARRAY32;
    signal User_TxPause : std_logic_vector(15 downto 0);
    signal User_TxPaused : std_logic_vector(15 downto 0);
    signal User_TxData : ARRAY8;
    signal User_TxEnable : std_logic_vector(15 downto 0);
    signal User_TxBusy : std_logic_vector(15 downto 0);
    signal User_TxBufferEmpty : std_logic_vector(15 downto 0);
    signal User_RxData : ARRAY8;
    signal User_RxEnable : std_logic_vector(15 downto 0);
    signal User_RxBusy : std_logic_vector(15 downto 0);
    signal User_LinkStatusVal : std_logic;
    signal User_LocalIPAddrVal : std_logic_vector(31 downto 0);
    signal LinkUpCount : std_logic_vector(7 downto 0);

    -- GigExpedite interface wires
    signal CtrlWE : std_logic;
    signal CtrlRE : std_logic;
    signal CtrlAddr : std_logic_vector(4 downto 0);
    signal CtrlBE : std_logic_vector(1 downto 0);
    signal CtrlWriteData : std_logic_vector(15 downto 0);
    signal CtrlOwner : std_logic_vector(7 downto 0);
    signal DataWE : std_logic;
    signal DataWEReq : std_logic;
    signal DataRE : std_logic;
    signal DataAddr : std_logic_vector(4 downto 0);
    signal DataBE : std_logic_vector(1 downto 0);
    signal DataWriteData : std_logic_vector(15 downto 0);
    signal DataOwner : std_logic_vector(7 downto 0);
    signal UserWE  : std_logic;
    signal UserRE  : std_logic;
    signal UserAddr : std_logic_vector(4 downto 0);
    signal UserBE : std_logic_vector(1 downto 0);
    signal UserWriteData : std_logic_vector(15 downto 0);
    signal UserReadData : std_logic_vector(15 downto 0);
    signal UserReadDataValid : std_logic;
    signal UserInterrupt : std_logic;
    signal UserOwner : std_logic_vector(7 downto 0);
    signal UserValidOwner : std_logic_vector(7 downto 0);

    -- Control state machine
    signal Delay : std_logic_vector(23 downto 0);
    signal DelayDone : std_logic;
    signal CleanDone : std_logic;
    signal SinceStatusUpdate : std_logic_vector(15 downto 0);
    signal SinceStatusUpdateDone : std_logic;
    signal User_EnableEdge : std_logic_vector(15 downto 0);
    signal User_EnableEdgeVal : std_logic;
    signal User_TxPauseEdge : std_logic_vector(15 downto 0);
    signal LastUser_Enable : std_logic_vector(15 downto 0);
    signal LastUser_TxPause : std_logic_vector(15 downto 0);
    signal User_RegEnable : std_logic_vector(15 downto 0);
    signal User_EnableVal : std_logic;
    signal User_RegEnableVal : std_logic;
    signal ConnState : STATE_CONN_ARRAY;
    signal CtrlConnection : std_logic_vector(3 downto 0);
    signal CtrlConnectionMask : std_logic_vector(15 downto 0);
    signal NextCtrlConnectionValid : std_logic;
    signal NextCtrlConnection : std_logic_vector(3 downto 0);
    signal NextCtrlConnectionPlus1 : std_logic_vector(3 downto 0);
    signal NextCtrlConnectionMask : std_logic_vector(15 downto 0);
    signal CtrlState : STATE_TYPE;
    signal CtrlSubState : std_logic_vector(15 downto 0);
    signal UserValidCount : std_logic_vector(15 downto 0);
    signal CurrentState : std_logic_vector(5 downto 0);
    signal InterruptState : std_logic_vector(3 downto 0);
    signal BlockDataConnection : std_logic_vector(4 downto 0);
    signal BlockDataRE : std_logic;
    signal BlockDataWE : std_logic;
    signal BlockTxMergeFIFO : std_logic;
    
    -- Data state machine
    signal DataConnection : std_logic_vector(4 downto 0);
    signal NextDataConnection : std_logic_vector(4 downto 0);
    signal ReadOK : std_logic;
    signal WriteOK : std_logic;
    signal NextTransferOK : std_logic;
    signal ArbCounter : std_logic_vector(7 downto 0);
    
    -- Intermediate Tx FIFO
    signal TxConnection : std_logic_vector(3 downto 0);
    signal TxConnectionMask : std_logic_vector(15 downto 0);
    signal NextTxConnection : std_logic_vector(3 downto 0);
    signal NextTxConnectionMask : std_logic_vector(15 downto 0);
    signal TxMergeFIFOWriteCount : std_logic_vector(3 downto 0);
    signal RegTxMergeFIFOWriteCount : std_logic_vector(3 downto 0);
    signal TxMergeFIFOReadCount : std_logic_vector(3 downto 0);
    signal TxMergeFIFOReadAddr : std_logic_vector(3 downto 0);
    signal TxMergeFIFODataOut : std_logic_vector(21 downto 0);
    signal TxMergeFIFOCount : std_logic_vector(3 downto 0);
    signal TxMergeFIFOFull : std_logic;
    signal TxMergeFIFOEmpty : std_logic;
    signal TxMergeFIFO : ARRAY22;
    signal TxMergeFIFOWE : std_logic;
    signal TxMergeFIFORE : std_logic;
    signal TxMergeFIFOConn : std_logic_vector(3 downto 0);
    signal TxMergeFIFODataIn : std_logic_vector(17 downto 0);
    signal TxMergeFIFODataOutVal : std_logic_vector(17 downto 0);

    -- Per-connection state RAMs
    signal ConnectionState : ARRAY6;
    signal ConnectionStateVal : std_logic_vector(5 downto 0);
    signal ConnectionStateDataVal : std_logic_vector(5 downto 0);
    signal ConnectionStateTxDataVal : std_logic_vector(5 downto 0);
    signal ConnectionStateTxMergeDataVal : std_logic_vector(5 downto 0);
    
    signal RxFrameLength : ARRAY16;
    signal RxFrameLengthOdd : ARRAY1;
    signal RxFrameLengthNotZero : ARRAY1;
    signal RxFrameLengthOddDataVal : std_logic;
    signal RxFrameLengthDataVal : std_logic_vector(15 downto 0);
    signal RxFrameLengthNotZeroVal : std_logic;
    signal RxFrameLengthNotZeroDataVal : std_logic;
    signal RxFrameLengthNotZeroNextDataVal : std_logic;
    signal RxFrameLengthWE : std_logic;
    signal RxFrameLengthAddr : std_logic_vector(3 downto 0);
    signal RxFrameLengthData : std_logic_vector(15 downto 0);
    signal RxFrameLengthNotZeroData : std_logic;
    signal LastRxFrameLengthWE : std_logic;
    signal LastRxFrameLengthAddr : std_logic_vector(3 downto 0);
    signal LastRxFrameLengthData : std_logic_vector(15 downto 0);
    signal RxFrameLengthOddData : std_logic;
    
    signal RxBurstLength : ARRAY8;
    signal RxBurstLengthNotZero : ARRAY1;
    signal RxBurstLengthDataVal : std_logic_vector(7 downto 0);
    signal RxBurstLengthNotZeroVal : std_logic;
    signal RxBurstLengthNotZeroDataVal : std_logic;
    signal RxBurstLengthNotZeroNextDataVal : std_logic;
    signal RxBurstLengthWE : std_logic;
    signal RxBurstLengthAddr : std_logic_vector(3 downto 0);
    signal RxBurstLengthData : std_logic_vector(7 downto 0);
    signal RxBurstLengthNotZeroData : std_logic;
    
    signal TxFrameLength : ARRAY16;
    signal TxFrameLengthNotZero : ARRAY1;
    signal TxFrameLengthTxDataVal : std_logic_vector(15 downto 0);
    signal TxFrameLengthNotZeroVal : std_logic;
    signal TxFrameLengthNotZeroTxDataVal : std_logic;
    signal TxFrameLengthNotZeroNextTxDataVal : std_logic;
    signal TxFrameLengthWE : std_logic;
    signal TxFrameLengthAddr : std_logic_vector(3 downto 0);
    signal TxFrameLengthData : std_logic_vector(15 downto 0);
    signal TxFrameLengthNotZeroData : std_logic;

    signal TxBurstLength : ARRAY16;
    signal TxBurstLengthNotZero : ARRAY1;
    signal TxBurstLengthTxDataVal : std_logic_vector(15 downto 0);
    signal TxBurstLengthNotZeroTxDataVal : std_logic;
    signal TxBurstLengthNotZeroNextTxDataVal : std_logic;
    signal TxBurstLengthWE : std_logic;
    signal TxBurstLengthAddr : std_logic_vector(3 downto 0);
    signal TxBurstLengthData : std_logic_vector(15 downto 0);
    signal TxBurstLengthNotZeroData : std_logic;
    signal TxLastByte : ARRAY8;
    signal TxLastByteValid : ARRAY1;
    signal TxLastByteVal : std_logic_vector(7 downto 0);
    signal TxLastByteValidVal : std_logic;
    signal FrameLength : std_logic_vector(15 downto 0);
    signal DataByte : std_logic_vector(7 downto 0);
    signal DataByteValid : std_logic;
    
    signal User_LocalPortVal : std_logic_vector(15 downto 0);
    signal User_WriteRemoteIPAddrVal : std_logic_vector(31 downto 0);
    signal User_WriteRemotePortVal : std_logic_vector(15 downto 0);
    signal User_MTUTTLVal : std_logic_vector(15 downto 0);

    signal TxBufferEmpty : std_logic_vector(15 downto 0);

    signal InterruptLatency : ARRAY5;
    signal InterruptLatencyNotZero : ARRAY1;
    signal InterruptLatencyNotZeroVal : std_logic;
    
    -- FIFOs to/from user code
    signal FIFORST : std_logic_vector(15 downto 0);
    signal TxFIFODataOut : ARRAY18;
    signal TxFIFORE : std_logic_vector(15 downto 0);
    signal TxFIFOREData : std_logic_vector(15 downto 0);
    signal TxFIFORECtrl : std_logic_vector(15 downto 0);
    signal TxFIFOEmpty : std_logic_vector(15 downto 0);
    signal TxFIFONextEmpty : std_logic_vector(15 downto 0);
    signal TxFIFODataOutVal : std_logic_vector(17 downto 0);
    signal TxFIFODataOutTxDataVal : std_logic_vector(17 downto 0);
    signal TxFIFOEmptyVal : std_logic;
    signal TxFIFOEmptyTxVal : std_logic;
    signal TxFIFOEmptyNextTxVal : std_logic;
    
    signal RxFIFODataIn : std_logic_vector(17 downto 0);
    signal RxFIFOWE : std_logic_vector(15 downto 0);
    signal RxFIFOEmpty : std_logic_vector(15 downto 0);
    signal RxFIFOFull : std_logic_vector(15 downto 0);
    signal RxFIFOFullVal : std_logic;
    signal RxFIFOFullDataVal : std_logic;
    signal RxFIFOFullNextVal : std_logic;

begin

    --------------------------------------------------------------------------
    -- Convert individual signals to arrays and vice versa
    User_LinkStatus <= User_LinkStatusVal;
    User_LocalIPAddr <= User_LocalIPAddrVal;
    User_Enable <= User_Enable15 & User_Enable14 &
                   User_Enable13 & User_Enable12 &
                   User_Enable11 & User_Enable10 &
                   User_Enable9 & User_Enable8 &
                   User_Enable7 & User_Enable6 &
                   User_Enable5 & User_Enable4 &
                   User_Enable3 & User_Enable2 &
                   User_Enable1 & User_Enable0;
    User_Server <= User_Server15 & User_Server14 &
                   User_Server13 & User_Server12 &
                   User_Server11 & User_Server10 &
                   User_Server9 & User_Server8 &
                   User_Server7 & User_Server6 &
                   User_Server5 & User_Server4 &
                   User_Server3 & User_Server2 &
                   User_Server1 & User_Server0;
    User_TCP <= User_TCP15 & User_TCP14 &
                User_TCP13 & User_TCP12 &
                User_TCP11 & User_TCP10 &
                User_TCP9 & User_TCP8 &
                User_TCP7 & User_TCP6 &
                User_TCP5 & User_TCP4 &
                User_TCP3 & User_TCP2 &
                User_TCP1 & User_TCP0;
    User_Connected0 <= User_Connected(0);
    User_Connected1 <= User_Connected(1);
    User_Connected2 <= User_Connected(2);
    User_Connected3 <= User_Connected(3);
    User_Connected4 <= User_Connected(4);
    User_Connected5 <= User_Connected(5);
    User_Connected6 <= User_Connected(6);
    User_Connected7 <= User_Connected(7);
    User_Connected8 <= User_Connected(8);
    User_Connected9 <= User_Connected(9);
    User_Connected10 <= User_Connected(10);
    User_Connected11 <= User_Connected(11);
    User_Connected12 <= User_Connected(12);
    User_Connected13 <= User_Connected(13);
    User_Connected14 <= User_Connected(14);
    User_Connected15 <= User_Connected(15);
    User_MTUTTL(0) <= User_MTU0 & User_TTL0;
    User_MTUTTL(1) <= User_MTU1 & User_TTL1;
    User_MTUTTL(2) <= User_MTU2 & User_TTL2;
    User_MTUTTL(3) <= User_MTU3 & User_TTL3;
    User_MTUTTL(4) <= User_MTU4 & User_TTL4;
    User_MTUTTL(5) <= User_MTU5 & User_TTL5;
    User_MTUTTL(6) <= User_MTU6 & User_TTL6;
    User_MTUTTL(7) <= User_MTU7 & User_TTL7;
    User_MTUTTL(8) <= User_MTU8 & User_TTL8;
    User_MTUTTL(9) <= User_MTU9 & User_TTL9;
    User_MTUTTL(10) <= User_MTU10 & User_TTL10;
    User_MTUTTL(11) <= User_MTU11 & User_TTL11;
    User_MTUTTL(12) <= User_MTU12 & User_TTL12;
    User_MTUTTL(13) <= User_MTU13 & User_TTL13;
    User_MTUTTL(14) <= User_MTU14 & User_TTL14;
    User_MTUTTL(15) <= User_MTU15 & User_TTL15;
    User_LocalPort(0) <= User_LocalPort0;
    User_LocalPort(1) <= User_LocalPort1;
    User_LocalPort(2) <= User_LocalPort2;
    User_LocalPort(3) <= User_LocalPort3;
    User_LocalPort(4) <= User_LocalPort4;
    User_LocalPort(5) <= User_LocalPort5;
    User_LocalPort(6) <= User_LocalPort6;
    User_LocalPort(7) <= User_LocalPort7;
    User_LocalPort(8) <= User_LocalPort8;
    User_LocalPort(9) <= User_LocalPort9;
    User_LocalPort(10) <= User_LocalPort10;
    User_LocalPort(11) <= User_LocalPort11;
    User_LocalPort(12) <= User_LocalPort12;
    User_LocalPort(13) <= User_LocalPort13;
    User_LocalPort(14) <= User_LocalPort14;
    User_LocalPort(15) <= User_LocalPort15;
    User_WriteRemotePort(0) <= User_WriteRemotePort0;
    User_WriteRemotePort(1) <= User_WriteRemotePort1;
    User_WriteRemotePort(2) <= User_WriteRemotePort2;
    User_WriteRemotePort(3) <= User_WriteRemotePort3;
    User_WriteRemotePort(4) <= User_WriteRemotePort4;
    User_WriteRemotePort(5) <= User_WriteRemotePort5;
    User_WriteRemotePort(6) <= User_WriteRemotePort6;
    User_WriteRemotePort(7) <= User_WriteRemotePort7;
    User_WriteRemotePort(8) <= User_WriteRemotePort8;
    User_WriteRemotePort(9) <= User_WriteRemotePort9;
    User_WriteRemotePort(10) <= User_WriteRemotePort10;
    User_WriteRemotePort(11) <= User_WriteRemotePort11;
    User_WriteRemotePort(12) <= User_WriteRemotePort12;
    User_WriteRemotePort(13) <= User_WriteRemotePort13;
    User_WriteRemotePort(14) <= User_WriteRemotePort14;
    User_WriteRemotePort(15) <= User_WriteRemotePort15;
    User_WriteRemoteIPAddr(0) <= User_WriteRemoteIPAddr0;
    User_WriteRemoteIPAddr(1) <= User_WriteRemoteIPAddr1;
    User_WriteRemoteIPAddr(2) <= User_WriteRemoteIPAddr2;
    User_WriteRemoteIPAddr(3) <= User_WriteRemoteIPAddr3;
    User_WriteRemoteIPAddr(4) <= User_WriteRemoteIPAddr4;
    User_WriteRemoteIPAddr(5) <= User_WriteRemoteIPAddr5;
    User_WriteRemoteIPAddr(6) <= User_WriteRemoteIPAddr6;
    User_WriteRemoteIPAddr(7) <= User_WriteRemoteIPAddr7;
    User_WriteRemoteIPAddr(8) <= User_WriteRemoteIPAddr8;
    User_WriteRemoteIPAddr(9) <= User_WriteRemoteIPAddr9;
    User_WriteRemoteIPAddr(10) <= User_WriteRemoteIPAddr10;
    User_WriteRemoteIPAddr(11) <= User_WriteRemoteIPAddr11;
    User_WriteRemoteIPAddr(12) <= User_WriteRemoteIPAddr12;
    User_WriteRemoteIPAddr(13) <= User_WriteRemoteIPAddr13;
    User_WriteRemoteIPAddr(14) <= User_WriteRemoteIPAddr14;
    User_WriteRemoteIPAddr(15) <= User_WriteRemoteIPAddr15;
    User_ReadRemotePort0 <= User_ReadRemotePort(0);
    User_ReadRemotePort1 <= User_ReadRemotePort(1);
    User_ReadRemotePort2 <= User_ReadRemotePort(2);
    User_ReadRemotePort3 <= User_ReadRemotePort(3);
    User_ReadRemotePort4 <= User_ReadRemotePort(4);
    User_ReadRemotePort5 <= User_ReadRemotePort(5);
    User_ReadRemotePort6 <= User_ReadRemotePort(6);
    User_ReadRemotePort7 <= User_ReadRemotePort(7);
    User_ReadRemotePort8 <= User_ReadRemotePort(8);
    User_ReadRemotePort9 <= User_ReadRemotePort(9);
    User_ReadRemotePort10 <= User_ReadRemotePort(10);
    User_ReadRemotePort11 <= User_ReadRemotePort(11);
    User_ReadRemotePort12 <= User_ReadRemotePort(12);
    User_ReadRemotePort13 <= User_ReadRemotePort(13);
    User_ReadRemotePort14 <= User_ReadRemotePort(14);
    User_ReadRemotePort15 <= User_ReadRemotePort(15);
    User_ReadRemoteIPAddr0 <= User_ReadRemoteIPAddr(0);
    User_ReadRemoteIPAddr1 <= User_ReadRemoteIPAddr(1);
    User_ReadRemoteIPAddr2 <= User_ReadRemoteIPAddr(2);
    User_ReadRemoteIPAddr3 <= User_ReadRemoteIPAddr(3);
    User_ReadRemoteIPAddr4 <= User_ReadRemoteIPAddr(4);
    User_ReadRemoteIPAddr5 <= User_ReadRemoteIPAddr(5);
    User_ReadRemoteIPAddr6 <= User_ReadRemoteIPAddr(6);
    User_ReadRemoteIPAddr7 <= User_ReadRemoteIPAddr(7);
    User_ReadRemoteIPAddr8 <= User_ReadRemoteIPAddr(8);
    User_ReadRemoteIPAddr9 <= User_ReadRemoteIPAddr(9);
    User_ReadRemoteIPAddr10 <= User_ReadRemoteIPAddr(10);
    User_ReadRemoteIPAddr11 <= User_ReadRemoteIPAddr(11);
    User_ReadRemoteIPAddr12 <= User_ReadRemoteIPAddr(12);
    User_ReadRemoteIPAddr13 <= User_ReadRemoteIPAddr(13);
    User_ReadRemoteIPAddr14 <= User_ReadRemoteIPAddr(14);
    User_ReadRemoteIPAddr15 <= User_ReadRemoteIPAddr(15);
    User_TxPause <= User_TxPause15 & User_TxPause14 &
                    User_TxPause13 & User_TxPause12 &
                    User_TxPause11 & User_TxPause10 &
                    User_TxPause9 & User_TxPause8 &
                    User_TxPause7 & User_TxPause6 &
                    User_TxPause5 & User_TxPause4 &
                    User_TxPause3 & User_TxPause2 &
                    User_TxPause1 & User_TxPause0;
    User_TxPaused0 <= User_TxPaused(0);
    User_TxPaused1 <= User_TxPaused(1);
    User_TxPaused2 <= User_TxPaused(2);
    User_TxPaused3 <= User_TxPaused(3);
    User_TxPaused4 <= User_TxPaused(4);
    User_TxPaused5 <= User_TxPaused(5);
    User_TxPaused6 <= User_TxPaused(6);
    User_TxPaused7 <= User_TxPaused(7);
    User_TxPaused8 <= User_TxPaused(8);
    User_TxPaused9 <= User_TxPaused(9);
    User_TxPaused10 <= User_TxPaused(10);
    User_TxPaused11 <= User_TxPaused(11);
    User_TxPaused12 <= User_TxPaused(12);
    User_TxPaused13 <= User_TxPaused(13);
    User_TxPaused14 <= User_TxPaused(14);
    User_TxPaused15 <= User_TxPaused(15);
    User_TxData(0) <= User_TxData0;
    User_TxData(1) <= User_TxData1;
    User_TxData(2) <= User_TxData2;
    User_TxData(3) <= User_TxData3;
    User_TxData(4) <= User_TxData4;
    User_TxData(5) <= User_TxData5;
    User_TxData(6) <= User_TxData6;
    User_TxData(7) <= User_TxData7;
    User_TxData(8) <= User_TxData8;
    User_TxData(9) <= User_TxData9;
    User_TxData(10) <= User_TxData10;
    User_TxData(11) <= User_TxData11;
    User_TxData(12) <= User_TxData12;
    User_TxData(13) <= User_TxData13;
    User_TxData(14) <= User_TxData14;
    User_TxData(15) <= User_TxData15;
    User_TxEnable <= User_TxEnable15 & User_TxEnable14 &
                     User_TxEnable13 & User_TxEnable12 &
                     User_TxEnable11 & User_TxEnable10 &
                     User_TxEnable9 & User_TxEnable8 &
                     User_TxEnable7 & User_TxEnable6 &
                     User_TxEnable5 & User_TxEnable4 &
                     User_TxEnable3 & User_TxEnable2 &
                     User_TxEnable1 & User_TxEnable0;
    User_TxBusy0 <= User_TxBusy(0);
    User_TxBusy1 <= User_TxBusy(1);
    User_TxBusy2 <= User_TxBusy(2);
    User_TxBusy3 <= User_TxBusy(3);
    User_TxBusy4 <= User_TxBusy(4);
    User_TxBusy5 <= User_TxBusy(5);
    User_TxBusy6 <= User_TxBusy(6);
    User_TxBusy7 <= User_TxBusy(7);
    User_TxBusy8 <= User_TxBusy(8);
    User_TxBusy9 <= User_TxBusy(9);
    User_TxBusy10 <= User_TxBusy(10);
    User_TxBusy11 <= User_TxBusy(11);
    User_TxBusy12 <= User_TxBusy(12);
    User_TxBusy13 <= User_TxBusy(13);
    User_TxBusy14 <= User_TxBusy(14);
    User_TxBusy15 <= User_TxBusy(15);
    User_TxBufferEmpty0 <= User_TxBufferEmpty(0);
    User_TxBufferEmpty1 <= User_TxBufferEmpty(1);
    User_TxBufferEmpty2 <= User_TxBufferEmpty(2);
    User_TxBufferEmpty3 <= User_TxBufferEmpty(3);
    User_TxBufferEmpty4 <= User_TxBufferEmpty(4);
    User_TxBufferEmpty5 <= User_TxBufferEmpty(5);
    User_TxBufferEmpty6 <= User_TxBufferEmpty(6);
    User_TxBufferEmpty7 <= User_TxBufferEmpty(7);
    User_TxBufferEmpty8 <= User_TxBufferEmpty(8);
    User_TxBufferEmpty9 <= User_TxBufferEmpty(9);
    User_TxBufferEmpty10 <= User_TxBufferEmpty(10);
    User_TxBufferEmpty11 <= User_TxBufferEmpty(11);
    User_TxBufferEmpty12 <= User_TxBufferEmpty(12);
    User_TxBufferEmpty13 <= User_TxBufferEmpty(13);
    User_TxBufferEmpty14 <= User_TxBufferEmpty(14);
    User_TxBufferEmpty15 <= User_TxBufferEmpty(15);
    User_RxData0 <= User_RxData(0);
    User_RxData1 <= User_RxData(1);
    User_RxData2 <= User_RxData(2);
    User_RxData3 <= User_RxData(3);
    User_RxData4 <= User_RxData(4);
    User_RxData5 <= User_RxData(5);
    User_RxData6 <= User_RxData(6);
    User_RxData7 <= User_RxData(7);
    User_RxData8 <= User_RxData(8);
    User_RxData9 <= User_RxData(9);
    User_RxData10 <= User_RxData(10);
    User_RxData11 <= User_RxData(11);
    User_RxData12 <= User_RxData(12);
    User_RxData13 <= User_RxData(13);
    User_RxData14 <= User_RxData(14);
    User_RxData15 <= User_RxData(15);
    User_RxEnable0 <= User_RxEnable(0);
    User_RxEnable1 <= User_RxEnable(1);
    User_RxEnable2 <= User_RxEnable(2);
    User_RxEnable3 <= User_RxEnable(3);
    User_RxEnable4 <= User_RxEnable(4);
    User_RxEnable5 <= User_RxEnable(5);
    User_RxEnable6 <= User_RxEnable(6);
    User_RxEnable7 <= User_RxEnable(7);
    User_RxEnable8 <= User_RxEnable(8);
    User_RxEnable9 <= User_RxEnable(9);
    User_RxEnable10 <= User_RxEnable(10);
    User_RxEnable11 <= User_RxEnable(11);
    User_RxEnable12 <= User_RxEnable(12);
    User_RxEnable13 <= User_RxEnable(13);
    User_RxEnable14 <= User_RxEnable(14);
    User_RxEnable15 <= User_RxEnable(15);
    User_RxBusy <= User_RxBusy15 & User_RxBusy14 &
                   User_RxBusy13 & User_RxBusy12 &
                   User_RxBusy11 & User_RxBusy10 &
                   User_RxBusy9 & User_RxBusy8 &
                   User_RxBusy7 & User_RxBusy6 &
                   User_RxBusy5 & User_RxBusy4 &
                   User_RxBusy3 & User_RxBusy2 &
                   User_RxBusy1 & User_RxBusy0;

    --------------------------------------------------------------------------
    -- There are two types of GigEx access - control and data
    -- Multiplex between the two here
    UserWE <= CtrlWE or DataWE;
    UserRE <= CtrlRE or DataRE;
    UserAddr <= CtrlAddr when CtrlWE='1' or CtrlRE='1' else DataAddr;
    UserBE <= CtrlBE when CtrlWE='1' or CtrlRE='1' else DataBE;
    UserWriteData <= CtrlWriteData when CtrlWE='1' or CtrlRE='1' else DataWriteData;
    UserOwner <= CtrlOwner when CtrlRE='1' else DataOwner;

    --------------------------------------------------------------------------
    -- Distributed RAM reads
    NextCtrlConnectionPlus1 <= NextCtrlConnection+1;
    
    User_EnableVal <= User_Enable(conv_integer(CtrlConnection));
    User_RegEnableVal <= User_RegEnable(conv_integer(CtrlConnection));

    ConnectionStateVal <= ConnectionState(conv_integer(CtrlConnection));
    ConnectionStateDataVal <= ConnectionState(conv_integer(DataConnection(3 downto 0)));
    ConnectionStateTxDataVal <= ConnectionState(conv_integer(TxConnection));
    ConnectionStateTxMergeDataVal <= ConnectionState(conv_integer(TxMergeFIFOConn));
    
    RxFrameLengthOddDataVal <= RxFrameLengthOdd(conv_integer(DataConnection(3 downto 0)));
    RxFrameLengthDataVal <= RxFrameLength(conv_integer(DataConnection(3 downto 0)));
    RxFrameLengthNotZeroVal <= RxFrameLengthNotZero(conv_integer(CtrlConnection));
    RxFrameLengthNotZeroDataVal <= RxFrameLengthNotZero(conv_integer(DataConnection(3 downto 0)));
    RxFrameLengthNotZeroNextDataVal <= RxFrameLengthNotZero(conv_integer(NextDataConnection(3 downto 0)));
    
    RxBurstLengthDataVal <= RxBurstLength(conv_integer(DataConnection(3 downto 0)));
    RxBurstLengthNotZeroVal <= RxBurstLengthNotZero(conv_integer(CtrlConnection));
    RxBurstLengthNotZeroDataVal <= RxBurstLengthNotZero(conv_integer(DataConnection(3 downto 0)));
    RxBurstLengthNotZeroNextDataVal <= RxBurstLengthNotZero(conv_integer(NextDataConnection(3 downto 0)));

    TxFrameLengthTxDataVal <= TxFrameLength(conv_integer(TxConnection));
    TxFrameLengthNotZeroVal <= TxFrameLengthNotZero(conv_integer(CtrlConnection));
    TxFrameLengthNotZeroTxDataVal <= TxFrameLengthNotZero(conv_integer(TxConnection));
    TxFrameLengthNotZeroNextTxDataVal <= TxFrameLengthNotZero(conv_integer(NextTxConnection));

    TxBurstLengthTxDataVal <= TxBurstLength(conv_integer(TxConnection));
    TxBurstLengthNotZeroTxDataVal <= TxBurstLengthNotZero(conv_integer(TxConnection));
    TxBurstLengthNotZeroNextTxDataVal <= TxBurstLengthNotZero(conv_integer(NextTxConnection));
    TxLastByteVal <= TxLastByte(conv_integer(CtrlConnection));
    TxLastByteValidVal <= TxLastByteValid(conv_integer(CtrlConnection));
    
    InterruptLatencyNotZeroVal <= InterruptLatencyNotZero(conv_integer(CtrlConnection));
    
    TxFIFODataOutVal <= TxFIFODataOut(conv_integer(CtrlConnection));
    TxFIFODataOutTxDataVal <= TxFIFODataOut(conv_integer(TxConnection));
    TxFIFOEmptyNextTxVal <= TxFIFOEmpty(conv_integer(NextTxConnection));
    
    RxFIFOFullVal <= RxFIFOFull(conv_integer(CtrlConnection));
    RxFIFOFullNextVal <= RxFIFOFull(conv_integer(NextDataConnection(3 downto 0)));

    --------------------------------------------------------------------------
    -- State machine to control GigExpedite
    process (User_RST, User_CLK)
    begin
        if (User_RST='1') then
            CtrlState <= DELAY_INIT;
            Delay <= (others=>'0');
            DelayDone <= '0';
            CtrlSubState <= (others=>'0');
            UserValidCount <= (others=>'0');
            CtrlConnection <= (others=>'0');
            CtrlConnectionMask <= X"0001";
            CleanDone <= '0';
            User_Connected <= (others=>'0');
            User_RegEnable <= (others=>'0');
            User_EnableEdge <= (others=>'0');
            User_EnableEdgeVal <= '0';
            LastUser_Enable <= (others=>'0');
            User_TxPauseEdge <= (others=>'0');
            LastUser_TxPause <= (others=>'0');
            SinceStatusUpdate <= (others=>'0');
            SinceStatusUpdateDone <= '0';
            User_LinkStatusVal <= '0';
            User_LinkSpeed <= (others=>'0');
            User_LinkDuplex <= '0';
            User_LocalIPAddrVal <= (others=>'0');
            LinkUpCount <= (others=>'0');
            BlockDataConnection <= (others=>'0');
            BlockTxMergeFIFO <= '0';
            TxFIFOEmptyVal <= '1';
            for i in 0 to 15 loop
                ConnState(i) <= CS_DISABLED;
            end loop;
        elsif (User_CLK'event and User_CLK='1') then
            -- Timers
            Delay <= Delay + 1;
            if (Delay=X"ffffff") then
                DelayDone <= '1';
            end if;
            if (SinceStatusUpdate/=X"ffff") then
                SinceStatusUpdate <= SinceStatusUpdate + 1;
                SinceStatusUpdateDone <= '0';
            else
                SinceStatusUpdateDone <= '1';
            end if;
            
            -- Counter of completed control register reads
            if (UserReadDataValid='1' and UserValidOwner(7)='0') then
                UserValidCount <= UserValidCount + 1;
            end if;
            
            -- Detect edges on Enable and TxPause signal
            -- Delay until link is detected so that the GigEx is known to
            -- be booted.  This will hold off connections if the Xilinx FPGA
            -- boots from flash.
            if (DelayDone='1' and LinkUpCount=X"FF") then
                LastUser_Enable <= User_Enable;
                User_EnableEdge <= User_EnableEdge or (LastUser_Enable xor User_Enable);
                LastUser_TxPause <= User_TxPause;
                User_TxPauseEdge <= User_TxPauseEdge or (LastUser_TxPause xor User_TxPause);
            end if;
            
            -- Register empty signal from Tx FIFOs
            TxFIFOEmptyVal <= TxFIFONextEmpty(conv_integer(CtrlConnection));
            
            CtrlSubState <= CtrlSubState + 1;
            case (CtrlState) is
                -- Wait here to ensure the GigExpedite has locked to
                -- our clock
                when DELAY_INIT =>
                    if (DelayDone='1') then
                        CtrlSubState <= (others=>'0');
                        CtrlState <= CLEAN;
                    end if;

                -- Reset all connections in case the previous
                -- application closed unexpectedly and left some open
                -- Write 0 to each connection state then
                -- read the status for each connection in turn
                -- Loop round until all statuses are zero.
                when CLEAN =>
                    if (CtrlSubState(4 downto 0)="11111") then
                        CtrlSubState <= (others=>'0');
                        UserValidCount <= (others=>'0');
                        CtrlState <= CLEAN_CHECK;
                        CleanDone <= '1';
                    end if;
                when CLEAN_CHECK =>
                    if (UserReadDataValid='1' and UserReadData/=X"0000") then
                        CleanDone <= '0';
                    end if;
                    if (CtrlSubState(8 downto 0)="111111111") then
                        if (CleanDone='1') then
                            CtrlSubState <= (others=>'0');
                            CtrlState <= CLEAR_INTS;
                        else
                            CleanDone <= '1';
                            CtrlSubState <= (others=>'0');
                        end if;
                    end if;
                when CLEAR_INTS =>
                    if (CtrlSubState(8 downto 0)="111111111") then
                        CtrlSubState <= (others=>'0');
                        CtrlState <= IDLE;
                    end if;
                
                -- Wait for interrupt from GigExpedite device
                -- or request from user interface
                when IDLE =>
                    CtrlSubState <= (others=>'0');
                    UserValidCount <= (others=>'0');
                    if (SinceStatusUpdateDone='1') then
                        SinceStatusUpdate <= (others=>'0');
                        SinceStatusUpdateDone <= '0';
                        CtrlState <= GET_STATUS;
                    elsif (User_TxPauseEdge/=X"0000" or User_EnableEdge/=X"0000") then
                        -- User has requested a connection open/close or pause/unpause
                        for i in 0 to 15 loop
                            if (User_TxPauseEdge(i)/='0' or User_EnableEdge(i)/='0') then
                                CtrlConnection <= conv_std_logic_vector(i, 4);
                                CtrlConnectionMask <= conv_std_logic_vector(2**i, 16);
                                User_EnableEdgeVal <= User_EnableEdge(i);
                            end if;
                        end loop;
                        CtrlState <= USER_STATE_CHANGE_REQ;
                    elsif (UserInterrupt='1') then
                        -- Interrupt has been received
                        CtrlConnection <= (others=>'0');
                        CtrlConnectionMask <= X"0001";
                        CtrlState <= CHECK_STATE;
                    end if;

                -- Update link status
                when GET_STATUS => 
                    if (UserReadDataValid='1' and UserValidOwner(7)='0') then
                        if (UserValidCount(1 downto 0)="00") then
                            User_LocalIPAddrVal(15 downto 0) <= UserReadData;
                        elsif (UserValidCount(1 downto 0)="01") then
                            User_LocalIPAddrVal(31 downto 16) <= UserReadData;
                        elsif (UserValidCount(1 downto 0)="10") then
                            if (User_LocalIPAddrVal/=X"00000000") then
                                if (LinkUpCount/=X"ff") then
                                    LinkUpCount <= LinkUpCount + 1;
                                end if;
                            else
                                LinkUpCount <= X"00";
                            end if;
                            User_LinkStatusVal <= UserReadData(4);
                            User_LinkDuplex <= UserReadData(3);
                            User_LinkSpeed <= UserReadData(2 downto 0);
                            CtrlSubState <= (others=>'0');
                            CtrlState <= IDLE;
                        end if;
                    end if;
                    
                -- User request to open/close a connection
                when USER_STATE_CHANGE_REQ =>
                    -- Change the connection state
                    CtrlSubState <= (others=>'0');
                    UserValidCount <= (others=>'0');
                    if (User_EnableEdgeVal/='0') then
                        if (UserInterrupt='1') then
                            -- Default state if can't service state change - check interrupt
                            CtrlConnection <= (others=>'0');
                            CtrlConnectionMask <= X"0001";
                            CtrlState <= CHECK_STATE;
                        else
                            CtrlState <= IDLE;
                        end if;
                        if (User_EnableVal='0' or
                            (User_RegEnableVal='1' and User_EnableVal='1')) then
                            -- Close or close then re-open connection
                            User_EnableEdge(conv_integer(CtrlConnection)) <= User_EnableVal;
                            User_RegEnable(conv_integer(CtrlConnection)) <= '0';
                            if (ConnState(conv_integer(CtrlConnection))/=CS_WAIT_DISABLED and
                                ConnState(conv_integer(CtrlConnection))/=CS_DISABLED) then
                                -- OK to service close req
                                CtrlConnection <= CtrlConnection;
                                CtrlState <= CLOSE_CONNECTION;
                            end if;
                        else
                            if (ConnState(conv_integer(CtrlConnection))=CS_DISABLED) then
                                -- Open closed connection
                                CtrlConnection <= CtrlConnection;
                                User_EnableEdge(conv_integer(CtrlConnection)) <= '0';
                                User_RegEnable(conv_integer(CtrlConnection)) <= '1';
                                User_LocalPortVal <= User_LocalPort(conv_integer(CtrlConnection));
                                User_WriteRemoteIPAddrVal <= User_WriteRemoteIPAddr(conv_integer(CtrlConnection));
                                User_WriteRemotePortVal <= User_WriteRemotePort(conv_integer(CtrlConnection));
                                User_MTUTTLVal <= User_MTUTTL(conv_integer(CtrlConnection));
                                CtrlState <= OPEN_CONNECTION;
                            end if;
                        end if;
                    else
                        -- Pause/unpause
                        User_TxPauseEdge(conv_integer(CtrlConnection)) <= '0';
                        CtrlState <= PAUSE_UNPAUSE_CONNECTION;
                    end if;
                when OPEN_CONNECTION =>
                    if (CtrlSubState(2 downto 0)="110") then
                        CtrlSubState <= (others=>'0');
                        CtrlState <= IDLE;
                        ConnState(conv_integer(CtrlConnection)) <= CS_WAIT_ESTAB;
                    end if;
                when CLOSE_CONNECTION =>
                    if (CtrlSubState(0)='1') then
                        CtrlSubState <= (others=>'0');
                        CtrlState <= IDLE;
                        ConnState(conv_integer(CtrlConnection)) <= CS_WAIT_CLOSE;
                    end if;
                when PAUSE_UNPAUSE_CONNECTION =>
                    CtrlSubState <= (others=>'0');
                    User_TxPaused(conv_integer(CtrlConnection)) <= 
                            User_TxPause(conv_integer(CtrlConnection));
                    CtrlState <= IDLE;
                
                -- Read state and interrupt status bits
                when CHECK_STATE =>
                    if (UserReadDataValid='1' and UserValidOwner(7)='0') then
                        if (UserValidCount(0)='0') then
                            -- Store connection state
                            CurrentState <= UserReadData(5 downto 0);
                        else
                            -- Store interrupt state
                            InterruptState <= UserReadData(3 downto 0);
                            CtrlSubState <= (others=>'0');
                            CtrlState <= PROCESS_INTERRUPTS;
                            if (UserReadData(IE_OUTGOING_NOT_FULL_BIT)='1') then
                                BlockTxMergeFIFO <= '1';
                            end if;
                        end if;
                    end if;
                    if (CtrlSubState(4 downto 0)="10000") then
                        BlockDataConnection <= '1' & CtrlConnection;
                    end if;
                    
                -- Process interrupt from GigExpedite
                when PROCESS_INTERRUPTS =>
                    CtrlSubState <= (others=>'0');
                    UserValidCount <= (others=>'0');
                    BlockTxMergeFIFO <= '0';
                    if (InterruptState(IE_OUTGOING_NOT_FULL_BIT)='1') then
                        -- Buffer not full interrupt
                        InterruptState(IE_OUTGOING_NOT_FULL_BIT) <= '0';
                        if (ConnectionStateVal(CONN_ENABLE_BIT)='1' and
                            ConnectionStateVal(CONN_TCP_BIT)='0' and
                            TxFrameLengthNotZeroVal='0' and
                            TxFIFOEmptyVal='0') then
                            BlockTxMergeFIFO <= '1';
                            BlockDataConnection <= (others=>'0');
                            CtrlState <= WRITE_LENGTH;
                        end if;
                    elsif (InterruptState(IE_INCOMING_BIT)='1') then
                        -- Data available interrupt
                        InterruptState(IE_INCOMING_BIT) <= '0';
                        if (ConnectionStateVal(CONN_ENABLE_BIT)='1' and
                            InterruptLatencyNotZeroVal='0' and
                            (ConnectionStateVal(CONN_TCP_BIT)='1' or
                             RxFIFOFullVal='0') and
                            (RxBurstLengthNotZeroVal='0' or
                             RxFrameLengthNotZeroVal='0')) then
                            CtrlState <= READ_LENGTH;
                        end if;
                    elsif (InterruptState(IE_OUTGOING_EMPTY_BIT)='1') then
                        -- Buffer empty interrupt
                        InterruptState(IE_OUTGOING_EMPTY_BIT) <= '0';
                    elsif (InterruptState(IE_STATE_BIT)='1') then
                        -- State change interrupt
                        InterruptState(IE_STATE_BIT) <= '0';
                        BlockTxMergeFIFO <= '1';
                        CtrlState <= STATE_CHANGE;
                    elsif (NextCtrlConnectionValid='1') then
                        -- Move to next connection
                        CtrlConnection <= NextCtrlConnection;
                        CtrlConnectionMask <= NextCtrlConnectionMask;
                        if (NextCtrlConnection=X"0") then
                            CtrlState <= IDLE;
                        else
                            CtrlState <= CHECK_STATE;
                        end if;
                        BlockDataConnection <= (others=>'0');
                    end if;

                -- Handle state change interrupt
                when STATE_CHANGE =>
                    if (CtrlSubState(3 downto 0)/=X"0" or
                        not (UserReadDataValid='1' and UserValidOwner(6)='1')) then
                        CtrlSubState <= CtrlSubState + 1;
                        BlockTxMergeFIFO <= '0';
                    end if;
                        
                    if (CurrentState(3 downto 0)=ESTABLISHED and
                        ConnState(conv_integer(CtrlConnection))=CS_WAIT_ESTAB) then
                        -- Connection made
                        if (UserReadDataValid='1' and UserValidOwner(7)='0') then
                            if (UserValidCount(2 downto 0)="000") then
                                User_ReadRemotePort(conv_integer(CtrlConnection)) <= UserReadData;
                            end if;
                            if (UserValidCount(2 downto 0)="001") then
                                User_ReadRemoteIPAddr(conv_integer(CtrlConnection))(31 downto 16) <= UserReadData;
                            end if;
                            if (UserValidCount(2 downto 0)="010") then
                                User_ReadRemoteIPAddr(conv_integer(CtrlConnection))(15 downto 0) <= UserReadData;
                            end if;
                        end if;
                        if (UserValidCount(2 downto 0)="010" and
                            CtrlSubState(3 downto 0)>=X"8") then
                            CtrlSubState <= (others=>'0');
                            CtrlState <= PROCESS_INTERRUPTS;
                            User_Connected(conv_integer(CtrlConnection)) <= '1';
                            ConnState(conv_integer(CtrlConnection)) <= CS_ESTAB;
                        end if;
                    elsif (CurrentState(3 downto 0)=CLOSED(3 downto 0) and
                           CurrentState(5)='1' and
                           (ConnState(conv_integer(CtrlConnection))=CS_ESTAB or
                            ConnState(conv_integer(CtrlConnection))=CS_WAIT_CLOSE)) then
                        -- Connection closed by remote end
                        if (CtrlSubState(1 downto 0)="10") then
                            CtrlSubState <= (others=>'0');
                            CtrlState <= PROCESS_INTERRUPTS;
                            ConnState(conv_integer(CtrlConnection)) <= CS_WAIT_DISABLED;
                        end if;
                    elsif (CurrentState(5)='0') then
                        -- Connection disabled
                        if (CtrlSubState(1 downto 0)="10") then
                            CtrlSubState <= (others=>'0');
                            CtrlState <= PROCESS_INTERRUPTS;
                            User_Connected(conv_integer(CtrlConnection)) <= '0';
                            ConnState(conv_integer(CtrlConnection)) <= CS_DISABLED;
                        end if;
                    else
                        -- Listen or connect state acknowledgement
                        CtrlSubState <= (others=>'0');
                        CtrlState <= PROCESS_INTERRUPTS;
                    end if;
                
                -- Handle incoming frame length
                when READ_LENGTH =>
                    if (RxFrameLengthNotZeroVal='1') then
                        -- Partially read frame - new burst available
                        CtrlSubState <= (others=>'0');
                        CtrlState <= PROCESS_INTERRUPTS;
                    elsif (UserReadDataValid='1' and UserValidOwner(6)='1' and
                           UserValidOwner(3 downto 0)=CtrlConnection) then
                        -- Read frame length from GigExpedite
                        CtrlSubState <= (others=>'0');
                        CtrlState <= PROCESS_INTERRUPTS;
                    end if;

                -- Copy datagram length from user FIFO to GigExpedite
                -- This complicated by the length being 2 bytes
                -- The first byte may be the lower half of the last
                -- data word or the upper byte from the TxFIFO
                -- The second byte may be the upper byte from the TxFIFO
                -- or the lower byte from the TxFIFO
                -- or the upper byte of the next word in the TxFIFO
                -- The lower byte in the TxFIFO may be data that has to
                -- be written to the GigExpedite data FIFO
                when WRITE_LENGTH =>
                    if (TxMergeFIFOEmpty='0') then
                        -- Wait for the merge FIFO to empty to prevent
                        -- writing the frame length before the last data
                        -- BlockTxMergeFIFO=1 prevents writes to TxMergeFIFO, but the registering
                        -- of TxMergeFIFOEmpty means it could go high then low, before going high and
                        -- staying high for duration of this state.
                        -- To make sure TxMergeFIFO really is empty, start rest of this state at
                        -- CtrlSubState=2
                        CtrlSubState <= X"0000";
                    elsif (CtrlSubState(2 downto 0)="010") then --2
                        DataByteValid <= '0';
                        if (TxLastByteValidVal='1') then
                            -- There is a byte left over from the previous
                            -- data word that will form the top 8 bits of the
                            -- frame length
                            FrameLength(15 downto 8) <= TxLastByteVal;
                            FrameLength(7 downto 0) <= TxFIFODataOutVal(15 downto 8);
                            DataByteValid <= TxFIFODataOutVal(16);
                            DataByte <= TxFIFODataOutVal(7 downto 0);
                            CtrlSubState <= X"0004";
                        elsif (TxFIFODataOutVal(17 downto 16)="11") then
                            -- Complete frame length in 2 byte word
                            FrameLength <= TxFIFODataOutVal(15 downto 0);
                            CtrlSubState <= X"0004";
                        else
                            -- Half of frame length from top byte
                            FrameLength(15 downto 8) <= TxFIFODataOutVal(15 downto 8);
                        end if;
                    elsif (CtrlSubState(2 downto 0)="011") then --3
                        if (TxFIFOEmptyVal='1') then
                            CtrlSubState <= X"0003";
                        else
                            -- Fill in bottom of frame length
                            DataByteValid <= TxFIFODataOutVal(16);
                            DataByte <= TxFIFODataOutVal(7 downto 0);
                        end if;
                        FrameLength(7 downto 0) <= TxFIFODataOutVal(15 downto 8);
                    elsif (CtrlSubState(2 downto 0)="100") then --4
                        -- Write frame length to GigExpedite here
                        if (DataByteValid='0') then
                            BlockDataConnection <= '1' & CtrlConnection;
                            CtrlSubState <= (others=>'0');
                            CtrlState <= PROCESS_INTERRUPTS;
                        else
                            -- We will be writing an extra byte from this word
                            -- to the data FIFO
                            FrameLength <= FrameLength - 1;
                        end if;
                    elsif (CtrlSubState(2 downto 0)="101") then --5
                        -- Write extra byte from length word to data FIFO here
                        BlockDataConnection <= '1' & CtrlConnection;
                        CtrlSubState <= (others=>'0');
                        CtrlState <= PROCESS_INTERRUPTS;
                    end if;

                when others => null;
            end case;
        end if;
    end process;
    
    -- Map control states to GigExpedite register accesses
    process (User_CLK)
    begin
        if (User_CLK'event and User_CLK='1') then
            CtrlOwner(7) <= '0';                  -- Used to indicate a data read
            CtrlOwner(6) <= '0';                  -- Used to block data accesses
            CtrlOwner(5 downto 4) <= CtrlBE;      -- Byte valid flags
            CtrlOwner(3 downto 0) <= CtrlConnection;
            
            case (CtrlState) is
                when CLEAN =>
                    -- Reset all connections
                    CtrlRE <= '0';
                    CtrlWE <= '1';
                    CtrlBE <= "11";
                    if (CtrlSubState(0)='0') then
                        CtrlAddr <= CONNECTION_PAGE;
                        CtrlWriteData <= X"000" & CtrlSubState(4 downto 1);
                    else
                        CtrlAddr <= CONNECTION_STATE;
                        CtrlWriteData <= X"0000";
                    end if;
                
                when CLEAN_CHECK =>
                    -- Check all connections have been reset
                    CtrlBE <= "11";
                    if (CtrlSubState(4 downto 0)="00000") then
                        CtrlRE <= '0';
                        CtrlWE <= '1';
                        CtrlAddr <= CONNECTION_PAGE;
                    elsif (CtrlSubState(4 downto 0)="10000") then
                        CtrlRE <= '1';
                        CtrlWE <= '0';
                        CtrlAddr <= CONNECTION_STATE;
                    else
                        CtrlRE <= '0';
                        CtrlWE <= '0';
                    end if;
                    CtrlWriteData <= X"000" & CtrlSubState(8 downto 5);
                when CLEAR_INTS =>
                    -- Clear state change interrupts
                    CtrlBE <= "11";
                    if (CtrlSubState(4 downto 0)="00000") then
                        CtrlRE <= '0';
                        CtrlWE <= '1';
                        CtrlAddr <= CONNECTION_PAGE;
                    elsif (CtrlSubState(4 downto 0)="10000") then
                        CtrlRE <= '1';
                        CtrlWE <= '0';
                        CtrlAddr <= INTERRUPT_ENABLE_STATUS;
                    else
                        CtrlRE <= '0';
                        CtrlWE <= '0';
                    end if;
                    CtrlWriteData <= X"000" & CtrlSubState(8 downto 5);
                
                when GET_STATUS => 
                    -- Read link status
                    if (CtrlSubState(3 downto 0)<=X"2") then
                        CtrlRE <= '1';
                    else
                        CtrlRE <= '0';
                    end if;
                    if (CtrlSubState(1 downto 0)="00") then
                        CtrlAddr <= LOCAL_IP_ADDR_LOW;
                    elsif (CtrlSubState(1 downto 0)="01") then
                        CtrlAddr <= LOCAL_IP_ADDR_HIGH;
                    else
                        CtrlAddr <= LINK_STATUS;
                    end if;
                    CtrlWE <= '0';
                    CtrlBE <= "11";
                    CtrlWriteData <= X"0000";
                    
                when USER_STATE_CHANGE_REQ =>
                    -- Change connection to the requested one for state change
                    CtrlRE <= '0';
                    CtrlWE <= '1';
                    CtrlBE <= "11";
                    CtrlAddr <= CONNECTION_PAGE;
                    CtrlWriteData <= X"000" & CtrlConnection;
                
                when OPEN_CONNECTION =>
                    -- Write settings to GigExpedite connection registers
                    CtrlRE <= '0';
                    CtrlWE <= '1';
                    CtrlBE <= "11";
                    if (CtrlSubState(2 downto 0)="000") then
                        CtrlAddr <= LOCAL_PORT;
                        CtrlWriteData <= User_LocalPortVal;
                    elsif (CtrlSubState(2 downto 0)="001") then
                        CtrlAddr <= REMOTE_IP_ADDR_LOW;
                        CtrlWriteData <= User_WriteRemoteIPAddrVal(15 downto 0);
                    elsif (CtrlSubState(2 downto 0)="010") then
                        CtrlAddr <= REMOTE_IP_ADDR_HIGH;
                        CtrlWriteData <= User_WriteRemoteIPAddrVal(31 downto 16);
                    elsif (CtrlSubState(2 downto 0)="011") then
                        CtrlAddr <= REMOTE_PORT;
                        CtrlWriteData <= User_WriteRemotePortVal;
                    elsif (CtrlSubState(2 downto 0)="100") then
                        CtrlAddr <= MTU_TTL;
                        CtrlWriteData <= User_MTUTTLVal;
                    elsif (CtrlSubState(2 downto 0)="101") then
                        CtrlAddr <= INTERRUPT_ENABLE_STATUS;
                        CtrlWriteData <= IE_STATE;
                    else
                        CtrlAddr <= CONNECTION_STATE;
                        if (User_TCP(conv_integer(CtrlConnection))='1') then
                            if (User_Server(conv_integer(CtrlConnection))='1') then
                                CtrlWriteData <= CONN_ENABLE or CONN_TCP or LISTEN;
                            else
                                CtrlWriteData <= CONN_ENABLE or CONN_TCP or CONNECT;
                            end if;
                        else
                            CtrlWriteData <= CONN_ENABLE or ESTABLISHED;
                        end if;
                    end if;

                when CLOSE_CONNECTION =>
                    -- Write settings to GigExpedite connection registers
                    CtrlRE <= '0';
                    if (ConnectionStateVal/="000000") then
                        CtrlWE <= '1';
                    else
                        CtrlWE <= '0';
                    end if;
                    CtrlBE <= "11";
                    if (CtrlSubState(0)='0') then
                        CtrlAddr <= CONNECTION_STATE;
                        CtrlWriteData <= X"000" & CLOSED(3 downto 0);
                    else
                        CtrlAddr <= INTERRUPT_ENABLE_STATUS;
                        CtrlWriteData <= IE_INCOMING or IE_OUTGOING_EMPTY or
                                         IE_OUTGOING_NOT_FULL or IE_STATE;
                    end if;
                
                when PAUSE_UNPAUSE_CONNECTION =>
                    -- Write settings to GigExpedite connection registers
                    CtrlRE <= '0';
                    CtrlWE <= '1';
                    CtrlBE <= "11";
                    CtrlAddr <= CONNECTION_STATE;
                    CtrlWriteData <= X"00" & '0' &
                                     User_TxPause(conv_integer(CtrlConnection)) &
                                     ConnectionStateVal;
                
                when CHECK_STATE =>
                    -- Read connection state and interrupt state
                    CtrlBE <= "11";
                    if (CtrlSubState(4 downto 0)="00000") then
                        CtrlRE <= '0';
                        CtrlWE <= '1';
                        CtrlAddr <= CONNECTION_PAGE;
                    elsif (CtrlSubState(4 downto 0)="10000") then
                        CtrlRE <= '1';
                        CtrlWE <= '0';
                        CtrlAddr <= CONNECTION_STATE;
                    elsif (CtrlSubState(4 downto 0)="10001") then
                        CtrlRE <= '1';
                        CtrlWE <= '0';
                        CtrlAddr <= INTERRUPT_ENABLE_STATUS;
                    else
                        CtrlRE <= '0';
                        CtrlWE <= '0';
                    end if;
                    CtrlWriteData <= X"000" & CtrlConnection;

                when STATE_CHANGE =>
                    -- Handle state change interrupt
                    if (CurrentState(3 downto 0)=ESTABLISHED and
                        ConnState(conv_integer(CtrlConnection))=CS_WAIT_ESTAB) then
                        if (CtrlSubState(3 downto 0)/=X"0" and
                            CtrlSubState(3 downto 0)<"0100") then
                            CtrlRE <= '1';
                        else
                            CtrlRE <= '0';
                        end if;
                        if (CtrlSubState(3 downto 0)="0111" or
                            CtrlSubState(3 downto 0)="1000") then
                            CtrlWE <= '1';
                        else
                            CtrlWE <= '0';
                        end if;
                        if (CtrlSubState(2 downto 0)="001") then
                            CtrlAddr <= REMOTE_PORT;
                        elsif (CtrlSubState(2 downto 0)="010") then
                            CtrlAddr <= REMOTE_IP_ADDR_HIGH;
                        elsif (CtrlSubState(2 downto 0)="011") then
                            CtrlAddr <= REMOTE_IP_ADDR_LOW;
                        elsif (CtrlSubState(2 downto 0)="111") then
                            CtrlAddr <= INTERRUPT_ENABLE_STATUS;
                        else
                            CtrlAddr <= CONNECTION_STATE;
                        end if;
                        if (CtrlSubState(2 downto 0)="111") then
                            CtrlWriteData <= (IE_STATE or IE_INCOMING or
                                              IE_OUTGOING_NOT_FULL or IE_OUTGOING_EMPTY);
                        else
                            CtrlWriteData <= X"00" & '0' &
                                             User_TxPause(conv_integer(CtrlConnection)) &
                                             CurrentState;
                        end if;
                    elsif (CurrentState(3 downto 0)=CLOSED(3 downto 0) and
                           CurrentState(5)='1' and
                           (ConnState(conv_integer(CtrlConnection))=CS_ESTAB or
                            ConnState(conv_integer(CtrlConnection))=CS_WAIT_CLOSE)) then
                        -- Received closed but not disabled
                        CtrlRE <= '0';
                        if (CtrlSubState(3 downto 0)/=X"0") then
                            CtrlWE <= '1';
                        else
                            CtrlWE <= '0';
                        end if;
                        if (CtrlSubState(0)='1') then
                            CtrlAddr <= INTERRUPT_ENABLE_STATUS;
                            CtrlWriteData <= IE_STATE;
                        else
                            CtrlAddr <= CONNECTION_STATE;
                            CtrlWriteData <= X"000" & CLOSED(3 downto 0);
                        end if;
                    elsif (CurrentState(5)='0') then
                        -- Received disabled
                        CtrlRE <= '0';
                        if (CtrlSubState(3 downto 0)/=X"0") then
                            CtrlWE <= '1';
                        else
                            CtrlWE <= '0';
                        end if;
                        if (CtrlSubState(0)='1') then
                            CtrlAddr <= INTERRUPT_ENABLE_STATUS;
                            CtrlWriteData <= X"0000";
                        else
                            CtrlAddr <= CONNECTION_STATE;
                            CtrlWriteData <= X"000" & CLOSED(3 downto 0);
                        end if;
                    else
                        -- Ignore other states
                        CtrlRE <= '0';
                        CtrlWE <= '0';
                    end if;
                    CtrlBE <= "11";
                
                when READ_LENGTH =>
                    -- Read from frame length register
                    if (RxFrameLengthNotZeroVal='0' and CtrlSubState(3 downto 0)=X"0") then
                        CtrlRE <= '1';
                    else
                        CtrlRE <= '0';
                    end if;
                    CtrlWE <= '0';
                    CtrlBE <= "11";
                    CtrlAddr <= FRAME_LENGTH;
                    CtrlWriteData <= X"0000";
                    CtrlOwner(7) <= not ConnectionStateVal(CONN_TCP_BIT);
                    CtrlOwner(6) <= '1';  -- Block data access to prevent conflict on 
                                          -- RxFrameLength and BurstLength RAMs
                    
                when WRITE_LENGTH => 
                    -- Write to frame length register
                    -- Also writes additional byte to data FIFO
                    CtrlRE <= '0';
                    if (CtrlSubState(2 downto 0)="100" or
                        CtrlSubState(2 downto 0)="101") then
                        CtrlWE <= '1';
                    else
                        CtrlWE <= '0';
                    end if;
                    if (CtrlSubState(2 downto 0)="100") then
                        CtrlBE <= "11";
                        CtrlAddr <= FRAME_LENGTH;
                        CtrlWriteData <= FrameLength;
                    else
                        CtrlBE <= "10";
                        CtrlAddr <= DATA_FIFO + CtrlConnection;
                        CtrlWriteData <= DataByte & X"00";
                    end if;
                    CtrlOwner(7) <= '0';
                    CtrlOwner(6) <= '0';
                    
                when others =>
                    -- Don't do anything
                    CtrlRE <= '0';
                    CtrlWE <= '0';
                    CtrlAddr <= (others=>'0');
                    CtrlBE <= "11";
                    CtrlWriteData <= (others=>'0');

            end case;
        end if;
    end process;
    
    -- Find the next connection to process
    -- Search takes place in the background to reduce cycles in 
    -- main control state machine
    process (User_CLK)
    begin
        if (User_CLK'event and User_CLK='1') then
            if (CtrlState=CHECK_STATE and CtrlSubState=X"0000") then
                NextCtrlConnection <= CtrlConnection;
                NextCtrlConnectionMask <= CtrlConnectionMask;
                NextCtrlConnectionValid <= '0';
            elsif (NextCtrlConnectionValid='0') then
                if (NextCtrlConnectionPlus1=X"0" or 
                    User_RegEnable(conv_integer(NextCtrlConnectionPlus1))='1' or
                    User_Connected(conv_integer(NextCtrlConnectionPlus1))='1') then
                    NextCtrlConnectionValid <= '1';
                else
                    NextCtrlConnectionValid <= '0';
                end if;
                NextCtrlConnection <= NextCtrlConnectionPlus1;
                NextCtrlConnectionMask <= NextCtrlConnectionMask(14 downto 0) &
                                          NextCtrlConnectionMask(15);
            end if;
        end if;
    end process;
    
    -- Keep shadow copy of GigEx connection state register
    process (User_CLK)
    begin
        if (User_CLK'event and User_CLK='1') then
            if (CtrlWE='1' and CtrlAddr=CONNECTION_STATE) then
                ConnectionState(conv_integer(CtrlConnection)) <= UserWriteData(5 downto 0);
            end if;
        end if;
    end process;

    --------------------------------------------------------------------------
    -- Feed data from 16 connection Tx FIFOs to single intermediate FIFO
    -- to improve timing
    process (User_RST, User_CLK)
    begin
        if (User_RST='1') then
            TxConnection <= X"0";
            TxConnectionMask <= X"0001";
            NextTxConnection <= X"0";
            NextTxConnectionMask <= X"0001";
            TxMergeFIFOWriteCount <= X"0";
            RegTxMergeFIFOWriteCount <= X"0";
            TxMergeFIFOReadCount <= X"0";
            TxMergeFIFODataOut <= (others=>'0');
            TxMergeFIFOFull <= '0';
            TxMergeFIFOEmpty <= '1';
        elsif (User_CLK'event and User_CLK='1') then

            -- Search for the next data connection that can be serviced
            TxFIFOEmptyTxVal <= TxFIFONextEmpty(conv_integer(TxConnection));
            if (TxFIFOEmptyNextTxVal='1' or
                TxFrameLengthNotZeroNextTxDataVal='0' or
                TxBurstLengthNotZeroNextTxDataVal='0') then
                NextTxConnection <= NextTxConnection + 1;
                NextTxConnectionMask <= NextTxConnectionMask(14 downto 0) & 
                                        NextTxConnectionMask(15);
            elsif (TxFIFOEmptyTxVal='1' or 
                   TxFrameLengthNotZeroTxDataVal='0' or
                   TxBurstLengthNotZeroTxDataVal='0') then
                TxConnection <= NextTxConnection;
                TxConnectionMask <= NextTxConnectionMask;
                TxFIFOEmptyTxVal <= TxFIFONextEmpty(conv_integer(NextTxConnection));
            end if;
            
            -- Manage FIFO write/read counts
            if (TxMergeFIFOWE='1') then
                TxMergeFIFOWriteCount <= TxMergeFIFOWriteCount+1;
            end if;
            RegTxMergeFIFOWriteCount <= TxMergeFIFOWriteCount;
            if (TxMergeFIFORE='1') then
                TxMergeFIFOReadCount <= TxMergeFIFOReadCount+1;
            end if;
            TxMergeFIFODataOut <= TxMergeFIFO(conv_integer(TxMergeFIFOReadAddr));
            
            -- Manage FIFO full/empty flags
            if (TxMergeFIFOCount>X"b") then 
                TxMergeFIFOFull <= '1';
            else
                TxMergeFIFOFull <= '0';
            end if;
            if (RegTxMergeFIFOWriteCount=TxMergeFIFOReadAddr) then
                TxMergeFIFOEmpty <= '1';
            else
                TxMergeFIFOEmpty <= '0';
            end if;
        end if;
    end process;
    
    -- Main FIFO storage
    process (User_CLK)
    begin
        if (User_CLK'event and User_CLK='1') then
            TxMergeFIFO(conv_integer(TxMergeFIFOWriteCount)) <= 
                    TxConnection & TxMergeFIFODataIn;
        end if;
    end process;
    TxMergeFIFODataIn(17) <= TxFIFODataOutTxDataVal(17);
    TxMergeFIFODataIn(16) <= '0' when ConnectionStateTxDataVal(CONN_TCP_BIT)='0' and
                                      TxFrameLengthTxDataVal=X"0001" else
                             TxFIFODataOutTxDataVal(16);
    TxMergeFIFODataIn(15 downto 0) <= TxFIFODataOutTxDataVal(15 downto 0);

    TxMergeFIFOReadAddr <= TxMergeFIFOReadCount when TxMergeFIFORE='0' else
                           TxMergeFIFOReadCount+1;
    TxMergeFIFOCount <= TxMergeFIFOWriteCount-TxMergeFIFOReadCount;

    TxMergeFIFOWE <= '1' when BlockTxMergeFIFO='0' and
                              TxFIFOEmptyTxVal='0' and
                              TxMergeFIFOFull='0' and
                              TxBurstLengthNotZeroTxDataVal='1' and
                              TxFrameLengthNotZeroTxDataVal='1' else '0';
    TxMergeFIFORE <= DataWEReq;
    TxMergeFIFOConn <= TxMergeFIFODataOut(21 downto 18);
    TxMergeFIFODataOutVal <= TxMergeFIFODataOut(17 downto 0);
    
    -- Feed data between GigExpedite and Rx/Tx FIFOs
    process (User_RST, User_CLK)
    begin
        if (User_RST='1') then
            DataConnection <= (others=>'0');
            NextDataConnection <= (others=>'0');
            RxFIFOFullDataVal <= '0';
            ArbCounter <= (others=>'0');
        elsif (User_CLK'event and User_CLK='1') then
            -- Move on when not able to continue transfer
            RxFIFOFullDataVal <= RxFIFOFull(conv_integer(DataConnection(3 downto 0)));
            ArbCounter <= ArbCounter + 1;
            if (NextTransferOK='0') then
                if (NextDataConnection(4)='0') then
                    NextDataConnection <= "10000";
                else
                    NextDataConnection <= NextDataConnection + 1;
                end if;
            elsif ((DataConnection(4)='0' and (WriteOK='0' or ArbCounter=X"ff")) or
                   (DataConnection(4)='1' and ReadOK='0')) then
                if (NextDataConnection(4)='0') then
                    NextDataConnection <= "10000";
                else
                    NextDataConnection <= NextDataConnection + 1;
                end if;
                DataConnection <= NextDataConnection;
                ArbCounter <= (others=>'0');
                RxFIFOFullDataVal <= RxFIFOFull(conv_integer(NextDataConnection(3 downto 0)));
            end if;
        end if;
    end process;

    process (CtrlState, NextDataConnection, User_Connected,
             ConnectionStateTxMergeDataVal,
             RxFIFOFullNextVal, RxFrameLengthNotZeroNextDataVal,
             RxBurstLengthNotZeroNextDataVal, DataConnection, 
             ConnectionStateDataVal, RxFrameLengthNotZeroVal,
             RxFIFOFullDataVal,
             RxFrameLengthNotZeroDataVal, RxBurstLengthNotZeroDataVal,
             CtrlWE, CtrlRE, UserReadDataValid, UserValidOwner,
             RxFrameLengthOddDataVal, DataBE, RxFrameLengthDataVal, 
             TxMergeFIFOEmpty, BlockDataConnection, TxMergeFIFOConn, 
             TxMergeFIFODataOutVal, BlockDataRE, BlockDataWE)
    begin
        -- Find the next connection that's ready to transfer
        if (NextDataConnection(4)='0') then
            if (TxMergeFIFOEmpty='0') then
                NextTransferOK <= '1';
            else
                NextTransferOK <= '0';
            end if;
        else
            if (RxFIFOFullNextVal='0' and
                RxFrameLengthNotZeroNextDataVal='1' and
                RxBurstLengthNotZeroNextDataVal='1') then
                NextTransferOK <= '1';
            else
                NextTransferOK <= '0';
            end if;
        end if;

        -- Check if a transfer is possible
        if (TxMergeFIFOEmpty='0') then
            WriteOK <= '1';
        else
            WriteOK <= '0';
        end if;
        if (RxFIFOFullDataVal='0' and
            RxFrameLengthNotZeroDataVal='1' and
            RxBurstLengthNotZeroDataVal='1') then
            ReadOK <= '1';
        else
            ReadOK <= '0';
        end if;
        
        -- Data read or write transfer
        if (DataConnection(4)='0') then
            -- Write data to GigExpedite FIFO
            if (CtrlWE='0' and CtrlRE='0' and BlockDataWE='0' and
                not (BlockDataConnection(4)='1' and BlockDataConnection(3 downto 0)=TxMergeFIFOConn) and
                WriteOK='1') then
                DataWEReq <= '1';
            else
                DataWEReq <= '0';
            end if;
            DataRE <= '0';
            DataBE <= TxMergeFIFODataOutVal(17 downto 16);
            DataAddr <= DATA_FIFO + TxMergeFIFOConn;
        else
            -- Read data from GigExpedite FIFO
            DataWEReq <= '0';
            if (CtrlWE='0' and CtrlRE='0' and BlockDataRE='0' and
                not (BlockDataConnection(4)='1' and BlockDataConnection(3 downto 0)=DataConnection(3 downto 0)) and
                ReadOK='1') then
                DataRE <= '1';
            else
                DataRE <= '0';
            end if;
            if (RxFrameLengthDataVal=X"0002") then
                DataBE <= '1' & (not RxFrameLengthOddDataVal);
            else
                DataBE <= "11";
            end if;
            DataAddr <= DATA_FIFO + DataConnection(3 downto 0);
        end if;
        
        -- Flush outgoing data when not connected
        if (ConnectionStateTxMergeDataVal(CONN_ENABLE_BIT)='1' and
            User_Connected(conv_integer(TxMergeFIFOConn))='1') then
            DataWE <= DataWEReq;
        else
            DataWE <= '0';
        end if;
        DataWriteData <= TxMergeFIFODataOutVal(15 downto 0);
        DataOwner <= "10" & DataBE & DataConnection(3 downto 0);
    end process;
    
    --------------------------------------------------------------------------
    -- Data transfer state RAMs
    -- Contains current frame lengths and positions in bursts for each
    -- connection
    process (CtrlConnection, UserReadData, TxBurstLengthTxDataVal, CtrlState,
             CtrlSubState, UserReadDataValid, UserValidOwner, ConnectionStateVal,
             DataRE, DataConnection, RxFrameLengthDataVal,
             RxFrameLengthOddDataVal, RxFrameLengthNotZeroVal, RxBurstLengthDataVal,
             CtrlWE, CtrlAddr, FrameLength, TxMergeFIFODataIn,
             TxFrameLengthTxDataVal, InterruptState, CurrentState, TxMergeFIFOWE,
             TxConnection, TxMergeFIFOConn, TxFIFODataOutTxDataVal,
             ConnectionStateTxDataVal)
    begin
        RxFrameLengthWE <= '0';
        RxFrameLengthAddr <= CtrlConnection;
        if (ConnectionStateVal(CONN_TCP_BIT)='1') then
            RxFrameLengthData(15 downto 1) <= UserReadData(15 downto 1)+UserReadData(0);
        else
            RxFrameLengthData(15 downto 1) <= UserReadData(15 downto 1)+UserReadData(0)+3;
        end if;
        RxFrameLengthData(0) <= '0';
        if (UserReadData=X"0000" and ConnectionStateVal(CONN_TCP_BIT)='1') then
            RxFrameLengthNotZeroData <= '0';
        else
            RxFrameLengthNotZeroData <= '1';
        end if;
        RxFrameLengthOddData <= UserReadData(0);
        RxBurstLengthWE <= '0';
        RxBurstLengthAddr <= CtrlConnection;
        RxBurstLengthData <= USER_BURST_LENGTH_WORDS;
        RxBurstLengthNotZeroData <= '1';
        TxFrameLengthWE <= '0';
        TxFrameLengthAddr <= CtrlConnection;
        if (TxFIFODataOutTxDataVal(17 downto 16)/="11") then
            TxFrameLengthData <= TxFrameLengthTxDataVal - 1;
        else
            TxFrameLengthData <= TxFrameLengthTxDataVal - 2;
        end if;
        if (TxFrameLengthTxDataVal=X"0001" or
            (TxFrameLengthTxDataVal=X"0002" and 
             TxFIFODataOutTxDataVal(17 downto 16)="11")) then
            TxFrameLengthNotZeroData <= '0';
        else
            TxFrameLengthNotZeroData <= '1';
        end if;
        TxBurstLengthWE <= '0';
        TxBurstLengthAddr <= CtrlConnection;
        TxBurstLengthData <= TxBurstLengthTxDataVal - 2;
        BlockDataRE <= '0';
        BlockDataWE <= '0';

        -- Initialise variables on connection open or close
        if (not (UserReadDataValid='1' and UserValidOwner(6)='1') and
            CtrlState=STATE_CHANGE and CtrlSubState(3 downto 0)=X"0") then
            BlockDataRE <= '1';
            BlockDataWE <= '1';
            RxFrameLengthWE <= '1';
            RxFrameLengthData <= (others=>'0');
            RxFrameLengthNotZeroData <= '0';
            RxBurstLengthWE <= '1';
            RxBurstLengthData <= (others=>'0');
            RxBurstLengthNotZeroData <= '0';
            TxFrameLengthWE <= '1';
            TxFrameLengthAddr <= CtrlConnection;
            TxFrameLengthData <= (others=>'0');
            TxFrameLengthNotZeroData <= CurrentState(CONN_TCP_BIT);
            TxBurstLengthWE <= '1';
            TxBurstLengthData <= (others=>'0');
        end if;
        
        -- Update receive frame length counter
        if (UserReadDataValid='1' and UserValidOwner(6)='1') then
            -- New frame length fom GigExpedite
            RxFrameLengthWE <= '1';
            BlockDataRE <= '1';
        end if;
        if (DataRE='1') then
            -- Decrement frame length as data is read
            RxFrameLengthWE <= '1';
            RxFrameLengthAddr <= DataConnection(3 downto 0);
            RxFrameLengthData <= RxFrameLengthDataVal-2;
            if (RxFrameLengthDataVal=X"0002") then
                RxFrameLengthNotZeroData <= '0';
            else
                RxFrameLengthNotZeroData <= '1';
            end if;
            RxFrameLengthOddData <= RxFrameLengthOddDataVal;
        end if;

        -- Update read burst counter
        if ((CtrlState=READ_LENGTH and RxFrameLengthNotZeroVal='1') or
            (UserReadDataValid='1' and UserValidOwner(6)='1')) then
            -- New burst available
            RxBurstLengthWE <= '1';
            BlockDataRE <= '1';
        end if;
        if (DataRE='1') then
            -- Decrement burst length as data is read
            RxBurstLengthWE <= '1';
            RxBurstLengthAddr <= DataConnection(3 downto 0);
            RxBurstLengthData <= RxBurstLengthDataVal - 1;
            if (RxBurstLengthDataVal=X"01") then
                RxBurstLengthNotZeroData <= '0';
            else
                RxBurstLengthNotZeroData <= '1';
            end if;
        end if;

        -- Update transmit frame length counter
        if (CtrlWE='1' and CtrlAddr=FRAME_LENGTH) then
            -- New UDP datagram length from user
            TxFrameLengthWE <= not ConnectionStateVal(CONN_TCP_BIT);
            TxFrameLengthData <= FrameLength;
            if (FrameLength/=X"0000") then
                TxFrameLengthNotZeroData <= '1';
            else
                TxFrameLengthNotZeroData <= '0';
            end if;
        elsif (TxMergeFIFOWE='1') then
            -- Decrement frame length as its written to GigExpedite
            TxFrameLengthWE <= not ConnectionStateTxDataVal(CONN_TCP_BIT);
            TxFrameLengthAddr <= TxConnection;
        end if;

        -- Update transmit burst length counter
        if (CtrlState=PROCESS_INTERRUPTS and
            InterruptState(IE_OUTGOING_NOT_FULL_BIT)='1') then
            -- New space available in the GigExpedite FIFO
            TxBurstLengthWE <= '1';
            TxBurstLengthData <= X"FF00";
        elsif (TxMergeFIFOWE='1') then
            -- Decrement space available as its filled
            TxBurstLengthWE <= '1';
            TxBurstLengthAddr <= TxConnection;
        end if;
    end process;
    
    -- Perform writes to state RAMs
    TxBurstLengthNotZeroData <= '1' when TxBurstLengthData/=X"0000" else '0';
    process (User_CLK)
    begin
        if (User_CLK'event and User_CLK='1') then
            if (RxFrameLengthWE='1') then
                RxFrameLength(conv_integer(RxFrameLengthAddr)) <= RxFrameLengthData;
                RxFrameLengthOdd(conv_integer(RxFrameLengthAddr)) <= RxFrameLengthOddData;
                RxFrameLengthNotZero(conv_integer(RxFrameLengthAddr)) <= RxFrameLengthNotZeroData;
            end if;
            if (RxBurstLengthWE='1') then
                RxBurstLength(conv_integer(RxBurstLengthAddr)) <= RxBurstLengthData;
                RxBurstLengthNotZero(conv_integer(RxBurstLengthAddr)) <= RxBurstLengthNotZeroData;
            end if;
            if (TxFrameLengthWE='1') then
                TxFrameLength(conv_integer(TxFrameLengthAddr)) <= TxFrameLengthData;
                TxFrameLengthNotZero(conv_integer(TxFrameLengthAddr)) <=
                                            TxFrameLengthNotZeroData;
            end if;
            if (TxBurstLengthWE='1') then
                TxBurstLength(conv_integer(TxBurstLengthAddr)) <= TxBurstLengthData;
                TxBurstLengthNotZero(conv_integer(TxBurstLengthAddr)) <=
                                            TxBurstLengthNotZeroData;
            end if;
            if (TxMergeFIFOWE='1') then
                TxLastByte(conv_integer(TxConnection)) <=
                          TxFIFODataOutTxDataVal(7 downto 0);
                TxLastByteValid(conv_integer(TxConnection)) <=
                          TxFIFODataOutTxDataVal(16) and not TxMergeFIFODataIn(16);
            end if;
        end if;
    end process;

    -- Update buffer empty flags
    process (User_RST, User_CLK)
    begin
        if (User_RST='1') then
            User_TxBufferEmpty <= X"FFFF";
            TxBufferEmpty <= X"FFFF";
        elsif (User_CLK'event and User_CLK='1') then
            if (CtrlState=PROCESS_INTERRUPTS and
                InterruptState(IE_OUTGOING_NOT_FULL_BIT)='0' and
                InterruptState(IE_INCOMING_BIT)='0' and
                InterruptState(IE_OUTGOING_EMPTY_BIT)='1') then
                TxBufferEmpty(conv_integer(CtrlConnection)) <= '1';
            end if;
            if (DataWE='1') then
                TxBufferEmpty(conv_integer(DataConnection(3 downto 0))) <= '0';
            end if;
            User_TxBufferEmpty <= TxBufferEmpty and TxFIFOEmpty;
        end if;
    end process;

    --------------------------------------------------------------------------
    -- Mask interrupts during latency periods
    process (User_RST, User_CLK)
    begin
        if (User_RST='1') then
            for i in 0 to 15 loop
                InterruptLatency(i) <= (others=>'0');
                InterruptLatencyNotZero(i) <= '0';
            end loop;
            LastRxFrameLengthWE <= '0';
            LastRxFrameLengthData <= (others=>'0');
            LastRxFrameLengthAddr <= (others=>'0');
        elsif (User_CLK'event and User_CLK='1') then
            for i in 0 to 15 loop
                if (InterruptLatency(i)/="00000") then
                    InterruptLatency(i) <= InterruptLatency(i) - 1;
                    InterruptLatencyNotZero(i) <= '1';
                else
                    InterruptLatencyNotZero(i) <= '0';
                end if;
            end loop;
            LastRxFrameLengthWE <= RxFrameLengthWE;
            LastRxFrameLengthData <= RxFrameLengthData;
            LastRxFrameLengthAddr <= RxFrameLengthAddr;
            if ((LastRxFrameLengthWE='1' and LastRxFrameLengthData=X"0000") or
                (CtrlWE='1' and CtrlAddr=CONNECTION_STATE)) then
                InterruptLatency(conv_integer(LastRxFrameLengthAddr)) <= "11110";
                InterruptLatencyNotZero(conv_integer(LastRxFrameLengthAddr)) <= '1';
            end if;
        end if;
    end process;

    --------------------------------------------------------------------------
    -- Instantiate FIFOs to/from user code
    RxFIFOInstGen:
        for g in 0 to 15 generate
        begin
            RxFIFOInst : entity work.ZestET1_RxFIFO
            port map (
                CLK => User_CLK,
                RST => FIFORST(g),
                DataIn => RxFIFODataIn,
                WE => RxFIFOWE(g),
                DataOut => User_RxData(g),
                RE => User_RxEnable(g),
                Empty => RxFIFOEmpty(g),
                Full => RxFIFOFull(g)
            );
        end generate;
    FIFORST <= X"FFFF" when User_RST='1' else not User_Connected;
    RxFIFODataIn <= UserValidOwner(5 downto 4) & UserReadData;
    User_RxEnable <= not RxFIFOEmpty and not User_RxBusy;
    process (UserValidOwner, UserReadDataValid) 
    begin
        RxFIFOWE <= X"0000";
        RxFIFOWE(conv_integer(UserValidOwner(3 downto 0))) <= UserReadDataValid and
                                                              UserValidOwner(7);
    end process;

    TxFIFOInstGen:
        for g in 0 to 15 generate
        begin
            TxFIFOInst : entity work.ZestET1_TxFIFO
            port map (
                CLK => User_CLK,
                RST => FIFORST(g),
                DataIn => User_TxData(g),
                WE => User_TxEnable(g),
                DataOut => TxFIFODataOut(g),
                RE => TxFIFORE(g),
                Empty => TxFIFOEmpty(g),
                NextEmpty => TxFIFONextEmpty(g),
                Full => User_TxBusy(g)
            );
        end generate;
    TxFIFOREData <= TxConnectionMask when TxMergeFIFOWE='1' else X"0000";
    TxFIFORECtrl <= CtrlConnectionMask when CtrlState=WRITE_LENGTH and
                                            TxMergeFIFOEmpty='1' and
                                            CtrlSubState(2 downto 1)="01" and
                                            TxFIFOEmptyVal='0' else X"0000";
    TxFIFORE <= TxFIFOREData or TxFIFORECtrl;
    
    --------------------------------------------------------------------------
    -- Ethernet interface physical layer
    EthernetInst : entity work.ZestET1_Ethernet
    generic map (
        CLOCK_RATE => CLOCK_RATE
    )
    port map (
        User_CLK => User_CLK,
        User_RST => User_RST,

        -- User interface
        User_WE => UserWE,
        User_RE => UserRE,
        User_Addr => UserAddr,
        User_Owner => UserOwner,
        User_WriteData => UserWriteData,
        User_BE => UserBE,
        User_ReadData => UserReadData,
        User_ReadDataValid => UserReadDataValid,
        User_ValidOwner => UserValidOwner,
        User_Interrupt => UserInterrupt,
        
        -- Interface to GigExpedite
        Eth_Clk => Eth_Clk,
        Eth_CSn => Eth_CSn,
        Eth_WEn => Eth_WEn,
        Eth_A => Eth_A,
        Eth_D => Eth_D,
        Eth_BE => Eth_BE,
        Eth_Intn => Eth_Intn
    );

end arch;


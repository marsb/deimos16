-- ZestET1 DDR SDRAM Physical layer
--   File name: ZestET1_SDRAMPhy.vhd
--   Version: 1.20
--   Date: 19/5/2011
--
--   DDR SDRAM controller for user FPGA on ZestET1 boards
--   Physical layer.  Directly instantiates components on time critical sections
--   of the controller logic.
--    
--   Copyright (C) 2011 Orange Tree Technologies Ltd. All rights reserved.
--   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and
--   modify this logic core in any form including but not limited to VHDL source code or
--   EDIF netlist in FPGA designs that target the ZestET1.
--   Orange Tree Technologies prohibits the use of this logic core or any modification of
--   it in any form including but not limited to VHDL source code or EDIF netlist in
--   FPGA or ASIC designs that target any other hardware unless the purchaser of the
--   ZestET1 has purchased the appropriate licence from Orange Tree Technologies.
--   Contact Orange Tree Technologies if you want to purchase such a licence.
--
--  *****************************************************************************************
--  **
--  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
--  **              provided to you "as is". Orange Tree Technologies and its licensors 
--  **              make and you receive no warranties or conditions, express, implied, 
--  **              statutory or otherwise, and Orange Tree Technologies specifically 
--  **              disclaims any implied warranties of merchantability, non-infringement,
--  **              or fitness for a particular purpose. Orange Tree Technologies does not
--  **              warrant that the functions contained in these designs will meet your 
--  **              requirements, or that the operation of these designs will be 
--  **              uninterrupted or error free, or that defects in the Designs will be 
--  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
--  **              make any representations regarding use or the results of the use of the 
--  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
--  **
--  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
--  **              or its licensors be liable for any loss of data, lost profits, cost or 
--  **              procurement of substitute goods or services, or for any special, 
--  **              incidental, consequential, or indirect damages arising from the use or 
--  **              operation of the designs or accompanying documentation, however caused 
--  **              and on any theory of liability. This limitation will apply even if 
--  **              Orange Tree Technologies has been advised of the possibility of such 
--  **              damage. This limitation shall apply notwithstanding the failure of the 
--  **              essential purpose of any limited remedies herein.
--  **
--  *****************************************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity ZestET1_SDRAMPhy is
    port (
        CLK : in std_logic;
        CLK90 : in std_logic;
        RST : in std_logic;
        
        -- Data path
        DataOut : in std_logic_vector(31 downto 0);
        DataTri_P : in std_logic;
        DataTri_N : in std_logic;
        DQSTri_P : in std_logic;
        DQSTri_N : in std_logic;
        DM_P : in std_logic_vector(1 downto 0);
        DM_N : in std_logic_vector(1 downto 0);
        DS_CLK_P : out std_logic;
        DS_CLK_N : out std_logic;
        DataIn : out std_logic_vector(31 downto 0);
        DS_DM : out std_logic_vector(1 downto 0);
        DataValid : out std_logic;
        DS_DQ : inout std_logic_vector(15 downto 0);
        DS_DQS : inout std_logic_vector(1 downto 0);

        -- Control path
        DS_A : out std_logic_vector(12 downto 0);
        DS_BA : out std_logic_vector(1 downto 0);
        DS_CAS_N : out std_logic;
        DS_RAS_N : out std_logic;
        DS_WE_N : out std_logic;
        Addr : in std_logic_vector(12 downto 0);
        BankAddr : in std_logic_vector(1 downto 0);
        CAS : in std_logic;
        RAS : in std_logic;
        WE : in std_logic
    );
end ZestET1_SDRAMPhy;

architecture arch of ZestET1_SDRAMPhy is

    --------------------------------------------------------------------------
    -- Declare signals
    signal nCLK : std_logic;
    signal nCLK90 : std_logic;
    signal DQSBuf1 : std_logic;
    signal DQSBuf0 : std_logic;
    attribute syn_keep: boolean;
    attribute buffer_type : string;
    attribute buffer_type of DQSBuf0 : signal is "none";
    attribute buffer_type of DQSBuf1 : signal is "none";
    attribute buffer_type of DS_DQS : signal is "none";
    attribute syn_keep of DQSBuf0: signal is true;
    attribute syn_keep of DQSBuf1: signal is true;

    signal DDRIn15 : std_logic;
    signal DDRIn14 : std_logic;
    signal DDRIn13 : std_logic;
    signal DDRIn12 : std_logic;
    signal DDRIn11 : std_logic;
    signal DDRIn10 : std_logic;
    signal DDRIn9 : std_logic;
    signal DDRIn8 : std_logic;
    signal DDRIn7 : std_logic;
    signal DDRIn6 : std_logic;
    signal DDRIn5 : std_logic;
    signal DDRIn4 : std_logic;
    signal DDRIn3 : std_logic;
    signal DDRIn2 : std_logic;
    signal DDRIn1 : std_logic;
    signal DDRIn0 : std_logic;
    attribute syn_keep of DDRIn15: signal is true;
    attribute syn_keep of DDRIn14: signal is true;
    attribute syn_keep of DDRIn13: signal is true;
    attribute syn_keep of DDRIn12: signal is true;
    attribute syn_keep of DDRIn11: signal is true;
    attribute syn_keep of DDRIn10: signal is true;
    attribute syn_keep of DDRIn9: signal is true;
    attribute syn_keep of DDRIn8: signal is true;
    attribute syn_keep of DDRIn7: signal is true;
    attribute syn_keep of DDRIn6: signal is true;
    attribute syn_keep of DDRIn5: signal is true;
    attribute syn_keep of DDRIn4: signal is true;
    attribute syn_keep of DDRIn3: signal is true;
    attribute syn_keep of DDRIn2: signal is true;
    attribute syn_keep of DDRIn1: signal is true;
    attribute syn_keep of DDRIn0: signal is true;
    
    signal DQSOut : std_logic_vector(1 downto 0);
    signal DQSTri : std_logic_vector(1 downto 0);
    signal DDRMask : std_logic_vector(1 downto 0);
    signal DDROut : std_logic_vector(15 downto 0);
    signal DDRTri : std_logic_vector(15 downto 0);
    signal nTristate_p : std_logic_vector(15 downto 0);
    signal nTristate_n : std_logic_vector(15 downto 0);

    signal DQSDelayed0 : std_logic;
    attribute syn_keep of DQSDelayed0: signal is true;
    signal DQSDelayed1 : std_logic;
    attribute syn_keep of DQSDelayed1: signal is true;

    signal DataInWriteCount : std_logic_vector(1 downto 0);
    signal DataInWriteCountInReadDomain : std_logic_vector(1 downto 0);
    signal DataInReadCount_0 : std_logic;
    signal DataInReadCount_1 : std_logic;
    signal DataInRC : std_logic_vector(3 downto 0);

    type DATA_IN_BUFFER_BIT is array(0 to 15) of std_logic;
    signal DataInBuf0 : DATA_IN_BUFFER_BIT;
    signal DataInBuf1 : DATA_IN_BUFFER_BIT;
    signal DataInBuf2 : DATA_IN_BUFFER_BIT;
    signal DataInBuf3 : DATA_IN_BUFFER_BIT;
    signal DataInBuf4 : DATA_IN_BUFFER_BIT;
    signal DataInBuf5 : DATA_IN_BUFFER_BIT;
    signal DataInBuf6 : DATA_IN_BUFFER_BIT;
    signal DataInBuf7 : DATA_IN_BUFFER_BIT;
    signal DataInBuf8 : DATA_IN_BUFFER_BIT;
    signal DataInBuf9 : DATA_IN_BUFFER_BIT;
    signal DataInBuf10 : DATA_IN_BUFFER_BIT;
    signal DataInBuf11 : DATA_IN_BUFFER_BIT;
    signal DataInBuf12 : DATA_IN_BUFFER_BIT;
    signal DataInBuf13 : DATA_IN_BUFFER_BIT;
    signal DataInBuf14 : DATA_IN_BUFFER_BIT;
    signal DataInBuf15 : DATA_IN_BUFFER_BIT;
    signal DataInBuf16 : DATA_IN_BUFFER_BIT;
    signal DataInBuf17 : DATA_IN_BUFFER_BIT;
    signal DataInBuf18 : DATA_IN_BUFFER_BIT;
    signal DataInBuf19 : DATA_IN_BUFFER_BIT;
    signal DataInBuf20 : DATA_IN_BUFFER_BIT;
    signal DataInBuf21 : DATA_IN_BUFFER_BIT;
    signal DataInBuf22 : DATA_IN_BUFFER_BIT;
    signal DataInBuf23 : DATA_IN_BUFFER_BIT;
    signal DataInBuf24 : DATA_IN_BUFFER_BIT;
    signal DataInBuf25 : DATA_IN_BUFFER_BIT;
    signal DataInBuf26 : DATA_IN_BUFFER_BIT;
    signal DataInBuf27 : DATA_IN_BUFFER_BIT;
    signal DataInBuf28 : DATA_IN_BUFFER_BIT;
    signal DataInBuf29 : DATA_IN_BUFFER_BIT;
    signal DataInBuf30 : DATA_IN_BUFFER_BIT;
    signal DataInBuf31 : DATA_IN_BUFFER_BIT;
    signal DataInBuf : std_logic_vector(31 downto 0);
    attribute syn_keep of DataInBuf: signal is true;

    signal DataInWriteCount0_0 : std_logic;
    signal DataInWriteCount0_1 : std_logic;
    attribute syn_keep of DataInWriteCount0_0: signal is true;
    attribute syn_keep of DataInWriteCount0_1: signal is true;
    signal DataInWC0 : std_logic_vector(3 downto 0);
    signal DataInWriteCount1_0 : std_logic;
    signal DataInWriteCount1_1 : std_logic;
    attribute syn_keep of DataInWriteCount1_0: signal is true;
    attribute syn_keep of DataInWriteCount1_1: signal is true;
    signal DataInWC1 : std_logic_vector(3 downto 0);
    signal DataInWriteCount2_0 : std_logic;
    signal DataInWriteCount2_1 : std_logic;
    attribute syn_keep of DataInWriteCount2_0: signal is true;
    attribute syn_keep of DataInWriteCount2_1: signal is true;
    signal DataInWC2 : std_logic_vector(3 downto 0);
    signal DataInWriteCount3_0 : std_logic;
    signal DataInWriteCount3_1 : std_logic;
    attribute syn_keep of DataInWriteCount3_0: signal is true;
    attribute syn_keep of DataInWriteCount3_1: signal is true;
    signal DataInWC3 : std_logic_vector(3 downto 0);
    signal DataInWriteCount4_0 : std_logic;
    signal DataInWriteCount4_1 : std_logic;
    attribute syn_keep of DataInWriteCount4_0: signal is true;
    attribute syn_keep of DataInWriteCount4_1: signal is true;
    signal DataInWC4 : std_logic_vector(3 downto 0);
    signal DataInWriteCount5_0 : std_logic;
    signal DataInWriteCount5_1 : std_logic;
    attribute syn_keep of DataInWriteCount5_0: signal is true;
    attribute syn_keep of DataInWriteCount5_1: signal is true;
    signal DataInWC5 : std_logic_vector(3 downto 0);
    signal DataInWriteCount6_0 : std_logic;
    signal DataInWriteCount6_1 : std_logic;
    attribute syn_keep of DataInWriteCount6_0: signal is true;
    attribute syn_keep of DataInWriteCount6_1: signal is true;
    signal DataInWC6 : std_logic_vector(3 downto 0);
    signal DataInWriteCount7_0 : std_logic;
    signal DataInWriteCount7_1 : std_logic;
    attribute syn_keep of DataInWriteCount7_0: signal is true;
    attribute syn_keep of DataInWriteCount7_1: signal is true;
    signal DataInWC7 : std_logic_vector(3 downto 0);
    signal DataInWriteCount8_0 : std_logic;
    signal DataInWriteCount8_1 : std_logic;
    attribute syn_keep of DataInWriteCount8_0: signal is true;
    attribute syn_keep of DataInWriteCount8_1: signal is true;
    signal DataInWC8 : std_logic_vector(3 downto 0);
    signal DataInWriteCount9_0 : std_logic;
    signal DataInWriteCount9_1 : std_logic;
    attribute syn_keep of DataInWriteCount9_0: signal is true;
    attribute syn_keep of DataInWriteCount9_1: signal is true;
    signal DataInWC9 : std_logic_vector(3 downto 0);
    signal DataInWriteCount10_0 : std_logic;
    signal DataInWriteCount10_1 : std_logic;
    attribute syn_keep of DataInWriteCount10_0: signal is true;
    attribute syn_keep of DataInWriteCount10_1: signal is true;
    signal DataInWC10 : std_logic_vector(3 downto 0);
    signal DataInWriteCount11_0 : std_logic;
    signal DataInWriteCount11_1 : std_logic;
    attribute syn_keep of DataInWriteCount11_0: signal is true;
    attribute syn_keep of DataInWriteCount11_1: signal is true;
    signal DataInWC11 : std_logic_vector(3 downto 0);
    signal DataInWriteCount12_0 : std_logic;
    signal DataInWriteCount12_1 : std_logic;
    attribute syn_keep of DataInWriteCount12_0: signal is true;
    attribute syn_keep of DataInWriteCount12_1: signal is true;
    signal DataInWC12 : std_logic_vector(3 downto 0);
    signal DataInWriteCount13_0 : std_logic;
    signal DataInWriteCount13_1 : std_logic;
    attribute syn_keep of DataInWriteCount13_0: signal is true;
    attribute syn_keep of DataInWriteCount13_1: signal is true;
    signal DataInWC13 : std_logic_vector(3 downto 0);
    signal DataInWriteCount14_0 : std_logic;
    signal DataInWriteCount14_1 : std_logic;
    attribute syn_keep of DataInWriteCount14_0: signal is true;
    attribute syn_keep of DataInWriteCount14_1: signal is true;
    signal DataInWC14 : std_logic_vector(3 downto 0);
    signal DataInWriteCount15_0 : std_logic;
    signal DataInWriteCount15_1 : std_logic;
    attribute syn_keep of DataInWriteCount15_0: signal is true;
    attribute syn_keep of DataInWriteCount15_1: signal is true;
    signal DataInWC15 : std_logic_vector(3 downto 0);

    signal DataInVal : std_logic_vector(31 downto 0);
    signal DataValidVal : std_logic;

    attribute IOB : string;
    attribute IOB of AddrInst0 : label is "TRUE";
    attribute IOB of AddrInst1 : label is "TRUE";
    attribute IOB of AddrInst2 : label is "TRUE";
    attribute IOB of AddrInst3 : label is "TRUE";
    attribute IOB of AddrInst4 : label is "TRUE";
    attribute IOB of AddrInst5 : label is "TRUE";
    attribute IOB of AddrInst6 : label is "TRUE";
    attribute IOB of AddrInst7 : label is "TRUE";
    attribute IOB of AddrInst8 : label is "TRUE";
    attribute IOB of AddrInst9 : label is "TRUE";
    attribute IOB of AddrInst10 : label is "TRUE";
    attribute IOB of AddrInst11 : label is "TRUE";
    attribute IOB of AddrInst12 : label is "TRUE";
    attribute IOB of BankAddrInst0 : label is "TRUE";
    attribute IOB of BankAddrInst1 : label is "TRUE";
    attribute IOB of CASInst : label is "TRUE";
    attribute IOB of RASInst : label is "TRUE";
    attribute IOB of WEInst : label is "TRUE";

begin
    
    -- Instantiate DDR FFs for clock output to RAM
    nCLK <= not CLK;
    nCLK90 <= not CLK90;
    CLK_PInst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DS_CLK_P, C0 => nCLK, C1 => CLK, CE => '1',
                  D0 => '1', D1 => '0', R => '0', S => '0');
    CLK_NInst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DS_CLK_N, C0 => nCLK, C1 => CLK, CE => '1',
                  D0 => '0', D1 => '1', R => '0', S => '0');

    -- Instantiate FFs for DQS signals
    DQSOut1Inst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DQSOut(1), C0 => nCLK, C1 => CLK, CE => '1',
                  D0 => '1', D1 => '0', R => '0', S => '0');

    DQSOut0Inst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DQSOut(0), C0 => nCLK, C1 => CLK, CE => '1',
                  D0 => '1', D1 => '0', R => '0', S => '0');

    DQSTri1Inst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DQSTri(1), C0 => nCLK, C1 => CLK, CE => '1',
                  D0 => DQSTri_P, D1 => DQSTri_N, R => '0', S => '0');

    DQSTri0Inst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DQSTri(0), C0 => nCLK, C1 => CLK, CE => '1',
                  D0 => DQSTri_P, D1 => DQSTri_N, R => '0', S => '0');

    -- Instantiate FFs for DM signals
    DM1Inst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DDRMask(1), C0 => CLK90, C1 => nCLK90, CE => '1',
                  D0 => DM_P(1), D1 => DM_N(1), R => '0', S => '0');
                 
    DM0Inst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DDRMask(0), C0 => CLK90, C1 => nCLK90, CE => '1',
                  D0 => DM_P(0), D1 => DM_N(0), R => '0', S => '0');
                 
    -- Instantiate FFs for DQ signals
    DQ15Inst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DDROut(15), C0 => CLK90, C1 => nCLK90, CE => '1',
                  D0 => DataOut(31), D1 => DataOut(15), R => '0', S => '0');

    DQ14Inst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DDROut(14), C0 => CLK90, C1 => nCLK90, CE => '1',
                  D0 => DataOut(30), D1 => DataOut(14), R => '0', S => '0');

    DQ13Inst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DDROut(13), C0 => CLK90, C1 => nCLK90, CE => '1',
                  D0 => DataOut(29), D1 => DataOut(13), R => '0', S => '0');

    DQ12Inst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DDROut(12), C0 => CLK90, C1 => nCLK90, CE => '1',
                  D0 => DataOut(28), D1 => DataOut(12), R => '0', S => '0');

    DQ11Inst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DDROut(11), C0 => CLK90, C1 => nCLK90, CE => '1',
                  D0 => DataOut(27), D1 => DataOut(11), R => '0', S => '0');

    DQ10Inst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DDROut(10), C0 => CLK90, C1 => nCLK90, CE => '1',
                  D0 => DataOut(26), D1 => DataOut(10), R => '0', S => '0');

    DQ9Inst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DDROut(9), C0 => CLK90, C1 => nCLK90, CE => '1',
                  D0 => DataOut(25), D1 => DataOut(9), R => '0', S => '0');

    DQ8Inst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DDROut(8), C0 => CLK90, C1 => nCLK90, CE => '1',
                  D0 => DataOut(24), D1 => DataOut(8), R => '0', S => '0');

    DQ7Inst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DDROut(7), C0 => CLK90, C1 => nCLK90, CE => '1',
                  D0 => DataOut(23), D1 => DataOut(7), R => '0', S => '0');

    DQ6Inst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DDROut(6), C0 => CLK90, C1 => nCLK90, CE => '1',
                  D0 => DataOut(22), D1 => DataOut(6), R => '0', S => '0');

    DQ5Inst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DDROut(5), C0 => CLK90, C1 => nCLK90, CE => '1',
                  D0 => DataOut(21), D1 => DataOut(5), R => '0', S => '0');

    DQ4Inst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DDROut(4), C0 => CLK90, C1 => nCLK90, CE => '1',
                  D0 => DataOut(20), D1 => DataOut(4), R => '0', S => '0');

    DQ3Inst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DDROut(3), C0 => CLK90, C1 => nCLK90, CE => '1',
                  D0 => DataOut(19), D1 => DataOut(3), R => '0', S => '0');

    DQ2Inst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DDROut(2), C0 => CLK90, C1 => nCLK90, CE => '1',
                  D0 => DataOut(18), D1 => DataOut(2), R => '0', S => '0');

    DQ1Inst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DDROut(1), C0 => CLK90, C1 => nCLK90, CE => '1',
                  D0 => DataOut(17), D1 => DataOut(1), R => '0', S => '0');

    DQ0Inst : ODDR2
        generic map (DDR_ALIGNMENT => "NONE", INIT => '0', SRTYPE => "SYNC")
        port map (Q => DDROut(0), C0 => CLK90, C1 => nCLK90, CE => '1',
                  D0 => DataOut(16), D1 => DataOut(0), R => '0', S => '0');

    -- Copy tristate signal and make sure its preserved
    TristateCopyp :
        for i in 0 to 15 generate
        begin
            FDCE_inst : FDCE
                generic map (
                    INIT => '0'
                )
                port map (
                    Q => nTristate_p(i),
                    C => nCLK,
                    CE => '1',
                    CLR => '0',
                    D => DataTri_P
                );            
        end generate;

    TristateCopyn :
        for i in 0 to 15 generate
        begin
            FDCE_inst : FDCE
                generic map (
                    INIT => '0'
                )
                port map (
                    Q => nTristate_n(i),
                    C => CLK,
                    CE => '1',
                    CLR => '0',
                    D => DataTri_N
                );            
        end generate;

    -- Instantiate FFs for tristate signals
    TristateDDR :
        for i in 0 to 15 generate
        begin
            TristateDDRInst : ODDR2
                generic map (
                    DDR_ALIGNMENT => "NONE",
                    INIT => '0',
                    SRTYPE => "SYNC"
                )
                port map (
                    Q => DDRTri(i), C0 => CLK90, C1 => nCLK90, CE => '1',
                    D0 => nTristate_p(i), D1 => nTristate_n(i), R => '0', S => '0'
                );
        end generate;
    
    -- Clock incoming data using DQS strobe
    DataInWC0 <= "00"&DataInWriteCount0_1&DataInWriteCount0_0;
    DataInWC1 <= "00"&DataInWriteCount1_1&DataInWriteCount1_0;
    DataInWC2 <= "00"&DataInWriteCount2_1&DataInWriteCount2_0;
    DataInWC3 <= "00"&DataInWriteCount3_1&DataInWriteCount3_0;
    DataInWC4 <= "00"&DataInWriteCount4_1&DataInWriteCount4_0;
    DataInWC5 <= "00"&DataInWriteCount5_1&DataInWriteCount5_0;
    DataInWC6 <= "00"&DataInWriteCount6_1&DataInWriteCount6_0;
    DataInWC7 <= "00"&DataInWriteCount7_1&DataInWriteCount7_0;
    DataInWC8 <= "00"&DataInWriteCount8_1&DataInWriteCount8_0;
    DataInWC9 <= "00"&DataInWriteCount9_1&DataInWriteCount9_0;
    DataInWC10 <= "00"&DataInWriteCount10_1&DataInWriteCount10_0;
    DataInWC11 <= "00"&DataInWriteCount11_1&DataInWriteCount11_0;
    DataInWC12 <= "00"&DataInWriteCount12_1&DataInWriteCount12_0;
    DataInWC13 <= "00"&DataInWriteCount13_1&DataInWriteCount13_0;
    DataInWC14 <= "00"&DataInWriteCount14_1&DataInWriteCount14_0;
    DataInWC15 <= "00"&DataInWriteCount15_1&DataInWriteCount15_0;
    process (DQSBuf0) begin
        if (DQSBuf0'event and DQSBuf0='0') then
            DataInBuf0(conv_integer(DataInWC0)) <= DDRIn0;
            DataInBuf1(conv_integer(DataInWC1)) <= DDRIn1;
            DataInBuf2(conv_integer(DataInWC2)) <= DDRIn2;
            DataInBuf3(conv_integer(DataInWC3)) <= DDRIn3;
            DataInBuf4(conv_integer(DataInWC4)) <= DDRIn4;
            DataInBuf5(conv_integer(DataInWC5)) <= DDRIn5;
            DataInBuf6(conv_integer(DataInWC6)) <= DDRIn6;
            DataInBuf7(conv_integer(DataInWC7)) <= DDRIn7;
        end if;
    end process;
    process (DQSBuf0) begin
        if (DQSBuf0'event and DQSBuf0='1') then
            DataInBuf16(conv_integer(DataInWC0)) <= DDRIn0;
            DataInBuf17(conv_integer(DataInWC1)) <= DDRIn1;
            DataInBuf18(conv_integer(DataInWC2)) <= DDRIn2;
            DataInBuf19(conv_integer(DataInWC3)) <= DDRIn3;
            DataInBuf20(conv_integer(DataInWC4)) <= DDRIn4;
            DataInBuf21(conv_integer(DataInWC5)) <= DDRIn5;
            DataInBuf22(conv_integer(DataInWC6)) <= DDRIn6;
            DataInBuf23(conv_integer(DataInWC7)) <= DDRIn7;
        end if;
    end process;
    process (DQSBuf1) begin
        if (DQSBuf1'event and DQSBuf1='0') then
            DataInBuf8(conv_integer(DataInWC8)) <= DDRIn8;
            DataInBuf9(conv_integer(DataInWC9)) <= DDRIn9;
            DataInBuf10(conv_integer(DataInWC10)) <= DDRIn10;
            DataInBuf11(conv_integer(DataInWC11)) <= DDRIn11;
            DataInBuf12(conv_integer(DataInWC12)) <= DDRIn12;
            DataInBuf13(conv_integer(DataInWC13)) <= DDRIn13;
            DataInBuf14(conv_integer(DataInWC14)) <= DDRIn14;
            DataInBuf15(conv_integer(DataInWC15)) <= DDRIn15;
        end if;
    end process;
    process (DQSBuf1) begin
        if (DQSBuf1'event and DQSBuf1='1') then
            DataInBuf24(conv_integer(DataInWC8)) <= DDRIn8;
            DataInBuf25(conv_integer(DataInWC9)) <= DDRIn9;
            DataInBuf26(conv_integer(DataInWC10)) <= DDRIn10;
            DataInBuf27(conv_integer(DataInWC11)) <= DDRIn11;
            DataInBuf28(conv_integer(DataInWC12)) <= DDRIn12;
            DataInBuf29(conv_integer(DataInWC13)) <= DDRIn13;
            DataInBuf30(conv_integer(DataInWC14)) <= DDRIn14;
            DataInBuf31(conv_integer(DataInWC15)) <= DDRIn15;
        end if;
    end process;

    -- Manage write counter
    process (RST, DQSBuf0) begin
        if (RST='1') then
            DataInWriteCount0_0 <= '0';
            DataInWriteCount0_1 <= '0';
            DataInWriteCount1_0 <= '0';
            DataInWriteCount1_1 <= '0';
            DataInWriteCount2_0 <= '0';
            DataInWriteCount2_1 <= '0';
            DataInWriteCount3_0 <= '0';
            DataInWriteCount3_1 <= '0';
            DataInWriteCount4_0 <= '0';
            DataInWriteCount4_1 <= '0';
            DataInWriteCount5_0 <= '0';
            DataInWriteCount5_1 <= '0';
            DataInWriteCount6_0 <= '0';
            DataInWriteCount6_1 <= '0';
            DataInWriteCount7_0 <= '0';
            DataInWriteCount7_1 <= '0';
            DataInWriteCount <= "00";
        elsif (DQSBuf0'event and DQSBuf0='0') then
            case DataInWC0(1 downto 0) is
                when "00" => DataInWriteCount0_1 <= '0'; DataInWriteCount0_0 <= '1';
                when "01" => DataInWriteCount0_1 <= '1'; DataInWriteCount0_0 <= '1';
                when "11" => DataInWriteCount0_1 <= '1'; DataInWriteCount0_0 <= '0';
                when others => DataInWriteCount0_1 <= '0'; DataInWriteCount0_0 <= '0';
            end case;
            case DataInWC1(1 downto 0) is
                when "00" => DataInWriteCount1_1 <= '0'; DataInWriteCount1_0 <= '1';
                when "01" => DataInWriteCount1_1 <= '1'; DataInWriteCount1_0 <= '1';
                when "11" => DataInWriteCount1_1 <= '1'; DataInWriteCount1_0 <= '0';
                when others => DataInWriteCount1_1 <= '0'; DataInWriteCount1_0 <= '0';
            end case;
            case DataInWC2(1 downto 0) is
                when "00" => DataInWriteCount2_1 <= '0'; DataInWriteCount2_0 <= '1';
                when "01" => DataInWriteCount2_1 <= '1'; DataInWriteCount2_0 <= '1';
                when "11" => DataInWriteCount2_1 <= '1'; DataInWriteCount2_0 <= '0';
                when others => DataInWriteCount2_1 <= '0'; DataInWriteCount2_0 <= '0';
            end case;
            case DataInWC3(1 downto 0) is
                when "00" => DataInWriteCount3_1 <= '0'; DataInWriteCount3_0 <= '1';
                when "01" => DataInWriteCount3_1 <= '1'; DataInWriteCount3_0 <= '1';
                when "11" => DataInWriteCount3_1 <= '1'; DataInWriteCount3_0 <= '0';
                when others => DataInWriteCount3_1 <= '0'; DataInWriteCount3_0 <= '0';
            end case;
            case DataInWC4(1 downto 0) is
                when "00" => DataInWriteCount4_1 <= '0'; DataInWriteCount4_0 <= '1';
                when "01" => DataInWriteCount4_1 <= '1'; DataInWriteCount4_0 <= '1';
                when "11" => DataInWriteCount4_1 <= '1'; DataInWriteCount4_0 <= '0';
                when others => DataInWriteCount4_1 <= '0'; DataInWriteCount4_0 <= '0';
            end case;
            case DataInWC5(1 downto 0) is
                when "00" => DataInWriteCount5_1 <= '0'; DataInWriteCount5_0 <= '1';
                when "01" => DataInWriteCount5_1 <= '1'; DataInWriteCount5_0 <= '1';
                when "11" => DataInWriteCount5_1 <= '1'; DataInWriteCount5_0 <= '0';
                when others => DataInWriteCount5_1 <= '0'; DataInWriteCount5_0 <= '0';
            end case;
            case DataInWC6(1 downto 0) is
                when "00" => DataInWriteCount6_1 <= '0'; DataInWriteCount6_0 <= '1';
                when "01" => DataInWriteCount6_1 <= '1'; DataInWriteCount6_0 <= '1';
                when "11" => DataInWriteCount6_1 <= '1'; DataInWriteCount6_0 <= '0';
                when others => DataInWriteCount6_1 <= '0'; DataInWriteCount6_0 <= '0';
            end case;
            case DataInWC7(1 downto 0) is
                when "00" => DataInWriteCount7_1 <= '0'; DataInWriteCount7_0 <= '1';
                when "01" => DataInWriteCount7_1 <= '1'; DataInWriteCount7_0 <= '1';
                when "11" => DataInWriteCount7_1 <= '1'; DataInWriteCount7_0 <= '0';
                when others => DataInWriteCount7_1 <= '0'; DataInWriteCount7_0 <= '0';
            end case;
            case DataInWriteCount is
                when "00" => DataInWriteCount <= "01";
                when "01" => DataInWriteCount <= "11";
                when "11" => DataInWriteCount <= "10";
                when others => DataInWriteCount <= "00";
            end case;
        end if;
    end process;
    process (RST, DQSBuf1) begin
        if (RST='1') then
            DataInWriteCount8_0 <= '0';
            DataInWriteCount8_1 <= '0';
            DataInWriteCount9_0 <= '0';
            DataInWriteCount9_1 <= '0';
            DataInWriteCount10_0 <= '0';
            DataInWriteCount10_1 <= '0';
            DataInWriteCount11_0 <= '0';
            DataInWriteCount11_1 <= '0';
            DataInWriteCount12_0 <= '0';
            DataInWriteCount12_1 <= '0';
            DataInWriteCount13_0 <= '0';
            DataInWriteCount13_1 <= '0';
            DataInWriteCount14_0 <= '0';
            DataInWriteCount14_1 <= '0';
            DataInWriteCount15_0 <= '0';
            DataInWriteCount15_1 <= '0';
        elsif (DQSBuf1'event and DQSBuf1='0') then
            case DataInWC8(1 downto 0) is
                when "00" => DataInWriteCount8_1 <= '0'; DataInWriteCount8_0 <= '1';
                when "01" => DataInWriteCount8_1 <= '1'; DataInWriteCount8_0 <= '1';
                when "11" => DataInWriteCount8_1 <= '1'; DataInWriteCount8_0 <= '0';
                when others => DataInWriteCount8_1 <= '0'; DataInWriteCount8_0 <= '0';
            end case;
            case DataInWC9(1 downto 0) is
                when "00" => DataInWriteCount9_1 <= '0'; DataInWriteCount9_0 <= '1';
                when "01" => DataInWriteCount9_1 <= '1'; DataInWriteCount9_0 <= '1';
                when "11" => DataInWriteCount9_1 <= '1'; DataInWriteCount9_0 <= '0';
                when others => DataInWriteCount9_1 <= '0'; DataInWriteCount9_0 <= '0';
            end case;
            case DataInWC10(1 downto 0) is
                when "00" => DataInWriteCount10_1 <= '0'; DataInWriteCount10_0 <= '1';
                when "01" => DataInWriteCount10_1 <= '1'; DataInWriteCount10_0 <= '1';
                when "11" => DataInWriteCount10_1 <= '1'; DataInWriteCount10_0 <= '0';
                when others => DataInWriteCount10_1 <= '0'; DataInWriteCount10_0 <= '0';
            end case;
            case DataInWC11(1 downto 0) is
                when "00" => DataInWriteCount11_1 <= '0'; DataInWriteCount11_0 <= '1';
                when "01" => DataInWriteCount11_1 <= '1'; DataInWriteCount11_0 <= '1';
                when "11" => DataInWriteCount11_1 <= '1'; DataInWriteCount11_0 <= '0';
                when others => DataInWriteCount11_1 <= '0'; DataInWriteCount11_0 <= '0';
            end case;
            case DataInWC12(1 downto 0) is
                when "00" => DataInWriteCount12_1 <= '0'; DataInWriteCount12_0 <= '1';
                when "01" => DataInWriteCount12_1 <= '1'; DataInWriteCount12_0 <= '1';
                when "11" => DataInWriteCount12_1 <= '1'; DataInWriteCount12_0 <= '0';
                when others => DataInWriteCount12_1 <= '0'; DataInWriteCount12_0 <= '0';
            end case;
            case DataInWC13(1 downto 0) is
                when "00" => DataInWriteCount13_1 <= '0'; DataInWriteCount13_0 <= '1';
                when "01" => DataInWriteCount13_1 <= '1'; DataInWriteCount13_0 <= '1';
                when "11" => DataInWriteCount13_1 <= '1'; DataInWriteCount13_0 <= '0';
                when others => DataInWriteCount13_1 <= '0'; DataInWriteCount13_0 <= '0';
            end case;
            case DataInWC14(1 downto 0) is
                when "00" => DataInWriteCount14_1 <= '0'; DataInWriteCount14_0 <= '1';
                when "01" => DataInWriteCount14_1 <= '1'; DataInWriteCount14_0 <= '1';
                when "11" => DataInWriteCount14_1 <= '1'; DataInWriteCount14_0 <= '0';
                when others => DataInWriteCount14_1 <= '0'; DataInWriteCount14_0 <= '0';
            end case;
            case DataInWC15(1 downto 0) is
                when "00" => DataInWriteCount15_1 <= '0'; DataInWriteCount15_0 <= '1';
                when "01" => DataInWriteCount15_1 <= '1'; DataInWriteCount15_0 <= '1';
                when "11" => DataInWriteCount15_1 <= '1'; DataInWriteCount15_0 <= '0';
                when others => DataInWriteCount15_1 <= '0'; DataInWriteCount15_0 <= '0';
            end case;
        end if;
    end process;
    
    DataInRC <= "00" & DataInReadCount_1 & DataInReadCount_0;
    DataInBuf <=  DataInBuf31(conv_integer(DataInRC)) &
                  DataInBuf30(conv_integer(DataInRC)) &
                  DataInBuf29(conv_integer(DataInRC)) &
                  DataInBuf28(conv_integer(DataInRC)) &
                  DataInBuf27(conv_integer(DataInRC)) &
                  DataInBuf26(conv_integer(DataInRC)) &
                  DataInBuf25(conv_integer(DataInRC)) &
                  DataInBuf24(conv_integer(DataInRC)) &
                  DataInBuf23(conv_integer(DataInRC)) &
                  DataInBuf22(conv_integer(DataInRC)) &
                  DataInBuf21(conv_integer(DataInRC)) &
                  DataInBuf20(conv_integer(DataInRC)) &
                  DataInBuf19(conv_integer(DataInRC)) &
                  DataInBuf18(conv_integer(DataInRC)) &
                  DataInBuf17(conv_integer(DataInRC)) &
                  DataInBuf16(conv_integer(DataInRC)) &
                  DataInBuf15(conv_integer(DataInRC)) &
                  DataInBuf14(conv_integer(DataInRC)) &
                  DataInBuf13(conv_integer(DataInRC)) &
                  DataInBuf12(conv_integer(DataInRC)) &
                  DataInBuf11(conv_integer(DataInRC)) &
                  DataInBuf10(conv_integer(DataInRC)) &
                  DataInBuf9(conv_integer(DataInRC)) &
                  DataInBuf8(conv_integer(DataInRC)) &
                  DataInBuf7(conv_integer(DataInRC)) &
                  DataInBuf6(conv_integer(DataInRC)) &
                  DataInBuf5(conv_integer(DataInRC)) &
                  DataInBuf4(conv_integer(DataInRC)) &
                  DataInBuf3(conv_integer(DataInRC)) &
                  DataInBuf2(conv_integer(DataInRC)) &
                  DataInBuf1(conv_integer(DataInRC)) &
                  DataInBuf0(conv_integer(DataInRC));
    process (RST, CLK) begin
        if (RST='1') then
            DataInWriteCountInReadDomain <= "00";
            DataInReadCount_0 <= '0';
            DataInReadCount_1 <= '0';
            DataValidVal <= '0';
            DataInVal <= (others=>'0');
        elsif (CLK'event and CLK='1') then
            DataInWriteCountInReadDomain <= DataInWC0(1 downto 0);
            
            if (DataInRC(1 downto 0)/=DataInWriteCountInReadDomain) then
                DataValidVal <= '1';
                DataInVal <= DataInBuf;
                case DataInRC(1 downto 0) is
                    when "00" => DataInReadCount_1 <= '0'; DataInReadCount_0 <= '1';
                    when "01" => DataInReadCount_1 <= '1'; DataInReadCount_0 <= '1';
                    when "11" => DataInReadCount_1 <= '1'; DataInReadCount_0 <= '0';
                    when others => DataInReadCount_1 <= '0'; DataInReadCount_0 <= '0';
                end case;
            else
                DataValidVal <= '0';
            end if;
        end if;
    end process;
    DataValid <= DataValidVal;
    DataIn <= DataInVal;

    -- Introduce phase shift on DQS here
    IBUF_DLY_ADJ_inst0 : IBUF
        generic map (
            IOSTANDARD => "DEFAULT"
        )
        port map (
            O => DQSDelayed0,
            I => DS_DQS(0)
        );
    DQS0Buffer0 : LUT1
        generic map (
            INIT => "10"
        )
        port map (
            I0 => DQSDelayed0,
            O => DQSBuf0
        );
    Inst2_BB0 : OBUFT port map (I => DQSOut(0), T => DQSTri(0), O => DS_DQS(0));
    
    IBUF_DLY_ADJ_inst1 : IBUF
        generic map (
            IOSTANDARD => "DEFAULT"
        )
        port map (
            I => DS_DQS(1),
            O => DQSDelayed1
        );
    DQS1Buffer0 : LUT1
        generic map (
            INIT => "10"
        )
        port map (
            I0 => DQSDelayed1,
            O => DQSBuf1
        );
    DQSOBuf1 : OBUFT port map (I => DQSOut(1), T => DQSTri(1), O => DS_DQS(1));

    -- Data bus buffers
    DMBuf1 : OBUF port map (I => DDRMask(1), O => DS_DM(1));
    DMBuf0 : OBUF port map (I => DDRMask(0), O => DS_DM(0));
    DQIBuf15 : IBUF port map (O => DDRIn15, I => DS_DQ(15));
    DQIBuf14 : IBUF port map (O => DDRIn14, I => DS_DQ(14));
    DQIBuf13 : IBUF port map (O => DDRIn13, I => DS_DQ(13));
    DQIBuf12 : IBUF port map (O => DDRIn12, I => DS_DQ(12));
    DQIBuf11 : IBUF port map (O => DDRIn11, I => DS_DQ(11));
    DQIBuf10 : IBUF port map (O => DDRIn10, I => DS_DQ(10));
    DQIBuf9 : IBUF port map (O => DDRIn9, I => DS_DQ(9));
    DQIBuf8 : IBUF port map (O => DDRIn8, I => DS_DQ(8));
    DQIBuf7 : IBUF port map (O => DDRIn7, I => DS_DQ(7));
    DQIBuf6 : IBUF port map (O => DDRIn6, I => DS_DQ(6));
    DQIBuf5 : IBUF port map (O => DDRIn5, I => DS_DQ(5));
    DQIBuf4 : IBUF port map (O => DDRIn4, I => DS_DQ(4));
    DQIBuf3 : IBUF port map (O => DDRIn3, I => DS_DQ(3));
    DQIBuf2 : IBUF port map (O => DDRIn2, I => DS_DQ(2));
    DQIBuf1 : IBUF port map (O => DDRIn1, I => DS_DQ(1));
    DQIBuf0 : IBUF port map (O => DDRIn0, I => DS_DQ(0));
   
    DOutBuf :
        for i in 0 to 15 generate
        begin
            DQOBuf : OBUFT 
                port map (
                    I => DDROut(i),
                    T => DDRTri(i),
                    O => DS_DQ(i)
                );
        end generate;
    
    -- Instantiate FFs for address and control strobes
    AddrInst0 : FDCE
        generic map (INIT => '1')
        port map (Q=>DS_A(0), C=>CLK, CE=>'1', CLR=>'0', D=>Addr(0));
    AddrInst1 : FDCE
        generic map (INIT => '1')
        port map (Q=>DS_A(1), C=>CLK, CE=>'1', CLR=>'0', D=>Addr(1));
    AddrInst2 : FDCE
        generic map (INIT => '1')
        port map (Q=>DS_A(2), C=>CLK, CE=>'1', CLR=>'0', D=>Addr(2));
    AddrInst3 : FDCE
        generic map (INIT => '1')
        port map (Q=>DS_A(3), C=>CLK, CE=>'1', CLR=>'0', D=>Addr(3));
    AddrInst4 : FDCE
        generic map (INIT => '1')
        port map (Q=>DS_A(4), C=>CLK, CE=>'1', CLR=>'0', D=>Addr(4));
    AddrInst5 : FDCE
        generic map (INIT => '1')
        port map (Q=>DS_A(5), C=>CLK, CE=>'1', CLR=>'0', D=>Addr(5));
    AddrInst6 : FDCE
        generic map (INIT => '1')
        port map (Q=>DS_A(6), C=>CLK, CE=>'1', CLR=>'0', D=>Addr(6));
    AddrInst7 : FDCE
        generic map (INIT => '1')
        port map (Q=>DS_A(7), C=>CLK, CE=>'1', CLR=>'0', D=>Addr(7));
    AddrInst8 : FDCE
        generic map (INIT => '1')
        port map (Q=>DS_A(8), C=>CLK, CE=>'1', CLR=>'0', D=>Addr(8));
    AddrInst9 : FDCE
        generic map (INIT => '1')
        port map (Q=>DS_A(9), C=>CLK, CE=>'1', CLR=>'0', D=>Addr(9));
    AddrInst10 : FDCE
        generic map (INIT => '1')
        port map (Q=>DS_A(10), C=>CLK, CE=>'1', CLR=>'0', D=>Addr(10));
    AddrInst11 : FDCE
        generic map (INIT => '1')
        port map (Q=>DS_A(11), C=>CLK, CE=>'1', CLR=>'0', D=>Addr(11));
    AddrInst12 : FDCE
        generic map (INIT => '1')
        port map (Q=>DS_A(12), C=>CLK, CE=>'1', CLR=>'0', D=>Addr(12));
    BankAddrInst0 : FDCE
        generic map (INIT => '1')
        port map (Q=>DS_BA(0), C=>CLK, CE=>'1', CLR=>'0', D=>BankAddr(0));
    BankAddrInst1 : FDCE
        generic map (INIT => '1')
        port map (Q=>DS_BA(1), C=>CLK, CE=>'1', CLR=>'0', D=>BankAddr(1));
    CASInst : FDCE
        generic map (INIT => '1')
        port map (Q=>DS_CAS_N, C=>CLK, CE=>'1', CLR=>'0', D=>CAS);
    RASInst : FDCE
        generic map (INIT => '1')
        port map (Q=>DS_RAS_N, C=>CLK, CE=>'1', CLR=>'0', D=>RAS);
    WEInst : FDCE
        generic map (INIT => '1')
        port map (Q=>DS_WE_N, C=>CLK, CE=>'1', CLR=>'0', D=>WE);

end arch;

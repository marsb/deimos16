--
-- Input FIFO
-- Takes serial data from ADC and converts to parallel data in the main system clock domain
--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity DataInFIFO is
    port (
        RST : in std_logic;
        
        ADCClockIn : in std_logic;
        ADCDataIn : in std_logic;
        ADCWordValid : in std_logic;
        
        CLK : in std_logic;
        DataOutEmpty : out std_logic;
        DataOutRE : in std_logic;
        DataOut : out std_logic_vector(13 downto 0)
        
        --; Debug : out std_logic_vector(1 downto 0)
    );
end DataInFIFO;

architecture arch of DataInFIFO is

    signal nADCClockIn : std_logic;
    signal ADCDataInRegP : std_logic;
    signal ADCDataInRegN : std_logic;
    signal ADCDataInSR : std_logic_vector(15 downto 0);
    signal FIFOWriteCount : std_logic_vector(3 downto 0);
    signal RegFIFOWriteCount : std_logic_vector(3 downto 0);
    signal NextFIFOWriteCount : std_logic_vector(3 downto 0);
    signal FIFOWriteCountInReadDomain : std_logic_vector(3 downto 0);
    signal FIFOReadCount : std_logic_vector(3 downto 0);
    signal NextFIFOReadCount : std_logic_vector(3 downto 0);
    type ADC_FIFO_TYPE is array(0 to 15) of std_logic_vector(13 downto 0);
    signal ADCFIFO : ADC_FIFO_TYPE;
    attribute RAM_STYLE : string;
    attribute RAM_STYLE of ADCFIFO : signal is "DISTRIBUTED";    
    
begin

    -- Register incoming DDR data
    nADCClockIn <= not ADCClockIn;
    DataPNInst: IDDR2
        generic map(
            DDR_ALIGNMENT => "NONE",
            INIT_Q0 => '0',
            INIT_Q1 => '0',
            SRTYPE => "ASYNC")
        port map (
            Q0 => ADCDataInRegP,
            Q1 => ADCDataInRegN,
            C0 => ADCClockIn,
            C1 => nADCClockIn,
            CE => '1',
            D => ADCDataIn,
            R => RST,
            S => '0'
        );

    -- Input shift register
    process (ADCClockIn) begin
        if (ADCClockIn'event and ADCClockIn='1') then
            ADCDataInSR <= ADCDataInSR(13 downto 0) & ADCDataInRegP & ADCDataInRegN;
        end if;
    end process;

    -- FIFO write counter
    process (RST, ADCClockIn) begin
        if (RST='1') then
            FIFOWriteCount <= (others=>'0');
            RegFIFOWriteCount <= (others=>'0');
        elsif (ADCClockIn'event and ADCClockIn='1') then
            if (ADCWordValid='1') then
                -- Greycode counter
                -- FIXME: Handle FIFO Full case?
                FIFOWriteCount <= NextFIFOWriteCount;
            end if;
            RegFIFOWriteCount <= FIFOWriteCount;
        end if;
    end process;
    WC3Inst : LUT4 generic map (INIT => X"fe10")
                   port map (I3=>FIFOWriteCount(3), I2=>FIFOWriteCount(2), I1=>FIFOWriteCount(1), I0=>FIFOWriteCount(0), O=>NextFIFOWriteCount(3));
    WC2Inst : LUT4 generic map (INIT => X"b0f4")
                   port map (I3=>FIFOWriteCount(3), I2=>FIFOWriteCount(2), I1=>FIFOWriteCount(1), I0=>FIFOWriteCount(0), O=>NextFIFOWriteCount(2));
    WC1Inst : LUT4 generic map (INIT => X"e44e")
                   port map (I3=>FIFOWriteCount(3), I2=>FIFOWriteCount(2), I1=>FIFOWriteCount(1), I0=>FIFOWriteCount(0), O=>NextFIFOWriteCount(1));
    WC0Inst : LUT4 generic map (INIT => X"3cc3")
                   port map (I3=>FIFOWriteCount(3), I2=>FIFOWriteCount(2), I1=>FIFOWriteCount(1), I0=>FIFOWriteCount(0), O=>NextFIFOWriteCount(0));

    -- FIFO storage
    process (ADCClockIn) begin
        if (ADCClockIn'event and ADCClockIn='1') then
            if (ADCWordValid='1') then
                -- ADCWordValid is a registered version of the frame rising edge so its
                -- delayed relative to the data coming in
                ADCFIFO(conv_integer(FIFOWriteCount)) <= ADCDataInSR(15 downto 2);
            end if;
        end if;
    end process;

    -- Output stage
    process (RST, CLK) begin
        if (RST='1') then
            FIFOWriteCountInReadDomain <= (others=>'0');
            FIFOReadCount <= (others=>'0');
        elsif (CLK'event and CLK='1') then
            FIFOWriteCountInReadDomain <= RegFIFOWriteCount;
            if (DataOutRE='1') then
                -- Greycode counter
                FIFOReadCount <= NextFIFOReadCount;
            end if;
        end if;
    end process;
    RC3Inst : LUT4 generic map (INIT => X"fe10")
                   port map (I3=>FIFOReadCount(3), I2=>FIFOReadCount(2), I1=>FIFOReadCount(1), I0=>FIFOReadCount(0), O=>NextFIFOReadCount(3));
    RC2Inst : LUT4 generic map (INIT => X"b0f4")
                   port map (I3=>FIFOReadCount(3), I2=>FIFOReadCount(2), I1=>FIFOReadCount(1), I0=>FIFOReadCount(0), O=>NextFIFOReadCount(2));
    RC1Inst : LUT4 generic map (INIT => X"e44e")
                   port map (I3=>FIFOReadCount(3), I2=>FIFOReadCount(2), I1=>FIFOReadCount(1), I0=>FIFOReadCount(0), O=>NextFIFOReadCount(1));
    RC0Inst : LUT4 generic map (INIT => X"3cc3")
                   port map (I3=>FIFOReadCount(3), I2=>FIFOReadCount(2), I1=>FIFOReadCount(1), I0=>FIFOReadCount(0), O=>NextFIFOReadCount(0));
                          
    DataOutEmpty <= '0' when FIFOReadCount/=FIFOWriteCountInReadDomain else '1';
    DataOut <= ADCFIFO(conv_integer(FIFOReadCount));


    -- Debug output
    --Debug <= ADCDataInRegP & ADCDataInRegN;
    
end arch;


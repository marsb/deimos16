-- ZestET1 DCM instantiation code
--   File name: ZestET1_Clocks.vhd
--   Version: 1.20
--   Date: 11/5/2011
--
--   By default, the input reference clock is assumed to be 125MHz
--   The generated clocks are:
--        EthClk      : 125MHz clock for GigExpedite interface
--        RAMClk      : 166MHz clock for SDRAM
--        RAMClk90    : 166MHz clock with 90 degree shift for SDRAM
--    
--   Copyright (C) 2011 Orange Tree Technologies Ltd. All rights reserved.
--   Orange Tree Technologies grants the purchaser of a ZestET1 the right to use and
--   modify this logic core in any form including but not limited to VHDL source code or
--   EDIF netlist in FPGA designs that target the ZestET1.
--   Orange Tree Technologies prohibits the use of this logic core or any modification of
--   it in any form including but not limited to VHDL source code or EDIF netlist in
--   FPGA or ASIC designs that target any other hardware unless the purchaser of the
--   ZestET1 has purchased the appropriate licence from Orange Tree Technologies.
--   Contact Orange Tree Technologies if you want to purchase such a licence.
--
--  *****************************************************************************************
--  **
--  **  Disclaimer: LIMITED WARRANTY AND DISCLAIMER. These designs are
--  **              provided to you "as is". Orange Tree Technologies and its licensors 
--  **              make and you receive no warranties or conditions, express, implied, 
--  **              statutory or otherwise, and Orange Tree Technologies specifically 
--  **              disclaims any implied warranties of merchantability, non-infringement,
--  **              or fitness for a particular purpose. Orange Tree Technologies does not
--  **              warrant that the functions contained in these designs will meet your 
--  **              requirements, or that the operation of these designs will be 
--  **              uninterrupted or error free, or that defects in the Designs will be 
--  **              corrected. Furthermore, Orange Tree Technologies does not warrant or 
--  **              make any representations regarding use or the results of the use of the 
--  **              designs in terms of correctness, accuracy, reliability, or otherwise.                                               
--  **
--  **              LIMITATION OF LIABILITY. In no event will Orange Tree Technologies 
--  **              or its licensors be liable for any loss of data, lost profits, cost or 
--  **              procurement of substitute goods or services, or for any special, 
--  **              incidental, consequential, or indirect damages arising from the use or 
--  **              operation of the designs or accompanying documentation, however caused 
--  **              and on any theory of liability. This limitation will apply even if 
--  **              Orange Tree Technologies has been advised of the possibility of such 
--  **              damage. This limitation shall apply notwithstanding the failure of the 
--  **              essential purpose of any limited remedies herein.
--  **
--  *****************************************************************************************

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity ZestET1_Clocks is
    port (
        RST : in std_logic;
        RefClk : in std_logic;      -- Reference input clock
        EthClk : out std_logic;     -- Ethernet interface clock
        RAMRST : out std_logic;     -- Reset for RAM controller
        RAMClk : out std_logic;     -- RAM clock
        RAMClk90 : out std_logic    -- RAM clock with 90 degree phase shift
    );
end ZestET1_Clocks;

architecture arch of ZestET1_Clocks is

    signal RefRST : std_logic;
    signal RefClkBuf : std_logic;
    signal RefClkFBUnBuf : std_logic;
    signal RefClkFx : std_logic;
    signal RefClkLocked : std_logic;
    signal RefClkFB : std_logic;
    signal RefStatus : std_logic_vector(7 downto 0);
    signal RefResetCount : std_logic_vector(15 downto 0);
    
    signal RAMClkVal : std_logic;
    signal RAMClkUnBuf : std_logic;
    signal RAMClk90UnBuf : std_logic;
    signal RAMClkLocked : std_logic;
    signal RAMStatus : std_logic_vector(7 downto 0);
    signal RAMDCMRST : std_logic;
    signal RAMResetCount : std_logic_vector(15 downto 0);
    signal RAMResetTimer : std_logic;
    attribute TIG : string;
    attribute TIG of RAMResetTimer : signal is "TRUE";
    
    signal RegRAMResetTimer : std_logic;
    signal RAMRSTVal : std_logic;
    
begin
    
    -- Generate Ethernet interface clocks
    RefClkDCMInst : DCM_SP
        generic map (
            CLKDV_DIVIDE => 2.0,
            CLKFX_DIVIDE => 3,     -- For 166MHz RAM clock
            CLKFX_MULTIPLY => 4,
            CLKIN_DIVIDE_BY_2 => FALSE,
            CLKIN_PERIOD => 8.0,
            CLKOUT_PHASE_SHIFT => "NONE",
            CLK_FEEDBACK => "1X",
            DESKEW_ADJUST => "SYSTEM_SYNCHRONOUS",
            DLL_FREQUENCY_MODE => "LOW",
            DUTY_CYCLE_CORRECTION => TRUE,
            PHASE_SHIFT => 0,
            STARTUP_WAIT => FALSE
        )
        port map (
            CLK0 => RefClkFBUnBuf,
            CLKFX => RefClkFx,
            LOCKED => RefClkLocked,
            STATUS => RefStatus,
            CLKFB => RefClkFB,
            CLKIN => RefClkBuf,
            PSCLK => '0',
            PSEN => '0',
            PSINCDEC => '0',
            RST => RefRST
        );
    RefClkBuffer : BUFG port map (I => RefClk, O => RefClkBuf);
    RefClkFBInst : BUFG port map (I => RefClkFBUnBuf, O => RefClkFB);
    EthClk <= RefClkFB;

    -- Generate 90 degree phase shifted RAM clock
    RAMClkDCMInst : DCM_SP
        generic map (
            CLKDV_DIVIDE => 2.0,
            CLKFX_DIVIDE => 2,
            CLKFX_MULTIPLY => 2,
            CLKIN_DIVIDE_BY_2 => FALSE,
            CLKIN_PERIOD => 6.0,
            CLKOUT_PHASE_SHIFT => "NONE",
            CLK_FEEDBACK => "1X",
            DESKEW_ADJUST => "SYSTEM_SYNCHRONOUS",
            DLL_FREQUENCY_MODE => "LOW",
            DUTY_CYCLE_CORRECTION => TRUE,
            PHASE_SHIFT => 0,
            STARTUP_WAIT => FALSE
        )
        port map (
            CLK0 => RAMClkUnBuf,
            CLK90 => RAMClk90UnBuf,
            LOCKED => RAMClkLocked,
            STATUS => RAMStatus,
            CLKFB => RAMClkVal,
            CLKIN => RefClkFx,
            PSCLK => '0',
            PSEN => '0',
            PSINCDEC => '0',
            RST => RAMDCMRST
        );
    
    -- Generate resets
    process (RST, RefClkBuf)
    begin
        if (RST='1') then
            RefRST <= '1';
            RefResetCount <= (others=>'0');
        elsif (RefClkBuf'event and RefClkBuf='1') then
            if (RefResetCount/=X"FFFF") then
                RefResetCount <= RefResetCount + 1;
                if (RefResetCount=X"00FF") then
                    RefRST <= '0';
                end if;
            elsif (RefClkLocked='0' or RefStatus(2 downto 1)/="00") then
                RefResetCount <= (others=>'0');
                RefRST <= '1';
            end if;
        end if;
    end process;
    process (RST, RefClkBuf)
    begin
        if (RST='1') then
            RAMDCMRST <= '1';
            RAMResetCount <= (others=>'0');
            RAMResetTimer <= '1';
        elsif (RefClkBuf'event and RefClkBuf='1') then
            if (RefClkLocked='0') then
                RAMResetCount <= (others=>'0');
                RAMDCMRST <= '1';
                RAMResetTimer <= '1';
            elsif (RAMResetCount/=X"FFFF") then
                RAMResetCount <= RAMResetCount + 1;
                if (RAMResetCount=X"00FF") then
                    RAMDCMRST <= '0';
                end if;
            elsif (RAMClkLocked='0' or RAMStatus(1)='1') then
                RAMResetCount <= (others=>'0');
                RAMDCMRST <= '1';
                RAMResetTimer <= '1';
            else
                RAMResetTimer <= '0';
            end if;
        end if;
    end process;
    
    process (RST, RAMClkVal)
    begin
        if (RST='1') then
            RegRAMResetTimer <= '1';
            RAMRSTVal <= '1';
        elsif (RAMClkVal'event and RAMClkVal='1') then
            RegRAMResetTimer <= RAMResetTimer;
            RAMRSTVal <= RegRAMResetTimer;
        end if;
    end process;
    RAMRST <= RAMRSTVal;
    
    -- Buffer RAM clocks
    RAMClkInst : BUFG port map (I => RAMClkUnBuf, O => RAMClkVal);
    RAMClk90Inst : BUFG port map (I => RAMClk90UnBuf, O => RAMClk90);
    RAMClk <= RAMClkVal;

end arch;

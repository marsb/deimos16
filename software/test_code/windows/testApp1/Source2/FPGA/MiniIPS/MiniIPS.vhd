library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity MiniIPS is
    port (
        -- Programmable Clock
        CLK : in std_logic;
        
        -- Flash Interface
        Flash_CSOn : out std_logic;
        Flash_CLK : out std_logic;
        Flash_MOSI : out std_logic;
        Flash_MISO : in std_logic;

        -- IO connector
        IO0_In : in std_logic;
        IO0 : inout std_logic_vector(39 downto 0);
        IO3_In : in std_logic_vector(2 downto 0);
        IO3 : inout std_logic_vector(35 downto 0);

        -- Ethernet Interface
        Eth_Clk : out std_logic;
        Eth_CSn : out std_logic;
        Eth_WEn : out std_logic;
        Eth_A : out std_logic_vector(4 downto 0);
        Eth_D : inout std_logic_vector(15 downto 0);
        Eth_BE : out std_logic_vector(1 downto 0);
        Eth_Intn : in std_logic;

        -- DDR SDRAM interface
        DS_CLK_P : out std_logic;
        DS_CLK_N : out std_logic;
        DS_CKE : out std_logic;
        DS_A : out std_logic_vector(12 downto 0);
        DS_BA : out std_logic_vector(1 downto 0);
        DS_CAS_N : out std_logic;
        DS_RAS_N : out std_logic;
        DS_WE_N : out std_logic;
        DS_DQS : inout std_logic_vector(1 downto 0);
        DS_DM : out std_logic_vector(1 downto 0);
        DS_DQ : inout std_logic_vector(15 downto 0)
    );
end MiniIPS;

architecture arch of MiniIPS is

    --------------------------------------------------------------------------
    -- Declare constants
    
    -- These map the internal signals to IO0 pin index
    type BIT_MAP_TYPE is array (0 to 15) of integer;
    constant ADCClockInPMap : BIT_MAP_TYPE := (20,16,others=>0);
    constant ADCClockInNMap : BIT_MAP_TYPE := (21,17,others=>0);
    constant ADCFrameInPMap : BIT_MAP_TYPE := (24,12,others=>0);
    constant ADCFrameInNMap : BIT_MAP_TYPE := (25,13,others=>0);
    constant ADCDataInPMap : BIT_MAP_TYPE := (2,10,18,4,28,36,26,34,6,14,0,8,32,22,30,38);
    constant ADCDataInNMap : BIT_MAP_TYPE := (3,11,19,5,29,37,27,35,7,15,1,9,33,23,31,39);
    
    --------------------------------------------------------------------------
    -- Declare signals
    
    -- IO signals
    signal ADCClockInP : std_logic_vector(1 downto 0);
    signal ADCClockInN : std_logic_vector(1 downto 0);
    signal ADCClockIn : std_logic_vector(1 downto 0);
    signal ADCClockInVal : std_logic_vector(1 downto 0);
    signal ADCDataInP : std_logic_vector(15 downto 0);
    signal ADCDataInN : std_logic_vector(15 downto 0);
    signal ADCDataIn : std_logic_vector(15 downto 0);
    signal ADCDataInVal : std_logic_vector(15 downto 0);
    signal ADCFrameInP : std_logic_vector(1 downto 0);
    signal ADCFrameInN : std_logic_vector(1 downto 0);
    signal ADCFrameIn : std_logic_vector(1 downto 0);
    signal ADCFrameInVal : std_logic_vector(1 downto 0);

    -- ADC simulator signals
    signal ADCClockOut : std_logic_vector(1 downto 0);
    signal ADCFrameOut : std_logic_vector(1 downto 0);
    signal ADCDataOut : std_logic_vector(15 downto 0);
    signal SimADCClockOut : std_logic;
    signal SimADCFrameOut : std_logic;
    signal SimADCDataOut : std_logic;
    signal nSimEnable : std_logic;
    
    -- Clocks and reset
    signal RST : std_logic;
    signal RAMRST : std_logic;
    signal RAMClk : std_logic;
    signal RAMClk90 : std_logic;
    signal EthClk : std_logic;

    -- Link status variables
    signal LocalIP : std_logic_vector(31 downto 0);
    signal LinkStatus : std_logic;
    signal LinkDuplex : std_logic;
    signal LinkSpeed : std_logic_vector(2 downto 0);
    
    -- Connection state signals
    signal EnableConnection : std_logic_vector(1 downto 0);
    signal Connected : std_logic_vector(1 downto 0);
    signal LastConnected : std_logic_vector(1 downto 0);

    -- Global time stamp
    signal TimeStamp : std_logic_vector(63 downto 0);

    -- Data signals
    signal TxData0 : std_logic_vector(7 downto 0);
    signal TxEnable0 : std_logic;
    signal TxBusy0 : std_logic;
    signal TxBufferEmpty0 : std_logic;
    signal RxEnable0 : std_logic;
    signal RxData0 : std_logic_vector(7 downto 0);
    signal RxBusy0 : std_logic;
    signal TxData1 : std_logic_vector(7 downto 0);
    signal TxEnable1 : std_logic;
    signal TxBusy1 : std_logic;
    signal TxBufferEmpty1 : std_logic;
    signal RxEnable1 : std_logic;
    signal RxData1 : std_logic_vector(7 downto 0);
    signal RxBusy1 : std_logic;

    -- Register access signals
    signal RegWE : std_logic;
    signal RegRE : std_logic;
    signal RegAddr : std_logic_vector(15 downto 0);
    signal RegWriteData : std_logic_vector(7 downto 0);
    signal RegReadValid : std_logic;
    signal RegReadData : std_logic_vector(7 downto 0);
    
    -- Neutron detection
    signal Detection : std_logic;
    signal DetectionStretched : std_logic;
    
    -- GPIO and SPI interfaces
    signal GPIODataOut : std_logic_vector(31 downto 0);
    signal GPIODataIn : std_logic_vector(31 downto 0);
    signal GPIOnEnable : std_logic_vector(31 downto 0);
    signal SPICLK : std_logic;
    signal SPIMOSI : std_logic;
    signal SPIMISO : std_logic;
    signal SDIOnEnable : std_logic;
    signal SDIODataOut : std_logic;
    signal SDIODataIn : std_logic;

begin
    
    -- Connect IO signals
    -- FIXME: pinout of these signals
    IO3(0) <= GPIODataOut(0) when GPIOnEnable(0)='0' else 'Z';
    IO3(1) <= GPIODataOut(1) when GPIOnEnable(1)='0' else 'Z';
    IO3(2) <= GPIODataOut(2) when GPIOnEnable(2)='0' else 'Z';
    IO3(3) <= GPIODataOut(3) when GPIOnEnable(3)='0' else 'Z';
    --IO3(4) <= GPIODataOut(4) when GPIOnEnable(4)='0' else 'Z';
    --IO3(5) <= GPIODataOut(5) when GPIOnEnable(5)='0' else 'Z';
    --IO3(6) <= GPIODataOut(6) when GPIOnEnable(6)='0' else 'Z';
    --IO3(7) <= GPIODataOut(7) when GPIOnEnable(7)='0' else 'Z';
    IO3(32) <= GPIODataOut(4) when GPIOnEnable(4)='0' else 'Z';
    IO3(33) <= GPIODataOut(5) when GPIOnEnable(5)='0' else 'Z';
    IO3(34) <= GPIODataOut(6) when GPIOnEnable(6)='0' else 'Z';
    IO3(35) <= GPIODataOut(7) when GPIOnEnable(7)='0' else 'Z';
    IO3(8) <= GPIODataOut(8) when GPIOnEnable(8)='0' else 'Z';
    IO3(9) <= GPIODataOut(9) when GPIOnEnable(9)='0' else 'Z';
    --IO3(10) <= GPIODataOut(10) when GPIOnEnable(10)='0' else 'Z';
    --IO3(11) <= GPIODataOut(11) when GPIOnEnable(11)='0' else 'Z';
    --IO3(12) <= GPIODataOut(12) when GPIOnEnable(12)='0' else 'Z';
    IO3(13) <= GPIODataOut(13) when GPIOnEnable(13)='0' else 'Z';
    --IO3(14) <= GPIODataOut(14) when GPIOnEnable(14)='0' else 'Z';
    --IO3(15) <= GPIODataOut(15) when GPIOnEnable(15)='0' else 'Z';
    --IO3(16) <= GPIODataOut(16) when GPIOnEnable(16)='0' else 'Z';
    --IO3(17) <= GPIODataOut(17) when GPIOnEnable(17)='0' else 'Z';
    --IO3(18) <= GPIODataOut(18) when GPIOnEnable(18)='0' else 'Z';
    --IO3(19) <= GPIODataOut(19) when GPIOnEnable(19)='0' else 'Z';
    --IO3(20) <= GPIODataOut(20) when GPIOnEnable(20)='0' else 'Z';
    IO3(21) <= GPIODataOut(21) when GPIOnEnable(21)='0' else 'Z';
    --IO3(22) <= GPIODataOut(22) when GPIOnEnable(22)='0' else 'Z';
    --IO3(23) <= GPIODataOut(23) when GPIOnEnable(23)='0' else 'Z';
    IO3(24) <= GPIODataOut(24) when GPIOnEnable(24)='0' else 'Z';
    IO3(25) <= GPIODataOut(25) when GPIOnEnable(25)='0' else 'Z';
    --IO3(26) <= GPIODataOut(26) when GPIOnEnable(26)='0' else 'Z';
    --IO3(27) <= GPIODataOut(27) when GPIOnEnable(27)='0' else 'Z';
    IO3(28) <= GPIODataOut(28) when GPIOnEnable(28)='0' else 'Z';
    IO3(29) <= GPIODataOut(29) when GPIOnEnable(29)='0' else 'Z';
    --IO3(30) <= GPIODataOut(30) when GPIOnEnable(30)='0' else 'Z';
    --GPIODataIn <= IO3(31 downto 0);--FIXME need IO0_In and IO3_In(1)
    GPIODataIn <= IO3_In(1) & IO0_In & IO3(29 downto 0);
    
    IO3(31) <= Detection;
    IO3(20) <= DetectionStretched;
    
    IO3(5) <= SPICLK;
    IO3(16) <= SPIMOSI;
    SPIMISO <= IO3(17);
    IO3(12) <= SDIODataOut when SDIOnEnable='0' else 'Z';    --FIXME: need to output direction for external buffer
    SDIODataIn <= IO3(12);
    IO3(4) <= not SDIOnEnable;
    
    --FIXME: USB connections
    --IO3(6) <= USB_nOE;
    --IO3(7) <= USB_nRD;
    --IO3(10) <= USB_SIWU;
    --IO3(11) <= USB_nWR;
    --USB_Clk <= IO3(14);
    --IO3(15) <= USB_DataOut(7) when USB_nOE='1' else 'Z';
    --IO3(18) <= USB_DataOut(6) when USB_nOE='1' else 'Z';
    --IO3(19) <= USB_DataOut(5) when USB_nOE='1' else 'Z';
    --IO3(22) <= USB_DataOut(4) when USB_nOE='1' else 'Z';
    --IO3(23) <= USB_DataOut(3) when USB_nOE='1' else 'Z';
    --IO3(26) <= USB_DataOut(2) when USB_nOE='1' else 'Z';
    --IO3(27) <= USB_DataOut(1) when USB_nOE='1' else 'Z';
    --IO3(30) <= USB_DataOut(0) when USB_nOE='1' else 'Z';
    --USB_DataIn <= {IO3(15), IO3(18), IO3(19), IO3(22),
    --               IO3(23), IO3(26), IO3(27), IO3(30)};
    --USB_nRXF <= IO3_In(0);
    --USB_nTXE <= IO3_In(2);
    
    -- Instantiate IO buffers
    ADCClockInCopy:
        for g in 0 to 1 generate
        begin
            --ADCClockInInst : IOBUFDS 
            --    generic map (DIFF_TERM=>TRUE,
            --                 IOSTANDARD=>"BLVDS_25")
            --    port map (IO=>IO0(ADCClockInPMap(g)), IOB=>IO0(ADCClockInNMap(g)),
            --              O=>ADCClockIn(g), I=>ADCClockOut(g), T=>nSimEnable);
            ADCClockInInst : IBUFDS 
                generic map (DIFF_TERM=>TRUE,
                             IOSTANDARD=>"LVDS_25")
                port map (I=>IO0(ADCClockInPMap(g)), IB=>IO0(ADCClockInNMap(g)),
                          O=>ADCClockIn(g));
        end generate;
    ADCFrameInCopy:
        for g in 0 to 1 generate
        begin
--            ADCFrameInInst : IOBUFDS 
--                generic map (DIFF_TERM=>TRUE,
--                             IBUF_DELAY_VALUE => "2",
--                             IFD_DELAY_VALUE => "2",
--                             IOSTANDARD=>"BLVDS_25")
--                port map (IO=>IO0(ADCFrameInPMap(g)), IOB=>IO0(ADCFrameInNMap(g)),
--                          O=>ADCFrameIn(g), I=>ADCFrameOut(g), T=>nSimEnable);
            ADCFrameInInst : IBUFDS 
                generic map (DIFF_TERM=>TRUE,
                             IBUF_DELAY_VALUE => "2",
                             IFD_DELAY_VALUE => "2",
                             IOSTANDARD=>"LVDS_25")
                port map (I=>IO0(ADCFrameInPMap(g)), IB=>IO0(ADCFrameInNMap(g)),
                          O=>ADCFrameIn(g));
        end generate;
    ADCDataInCopy:
        for g in 0 to 15 generate
        begin
--            ADCDataInInst : IOBUFDS 
--                generic map (DIFF_TERM=>TRUE,
--                             IBUF_DELAY_VALUE => "2",
--                             IFD_DELAY_VALUE => "2",
--                             IOSTANDARD=>"BLVDS_25")
--                port map (IO=>IO0(ADCDataInPMap(g)), IOB=>IO0(ADCDataInNMap(g)),
--                          O=>ADCDataIn(g), I=>ADCDataOut(g), T=>nSimEnable);
            ADCDataInInst : IBUFDS 
                generic map (DIFF_TERM=>TRUE,
                             IBUF_DELAY_VALUE => "2",
                             IFD_DELAY_VALUE => "2",
                             IOSTANDARD=>"LVDS_25")
                port map (I=>IO0(ADCDataInPMap(g)), IB=>IO0(ADCDataInNMap(g)),
                          O=>ADCDataIn(g));
        end generate;

    -- Choose between simulated ADC and real external ADC
    ADCClockInVal <= ADCClockIn;
    ADCFrameInVal <= ADCFrameIn;
    ADCDataInVal <= ADCDataIn;
    ADCClockOut <= '0' & SimADCClockOut;
    ADCFrameOut <= '0' & SimADCFrameOut;
    ADCDataOut <= "000000000000000" & SimADCDataOut;
    --nSimEnable <= '0';    -- Simulated ADC
    nSimEnable <= '1';      -- External ADC
    
    -- Tie unused signals
    Flash_CSOn <= '1';
    Flash_CLK <= '1';
    Flash_MOSI <= '1';
    --IO0 <= (others=>'Z');
    --IO3 <= (others=>'Z');
    
    -- Reset controller
    ROCInst : ROC port map (O=>RST);

    --------------------------------------------------------------------------
    -- Simulate ADC output
    SimADCInst : entity work.SimADC
        port map (
            CLK => CLK,
            RST => RST,
            
            ADCClockOut => SimADCClockOut,
            ADCFrameOut => SimADCFrameOut,
            ADCDataOut => SimADCDataOut
        );
    
    --------------------------------------------------------------------------
    -- Main application code

    -- Connection management
    -- Open a connection whenever possible
    -- If the host closes the connection then we immediately re-open it
    -- NB: If the connection isn't closed (e.g. host application crashes)
    -- then we would need to close it from this end before we could re-open it
    process (RAMRST, EthClk)
    begin
        if (RAMRST='1') then
            EnableConnection <= "00";
            LastConnected <= "00";
        elsif (EthClk'event and EthClk='1') then
            LastConnected <= Connected;
            if (EnableConnection(0)='0') then
                -- Rising edge will start listening on connection
                EnableConnection(0) <= '1';
            else 
                if (LastConnected(0)='1' and Connected(0)='0') then
                    -- Falling edge indicates connection was closed
                    -- We need to force a rising edge on EnableConnection
                    -- to start listening again
                    EnableConnection(0) <= '0';
                end if;
            end if;
            if (EnableConnection(1)='0') then
                -- Rising edge will start listening on connection
                EnableConnection(1) <= '1';
            else 
                if (LastConnected(1)='1' and Connected(1)='0') then
                    -- Falling edge indicates connection was closed
                    -- We need to force a rising edge on EnableConnection
                    -- to start listening again
                    EnableConnection(1) <= '0';
                end if;
            end if;
        end if;
    end process;

    -- Timestamping
    process (RST, CLK) begin
        if (RST='1') then
            TimeStamp <= (others=>'0');
        elsif (CLK'event and CLK='1') then
            TimeStamp <= TimeStamp + 1;
        end if;
    end process;
    
    -- Instantiate low speed control block
    ControlInst : entity work.Control
        port map (
            CLK => EthClk,
            RST => RST,
            
            TxData => TxData0,
            TxEnable => TxEnable0,
            TxBusy => TxBusy0,
            TxBufferEmpty => TxBufferEmpty0,
            RxEnable => RxEnable0,
            RxData => RxData0,
            RxBusy => RxBusy0,
            
            RegWE => RegWE,
            RegRE => RegRE,
            RegAddr => RegAddr,
            RegWriteData => RegWriteData,
            RegReadValid => RegReadValid,
            RegReadData => RegReadData,
            
            TimeStamp => TimeStamp,
            
            -- SPI, GPIO, ADC SDIO ports
            GPIODataOut => GPIODataOut,
            GPIODataIn => GPIODataIn,
            GPIOnEnable => GPIOnEnable,
            SPICLK => SPICLK,
            SPIMOSI => SPIMOSI,
            SPIMISO => SPIMISO,
            SDIOnEnable => SDIOnEnable,
            SDIODataOut => SDIODataOut,
            SDIODataIn => SDIODataIn
        );
    
    -- Instantiate high speed data block
    DataInputInst : entity work.DataInput
        port map (
            CLK => EthClk,
            RST => RST,
            
            TxData => TxData1,
            TxEnable => TxEnable1,
            TxBusy => TxBusy1,
            TxBufferEmpty => TxBufferEmpty1,
            RxEnable => RxEnable1,
            RxData => RxData1,
            RxBusy => RxBusy1,
            
            ADCClockIn => ADCClockInVal,
            ADCDataIn => ADCDataInVal,
            ADCFrameIn => ADCFrameInVal,
            
            TimeStamp => TimeStamp,
            
            Detection => Detection,
            DetectionStretched => DetectionStretched,
            
            RegWE => RegWE,
            RegRE => RegRE,
            RegAddr => RegAddr,
            RegWriteData => RegWriteData,
            RegReadValid => RegReadValid,
            RegReadData => RegReadData
        );
    
    --------------------------------------------------------------------------
    -- Instantiate clocks
    -- (Assumes default 125MHz reference clock)
    ClockInst : entity work.ZestET1_Clocks
        port map (
            RST => RST,
            RefClk => CLK,
            EthClk => EthClk,
            RAMRST => RAMRST,
            RAMClk => RAMClk,
            RAMClk90 => RAMClk90    
        );

    --------------------------------------------------------------------------
    -- Ethernet interface wrapper instantiation
    EthernetInst : entity work.ZestET1_EthernetWrapper 
        generic map (
            CLOCK_RATE => 125000000
        )
        port map (
            User_CLK => EthClk,
            User_RST => RST,
            User_LocalIPAddr => LocalIP,
            User_LinkStatus => LinkStatus,
            User_LinkDuplex => LinkDuplex,
            User_LinkSpeed => LinkSpeed,

            -- User interface - only one connection used
            -- Control ports for low speed connection
            User_Enable0 => EnableConnection(0),
            User_Server0 => '1',
            User_TCP0 => '1',
            User_TxPause0 => '0',
            User_Connected0 => Connected(0),
            User_MTU0 => X"80",
            User_TTL0 => X"80",
            User_LocalPort0 => X"6000",
            User_WriteRemotePort0 => X"0000",
            User_WriteRemoteIPAddr0 => X"00000000",
            --User_ReadRemotePort0 => RemotePort0,
            --User_ReadRemoteIPAddr0 => RemoteIPAddr0,
            
            -- Transmit and receive ports for low speed connection
            User_TxData0 => TxData0,
            User_TxEnable0 => TxEnable0,
            User_TxBusy0 => TxBusy0,
            User_TxBufferEmpty0 => TxBufferEmpty0,
            User_RxData0 => RxData0,
            User_RxEnable0 => RxEnable0,
            User_RxBusy0 => RxBusy0,

            -- Control ports for high speed connection
            User_Enable1 => EnableConnection(1),
            User_Server1 => '1',
            User_TCP1 => '1',
            User_TxPause1 => '0',
            User_Connected1 => Connected(1),
            User_MTU1 => X"80",
            User_TTL1 => X"80",
            User_LocalPort1 => X"6001",
            User_WriteRemotePort1 => X"0000",
            User_WriteRemoteIPAddr1 => X"00000000",
            --User_ReadRemotePort1 => RemotePort1,
            --User_ReadRemoteIPAddr1 => RemoteIPAddr1,
            
            -- Transmit and receive ports for high speed connection
            User_TxData1 => TxData1,
            User_TxEnable1 => TxEnable1,
            User_TxBusy1 => TxBusy1,
            User_TxBufferEmpty1 => TxBufferEmpty1,
            User_RxData1 => RxData1,
            User_RxEnable1 => RxEnable1,
            User_RxBusy1 => RxBusy1,

            -- Disable all other connections
            User_Enable2 => '0',
            User_Enable3 => '0',
            User_Enable4 => '0',
            User_Enable5 => '0',
            User_Enable6 => '0',
            User_Enable7 => '0',
            User_Enable8 => '0',
            User_Enable9 => '0',
            User_Enable10 => '0',
            User_Enable11 => '0',
            User_Enable12 => '0',
            User_Enable13 => '0',
            User_Enable14 => '0',
            User_Enable15 => '0',
            
            -- Interface to GigExpedite
            Eth_Clk => Eth_Clk,
            Eth_CSn => Eth_CSn,
            Eth_WEn => Eth_WEn,
            Eth_A => Eth_A,
            Eth_D => Eth_D,
            Eth_BE => Eth_BE,
            Eth_Intn => Eth_Intn
        );

    --------------------------------------------------------------------------
    -- SDRAM Buffer
    -- Place holder - not used in the example but ties the pins correctly
    
    -- Instantiate SDRAM component
    SDRAMInst : entity work.ZestET1_SDRAM
        generic map (
            CLOCK_RATE => 166666666
        )
        port map (
            User_CLK => RAMClk,
            User_CLK90 => RAMClk90,
            User_RST => RAMRST,

            User_A => (others=>'0'),
            User_RE => '0',
            User_WE => '0',
            User_Owner => X"00",
            User_BE => (others=>'0'),
            User_DW => (others=>'0'),
            --User_DR => ,
            --User_DR_Valid => ,
            --User_ValidOwner => ,
            --User_Busy => ,
            --User_InitDone => ,

            DS_CLK_P => DS_CLK_P,
            DS_CLK_N => DS_CLK_N,
            DS_A => DS_A,
            DS_BA => DS_BA,
            DS_DQ => DS_DQ,
            DS_DQS => DS_DQS,
            DS_DM => DS_DM,
            DS_CAS_N => DS_CAS_N,
            DS_RAS_N => DS_RAS_N,
            DS_CKE => DS_CKE,
            DS_WE_N => DS_WE_N
        );

end arch;


--
-- Data input test bench
--

library ieee;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use std.textio.all;
 
entity TestDataInput is
end TestDataInput;
 
architecture arch of TestDataInput is
 
    -- Component Declaration for the Unit Under Test (UUT)

    component DataInput
    port (
        CLK : in std_logic;
        RST : in std_logic;
        TxData : out std_logic_vector(7 downto 0);
        TxEnable : out std_logic;
        TxBusy : in std_logic;
        TxBufferEmpty : in std_logic;
        RxEnable : in std_logic;
        RxData : in std_logic_vector(7 downto 0);
        RxBusy : out std_logic;
        ADCClockIn : in std_logic_vector(1 downto 0);
        ADCDataIn : in std_logic_vector(15 downto 0);
        ADCFrameIn : in std_logic_vector(1 downto 0);
        RegWE : in std_logic;
        RegRE : in std_logic;
        RegAddr : in std_logic_vector(15 downto 0);
        RegWriteData : in std_logic_vector(7 downto 0);
        RegReadValid : out std_logic;
        RegReadData : out std_logic_vector(7 downto 0)
    );
    end component;
    
    --Inputs
    signal CLK : std_logic := '0';
    signal RST : std_logic := '0';
    signal TxBusy : std_logic := '0';
    signal TxBufferEmpty : std_logic := '0';
    signal RxEnable : std_logic := '0';
    signal RxData : std_logic_vector(7 downto 0) := (others => '0');
    signal ADCClockIn : std_logic_vector(1 downto 0) := (others => '0');
    signal ADCDataIn : std_logic_vector(15 downto 0) := (others => '0');
    signal ADCFrameIn : std_logic_vector(1 downto 0) := (others => '0');
    signal RegWE : std_logic := '0';
    signal RegRE : std_logic := '0';
    signal RegAddr : std_logic_vector(15 downto 0) := (others => '0');
    signal RegWriteData : std_logic_vector(7 downto 0) := (others => '0');

    --Outputs
    signal TxData : std_logic_vector(7 downto 0);
    signal TxEnable : std_logic;
    signal RxBusy : std_logic;
    signal RegReadValid : std_logic;
    signal RegReadData : std_logic_vector(7 downto 0);

    -- Clock period definitions
    constant CLK_period : time := 8 ns;
    constant ADCCLK_period : time := 4.76 ns;   -- 30MHz sample rate, 14 bits = 210 MHz DDR clock rate
 
begin
 
	-- Instantiate the Unit Under Test (UUT)
    uut: DataInput port map (
        CLK => CLK,
        RST => RST,
        TxData => TxData,
        TxEnable => TxEnable,
        TxBusy => TxBusy,
        TxBufferEmpty => TxBufferEmpty,
        RxEnable => RxEnable,
        RxData => RxData,
        RxBusy => RxBusy,
        ADCClockIn => ADCClockIn,
        ADCDataIn => ADCDataIn,
        ADCFrameIn => ADCFrameIn,
        RegWE => RegWE,
        RegRE => RegRE,
        RegAddr => RegAddr,
        RegWriteData => RegWriteData,
        RegReadValid => RegReadValid,
        RegReadData => RegReadData
    );

    -- Clock process definitions
    CLK_process : process
    begin
        CLK <= '1';
        wait for CLK_period/2;
        CLK <= '0';
        wait for CLK_period/2;
    end process;
    ADCClk_process : process
    begin
        ADCClockIn <= (others=>'0');
        wait for ADCCLK_period/2;
        ADCClockIn <= (others=>'1');
        wait for ADCCLK_period/2;
    end process;
    
    -- Reset process
    RST_process : process
    begin
        RST <= '1';
        wait for 100 ns;
        RST <= '0';
        wait;
    end process;
 
    -- Stimulus process
    -- ADC inputs
    ADC_process : process
        file FileHandle : text;
        variable InputLine : line;
        variable Val : integer;
        variable SigVal : std_logic_vector(13 downto 0);
        variable i : integer;
    begin
        wait for ADCCLK_period/4;
        
        file_open(FileHandle, "TestData.txt", READ_MODE);
        while not endfile(FileHandle) loop
            readline (FileHandle, InputLine);
            read (Inputline, Val);
            SigVal := conv_std_logic_vector(Val, 14);
            
            ADCFrameIn <= "11";
            for i in 13 downto 0 loop
                if (i=6) then
                    ADCFrameIn <= "00";
                end if;
                if (SigVal(i)='1') then
                    ADCDataIn <= (others=>'1');
                else
                    ADCDataIn <= (others=>'0');
                end if;
                wait for ADCCLK_period/2;
            end loop;
        end loop;
        file_close(FileHandle);
    end process;
    
    -- Register setup
    Registers_proc: process
    begin		
        -- Wait for startup
        wait for CLK_period*20.5;

        -- Write to registers
        --RegWE <= '1'; RegAddr <= X"0000"; RegWriteData <= X"00"; wait for CLK_period;   -- Trigger level MSB
        RegWE <= '1'; RegAddr <= X"0000"; RegWriteData <= X"10"; wait for CLK_period;   -- Trigger level MSB
        RegWE <= '1'; RegAddr <= X"0010"; RegWriteData <= X"00"; wait for CLK_period;   -- Trigger level LSB
        RegWE <= '1'; RegAddr <= X"0020"; RegWriteData <= X"00"; wait for CLK_period;   -- Pre-trigger calculation samples MSB
        RegWE <= '1'; RegAddr <= X"0030"; RegWriteData <= X"20"; wait for CLK_period;   -- Pre-trigger calculation samples LSB
        RegWE <= '1'; RegAddr <= X"0040"; RegWriteData <= X"00"; wait for CLK_period;   -- Post-trigger calculation samples MSB
        RegWE <= '1'; RegAddr <= X"0050"; RegWriteData <= X"30"; wait for CLK_period;   -- Post-trigger calculation samples LSB
        RegWE <= '1'; RegAddr <= X"0060"; RegWriteData <= X"00"; wait for CLK_period;   -- Pre-trigger network samples MSB
        RegWE <= '1'; RegAddr <= X"0070"; RegWriteData <= X"28"; wait for CLK_period;   -- Pre-trigger network samples LSB
        --RegWE <= '1'; RegAddr <= X"0070"; RegWriteData <= X"00"; wait for CLK_period;   -- Pre-trigger network samples LSB
        RegWE <= '1'; RegAddr <= X"0080"; RegWriteData <= X"00"; wait for CLK_period;   -- Post-trigger network samples MSB
        RegWE <= '1'; RegAddr <= X"0090"; RegWriteData <= X"38"; wait for CLK_period;   -- Post-trigger network samples LSB
        RegWE <= '1'; RegAddr <= X"00A0"; RegWriteData <= X"3f"; wait for CLK_period;   -- Peak threshold MSB
        RegWE <= '1'; RegAddr <= X"00B0"; RegWriteData <= X"40"; wait for CLK_period;   -- Peak threshold LSB
        RegWE <= '1'; RegAddr <= X"00C0"; RegWriteData <= X"00"; wait for CLK_period;   -- Area threshold MSB
        RegWE <= '1'; RegAddr <= X"00D0"; RegWriteData <= X"30"; wait for CLK_period;   -- Area threshold
        RegWE <= '1'; RegAddr <= X"00E0"; RegWriteData <= X"00"; wait for CLK_period;   -- Area threshold LSB
        RegWE <= '1'; RegAddr <= X"00F0"; RegWriteData <= X"01"; wait for CLK_period;   -- Enable triggering

        RegWE <= '0';
        wait;
    end process;

end;

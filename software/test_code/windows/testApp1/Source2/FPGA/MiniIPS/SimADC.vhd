--
-- Simulation of ADC chip
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity SimADC is
    port (
        CLK : in std_logic;
        RST : in std_logic;
        
        ADCClockOut : out std_logic;
        ADCFrameOut : out std_logic;
        ADCDataOut : out std_logic
    );
end SimADC;

architecture arch of SimADC is

    -- Declare signals
    signal ADCClockOutVal : std_logic;
    signal ADCFrameOutVal : std_logic;
    signal ADCDataOutVal : std_logic_vector(13 downto 0);
    signal Offset : std_logic_vector(3 downto 0);
    
    signal PatternCount : std_logic_vector(5 downto 0);
    signal PatternData : std_logic_vector(13 downto 0);
    
begin

    -- Low speed simulation uses 100MHz clock to simulate (100/14) = 7 Msamples per second
    process (RST, CLK) begin
        if (RST='1') then
            ADCClockOutVal <= '1';
        elsif (CLK'event and CLK='0') then
            ADCClockOutVal <= not ADCClockOutVal;
        end if;
    end process;
    process (RST, CLK) begin
        if (RST='1') then
            Offset <= (others=>'0');
            ADCFrameOutVal <= '0';
            ADCDataOutVal <= (others=>'0');
        elsif (CLK'event and CLK='1') then
            ADCDataOutVal <= ADCDataOutVal(12 downto 0) & '0';
            
            if ((Offset=X"d" and ADCClockOutVal='0') or Offset=X"e") then
                Offset <= (others=>'0');
            else
                Offset <= Offset + 1;
            end if;
            if (Offset=X"d") then
                ADCFrameOutVal <= '1';
                PatternCount <= PatternCount + 1;
                ADCDataOutVal <= PatternData;
            elsif (Offset=X"6") then
                ADCFrameOutVal <= '0';
            end if;
            
        end if;
    end process;
    ADCClockOut <= ADCClockOutVal;
    ADCFrameOut <= ADCFrameOutVal;
    ADCDataOut <= ADCDataOutVal(13);

    -- Pattern data
    process (PatternCount) begin
        case PatternCount is
            when "000000" => PatternData <= conv_std_logic_vector(10, 14);
            when "000001" => PatternData <= conv_std_logic_vector(20, 14);
            when "000010" => PatternData <= conv_std_logic_vector(30, 14);
            when "000011" => PatternData <= conv_std_logic_vector(40, 14);
            when "000100" => PatternData <= conv_std_logic_vector(50, 14);
            when "000101" => PatternData <= conv_std_logic_vector(60, 14);
            when "000110" => PatternData <= conv_std_logic_vector(70, 14);
            when "000111" => PatternData <= conv_std_logic_vector(80, 14);
            when "001000" => PatternData <= conv_std_logic_vector(500, 14);
            when "001001" => PatternData <= conv_std_logic_vector(5000, 14);
            when "001010" => PatternData <= conv_std_logic_vector(3000, 14);
            when "001011" => PatternData <= conv_std_logic_vector(2000, 14);
            when "001100" => PatternData <= conv_std_logic_vector(1000, 14);
            when "001101" => PatternData <= conv_std_logic_vector(750, 14);
            when "001110" => PatternData <= conv_std_logic_vector(500, 14);
            when "001111" => PatternData <= conv_std_logic_vector(250, 14);
            when "010000" => PatternData <= conv_std_logic_vector(100, 14);
            when "010001" => PatternData <= conv_std_logic_vector(10, 14);
            when "010010" => PatternData <= conv_std_logic_vector(20, 14);
            when "010011" => PatternData <= conv_std_logic_vector(30, 14);
            when "010100" => PatternData <= conv_std_logic_vector(40, 14);
            when "010101" => PatternData <= conv_std_logic_vector(30, 14);
            when "010110" => PatternData <= conv_std_logic_vector(20, 14);
            when "010111" => PatternData <= conv_std_logic_vector(10, 14);
            when "011000" => PatternData <= conv_std_logic_vector(20, 14);
            when "011001" => PatternData <= conv_std_logic_vector(30, 14);
            when "011010" => PatternData <= conv_std_logic_vector(40, 14);
            when "011011" => PatternData <= conv_std_logic_vector(30, 14);
            when "011100" => PatternData <= conv_std_logic_vector(20, 14);
            when "011101" => PatternData <= conv_std_logic_vector(10, 14);
            when "011110" => PatternData <= conv_std_logic_vector(20, 14);
            when "011111" => PatternData <= conv_std_logic_vector(30, 14);
            when "100000" => PatternData <= conv_std_logic_vector(40, 14);
            when "100001" => PatternData <= conv_std_logic_vector(30, 14);
            when "100010" => PatternData <= conv_std_logic_vector(20, 14);
            when "100011" => PatternData <= conv_std_logic_vector(10, 14);
            when "100100" => PatternData <= conv_std_logic_vector(20, 14);
            when "100101" => PatternData <= conv_std_logic_vector(30, 14);
            when "100110" => PatternData <= conv_std_logic_vector(40, 14);
            when "100111" => PatternData <= conv_std_logic_vector(30, 14);
            when "101000" => PatternData <= conv_std_logic_vector(20, 14);
            when "101001" => PatternData <= conv_std_logic_vector(10, 14);
            when "101010" => PatternData <= conv_std_logic_vector(20, 14);
            when "101011" => PatternData <= conv_std_logic_vector(30, 14);
            when "101100" => PatternData <= conv_std_logic_vector(40, 14);
            when "101101" => PatternData <= conv_std_logic_vector(30, 14);
            when "101110" => PatternData <= conv_std_logic_vector(20, 14);
            when "101111" => PatternData <= conv_std_logic_vector(10, 14);
            when "110000" => PatternData <= conv_std_logic_vector(20, 14);
            when "110001" => PatternData <= conv_std_logic_vector(30, 14);
            when "110010" => PatternData <= conv_std_logic_vector(40, 14);
            when "110011" => PatternData <= conv_std_logic_vector(30, 14);
            when "110100" => PatternData <= conv_std_logic_vector(20, 14);
            when "110101" => PatternData <= conv_std_logic_vector(10, 14);
            when "110110" => PatternData <= conv_std_logic_vector(20, 14);
            when "110111" => PatternData <= conv_std_logic_vector(30, 14);
            when "111000" => PatternData <= conv_std_logic_vector(40, 14);
            when "111001" => PatternData <= conv_std_logic_vector(30, 14);
            when "111010" => PatternData <= conv_std_logic_vector(20, 14);
            when "111011" => PatternData <= conv_std_logic_vector(10, 14);
            when "111100" => PatternData <= conv_std_logic_vector(20, 14);
            when "111101" => PatternData <= conv_std_logic_vector(30, 14);
            when "111110" => PatternData <= conv_std_logic_vector(40, 14);
            when others => PatternData <= conv_std_logic_vector(30, 14);
        end case;
    end process;
    
end arch;


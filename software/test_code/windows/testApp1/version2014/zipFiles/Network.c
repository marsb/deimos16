
#include <windows.h>
#include <stdlib.h>
#include "ZestET1.h"
#include "Network.h"


//
// Byte stuff data to device with escape characters
// any bytes with value USB_ESC must be preceeded by a second USB_ESC
// This allows us to distinguish between SOF, EOF and payload data
//
static void Net_EscData(unsigned char *Buf, int *Ptr, unsigned char *Data, int Length)
{
    int i = 0;
    while (Length!=0)
    {
        if (Data[i]==NET_ESC)
            Buf[(*Ptr)++] = NET_ESC;
        Buf[(*Ptr)++] = Data[i];
        Length--;
        i++;
    }
}

//
// Remove byte stuffing from data and return the resulting data length
//
static int Net_UnEscData(unsigned char *Src, int Length, unsigned char *Dest, int *Esc)
{
    int i;
    int DPtr = 0;

    for (i=0; i<Length; i++)
    {
        if (Src[i]==NET_ESC && *Esc==0)
            *Esc=1;
        else
        {
            Dest[DPtr++] = Src[i];
            *Esc=0;
        }
    }
    return DPtr;
}

//
// Build and send a packet of data to the USB device
//
static ZESTET1_STATUS Net_SendPacket(ZESTET1_CONNECTION Handle, unsigned char Cmd, unsigned short Addr, 
                                     unsigned long Length, void *Buffer)
{
    ZESTET1_STATUS Status;
    unsigned char *Packet = (unsigned char *)malloc(9+2*Length);
    int PacketPtr = 0;
    unsigned long Written = 0;
    unsigned char Val;

    if (Packet==NULL)
        return ZESTET1_OUT_OF_MEMORY;

    // Build packet to send to device
    Packet[PacketPtr++] = NET_ESC;
    Packet[PacketPtr++] = NET_SOF;
    Packet[PacketPtr++] = Cmd;
    Val = Addr>>8;
    Net_EscData(Packet, &PacketPtr, &Val, 1);
    Val = Addr&0xff;
    Net_EscData(Packet, &PacketPtr, &Val, 1);
    Net_EscData(Packet, &PacketPtr, (unsigned char *)Buffer, Length);
    Packet[PacketPtr++] = NET_ESC;
    Packet[PacketPtr++] = NET_EOF;

    Status = ZestET1WriteData(Handle, Packet, PacketPtr, &Written, 1000);
    free(Packet);
    if (Status!=ZESTET1_SUCCESS || Written!=PacketPtr)
        return Status;

    return ZESTET1_SUCCESS;
}

//
// Read a packet of data from the device
//
static ZESTET1_STATUS Net_RecvPacket(ZESTET1_CONNECTION Handle, unsigned long Length, void *Buffer)
{
    ZESTET1_STATUS Status;
    unsigned char *Packet = (unsigned char *)malloc(5+2*Length);
    int PacketPtr = 0;
    unsigned char *Buff = (unsigned char *)Buffer;
    int BuffPtr = 0;
    unsigned long Read = 0;
    int Esc;

    if (Packet==NULL)
        return ZESTET1_OUT_OF_MEMORY;

    // Get packet header and check for start of frame
    Status = ZestET1ReadData(Handle, Packet+PacketPtr, 3, &Read, 1000);
    if (Status!=ZESTET1_SUCCESS || Read!=3)
    {
        free(Packet);
        return Status;
    }
    if (Packet[0]!=NET_ESC || Packet[1]!=NET_SOF)
    {
        free(Packet);
        return ZESTET1_INTERNAL_ERROR;
    }

    // Get payload data
    Esc = 0;
    while (Length!=0)
    {
        int Bytes;

        Status = ZestET1ReadData(Handle, Packet+PacketPtr, Length, &Read, 1000);
        if (Status!=ZESTET1_SUCCESS || Read!=Length)
        {
            free(Packet);
            return Status;
        }

        Bytes = Net_UnEscData(Packet+PacketPtr, Length, Buff+BuffPtr, &Esc);
        PacketPtr += Length;
        Length -= Bytes;
        BuffPtr+=Bytes;
    }

    // Get packet footer and check for end of frame
    Status = ZestET1ReadData(Handle, Packet+PacketPtr, 2, &Read, 1000);
    if (Status!=ZESTET1_SUCCESS || Read!=2)
    {
        free(Packet);
        return Status;
    }
    if (Packet[PacketPtr]!=NET_ESC || Packet[PacketPtr+1]!=NET_EOF)
    {
        free(Packet);
        return ZESTET1_INTERNAL_ERROR;
    }

    free(Packet);
    return ZESTET1_SUCCESS;
}

//////////////////////////////////////////////////////////////////////////////
// External API

//
// Write trigger unit register
//
ZESTET1_STATUS NetWriteTriggerReg(ZESTET1_CONNECTION Handle, int Index, unsigned short Addr, unsigned char Data)
{
    ZESTET1_STATUS Status;

    // Write to register
    Status = Net_SendPacket(Handle, NET_TRIGGER_WRITE_CMD, (Addr<<4)|Index, 1, &Data);
    if (Status!=ZESTET1_SUCCESS)
        return Status;

    return ZESTET1_SUCCESS;
}

//
// Read trigger unit register
//
ZESTET1_STATUS NetReadTriggerReg(ZESTET1_CONNECTION Handle, int Index, unsigned short Addr, unsigned char *Data)
{
    ZESTET1_STATUS Status;

    // Read from register
    Status = Net_SendPacket(Handle, NET_TRIGGER_READ_CMD, (Addr<<4)|Index, 0, NULL);
    if (Status!=ZESTET1_SUCCESS)
        return Status;

    // Receive result
    Status = Net_RecvPacket(Handle, 1, Data);
    if (Status!=ZESTET1_SUCCESS)
        return Status;

    return ZESTET1_SUCCESS;
}

//
// Write control unit register
//
ZESTET1_STATUS NetWriteControlReg(ZESTET1_CONNECTION Handle, unsigned short Addr, unsigned char Data)
{
    ZESTET1_STATUS Status;

    // Write to register
    Status = Net_SendPacket(Handle, NET_CONTROL_WRITE_CMD, Addr, 1, &Data);
    if (Status!=ZESTET1_SUCCESS)
        return Status;

    return ZESTET1_SUCCESS;
}

//
// Read trigger unit register
//
ZESTET1_STATUS NetReadControlReg(ZESTET1_CONNECTION Handle, unsigned short Addr, unsigned char *Data)
{
    ZESTET1_STATUS Status;

    // Read from register
    Status = Net_SendPacket(Handle, NET_CONTROL_READ_CMD, Addr, 0, NULL);
    if (Status!=ZESTET1_SUCCESS)
        return Status;

    // Receive result
    Status = Net_RecvPacket(Handle, 1, Data);
    if (Status!=ZESTET1_SUCCESS)
        return Status;

    return ZESTET1_SUCCESS;
}

//
// Set GPIO pin directions
//
ZESTET1_STATUS NetSetGPIODirection(ZESTET1_CONNECTION Handle, unsigned long nEnables)
{
    ZESTET1_STATUS Status;

    Status = NetWriteControlReg(Handle, CTRL_GPIO_nOUTPUT_MSB, (nEnables>>24)&0xff);
    if (Status!=ZESTET1_SUCCESS)
        return Status;
    Status = NetWriteControlReg(Handle, CTRL_GPIO_nOUTPUT_2, (nEnables>>16)&0xff);
    if (Status!=ZESTET1_SUCCESS)
        return Status;
    Status = NetWriteControlReg(Handle, CTRL_GPIO_nOUTPUT_1, (nEnables>>8)&0xff);
    if (Status!=ZESTET1_SUCCESS)
        return Status;
    Status = NetWriteControlReg(Handle, CTRL_GPIO_nOUTPUT_LSB, (nEnables>>0)&0xff);
    if (Status!=ZESTET1_SUCCESS)
        return Status;

    return ZESTET1_SUCCESS;
}

//
// Set GPIO pin output states
//
ZESTET1_STATUS NetSetGPIO(ZESTET1_CONNECTION Handle, unsigned long DataOut)
{
    ZESTET1_STATUS Status;

    Status = NetWriteControlReg(Handle, CTRL_GPIO_MSB, (DataOut>>24)&0xff);
    if (Status!=ZESTET1_SUCCESS)
        return Status;
    Status = NetWriteControlReg(Handle, CTRL_GPIO_2, (DataOut>>16)&0xff);
    if (Status!=ZESTET1_SUCCESS)
        return Status;
    Status = NetWriteControlReg(Handle, CTRL_GPIO_1, (DataOut>>8)&0xff);
    if (Status!=ZESTET1_SUCCESS)
        return Status;
    Status = NetWriteControlReg(Handle, CTRL_GPIO_LSB, (DataOut>>0)&0xff);
    if (Status!=ZESTET1_SUCCESS)
        return Status;

    return ZESTET1_SUCCESS;
}

//
// Get GPIO pin input states
//
ZESTET1_STATUS NetGetGPIO(ZESTET1_CONNECTION Handle, unsigned long *DataIn)
{
    unsigned char Val;
    ZESTET1_STATUS Status;

    *DataIn = 0;
    Status = NetReadControlReg(Handle, CTRL_GPIO_MSB, &Val);
    if (Status!=ZESTET1_SUCCESS)
        return Status;
    *DataIn |= Val<<24;
    Status = NetReadControlReg(Handle, CTRL_GPIO_2, &Val);
    if (Status!=ZESTET1_SUCCESS)
        return Status;
    *DataIn |= Val<<16;
    Status = NetReadControlReg(Handle, CTRL_GPIO_1, &Val);
    if (Status!=ZESTET1_SUCCESS)
        return Status;
    *DataIn |= Val<<8;
    Status = NetReadControlReg(Handle, CTRL_GPIO_LSB, &Val);
    if (Status!=ZESTET1_SUCCESS)
        return Status;
    *DataIn |= Val<<0;

    return ZESTET1_SUCCESS;
}

//
// Read/write SPI interface
//
ZESTET1_STATUS NetSPIReadWrite(ZESTET1_CONNECTION Handle, int ClockRate, int BitWidth,
                               unsigned char *WriteData, unsigned char *ReadData, int Length)
{
    ZESTET1_STATUS Status;
    int i;
    unsigned char Val;

    Status = NetWriteControlReg(Handle, CTRL_SPI_MUX_LEN, 0x80 | (BitWidth-1));  // Choose SPI input data and set bit width
    if (Status!=ZESTET1_SUCCESS)
        return Status;
    Status = NetWriteControlReg(Handle, CTRL_SPI_CLOCK_RATE, ClockRate);
    if (Status!=ZESTET1_SUCCESS)
        return Status;

    for (i=0; i<Length; i++)
    {
        if (WriteData!=NULL)
            Val = WriteData[i];
        else
            Val = 0;
        Status = NetWriteControlReg(Handle, CTRL_SPI_DATA, Val);
        if (Status!=ZESTET1_SUCCESS)
            return Status;
        if (ReadData!=NULL)
        {
            Sleep(1);   // Wait for transaction to happen
            Status = NetReadControlReg(Handle, CTRL_SPI_DATA, &Val);
            if (Status!=ZESTET1_SUCCESS)
                return Status;
            ReadData[i] = Val;
        }
    }

    return ZESTET1_SUCCESS;
}

//
// Read/write SDIO interface
ZESTET1_STATUS NetSDIOReadWrite(ZESTET1_CONNECTION Handle, int ClockRate, int BitWidth,
                                unsigned char *nEnables, unsigned char *WriteData, unsigned char *ReadData, int Length)
{
    ZESTET1_STATUS Status;
    int i;
    unsigned char Val;
    unsigned char EnVal;

    Status = NetWriteControlReg(Handle, CTRL_SPI_MUX_LEN, (BitWidth-1));  // Choose SDIO input data and set bit width
    if (Status!=ZESTET1_SUCCESS)
        return Status;
    Status = NetWriteControlReg(Handle, CTRL_SPI_CLOCK_RATE, ClockRate);
    if (Status!=ZESTET1_SUCCESS)
        return Status;

    for (i=0; i<Length; i++)
    {
        if (WriteData!=NULL)
        {
            EnVal = nEnables[i];
            Val = WriteData[i];
        }
        else
        {
            EnVal = 0;
            Val = 0;
        }
        Status = NetWriteControlReg(Handle, CTRL_SPI_ENABLE, EnVal);
        if (Status!=ZESTET1_SUCCESS)
            return Status;
        Status = NetWriteControlReg(Handle, CTRL_SPI_DATA, Val);
        if (Status!=ZESTET1_SUCCESS)
            return Status;
        if (ReadData!=NULL)
        {
            Sleep(1);   // Wait for transaction to happen
            Status = NetReadControlReg(Handle, CTRL_SPI_DATA, &Val);
            if (Status!=ZESTET1_SUCCESS)
                return Status;
            ReadData[i] = Val;
        }
    }

    return ZESTET1_SUCCESS;
}

//
// Read one trigger's worth of data
//
ZESTET1_STATUS NetReadTriggerData(ZESTET1_CONNECTION DataConn, unsigned char *Buffer, int Length)
{
    ZESTET1_STATUS Status;
    unsigned char *Packet = (unsigned char *)malloc(5+2*Length);
    int PacketPtr = 0;
    unsigned char *Buff = (unsigned char *)Buffer;
    int BuffPtr = 0;
    unsigned long Read = 0;
    int Esc;
    int Len;

    if (Packet==NULL)
        return ZESTET1_OUT_OF_MEMORY;

    // Get packet header and check for start of frame
    Status = ZestET1ReadData(DataConn, Packet+PacketPtr, 2, &Read, 1000);
    if (Status!=ZESTET1_SUCCESS || Read!=2)
    {
        free(Packet);
        return Status;
    }
    if (Packet[0]!=NET_ESC || Packet[1]!=NET_SOF)
    {
        free(Packet);
        return ZESTET1_INTERNAL_ERROR;
    }

    // Get header data
    Esc = 0;
    Len = min(Length, 12);
    Length -= Len;
    PacketPtr = 0;
    while (Len!=0)
    {
        int Bytes;

        Status = ZestET1ReadData(DataConn, Packet+PacketPtr, Len, &Read, 1000);
        if (Status!=ZESTET1_SUCCESS || Read!=Len)
        {
            free(Packet);
            return Status;
        }

        Bytes = Net_UnEscData(Packet+PacketPtr, Len, Buff+BuffPtr, &Esc);
        PacketPtr += Len;
        Len -= Bytes;
        BuffPtr+=Bytes;
    }

    // Get header data
    Esc = 0;
    Len = ((Buffer[8]<<8) | Buffer[9]) +    // Pre trigger samples
          ((Buffer[10]<<8) | Buffer[11]);   // Post trigger samples
    Len*=2;
    Len = min(Len, Length);
    Length -= Len;
    while (Len!=0)
    {
        int Bytes;

        Status = ZestET1ReadData(DataConn, Packet+PacketPtr, Len, &Read, 1000);
        if (Status!=ZESTET1_SUCCESS || Read!=Len)
        {
            free(Packet);
            return Status;
        }

        Bytes = Net_UnEscData(Packet+PacketPtr, Len, Buff+BuffPtr, &Esc);
        PacketPtr += Len;
        Len -= Bytes;
        BuffPtr+=Bytes;
    }

    // Get packet footer
    Len = min(6, Length);
    PacketPtr = 0;
    while (Len!=0)
    {
        int Bytes;

        Status = ZestET1ReadData(DataConn, Packet+PacketPtr, Len, &Read, 1000);
        if (Status!=ZESTET1_SUCCESS || Read!=Len)
        {
            free(Packet);
            return Status;
        }

        Bytes = Net_UnEscData(Packet+PacketPtr, Len, Buff+BuffPtr, &Esc);
        PacketPtr += Len;
        Len -= Bytes;
        BuffPtr+=Bytes;
    }

    // Get end of frame
    Status = ZestET1ReadData(DataConn, Packet+PacketPtr, 2, &Read, 1000);
    if (Status!=ZESTET1_SUCCESS || Read!=2)
    {
        free(Packet);
        return Status;
    }
    if (Packet[PacketPtr]!=NET_ESC || Packet[PacketPtr+1]!=NET_EOF)
    {
        free(Packet);
        return ZESTET1_INTERNAL_ERROR;
    }

    free(Packet);
    return ZESTET1_SUCCESS;
}


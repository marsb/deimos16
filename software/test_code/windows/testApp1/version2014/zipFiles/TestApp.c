//////////////////////////////////////////////////////////////////////
//
// File:      TestApp.c
//
// Purpose:
//    MiniIPS test application
//  
//////////////////////////////////////////////////////////////////////

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "ZestET1.h"
#include "Network.h"

//
// Error handler function
//
void ErrorHandler(const char *Function, 
                  ZESTET1_CARD_INFO *CardInfo,
                  ZESTET1_STATUS Status,
                  const char *Msg)
{
    printf("**** TestApp - Function %s returned an error\n        \"%s\"\n\n", Function, Msg);
    exit(1);
}

//
// Dump trigger results from FPGA
//
void DumpData(ZESTET1_CONNECTION DataConn)
{
    ZESTET1_STATUS Status;
    unsigned char Buffer[1024];

    ZestET1RegisterErrorHandler(NULL);
    while(1)
    {
        Status=NetReadTriggerData(DataConn, Buffer, sizeof(Buffer));
        if (Status==ZESTET1_SUCCESS)
        {
            unsigned __int64 TimeStamp;
            int PreSamples;
            int PostSamples;
            int i;

            // Received some data
            TimeStamp = (((unsigned long long)(Buffer[0]))<<56ll) | (((unsigned long long)(Buffer[1]))<<48ll) |
                        (((unsigned long long)(Buffer[2]))<<40ll) | (((unsigned long long)(Buffer[3]))<<32ll) |
                        (((unsigned long long)(Buffer[4]))<<24ll) | (((unsigned long long)(Buffer[5]))<<16ll) |
                        (((unsigned long long)(Buffer[6]))<<8ll) | (((unsigned long long)(Buffer[7]))<<0ll);
            printf("Time: %.2f secs\n", TimeStamp/100000000.0);
            PreSamples = (Buffer[8]<<8) | Buffer[9];
            PostSamples = (Buffer[10]<<8) | Buffer[11];
            for (i=0; i<PreSamples; i++)
            {
                int Val = (Buffer[12+2*i]<<8) | Buffer[12+2*i+1];
                printf("%d\n", Val);

            }
            printf("--Trigger Point--\n");
            for (; i<PreSamples+PostSamples; i++)
            {
                printf("%d\n", (Buffer[12+2*i]<<8) | Buffer[12+2*i+1]);
            }
            i = 12+2*i;
            printf("Neutron? : %s\n", Buffer[i] ? "Yes" : "No");
            printf("Peak : %d\n", (Buffer[i+1]<<8) | Buffer[i+2]);
            printf("Area : %d\n", (Buffer[i+3]<<16) | (Buffer[i+4]<<8) | Buffer[i+5]);

            ZestET1RegisterErrorHandler(ErrorHandler);
            return;
        }
    }
}

//
// Main program
//
int main(int argc, char **argv)
{
    unsigned long Count;
    unsigned long NumCards;
    ZESTET1_CARD_INFO *CardInfo;
    ZESTET1_CONNECTION CtrlConn;
    ZESTET1_CONNECTION DataConn;
    unsigned char Val;
    unsigned long Time;
    int i;
    unsigned char nEnables[32];
    unsigned char ReadData[32];
    unsigned char WriteData[32];

    //
    // Install an error handler and initialise library
    //
    ZestET1RegisterErrorHandler(ErrorHandler);
    ZestET1Init();

    //
    // Request information about the system
    // Wait for 2 seconds (on each interface) for boards to respond to query
    //
    printf("Searching for ZestET1 cards...\n");
    ZestET1CountCards(&NumCards, &CardInfo, 2000);
    printf("%ld available cards in the system\n\n\n", NumCards);
    if (NumCards==0)
    {
        printf("No cards found\n");
        exit(1);
    }

    for (Count=0; Count<NumCards; Count++)
    {
        printf("%ld : SerialNum = %ld, IPAddress = %d.%d.%d.%d\n",
            Count, CardInfo[Count].SerialNumber,
            CardInfo[Count].IPAddr[0], CardInfo[Count].IPAddr[1],
            CardInfo[Count].IPAddr[2], CardInfo[Count].IPAddr[3]);
    }

    //
    // Configure the FPGA directly from a file
    // Wait for user FPGA application to start listening on its connection
    // Open control and data connections to user FPGA
    //
    ZestET1ConfigureFromFile(&CardInfo[0], "../../FPGA/MiniIPS/MiniIPS.bit");
    Sleep(1000);
    ZestET1OpenConnection(&CardInfo[0], ZESTET1_TYPE_TCP, 0x6000, 0, &CtrlConn);
    ZestET1OpenConnection(&CardInfo[0], ZESTET1_TYPE_TCP, 0x6001, 0, &DataConn);

    // 
    // Access registers
    //
    NetReadControlReg(CtrlConn, CTRL_FIRMWARE_VERSION, &Val);
    printf("Firmware version : %x.%x\n", Val>>4, Val&0xf);

    // Get Time
    for (i=0; i<10; i++)
    {
        NetWriteControlReg(CtrlConn, CTRL_TIME_MSB, 0); // Latches current time
        NetReadControlReg(CtrlConn, CTRL_TIME_MSB, &Val); // Latches current time
        Time = Val<<24;
        NetReadControlReg(CtrlConn, CTRL_TIME_2, &Val); // Latches current time
        Time |= Val<<16;
        NetReadControlReg(CtrlConn, CTRL_TIME_1, &Val); // Latches current time
        Time |= Val<<8;
        NetReadControlReg(CtrlConn, CTRL_TIME_LSB, &Val); // Latches current time
        Time |= Val;
        printf("Current Time = %d\n", Time);
    }

    // Test SPI
    // Set GPIO pin - the SPI CS is just a GPIO pin
    NetSetGPIO(CtrlConn, 0xffffffff);   // Set GPIO0 high
    NetSetGPIODirection(CtrlConn, 0xfffffffe);      // Set GPIO0 as output
    NetSetGPIO(CtrlConn, 0xfffffffe);   // Set GPIO0 low
    WriteData[0] = 0xaa;
    ReadData[0] = 0;
    NetSPIReadWrite(CtrlConn, 2, 8, WriteData, ReadData, 1); 
    NetSetGPIO(CtrlConn, 0xffffffff);   // Set GPIO0 high

    // Test ADC SDIO
    NetSetGPIO(CtrlConn, 0xffffffff);   // Set GPIO1 high
    NetSetGPIODirection(CtrlConn, 0xfffffffd);      // Set GPIO1 as output
    // Read model number
    NetSetGPIO(CtrlConn, 0xfffffffd);   // Set GPIO1 low
    nEnables[0] = 0x00;                 // Output
    nEnables[1] = 0x00;
    nEnables[2] = 0xff;                 // Input
    WriteData[0] = 0x80 | 0x00 | 0x00;  // RnW = 1 so read, W1:0=0 so read 1 byte, A12:8=0
    WriteData[1] = 0x01;                // A7:0 = 0x01 so read from register address 0x001
    WriteData[2] = 0x00;
    ReadData[0] = 0;
    ReadData[1] = 0;
    ReadData[2] = 0;
    NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
    printf("ADC model = %02x\n", ReadData[2]);
    // Write register
    NetSetGPIO(CtrlConn, 0xfffffffd);   // Set GPIO1 low
    nEnables[0] = 0x00;                 // Output
    nEnables[1] = 0x00;
    nEnables[2] = 0x00;
    WriteData[0] = 0x00 | 0x00 | 0x00;  // RnW = 0 so read, W1:0=0 so read 1 byte, A12:8=0
    WriteData[1] = 0x01;                // A7:0 = 0x01 so write to register address 0x001 (does nothing)
    WriteData[2] = 0x00;
    ReadData[0] = 0;
    ReadData[1] = 0;
    ReadData[2] = 0;
    NetSDIOReadWrite(CtrlConn, 2, 8, nEnables, WriteData, ReadData, 3); 
    NetSetGPIO(CtrlConn, 0xffffffff);   // Set GPIO1 high

    //
    // Test triggering
    //
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_TRIG_LEVEL_HIGH, (1000>>8));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_TRIG_LEVEL_LOW, (1000>>0));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_PRE_CALC_SAMPLES_HIGH, (5>>8));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_PRE_CALC_SAMPLES_LOW, (5>>0));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_POST_CALC_SAMPLES_HIGH, (12>>8));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_POST_CALC_SAMPLES_LOW, (12>>0));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_PRE_NET_SAMPLES_HIGH, (7>>8));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_PRE_NET_SAMPLES_LOW, (7>>0));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_POST_NET_SAMPLES_HIGH, (17>>8));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_POST_NET_SAMPLES_LOW, (17>>0));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_PEAK_THRESHOLD_HIGH, (7000>>8));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_PEAK_THRESHOLD_LOW, (7000>>0));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_AREA_THRESHOLD_HIGH, (3000>>16));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_AREA_THRESHOLD_MED, (3000>>8));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_AREA_THRESHOLD_LOW, (3000>>0));
    NetWriteTriggerReg(CtrlConn, 0, TRIGGER_ENABLE, 1);
    for (i=0; i<10; i++)
        DumpData(DataConn);

    //
    // Free the card information structure
    //
    ZestET1CloseConnection(CtrlConn);
    ZestET1CloseConnection(DataConn);
    ZestET1FreeCards(CardInfo);

    //
    // Close library
    //
    ZestET1Close();

    return 0;
}

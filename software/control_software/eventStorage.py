import threading
import Queue
import numpy as np
import time
from saveEvents import SaveEvents

class EventStorage(threading.Thread):
    def __init__(self, eventQueue):
        threading.Thread.__init__(self)
        self.eventQueue = eventQueue
        self.daemon = True
        self.events=[]
        self.maxNSamples=0
        self.backupInterval=30*60 #in s
        self.lastBackupTime=0
        self.nextEventToBackup=0
        self.saveEvents=SaveEvents()
        
    def run(self):
        self.signal = True
        sleepTimeNoData=1.0
        self.lastBackupTime=time.time()

        while self.signal:
            #print "looping (storage)"
            oldTime=time.time()
            try: # New data in the queue
                event=self.eventQueue.get(False)
                self.maxNSamples=max(self.maxNSamples,
                                     event.nPreSamples+event.nPostSamples)
                self.events.append(event)
            except Queue.Empty: # No new data in the queue
                # print "No new data"
                time.sleep(sleepTimeNoData-(oldTime-time.time()))  # wait some time

            ## Save backup root file
            if time.time()>self.lastBackupTime+self.backupInterval:
                lastEventToBackup=len(self.events)-1
                self.saveEvents.set_filename("backup_"+time.strftime("%Y-%m-%d_%H:%M:%S")+".root")
                self.saveEvents.create_event_tree(self.getMaxNSamples())
                self.saveEvents.create_settings_tree()
                self.saveEvents.add_events(self.events[self.nextEventToBackup:lastEventToBackup+1])
                self.saveEvents.write_root_file()
                self.saveEvents.close_root_file()
                self.lastBackupTime=time.time()
                self.nextEventToBackup=lastEventToBackup+1
            
                
    def getEvents(self):
        return self.events
        
    def getMaxNSamples(self):
        return self.maxNSamples

    def clear(self):
        self.events=[]
        self.lastBackup=time.time()
        self.nextEventToBackup=0
        
    def stop(self):
        self.signal=False
        # time.sleep(2)

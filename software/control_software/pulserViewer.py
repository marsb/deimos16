#!/usr/bin/python

import Tkinter as tk

import matplotlib
import threading
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
# implement the default mpl key bindings
# from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure
# import matplotlib.pyplot as plt

import numpy as np
import scipy as sc
import time

# from lmfit import minimize, Parameters, Minizer

class pulserViewer:
    def __init__(self,master,daq):
        """ TK frame for setting pulser parameters online """

        self.daq=daq
        self.master=master
        self.master.grid()

        # run close window when exit
        self.master.protocol("WM_DELETE_WINDOW", self.close_window)
        self.frame = tk.Frame(self.master)
        self.frame.grid(column=0,row=0,sticky='NSEW')

        # Text pulser rate:
        rateLabel= tk.Label(self.frame, text="Pulser rate:",
                              anchor="w",fg="black",bg="white")
        rateLabel.grid(column=0,row=0,sticky='EW')
   
        # Add rate entry field
        self.rateVariable = tk.DoubleVar()
        self.rateEntry = tk.Entry(self.frame, 
                                         textvariable=self.rateVariable,
                                         width=10) # Text entry
        self.rateEntry.grid(column=1,row=0,sticky='EW') # Stick East West 
        # (Tries to stick both right and left, which makes it expanding)
        #self.pulserRateEntry.bind("<Return>", self.SetEventEntry)
        self.rateVariable.set(0)

        # Text pulser rate:
        rateUnitLabel= tk.Label(self.frame, text="Hz",
                              anchor="w",fg="black",bg="white")
        rateUnitLabel.grid(column=2,row=0,sticky='EW')


        # Button toolbar
        self.buttonToolbar=tk.Frame(self.frame)
        self.buttonToolbar.grid(column=2,row=1,sticky='W')
        # Add button set
        setButton = tk.Button(self.buttonToolbar,text=u"Set",
                                    command=self.SetPulser)
        setButton.grid(column=0,row=0)
        # Add button read
        closeButton = tk.Button(self.buttonToolbar,text=u"Close",
                                    command=self.close_window)
        closeButton.grid(column=1,row=0)

        
        
# # Enable resizing
        self.master.resizable(True,False) # Window can only resize horizontally
        self.master.grid_columnconfigure(0,weight=1) # resize only the second column (1)
        #self.master.grid_rowconfigure(0,weight=1) # resize only the third row (2)
        
        # self.frame.resizable(True,True) # Window can only resize horizontally
        self.frame.grid_columnconfigure(1,weight=1) # resize only the first column (0)
        # self.frame.grid_rowconfigure(2,weight=1) # resize only the third row (2)

        # # But prevent constant resizing
        # self.update()
        # self.geometry(self.geometry()) 

        # Keep the text entry focused
        # self.eventNumberEntry.focus_set()
        # self.eventNumberEntry.selection_range(0, tk.END)


    def close_window(self):
        try:
            self.pulser.stop()
        except AttributeError:
            pass
        self.master.destroy()

    def SetPulser(self):
        try:
            if not self.pulser.isAlive():
                print "Starting new  pulser"
                self.pulser=Pulser(self.daq)  
                self.pulser.start()
        except AttributeError:
                print "Starting new  pulser"
                self.pulser=Pulser(self.daq)  
                self.pulser.start()

        self.pulser.setRate(float(self.rateVariable.get()))
            

    def set_time_bin(self,event):
        timeBin=self.timeBinVariable.get()        
        # self.load_new_event(nEvent)
        
        self.rateCalculator.stop()
        # self.rateCalculator=RateCalculator(self.events,self.totalRatePlotter,timeBin)
        self.rateCalculator.start() # Start thread

        # Keep the text entry focused
        self.timeBinEntry.focus_set()
        self.timeBinEntry.selection_range(0, tk.END)


class Pulser(threading.Thread):
    def __init__(self, daq):
        threading.Thread.__init__(self)
        self.daq=daq
        self.daemon = True
        self.sleepTime=1.0
        self.nPulses=0
    def run(self):
        self.signal = True
        while self.signal:
            self.nPulses+=1
            self.daq.shoot_calibration_pulse()
            if (self.nPulses%1000)==0:
                print "Number of pulses:", self.nPulses
            oldTime=time.time()
            time.sleep(self.sleepTime-(oldTime-time.time()))  # wait some time
            

    def setRate(self,rate):
        print "Pulser rate: ", rate, "Hz"
        self.sleepTime=1./rate
        self.nPulses=0

    def stop(self):
        self.signal=False
        # time.sleep(10)


def main():

    root=tk.Tk()
    eventStorage=None
    app=RateViewer(root,eventStorage)
    app.master.title('Rate viewer')
    root.mainloop()


if __name__ == "__main__":
    main()

#!/usr/bin/python

import Tkinter as tk

import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
# implement the default mpl key bindings
# from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure
from matplotlib.backend_bases import key_press_handler
import numpy as np
import scipy as sc
# from lmfit import minimize, Parameters, Minizer

class waveformViewer:
    def __init__(self,master,eventStorage):

        self.eventStorage=eventStorage
        self.events=eventStorage.getEvents()

        self.master=master
        self.master.grid()
        self.frame = tk.Frame(self.master)
        self.frame.grid(column=0,row=0,sticky='NSEW')
        
        # Make a toolbar for events
        self.eventToolbar=tk.Frame(self.frame)
        self.eventToolbar.grid(column=0,row=0, sticky='W')


        # Add button first
        self.firstEventButton = tk.Button(self.eventToolbar,text=u"First",
                                        command=self.SetEventFirst)
        self.firstEventButton.grid(column=0,row=0)


        # Add button previous
        self.previousEventButton = tk.Button(self.eventToolbar,text=u"Previous",
                                        command=self.SetEventPrevious)
        self.previousEventButton.grid(column=1,row=0)

        # Add text entry
        self.eventNumberVariable = tk.IntVar()
        self.eventNumberEntry = tk.Entry(self.eventToolbar, 
                                         textvariable=self.eventNumberVariable,
                                         width=10) # Text entry
        self.eventNumberEntry.grid(column=2,row=0,sticky='EW') # Stick East West 
        # (Tries to stick both right and left, which makes it expanding)
        self.eventNumberEntry.bind("<Return>", self.SetEventEntry)
        self.eventNumberVariable.set(u"-1")

        # Add button next
        self.nextEventButton = tk.Button(self.eventToolbar,text=u"Next",
                                    command=self.SetEventNext)
        self.nextEventButton.grid(column=3,row=0)

        # Add button last
        self.lastEventButton = tk.Button(self.eventToolbar,text=u"Last",
                                         command=self.SetEventLast)
        self.lastEventButton.grid(column=4,row=0)



        # Make info frame
        self.infoFrame=tk.Frame(self.frame)
        self.infoFrame.grid(column=0,row=1,columnspan=2,sticky='W')

        # Event Id
        eventIdLabel= tk.Label(self.infoFrame, text="Id:",
                              anchor="w",fg="white",bg="blue")
        eventIdLabel.grid(column=0,row=0,sticky='EW')
        self.eventIdVariable = tk.StringVar()
        label = tk.Label(self.infoFrame, textvariable=self.eventIdVariable,
                              anchor="w",fg="black",bg="white")
        label.grid(column=1,row=0,sticky='EW')
        self.eventIdVariable.set(u"id")

        # Event FPGA
        label= tk.Label(self.infoFrame, text=" FPGA:",
                              anchor="w",fg="white",bg="blue")
        label.grid(column=2,row=0,sticky='EW')
        self.eventFpgaVariable = tk.StringVar()
        label = tk.Label(self.infoFrame, textvariable=self.eventFpgaVariable,
                              anchor="w",fg="black",bg="white")
        label.grid(column=3,row=0,sticky='EW')
        self.eventFpgaVariable.set(u"fpga")

        # Event channel
        label= tk.Label(self.infoFrame, text=" Ch:",
                              anchor="w",fg="white",bg="blue")
        label.grid(column=4,row=0,sticky='EW')
        self.eventChVariable = tk.StringVar()
        label = tk.Label(self.infoFrame, textvariable=self.eventChVariable,
                              anchor="w",fg="black",bg="white")
        label.grid(column=5,row=0,sticky='EW')
        self.eventChVariable.set(u"ch")

        # Event Time
        eventTimeLabel= tk.Label(self.infoFrame, text=" Time:",
                              anchor="w",fg="white",bg="blue")
        eventTimeLabel.grid(column=6,row=0,sticky='EW')
        self.eventTimeVariable = tk.StringVar()
        label = tk.Label(self.infoFrame, textvariable=self.eventTimeVariable,
                              anchor="w",fg="black",bg="white")
        label.grid(column=7,row=0,sticky='EW')
        self.eventTimeVariable.set(u"time")

        # Event peak
        label= tk.Label(self.infoFrame, text=" Peak:",
                              anchor="w",fg="white",bg="blue")
        label.grid(column=8,row=0,sticky='EW')
        self.eventPeakVariable = tk.StringVar()
        label = tk.Label(self.infoFrame, textvariable=self.eventPeakVariable,
                              anchor="w",fg="black",bg="white")
        label.grid(column=9,row=0,sticky='EW')
        self.eventPeakVariable.set(u"peak")

        # Event sum
        label= tk.Label(self.infoFrame, text=" Sum:",
                              anchor="w",fg="white",bg="blue")
        label.grid(column=10,row=0,sticky='EW')
        self.eventSumVariable = tk.StringVar()
        label = tk.Label(self.infoFrame, textvariable=self.eventSumVariable,
                              anchor="w",fg="black",bg="white")
        label.grid(column=11,row=0,sticky='EW')
        self.eventSumVariable.set(u"sum")

        # Event ToT
        label= tk.Label(self.infoFrame, text=" ToT:",
                              anchor="w",fg="white",bg="blue")
        label.grid(column=12,row=0,sticky='EW')
        self.eventToTVariable = tk.StringVar()
        label = tk.Label(self.infoFrame, textvariable=self.eventToTVariable,
                              anchor="w",fg="black",bg="white")
        label.grid(column=13,row=0,sticky='EW')
        self.eventToTVariable.set(u"tot")

        # Event neutron
        label= tk.Label(self.infoFrame, text=" Neutron:",
                              anchor="w",fg="white",bg="blue")
        label.grid(column=14,row=0,sticky='EW')
        self.eventNeutronVariable = tk.StringVar()
        label = tk.Label(self.infoFrame, textvariable=self.eventNeutronVariable,
                              anchor="w",fg="black",bg="white")
        label.grid(column=15,row=0,sticky='EW')
        self.eventNeutronVariable.set(u"ne")


        # # Add canvas for plots
        self.figure = Figure(figsize=(8,6), dpi=100)
        self.graph1 = self.figure.add_subplot(111)

        xData=np.arange(0.0,3.0,0.01)
        yData=np.sin(2*np.pi*xData)
        self.plotWaveformData(xData ,yData)

        # a tk.DrawingArea
        self.canvas = FigureCanvasTkAgg(self.figure, master=self.frame)
        self.canvas.show()
        self.canvas.get_tk_widget().grid(column=0,row=2,columnspan=2,sticky='NSEW')
        
        # Add toolbar with zoom button
        self.plotToolbarFrame=tk.Frame(self.frame)
        self.plotToolbarFrame.grid(column=1,row=0,sticky='W')
        self.plotToolbar=NavigationToolbar2TkAgg(self.canvas,self.plotToolbarFrame)    
        # Add keyboard shortcuts
        self.canvas.mpl_connect('key_press_event',self.on_key_event)
        
        # # Enable resizing
        # self.master.resizable(True,True) # Window can only resize horizontally
        self.master.grid_columnconfigure(0,weight=1)
        self.master.grid_rowconfigure(0,weight=1) 

        self.frame.grid_columnconfigure(1,weight=1) # resize only the second column (0)
        self.frame.grid_rowconfigure(2,weight=1) # resize only the third row (2)

        # # But prevent constant resizing
        # self.update()
        # self.geometry(self.geometry()) 

        # Keep the text entry focused
        #If this is applied, keyboard shortcuts for zooming cannot be used
        #self.eventNumberEntry.focus_set()
        #self.eventNumberEntry.selection_range(0, tk.END)

    def close_window(self):
        self.master.destroy()

    def on_key_event(self,event):
        print "You pressed key:", event.key
        key_press_handler(event,self.canvas,self.plotToolbar)

    def SetEventEntry(self,event):
        nEvent=self.eventNumberVariable.get()        
        self.load_new_event(nEvent)

    def load_new_event(self,nEvent):

        if nEvent<=0:
            nEvent=0
            self.firstEventButton.config(state='disable')
            self.previousEventButton.config(state='disable')            
            self.nextEventButton.config(state='normal')
            self.lastEventButton.config(state='normal')
        elif nEvent>=(len(self.events)-1):
            nEvent=len(self.events)-1
            self.firstEventButton.config(state='normal')
            self.previousEventButton.config(state='normal')            
            self.nextEventButton.config(state='normal')
            self.lastEventButton.config(state='normal')
        else:
            self.firstEventButton.config(state='normal')
            self.previousEventButton.config(state='normal')            
            self.nextEventButton.config(state='normal')
            self.lastEventButton.config(state='normal')


        event=self.events[nEvent]
        xData=range(len(event.waveform))
        self.plotWaveformDataEvent(xData,event)
        self.canvas.draw()

        print "Plottin the waveform of the event: ", nEvent
        
        self.eventIdVariable.set(nEvent)
        self.eventFpgaVariable.set(event.fpga)
        self.eventChVariable.set(event.channel)
        self.eventTimeVariable.set(event.time)
        self.eventPeakVariable.set(event.peakValue)
        self.eventSumVariable.set(event.sumValue)
        self.eventToTVariable.set(event.totValue)
        self.eventNeutronVariable.set(str(bool(event.neutronFlag)))

        self.eventNumberVariable.set(nEvent)


        # Keep the text entry focused
        #self.eventNumberEntry.focus_set()
        #self.eventNumberEntry.selection_range(0, tk.END)

    def SetEventFirst(self):
        nEvent=0        
        self.load_new_event(nEvent)


    def SetEventPrevious(self):
        nEvent=self.eventNumberVariable.get()-1        
        self.load_new_event(nEvent)

        # # Keep the text entry focused
        # self.eventNumberEntry.focus_set()
        # self.eventNumberEntry.selection_range(0, tk.END)

    def SetEventNext(self):
        nEvent=self.eventNumberVariable.get()+1        
        self.load_new_event(nEvent)

    def SetEventLast(self):
        nEvent=len(self.events)-1        
        self.load_new_event(nEvent)


    def plotWaveformData(self,xData,yData):
        self.graph1.clear()    
        self.graph1.plot(xData,yData)
        # a.set_title('Tk embedding')
        self.graph1.set_xlabel('Sample')
        self.graph1.set_ylabel('Value')
        self.figure.set_tight_layout(True)

    def plotWaveformDataEvent(self,xData,event):
        self.graph1.clear()    
        self.graph1.plot(xData,event.waveform)
        # a.set_title('Tk embedding')
        self.graph1.set_xlabel('Sample')
        self.graph1.set_ylabel('Value')

        #textX=graph.get_xlim()[0]+0.6*(graph.get_xlim()[1]-graph.get_xlim()[0])
        #textY=graph.get_ylim()[0]+0.7*(graph.get_ylim()[1]-graph.get_ylim()[0])
        #text="Channel: "+str(event.channel)+"\n"
        #text+="Peak value: "+str(event.peakValue)+"\n"
        #text+="Sum value: "+str(event.sumValue)+"\n"
        #text+="Is neutron: "+str(bool(event.neutronFlag))
        #graph.text(textX, textY, text)


def main():

    root=tk.Tk()
    eventStorage=None
    app=waveformViewer(root,eventStorage)
    app.master.title('Waveform viewer')
    root.mainloop()


if __name__ == "__main__":
    main()

import numpy as np
import copy
import time
from scipy.signal import argrelextrema
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import pafitter
import pickle
import dataStructures

class Autosetup:
    def __init__(self,eventStorage, daq):
        self.eventStorage=eventStorage
        self.daq=daq

        self.baselineEstimates={}

    def getBaseline(self):
        baselineMeans={}
        baselineMedians={}
        baselineStds={}
        baselineEstimates={}
        
        baselineWfs=self.getRandomTriggerSample(1000,trimV=-10.0)

        # Calculate mean and std of the baseline
        for key in baselineWfs:
            baseline=baselineWfs[key]
            if len(baseline)>150:
                baselineMeans[key]=np.mean(baseline)
                baselineMedians[key]=np.median(baseline)
                baselineStds[key]=np.std(baseline)
                baselineEstimates[key]=np.mean(baseline[np.abs(baseline-baselineMedians[key])<5])

        self.baselineEstimates=baselineEstimates

        return baselineEstimates, baselineStds

    def getRandomTriggerSample(self, nWaveforms, trimV=None, verbose=0):

        # Library for saving the waveforms
        baselineWfs={}        

        # Save original settings
        originalDaqSettings=copy.deepcopy(self.daq.daqSettings)

        # Settings for deterining the baseline
        fanOn=False
        pedestal=8100
        th=4000
        wfPre=120
        wfPost=0
        calcPre=20
        calcPost=20
        totMin=2
        
        # Stop DAQ if running
        if (self.daq.isRunning==True):
            self.daq.stop()
        
        # Write settings
        for fpga in self.daq.daqSettings.fpgas:
            fpga.fanOn=fanOn
            for ch in fpga.chs:
                if trimV!=None:
                    ch.trimV=trimV
                ch.pedestal=pedestal
                ch.th=th
                ch.wfPre=wfPre
                ch.wfPost=wfPost
                ch.calcPre=calcPre
                ch.calcPos=calcPost
                ch.totMin=totMin
                
                #baselineMeans[(fpga.id,ch.id)]=0
                #baselineMedians[(fpga.id,ch.id)]=0
                #baselineStds[(fpga.id,ch.id)]=0

        # Connect DAQ if not connected
        if (self.daq.isConnected==False):
            self.daq.connect()

        # Apply settings
        self.daq.set_channel_parameters()
        
        # Clear event buffer
        self.eventStorage.clear()

        # Start DAQ
        self.daq.start()

        # Shoot calibration pulses
        for i in range(nWaveforms):
            self.daq.shoot_calibration_pulse()
            time.sleep(0.003)

        # Stop data acquisition
        self.daq.stop()

        # Return original settings
        self.daq.set_channel_parameters(originalDaqSettings)

        # Go through events and save to one long wf
        # Discard 20 first events (often weird)
        # Discard 20 last samples of the waveforms (trigger pulse)
        events=self.eventStorage.getEvents()
        counter=1
        if verbose>0:
            print "Number of random sample events:",len(events) 
        for event in events[20:]:
            counter+=1
            # Append if wf already exists
            # Problem! Fpgaid not stored in event
            key=(event.fpga,event.channel)
            if key not in baselineWfs:
                baselineWfs[key]=[]
            baselineWfs[key].append(event.waveform[0:wfPre-20])

        # Convert list of np arrays to 1D np array
        for key in baselineWfs:
            baselineWfs[key]=np.concatenate(baselineWfs[key])
        return baselineWfs


    def voltageScan(self):

        trimVs=[-1.0,0.0,1.0,2.0,3.0]
        #trimVs=[-0.5]
        paData_all=[]
        histograms={}

        for trimV in trimVs:
            print "Voltage trim=",trimV
            baselineWfs=self.getRandomTriggerSample(5000,trimV=trimV)

            for key in baselineWfs:
                #print "Analysing channel:",key
                indices,=argrelextrema(baselineWfs[key], np.greater)
                localMaxima=baselineWfs[key][indices]

                # If baseline has been measured, use it
                if key in self.baselineEstimates:
                    baseline=self.baselineEstimates[key]
                else:
                    baseline=8100
                hist, edges = np.histogram(localMaxima,
                                           bins=200,
                                           range=(baseline,baseline+200))
                # Store histogram to file for further (debugging) use
                histograms[key]=(self.baselineEstimates[key],hist,edges)

                # Find fit range minimum
                #try:
                #    indices,=argrelextrema(hist, np.less)
                #    fitRangeMin=edges[indices[0]]-baseline
                #except ValueError:
                #    fitRangeMin=0 
                    
                # Fit PA peaks
                (peakDistance, smoothX, smoothY, 
                 xdata, baselineData, peakData)=pafitter.fit(edges-baseline,
                                                             hist,
                                                             fitRange=(0, 150),
                                                             verbose=1
                                                         )
                paData_all.append(dataStructures.PaData(fpga=key[0],
                                                        channel=key[1],
                                                        trimV=trimV,
                                                        rawX=edges[:-1]-baseline,
                                                        rawData=hist,
                                                        smoothX=smoothX,
                                                        smoothY=smoothY,
                                                        fitX=xdata,
                                                        fitData=peakData,
                                                        baselineData=baselineData,
                                                        paHeight=peakDistance))


            pickle.dump(histograms,open("PA_V"+str(trimV)+".p","wb"))
        return paData_all


    def expFunction(x,amp,slope):
        return amp*x**-slope


    def calculatePaVoltageSlopes(self,paData_all):
        # Dictionary ((key fpga,channel)) of dictionaries (key trimV, value PA)
        paPerVoltage={}
        for paData in paData_all:
            if (paData.fpga,paData.channel) not in paPerVoltage:
                paPerVoltage[(paData.fpga,paData.channel)]={}
            if paData.paHeight!=None:
                paPerVoltage[(paData.fpga,paData.channel)][paData.trimV]=paData.paHeight

        slopes={}
        v0s={}
        for channel in paPerVoltage:
            print "Channel:",channel
            voltages=[]
            pas=[]
            for voltage in sorted(paPerVoltage[channel]):
                voltages.append(voltage)
                pas.append(paPerVoltage[channel][voltage])
                #print voltages[-1], pas[-1]
            if len(voltages)>=3:
                voltages=np.array(voltages)
                pas=np.array(pas)
                print "Vs:",voltages
                print "PAs:",pas
                fitP=np.polyfit(voltages,pas,1)
                slope=fitP[0]
                pa0=fitP[1]
                v0=-pa0/slope

                relDiff=(pas-(slope*voltages+pa0))/pas
                if np.max(relDiff)<20:
                    slopes[channel]=slope
                    v0s[channel]=v0
                else:
                    print "Channel", channel, ". PA points are not in line"

            else:
                print "Channel", channel, ". Only",len(voltages), "voltage points found (min 3)"

        return slopes, v0s

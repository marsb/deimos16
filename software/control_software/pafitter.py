import numpy as np

from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
from scipy.signal import argrelextrema
import pickle
import matplotlib.mlab as mlab
def fit(edges,hist,fitRange,verbose=1):
    ### Select data range
    xdata=(edges[:-1]+edges[1:])*0.5
    ydata=hist[(xdata>fitRange[0])*(xdata<fitRange[1])]
    xdata=xdata[(xdata>fitRange[0])*(xdata<fitRange[1])]

    dipsX,peaksX,dipsY,peaksY=peakDipFinder(xdata,ydata)
    #print "Dips:", dips
    #print "Peaks:",peaks

    ### Smooth data
    xSmooth,ySmooth=smoother(xdata,ydata,meanRange=5)

    # Redecure data range to start from first local minima
    #dipsX=dipFinder(xSmooth,ySmooth)
    if verbose>1:
        print "Dip locations:", dipsX
    if len(dipsX)==0:
        if verbose>0:
            print "Local minima not found. Cannot set the range for peak finding"
        return None, xSmooth, ySmooth, [], [], []
    ySmooth=ySmooth[xSmooth>=dipsX[0]]
    xSmooth=xSmooth[xSmooth>=dipsX[0]]

    ## Only accept peaks that are within the fitting range
    peaksX=peaksX[peaksX>dipsX[0]]
    peaksY=peaksY[peaksX>dipsX[0]]
    
    ## Find max peak
    maxPeakX=peaksX[np.argmax(peaksY)]
    maxPeakY=peaksY[np.argmax(peaksY)]

    ### Find approximate peak locations
    #peaksX=peakFinder(xSmooth,ySmooth)
    if verbose>1:
        print "Peak initial locatios:", peaksX
    # If no peaks found, don't do anything
    if len(peaksX)<1:
        if verbose>0:
            print "PA peaks not found. Cannot fit"
        return None, xSmooth, ySmooth, [], [], []
        #raise RuntimeError('PA peaks not found. Cannot fit.')


    ### Step 3: Reduce range to cover only two peaks
    if len(dipsX[dipsX<maxPeakX-5])>0:
        rangeMin=np.max(dipsX[dipsX<maxPeakX-5])
    else:
        rangeMin=xSmooth[0]
    if len(dipsX[dipsX>maxPeakX*2+20])>0:
        rangeMax=np.min(dipsX[dipsX>maxPeakX*2+20])
    else:
        rangeMax=xSmooth[-1]
    
    if verbose>1:
        print "Fit range:", rangeMin, rangeMax
    ydata=ydata[(xdata>=rangeMin)*(xdata<=rangeMax)]
    xdata=xdata[(xdata>=rangeMin)*(xdata<=rangeMax)]
    ySmooth=ySmooth[(xSmooth>=rangeMin)*(xSmooth<=rangeMax)]
    xSmooth=xSmooth[(xSmooth>=rangeMin)*(xSmooth<=rangeMax)]

    # Step 3: Fit exponential
    p0=[100,0.1]
    try:
        p, pconv=curve_fit(expFunction, xdata, ydata, p0=p0, sigma=(ydata+2)**0.5)
    except RuntimeError or RuntimeWarning:
        if verbose>0:
            print "Cannot fit exponential baseline. Fit failed"
        return None, xSmooth, ySmooth, [], [], []

    if verbose>1:
        print "Fitted exponential baseline paramters:", p
    #plt.plot(xdata,expFunction(xdata,p[0],p[1]))
        
    # Step 4: fit exponential plus gausians starting from approximate locations
    p0=[p[0],p[1],maxPeakX,10000,10000,8]
    try:
        p, pconv = curve_fit(fullPaFunction, xdata, ydata, p0=p0, sigma=(ydata+2)**0.5)
        if verbose>1:
            print "Fitted full PA shape paramters:", p
    except RuntimeError or RuntimeWarning:
        if verbose>0:
            print "Cannot fit PA function. Fit failed"
        return None, xSmooth, ySmooth, [], [], []

    # Fit fails if peaks are negitive, or second peak is larger
    if (p[4]<0 or p[3]<p[4]):
        if verbose>0:
            print "PA fit failed. Second peak larger than the first one or negative"
        #raise RuntimeError('PA fit failed. Negative peak areas.')
        peakDistance=None
    else:
        peakDistance=p[2]

    baselineData=expFunction(xdata,p[0],p[1])
    peakData=fullPaFunction(xdata,p[0],p[1], p[2], p[3], p[4], p[5])

    # Step 5: Return peakDistance, xdata, baselineData, fittedData
    return peakDistance, xSmooth, ySmooth, xdata, baselineData, peakData


def smoother(x,y,meanRange=5):
    cumsum=np.cumsum(y)
    ySmooth=(cumsum[meanRange:]-cumsum[:-meanRange])/meanRange
    xSmooth=x[meanRange/2:-meanRange/2]
    return xSmooth, ySmooth

def peakFinder(x,y):

    try:
        #indices=localMax(y,3)
        indices=localMax(y,5)
        return x[indices]
    except ValueError:
        print "No peaks found"
        #return np.array([25,50,75])
        return np.array([])

    #xMax=[]
    #previousX=0
    #for i in indices:
    #    if (i>1 and x[i]>previousX+5):
    #        xMax.append([i])
    #        previousX=x[i]
    #return np.array(xMax)

def dipFinder(x,y):
    try:
        indices=localMin(y, 5)
    except ValueError:
        print "No dips found"
        #return np.array([10,40,60,90])
        return np.array([])
    xMin=[]
    previousX=0
    for i in indices:
        if (i>1 and x[i]>previousX+5):
            xMin.append(x[i])
            previousX=x[i]

    return np.array(xMin)

def peakDipFinder(x,y,sumRange=11):
    xS,yS=smoother(x,y,sumRange)

    yDiff=yS[sumRange:-sumRange]-0.5*(yS[sumRange*2:]+yS[:-sumRange*2])
    xDiff=xS[sumRange:-sumRange]
    #print "yDiff:",yDiff
    #print "xDiff:",xDiff
    #dips=dipFinder(xDiff,yDiff)
    #peaks=peakFinder(xDiff,yDiff)
    dipsX=xDiff[localMin(yDiff)]
    peaksX=xDiff[localMax(yDiff)]
    dipsY=yDiff[np.in1d(xDiff,dipsX)]
    peaksY=yDiff[np.in1d(xDiff,peaksX)]
    return dipsX,peaksX,dipsY,peaksY

def expFunction(x,amp,slope):
    return amp*x**-slope

def polynomial2(x,a0,a1,a2):
    return a0+a1*x+a2*x**2

def gaussian(x, amp, loc, sig):
    return amp*mlab.normpdf(x,loc,sig) 
    #return np.exp(-np.power(x - loc, 2.) / (2 * np.power(sig, 2.)))

def fullPaFunction(x,amp,slope, loc1, amp1, amp2, sig):
    f=(expFunction(x,amp,slope)
       +gaussian(x,amp1,loc1,sig)
       +gaussian(x,amp2,2*loc1,sig))
    return f
    

def localMax(a,extend=1):
    accepted=np.array(len(a)*[True])
    for i in range(1,extend+1):
        accepted*=(np.r_[[True]*i, a[i:] > a[:-i]] & np.r_[a[:-i] >= a[i:], [True]*i])
    #indeces =np.where(np.r_[True, a[1:] >= a[:-1]] & np.r_[a[:-1] >= a[1:], True])[0]
    return np.where(accepted)[0]


def localMin(a,extend=1):
    accepted=np.array(len(a)*[True])
    for i in range(1,extend+1):
        accepted*=np.r_[[True]*i, a[i:] < a[:-i]] & np.r_[a[:-i] <= a[i:], [True]*i]
    return np.where(accepted)[0]


def main():
    import paViewer
    import dataStructures
    import Tkinter as tk 
    data=pickle.load(open("PA_V-0.5.p","rb"))
    paData=[]
    for key in data:
        baseline=data[key][0]
        hist=data[key][1]
        edges=data[key][2]

        #plt.figure("Channel="+str(key[1]))
        #plt.plot(edges[:-1]-baseline,hist)
        
        print "Fitting channel:",key
        peakDistance, smoothX, smoothY, xdata, baselineData, peakData=fit(edges-baseline,
                                                                          hist,
                                                                          fitRange=(0,150),
                                                                          verbose=2)
        #plt.plot(xdata,baselineData)
        #plt.plot(xdata,peakData)

        paData.append(dataStructures.PaData(fpga=key[0],channel=key[1],trimV=0.0,
                                                rawX=edges[:-1]-baseline,
                                                rawData=hist,
                                                smoothX=smoothX,
                                                smoothY=smoothY,
                                                fitX=xdata,
                                                fitData=peakData,
                                                baselineData=baselineData,
                                                paHeight=peakDistance))


            

    root=tk.Tk()
    app=paViewer.PaViewer(root,paData)
    app.master.title('PA viewer')
    root.mainloop()

if __name__ == "__main__":
    main()

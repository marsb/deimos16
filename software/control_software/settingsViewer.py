#!/usr/bin/python

import Tkinter as tk

import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure

import numpy as np
import scipy as sc
import copy
# from lmfit import minimize, Parameters, Minizer

import dataStructures

class SettingsViewer:
    def __init__(self,master,daq):

        self.daq=daq
        self.master=master
        self.master.grid()
        self.frame = tk.Frame(self.master)
        self.frame.grid(column=0,row=0,sticky='NSEW')

        ### Parameters frame
        self.parametersFrame=tk.Frame(self.frame)
        self.parametersFrame.grid(column=0,row=0,sticky='W')

        # Title row
        nextColumn=0
        nextRow=0
        parameterTitle = tk.Label(self.parametersFrame, text="FPGA",
                                  anchor="w",fg="white",bg="blue")
        parameterTitle.grid(column=nextColumn,row=nextRow,sticky='EW')
        nextColumn+=1
        for settingName in dataStructures.ChannelSettings.settingNames:
            parameterTitle = tk.Label(self.parametersFrame, text=settingName,
                                      anchor="w",fg="white",bg="blue")
            parameterTitle.grid(column=nextColumn,row=nextRow,sticky='EW')
            nextColumn+=1
        nextRow+=1

        # Values
        nextColumn=0
        for fpga in daq.daqSettings.fpgas:
            for ch in fpga.chs:
                # Fpga Id
                parameterVariable=tk.IntVar()
                parameterVariable.set(fpga.id)
                parameterValue=tk.Entry(self.parametersFrame, 
                                        textvariable=parameterVariable, 
                                        width=7)
                parameterValue.grid(column=nextColumn,row=nextRow,sticky='EW') # Stick East West
                nextColumn+=1
                for settingName in ch.settingNames:
                    exec("value=ch."+settingName)
                    parameterVariable=tk.StringVar()
                    if type(value) == float:
                        parameterVariable.set("{:.2f}".format(value))
                    else:
                        parameterVariable.set(value)
                    parameterValue=tk.Entry(self.parametersFrame, 
                                            textvariable=parameterVariable, 
                                            width=5)
                    parameterValue.grid(column=nextColumn,row=nextRow,sticky='EW') # Stick East West
                    nextColumn+=1
                nextRow+=1
                nextColumn=0

        # Button toolbar
        self.buttonFrame=tk.Frame(self.frame)
        self.buttonFrame.grid(column=0,row=1,sticky='W')
        # Add button set
        #setButton = tk.Button(self.buttonFrame,text=u"Set",
        #                      command=self.SetVoltages)
        #setButton.grid(column=0,row=0)
        # Add button default
        #defaultButton = tk.Button(self.buttonFrame,text=u"Default",
        #                            command=self.Default)
        #defaultButton.grid(column=1,row=0)
        # Add button close
        closeButton = tk.Button(self.buttonFrame,text=u"Close",
                                command=self.closeWindows)
        closeButton.grid(column=0,row=0)

        
# # Enable resizing
        self.master.resizable(True,False) # Window can only resize horizontally
        self.master.grid_columnconfigure(0,weight=1) # resize only the second column (1)
        #self.master.grid_rowconfigure(0,weight=1) # resize only the third row (2)
        
        # self.frame.resizable(True,True) # Window can only resize horizontally
        self.frame.grid_columnconfigure(1,weight=1) # resize only the first column (0)
        # self.frame.grid_rowconfigure(2,weight=1) # resize only the third row (2)

        # # But prevent constant resizing
        # self.update()
        # self.geometry(self.geometry()) 

        # Keep the text entry focused
        # self.eventNumberEntry.focus_set()
        # self.eventNumberEntry.selection_range(0, tk.END)


        #self.ReadVoltages()

    def closeWindows(self):
        self.master.destroy()

#    def SetEventEntry(self,event):
#        nEvent=self.eventNumberVariable.get()
#
#        event=self.events[nEvent]
#        xData=range(len(event.waveform))
#        plotWaveformData(self.graph1,xData,event.waveform)
#        self.canvas.draw()
#
#        print "Plottin the waveform of the event: ", nEvent
#        
#        self.labelVariable.set(nEvent)
#        # Keep the text entry focused
#        self.eventNumberEntry.focus_set()
#        self.eventNumberEntry.selection_range(0, tk.END)


    def SetVoltages(self):
        voltages=[]
        for idChannel, channelNumber in enumerate(self.channelNumbers):
            voltage=self.voltageVariables[idChannel].get()
            voltages.append(voltage)
        self.voltageControl.updateVoltages(uncorrectedVoltages=np.array(voltages),
                                           trimVoltage=self.trimVariable.get())

    def OnCorrectionButtonClick(self):
        if self.correctionButton["text"]=="Disable temp":
            self.voltageControl.temperatureFeedback=False
            self.voltageControl.updateVoltages(voltageTempCorrection=0)
            self.correctionButton["text"]="Enable temp"
        elif self.correctionButton["text"]=="Enable temp":
            self.voltageControl.temperatureFeedback=True
            self.voltageControl.doTempCorrection()
            self.correctionButton["text"]="Disable temp"
        
    def Default(self):
        print "Read default parameters"
        voltages=self.voltageControl.voltageDefaults
        for idChannel, channelNumber in enumerate(self.channelNumbers):
            voltage=voltages[idChannel]
            self.voltageVariables[idChannel].set(voltage)
        self.trimVariable.set(self.voltageControl.trimVoltage)

    def ReadVoltages(self):
        print "Read voltage parameters"
        voltages=self.voltageControl.uncorrectedVoltages
        for idChannel, channelNumber in enumerate(self.channelNumbers):
            voltage=voltages[idChannel]
            self.voltageVariables[idChannel].set(voltage)
        self.trimVariable.set(self.voltageControl.trimVoltage)


        
    def UpdateTemperatures(self):
        tem1=self.miniips.get_temperature1()
        tem2=self.miniips.get_temperature2()
        self.tem1Variable.set("{:2.1f}".format(tem1))
        self.tem2Variable.set("{:2.1f}".format(tem2))

def main():
    root=tk.Tk()
    miniips=''
    app=parametersViewer(root,miniips)
    app.master.title('Parameters')
    root.mainloop()


if __name__ == "__main__":
    main()

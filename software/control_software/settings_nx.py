#!/usr/bin/python
from dataStructures import *

# This file contains the default configuration parameters
# for backpack 1

daqSettings=DaqSettings()
daqSettings.applyTime=0

timeout=100000
port=20481
tempControl=True
firmware="../FPGA/miniips2.bit"
nomTem=25.0
temCoef=0.054
fanOn=True
hv=58.0

#### Neutron trigger
wfPre=100
wfPost=200
calcPre=60
calcPost=100
totMin=40
idPeakMax=4000
idTotMin=80
trimV=1.0 # Above nominal
#th=25
th=20

#### Muon trigger
#totMin=1
#th=1000

### FPGA 0 Deimos 005 (Top)
fpga=FpgaSettings()
fpga.id=0
fpga.ip=[192,168,1,100]
fpga.timeout=timeout
fpga.port=port
fpga.tempControl=tempControl
fpga.firmware=firmware
fpga.nomTem=nomTem
fpga.temCoef=temCoef
fpga.fanOn=fanOn
fpga.hv=hv
fpga.maxHv=79.2
fpga.minHv=29.5
daqSettings.fpgas.append(fpga)
sipmIds  = [ 63681, 63761, 63754,  63778,  
             63764, 63760, 63774,  63772,
             63768, 63683, 63780, 63685,
             63762, 63771, 63777, 63773]
nomV =     [54.59, 54.72, 55.15, 54.94, 
            54.71, 54.81, 54.33, 54.41,
            54.88, 54.36, 55.18, 54.65, 
            54.65, 54.41, 54.82, 54.33]
pedestal = [8157, 8155, 8139, 8154,
            8146, 8145, 8168, 8179,
            8144, 8146, 8154, 8172,
            8149, 8149, 8140, 8154]

chTypes = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
xs = [-1, -1, -1, -1,
      -1, -1, -1, -1,
      -1, -1, -1, -1,
      -1, -1, -1, -1]
ys = [3, 2, 1, 0,
      3, 2, 1, 0,
      3, 2, 1, 0,
      3, 2, 1, 0]
zs = [0, 0, 0, 0,
      1, 1, 1, 1,
      2, 2, 2, 2,
      3, 3, 3, 3]

for index in range(16):
    ch=ChannelSettings()
    ch.id=index
    ch.sipmId=sipmIds[index]
    ch.type=chTypes[index]
    ch.pedestal=pedestal[index]
    ch.th=th
    ch.wfPre=wfPre
    ch.wfPost=wfPost
    ch.calcPre=calcPre
    ch.calcPost=calcPost
    ch.totMin=totMin
    ch.idAreaMin=0 # This is not in use
    ch.idPeakMax=idPeakMax
    ch.idTotMin=idTotMin
    
    ch.nomV=nomV[index]
    ch.trimV=trimV
    ch.temV=0
    ch.setV=0

    ch.x=xs[index]
    ch.y=ys[index]
    ch.z=zs[index]

    daqSettings.fpgas[-1].chs.append(ch)

### Deimos 001 (Bottom)
fpga=FpgaSettings()
fpga.id=1
fpga.ip=[192,168,1,101]
fpga.timeout=timeout
fpga.port=port
fpga.tempControl=tempControl
fpga.firmware=firmware
fpga.nomTem=nomTem
fpga.temCoef=temCoef
fpga.fanOn=fanOn
fpga.hv=hv
fpga.maxHv=78.8
fpga.minHv=29.3
daqSettings.fpgas.append(fpga)
sipmIds  = [ 63690, 63687, 63684,  63758,  
             63767, 63755, 63688,  63711,
             63770, 63781, 63769, 63763,
             63782, 63689, 63766, 63682]
nomV =     [55.24, 54.89, 54.46, 55.07, 
            54.83, 55.09, 54.97, 54.51,
            54.52, 55.53, 54.60, 54.68, 
            55.64, 55.06, 54.62, 54.46]
pedestal = [8144, 8165, 8176, 8138, 
            8159, 8138, 8146, 8160, 
            8173, 8120, 8151, 8150, 
            8164, 8170, 8156, 8144]
chTypes=[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
xs = [0, 1, 2, 3,
      0, 1, 2, 3,
      0, 1, 2, 3,
      0, 1, 2, 3]
ys = [-1, -1, -1, -1,
      -1, -1, -1, -1,
      -1, -1, -1, -1,
      -1, -1, -1, -1]
zs = [3, 3, 3, 3,
      2, 2, 2, 2,
      1, 1, 1, 1,
      0, 0, 0, 0]

for index in range(16):
    ch=ChannelSettings()
    ch.id=index
    ch.sipmId=sipmIds[index]
    ch.type=chTypes[index]
    ch.pedestal=pedestal[index]
    ch.th=th
    ch.wfPre=wfPre
    ch.wfPost=wfPost
    ch.calcPre=calcPre
    ch.calcPost=calcPost
    ch.totMin=totMin
    ch.idAreaMin=0 # This is not in use
    ch.idPeakMax=idPeakMax
    ch.idTotMin=idTotMin
    
    ch.nomV=nomV[index]
    ch.trimV=trimV
    ch.temV=0
    ch.setV=0

    ch.x=xs[index]
    ch.y=ys[index]
    ch.z=zs[index]
            
    daqSettings.fpgas[-1].chs.append(ch)







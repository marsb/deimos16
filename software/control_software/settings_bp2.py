#!/usr/bin/python
from dataStructures import *

# This file contains the default configuration parameters
# for backpack 2

daqSettings=DaqSettings()
daqSettings.applyTime=0

### FPGA 0
fpga=FpgaSettings()
fpga.id=0
fpga.ip=[192,168,1,100]
fpga.timeout=100000
fpga.port=20481
fpga.tempControl=True
fpga.firmware="../FPGA/miniips2.bit"
fpga.nomTem=25.0
fpga.temCoef=0.06
fpga.fanOn=True
fpga.hv=68.0
fpga.maxHv=79.2
fpga.minHv=29.5


daqSettings.fpgas.append(fpga)

### Channel settings
sipmIds  = [ 1150, 1152, 1153,  1154,
             1155,  1156,  1157,  1158,
             1159, 1160, 0,  0,
             0,  0,  0,  0]
nomV = [66.80,  66.80, 66.75,  66.85,
        66.90,  66.90, 66.60,  66.60,
        66.60,  66.60,   67.00, 67.00,
        67.00, 67.00, 67.00, 67.00]
pedestal = [8144, 8166, 8174, 8137,
            8159, 8138, 8143, 8160,
            8172, 8120, 8147, 8151,
            8160, 8171, 8153, 8142]

# ChType 0: Not in use, 1: Neutron, 2: Gamma
chTypes=[1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0]

xs = [0, 1, 2, 3,
      4, 5, 6, 7,
      8, 9, -1, -1,
      -1, -1, -1, -1]
ys = [0, 0, 0, 0,
      0, 0, 0, 0,
      0, 0, 0, 0,
      0, 0, 0, 0]
zs = [0, 0, 0, 0,
      0, 0, 0, 0,
      0, 0, 0, 0,
      0, 0, 0, 0]


for index in range(16):
    ch=ChannelSettings()
    ch.id=index
    ch.sipmId=sipmIds[index]
    ch.type=chTypes[index]

    ch.pedestal=pedestal[index]
    ch.th=15
    ch.wfPre=50
    ch.wfPost=150
    ch.calcPre=50
    ch.calcPost=150
    ch.totMin=35
    ch.idAreaMin=0 # This is not in use
    ch.idPeakMax=300
    ch.idTotMin=40

    ch.nomV=nomV[index]
    ch.trimV=-1.25
    ch.temV=0
    ch.setV=0

    ch.x=xs[index]
    ch.y=ys[index]
    ch.z=zs[index]

    daqSettings.fpgas[0].chs.append(ch)




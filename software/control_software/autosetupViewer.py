#!/usr/bin/python

import Tkinter as tk
from autosetup import Autosetup
import dataStructures
from paViewer import PaViewer
# from lmfit import minimize, Parameters, Minizer

class AutosetupViewer(tk.Tk):
    def __init__(self,master,eventStorage,daq):
        """ TK frame for setting parameters automatically """

        self.autosetup=Autosetup(eventStorage,daq)
        self.daq=daq
        self.master=master
        self.master.grid()
        self.frame = tk.Frame(self.master)
        self.frame.grid(column=0,row=0,sticky='NSEW')

        self.baselineVariables={}
        self.baselineStdVariables={}

        self.v0Variables={}
        self.vSlopeVariables={}

        ### Parameters frame
        self.parametersFrame=tk.Frame(self.frame)
        self.parametersFrame.grid(column=0,row=0,sticky='W')

        # Title row
        nextColumn=0
        nextRow=0
        parameterNames=["FPGA", "Ch", "baseline", "std", "V0", "VSlope"]
        for parameterName in parameterNames:
            parameterTitle = tk.Label(self.parametersFrame, text=parameterName,
                                      anchor="w",fg="white",bg="blue",
                                      bd=2, relief=tk.GROOVE)
            parameterTitle.grid(column=nextColumn,row=nextRow,sticky='EW')
            nextColumn+=1
        nextRow+=1

        # Values
        nextColumn=0
        for fpga in daq.daqSettings.fpgas:
            for ch in fpga.chs:

                # Fpga Id
                parameterValue = tk.Label(self.parametersFrame, text=fpga.id,
                                          anchor="w",fg="black",bg="white",
                                          bd=2, relief=tk.GROOVE)
                parameterValue.grid(column=nextColumn,row=nextRow,sticky='EW')
                nextColumn+=1

                # Ch Id
                # print "Ch:",ch.id
                parameterValue = tk.Label(self.parametersFrame, text=ch.id,
                                          anchor="w",fg="black",bg="white",
                                          bd=2, relief=tk.GROOVE)
                parameterValue.grid(column=nextColumn,row=nextRow,sticky='EW')
                nextColumn+=1

                # Rest
                for id in range(2,len(parameterNames)):
                    parameterVariable=tk.StringVar()
                    parameterVariable.set("-")
                    parameterValue=tk.Entry(self.parametersFrame, 
                                            textvariable=parameterVariable, 
                                            width=10)
                    parameterValue.grid(column=nextColumn,row=nextRow,sticky='EW') # Stick East West
                    nextColumn+=1

                    if (parameterNames[id]=="baseline"):
                        self.baselineVariables[(fpga.id,ch.id)]=parameterVariable

                    if (parameterNames[id]=="std"):
                        self.baselineStdVariables[(fpga.id,ch.id)]=parameterVariable

                    if (parameterNames[id]=="V0"):
                        self.v0Variables[(fpga.id,ch.id)]=parameterVariable

                    if (parameterNames[id]=="VSlope"):
                        self.vSlopeVariables[(fpga.id,ch.id)]=parameterVariable
                        

                nextRow+=1
                nextColumn=0

        # Button toolbar
        self.buttonFrame=tk.Frame(self.frame)
        self.buttonFrame.grid(column=0,row=1,sticky='W')
        # Add button set
        #setButton = tk.Button(self.buttonFrame,text=u"Set",
        #                      command=self.SetVoltages)
        #setButton.grid(column=0,row=0)
        # Add button default
        #defaultButton = tk.Button(self.buttonFrame,text=u"Default",
        #                            command=self.Default)
        #defaultButton.grid(column=1,row=0)
        # Get baseline
        getBaselineButton = tk.Button(self.buttonFrame,text=u"Get baseline",
                                      command=self.getBaseline)
        getBaselineButton.grid(column=0,row=0)

        # Apply baseline
        applyBaselineButton = tk.Button(self.buttonFrame,text=u"Apply baseline",
                                   command=self.applyBaseline)
        applyBaselineButton.grid(column=1,row=0)

        # Get PA
        getPAButton = tk.Button(self.buttonFrame,text=u"Get PA",
                                      command=self.getPAHeight)
        getPAButton.grid(column=2,row=0)

        # Add button close
        closeButton = tk.Button(self.buttonFrame,text=u"Close",
                                command=self.closeWindows)
        closeButton.grid(column=3,row=0)

        
        ## Enable resizing
        self.master.resizable(False,False) # Window can only resize horizontally
        #self.master.grid_columnconfigure(0,weight=1) # resize only the second column (1)
        #self.master.grid_rowconfigure(0,weight=1) # resize only the third row (2)
        
        # self.frame.resizable(True,True) # Window can only resize horizontally
        # self.frame.grid_columnconfigure(1,weight=1) # resize only the first column (0)
        # self.frame.grid_rowconfigure(2,weight=1) # resize only the third row (2)

        # # But prevent constant resizing
        # self.update()
        # self.geometry(self.geometry()) 

        # Keep the text entry focused
        # self.eventNumberEntry.focus_set()
        # self.eventNumberEntry.selection_range(0, tk.END)


        #self.ReadVoltages()

    def closeWindows(self):
        self.master.destroy()


    def getBaseline(self):
        (baselineMedians, baselineStds)=self.autosetup.getBaseline()
        for key in baselineMedians:
            self.baselineVariables[key].set("{:.1f}".format(baselineMedians[key]))
        for key in baselineStds:
            self.baselineStdVariables[key].set("{:.2f}".format(baselineStds[key]))


    def getPAHeight(self):
        paData=self.autosetup.voltageScan()
        slopes, v0s=self.autosetup.calculatePaVoltageSlopes(paData)

        for key in slopes:
            self.vSlopeVariables[key].set("{:.1f}".format(slopes[key]))
        for key in v0s:
            self.v0Variables[key].set("{:.2f}".format(v0s[key]))


        paWindow = tk.Toplevel(self.master)
        app=PaViewer(paWindow,paData)
        app.master.title('PA viewer')

                        

    def applyBaseline(self):
        for fpga in self.daq.daqSettings.fpgas:
            for ch in fpga.chs:
                if (self.baselineVariables[(fpga.id,ch.id)].get()>100.0):
                    ch.pedestal=int(round(float(self.baselineVariables[(fpga.id,ch.id)].get())))
        self.daq.set_channel_parameters()


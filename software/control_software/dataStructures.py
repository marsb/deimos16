import collections

Event=collections.namedtuple('Event', 
                             '''status, fpga, channel, storeTime, time, 
                             totValue, peakValue, sumValue, neutronFlag,  
                             nPreSamples, nPostSamples, waveform, pedestal''')

Trigger=collections.namedtuple('Trigger', 
                               '''level, calcSamplesPre, calcSamplesPost, 
                               netSamplesPre,netSamplesPost, peakThreshold,  
                               areaThreshold, totTrigThreshold, totDetectThreshold, 
                               enabled''')

PaData=collections.namedtuple('PaData',
                              '''fpga, channel, trimV, rawX, 
                              rawData, smoothX, smoothY, fitX, 
                              fitData, baselineData, paHeight''')

class ChannelSettings:
    settingNames=["id", "sipmId", "type", "pedestal", "th",
                  "wfPre", "wfPost", "calcPre", "calcPost",
                  "totMin", "idAreaMin", "idPeakMax", "idTotMin",
                  "nomV", "trimV", "temV", "setV",
                  "x", "y", "z"]
    
    def __init__(self):
        self.id = None        
        self.sipmId=None
        self.type=None # 0 = not in use, 1 = neutron, 2 = gamma

        self.pedestal=None
        self.th = None
        self.wfPre = None
        self.wfPost = None
        self.calcPre = None
        self.calcPost = None
        self.totMin = None
        self.idAreaMin = None
        self.idPeakMax = None
        self.idTotMin = None

        self.nomV = None
        self.trimV = None
        self.temV=None
        self.setV= None

        self.x = None
        self.y = None
        self.z = None
                        


    def get_trigger(self):
        return Trigger(level = self.pedestal+self.th,
                       calcSamplesPre = self.calcPre,
                       calcSamplesPost = self.calcPost, 
                       netSamplesPre = self.wfPre,
                       netSamplesPost = self.wfPost, 
                       peakThreshold = self.idPeakMax+self.pedestal, 
                       areaThreshold = self.idAreaMin, 
                       totTrigThreshold = self.totMin, 
                       totDetectThreshold = self.idTotMin, 
                       enabled=0)

class FpgaSettings:
    settingNames=["id","ip", "port", "timeout", "chs", "tempControl",
                  "firmware", "nomTem", "temCoef", "sipmTem","fanOn",
                  "hv","maxHv","minHv"]

    def __init__(self):
        self.id=None
        self.ip=[]
        self.port=None
        self.timeout=None
        self.chs=[]
        self.tempControl=None
        self.firmware=None
        self.nomTem=None
        self.temCoef=None
        self.sipmTem=None
        self.fanOn=None
        self.hv=None
        self.maxHv=None
        self.minHv=None


class DaqSettings:
    def __init__(self):
        self.fpgas=[]
        self.applyTime=None

#!/usr/bin/python

import Tkinter as tk

import matplotlib
import threading
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
import numpy as np
import scipy as sc
import time

# from lmfit import minimize, Parameters, Minizer

class RateViewer:
    def __init__(self,master,eventStorage,daqSettings):
        """ TK frame for viewing event rates online """

        self.eventStorage=eventStorage
        self.events=eventStorage.getEvents()

        # Notice, event does not contain the fpgaId. Thus, the right FPGA cannot be
        # identified
        self.neutronChannels=[]
        self.gammaChannels=[]
        self.allChannels=[]
        for fpga in daqSettings.fpgas:
            chnro=0
            for ch in fpga.chs:
                self.allChannels.append(chnro)
                if ch.type==1:
                    self.neutronChannels.append(chnro)
                if ch.type==2:
                    self.gammaChannels.append(chnro)
                chnro+=1
                    
        self.master=master
        self.master.grid()
        # run close window when exit
        self.master.protocol("WM_DELETE_WINDOW", self.close_window)
        self.frame = tk.Frame(self.master)
        self.frame.grid(column=0,row=0,sticky='NSEW')

        # Create new total rate plotter
        self.totalRatePlotter=RatePlotter('Total rate')
        # Add plotter to TK drawing area
        canvas = FigureCanvasTkAgg(self.totalRatePlotter.figure, master=self.frame)
        canvas.show()
        canvas.get_tk_widget().grid(column=0,row=0,sticky='NSEW')
        # #toolbar = NavigationToolbar2TkAgg( canvas, root )
        # #toolbar.update()
        # Create ratate calculator
        self.totalRateCalculator=RateCalculator(self.events,
                                                self.totalRatePlotter,
                                                1000000,
                                                self.allChannels,
                                                "any")
        self.totalRateCalculator.start() # Start thread
        # Create rate toolbar
        self.toolbarFrame=tk.Frame(self.frame)
        self.toolbarFrame.grid(column=0,row=1,sticky='EW')
        self.totalRateToolbar=RateToolbar(self.totalRateCalculator,self.toolbarFrame,
                                          self.allChannels,"any")

        # Create new gamma rate plotter
        self.gammaRatePlotter=RatePlotter('Gamma rate')
        # Add plotter to TK drawing area
        canvas = FigureCanvasTkAgg(self.gammaRatePlotter.figure, master=self.frame)
        canvas.show()
        canvas.get_tk_widget().grid(column=0,row=2,sticky='NSEW')
        # #toolbar = NavigationToolbar2TkAgg( canvas, root )
        # #toolbar.update()
        # Create ratate calculator
        self.gammaRateCalculator=RateCalculator(self.events,
                                                self.gammaRatePlotter,
                                                1000000,
                                                self.gammaChannels, 
                                                "gamma")
        self.gammaRateCalculator.start() # Start thread
        # Create rate toolbar
        self.gammaToolbarFrame=tk.Frame(self.frame)
        self.gammaToolbarFrame.grid(column=0,row=3,sticky='EW')
        self.gammaRateToolbar=RateToolbar(self.gammaRateCalculator,
                                          self.gammaToolbarFrame,
                                          self.gammaChannels,
                                          "gamma")



        # Create new neutronrate plotter
        self.neutronRatePlotter=RatePlotter('Neutron rate')
        # Add plotter to TK drawing area
        canvas = FigureCanvasTkAgg(self.neutronRatePlotter.figure, master=self.frame)
        canvas.show()
        canvas.get_tk_widget().grid(column=0,row=4,sticky='NSEW')
        # #toolbar = NavigationToolbar2TkAgg( canvas, root )
        # #toolbar.update()
        # Create ratate calculator
        self.neutronRateCalculator=RateCalculator(self.events,
                                                  self.neutronRatePlotter,
                                                  1000000,
                                                  self.neutronChannels, 
                                                  "neutron")
        self.neutronRateCalculator.start() # Start thread
        # Create rate toolbar
        self.neutronToolbarFrame=tk.Frame(self.frame)
        self.neutronToolbarFrame.grid(column=0,row=5,sticky='EW')
        self.neutronRateToolbar=RateToolbar(self.neutronRateCalculator,
                                            self.neutronToolbarFrame,
                                            self.neutronChannels,
                                            "neutron")


        
        # # Enable resizing
        self.master.resizable(True,True) # Window can only resize horizontally
        self.master.grid_columnconfigure(0,weight=1) # resize only the first column (0)
        self.master.grid_rowconfigure(0,weight=1) # resize only the third row (1)

        self.frame.grid_columnconfigure(0,weight=1) # resize only the first column (0)
        self.frame.grid_rowconfigure(0,weight=1) # resize only the first, third and fifth row
        self.frame.grid_rowconfigure(2,weight=1)
        self.frame.grid_rowconfigure(4,weight=1)

        # # But prevent constant resizing
        # self.update()
        # self.geometry(self.geometry()) 

        # Keep the text entry focused
        # self.eventNumberEntry.focus_set()
        # self.eventNumberEntry.selection_range(0, tk.END)

    def close_window(self):
        # print "Rate viewer is closing"
        self.totalRateCalculator.stop()
        self.neutronRateCalculator.stop()
        del self.neutronRateToolbar
        del self.totalRateToolbar
        # del self.rateCalculator
        # del self.totalRatePlotter
        self.master.destroy()
        
    def set_time_bin(self,event):
        timeBin=self.timeBinVariable.get()        
        # self.load_new_event(nEvent)
        
        self.rateCalculator.stop()
        # self.rateCalculator=RateCalculator(self.events,self.totalRatePlotter,timeBin)
        self.rateCalculator.start() # Start thread

        # Keep the text entry focused
        self.timeBinEntry.focus_set()
        self.timeBinEntry.selection_range(0, tk.END)

class RateCalculator(threading.Thread):
    def __init__(self, events,ratePlotter,timeBin,acceptedChannels,eventType):
        """Calculates the number of events with the time bin and plots 
        the rate with rate plotter"""

        threading.Thread.__init__(self)
        self.daemon=True
        self.signal=True
        self.events=events
        self.ratePlotter=ratePlotter
        self.timeBin=timeBin
        self.acceptedChannels=acceptedChannels
        self.maxNBins=200        

        if eventType=="neutron":
            self.neutronFlags=[1]
        if eventType=="gamma":
            self.neutronFlags=[0]
        if eventType=="any":
            self.neutronFlags=[0,1]
        
    def run(self):
        sleepTime=2.0
        oldTime=0.0
        rateValues=[]
        startTime=[]
        endTime=[]

        nEventsProcessed=0

        while self.signal:
            # print "I am still running!"
            oldTime=time.time()
            newEvents=False
            for event in self.events[nEventsProcessed:]:

                timeStamp=event.time
                
                # Initialise if this was the first event
                if len(rateValues)==0:
                    startTime.append(timeStamp)
                    endTime.append(timeStamp+self.timeBin)
                    rateValues.append(0)
                        
                # Add new slot for rate collection
                while timeStamp>endTime[-1]:
                    endTime.append(endTime[-1]+self.timeBin)
                    startTime.append(endTime[-2])
                    rateValues.append(0)

                # Add number of event processed
                nEventsProcessed+=1
                newEvents=True

                # Only add to histogram if channel and neutron tag are correct
                if ((event.neutronFlag in self.neutronFlags) 
                    and (event.channel in self.acceptedChannels)):
                    rateValues[-1]+=1
            
            # After processing all events, limit number of bins
            # and update plot
            if newEvents:
                if (len(rateValues)>self.maxNBins):
                    rateValues=rateValues[-self.maxNBins:]
                    endTime=endTime[-self.maxNBins:]
                    startTime=startTime[-self.maxNBins:]
                    
                self.ratePlotter.update(endTime,rateValues)

            # Wait some time
            time.sleep(max(0.1,sleepTime-(time.time()-oldTime)))

    def stop(self):
        self.signal=False
        # time.sleep(2)
        # del self.ratePlotter
        # self.exit()


class RatePlotter():
    def __init__(self,title):
        """ Plots the rate values """

        self.figure = Figure(figsize=(5,3), dpi=100)
        self.ax = self.figure.add_subplot(111)
        self.ax.clear()    
        self.lines,=self.ax.plot([],[],drawstyle="steps")
        self.ax.set_title(title)
        self.ax.set_xlabel('Time')
        self.ax.set_ylabel('Rate')
        self.ax.set_xlim([0,1000000])
        self.ax.set_ylim([0,10])
        #self.ax.grid()

        self.minTimeInterval=100000000
        self.maxNSamples=50

        self.figure.set_tight_layout(True)

    def update(self, xdata, ydata):
        #print "Rate plot on"
        nSamples=min(self.maxNSamples,len(xdata))
        self.lines.set_xdata(xdata[-nSamples:])
        self.lines.set_ydata(ydata[-nSamples:])
        self.ax.set_xlim([xdata[-nSamples],
                          xdata[-1]])
        self.ax.set_ylim([0,max(max(ydata[-nSamples:]),1)])
        self.figure.canvas.draw()
        self.figure.canvas.flush_events()
        # self.figure.set_tight_layout(True)

class RateToolbar():
    """ TK toolbar for editing the parameters of the rate calculator """ 
    def __init__(self,rateCalculator,master,acceptedChannels,eventType):
        self.rateCalculator=rateCalculator
        self.events=self.rateCalculator.events
        self.ratePlotter=self.rateCalculator.ratePlotter
        self.toolbarFrame=master
        self.acceptedChannels=acceptedChannels
        self.eventType=eventType

        # Add time bin text
        timeBinLabel= tk.Label(self.toolbarFrame, text="Time bin:",
                              anchor="w",fg="black",bg="white")
        timeBinLabel.grid(column=0,row=0,sticky='W')

        # Add time interval entry
        self.timeBinVariable = tk.IntVar()
        self.timeBinEntry = tk.Entry(self.toolbarFrame, 
                                         textvariable=self.timeBinVariable,
                                         width=10) # Text entry
        self.timeBinEntry.grid(column=1,row=0,sticky='W') # Stick West 
        self.timeBinEntry.bind("<Return>", self.set_time_bin)
        self.timeBinVariable.set(u"100000000")
        self.set_time_bin(None)

            
    def set_time_bin(self,event):
        timeBin=self.timeBinVariable.get()        
        # self.load_new_event(nEvent)
        
        self.rateCalculator.stop()
        self.rateCalculator=RateCalculator(self.events,self.ratePlotter,
                                           timeBin,self.acceptedChannels,self.eventType)
        self.rateCalculator.start() # Start thread

        # Keep the text entry focused
        self.timeBinEntry.focus_set()
        self.timeBinEntry.selection_range(0, tk.END)
    
    def __del__(self):
        self.rateCalculator.stop()

def main():
    from eventStorage import EventStorage
    from settings import daqSettings
    from Queue import Queue

    eventQueue=Queue()
    eventStorage=EventStorage(eventQueue)
    root=tk.Tk()
    app=RateViewer(root,eventStorage,daqSettings)
    app.master.title('Rate viewer')
    root.mainloop()


if __name__ == "__main__":
    main()

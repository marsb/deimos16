#!/usr/bin/python
from miniipsLib import MiniipsLib
from miniipsLib import ZESTET1_CARD_INFO
#import rawDataBuffer
import time
import copy
from ctypes import *
from dataStructures import *



class FpgaControl:
    def __init__(self):
        print "Creating new FPGA control object"
        
        self.mi=MiniipsLib()
        
        self.errorFunction = None
        self.errorHandle = None
        self.cardInfo=None
        self.ctrlConn = None
        self.dataConn = None
        self.isConnected=False
        

    def connect(self,settings):
        self.errorFunction=self.mi.example_error_function
        self.errorHandle=self.mi.initialise_error_handler(self.errorFunction)
        self.mi.initialise_library()

        # Change FPGA id
        self.fpgaId=settings.id
        self.mi.set_fpga_id(self.fpgaId)
        
        # Read IP from settings
        ipaddr=(c_ubyte * 4)()
        ipaddr[0]=settings.ip[0]
        ipaddr[1]=settings.ip[1]
        ipaddr[2]=settings.ip[2]
        ipaddr[3]=settings.ip[3]
        self.cardInfo=ZESTET1_CARD_INFO(IPAddr=ipaddr,
                                        ControlPort=settings.port,
                                        Timeout=settings.timeout)


        print "   Getting card info"
        print "   IPAddr:",self.cardInfo.IPAddr[0],self.cardInfo.IPAddr[1],
        print self.cardInfo.IPAddr[2],self.cardInfo.IPAddr[3]
        print "   ControlPort:", self.cardInfo.ControlPort
        print "   Timeout:", self.cardInfo.Timeout
        self.mi.get_card_info(self.cardInfo)

        print "   Opening connection"
        self.mi.configure_from_file(self.cardInfo,settings.firmware)

        #print "Programming flash from file"
        #self.mi.program_flash_from_file(self.cardInfo,settings.firmware)    
        time.sleep(1.0);

        self.ctrlConn=self.mi.open_control_connection(self.cardInfo)
        self.dataConn=self.mi.open_data_connection(self.cardInfo)
    
        firmwareVersion=self.mi.get_firmware_version(self.ctrlConn)
        print "   Firmware version :", firmwareVersion
        time.sleep(0.1)

        print "   Initialising DAC"
        self.mi.dac_initialise(self.ctrlConn,
                               hv=settings.hv,
                               max_hv=settings.maxHv,
                               min_hv=settings.minHv)
        time.sleep(0.1)

        print "   Initialising TEM1"
        self.mi.tem_initialise(self.ctrlConn,1)
        time.sleep(0.1)

        print "   Initialising TEM2"
        self.mi.tem_initialise(self.ctrlConn,2)
        time.sleep(0.1)

        print "   Initialising ADC"
        self.mi.adc_initialise(self.ctrlConn)
        time.sleep(0.1)

        print "   Setting channel HV and trigger parameters"
        self.set_channel_parameters(settings)

        self.isConnected=True
        print "   Card successfully connected and initialised"

    def set_channel_parameters(self,fpgaSettings,setVoltages=True,setTriggers=True):
        self.set_fan(fpgaSettings)
        print "Changing SiPM voltages for FPGA", self.fpgaId
        chs_below_minimum=[]
        min_voltage=0
        chs_above_maximum=[]
        max_voltage=0
        for chSettings in fpgaSettings.chs:
            ch=chSettings.id
            if (setVoltages == True):
                voltage=chSettings.nomV+chSettings.trimV+chSettings.temV
                chSettings.setV=self.set_channel_voltage(ch,voltage)
                if (voltage < chSettings.setV-0.01):
                    chs_below_minimum.append(ch)
                    min_voltage=chSettings.setV
                if (voltage > chSettings.setV+0.01):
                    chs_above_maximum.append(ch)
                    max_voltage=chSettings.setV
            if (setTriggers == True):
                triggerParameters=chSettings.get_trigger()
                self.set_trigger_parameters(ch,triggerParameters)

        # Warn if channel votage was out of range
        if len(chs_below_minimum)>0:
            print "   Voltage set below minimum on channels:"
            print "  ",chs_below_minimum
            print "   (using mimimum voltage {:.2f} V)".format(min_voltage)
        if len(chs_above_maximum)>0:
            print "   Voltage set above maximum on channels" 
            print "  ",chs_above_maximum
            print "   (using maximum voltage {:.2f} V)".format(max_voltage)
        

    def set_channel_voltage(self,channelNumber,voltage):
        setVoltage=self.mi.set_channel_voltage(self.ctrlConn,channelNumber,voltage)
        return setVoltage


    def set_trigger_parameters(self, ch, triggerParameters,verbose=0):
        if verbose>0:
            print "Setting trigger parameters for channel ", ch
            print triggerParameters
        #self.triggerParametersDict[index]=triggerParameters
        self.mi.set_trigger_parameters(self.ctrlConn, ch, triggerParameters)

    def enable_trigger(self, ch):
        #  self.mi.set_test_data(self.ctrlConn)
        #print "Enabling trigger for channel ", ch
        #self.triggerParametersDict[index].enabled=1
        self.mi.enable_trigger(self.ctrlConn, ch)

    def disable_trigger(self, ch):
        #  self.mi.set_test_data(self.ctrlConn)
        #print "Disabling trigger for channel ", ch
        # self.triggerParametersDict[index].enable=1
        self.mi.disable_trigger(self.ctrlConn, ch)

    def set_fan(self,fpgaSettings):
        if fpgaSettings.fanOn==False:
            self.mi.set_gpios_default_fan_off(self.ctrlConn)
        else:
            self.mi.set_gpios_default_fan_on(self.ctrlConn)


    def reset_timer(self):
        print "Resetting timer"
        self.mi.reset_timer(self.ctrlConn)

    def shoot_calibration_pulse(self):
        self.mi.set_gpios_trigger(self.ctrlConn)
        time.sleep(0.0001)
        self.mi.set_gpios_default(self.ctrlConn)

    def get_data(self):
        (status, event)=self.mi.get_data(self.dataConn)
        return status, event

    def get_temperature1(self):
        temperature=self.mi.tem_get_temperature(self.ctrlConn,1)
        return temperature

    def get_temperature2(self):
        temperature=self.mi.tem_get_temperature(self.ctrlConn,2)
        return temperature


    def disconnect(self):
        print "Closing ctrl connnection"
        self.mi.close_connection(self.ctrlConn)
        print "Closing data connection"
        self.mi.close_connection(self.dataConn)
        ### This is only needed if carInfos would be created by find_cards
        # print "Free card"
        # self.mi.free_card(self.cardInfos)
        print "Closing library"
        self.mi.close_library()
        
        print "Card successfully disconnected"
        
        self.isConnected=False

def main():
    from defaults import daqSettings
    fpgaControl=FpgaControl()
    fpgaControl.connect(daqSettings.fpgas[0])
    fpgaControl.disconnect()
    
if __name__ == "__main__":
    main()

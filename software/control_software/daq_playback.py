#!/usr/bin/python
from __future__ import print_function
import fpgaControl
from dataStructures import *
import threading
import Queue
import ROOT
import numpy as np
import time
import sys

class Daq():
    def __init__(self, daqSettings,filename):
        """ Initialise the DAQ by giving DaqSettings and Queue objects"""
        self.daqSettings=daqSettings
        self.eventQueue=Queue.Queue()
        self.dataCollector=None

        # Status
        self.isRunning=False
        self.isConnected=False

        self.tempCorrection=True
        
        self.dataCollector=DataCollector(filename,self.eventQueue)
        
    def connect(self):
        self.isConnected=True
        # self.dataAcquisition.start()
        


    def disconnect(self):
        self.stop()
        self.isConnected=False
        print("DAQ disconnected")

    def start(self):
        # self.eventStorage.clear()
        
        self.dataCollector.start()
        self.isRunning=True
        
    def stop(self):
        # Disbale triggers
        self.dataCollector.stop()
        self.isRunning=False

    def shoot_calibration_pulse(self):
        print("Shoot call pulse (does nothing)")

    def set_channel_parameters(self,settings=None, setVoltages=True, setTriggers=True):
        print("Setting channel parameters (does nothing)")
            
class DataCollector(threading.Thread):
    def __init__(self, filename, eventQueue):
        threading.Thread.__init__(self)

        self.filename=filename
        
        self.eventQueue = eventQueue
        self.daemon = True
        self.signal = False
        self.status=0

        
    def run(self):
        self.signal = True
        f=ROOT.TFile(self.filename)
        tree=f.Get("events");
        nEntries=tree.GetEntries()
        idEntry=0
        tree.GetEntry(idEntry)
        timePrev=tree.storeTime
        timeOffset=0
        
        while self.signal:
            tree.GetEntry(idEntry)
            time.sleep(max(tree.storeTime-timePrev,0))
            event=Event(status=0, fpga=tree.fpga,
                        channel=tree.channel, storeTime=tree.storeTime,
                        time=tree.time+timeOffset, totValue=tree.totValue,
                        peakValue=tree.peakValue, sumValue=tree.sumValue,
                        neutronFlag=tree.neutronFlag,  nPreSamples=tree.nPreSamples,
                        nPostSamples=tree.nPostSamples, waveform=np.array(tree.waveform),
                        pedestal=np.median(np.array(tree.waveform)))
            self.eventQueue.put(event)
            timePrev=tree.storeTime
            idEntry+=1
            if idEntry == nEntries: # Start the loop again
                print("End of the root file. Readin the same events again")
                idEntry=0
                tree.GetEntry(idEntry)
                timeOffset+=timePrev-tree.time

    def stop(self):
        ''' Stops the thread immediately'''
        self.signal=False
        # time.sleep(10)

    def softStop(self):
        ''' Stops the thread after cheking that no now data is comming in'''
        self.signal=False
        return 0                


def main():
    from settings import daqSettings
    eventQueue = Queue.Queue()
    daq=Daq(daqSettings, eventQueue)
    daq.connect()
    daq.start()
    time.sleep(10.0)
    daq.stop()
    time.sleep(5.0)
    daq.start()
    time.sleep(10.0)
    daq.stop()
    daq.disconnect()
    
if __name__ == "__main__":
    main()

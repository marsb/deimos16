#!/usr/bin/python

import Tkinter as tk

import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
# implement the default mpl key bindings
# from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure
from matplotlib.backend_bases import key_press_handler
import numpy as np
import scipy as sc
import dataStructures
# from lmfit import minimize, Parameters, Minizer

class PaViewer:
    def __init__(self,master,paData_all):

        self.paData_all=paData_all
        self.channels=[]
        # Find fpgas and channels. Then sort by fpga and channel id
        for paData in paData_all:
            if (paData.fpga,paData.channel) not in self.channels:
                self.channels.append((paData.fpga,paData.channel))
        self.channels=sorted(self.channels, key = lambda x: (x[0], x[1]))
        self.currentChannelIndex=0
                    
        self.master=master
        self.master.grid()
        self.frame = tk.Frame(self.master)
        self.frame.grid(column=0,row=0,sticky='NSEW')
        
        # Make toolbar
        self.toolbar=tk.Frame(self.frame)
        self.toolbar.grid(column=0,row=0,sticky='W')
        # Add button previous
        self.previousChButton = tk.Button(self.toolbar,text=u"Previous",
                                          command=self.GetPreviousChannel)
        self.previousChButton.grid(column=0,row=0)
        # Add button next
        self.nextChButton = tk.Button(self.toolbar,text=u"Next",
                                      command=self.GetNextChannel)
        self.nextChButton.grid(column=1,row=0)


        # Make info frame
        self.infoFrame=tk.Frame(self.frame)
        self.infoFrame.grid(column=0,row=1,columnspan=2,sticky='EW')

        # FPGA Id
        fpgaLabel= tk.Label(self.infoFrame, text="FPGA:",
                            anchor="w",fg="white",bg="blue",
                            bd=2, relief=tk.GROOVE)
        fpgaLabel.grid(column=0,row=0,sticky='EW')
        self.fpgaVariable = tk.StringVar()
        label = tk.Label(self.infoFrame, textvariable=self.fpgaVariable,
                         anchor="w",fg="black",bg="white",
                         bd=2, relief=tk.GROOVE)
        label.grid(column=0,row=1,sticky='EW')
        self.fpgaVariable.set(u"fpga")

        # Channel Id
        label= tk.Label(self.infoFrame, text="Ch:",
                        anchor="w",fg="white",bg="blue",
                        bd=2, relief=tk.GROOVE)
        label.grid(column=1,row=0,sticky='EW')
        self.chVariable = tk.StringVar()
        label = tk.Label(self.infoFrame, textvariable=self.chVariable,
                         anchor="w",fg="black",bg="white",
                         bd=2, relief=tk.GROOVE)
        label.grid(column=1,row=1,sticky='EW')
        self.chVariable.set(u"ch")

        # TrimV1
        trim1Label= tk.Label(self.infoFrame, text="TrimV1:",
                             anchor="w",fg="white",bg="blue",
                             bd=2, relief=tk.GROOVE)
        trim1Label.grid(column=2,row=0,sticky='EW')
        self.trim1Variable = tk.StringVar()
        label = tk.Label(self.infoFrame, textvariable=self.trim1Variable,
                         anchor="w",fg="black",bg="white",
                         bd=2, relief=tk.GROOVE)
        label.grid(column=2,row=1,sticky='EW')
        self.trim1Variable.set(u"trim1")

        # PA1
        pa1Label= tk.Label(self.infoFrame, text="PA1:",
                           anchor="w",fg="white",bg="blue",
                           bd=2, relief=tk.GROOVE)
        pa1Label.grid(column=3,row=0,sticky='EW')
        self.pa1Variable = tk.StringVar()
        label = tk.Label(self.infoFrame, textvariable=self.pa1Variable,
                         anchor="w",fg="black",bg="white",
                         bd=2, relief=tk.GROOVE)
        label.grid(column=3,row=1,sticky='EW')
        self.pa1Variable.set(u"pa1")

        # TrimV2
        trim2Label= tk.Label(self.infoFrame, text="TrimV2:",
                             anchor="w",fg="white",bg="blue",
                             bd=2, relief=tk.GROOVE)
        trim2Label.grid(column=4,row=0,sticky='EW')
        self.trim2Variable = tk.StringVar()
        label = tk.Label(self.infoFrame, textvariable=self.trim2Variable,
                         anchor="w",fg="black",bg="white",
                         bd=2, relief=tk.GROOVE)
        label.grid(column=4,row=1,sticky='EW')
        self.trim2Variable.set(u"trim2")

        # PA2
        pa2Label= tk.Label(self.infoFrame, text="PA2:",
                           anchor="w",fg="white",bg="blue",
                           bd=2, relief=tk.GROOVE)
        pa2Label.grid(column=5,row=0,sticky='EW')
        self.pa2Variable = tk.StringVar()
        label = tk.Label(self.infoFrame, textvariable=self.pa2Variable,
                         anchor="w",fg="black",bg="white",
                         bd=2, relief=tk.GROOVE)
        label.grid(column=5,row=1,sticky='EW')
        self.pa2Variable.set(u"pa2")

        # TrimV3
        trim3Label= tk.Label(self.infoFrame, text="TrimV3:",
                             anchor="w",fg="white",bg="blue",
                             bd=2, relief=tk.GROOVE)
        trim3Label.grid(column=6,row=0,sticky='EW')
        self.trim3Variable = tk.StringVar()
        label = tk.Label(self.infoFrame, textvariable=self.trim3Variable,
                         anchor="w",fg="black",bg="white",
                         bd=2, relief=tk.GROOVE)
        label.grid(column=6,row=1,sticky='EW')
        self.trim3Variable.set(u"trim3")

        # PA2
        pa3Label= tk.Label(self.infoFrame, text="PA3:",
                           anchor="w",fg="white",bg="blue",
                           bd=2, relief=tk.GROOVE)
        pa3Label.grid(column=7,row=0,sticky='EW')
        self.pa3Variable = tk.StringVar()
        label = tk.Label(self.infoFrame, textvariable=self.pa3Variable,
                         anchor="w",fg="black",bg="white",
                         bd=2, relief=tk.GROOVE)
        label.grid(column=7,row=1,sticky='EW')
        self.pa3Variable.set(u"pa3")

        # # Add canvas for plots
        self.figure = Figure(figsize=(8,6), dpi=100)
        self.graph1 = self.figure.add_subplot(111)

        # a tk.DrawingArea
        self.canvas = FigureCanvasTkAgg(self.figure, master=self.frame)
        self.canvas.show()
        self.canvas.get_tk_widget().grid(column=0,row=2,columnspan=2,sticky='NSEW')

        # Add toolbar with zoom button
        self.plotToolbarFrame=tk.Frame(self.frame)
        self.plotToolbarFrame.grid(column=1,row=0,sticky='W')
        self.plotToolbar=NavigationToolbar2TkAgg(self.canvas,self.plotToolbarFrame)    
        # Add keyboard shortcuts
        self.canvas.mpl_connect('key_press_event',self.on_key_event)

        xData=np.arange(0.0,3.0,0.01)
        yData=np.sin(2*np.pi*xData)
        self.plotData(xData ,yData)

        # Plot first channel if available
        if len(self.channels)>0:
            self.GetChannel(self.channels[self.currentChannelIndex][0],
                            self.channels[self.currentChannelIndex][1])

        # #toolbar = NavigationToolbar2TkAgg( canvas, root )
        # #toolbar.update()
        
        # # Enable resizing
        # self.master.resizable(True,True) # Window can only resize horizontally
        self.master.grid_columnconfigure(0,weight=1) # resize only the first column (0)
        self.master.grid_rowconfigure(0,weight=1) # resize only the third row (2)

        self.frame.grid_columnconfigure(1,weight=1) # resize only the second column (0)
        self.frame.grid_rowconfigure(2,weight=1) # resize only the third row (2)

        # # But prevent constant resizing
        # self.update()
        # self.geometry(self.geometry()) 

        # Keep the text entry focused
        #self.eventNumberEntry.focus_set()
        #self.eventNumberEntry.selection_range(0, tk.END)

    def close_window(self):
        self.master.destroy()

    def on_key_event(self,event):
        print "You pressed key:", event.key
        key_press_handler(event,self.canvas,self.plotToolbar)

    def GetChannel(self,fpga,channel):
        self.fpgaVariable.set(fpga)
        self.chVariable.set(channel)
        self.trim1Variable.set("-")
        self.pa1Variable.set("-")
        self.trim2Variable.set("-")
        self.pa2Variable.set("-")
        self.trim3Variable.set("-")
        self.pa3Variable.set("-")


        paData_selected=[]
        counter=0
        for paData in self.paData_all:
            if (paData.fpga==fpga and paData.channel==channel):
                paData_selected.append(paData)
                if paData.paHeight!=None:
                    counter+=1
                    if counter==1:
                        self.trim1Variable.set("{:.2f}".format(paData.trimV))
                        self.pa1Variable.set("{:.2f}".format(paData.paHeight))
                    if counter==2:
                        self.trim2Variable.set("{:.2f}".format(paData.trimV))
                        self.pa2Variable.set("{:.2f}".format(paData.paHeight))
                    if counter==3:
                        self.trim3Variable.set("{:.2f}".format(paData.trimV))
                        self.pa3Variable.set("{:.2f}".format(paData.paHeight))

        print "Plottin PA data for fpga:", fpga, "channel:", channel                    
        self.plotPaData(paData_selected)

        # # Keep the text entry focused
        # self.eventNumberEntry.focus_set()
        # self.eventNumberEntry.selection_range(0, tk.END)

    def GetPreviousChannel(self):
        if self.currentChannelIndex>0:
            self.currentChannelIndex-=1
            self.GetChannel(self.channels[self.currentChannelIndex][0],
                            self.channels[self.currentChannelIndex][1])
        else:
            print "Already showing first channel"

    def GetNextChannel(self):
        if self.currentChannelIndex<len(self.channels)-1:
            self.currentChannelIndex+=1
            self.GetChannel(self.channels[self.currentChannelIndex][0],
                            self.channels[self.currentChannelIndex][1])
        else:
            print "Already showing last channel"

    def plotData(self,xData,yData):
        self.graph1.clear()    
        self.graph1.plot(xData,yData)
        # a.set_title('Tk embedding')
        self.graph1.set_xlabel('X-Axis')
        self.graph1.set_ylabel('Y-Axis')
        self.figure.set_tight_layout(True)
        self.canvas.draw()

    def plotPaData(self,paData_list):
        self.graph1.clear()
        lineStyle=['b-','r-','g-','c-','m-','b--','r--','g--','c--','m--']
        lineNumber=0
        for paData in paData_list:
            self.graph1.plot(paData.rawX,paData.rawData,lineStyle[min(lineNumber,9)],
                             label="V={:.2f}".format(paData.trimV))
            if len(paData.smoothX)>0:
                self.graph1.plot(paData.smoothX,paData.smoothY,'k--')
            if len(paData.fitX)>0:
                self.graph1.plot(paData.fitX,paData.fitData,'k-')
                self.graph1.plot(paData.fitX,paData.baselineData,'k-')
            lineNumber+=1
        # a.set_title('Tk embedding')
        self.graph1.set_xlabel('Height (ADC)')
        self.graph1.set_ylabel('Entries')
        self.graph1.set_yscale('log')
        ylim=self.graph1.get_ylim()
        self.graph1.set_ylim((max(ylim[0],1),ylim[1]))
        self.graph1.legend()
        self.canvas.draw()

        #textX=graph.get_xlim()[0]+0.6*(graph.get_xlim()[1]-graph.get_xlim()[0])
        #textY=graph.get_ylim()[0]+0.7*(graph.get_ylim()[1]-graph.get_ylim()[0])
        #text="Channel: "+str(event.channel)+"\n"
        #text+="Peak value: "+str(event.peakValue)+"\n"
        #text+="Sum value: "+str(event.sumValue)+"\n"
        #text+="Is neutron: "+str(bool(event.neutronFlag))
        #graph.text(textX, textY, text)


def main():

    root=tk.Tk()
    paData=[]
    app=PaViewer(root,paData)
    app.master.title('PA viewer')
    root.mainloop()


if __name__ == "__main__":
    main()

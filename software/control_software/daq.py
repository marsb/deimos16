#!/usr/bin/python
from __future__ import print_function
import fpgaControl
import threading
import Queue
import numpy as np
import time
import sys

class Daq():
    def __init__(self, daqSettings):
        """ Initialise the DAQ by giving DaqSettings and Queue objects"""
        self.daqSettings=daqSettings
        self.eventQueue=Queue.Queue()
        self.fpgaControls=[]
        self.dataCollectors=[]

        # Status
        self.isRunning=False
        self.isConnected=False

        self.tempCorrection=True
        

        # Create one control and collector per FPGA
        for fpgaId in range(len(self.daqSettings.fpgas)):
            fpgaSettings=self.daqSettings.fpgas[fpgaId]
            self.fpgaControls.append(fpgaControl.FpgaControl())
            self.dataCollectors.append(DataCollector(self.fpgaControls[fpgaId],self.eventQueue))
    
    def connect(self):
        for fpgaId in range(len(self.fpgaControls)):
            print("Connecting to FPGA", self.daqSettings.fpgas[fpgaId].id)
            self.fpgaControls[fpgaId].connect(self.daqSettings.fpgas[fpgaId])
        if(self.tempCorrection==True):
            print("Startin voltage temperature control")
            self.voltageControl=VoltageControl(self) 
            self.voltageControl.start()

        self.isConnected=True
        # self.dataAcquisition.start()
        


    def disconnect(self):
        self.stop()
        self.voltageControl.stop()
        self.isConnected=False
        print("DAQ disconnected")

    def start(self):
        # self.eventStorage.clear()
        
        # Start data collectors
        self.dataCollectors=[]
        for fpgaId in range(len(self.daqSettings.fpgas)):
            self.dataCollectors.append(DataCollector(self.fpgaControls[fpgaId],self.eventQueue))
            self.dataCollectors[-1].start()

        # Send reset timer from master FPGA
        self.fpgaControls[0].reset_timer()

        # Enable triggering
        print("Enabling triggering for")
        for fpgaId in range(len(self.daqSettings.fpgas)):
            print("   FPGA ", fpgaId, "channels:",end='')
            fpgaControl=self.fpgaControls[fpgaId]
            for chId in range(len(self.daqSettings.fpgas[fpgaId].chs)):
                if self.daqSettings.fpgas[fpgaId].chs[chId].type>0:
                    fpgaControl.enable_trigger(chId)
                    print("",chId,end='')
            print()
        self.isRunning=True
        
    def stop(self):
        # Disbale triggers
        print("Disabling triggering for")
        for fpgaId in range(len(self.daqSettings.fpgas)):
            fpgaControl=self.fpgaControls[fpgaId]
            print("   FPGA ", fpgaId, "channels:",end='')
            for chId in range(len(self.daqSettings.fpgas[fpgaId].chs)):
                fpgaControl.disable_trigger(chId)
                print("",chId,end='')
            print()
        # Stop data collectors
        for fpgaId in range(len(self.daqSettings.fpgas)):
            dataCollector=self.dataCollectors[fpgaId]
            status=dataCollector.softStop()
            if (status!=0):
                print("Forcing FPGA",fpgaId,"data collector to stop"
                      +"(some data may be left in FPGA buffer).")
                dataCollector.stop()
        self.isRunning=False

    def shoot_calibration_pulse(self):
        for fpgaControl in self.fpgaControls:
            fpgaControl.shoot_calibration_pulse()

    def set_channel_parameters(self,settings=None, setVoltages=True, setTriggers=True):
        """ Set channel parameters on all FPGAs """
        if (settings!=None):
            self.daqSettings=settings
        for idFpga in range(len(self.daqSettings.fpgas)):
            self.fpgaControls[idFpga].set_channel_parameters(self.daqSettings.fpgas[idFpga], 
                                                            setVoltages=setVoltages,
                                                            setTriggers=setTriggers)
            
class DataCollector(threading.Thread):
    def __init__(self, fpgaControl, eventQueue):
        threading.Thread.__init__(self)
        self.miniips=fpgaControl # Notice! Naming not corrected inside this class
        self.eventQueue = eventQueue
        self.daemon = True
        self.signal = False
        self.status=0

    def run(self):
        self.signal = True
        sleepTime=0.01
        counterReset=0.0
        counterGammaAll=0.0
        counterGammaNeutron=0.0
        counterNeutronAll=0.0
        counterNeutronNeutron=0.0

        timeoutsInRow=0
        errorsInRow=0
        while self.signal:
            if ((time.time()-counterReset)>10):
                print("FPGA", self.miniips.fpgaId, "Event rate",
                      "total: {0:.1f} Hz".format(counterGammaAll/(time.time()-counterReset)),
                      "n: {0:.1f} Hz".format(counterGammaNeutron/(time.time()-counterReset)))
                #print("Neutron channel rate",
                #      "total: {0:.1f} Hz".format(counterNeutronAll/(time.time()-counterReset)),
                #      "n: {0:.1f} Hz".format(counterNeutronNeutron/(time.time()-counterReset)))
                counterReset=time.time()
                counterGammaAll=0.0
                counterGammaNeutron=0.0
                #counterNeutronAll=0.0
                #counterNeutronNeutron=0.0
            oldTime=time.time()
            (self.status,event)=self.miniips.get_data()
            if (self.status==0 and event.peakValue!=None):
                #if (event.channel in defaults.neutronChannels):
                #    counterNeutronAll+=1.0
                #    if (event.neutronFlag==True):
                #        counterNeutronNeutron+=1.0
                #if (event.channel in defaults.gammaChannels):
                counterGammaAll+=1.0
                if (event.neutronFlag==True):
                    counterGammaNeutron+=1.0

                timeoutsInRow=0
                errorsInRow=0
                # print event
                self.eventQueue.put(event)
                # print "*", 
            elif self.status==32777: #Time out
                timeoutsInRow+=1
                #print "Time out (status=",status,")"
                print("+")
                sys.stdout.flush()
                time.sleep(sleepTime-(oldTime-time.time()))  # wait some time
                if timeoutsInRow>50:
                    print("Continuous timeouts")
                    timeoutsInRow=0
            else:
                #print "Other error (status=",status,")"
                errorsInRow+=1
                print("-",end='')
                sys.stdout.flush()
                if errorsInRow>50:
                    print("Continuous errors (buffer overflow?) Latest status=",self.status,")")
                    errorsInRow=0
    def stop(self):
        ''' Stops the thread immediately'''
        self.signal=False
        # time.sleep(10)

    def softStop(self):
        ''' Stops the thread after cheking that no now data is comming in'''
        sleepTime=0.1 # in s
        maxTime=15.0 # in s
        timePast=0
        time.sleep(sleepTime)
        while(True):
            if(self.status==32777):
                print("No new data in FPGA buffer")
                self.signal=False
                time.sleep(0.1)
                return 0
            time.sleep(sleepTime)
            timePast+=sleepTime
            if (timePast>=maxTime):
                print("Data collector could not be stopped (too much data)")
                return 1
                

class VoltageControl(threading.Thread):
    def __init__(self, daq):
        threading.Thread.__init__(self)
        self.daq=daq
        self.temperatureFeedback=True
        self.tolerance=0.5
        
    def run(self):
        self.signal = True
        updateSleepTime=60.0 # in s
        # self.doTempCorrection()

        while self.signal:
            #for fpgaId in range(len(self.daq.daqSettings.fpgas)):
            # Only read the temperature from the master
            fpgaId=0
            newTem=self.daq.fpgaControls[fpgaId].get_temperature2()
            oldTem=self.daq.daqSettings.fpgas[fpgaId].sipmTem
            if oldTem==None: # sipmTem not yet set
                oldTem=self.daq.daqSettings.fpgas[fpgaId].nomTem
            if ((self.temperatureFeedback==True) 
                and newTem>10.0
                and newTem<50.0
                and abs(oldTem-newTem)>self.tolerance):
                print("MPPC temperature: ", oldTem,"->",newTem," (Automatic voltage correction)")
                self.doTempCorrection(newTem)
            if (newTem<10.0 or newTem>50.0):
                print("MPPC temperature out or range: ", oldTem,"->",newTem," (No voltage correction)")

            time.sleep(updateSleepTime)  # wait some time

    def doTempCorrection(self,newTem=None):
        for fpgaId in range(len(self.daq.daqSettings.fpgas)):
            if newTem==None:
                newTem=self.daq.fpgaControls[fpgaId].get_temperature2()
            nomTem=self.daq.daqSettings.fpgas[fpgaId].nomTem
            temCoef=self.daq.daqSettings.fpgas[fpgaId].temCoef
            temV=temCoef*(newTem-nomTem)
            ## Update settings
            # sipm temperature
            self.daq.daqSettings.fpgas[fpgaId].sipmTem=newTem
            # sipm temperature voltage
            for ch in self.daq.daqSettings.fpgas[fpgaId].chs:
                ch.temV=temV
            # Apply new settings
        self.daq.set_channel_parameters(setVoltages=True, setTriggers=False)

    def stop(self):
        self.signal=False
        # time.sleep(10)
    

def main():
    from settings import daqSettings
    eventQueue = Queue.Queue()
    daq=Daq(daqSettings, eventQueue)
    daq.connect()
    daq.start()
    time.sleep(10.0)
    daq.stop()
    time.sleep(5.0)
    daq.start()
    time.sleep(10.0)
    daq.stop()
    daq.disconnect()
    
if __name__ == "__main__":
    main()

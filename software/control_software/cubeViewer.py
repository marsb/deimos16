#!/usr/bin/python

import Tkinter as tk

import matplotlib
import ndap
import threading
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
import numpy as np
import scipy as sc
import time

# from lmfit import minimize, Parameters, Minizer

class CubeViewer:
    def __init__(self,master,eventStorage,daqSettings):
        """ TK frame for viewing cube rates online """

        self.eventStorage=eventStorage
        self.events=eventStorage.getEvents()

        self.neutronChannels=[]
        self.gammaChannels=[]
        self.allChannels=[]
        for fpga in daqSettings.fpgas:
            for ch in fpga.chs:
                self.allChannels.append((fpga,ch))
                if ch.type==1:
                    self.neutronChannels.append((fpga,ch))
                if ch.type==2:
                    self.gammaChannels.append((fpga,ch))
                    
        self.master=master
        self.master.grid()
        # run close window when exit
        self.master.protocol("WM_DELETE_WINDOW", self.close_window)
        self.frame = tk.Frame(self.master)
        self.frame.grid(column=0,row=0,sticky='NSEW')

        # Create new total rate plotter
        self.cubePlotter=CubePlotter('Neutron hits')
        # Add plotter to TK drawing area
        canvas = FigureCanvasTkAgg(self.cubePlotter.figure, master=self.frame)
        self.cubePlotter.figure.axes[0].mouse_init() # Enable rotation
        canvas.show()
        canvas.get_tk_widget().grid(column=0,row=0,sticky='NSEW')
        # #toolbar = NavigationToolbar2TkAgg( canvas, root )
        # #toolbar.update()
        # Create ratate calculator
        self.cubeRateCalculator=CubeRateCalculator(self.events,
                                                   daqSettings,
                                                   self.cubePlotter,
                                                   1000000000,
                                                   self.allChannels,
                                                   "neutron")
        self.cubeRateCalculator.start() # Start thread
        # Create rate toolbar
        self.toolbarFrame=tk.Frame(self.frame)
        self.toolbarFrame.grid(column=0,row=1,sticky='EW')
        self.cubeRateToolbar=CubeRateToolbar(self.cubeRateCalculator,self.toolbarFrame)

        # # Enable resizing
        self.master.resizable(True,True) # Window can only resize horizontally
        self.master.grid_columnconfigure(0,weight=1) # resize only the first column (0)
        self.master.grid_rowconfigure(0,weight=1) # resize only the third row (1)

        self.frame.grid_columnconfigure(0,weight=1) # resize only the first column (0)
        self.frame.grid_rowconfigure(0,weight=1) # resize only the first, third and fifth row

        # # But prevent constant resizing
        # self.update()
        # self.geometry(self.geometry()) 

        # Keep the text entry focused
        # self.eventNumberEntry.focus_set()
        # self.eventNumberEntry.selection_range(0, tk.END)

    def close_window(self):
        print "Cube viewer is closing"
        self.cubeRateCalculator.stop()
        del self.cubeRateToolbar
        # del self.cubeRateCalculator
        # del self.cubePlotter
        self.master.destroy()
        
class CubeRateCalculator(threading.Thread):
    def __init__(self, events, daqSettings, cubePlotter,timeBin,acceptedChannels,eventType):
        """Calculates the number of events with the time bin and plots 
        the rate with rate plotter"""
        
        threading.Thread.__init__(self)
        self.daemon=True
        self.signal=True
        self.events=events
        self.cubePlotter=cubePlotter
        self.reset_interval=timeBin # in adc samples
        self.update_interval=1.0 # in s
        self.acceptedChannels=acceptedChannels
        self.maxNBins=200        

        if eventType=="neutron":
            self.neutronFlags=[1]
        if eventType=="gamma":
            self.neutronFlags=[0]
        if eventType=="any":
            self.neutronFlags=[0,1]

        self.settings_xyz={}
        self.settings_pedestal={}
        for fpga in daqSettings.fpgas:
            for ch in fpga.chs:
                self.settings_xyz[(fpga.id,ch.id)]=(ch.x,ch.y,ch.z)
                self.settings_pedestal[(fpga.id,ch.id)]=ch.pedestal
                
    def run(self):
        oldTime=0.0
        nEventsProcessed=len(self.events)-1
        newEvents=False
        x=-1
        y=-1
        z=-1
        rightType=False
        previous_update=time.time()
        previous_reset=0
        time_coincidence=0
        resets_since_update=0
        
        while self.signal:

            #print "New events:",len(self.events)-nEventsProcessed
            for event in self.events[nEventsProcessed:]:
                nEventsProcessed+=1

                ### Update plot if update interval passed and new events to show
                if newEvents and (time.time()-previous_update)>self.update_interval:
                    #print "Updating"
                    self.cubePlotter.draw()
                    previous_update=time.time()
                    resets_since_update=0

                if newEvents and resets_since_update > 2:
                    continue

                ### Filter out events with baseline shift
                if (event.pedestal-self.settings_pedestal[(event.fpga,event.channel)])>20:
                    continue
                
                ### Find xyz coincidence (assumes time orderd)
                xyz=self.settings_xyz[(event.fpga,event.channel)]
                if (abs(event.time-time_coincidence)<100): # in time coincidecen
                    if xyz[0]>-1: x=xyz[0]
                    if xyz[1]>-1: y=xyz[1]
                    if xyz[2]>-1: z=xyz[2]
                    if event.neutronFlag in self.neutronFlags: rightType=True
                    if (x>=0 and y>=0 and z>=0 and rightType):
                        newEvents=True
                else: # not in time coincidence
                    x=xyz[0]
                    y=xyz[1]
                    z=xyz[2]
                    time_coincidence=event.time
                    if event.neutronFlag in self.neutronFlags: rightType=True
                    else: rightType=False

                ### Reset plot if reset interval passed and new events to show
                if (event.time-previous_reset)>self.reset_interval:
                    #print "Reseting"
                    self.cubePlotter.reset()
                    previous_reset=event.time
                    newEvents=False
                    resets_since_update+=1
                    
                ### Add to cube plot if in time coincidence
                if (x>=0 and y>=0 and z>=0 and rightType):
                    #print "xyz:",x,y,z
                    self.cubePlotter.add((x,y,z))
                    #self.cubePlotter.add((1,1,3)) ### force coordinates for testing

            #print max(0.2,self.update_interval-(time.time()-previous_update))
            time.sleep(max(0.1,self.update_interval-(time.time()-previous_update)))

            
    def stop(self):
        self.signal=False
        time.sleep(0.5)
        # del self.ratePlotter
        #self.exit()

    def set_time_bin(self,time_bin):
        self.cubePlotter.nmax=max(self.cubePlotter.nmax*float(time_bin)/self.reset_interval,5.0)
        self.reset_interval=int(time_bin)
        print "Time bin changed to",str(self.reset_interval)


class CubePlotter():
    def __init__(self,title):
        """ Plots the rate values """
        self.colormap=matplotlib.cm.get_cmap('jet')
        self.nmax=5.
        self.nevents_xyz=np.zeros((4,4,4))
        
        subject = np.ones((4,4,4))
        self.plotter = ndap.NDArrayPlotter(subject,spacing=("even", 0.1,0.1,0.1) )
        self.plotter.set_alpha(0.05)
        self.plotter.set_color(self.colormap(0.0))
        (self.figure, self.ax) = self.plotter.render(azim=-40,elev=25)
        #self.add((0,0,0))

        self.figure.set_tight_layout(True)
        
    def add(self, xyz):
        self.nevents_xyz[xyz[0],xyz[1],xyz[2]]+=1
        color=self.colormap(self.nevents_xyz[xyz[0],xyz[1],xyz[2]]/self.nmax)
        self.plotter.set_alpha(0.5,xyz)
        self.plotter.set_color(color,xyz)

    def reset(self):
        new_nmax=max(np.max(self.nevents_xyz),5.0)
        if ((self.nmax/new_nmax)>1.5 or (new_nmax/self.nmax)>1.5):
            self.nmax=new_nmax
        self.nevents_xyz=np.zeros((4,4,4))
        self.plotter.set_alpha(0.05)
        self.plotter.set_color(self.colormap(0.0))

    def draw(self):
        self.figure.canvas.draw() 
        self.figure.canvas.flush_events()
    
        
        
class CubeRateToolbar():
    """ TK toolbar for editing the parameters of the rate calculator """ 
    def __init__(self,cubeRateCalculator,master):
        self.cubeRateCalculator=cubeRateCalculator
        self.toolbarFrame=master

        # Add time bin text
        timeBinLabel= tk.Label(self.toolbarFrame, text="Time bin:",
                              anchor="w",fg="black",bg="white")
        timeBinLabel.grid(column=0,row=0,sticky='W')

        # Add time interval entry
        self.timeBinVariable = tk.IntVar()
        self.timeBinEntry = tk.Entry(self.toolbarFrame, 
                                         textvariable=self.timeBinVariable,
                                         width=10) # Text entry
        self.timeBinEntry.grid(column=1,row=0,sticky='W') # Stick West 
        self.timeBinEntry.bind("<Return>", self.set_time_bin)
        self.timeBinVariable.set(u"100000000")
        self.set_time_bin(None)
            
    def set_time_bin(self,event): ### Event must be here but is not used
        timeBin=self.timeBinVariable.get()
        self.cubeRateCalculator.set_time_bin(timeBin)
        #### This does do anything yet
        # Keep the text entry focused
        self.timeBinEntry.focus_set()
        self.timeBinEntry.selection_range(0, tk.END)
    
    def __del__(self):
        print "Cube rate toolbar deleted"

def main():
    from eventStorage import EventStorage
    from settings import daqSettings
    from Queue import Queue
    from daq_playback import Daq
    from argparse import ArgumentParser

    parser = ArgumentParser(description='Plot rate in cubes')
    parser.add_argument('input_file_name', metavar='input.root', type=str, 
                        help='Input root files')
    args=parser.parse_args()


    daq=Daq(daqSettings,args.input_file_name)
    eventQueue=daq.eventQueue
    eventStorage=EventStorage(eventQueue)
    eventStorage.start()
    daq.start()
    root=tk.Tk()
    app=CubeViewer(root,eventStorage,daqSettings)
    app.master.title('Rate viewer')
    print "Start main loop"
    root.mainloop()
    print "Exit main loop"
    eventStorage.stop()

if __name__ == "__main__":
    main()

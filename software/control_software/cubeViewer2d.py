#!/usr/bin/python

import Tkinter as tk
from directionFitter import DirectionFitter
import matplotlib
import ndap
import threading
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.figure import Figure
from matplotlib.patches import Wedge, Circle
from matplotlib.colors import LinearSegmentedColormap
import numpy as np
import scipy as sc
import time

# from lmfit import minimize, Parameters, Minizer

class CubeViewer2D:
    def __init__(self,master,eventStorage,daqSettings):
        """ TK frame for viewing cube rates online """

        self.eventStorage=eventStorage
        self.events=eventStorage.getEvents()

        self.neutronChannels=[]
        self.gammaChannels=[]
        self.allChannels=[]
        for fpga in daqSettings.fpgas:
            for ch in fpga.chs:
                self.allChannels.append((fpga,ch))
                if ch.type==1:
                    self.neutronChannels.append((fpga,ch))
                if ch.type==2:
                    self.gammaChannels.append((fpga,ch))
                    
        self.master=master
        self.master.grid()
        # run close window when exit
        self.master.protocol("WM_DELETE_WINDOW", self.close_window)
        self.frame = tk.Frame(self.master)
        self.frame.grid(column=0,row=0,sticky='NSEW')

        # Create new total rate plotter
        self.cubePlotter=CubePlotter2D((4,4,4))
        # Add plotter to TK drawing area
        canvas = FigureCanvasTkAgg(self.cubePlotter.fig, master=self.frame)
        #self.cubePlotter.fig.axes[0].mouse_init() # Enable rotation
        canvas.show()
        canvas.get_tk_widget().grid(column=0,row=0,sticky='NSEW')
        # #toolbar = NavigationToolbar2TkAgg( canvas, root )
        # #toolbar.update()
        # Create ratate calculator
        self.cubeRateCalculator=CubeRateCalculator(self.events,
                                                   daqSettings,
                                                   self.cubePlotter,
                                                   1000000000,
                                                   self.allChannels,
                                                   "neutron")
        self.cubeRateCalculator.start() # Start thread
        # Create rate toolbar
        self.toolbarFrame=tk.Frame(self.frame)
        self.toolbarFrame.grid(column=0,row=1,sticky='EW')
        self.cubeRateToolbar=CubeRateToolbar(self.cubeRateCalculator,self.toolbarFrame)

        # # Enable resizing
        self.master.resizable(True,True) # Window can only resize horizontally
        self.master.grid_columnconfigure(0,weight=1) # resize only the first column (0)
        self.master.grid_rowconfigure(0,weight=1) # resize only the third row (1)

        self.frame.grid_columnconfigure(0,weight=1) # resize only the first column (0)
        self.frame.grid_rowconfigure(0,weight=1) # resize only the first, third and fifth row

        # # But prevent constant resizing
        # self.update()
        # self.geometry(self.geometry()) 

        # Keep the text entry focused
        # self.eventNumberEntry.focus_set()
        # self.eventNumberEntry.selection_range(0, tk.END)

    def close_window(self):
        print "Cube viewer is closing"
        self.cubeRateCalculator.stop()
        del self.cubeRateToolbar
        # del self.cubeRateCalculator
        # del self.cubePlotter
        self.master.destroy()
        
class CubeRateCalculator(threading.Thread):
    def __init__(self, events, daqSettings, cubePlotter,timeBin,acceptedChannels,eventType):
        """Calculates the number of events with the time bin and plots 
        the rate with rate plotter"""
        
        threading.Thread.__init__(self)
        self.daemon=True
        self.signal=True
        self.events=events
        self.cubePlotter=cubePlotter
        self.reset_interval=timeBin # in adc samples
        self.update_interval=1.0 # in s
        self.sleep_time=0.1 # in s
        self.acceptedChannels=acceptedChannels
        self.maxNBins=200
        self.liveUpdates=False
        
        self.time_last_event=0
        self.previous_reset=0
        self.previous_update=0
        
        if eventType=="neutron":
            self.neutronFlags=[1]
        if eventType=="gamma":
            self.neutronFlags=[0]
        if eventType=="any":
            self.neutronFlags=[0,1]

        self.settings_xyz={}
        self.settings_pedestal={}
        for fpga in daqSettings.fpgas:
            for ch in fpga.chs:
                self.settings_xyz[(fpga.id,ch.id)]=(ch.x,ch.y,ch.z)
                self.settings_pedestal[(fpga.id,ch.id)]=ch.pedestal
                
    def run(self):
        oldTime=0.0
        newEvents=False
        x=-1
        y=-1
        z=-1
        rightType=False

        ### Wait for the first event
        while len(self.events)==0:
            time.sleep(0.1)
            
        self.previous_update=time.time()
        firstEventId=len(self.events)
        self.previous_reset=self.events[-1].time
        lastEventId=len(self.events)
    
        time_coincidence=0
        resets_since_update=0
        
        while self.signal:
            #print "New events:",len(self.events)-nEventsProcessed
            lastEventId=len(self.events)
            for event in self.events[firstEventId:lastEventId]:
                self.time_last_event=event.time
                ### Only add events if wf baseline has not shifted
                ### and reset interval was not passed
                if ((event.pedestal-self.settings_pedestal[(event.fpga,event.channel)]<20)
                    and ((event.time-self.previous_reset)<self.reset_interval)):
                    ### Find xyz coincidence (assumes time orderd)
                    xyz=self.settings_xyz[(event.fpga,event.channel)]
                    if (abs(event.time-time_coincidence)<100): # in time coincidecen
                        if xyz[0]>-1: x=xyz[0]
                        if xyz[1]>-1: y=xyz[1]
                        if xyz[2]>-1: z=xyz[2]
                        if event.neutronFlag in self.neutronFlags: rightType=True
                        if (x>=0 and y>=0 and z>=0 and rightType):
                            newEvents=True
                    else: # not in time coincidence
                        x=xyz[0]
                        y=xyz[1]
                        z=xyz[2]
                        time_coincidence=event.time
                        if event.neutronFlag in self.neutronFlags: rightType=True
                        else: rightType=False

                    ### Add to cube plot if in time coincidence
                    if (x>=0 and y>=0 and z>=0 and rightType):
                        #print "xyz:",x,y,z
                        self.cubePlotter.add((x,y,z))
                        #self.cubePlotter.add((0,1,1)) ### force coordinates for testing

                ### Update plot if update interval passed
                if (time.time()-self.previous_update)>self.update_interval:
                    ### Update plot if live update or reset interval passed
                    if (self.liveUpdates==True or (event.time-self.previous_reset)>self.reset_interval):
                        if (self.liveUpdates==True):
                            self.cubePlotter.update_arrow(minor=True, major=False)
                        else:
                            self.cubePlotter.update_arrow(minor=False, major=True)
                            self.minorArrow=None
                        self.cubePlotter.draw() ### Uncomment for live updates
                        self.previous_update=time.time()
                        newEvents=False
                        
                    ### Reset plot if reset interval passed
                    if (event.time-self.previous_reset)>self.reset_interval:
                        #print "Reseting"
                        #self.cubePlotter.draw()
                        self.previous_reset=event.time
                        if (self.liveUpdates==True):
                            self.cubePlotter.update_arrow(minor=False, major=True)
                        self.cubePlotter.reset()
                        #newEvents=False

            time.sleep(self.sleep_time)
            firstEventId=lastEventId
            
    def stop(self):
        self.signal=False
        time.sleep(0.5)
        # del self.ratePlotter
        #self.exit()

    def set_time_bin(self,time_bin):
        self.previous_reset=self.time_last_event
        self.cubePlotter.reset(float(time_bin)/self.reset_interval)
        self.reset_interval=int(time_bin)
        self.previous_update=time.time()
        print "Time bin changed to",str(self.reset_interval)


class CubePlotter2D():
    def __init__(self,(x,y,z),plotCubes=True):
        """ Plots the rate values """
        if plotCubes:
            self.colormap=matplotlib.cm.get_cmap('jet')
            self.colormap.set_under(color="w") ## White if below min
        else:
            self.colormap=LinearSegmentedColormap.from_list(
                "white", [(1, 1, 1), (1, 1, 1), (1, 1, 1)])
        self.nmin=1
        self.nmax=5
        self.nevents_xyz=np.zeros((x,y,z))
        
        self.fig = Figure(dpi=120)

        ### Carthesian
        ax_loc = [0.1, 0.1, 0.8, 0.8]
        self.ax = self.fig.add_subplot(122)
        #self.ax=self.fig.add_axes(ax_loc, frameon=True)
        self.ax.set_xticks(range(x))
        self.ax.set_yticks(range(y))
        #self.ax.set_axis_off()
        
        self.im=self.ax.imshow(np.sum(self.nevents_xyz, axis=2).transpose(),
                               cmap=self.colormap, vmin=self.nmin, vmax=self.nmax,
                               origin="lower")
        self.ax.set_xlim(self.ax.get_xlim()[::-1])
        self.ax.yaxis.tick_right()

        self.directionFitter=DirectionFitter()
        self.minorArrow=None
        self.majorArrow=None
        self.majorArrowText=None
        self.majorArrowUnc=None
        self.compassCircle=None
        
        #self.fig.colorbar(self.im)
        self.fig.colorbar(self.im,fraction=0.046, pad=0.04)

        ### Polar
        ax_polar_loc=[0.1,0.1,0.8,0.8]
        self.ax_polar = self.fig.add_subplot(121, projection='polar')
        ## Draw grid manualy (grid(True) not working
        for angle in np.linspace(-np.pi,  np.pi, 8, endpoint=False):
            self.ax_polar.plot([angle,angle],[1.20,0.1], color='0.7', linewidth=1.0, linestyle=':')

        self.ax_polar.set_yticklabels([])
        self.ax_polar.set_rmax(1.0)
        self.ax_polar.set_theta_zero_location("S")
        self.ax_polar.set_thetagrids(np.arange(0,360,45), frac=1.2)
        self.ax_polar.set_xticks(np.pi/180. * np.linspace(180,  -180, 8, endpoint=False))
        #self.ax_polar.set_theta_direction(-1)
        self.ax_polar.grid(color='blue', linestyle='-', linewidth=2)
        self.ax_polar.spines['polar'].set_color([0.0,0.0,0.5])
        self.ax_polar.xaxis.label.set_color([0.0,0.0,0.5])
        self.ax_polar.tick_params(colors=[0.0,0.0,0.5])
        self.fig.set_tight_layout(True)

        
    def add(self, xyz):
        self.nevents_xyz[xyz[0],xyz[1],xyz[2]]+=1

    def reset(self,scaler=None):
        ### Set scale manually or automatically
        if scaler==None:
            new_nmax=max(np.max(np.sum(self.nevents_xyz,axis=2)),5.0)
            new_nmin=max(np.min(np.sum(self.nevents_xyz,axis=2)),1.0)
            if ((self.nmax/new_nmax)>1.5 or (new_nmax/self.nmax)>1.2):
                self.nmax=new_nmax
            if ((self.nmin/new_nmin)>1.0 or ((new_nmin/self.nmin)>2.0 and (new_nmin-self.nmin)>4.0)):
                self.nmin=new_nmin
        else:
            self.nmax=np.round(max(self.nmax*scaler,5.0))
            self.nmin=np.round(max(self.nmin*scaler,1.))
        self.im.set_clim(self.nmin,self.nmax)

        self.nevents_xyz[:]=0
        self.im.set_data(np.sum(self.nevents_xyz, axis=2));
        
    def draw(self):
        self.im.set_data(np.sum(self.nevents_xyz, axis=2).transpose());
        self.fig.canvas.draw() 
        #self.fig.canvas.flush_events()

    def update_arrow(self,minor=False, major=False):
        (phi,theta, const, sigma_phi,ratio) = self.directionFitter.fit(np.sum(self.nevents_xyz, axis=2))
        x0=1.5
        y0=1.5
        angle=phi
        angle_deg=np.rad2deg(-angle)-90
        angle_sigma_deg=np.rad2deg(sigma_phi)
        if angle_deg <= 180 and angle_deg >= -180:
            #angle_txt='{:0.0f}'.format(angle_deg)+u"\u2213"+'{:0.0f} deg'.format(angle_sigma_deg)
            angle_txt=r'{:0.1f} $\pm$ {:0.1f} deg'.format(angle_deg,angle_sigma_deg)
        elif angle_deg > 180:
            angle_txt='{:0.1f} $\pm$ {:0.1f} deg'.format(360.-angle_deg, angle_sigma_deg)
        elif angle_deg < -180:
            angle_txt='{:0.1f} $\pm$ {:0.1f} deg'.format(360.+angle_deg, angle_sigma_deg)
        angle_txt=angle_txt+'\n({:0.0f} \%)'.format(ratio*100)
        
        x_dev=1.2*np.cos(angle)
        y_dev=1.2*np.sin(angle)
        print "ang:", np.rad2deg(angle) #,"cos:",np.cos(angle), "sin:",np.sin(angle)
        if minor == True:
            if self.minorArrow != None:
                self.minorArrow.remove()
            self.minorArrow=self.ax.arrow(x0-x_dev,
                                          y0-y_dev,
                                          2*x_dev,2*y_dev,
                                          lw=3,
                                          alpha=0.3,
                                          head_width=0.12,
                                          head_length=0.2,
                                          fc='0.5', ec='0.5')


        ### Major arrow
        if major == True:

            ### Remove old
            if self.majorArrowUnc != None:
                self.majorArrowUnc.remove()
                self.majorArrowUnc = None
            if self.majorArrow != None:
                self.majorArrow.remove()
                self.majorArrow = None
            if self.majorArrowText != None:
                self.majorArrowText.remove()
                self.majorArrowText = None

                
            if angle_sigma_deg < 45.0 and ratio>0.2:
                ## Uncertainty cone
                self.majorArrowUnc=Wedge((0.5,0.5),
                                         1.9,
                                         angle_deg-angle_sigma_deg-90.,
                                         angle_deg+angle_sigma_deg-90.,
                                         transform=self.ax_polar.transAxes,
                                         lw=0.,
                                         facecolor=(0.,0.,1.),
                                         alpha=0.2)
                self.ax_polar.add_artist(self.majorArrowUnc)
            
                ## Arrow    
                #self.majorArrow=self.ax.arrow(x0-x_dev,
                #                              y0-y_dev,
                #                              2*x_dev,2*y_dev,
                #                              lw=3,
                #                              alpha=1.0,
                #                              head_width=0.12,
                #                              head_length=0.2,
                #                              fc='k', ec='k')
                self.majorArrow=self.ax_polar.annotate("", xytext=(0.0,0.0),
                                                       xy=(-angle-np.pi/2.,0.9),
                                                       arrowprops=dict(facecolor='red',
                                                                       lw=0))
                
                ## Text
                if (angle_deg < 90 and angle_deg > -90):
                    text_theta=np.pi
                else:
                    text_theta=0
                self.majorArrowText=self.ax_polar.text(text_theta,0.2,
                                                       angle_txt,
                                                       size=12,
                                                       #backgroundcolor='w',
                                                       verticalalignment='center',
                                                       horizontalalignment='center')

        ## Compass circle
        #if self.compassCircle == None:
        #    self.compassCircle = Circle((1.5, 1.5), 1.9, color='0.0', fill=False)
        #self.ax.add_artist(self.compassCircle)
        
class CubeRateToolbar():
    """ TK toolbar for editing the parameters of the rate calculator """ 
    def __init__(self,cubeRateCalculator,master):
        self.cubeRateCalculator=cubeRateCalculator
        self.toolbarFrame=master

        # Add time bin text
        timeBinLabel= tk.Label(self.toolbarFrame, text="Integration time (10 ns):",
                              anchor="w",fg="black",bg="white")
        timeBinLabel.grid(column=0,row=0,sticky='W')

        # Add time interval entry
        self.timeBinVariable = tk.IntVar()
        self.timeBinEntry = tk.Entry(self.toolbarFrame, 
                                         textvariable=self.timeBinVariable,
                                         width=10) # Text entry
        self.timeBinEntry.grid(column=1,row=0,sticky='W') # Stick West 
        self.timeBinEntry.bind("<Return>", self.set_time_bin)
        self.timeBinVariable.set(u"100000000")
        self.set_time_bin(None)
            
    def set_time_bin(self,event): ### Event must be here but is not used
        timeBin=self.timeBinVariable.get()
        self.cubeRateCalculator.set_time_bin(timeBin)
        #### This does do anything yet
        # Keep the text entry focused
        self.timeBinEntry.focus_set()
        self.timeBinEntry.selection_range(0, tk.END)
    
    def __del__(self):
        print "Cube rate toolbar deleted"

def main():
    from eventStorage import EventStorage
    from settings import daqSettings
    from Queue import Queue
    from daq_playback import Daq
    from argparse import ArgumentParser

    parser = ArgumentParser(description='Plot rate in cubes')
    parser.add_argument('input_file_name', metavar='input.root', type=str, 
                        help='Input root files')
    args=parser.parse_args()


    daq=Daq(daqSettings,args.input_file_name)
    eventQueue=daq.eventQueue
    eventStorage=EventStorage(eventQueue)
    eventStorage.start()
    daq.start()
    root=tk.Tk()
    app=CubeViewer2D(root,eventStorage,daqSettings)
    app.master.title('Rate viewer')
    print "Start main loop"
    root.mainloop()
    print "Exit main loop"
    eventStorage.stop()

if __name__ == "__main__":
    main()

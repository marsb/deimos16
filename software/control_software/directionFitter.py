import numpy as np
from scipy.optimize import curve_fit

class DirectionFitter:
    def __init__(self):
        self.p0=np.array([(1./3.)**0.5,
                          (1./3.)**0.5,
                          10]) ### Initial parameters
        
    def fit(self,hit_array):
        x,y=np.indices(hit_array.shape)
        xy=np.array([x.flatten(),y.flatten()])
        nevents=hit_array.flatten()+0.1
        nevents_sigma=(nevents)**0.5
        
        try:
            #print "p0:",self.p0
            self.p0[np.abs(self.p0)<0.01]=0.01 ### Otherwise may get stuck
            self.p0[np.abs(self.p0)>100]=1 ### Otherwise may get stuck
            popt,pcov=curve_fit(self.surface_plane,xy,nevents,p0=self.p0,sigma=nevents_sigma)
            #print "p:",popt
            #r=(popt[0]**2+popt[1]**2+popt[2]**2)**0.5
            r=1.0
            phi=np.arctan2(popt[1], popt[0]) # Was np.arctan(popt[1]/popt[0])
            if (np.abs(pcov[0,0])<100 and np.abs(pcov[0,0])<100): 
                mult_1=1./(1+(popt[1]/popt[0])**2)
                mult_0=-1./(1+(popt[1]/popt[0])**2)*popt[1]/popt[0]**2
                sigma_phi=(mult_1**2*pcov[1,1]
                           +mult_0**2*pcov[0,0]
                           +mult_1*mult_0*pcov[0,1])**0.5
                sigma_phi=2.*sigma_phi # Not based on anything (just conservative)
            else:
                sigma_phi=np.pi
            if sigma_phi != sigma_phi: # Check for NaN
                sigma_phi=np.pi
            #print "sigma_phi:", np.rad2deg(sigma_phi)
            #print popt[2]
            z=(np.abs(popt[0]**2+popt[1]**2-1))**0.5
            theta=np.arccos(z) # In rad
            const= popt[2]/r
            n_min=np.min(self.surface_plane(xy,popt[0],popt[1],popt[2]))
            n_max=np.max(self.surface_plane(xy,popt[0],popt[1],popt[2]))
            ratio=np.abs((n_max-np.abs(n_min))/n_max)
            # print n_min, n_max, ratio
            self.p0 = popt
        except RuntimeError:
            print "Direction fit failed"
            theta = 0
            phi = 0
            const = 0
            sigma_phi= np.pi
            ratio=1
        return phi, theta, const, sigma_phi, ratio

    def surface_plane(self,xy,nx,ny,p):
        nz=(np.abs(1-nx**2-ny**2))**0.5
        z=(nx*xy[0]+ny*xy[1]+p)/nz
        return z
    

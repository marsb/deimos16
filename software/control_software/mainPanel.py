#!/usr/bin/python

from __future__ import print_function # Enables flushing: print('.', end='') 
import Tkinter as tk
import tkFileDialog
import threading
import Queue
import time
import sys

from argparse import ArgumentParser
import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
# implement the default mpl key bindings
# from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure

import numpy as np
import scipy as sc
# from lmfit import minimize, Parameters, Minizer

from waveformViewer import waveformViewer
from settingsViewer import SettingsViewer
from pulserViewer import pulserViewer
from saveEvents import SaveEvents
from rateViewer import RateViewer
from cubeViewer import CubeViewer
from cubeViewer2d import CubeViewer2D
from autosetupViewer import AutosetupViewer
from settings import daqSettings
from eventStorage import EventStorage
from daq import Daq
from daq_playback import Daq as Daq_Playback

class MainPanel(tk.Tk):
#class MainPanel(tk.Tk):
    def __init__(self, daq, master=None):
        # tk.Tk.__init__(self,master)
        self.master=master
        self.frame=tk.Frame(self.master)
        self.frame.grid()
        # run close window when exit
        self.master.protocol("WM_DELETE_WINDOW", self.OnQuitButtonClick)

        self.daq=daq
        self.daqSettings = self.daq.daqSettings

        # Create and start event storage thread
        self.eventStorage = EventStorage(self.daq.eventQueue)
        self.eventStorage.start()

        # Add button Connect
        self.connectButton = tk.Button(self.frame,
                                       text=u"Connect",
                                       state='normal',
                                       command=self.OnConnectButtonClick)
        
        self.connectButton.grid(column=0,row=0,sticky='EW')


        # Add button Auto setup
        self.autosetupButton = tk.Button(self.frame,
                                        text=u"Auto setup",
                                        state='disabled',
                                        command=self.OnAutosetupButtonClick)
        self.autosetupButton.grid(column=0,row=1,sticky='EW')

        # Add button Settings
        self.settingsButton = tk.Button(self.frame,
                                        text=u"Settings",
                                        state='normal',
                                        command=self.OnSettingsButtonClick)
        self.settingsButton.grid(column=0,row=2,sticky='EW')


       # Add button pulser
        self.pulserButton = tk.Button(self.frame,
                                        text=u"Pulser",
                                        state='disabled',
                                        command=self.OnPulserButtonClick)
        self.pulserButton.grid(column=0,row=3,sticky='EW')

        # Add button Start
        self.startButton = tk.Button(self.frame,
                                     text=u"Start",
                                     state='disable',
                                     command=self.OnStartButtonClick)
        self.startButton.grid(column=0,row=4,sticky='EW')

       # Add button Waveform
        self.waveformButton = tk.Button(self.frame,
                                        text=u"Waveform",
                                        state='normal',
                                        command=self.OnWaveformButtonClick)
        self.waveformButton.grid(column=0,row=5,sticky='EW')

        # Add button Rate
        self.rateButton = tk.Button(self.frame,
                                    text=u"Rate",
                                    state='normal',
                                    command=self.OnRateButtonClick)
        self.rateButton.grid(column=0,row=6,sticky='EW')
        # Add button Alarms
        self.alarmButton = tk.Button(self.frame,
                                    text=u"Alarms",
                                    state='normal',
                                    command=self.OnAlarmButtonClick)
        self.alarmButton.grid(column=0,row=7,sticky='EW')

        
        # Add button Cubes 2D
        self.cube2dButton = tk.Button(self.frame,
                                      text=u"Direction",
                                      state='normal',
                                      command=self.OnCube2dButtonClick)
        self.cube2dButton.grid(column=0,row=8,sticky='EW')

        
        # Add button Cubes 3D
        self.cubeButton = tk.Button(self.frame,
                                    text=u"3D View",
                                    state='normal',
                                    command=self.OnCubeButtonClick)
        self.cubeButton.grid(column=0,row=9,sticky='EW')
        
        # Add button Save
        self.saveButton = tk.Button(self.frame,
                                    text=u"Save",
                                    state='normal',
                                    command=self.OnSaveButtonClick)
        self.saveButton.grid(column=0,row=10,sticky='EW')

       # Add button Quit
        self.quitButton = tk.Button(self.frame,
                                    text=u"Quit",
                                    state='normal',
                                    command=self.OnQuitButtonClick)
        self.quitButton.grid(column=0,row=11,sticky='EW')

        # prevent resizing
        self.master.resizable(0,0)

    def OnConnectButtonClick(self):
        if self.connectButton["text"]=="Connect":
            # Connect
            #self.eventStorage.clear()
            self.daq.connect()
            #self.daq.start()

            #self.settingsButton.config(state='normal')
            self.startButton.config(state='normal')
            self.pulserButton.config(state='normal')
            self.autosetupButton.config(state='normal')
            self.connectButton["text"]="Disconnect"
        else:
            #Disconnect
            if (self.daq.isRunning == True):
                self.daq.stop()
            self.daq.disconnect()

            #self.settingsButton.config(state='disabled')
            self.startButton.config(state='disabled')
            self.pulserButton.config(state='disabled')
            self.autosetupButton.config(state='disabled')
            self.connectButton["text"]="Connect" 

    def OnSettingsButtonClick(self):
        #try:
        #    self.settingsWindow
        #except NameError:
        self.settingsWindow = tk.Toplevel(self.master)
        self.settingsViewer=SettingsViewer(self.settingsWindow,self.daq)
        self.settingsWindow.title('Settings')
        #else:
        #    self.settingsWindow.lift()

    def OnAutosetupButtonClick(self):
        self.autosetupWindow = tk.Toplevel(self.master)
        self.autosetupViewer=AutosetupViewer(self.autosetupWindow,self.eventStorage,self.daq)
        self.autosetupWindow.title('Auto setup')

    def OnStartButtonClick(self):
        if self.startButton["text"]=="Start":
            #Start data acquisition
            self.eventStorage.clear()
            self.daq.start()


            self.connectButton.config(state='disabled')
            self.settingsButton.config(state='disabled')
            self.saveButton.config(state='disabled')
            self.quitButton.config(state='disabled')
            self.startButton["text"]="Stop"
        else:
            # Stop data acquisition
            self.daq.stop()

            self.connectButton.config(state='normal')
            self.settingsButton.config(state='normal')
            self.saveButton.config(state='normal')
            self.quitButton.config(state='normal')
            #self.dataAcquisition.stop()
            self.startButton["text"]="Start"    

    def OnWaveformButtonClick(self):
        self.waveformWindow = tk.Toplevel(self.master)
        self.waveformApp=waveformViewer(self.waveformWindow,self.eventStorage)
        self.waveformWindow.title('Waveform viewer')

    def OnRateButtonClick(self):
        self.rateWindow = tk.Toplevel(self.master)
        self.rateApp=RateViewer(self.rateWindow,self.eventStorage, self.daqSettings)
        self.rateWindow.title('Rate viewer')
    def OnAlarmButtonClick(self):
        pass
    
    def OnCubeButtonClick(self):
        self.cubeWindow = tk.Toplevel(self.master)
        self.cubeApp=CubeViewer(self.cubeWindow,self.eventStorage, self.daqSettings)
        self.cubeWindow.title('Cube viewer')

    def OnCube2dButtonClick(self):
        self.cube2dWindow = tk.Toplevel(self.master)
        self.cube2dApp=CubeViewer2D(self.cube2dWindow,self.eventStorage, self.daqSettings)
        self.cube2dWindow.title('Cube 2D viewer')

    def OnPulserButtonClick(self):
        print("Pulser button clicked","\n")
        self.pulserWindow = tk.Toplevel(self.master)
        self.pulserApp=pulserViewer(self.pulserWindow,self.daq)
        self.pulserWindow.title('Pulser')


    def OnSaveButtonClick(self):
        saveEvents=SaveEvents()
        if saveEvents.set_filename_dialog():
            saveEvents.create_event_tree(self.eventStorage.getMaxNSamples())
            saveEvents.create_settings_tree()
            saveEvents.add_events(self.eventStorage.getEvents())
            saveEvents.add_settings(self.daq.daqSettings)
            saveEvents.write_root_file()
            saveEvents.close_root_file()
        
        # if filename:
        #    return open(filename, 'w')

    def OnQuitButtonClick(self):
        #if 'waveformApp' in locals():
	# print "Closing waveform app"
        # self.waveformApp.close_window()
        # try: self.rateApp.close_window()
	# except (): pass	
        #del self.rateApp

        if self.daq.isConnected:
            self.daq.disconnect()
        self.master.destroy()
	print("Quit!")
        sys.exit()
	# self.quit()

    def disable_all_buttons(self):
        self.connectButton.config(state='disable')
        self.settingsButton.config(state='disable')
        self.saveButton.config(state='disable')
        self.waveformButton.config(state='disable')
        self.cubeButton.config(state='disable')
        self.cube2dButton.config(state='disable')
        self.saveButton.config(state='disable')
        self.quitButton.config(state='disable')


    def enable_all_buttons(self):
        self.connectButton.config(state='normal')
        self.settingsButton.config(state='normal')
        self.saveButton.config(state='normal')
        self.waveformButton.config(state='normal')
        self.cubeButton.config(state='normal')
        self.cube2dButton.config(state='normal')
        self.saveButton.config(state='normal')
        self.quitButton.config(state='normal')


def main():
    parser = ArgumentParser(description='Run DAQ software (real or playback)')
    parser.add_argument('input_file_name', metavar='input.root',
                        type=str, nargs='?', 
                        help='If given, playback data from this file')
    args=parser.parse_args()

    if args.input_file_name is not None:
        daq=Daq_Playback(daqSettings,args.input_file_name)
    else:
        daq=Daq(daqSettings)
    root=tk.Tk()
    mainPanel=MainPanel(daq,root)
    mainPanel.master.title('nF')
    root.mainloop()

if __name__ == "__main__":
    main()

#!/usr/bin/python
from dataStructures import *

# This file contains the default configuration parameters
# for backpack 1

daqSettings=DaqSettings()
daqSettings.applyTime=0

### FPGA 0
fpga=FpgaSettings()
fpga.id=0
fpga.ip=[192,168,1,100]
fpga.timeout=100000
fpga.port=20481
fpga.tempControl=True
fpga.firmware="../FPGA/miniips2.bit"
fpga.nomTem=25.0
fpga.temCoef=0.06
fpga.fanOn=True
fpga.hv=68.0
fpga.maxHv=79.2
fpga.minHv=29.5

daqSettings.fpgas.append(fpga)


### Channel settings
sipmIds  = [ 30008, 0, 0,  0,
             0,  0, 1139,  1140,
             1141, 1142, 1143, 1144,
             1145,  1146,  1147, 1148]
offV=66.0

nomV =     [67.17, offV,  offV, offV,
            offV, offV, 66.60, 66.61,
            66.68, 66.78, 66.83, 66.89,
            66.94, 66.94, 66.95, 66.90]
# V=67: not connected, V=64: no signal (light leak)            
pedestal = [8140, 8156, 8165, 8144,
            8135, 8148, 8150, 8144,
            8159, 8143, 8155, 8128,
            8158, 8160, 8150, 8143]
## chTypes 0: Not in use, 1: Neutron, 2: Gamma
chTypes=[2, 0, 0, 0,
         0, 0, 1, 1,
         1, 1, 1, 1,
         1, 1, 1, 1]

xs = [10,-1, -1, -1,
      -1, -1, 0, 1,
      2, 3, 4, 5,
      6, 7, 8, 9]
ys = [0, 0, 0, 0,
      0, 0, 0, 0,
      0, 0, 0, 0,
      0, 0, 0, 0]
zs = [0, 0, 0, 0,
      0, 0, 0, 0,
      0, 0, 0, 0,
      0, 0, 0, 0]


for index in range(16):
    ch=ChannelSettings()
    ch.id=index
    ch.sipmId=sipmIds[index]
    ch.type=chTypes[index]

    ch.pedestal=pedestal[index]
    ch.th=12
    ch.wfPre=100
    ch.wfPost=200
    ch.calcPre=60
    ch.calcPost=100
    ch.totMin=40
    ch.idAreaMin=0 # This is not in use
    ch.idPeakMax=3000
    ch.idTotMin=60

    ch.nomV=nomV[index]
    ch.trimV=-1.25
    ch.temV=0
    ch.setV=0

    ch.x=xs[index]
    ch.y=ys[index]
    ch.z=zs[index]


    daqSettings.fpgas[0].chs.append(ch)




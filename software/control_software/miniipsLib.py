#!/usr/bin/python
from ctypes import *
import time
import math
import numpy as np
import collections
from dataStructures import *


ADC_CHANNEL_MAP=[0,1,3,2,4,5,7,6,8,9,11,10,12,13,15,14]
DAC_CHANNEL_MAP=[8,9,10,11,12,13,14,15,0,2,3,4,1,5,6,7]

##########################################################

# ZESTET1_SATAUS codes
STRING = c_char_p

ZESTET1_XC3S1400A = 1
ZESTET1_ILLEGAL_FILE = 32779
ZESTET1_TIMEOUT = 32777
ZESTET1_MAX_ERROR = 32783
ZESTET1_INVALID_PART_TYPE = 32782
ZESTET1_INVALID_CONNECTION_TYPE = 32774
ZESTET1_FILE_ERROR = 32781
ZESTET1_FILE_NOT_FOUND = 32780
ZESTET1_ILLEGAL_IMAGE_HANDLE = 32778
ZESTET1_SOCKET_CLOSED = 32776
ZESTET1_OUT_OF_MEMORY = 32773
ZESTET1_INTERNAL_ERROR = 32769
ZESTET1_FPGA_UNKNOWN = 0
ZESTET1_SOCKET_ERROR = 32768
ZESTET1_ILLEGAL_CLOCK_RATE = 32772
ZESTET1_ILLEGAL_STATUS_CODE = 32770
ZESTET1_TYPE_UDP = 1
ZESTET1_MAX_WARNING = 16384
ZESTET1_NULL_PARAMETER = 32771
ZESTET1_SUCCESS = 0
ZESTET1_TYPE_TCP = 0
ZESTET1_MAX_INFO = 1
ZESTET1_ILLEGAL_CONNECTION = 32775
ZESTET1_HANDLE = c_void_p
ZESTET1_IMAGE = c_void_p

# Trigger register 
TRIGGER_TRIG_LEVEL_HIGH=c_ushort(0x000)
TRIGGER_TRIG_LEVEL_LOW=c_ushort(0x001)
TRIGGER_PRE_CALC_SAMPLES_HIGH=c_ushort(0x002)
TRIGGER_PRE_CALC_SAMPLES_LOW=c_ushort(0x003)
TRIGGER_POST_CALC_SAMPLES_HIGH=c_ushort(0x004)
TRIGGER_POST_CALC_SAMPLES_LOW=c_ushort(0x005)
TRIGGER_PRE_NET_SAMPLES_HIGH=c_ushort(0x006)
TRIGGER_PRE_NET_SAMPLES_LOW=c_ushort(0x007)
TRIGGER_POST_NET_SAMPLES_HIGH=c_ushort(0x008)
TRIGGER_POST_NET_SAMPLES_LOW=c_ushort(0x009)
TRIGGER_PEAK_THRESHOLD_HIGH=c_ushort(0x00a)
TRIGGER_PEAK_THRESHOLD_LOW=c_ushort(0x00b)
TRIGGER_AREA_THRESHOLD_HIGH=c_ushort(0x00c)
TRIGGER_AREA_THRESHOLD_MED=c_ushort(0x00d)
TRIGGER_AREA_THRESHOLD_LOW=c_ushort(0x00e)
TRIGGER_ENABLE=c_ushort(0x00f)
TRIGGER_TOT_DETECT_THRESHOLD_HIGH=c_ushort(0x010)
TRIGGER_TOT_DETECT_THRESHOLD_LOW=c_ushort(0x011)
# Number of samples in pre window required for trigger
TRIGGER_TOT_TRIG_THRESHOLD_HIGH=c_ushort(0x012) 
TRIGGER_TOT_TRIG_THRESHOLD_LOW=c_ushort(0x013)

TRIGGER_OVERFLOW=c_ushort(0x000)



# Control unit addresses
#CTRL_GPIO_MSB=c_ushort(0x0000)
#CTRL_GPIO_2=c_ushort(0x0001)
#CTRL_GPIO_1=c_ushort(0x0002)
#CTRL_GPIO_LSB=c_ushort(0x0003)
#CTRL_GPIO_nOUTPUT_MSB=c_ushort(0x0004)
#CTRL_GPIO_nOUTPUT_2=c_ushort(0x0005)
#CTRL_GPIO_nOUTPUT_1=c_ushort(0x0006)
#CTRL_GPIO_nOUTPUT_LSB=c_ushort(0x0007)
#
#CTRL_SPI_DATA=c_ushort(0x0010)
#CTRL_SPI_ENABLE=c_ushort(0x0011)
#CTRL_SPI_MUX_LEN=c_ushort(0x0012)
#CTRL_SPI_CLOCK_RATE=c_ushort(0x0013)
#
CTRL_TIME_MSB=c_ushort(0x0020)
CTRL_TIME_2=c_ushort(0x0021)
CTRL_TIME_1=c_ushort(0x0022)
CTRL_TIME_LSB=c_ushort(0x0023)
#
CTRL_FIRMWARE_VERSION=c_ushort(0xffff)
#
CTRL_TIME_RESET=c_ushort(0x0030)


# values for enumeration 'ZESTET1_FPGA_TYPE'
ZESTET1_FPGA_TYPE = c_int # enum
class ZESTET1_CARD_INFO(Structure):
    pass
ZESTET1_CARD_INFO._fields_ = [
    ('IPAddr', c_ubyte * 4),
    ('ControlPort', c_ushort),
    ('Timeout', c_ulong),
    ('HTTPPort', c_ushort),
    ('MACAddr', c_ubyte * 6),
    ('SubNet', c_ubyte * 4),
    ('Gateway', c_ubyte * 4),
    ('SerialNumber', c_ulong),
    ('FPGAType', ZESTET1_FPGA_TYPE),
    ('MemorySize', c_ulong),
    ('FirmwareVersion', c_ulong),
    ('HardwareVersion', c_ulong),
]

# ZESTET1_CONNECTION = c_void_p
ZESTET1_CONNECTION=(c_ubyte * 100)

# values for enumeration 'ZESTET1_CONNECTION_TYPE'
ZESTET1_CONNECTION_TYPE = c_int # enum

# values for enumeration 'ZESTET1_STATUS'
ZESTET1_STATUS = c_int # enum
ZESTET1_ERROR_FUNC = CFUNCTYPE(None, STRING, POINTER(ZESTET1_CARD_INFO), ZESTET1_STATUS, STRING)
ZESTET1_WARNING_BASE = 16384 # Variable c_int '16384'
ZESTET1_INFO_BASE = 0 # Variable c_int '0'
ZESTET1_ERROR_BASE = 32768 # Variable c_int '32768'
__all__ = ['ZESTET1_ILLEGAL_IMAGE_HANDLE', 'ZESTET1_SUCCESS',
           'ZESTET1_XC3S1400A', 'ZESTET1_CARD_INFO',
           'ZESTET1_INFO_BASE', 'ZESTET1_HANDLE',
           'ZESTET1_ERROR_BASE', 'ZESTET1_FPGA_UNKNOWN',
           'ZESTET1_TYPE_TCP', 'ZESTET1_FILE_ERROR',
           'ZESTET1_FPGA_TYPE', 'ZESTET1_MAX_INFO', 'ZESTET1_STATUS',
           'ZESTET1_OUT_OF_MEMORY', 'ZESTET1_CONNECTION_TYPE',
           'ZESTET1_ILLEGAL_FILE', 'ZESTET1_ILLEGAL_CLOCK_RATE',
           'ZESTET1_ILLEGAL_CONNECTION', 'ZESTET1_SOCKET_CLOSED',
           'ZESTET1_TYPE_UDP', 'ZESTET1_CONNECTION', 'ZESTET1_IMAGE',
           'ZESTET1_INVALID_CONNECTION_TYPE', 'ZESTET1_TIMEOUT',
           'ZESTET1_WARNING_BASE', 'ZESTET1_NULL_PARAMETER',
           'ZESTET1_SOCKET_ERROR', 'ZESTET1_INVALID_PART_TYPE',
           'ZESTET1_MAX_ERROR', 'ZESTET1_ILLEGAL_STATUS_CODE',
           'ZESTET1_MAX_WARNING', 'ZESTET1_ERROR_FUNC',
           'ZESTET1_FILE_NOT_FOUND', 'ZESTET1_INTERNAL_ERROR']

# Default GPIO direction (0=write, 1=read)
# 0b11001100110111111101110000110000
GPIO_DIRECTION_DEFAULT=c_ulong(0b11001100110111111101110000110000)
# Default GPIO value (0=low, 1=high)
# 0b11001111111111111111110111110000
GPIO_DEFAULT_FAN_ON=c_ulong(0b11001111111111111111110111110100) # With fan
GPIO_DEFAULT_FAN_OFF=c_ulong(0b11001111111111111111110111110000) # No Fan
# TRIGGER GPIO value (0=low, 1=high)
# 0b1111
#GPIO_TRIGGER=c_ulong(GPIO_DEFAULT.value | 0xf)
# ADC SPI GPIO
# 0b1111111111111111111111011111111
# GPIO_ADC_SPI=c_ulong(GPIO_DEFAULT.value & 0xfffffeff)
# DAC SPI GPIO (SYNC)
# 0b11111111111111111101111111111111
# GPIO_DAC_SPI=c_ulong(GPIO_DEFAULT.value & 0xffffdfff)
# DAC LOAD GPIO (LDAC)
# 0b11111111111111111111111101111111
#GPIO_DAC_LOAD=c_ulong(GPIO_DEFAULT.value & 0xffffff7f)
# TEM1 SPI GPIO
# 0b11111111110111111111111111111111
#GPIO_TEM1_SPI=c_ulong(GPIO_DEFAULT.value & 0xffdfffff)
# TEM2 SPI GPIO
# 0b11111110111111111111111111111111
# GPIO_TEM2_SPI=c_ulong(GPIO_DEFAULT.value & 0xfeffffff )



DAC_PULSE_CHANNELS=[32,33,34,35]
DAC_TRIM_CHANNELS=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
DAC_HV_CHANNELS=[38]
z1lib=CDLL("../Lib/x64/miniips2.so")

class MiniipsLib:
    
    def __init__(self,fpgaId=0):
        # Fan on by default
        self.gpio_default=0b11001111111111111111110111110100
        self.fpgaId=fpgaId

    def set_fpga_id(self,fpgaId):
        self.fpgaId=fpgaId
 
    def example_error_function(self, function_p, cardInfo_p, status, msg):
        if (status ==32777): # Time out
            pass
        elif (status==32769): # Buffer overflow
            pass
        else:
            print ("Function %s returned and error %i \n%s"
                   % (function_p, status, msg))
            exit(1)

    def initialise_error_handler(self, py_error_function):
        """Initialise z1 error handler.
        return value error_handler needs to be saved, or and error
        will cause a segmentation fault"""

        error_handler = ZESTET1_ERROR_FUNC(py_error_function)
        status=z1lib.ZestET1RegisterErrorHandler(error_handler);
        # print "ZestET1RegisterErrorHandler status: ", status
        return error_handler

    def initialise_library(self):
        """Initialise z1 library"""
        status=c_int()
        status=z1lib.ZestET1Init();
        return status

    def find_cards(self):
        """ Request information about the z1 cards attached to
        to system. Wait for 2 seconds (on each interface) for 
        cards to respond to query"""
        nCards=c_ulong()
        cardInfos = POINTER(ZESTET1_CARD_INFO)()
        waitTime=c_ulong(2000)
        print("Searching for ZestET1 cards...");
        #z1lib.ZestET1CountCards.argtypes = [POINTER(c_ulong), 
        #                                    POINTER(POINTER(ZESTET1_CARD_INFO)), c_ulong]
        status=z1lib.ZestET1CountCards(byref(nCards),byref(cardInfos),waitTime)
        return (nCards, cardInfos)

    def get_card_info(self,cardInfo):
        status=z1lib.ZestET1GetCardInfo(byref(cardInfo))

    def configure_from_file(self,cardInfo, filename):
        """ Configure the FPGA directly from a file """
        status=z1lib.ZestET1ConfigureFromFile(byref(cardInfo), filename);
        return status

    def program_flash_from_file(self,cardInfo, filename):
        """ Program the FPGA flas from a file """
        status=z1lib.ZestET1ProgramFlashFromFile(byref(cardInfo), filename);
        return status

    def open_control_connection(self,cardInfo):
        """ Open control connections to user FPGA"""
        connection =POINTER(ZESTET1_CONNECTION)()
        #  connection = ZESTET1_CONNECTION()
        connPort=c_ushort(0x6000)
        connType=ZESTET1_TYPE_TCP
        localPort=c_ushort(0)

        status=z1lib.ZestET1OpenConnection(byref(cardInfo), connType, 
                                           connPort, localPort, byref(connection));
        return connection


    def open_data_connection(self,cardInfo):
        """ Open data connections to user FPGA"""
        connection =POINTER(ZESTET1_CONNECTION)()
        # connection = ZESTET1_CONNECTION()
        connPort=c_ushort(0x6001)
        connType=ZESTET1_TYPE_TCP
        localPort=c_ushort(0)
        status=z1lib.ZestET1OpenConnection(byref(cardInfo), connType, 
                                           connPort, localPort, byref(connection));
        return connection


    def get_firmware_version(self,ctrlConn):
        """Read firmware version"""
        # CTRL_FIRMWARE_VERSION=c_ushort(0xffff)
        # data=c_char_p()
        data=c_ubyte()
        # data = create_string_buffer(100)
        status=z1lib.NetReadControlReg(ctrlConn, CTRL_FIRMWARE_VERSION, byref(data));
        #print repr(data.raw)
        version=str(int(data.value)>>4)+"."+str(int(data.value)&0xf)
        # print("Firmware version : %x.%x" % ((int(data.value)>>4), int(data.value)&0xf));
        return version
        
    def get_time(self,ctrlConn):
        """Read timestamp from the board"""
        time=0
        data=c_ubyte(0)
        z1lib.NetReadControlReg(ctrlConn, CTRL_TIME_MSB, byref(data));
        time = (time|(int(data.value)<<24))
        # print "{0:b}".format(int(data.value))
        # print "{0:b}".format(time)
        z1lib.NetReadControlReg(ctrlConn, CTRL_TIME_2, byref(data));
        time = (time|(int(data.value)<<16))
        # print "{0:b}".format(int(data.value))
        # print "{0:b}".format(time)
        z1lib.NetReadControlReg(ctrlConn, CTRL_TIME_1, byref(data));
        time = (time|(int(data.value)<<8))
        # print "{0:b}".format(int(data.value))
        # print "{0:b}".format(time)
        z1lib.NetReadControlReg(ctrlConn, CTRL_TIME_LSB, byref(data));
        time = (time|int(data.value))
        # print "{0:b}".format(int(data.value))
        # print "{0:b}".format(time)
        # print "Current time: ", time
        # print "Current time in binary: ", "{0:b}".format(time)
        return time
 
    def set_time_to_zero(self,ctrlConn):
        """ Set time to zero"""
        data=c_ubyte(0)
        time=0
        status=z1lib.NetWriteControlReg(ctrlConn, CTRL_TIME_MSB, data)
        status=z1lib.NetWriteControlReg(ctrlConn, CTRL_TIME_2, data)
        status=z1lib.NetWriteControlReg(ctrlConn, CTRL_TIME_1, data)
        status=z1lib.NetWriteControlReg(ctrlConn, CTRL_TIME_LSB, data)
        time=get_time(ctrlConn)
        return time

    def reset_timer(self,ctrlConn):
        """ Reset timer"""
        data=c_ubyte(1)
        status=z1lib.NetWriteControlReg(ctrlConn, CTRL_TIME_RESET, data)
        data=c_ubyte(0)
        status=z1lib.NetWriteControlReg(ctrlConn, CTRL_TIME_RESET, data)
    

    def set_gpio_directions_default(self,ctrlConn):
        "Set GPIO directions (read/write) to default"
        z1lib.NetSetGPIODirection(ctrlConn, GPIO_DIRECTION_DEFAULT) 

    def set_gpios_default_fan_on(self,ctrlConn):
        "Set GPIO values to default (no SPI selected) with fan on"
        # Default GPIO value (0=low, 1=high)
        gpio=c_ulong(0b11001111111111111111110111110100)
        self.gpio_default=gpio.value
        z1lib.NetSetGPIO(ctrlConn, gpio) 

    def set_gpios_default_fan_off(self,ctrlConn):
        "Set GPIO values to default (no SPI selected) with fan off"
        # Default GPIO value (0=low, 1=high)
        gpio=c_ulong(0b11001111111111111111110111110000)
        self.gpio_default=gpio.value
        z1lib.NetSetGPIO(ctrlConn, gpio) 

    def set_gpios_default(self,ctrlConn):
        "Set GPIO values to default (no SPI selected)"
        try:
            gpio=c_ulong(self.gpio_default)
            z1lib.NetSetGPIO(ctrlConn, gpio) 
        except NameError:
            set_gpios_default_fan_on(self,ctrlConn)


    def set_gpios_trigger(self,ctrlConn):
        "Turn calibration pulses on all channels"
        # TRIGGER GPIO value (0=low, 1=high)
        # 0b1111
        gpio=c_ulong(self.gpio_default | 0xf)
        z1lib.NetSetGPIO(ctrlConn, gpio) 

    def set_gpios_dac_spi(self,ctrlConn):
        "DAC SPI GPIO (sync)"
        # 0b11111111111111111101111111111111
        gpio=c_ulong(self.gpio_default & 0xffffdfff)
        z1lib.NetSetGPIO(ctrlConn, gpio) 

    def set_gpios_dac_load(self,ctrlConn):
        "DAC LOAD GPIO (LDAC)"
        # 0b11111111111111111111111101111111
        gpio=c_ulong(self.gpio_default & 0xffffff7f)
        z1lib.NetSetGPIO(ctrlConn, gpio) 

    def set_gpios_tem1_spi(self,ctrlConn):
        "Tem1 SPI"
        # 0b11111111110111111111111111111111
        gpio=c_ulong(self.gpio_default & 0xffdfffff)
        z1lib.NetSetGPIO(ctrlConn, gpio) 

    def set_gpios_tem2_spi(self,ctrlConn):
        "Tem2 SPI"
        # 0b11111110111111111111111111111111
        gpio=c_ulong(self.gpio_default & 0xfeffffff )
        z1lib.NetSetGPIO(ctrlConn, gpio) 

    def set_gpios_adc_spi(self,ctrlConn):
        " ADC SPI GPIO"
        # 0b1111111111111111111111011111111
        gpio=c_ulong(self.gpio_default & 0xfffffeff)
        z1lib.NetSetGPIO(ctrlConn, gpio) 

    def dac_set_data(self, ctrlConn, channel, value):
        writeData=(c_ubyte*3)()
        # Set pulse height
        writeData[0] = int(channel) # Write to A register of channel j
        writeData[1] = 0xC0 | ((value>>8)&0x3f) # Write to input data register (REG1:0 = 11)
        writeData[2] = value
        self.dac_write(ctrlConn,writeData)

    def set_channel_voltage(self,ctrlConn,channelNumber,voltage):
        hvVoltage=self.hv #V
        # 11.05.2017: the slope was changed to match the DAC voltage output (0-5V)
        # after "software reset" was added to the DAC initinalisation
        trimSlope=5.05/16381. # V/trim
        trimVoltage=hvVoltage-voltage
        trimValue=int(round(trimVoltage/trimSlope))
        dacChannel=DAC_CHANNEL_MAP[channelNumber]
        #print "Trim voltage:", trimVoltage
        #print "Trim value:", trimValue

        if (trimValue<1):
            #print "Voltage set above maximum. Using maximum voltage."
            trimValue=1
        if (trimValue>16382):
            #print "Voltage set below minimum. Using minimum voltage."
            trimValue=16382

        # Test for determining the slope
        #trimValue=1
        #2.1 mV
        #trimValue=16382
        #5.01 V

        # Set trim to selected value
        self.dac_set_data(ctrlConn, dacChannel, trimValue)
        time.sleep(0.01)
        self.dac_load(ctrlConn)

        setVoltage=hvVoltage-trimValue*trimSlope
        return setVoltage
    
    def dac_initialise(self,ctrlConn,hv=None,max_hv=79.3,min_hv=32.9):
        """ Initialise DAC"""
        #defaultTrim=int(round((16382.*(0.8+0.5)/5.0)))
        defaultTrim=16383
        defaultCalPulse=16383
        # Modified board (001 and 005)
        #defaultHV=0 # corresponds to 79.3 V

        # Set min and max HV
        self.max_hv=max_hv
        self.min_hv=min_hv

        ## Calculate HV bits from given voltage
        if hv == None:
            self.hv=self.min_hv
        else:
            self.hv=hv

        hv_slope= (self.max_hv-self.min_hv)/16383
        hv_bits=int(round((self.max_hv-self.hv)/hv_slope))

        # Unmodified board
        #defaultHV=0 # corresponds to 67.94 V

        writeData=(c_ubyte*3)()

        # Soft power up
        writeData[0] = 0b0001001 #'\x09'
        writeData[1] = 0x00 #'\x00'
        writeData[2] = 0x00 #'\x00'
        self.dac_write(ctrlConn,writeData)
        time.sleep(0.1)
        self.dac_load(ctrlConn)
        time.sleep(0.1)

        # Software reset
        writeData[0] = 0b0001111 #'\x09'
        writeData[1] = 0x00 #'\x00'
        writeData[2] = 0x00 #'\x00'
        self.dac_write(ctrlConn,writeData)
        time.sleep(0.1)
        self.dac_load(ctrlConn)
        time.sleep(0.1)

        # Set internal reference to 2.50 v (control register)
        writeData[0] = 0b00001100#'\x0c' Control register write
        writeData[1] = 0b00110100 #'\x24'
        writeData[2] = 0x00 #'\x00'
        self.dac_write(ctrlConn,writeData)
        time.sleep(0.1)
        self.dac_load(ctrlConn)
        time.sleep(0.1)

        # Set gain and offset
        for channel in DAC_TRIM_CHANNELS+DAC_PULSE_CHANNELS+DAC_HV_CHANNELS:
            #Set gain to 1
            writeData[0] = channel # Write to A register of channel j
            writeData[1] = 0b01111111       # Set gain to 1
            writeData[2] = 0b11111110
            self.dac_write(ctrlConn,writeData)
            time.sleep(0.02)
            
            # Set offset to 0
            writeData[0] = int(channel) # Write to A register of channel j
            writeData[1] = 0xa0;                  
            writeData[2] = 0x00;
            self.dac_write(ctrlConn,writeData)
            time.sleep(0.02)
        self.dac_load(ctrlConn)

        for channel in DAC_TRIM_CHANNELS:
            self.dac_set_data(ctrlConn, channel, defaultTrim)
            time.sleep(0.02)
        self.dac_load(ctrlConn)
    
        for channel in DAC_PULSE_CHANNELS:
            self.dac_set_data(ctrlConn, channel, defaultCalPulse)
            time.sleep(0.02)
        self.dac_load(ctrlConn)

        for channel in DAC_HV_CHANNELS:
            self.dac_set_data(ctrlConn, channel, hv_bits)
            time.sleep(0.02)
        self.dac_load(ctrlConn)

   
    def dac_write(self,ctrlConn,writeData):
        """Write parameters to dac"""
        clockRate=c_int(2)
        bitWidth=c_int(8)
        length=c_int(len(writeData))
        readData=(c_ubyte*length.value)()
        for id in range(len(writeData)):
            readData[id]=0x00 #'\x00'

        self.set_gpio_directions_default(ctrlConn)
        time.sleep(0.01)
        self.set_gpios_dac_spi(ctrlConn)
        time.sleep(0.01)
        z1lib.NetSPIReadWrite(ctrlConn, clockRate, bitWidth,
                              byref(writeData), byref(readData), length)
        self.set_gpios_default(ctrlConn)

    def dac_load(self,ctrlConn):
        self.set_gpio_directions_default(ctrlConn)
        self.set_gpios_dac_load(ctrlConn)
        time.sleep(0.01)
        self.set_gpios_default(ctrlConn)
        time.sleep(0.01)

    def tem_write(self,ctrlConn,sensorNumber,writeData):
        "write parameters to temperature sensort"
        "writeData must be a string buffer"
        clockRate=c_int(0xf)
        bitWidth=c_int(8)
        length=c_int(len(writeData))
        readData=(c_ubyte*length.value)()
        for id in range(len(writeData)):
            readData[id]=0x00 #'\x00'
        self.set_gpio_directions_default(ctrlConn)
        if sensorNumber==1:
            self.set_gpios_tem1_spi(ctrlConn)
        elif sensorNumber==2:
            self.set_gpios_tem2_spi(ctrlConn)
        else:
            print "Temperature sensor id not recognised"
            return
        z1lib.NetSPIReadWrite(ctrlConn, clockRate, bitWidth,
                              byref(writeData), byref(readData), length)
        self.set_gpios_default(ctrlConn)
        return readData


    def tem_initialise(self,ctrlConn,sensorNumber):
        writeData=(c_ubyte*2)()
        # Reset
        writeData[0] = 0xff #'\xff'
        writeData[1] = 0xff #'\xff'
        self.tem_write(ctrlConn,sensorNumber,writeData)
        # Set operation mode
        writeData[0] = 0x08 #'\x08'
        writeData[1] = 0xC9 #'\xc9'
        self.tem_write(ctrlConn,sensorNumber,writeData)

    def tem_get_chip_id(self,ctrlConn,sensorNumber):
        writeData=(c_ubyte*2)()
        # Get chip id
        writeData[0] = 0x58 #'\x58'
        writeData[1] = 0xff #'\xff'
        readData=self.tem_write(ctrlConn,sensorNumber,writeData)
        return readData

    def tem_get_temperature(self,ctrlConn,sensorNumber):
        writeData=(c_ubyte*3)()
        # Get chip id
        writeData[0] = 0x50
        writeData[1] = 0xff
        writeData[2] = 0xff
        readData=self.tem_write(ctrlConn,sensorNumber,writeData)
        temperatureAdc = (readData[1]<<8) | readData[2]
        temperatureC=temperatureAdc*0.0078 #16 bit readout
        return temperatureC

    def adc_write(self,ctrlConn, writeData, nRead):
        "write parameters to adc"
        "writeData must be a string buffer"
        clockRate=c_int(2)
        bitWidth=c_int(8)
        length=c_int(len(writeData))
        readData=(c_ubyte*length.value)()
        nEnables=(c_ubyte*length.value)()
        #readData=create_string_buffer(len(writeData))
        #nEnables=create_string_buffer(len(writeData))
        for id in range(len(writeData)):
            readData[id]=0x00
            nEnables[id]=0xff
        for id in range(len(writeData)-nRead):
            nEnables[id]=0x00
        self.set_gpio_directions_default(ctrlConn)
        self.set_gpios_adc_spi(ctrlConn)     
        z1lib.NetSDIOReadWrite(ctrlConn, clockRate, bitWidth,
                               byref(nEnables), byref(writeData), 
                               byref(readData), length)
        self.set_gpios_default(ctrlConn)
        return readData

    def adc_initialise(self,ctrlConn):
        #writeData=create_string_buffer(3)
        writeData=(c_ubyte*3)()
        nRead=0
        # Set output mode to offset binary
        writeData[0] = 0x00 # '\x00'
        writeData[1] = 0x14 #'\x14'
        writeData[2] = 0x00 #'\x00' # LVDS-ANSI, non inverted, offset binary
        self.adc_write(ctrlConn, writeData, nRead)

    def adc_get_model(self,ctrlConn):
        #writeData=create_string_buffer(3)
        writeData=(c_ubyte*3)()
        nRead=1
        # Set output mode to offset binary
        writeData[0] = 0x80 #'\x80'
        writeData[1] = 0x01 #'\x01'
        writeData[2] = 0x00 # '\x00'
        model=self.adc_write(ctrlConn, writeData, nRead)
        return model

    def set_trigger_parameters(self, ctrlConn, channel, triggerParameters):
        tr=triggerParameters
        index=ADC_CHANNEL_MAP[channel]
        self.set_trigger_registers(ctrlConn, index,
                                   (TRIGGER_TRIG_LEVEL_HIGH,TRIGGER_TRIG_LEVEL_LOW),
                                   tr.level)
        self.set_trigger_registers(ctrlConn, index,
                                   (TRIGGER_PRE_CALC_SAMPLES_HIGH,TRIGGER_PRE_CALC_SAMPLES_LOW),
                                   tr.calcSamplesPre)
        self.set_trigger_registers(ctrlConn, index,
                                   (TRIGGER_POST_CALC_SAMPLES_HIGH,TRIGGER_POST_CALC_SAMPLES_LOW),
                                   tr.calcSamplesPost)
        self.set_trigger_registers(ctrlConn, index,
                                   (TRIGGER_PRE_NET_SAMPLES_HIGH,TRIGGER_PRE_NET_SAMPLES_LOW),
                                   tr.netSamplesPre)
        self.set_trigger_registers(ctrlConn, index,
                                   (TRIGGER_POST_NET_SAMPLES_HIGH,TRIGGER_POST_NET_SAMPLES_LOW),
                                   tr.netSamplesPost)
        self.set_trigger_registers(ctrlConn, index,
                                   (TRIGGER_PEAK_THRESHOLD_HIGH,TRIGGER_PEAK_THRESHOLD_LOW),
                                   tr.peakThreshold)
        self.set_trigger_registers(ctrlConn, index,
                                   (TRIGGER_AREA_THRESHOLD_HIGH,
                                    TRIGGER_AREA_THRESHOLD_MED,
                                    TRIGGER_AREA_THRESHOLD_LOW),
                                   tr.areaThreshold)
        # Number of samples over the threshold in the calc window 
        # required to set neutron detection flag
        self.set_trigger_registers(ctrlConn, index,
                                   (TRIGGER_TOT_DETECT_THRESHOLD_HIGH,
                                    TRIGGER_TOT_DETECT_THRESHOLD_LOW),
                                   tr.totDetectThreshold)
        # Number of samples over the threshold in the calc pre-window 
        # required to trigger calculation
        self.set_trigger_registers(ctrlConn, index,
                                   (TRIGGER_TOT_TRIG_THRESHOLD_HIGH,TRIGGER_TOT_TRIG_THRESHOLD_LOW),
                                   tr.totTrigThreshold)
        self.set_trigger_registers(ctrlConn, index, 
                                   [TRIGGER_ENABLE],
                                   tr.enabled)

    def enable_trigger(self,ctrlConn, channel):
        index=ADC_CHANNEL_MAP[channel]
        self.set_trigger_registers(ctrlConn, index, 
                                   [TRIGGER_ENABLE],
                                   1)

    def disable_trigger(self,ctrlConn, channel):
        index=ADC_CHANNEL_MAP[channel]
        self.set_trigger_registers(ctrlConn, index, 
                                   [TRIGGER_ENABLE],
                                   0)


    def set_trigger_registers(self,ctrlConn, index, registers, value):
        #print "Value :", value
        nBytes=len(registers)
        for i in range(nBytes):
            register=registers[i]
            bitShift=(nBytes-1-i)*8
            byteValue=(value>>bitShift)&(0xff)
            #print "register, byteValue: ", register.value, byteValue
            status = z1lib.NetWriteTriggerReg(ctrlConn, c_int(index), 
                                              register, c_ubyte(byteValue))
            # print status

    
    def get_trigger_registers(self,ctrlConn, index, registers):
        """NOT WORKING: Trigger parameters cannot be read from register"""
        nBytes=len(registers)
        value=0
        byteValue=(c_ubyte*1)()
        byteValue[0]=10

        for i in range(nBytes):
            register=registers[i]
            bitShift=(nBytes-i-1)*8
            status = z1lib.NetReadTriggerReg(ctrlConn, c_int(index), 
                                             register, byteValue)
            # print "register, byteValue: ", register.value, byteValue[0]
            value=value+(byteValue[0]<<bitShift)

        return value


    def bytesToInt(self,bytes):
        summedValue=0
        nValues=len(bytes)
        for n in range(nValues):
            summedValue+=bytes[n]*math.pow(256,nValues-1-n)
        return summedValue


    def get_data(self,dataConn):
        buffer_size=c_int(1024)
        # data=create_string_buffer(buffer_size.value)
        data=(c_ubyte * buffer_size.value)()
        
        status=0
        channel=None
        storeTime=None
        triggerTime=None
        totValue=None
        peakValue=None
        sumValue=None
        neutronFlag=None
        nPreSamples=None
        nPostSamples=None
        waveform=None
        pedestal=None

        firstChannelId=0
        firstTimeId=1
        firstNPreSamplesId=9
        firstNPostSamplesId=11
        firstWaveformId=13
        
        status=z1lib.NetReadTriggerData(dataConn, byref(data),buffer_size)
        if (status==0):
            try:
                channel=ADC_CHANNEL_MAP.index(data[firstChannelId])
                storeTime=time.time()
                # time=data[0:8]
                # time=bytesToInt(time)
                triggerTime=((data[firstTimeId] << 56) + 
                             (data[firstTimeId+1] << 48) + 
                             (data[firstTimeId+2] << 40) + 
                             (data[firstTimeId+3] << 32) +
                             (data[firstTimeId+4] << 24) + 
                             (data[firstTimeId+5] << 16) +
                             (data[firstTimeId+6] << 8) + 
                             data[firstTimeId+7]
                         ) 
            
                nPreSamples=(data[firstNPreSamplesId] << 8) + data[firstNPreSamplesId+1]
                nPostSamples=(data[firstNPostSamplesId] << 8) + data[firstNPostSamplesId+1]
                nSamples=nPreSamples+nPostSamples
                lastWaveformId=firstWaveformId+nSamples*2-1
                waveformHigh=np.array(data[firstWaveformId:lastWaveformId:2])
                waveformLow=np.array(data[firstWaveformId+1:lastWaveformId+1:2])
                waveform=(waveformHigh<<8)+waveformLow

                pedestal = np.median(waveform)

                # One char neutron flag (0 = no neutron, 1 = neutron)
                neutronFlag=data[lastWaveformId+1]#=(data[12+2*nSamples+1]>>7)
                
                totValue=((data[lastWaveformId+2]<<8)
                          +(data[lastWaveformId+3]))
                
                peakValue=((data[lastWaveformId+4]<<8)
                           +(data[lastWaveformId+5]))
                sumValue=((data[lastWaveformId+6]<<16)
                          +(data[lastWaveformId+7]<<8)
                          +(data[lastWaveformId+8])
                      )
                lastValid=data[lastWaveformId+8]
                outOfRangeValue1=data[lastWaveformId+9]
                outOfRangeValue2=data[lastWaveformId+10]

                #print totValue, peakValue, sumValue
                #print "lastValid :", lastValid
                #print "outOfRangeValue1 :", outOfRangeValue1
                #print "outOfRangeValue2 :", outOfRangeValue2
            except TypeError:
                print "Very weird error happened"
                stautus=10
            
        event=Event(status=status, fpga=self.fpgaId,
                    channel=channel, storeTime=storeTime,
                    time=triggerTime, totValue=totValue,
                    peakValue=peakValue, sumValue=sumValue,
                    neutronFlag=neutronFlag,nPreSamples=nPreSamples, 
                    nPostSamples=nPostSamples, waveform=waveform,
                    pedestal=pedestal)
        return status, event

    def get_data_raw(self,dataConn):
        buffer_size=c_int(1024)
        read_size=c_ulong(0)
        # data=create_string_buffer(buffer_size.value)
        data=(c_ubyte * buffer_size.value)()
        status=z1lib.NetReadTriggerDataRaw(dataConn, byref(data),buffer_size, byref(read_size))
        data_np=np.ndarray( (read_size.value,), dtype= np.uint8, buffer=data)
        return status, data_np
  
    def clean_buffer(self,dataConn):
        buffer_size=c_int(1024)
        # data=create_string_buffer(buffer_size.value)
        data=(c_ubyte * buffer_size.value)()
        status=10
        while (status!=32777):
            status=z1lib.NetReadTriggerData(dataConn, byref(data),buffer_size)

    def close_connection(self,connection):
        """Close connection"""
        status=z1lib.ZestET1CloseConnection(connection)
        return status

    def free_cards(self,cardInfos):
        """Free memory allocated by find_cards"""
        status=z1lib.ZestET1FreeCards(cardInfos)
        return status

    def close_library(self):
        """Close library"""
        status=z1lib.ZestET1Close()
        return status

def main():
    # Initialise z1 error handler
    error_handler=initialise_error_handler(example_error_function)
    # error handler needs to be saved are causes segmentation fault

    initialise_library()
    nCards, cardInfos = find_cards()
    if (int(nCards.value)==0):
        print("No cards found")
        return(0)
    else:
        print("%d cards found" % (nCards.value))
        for nCard in range(int(nCards.value)):
            print("%ld : SerialNum = %ld, IPAddress = %d.%d.%d.%d" %(
                nCard, cardInfos[nCard].SerialNumber,
                cardInfos[nCard].IPAddr[0], cardInfos[nCard].IPAddr[1],
                cardInfos[nCard].IPAddr[2], cardInfos[nCard].IPAddr[3]))


    # Select the first card
    cardInfo=cardInfos[0]

    status= configure_from_file(cardInfo,"../FPGA/miniips2.bit")
    
    time.sleep(1);

    ctrlConn=open_control_connection(cardInfo)
    dataConn=open_data_connection(cardInfo)

    firmwareVersion=get_firmware_version(ctrlConn)
    print "Firmware version :", firmwareVersion

    cardTime=set_time_to_zero(ctrlConn)
    print "Current time: ", cardTime
    time.sleep(0.1)
    cardTime=set_time_to_zero(ctrlConn)
    print "Current time: ", cardTime
    time.sleep(0.1)
    cardTime=set_time_to_zero(ctrlConn)
    print "Current time: ", cardTime

    dac_initialise(ctrlConn)

    # Tem1
    tem_initialise(ctrlConn,1)
    time.sleep(0.1)
    tem1_chip_id=tem_get_chip_id(ctrlConn,1)
    print type(tem1_chip_id)
    print "Temperature chip ID = ",
    print " ".join(hex(n) for n in tem1_chip_id),
    print " (should be 0xc3) "
    tem1=tem_get_temperature(ctrlConn,1)
    print "Temperature 1: ", tem1, "degrees of Celsius"

    # Tem2
    tem_initialise(ctrlConn,2)
    time.sleep(0.1)
    tem2=tem_get_temperature(ctrlConn,2)
    print "Temperature 2: ", tem2, "degrees of Celsius"


    print "Initialise adc"
    adc_initialise(ctrlConn)
    time.sleep(0.5)
    adc_model=adc_get_model(ctrlConn)
    time.sleep(0.5)
    print "ADC model = ",
    print " ".join(hex(n) for n in adc_model),
    print " (should be 0x92) "

    time.sleep(1.0)
    #print "Setting trim"
    #for trim in [0,5000, 10000, 15000]:
    #    print "Trim value:", trim
    #dac_set_data(ctrlConn, 8,int(round((16382.*0.8/5.0))))
    #dac_load(ctrlConn)
    time.sleep(1)
    
    
    # Mid level 8192
    # Trigering to noise 8150
    # Trigger on calibration pulses 8700
    trigger=Trigger(level=12000, calcSamplesPre=5, 
                    calcSamplesPost=12, netSamplesPre=20,
                    netSamplesPost=30, peakThreshold=15000, 
                    areaThreshold=140000, enabled=1)
    triggerNew=Trigger(level=14000, calcSamplesPre=5, 
                    calcSamplesPost=12, netSamplesPre=10,
                    netSamplesPost=40, peakThreshold=15000, 
                    areaThreshold=140000, enabled=1)
    time.sleep(0.1)

    for index in [1,2,3,4,5,6,7,8,9,13,14,15]:
        print "Setting trigger paremeters for channel: ",index
        set_trigger_parameters(ctrlConn, index, trigger)

    for i in range(5):
        status,event=get_data(dataConn)
        print "Channel :", event.channel
        print "Time :", event.time
        print "NPreSamples: ", event.nPreSamples
        print "NPostSamples: ", event.nPostSamples
        print "Neutron flag: ", event.neutronFlag
        # print "Neutron flag: ", "{0:b}".format(event.neutronFlag)
        print "Peak value: ", event.peakValue
        print "Sum value: ", event.sumValue
        print "Waveform: ", event.waveform

    clean_buffer(dataConn)

    for index in [1,2,3,4,5,6,7,8,9,13,14,15]:
        print "Setting trigger paremeters for channel: ",index
        set_trigger_parameters(ctrlConn, index, triggerNew)
        print "Getting trigger parameters"
        triggerOut=get_trigger_parameters(ctrlConn, index)
        print triggerOut

    #for index in [0,1,2,3,4,5,6,7,8,9,10,11]:
    #    trigger=get_trigger_parameters(ctrlConn, index)
    #    print "Channel : ", index
    #    print trigger

 
    # for channel in DAC_TRIM_CHANNELS:
    #    print "Trimming dac channel: ", channel;
    #    dac_set_data(ctrlConn, channel, 0)
    #    time.sleep(0.01)
    #    dac_load(ctrlConn)
    #    try:
    #        a=input("Press Enter to continue...")
    #    except SyntaxError:
    #        pass
    #    dac_set_data(ctrlConn, channel, 16383)
    #    time.sleep(0.01)


    for i in range(2):
        status,event=get_data(dataConn)
        print "Channel :", event.channel
        print "Time :", event.time
        print "NPreSamples: ", event.nPreSamples
        print "NPostSamples: ", event.nPostSamples
        print "Neutron flag: ", event.neutronFlag
        # print "Neutron flag: ", "{0:b}".format(event.neutronFlag)
        print "Peak value: ", event.peakValue
        print "Sum value: ", event.sumValue
        print "Waveform: ", event.waveform
        
    close_connection(ctrlConn)
    close_connection(dataConn)
    # time.sleep(1)
    free_cards(cardInfos)
    close_library()

    print "Connection closed"
    return 0

if __name__ == "__main__":
    main()

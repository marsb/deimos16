#!/usr/bin/python
import miniipsLib as mi

# This file contains the default configuration parameters
# for backpack 2

#Trigger
trigger=mi.Trigger(level=8400, calcSamplesPre=5, 
                   calcSamplesPost=25, netSamplesPre=20,
                   netSamplesPost=280, peakThreshold=8850, 
                   areaThreshold=246600, totTrigThreshold=2,
                   totDetectThreshold=5, enabled=0)
triggers=[trigger, trigger, trigger, trigger, trigger, trigger, trigger, trigger, 
          trigger, trigger, trigger, trigger, trigger, trigger, trigger, trigger]

neutronChannels=[1,2,8,9,10,11,12,13,14,15]
gammaChannels=[0,3,4,7]


# Connection
ip=[192,168,1,100]
timeout=100000
port=20481


#MPPC

serials  = [ 3410, 1152, 1150,  3411,  3412,     0,  0,     3413,
             1153, 1154, 1155,  1156,  1157,  1158,  1159,  1160]
voltages = [67.05,  66.85, 66.87,  67.02, 67.04,  0, 0,    67.01,
            66.88,  66.85,   66.94, 66.97, 66.90, 66.76, 66.67, 66.59]
trim=-1.6

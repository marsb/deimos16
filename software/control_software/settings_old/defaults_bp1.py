#!/usr/bin/python
import miniipsLib as mi

# This file contains the default configuration parameters
# for backpack 1

#Trigger
_trigger0=mi.Trigger(level=12000, calcSamplesPre=5, 
                   calcSamplesPost=25, netSamplesPre=20,
                   netSamplesPost=280, peakThreshold=8850, 
                   areaThreshold=246600, enabled=0)

_trigger1=mi.Trigger(level=12000, calcSamplesPre=5, 
                   calcSamplesPost=25, netSamplesPre=20,
                   netSamplesPost=280, peakThreshold=8850, 
                   areaThreshold=246600, enabled=0)

_trigger2=mi.Trigger(level=12000, calcSamplesPre=5, 
                   calcSamplesPost=25, netSamplesPre=20,
                   netSamplesPost=280, peakThreshold=8850, 
                   areaThreshold=246600, enabled=0)

_trigger3=mi.Trigger(level=12000, calcSamplesPre=5, 
                   calcSamplesPost=25, netSamplesPre=20,
                   netSamplesPost=280, peakThreshold=8850, 
                   areaThreshold=246600, enabled=0)

_trigger4=mi.Trigger(level=8400, calcSamplesPre=5, 
                   calcSamplesPost=25, netSamplesPre=20,
                   netSamplesPost=280, peakThreshold=8850, 
                   areaThreshold=246600, enabled=0)

_trigger5=mi.Trigger(level=8400, calcSamplesPre=5, 
                   calcSamplesPost=25, netSamplesPre=20,
                   netSamplesPost=280, peakThreshold=8850, 
                   areaThreshold=246600, enabled=0)

_trigger6=mi.Trigger(level=12000, calcSamplesPre=5, 
                   calcSamplesPost=25, netSamplesPre=20,
                   netSamplesPost=180, peakThreshold=8850, 
                   areaThreshold=246600, enabled=0)

_trigger7=mi.Trigger(level=8400, calcSamplesPre=5, 
                   calcSamplesPost=25, netSamplesPre=20,
                   netSamplesPost=180, peakThreshold=8850, 
                   areaThreshold=246600, enabled=0)

_trigger8=mi.Trigger(level=8400, calcSamplesPre=5, 
                   calcSamplesPost=25, netSamplesPre=20,
                   netSamplesPost=180, peakThreshold=8850, 
                   areaThreshold=246600, enabled=0)

_trigger9=mi.Trigger(level=8400, calcSamplesPre=5, 
                   calcSamplesPost=25, netSamplesPre=20,
                   netSamplesPost=180, peakThreshold=8850, 
                   areaThreshold=246600, enabled=0)

_trigger10=mi.Trigger(level=8400, calcSamplesPre=5, 
                   calcSamplesPost=25, netSamplesPre=20,
                   netSamplesPost=180, peakThreshold=8850, 
                   areaThreshold=246600, enabled=0)

_trigger11=mi.Trigger(level=8400, calcSamplesPre=5, 
                   calcSamplesPost=25, netSamplesPre=20,
                   netSamplesPost=180, peakThreshold=8850, 
                   areaThreshold=246600, enabled=0)

_trigger12=mi.Trigger(level=8400, calcSamplesPre=5, 
                   calcSamplesPost=25, netSamplesPre=20,
                   netSamplesPost=180, peakThreshold=8850, 
                   areaThreshold=246600, enabled=0)

_trigger13=mi.Trigger(level=8400, calcSamplesPre=5, 
                   calcSamplesPost=25, netSamplesPre=20,
                   netSamplesPost=180, peakThreshold=8850, 
                   areaThreshold=246600, enabled=0)

_trigger14=mi.Trigger(level=8400, calcSamplesPre=5, 
                   calcSamplesPost=25, netSamplesPre=20,
                   netSamplesPost=180, peakThreshold=8850, 
                   areaThreshold=246600, enabled=0)

_trigger15=mi.Trigger(level=8400, calcSamplesPre=5, 
                   calcSamplesPost=25, netSamplesPre=20,
                   netSamplesPost=180, peakThreshold=8850, 
                   areaThreshold=246600, enabled=0)

triggers=[_trigger0, _trigger1, _trigger2, _trigger3, _trigger4, 
          _trigger5, _trigger6, _trigger7, _trigger8, _trigger9,
          _trigger10, _trigger11, _trigger12, _trigger13, _trigger14
          , _trigger15]

neutronChannels=[5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]

gammaChannels=[4]


# Connection
ip=[192,168,1,100]
timeout=100000
port=20481


#MPPC
serials  = [    0,      0,       0,     0,     30008, 1144,  1148,  1147, 1146,   1145,    0,     1143,  1142,  1141,  1140,  1139]

voltages = [  0,     0,     0,     0, 67.17, 66.89, 66.90, 66.95, 66.94, 66.94, 0, 66.83, 66.78, 66.68, 66.61, 66.60]

trim=-1.6

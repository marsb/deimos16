#!/usr/bin/python

import Tkinter as tk
import tkFileDialog
import numpy as np
from array import array
import ROOT

class SaveEvents():
    def __init__(self):
        print "Save events object created"
        self.filename="./tempdata.root"

    
    def set_filename_dialog(self):
        filename = tkFileDialog.asksaveasfilename(defaultextension='.root',
                                                  initialfile='datafile.root')
        if filename:
            self.filename=filename
            return True
        else:
            print "No file given"
            return False

    def set_filename(self,filename):
        self.filename=filename


    def create_event_tree(self,nSamplesMax):
        # Tree for events:
        self.nSamplesMax=nSamplesMax
        self.rFile = ROOT.TFile(self.filename, "recreate")
        self.eventTree = ROOT.TTree("events", "Event data")

        # create 1 dimensional float arrays
        self.time = np.zeros(1, dtype=np.uint64)
        self.storeTime = np.zeros(1, dtype=np.uint64)
        self.fpga = np.zeros(1, dtype=np.uint32)
        self.channel = np.zeros(1, dtype=np.uint32)
        self.totValue= np.zeros(1, dtype=np.uint32)
        self.peakValue= np.zeros(1, dtype=np.uint32)
        self.sumValue= np.zeros(1, dtype=np.uint32)
        self.neutronFlag= np.zeros(1, dtype=np.uint32)
        self.nSamples= np.zeros(1, dtype=np.uint32)
        self.nPreSamples= np.zeros(1, dtype=np.uint32)
        self.nPostSamples= np.zeros(1, dtype=np.uint32)
        self.waveform=np.zeros(self.nSamplesMax,dtype=np.uint32)
        # self.waveform=array('L',self.nSamplesMax*[0])
        # self.waveform= n.zeros(1, dtype=int)
        #self.waveform=ROOT.std.vector(ROOT.std.int)()

        self.eventTree.Branch('time',self.time,'time/l')
        self.eventTree.Branch('storeTime',self.storeTime,'storeTime/l')
        self.eventTree.Branch('channel',self.channel,'channel/i')
        self.eventTree.Branch('fpga',self.fpga,'fpga/i')
        self.eventTree.Branch('totValue',self.totValue,'totValue/i')
        self.eventTree.Branch('peakValue',self.peakValue,'peakValue/i')
        self.eventTree.Branch('sumValue',self.sumValue,'sumValue/i')
        self.eventTree.Branch('neutronFlag',self.neutronFlag,'neutronFlag/i')
        self.eventTree.Branch('nSamples',self.nSamples,'nSamples/i')
        self.eventTree.Branch('nPreSamples',self.nPreSamples,'nPreSamples/i')
        self.eventTree.Branch('nPostSamples',self.nPostSamples,'nPostSamples/i')
        self.eventTree.Branch('waveform',self.waveform,'waveform[nSamples]/i')
        # self.eventTree.Branch('waveform',self.waveform)

        print "New ROOT event tree created"

    def create_settings_tree(self):
        self.settingsTree = ROOT.TTree("settings", "DAQ settings")

        # create 1 dimensional float arrays
        self.chset_applyTime = np.zeros(1, dtype=np.uint64)
        self.chset_fpgaId = np.zeros(1, dtype=np.uint32)
        self.chset_chId = np.zeros(1, dtype=np.uint32)
        self.chset_sipmId = np.zeros(1, dtype=np.uint32)
        self.chset_type = np.zeros(1, dtype=np.uint32)
        self.chset_pedestal = np.zeros(1, dtype=np.uint32)
        self.chset_th = np.zeros(1, dtype=np.uint32)
        self.chset_wfPre = np.zeros(1, dtype=np.uint32)
        self.chset_wfPost = np.zeros(1, dtype=np.uint32)
        self.chset_calcPre = np.zeros(1, dtype=np.uint32)
        self.chset_calcPost = np.zeros(1, dtype=np.uint32)
        self.chset_totMin = np.zeros(1, dtype=np.uint32)
        self.chset_idAreaMin = np.zeros(1, dtype=np.uint32)
        self.chset_idPeakMax = np.zeros(1, dtype=np.uint32)
        self.chset_idTotMin = np.zeros(1, dtype=np.uint32)
        self.chset_nomV = np.zeros(1, dtype=np.float32)
        self.chset_trimV = np.zeros(1, dtype=np.float32)
        self.chset_temV = np.zeros(1, dtype=np.float32)
        self.chset_setV = np.zeros(1, dtype=np.float32)
        self.chset_tem = np.zeros(1, dtype=np.float32)
        self.chset_x = np.zeros(1, dtype=np.int32)
        self.chset_y = np.zeros(1, dtype=np.int32)
        self.chset_z = np.zeros(1, dtype=np.int32)

        self.settingsTree.Branch('applyTime',self.chset_applyTime,'applyTime/l')
        self.settingsTree.Branch('fpgaId',self.chset_fpgaId,'fpgaId/i')
        self.settingsTree.Branch('chId',self.chset_chId,'chId/i')
        self.settingsTree.Branch('sipmId',self.chset_sipmId,'sipmId/i')
        self.settingsTree.Branch('type',self.chset_type,'type/i')
        self.settingsTree.Branch('pedestal',self.chset_pedestal,'pedestal/i')
        self.settingsTree.Branch('th',self.chset_th,'th/i')
        self.settingsTree.Branch('wfPre',self.chset_wfPre,'wfPre/i')
        self.settingsTree.Branch('wfPost',self.chset_wfPost,'wfPost/i')
        self.settingsTree.Branch('calcPre',self.chset_calcPre,'calcPre/i')
        self.settingsTree.Branch('calcPost',self.chset_calcPost,'calcPost/i')
        self.settingsTree.Branch('totMin',self.chset_totMin,'totMin/i')
        self.settingsTree.Branch('idAreaMin',self.chset_idAreaMin,'idAreaMin/i')
        self.settingsTree.Branch('idPeakMax',self.chset_idPeakMax,'idPeakMax/i')
        self.settingsTree.Branch('idTotMin',self.chset_idTotMin,'idTotMin/i')
        self.settingsTree.Branch('nomV',self.chset_nomV,'nomV/F')
        self.settingsTree.Branch('trimV',self.chset_trimV,'trimV/F')
        self.settingsTree.Branch('temV',self.chset_temV,'temV/F')
        self.settingsTree.Branch('setV',self.chset_setV,'setV/F')
        self.settingsTree.Branch('tem',self.chset_tem,'tem/F')
        self.settingsTree.Branch('x',self.chset_x,'x/I')
        self.settingsTree.Branch('y',self.chset_y,'y/I')
        self.settingsTree.Branch('z',self.chset_z,'z/I')

        print "New ROOT settings tree created"

    def add_events(self,events):
        for event in events:
            self.time[0]=long(event.time)
            self.storeTime[0]=long(event.storeTime)
            self.fpga[0]=int(event.fpga)
            self.channel[0]=int(event.channel)
            self.totValue[0]=int(event.totValue)
            self.peakValue[0]=int(event.peakValue)
            self.sumValue[0]=int(event.sumValue)
            self.neutronFlag[0]=int(event.neutronFlag)
            self.nSamples[0]=int(event.nPreSamples+event.nPostSamples)
            self.nPreSamples[0]=int(event.nPreSamples)
            self.nPostSamples[0]=int(event.nPostSamples)
            self.waveform[:]=0 # Remove old values
            self.waveform[:len(event.waveform)]=event.waveform
            self.eventTree.Fill()

    def add_settings(self,daqSettings):
        for fpga in daqSettings.fpgas:
            for ch in fpga.chs:
                self.chset_applyTime[0]=long(daqSettings.applyTime)
                self.chset_fpgaId[0]=int(fpga.id)
                self.chset_chId[0] = int(ch.id)
                self.chset_sipmId[0] = int(ch.sipmId)
                self.chset_type[0] = int(ch.type)
                self.chset_pedestal[0] = int(ch.pedestal)
                self.chset_th[0] = int(ch.th)
                self.chset_wfPre[0] = int(ch.wfPre)
                self.chset_wfPost[0] = int(ch.wfPost)
                self.chset_calcPre[0] = int(ch.calcPre)
                self.chset_calcPost[0] = int(ch.calcPost)
                self.chset_totMin[0] = int(ch.totMin)
                self.chset_idAreaMin[0] = int(ch.idAreaMin)
                self.chset_idPeakMax[0] = int(ch.idPeakMax)
                self.chset_idTotMin[0] = int(ch.idTotMin)
                self.chset_nomV[0] = float(ch.nomV)
                self.chset_trimV[0] = float(ch.trimV)
                self.chset_temV[0] = float(ch.temV)
                self.chset_setV[0] = float(ch.setV)
                self.chset_x[0] = int(ch.x)
                self.chset_y[0] = int(ch.y)
                self.chset_z[0] = int(ch.z)
                if fpga.sipmTem is None:
                    self.chset_tem[0] = float(-100.0)
                else:
                    self.chset_tem[0] = float(fpga.sipmTem)

                self.settingsTree.Fill()

    def write_root_file(self):
        # write the tree into the output file
        self.rFile.Write()
        print "Trees saved to the ROOT file"

    def close_root_file(self):
        # close root file
        self.rFile.Close()
        print "ROOT file closed"

def main():
    saveEvents=SaveEvents()
    saveEvents.set_filename_dialog()
    saveEvents.create_root_trees(50)
    for i in range(20):
        event=mi.Event(status=0, time=i, storeTime=i, 
                       totValue=3, peakValue=10, 
                       sumValue=30, neutronFlag=0, 
                       nPreSamples=2, nPostSamples=6, 
                       waveform=np.array([40,50,60,70,80,90,80,70]))
        saveEvents.add_event(event)
    saveEvents.write_root_file()
    saveEvents.close_root_file()

    # Read the file generated
    f=ROOT.TFile("datafile.root")
    f.ls()

    t=f.Get("events")
    t.Print()
    
    nEvent=0
    for event in t:
        nEvent+=1
        print "Event: ", nEvent, "Time: ", event.time, "nPost: ", event.nPostSamples
        print "Waveform: ", np.array(event.waveform)

if __name__ == "__main__":
    main()

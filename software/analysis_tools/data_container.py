import numpy as np
import ROOT
from argparse import ArgumentParser
 
############################ DATA CONTAINER CLASS ##############################
class Data_Container:

    def __init__(self,root_input_file_name,event_min,event_max):
        ### Data from event tree
        self.nEvents = 0
        self.wfs, self.amps, self.tots  = np.array([]), np.array([]), np.array([])
        self.peds, self.nflags = np.array([]), np.array([])
        self.fpgas, self.chs = np.array([]), np.array([])
        self.times, self.storeTimes = np.array([]), np.array([])
        ### Data calculated based on event tree
        self.cgroups = np.array([])
        self.ncoin = np.array([])
        self.ccolors = np.array([]) 
        
        ### Data from settings tree
        self.settings_peds={}
        self.settings_types={}
        self.settings_xyz={}

        ### Data fulfilling the selection criteria
        # will be later initialised to all data
        self.nEvents_s = 0
        self.inds_s = np.array([])
        self.wfs_s, self.amps_s, self.tots_s  = np.array([]), np.array([]), np.array([])
        self.peds_s, self.nflags_s = np.array([]), np.array([])
        self.fpgas_s, self.chs_s = np.array([]), np.array([])
        self.times_s, self.storeTimes_s = np.array([]), np.array([])
        self.cgroups_s = np.array([])
        self.ncoin_s = np.array([])
        self.ccolors_s = np.array([])
        
        ### Connect to root file
        file_in=ROOT.TFile(root_input_file_name)
        # Get events tree
        events=file_in.Get("events")
        events.GetEntry(0)
        # Get settings tree
        settings=file_in.Get("settings")
        settings.GetEntry(0)

        ### Get channel pedestals and type from settings tree
        print "Reading settings data from root tree"
        for settingId in range(settings.GetEntries()):
            settings.GetEntry(settingId)
            self.settings_peds[(settings.fpgaId,settings.chId)]=settings.pedestal
            self.settings_types[(settings.fpgaId,settings.chId)]=settings.type
            try:
                self.settings_xyz[(settings.fpgaId,settings.chId)] = (settings.x,
                                                                      settings.y,
                                                                      settings.z)
            except AttributeError:
                self.settings_xyz[(settings.fpgaId,
                                   settings.chId)] = self.get_nemenix_xyz(settings.fpgaId,
                                                                          settings.chId)
                        
        ### Read data from events tree to python listst
        fpgas, chs, peds, amps, tots = [], [], [], [], []
        nflags, wfs, times, storeTimes = [], [], [], []
        events_id_range = range(event_min, min(events.GetEntries(),event_max))
        for event_id in events_id_range:

            if event_id%100000==0:
                print "Reading root event:", event_id
            events.GetEntry(event_id)
            fpga = events.fpga
            ch = events.channel
            amp = events.peakValue
            tot = events.totValue
            nflag = events.neutronFlag
            wf = np.array(events.waveform)
            time = events.time
            storeTime = events.storeTime
            
            chped = self.settings_peds[(fpga,ch)]
            chtype = self.settings_types[(fpga,ch)]
            ### Accept only neutron channels
            if chtype != 1:
                continue
            ### This part removes events with shift in baseline 
            wf_sort=np.sort(wf-chped)
            wf_min_mean=np.mean(wf_sort[:int(round(len(wf_sort)/5.))])
            if wf_min_mean>0:
                continue
            fpgas.append(fpga)
            chs.append(ch)
            amps.append(amp)
            tots.append(tot)
            peds.append(chped)
            nflags.append(nflag)
            wfs.append(wf)
            times.append(time)
            storeTimes.append(storeTime)
            self.nEvents+=1
        ### Convert lists to numpy arrays
        print "Converting data to numpy array"
        self.fpgas = np.array(fpgas)
        self.chs = np.array(chs)
        self.wfs = np.array(wfs)
        self.amps = np.array(amps)
        self.tots = np.array(tots)
        self.peds = np.array(peds)
        self.nflags = np.array(nflags)
        self.times = np.array(times)
        self.storeTimes = np.array(storeTimes)

        self.time_adc_s=1.*(storeTimes[-1]-storeTimes[0])/(times[-1]-times[0])

        self.amps_no_ped_min = np.min(self.amps-self.peds)
        self.amps_no_ped_max = np.max(self.amps-self.peds)
        self.tots_min = np.min(self.tots)
        self.tots_max = np.max(self.tots)

        self.amps_no_ped_bins = int(round((self.amps_no_ped_max-self.amps_no_ped_min)/60.))
        self.tots_bins=int(round(self.tots_max-self.tots_min))
        
        self.durationS=max(0.01,np.max(self.storeTimes)-np.min(self.storeTimes))

        self.deltaT_range_s=(np.min(self.times[1:]-self.times[:-1])*self.time_adc_s,
                           np.max(self.times[1:]-self.times[:-1])*self.time_adc_s)


        # This is not the ADC sampling rate but clock for trigger time stamps
        # (100 MHz). The ADC sampling rate is 30 MHz.
        #print "ADC sampling rate:", 1.0e-6/self.time_adc_s, "MHz"

        ### Sort events based on the FPGA time stamp
        print "Sorting events based on the time stamp"
        sorting = self.times.argsort()
        self.wfs = self.wfs[sorting]
        self.amps = self.amps[sorting]
        self.tots  = self.tots[sorting]
        self.peds = self.peds[sorting]
        self.nflags = self.nflags[sorting]
        self.fpgas = self.fpgas[sorting]
        self.chs = self.chs[sorting]
        self.times = self.times[sorting]
        self.storeTimes = self.storeTimes[sorting]

        ### Group in couincidence groups and count
        ### number of triggers that are in coindicende
        ### fills: cgroups, ncoin
        print "Finding coincidences"
        self.find_coincidences(100)
        print "Data read completed"
        print "Total number of events:", self.nEvents

        ### Initialise selected to all
        self.select()
        print "Done"

    def select(self,inds=[]):
        ### Data fulfilling the selection criteria
        if len(inds) == 0:
            inds = range(self.nEvents)
        else:
            inds.sort() # events should be keept sorted
        self.inds_s = np.array(inds)
        self.nEvents_s = len(inds)
        self.wfs_s = self.wfs[inds]
        self.amps_s = self.amps[inds]
        self.tots_s = self.tots[inds]
        self.peds_s = self.peds[inds]
        self.nflags_s = self.nflags[inds]
        self.fpgas_s = self.fpgas[inds]
        self.chs_s = self.chs[inds]
        self.times_s = self.times[inds]
        self.storeTimes_s = self.storeTimes[inds]
        self.cgroups_s = self.cgroups[inds]
        self.ncoin_s = self.ncoin[inds]
        self.ccolors_s = self.ccolors[inds]

    def find_coincidences(self,deltaSamples):
        """ Find events in coincidence based on the FPGA trigger time stamp"""
        ### Create coincidecen groups
        self.cgroups=np.zeros(self.nEvents)
        idGroup=0
        timePrev=self.times[0]
        #deltaSamples=int(round(deltaT/self.time_adc_s))
        print "Find coincidence groups"
        for i in range(self.nEvents):
            if ((self.times[i]-timePrev)>deltaSamples):
                idGroup+=1
            self.cgroups[i]=idGroup
            timePrev=self.times[i]

        print "Count number of coincidences"
        self.ncoin=np.zeros(self.nEvents)
        cgroup_prev=self.cgroups[0]
        ncoin=0
        for i in range(self.nEvents):
            if (self.cgroups[i] == cgroup_prev):
                ncoin+=1
            else:
                cgroup_prev=self.cgroups[i]
                self.ncoin[i-ncoin:i]=ncoin
                ncoin=1
            
        ### Count number of coincidecens for each trigger. 1 = no other triggers 
        #cgroups_unique, n_unique=np.unique(self.cgroups, return_counts=True)
        #self.ncoin=np.ones(self.nEvents) # Initialise to one
        #cgroups_over_one=cgroups_unique[n_unique>1]
        #n_over_one=n_unique[n_unique>1]
        #print "Count number of coincidences"
        #for i in range(len(cgroups_over_one)):
        #    cgroup = cgroups_over_one[i]
        #    n = n_over_one[i]
        #    self.ncoin[self.cgroups==cgroup] = n

        ### set color coding based in ncoin
        self.ccolors=np.array(self.nEvents*["green"]) # Initialise to green
        self.ccolors[self.ncoin==2]="red"
        self.ccolors[self.ncoin>2]="blue"

    def get_cube_nevents(self):
        cube_nevents={}
        ### Initialize to zero
        # Find x, y and z numbers used
        xs=[]
        ys=[]
        zs=[]
        for id in self.settings_xyz:
            if self.settings_xyz[id][0] > -1:
                xs.append(self.settings_xyz[id][0])
            if self.settings_xyz[id][1] > -1:
                ys.append(self.settings_xyz[id][1])
            if self.settings_xyz[id][2] > -1:
                zs.append(self.settings_xyz[id][2])
        xs=np.unique(xs)
        ys=np.unique(ys)
        zs=np.unique(zs)
        for z in zs:
            for y in ys:
                for x in xs:
                    cube_nevents[(x,y,z)]=0

        cgroup_prev=self.cgroups[0]
        #ncoin=0
        selected=True
        x=-1
        y=-1
        z=-1
        j=0
        #print self.inds_s[0:20]
        for i in range(self.nEvents):
            xyz=self.settings_xyz[(self.fpgas[i],self.chs[i])]
            ### Event in the same coincidence group as the previous
            if (self.cgroups[i] == cgroup_prev):
                #ncoin+=1
                x=max(x,xyz[0])
                y=max(y,xyz[1])
                z=max(z,xyz[2])
            ### Event is not in the same coincidence group
            else:
                cgroup_prev=self.cgroups[i]
                if (selected==True):
                    if (x>=0 and y>=0 and z>=0):
                        try:
                            cube_nevents[(x,y,z)]+=1
                        except KeyError:
                            cube_nevents[(x,y,z)]=1
                #ncoin=1
                x=xyz[0]
                y=xyz[1]
                z=xyz[2]
                selected = False

            if i == self.inds_s[j]: ## Check if the event has been selected
                selected=True
                j=min(j+1,len(self.inds_s)-1)

        """
        for cgroup in cgroups:
            inds,=np.where(self.cgroups == cgroup)
            inds_sorter=self.amps[inds].argsort()
            ind1=inds[inds_sorter[-1]]
            ind2=inds[inds_sorter[-2]]
            xyz1=self.settings_xyz[(self.fpgas[ind1],self.chs[ind1])]
            xyz2=self.settings_xyz[(self.fpgas[ind2],self.chs[ind2])]
            ### Require z1 == z2 (or match in any other dimension)
            if ((xyz1[0]==xyz2[0]) or (xyz1[1]==xyz2[1]) or (xyz1[2]==xyz2[2])):
                x=max(xyz1[0],xyz2[0])
                y=max(xyz1[1],xyz2[1])
                z=max(xyz1[2],xyz2[2])
                if (x>=0 and y>=0 and z>=0): # Removes x1=-1, x2=-1 events
                    try:
                        cube_nevents[(x,y,z)]+=1
                    except KeyError:
                        cube_nevents[(x,y,z)]=1
        """
        return cube_nevents
        
            
    def get_amp_hist(self):
        amps_no_peds_hist, amps_no_peds_edges = np.histogram(self.amps_s-self.peds_s,
                                                             bins=int(round(self.amps_no_ped_bins)),
                                                             range=(self.amps_no_ped_min,
                                                                    self.amps_no_ped_max))
        return amps_no_peds_hist, amps_no_peds_edges, 

    def get_tot_hist(self):
        tots_hist, tots_edges = np.histogram(self.tots_s,
                                             bins=self.tots_bins,
                                             range=(self.tots_min, self.tots_max))
        return tots_hist, tots_edges

    def get_amp_tot_hist2d(self):
        hist2d, amps_no_peds_edges, tots_edges= np.histogram2d(self.amps_s-self.peds_s,
                                                               self.tots_s,
                                                               bins=[self.amps_no_ped_bins,
                                                                     self.tots_bins],
                                                               range=((self.amps_no_ped_min,
                                                                       self.amps_no_ped_max),
                                                                      (self.tots_min,
                                                                       self.tots_max)))
        return hist2d, amps_no_peds_edges, tots_edges


    def get_ch_amp_hist2d(self):
        chids=self.chs_s+(np.max(self.chs_s)+1)*self.fpgas_s
        chid_min=np.min(chids)
        chid_max=np.max(chids)
        chids_bins=np.linspace(chid_min, chid_max+1,chid_max-chid_min+2)
        amps_no_ped_bins=int(round((self.amps_no_ped_max-self.amps_no_ped_min)/200.))
        hist2d, chids_edges, amps_no_peds_edges = np.histogram2d(chids,
                                                                 self.amps_s-self.peds_s,
                                                                 bins=[chids_bins,
                                                                       amps_no_ped_bins],
                                                                 range=((chid_min,
                                                                         chid_max),
                                                                        (self.amps_no_ped_min,
                                                                         self.amps_no_ped_max))
        )
        return hist2d, chids_edges, amps_no_peds_edges

    def get_ch_tot_hist2d(self):
        chids=self.chs_s+(np.max(self.chs_s)+1)*self.fpgas_s
        chid_min=np.min(chids)
        chid_max=np.max(chids)
        chids_bins=np.linspace(chid_min, chid_max+1,chid_max-chid_min+2)
        hist2d, chids_edges, tots_edges = np.histogram2d(chids,
                                                         self.tots_s,
                                                         bins=[chids_bins,
                                                               self.tots_bins],
                                                         range=((chid_min,
                                                                 chid_max),
                                                                (self.tots_min,
                                                                 self.tots_max))
        )
        return hist2d, chids_edges, tots_edges

    def get_deltaT_hist(self):
        deltaT=self.times_s[1:]-self.times_s[:-1]

        deltaT_hist, deltaT_edges = np.histogram(deltaT*self.time_adc_s, bins=200,
                                                 range=self.deltaT_range_s)
        return deltaT_hist, deltaT_edges
    
    def get_ch_data(self):
        ### Get unique fpga and ch combos and sort them
        fpga_chs = []
        for fpga_ch in self.settings_types:
            if self.settings_types[fpga_ch]==1:
                fpga_chs.append(fpga_ch)
        fpga_chs = sorted(fpga_chs, key = lambda x: (x[0], x[1])) # sort based on fpga and ch

        ch_data={"fpga":[], "ch":[],"triggers":[],"neutrons":[],
                 "singles":[],"doubles":[],"more":[]}

        allTriggers=0
        allNeutrons=0
        allSingles=0
        allDoubles=0
        allMore=0
                                            
        for fpga_ch in fpga_chs:
            ch_data["fpga"].append(fpga_ch[0])
            ch_data["ch"].append(fpga_ch[1])
            right_fpga_s=set(np.where(self.fpgas_s==fpga_ch[0])[0])
            right_ch_s=set(np.where(self.chs_s==fpga_ch[1])[0])
            right_fpga_ch_s=right_fpga_s.intersection(right_ch_s)            
            right_type_s=set(np.where(self.nflags_s==1)[0])
                        
            ch_data["triggers"].append(len(right_fpga_ch_s))
            ch_data["neutrons"].append(len(right_fpga_ch_s.intersection(right_type_s)))
            ### Notice, the other trigger does not need to be in the selection
            ch_data["singles"].append(
                np.count_nonzero(
                    self.ncoin_s[list(right_fpga_ch_s)] == 1))
            ch_data["doubles"].append(
                np.count_nonzero(
                    self.ncoin_s[list(right_fpga_ch_s)] == 2))
            ch_data["more"].append(
                np.count_nonzero(
                    self.ncoin_s[list(right_fpga_ch_s)] > 2))
                                   
            allTriggers+=ch_data["triggers"][-1]
            allNeutrons+=ch_data["neutrons"][-1]
            allSingles+=ch_data["singles"][-1]
            allDoubles+=ch_data["doubles"][-1]
            allMore+=ch_data["more"][-1]

        ### Write sum to the table
        ch_data["fpga"].append("any")
        ch_data["ch"].append("any")
        ch_data["triggers"].append(allTriggers)
        ch_data["neutrons"].append(allNeutrons)
        ch_data["singles"].append(allSingles)
        ch_data["doubles"].append(allDoubles)
        ch_data["more"].append(allMore)

        return ch_data

    def get_nemenix_xyz(self,fpgaId,chId):
        if (fpgaId == 0): # Board on the top (master)
            x = -1
            y = 3-chId%4
            z = chId/4 # 0 = bottom layer
            return (x,y,z)
        if (fpgaId == 1): # Board on the bottom (slave)
            x = chId%4
            y = -1
            z = 3-chId/4 # 0 = bottom layer
            return (x,y,z)

        
def main():
    parser = ArgumentParser(description="Print number of events per cube")
    parser.add_argument("input_file_name",metavar="input_file_name",
                        help="input root file")
    parser.add_argument("events_max",metavar="events_max", type=int,
                        help="Number of events to be read from the file")
    parser.add_argument("tot_min",metavar="tot_min", type=int,
                        help="Minumum ToT value for accepted events")
    args = parser.parse_args()
    data=Data_Container(args.input_file_name,0,args.events_max)
    inds_selected=np.nonzero(data.tots>=args.tot_min)[0]
    print "Inds:", inds_selected
    data.select(inds_selected)
    cube_nevents=data.get_cube_nevents()
    print "Printing number of events (n) per cube"
    print "x y z n"
    for xyz in cube_nevents:
        print xyz[0],xyz[1],xyz[2],cube_nevents[xyz]

if __name__ == "__main__":
    main()


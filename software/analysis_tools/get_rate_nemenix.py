import numpy as np
import ROOT 
from argparse import ArgumentParser
import matplotlib.pyplot as plt
from data_container import Data_Container

parser = ArgumentParser(description='Calcula neutron rate')
parser.add_argument("--tot_min", help="Minimum ToT value (default 40)", type=int, default=40)
parser.add_argument("--amp_max", help="Max Amp value (default 5000)", type=int, default=5000)
parser.add_argument("--nevents", help="Number of events (default 5E+05)", type=int, default=200000)
parser.add_argument("--scaler", help="Scale rate to efficiency (default 1.0)", type=float, default=1.0)
parser.add_argument('--sim_file', metavar='simulated_rate_xyz.npy', type=str, 
                    help='Simulation data file')
parser.add_argument('input_file_name', metavar='input.root', type=str, help='Input root files')

args=parser.parse_args()


print "File", args.input_file_name
data=Data_Container(args.input_file_name,0,args.nevents)
inds_selected=np.nonzero((data.tots>=args.tot_min)*((data.amps-data.peds)<=args.amp_max))[0]
print "Inds:", inds_selected
data.select(inds_selected)
cube_nevents=data.get_cube_nevents()
n=0
n_xyz=np.zeros((6,6,6)) # Zero bins at both ends
for xyz in cube_nevents:
    n+=cube_nevents[xyz]
    n_xyz[xyz[0]+1,xyz[1]+1,xyz[2]+1]=cube_nevents[xyz]
duration=1.*data.durationS
nrate=n/duration
print n_xyz
        
print "Neutrons:", n, "Duration:", "{0:.1f}".format(duration), "Rate:", "{0:.4f}".format(nrate)

n_xyz=n_xyz/duration/args.scaler

np.save("measured_rate_xyz.npy",n_xyz)

if (args.sim_file):
    sim_xyz=np.load(args.sim_file)
    # Invert x axis in sim data (different direction than in measurement)
    sim_xyz=np.flip(sim_xyz,0)
    
x=[-1,0,1,2,3,4]
### X
plt.figure("Neutron rate x")
y=n_xyz.sum((1,2))
plt.step(x, y, where="mid", label="Meas")
if (args.sim_file):
    plt.step(x,sim_xyz.sum((1,2)), where="mid", label="Sim")
    plt.legend()
    plt.ylim((0,1.1*max(np.max(y),np.max(sim_xyz.sum((1,2))))))
else:
    plt.ylim((0,np.max(y)*1.1))
plt.xlim((-0.5,3.5))
plt.xlabel("X (cubes)")
plt.xticks([0,1,2,3])
plt.ylabel("Efficiency")
plt.tight_layout()
plt.savefig("neutronrate_x.pdf")
### Y
plt.figure("Neutron rate y")
y=n_xyz.sum((0,2))
plt.step(x, y, where="mid", label="Meas")
if (args.sim_file):
    plt.step(x,sim_xyz.sum((0,2)), where="mid", label="Sim")
    plt.ylim((0,1.1*max(np.max(y),np.max(sim_xyz.sum((0,2))))))
    plt.legend()
else:
    plt.ylim((0,np.max(y)*1.1))
plt.xlim((-0.5,3.5))
plt.xlabel("Y (cubes)")
plt.xticks([0,1,2,3])
plt.ylabel("Efficiency")
plt.tight_layout()
plt.savefig("neutronrate_y.pdf")
### Z
plt.figure("Neutron rate z")
y=n_xyz.sum((0,1))
plt.step(x, y, where="mid", label="Meas")
if (args.sim_file):
    plt.step(x,sim_xyz.sum((0,1)), where="mid", label="Sim")
    plt.ylim((0,1.1*max(np.max(y),np.max(sim_xyz.sum((0,1))))))
    plt.legend()
else:
    plt.ylim((0,np.max(y)*1.1))
plt.xlim((-0.5,3.5))
plt.xlabel("Z (cubes)")
plt.xticks([0,1,2,3])
plt.ylabel("Efficiency")
plt.tight_layout()
plt.savefig("neutronrate_z.pdf")

### Rate per cube
plt.figure("Neutron rate all")
x=range(4*4*4)
y=n_xyz[1:5,1:5,1:5]
y=y.flatten()
plt.step(x, y, where="mid", label="Meas")
if (args.sim_file):
    y_sim=sim_xyz[1:5,1:5,1:5]
    y_sim=y_sim.flatten()
    plt.step(x,y_sim, where="mid", label="Sim")
    plt.ylim((0,1.1*max(np.max(y),np.max(y_sim))))
    plt.legend()
else:
    plt.ylim((0,np.max(y)*1.1))
plt.xlim((-0.5,63.5))
plt.xlabel("Cube id")
#plt.xticks([0,1,2,3])
plt.ylabel("Efficiency")
plt.tight_layout()
plt.savefig("neutronrate_cube.pdf")
plt.show()

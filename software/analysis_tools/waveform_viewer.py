''' 
Run the program with command:
bokeh serve --show waveform_viewer.py --args input_data.root 500000

This takes a few seconds, and is ready when a message like this is printed:

2017-01-20 12:09:24,703 Starting Bokeh server with process id: 7042

This starts a web server on the executing machine, and the plots can be viewed
by navigating to the following 1URL using a modern web browser:

http://localhost:5006/waveform_viewer

Loading the web page can take up to minutes depending on the size of the data file.

'''
from fit_profile import fit_profile
from data_container import Data_Container
import numpy as np
import ROOT
from argparse import ArgumentParser
from bokeh.io import curdoc
from bokeh.palettes import all_palettes
from bokeh.layouts import row, column, widgetbox
from bokeh.models.widgets import Slider, DataTable, TableColumn
from bokeh.models import ColumnDataSource, TapTool, Spacer, Range1d, HoverTool
from bokeh.models import LabelSet, Arrow, OpenHead, NormalHead, VeeHead
from bokeh.models.widgets import Slider, TextInput
from bokeh.plotting import figure
 
############################ CLASS FOR BOKEH PLOTS##############################
    
class Plotter:
    
    def __init__(self,data):
        self.data=data
        
        self.amp_range=[self.data.amps_no_ped_min, self.data.amps_no_ped_max]
        self.tot_range=[self.data.tots_min, self.data.tots_max]
    
        self.fig_amp_tot_3d = None
        self.fig_amp = None
        self.fig_tot = None
        self.fig_ch_amp_3d = None
        self.fig_ch_tot_3d = None
        self.table_ch = None
        self.fig_deltat = None
        self.fig_amp_tot_scatter = None
        self.fig_wf = None
        self.fig_tot_tot_scatter = None
        self.table_cube = None
        self.figs_layer = {}


        self.data_amp_tot_3d = None
        self.data_amp = None
        self.data_tot = None
        self.data_ch_amp_3d = None
        self.data_ch_tot_3d = None
        self.data_ch = None
        self.data_deltat = None
        self.data_amp_tot_scatter = None
        self.data_wf = None
        self.data_tot_tot_scatter = None
        self.data_cube = None
        self.data_layer={}
        self.data_direction = None


        self.std_plot_width = 800
        self.std_plot_height = 600
        
        self.nevents_scatter = 1000

        self.tools_hist2d = "tap, box_select, pan, wheel_zoom, box_zoom, reset"
        self.std_line_args = dict(color="#3A5785", line_color=None)
        
    def create_fig_amp_tot_3d(self):        
        ### Create 2d hist plot
        hist2d, amp_edges, tot_edges=self.data.get_amp_tot_hist2d()
        fig_amp_tot = figure(tools = self.tools_hist2d,
                               width = self.std_plot_width,
                               height = self.std_plot_height,
                               x_axis_label="Amplitude",
                               y_axis_label="Time over threshold",
                               toolbar_location="above",
                               x_range=self.amp_range,
                               y_range=self.tot_range)
        palette = all_palettes["Plasma"][256]
        palette[0] = "#ffffff"
        plot1 = fig_amp_tot.circle(x=data.amps-data.peds,y=data.tots, size = 7)
        self.data_amp_tot_3d = plot1.data_source
        plot2 = fig_amp_tot.image(image=[np.log(1+hist2d.T)], 
                                  x=amp_edges[0], y=tot_edges[0], 
                                  dw=amp_edges[-1]-amp_edges[0], 
                                  dh=tot_edges[-1]-tot_edges[0], 
                                  palette=palette)

        # Attach tot and amp range to this plot
        self.amp_range = fig_amp_tot.x_range
        self.tot_range = fig_amp_tot.y_range

        # Add function when points are selected
        self.data_amp_tot_3d.on_change('selected', self.select_amp_tot_3d)
        
        # Store figure
        self.fig_amp_tot_3d = fig_amp_tot
        
        return fig_amp_tot
    
    def create_fig_amp(self):
        ### Create 1D histogram of amplitudes
        amp_hist, amp_edges = self.data.get_amp_hist()
        fig_amp = figure(toolbar_location = None, 
                         plot_width = self.std_plot_width+30, 
                         plot_height = 200, 
                         x_range=self.amp_range,
                         y_range = (0, 1.1*np.max(amp_hist)), 
                         min_border = 10, min_border_left=50, y_axis_location="right")
        # y_axis_type="log")
        plot = fig_amp.quad(bottom=0, 
                            left=amp_edges[:-1], 
                            right=amp_edges[1:], 
                            top=amp_hist, 
                            alpha=0.5, **self.std_line_args)
        self.data_amp = plot.data_source
        self.fig_amp=fig_amp
        return fig_amp

    def update_fig_amp(self):
        amp_hist, amp_edges = data.get_amp_hist()
        self.data_amp.data["top"]   =  amp_hist
        self.fig_amp.y_range.end = 1.1*np.max(amp_hist)


    def create_fig_tot(self):
        ### Create 1D histogram of time over threshold
        tot_hist, tot_edges = self.data.get_tot_hist()
        fig_tot = figure(toolbar_location=None, 
                         plot_width=200, 
                         plot_height=self.std_plot_height, 
                         x_range=(0, 1.1*np.max(tot_hist)),
                         y_range=self.tot_range, 
                         min_border=10, y_axis_location="right")
        #y_axis_type="log"
        plot = fig_tot.quad(left=0, 
                            bottom=tot_edges[:-1], 
                            top=tot_edges[1:], 
                            right=tot_hist, 
                            alpha=0.5, **self.std_line_args)
        self.data_tot = plot.data_source
        self.fig_tot = fig_tot
        return fig_tot

    def update_fig_tot(self):
        tot_hist, tot_edges = data.get_tot_hist()
        self.data_tot.data["right"]   =  tot_hist
        self.fig_tot.x_range.end = 1.1*np.max(tot_hist)


    def create_table_ch(self):
        ### Create table containing channel specific information
        self.data_ch = ColumnDataSource(self.data.get_ch_data())
        ch_table_columns = [
            TableColumn(field="fpga", title="FPGA", width=50),
            TableColumn(field="ch", title="CH", width=50),
            TableColumn(field="triggers", title="Triggers"),
            TableColumn(field="neutrons", title="Neutrons"),
            TableColumn(field="singles", title="Singles"),
            TableColumn(field="doubles", title="Doubles"),
            TableColumn(field="more", title="More")
        ]
        self.table_ch = DataTable(source=self.data_ch, columns=ch_table_columns,
                                  width=self.std_plot_width, height=200)
        return self.table_ch

    def update_table_ch(self):
        self.data_ch.data=self.data.get_ch_data()

    def create_fig_ch_amp_3d(self):        
        ### Create 2d hist plot
        hist2d, chid_edges, amp_edges = self.data.get_ch_amp_hist2d()
        fig_ch_amp= figure(tools = self.tools_hist2d,
                           width = self.std_plot_width,
                           height = 300,
                           x_axis_label="Ch",
                           y_axis_label="Amplitude [ADC]",
                           toolbar_location="above",
                           x_range=(chid_edges[0],chid_edges[-1]),
                           y_range=self.amp_range)
        palette = all_palettes["Plasma"][256]
        palette[0] = "#ffffff"
        plot = fig_ch_amp.image(image=[hist2d.T],
                                #image = [np.log(1+hist2d.T)], 
                                x=chid_edges[0], y=amp_edges[0], 
                                dw=chid_edges[-1]-chid_edges[0], 
                                dh=amp_edges[-1]-amp_edges[0], 
                                palette=palette)
        
        # Store figure
        self.fig_ch_amp_3d = fig_ch_amp
        self.data_ch_amp_3d = plot.data_source
        
        return fig_ch_amp

    def update_fig_ch_amp_3d(self):        
        ### Create 2d hist plot
        hist2d, chid_edges, amp_edges = self.data.get_ch_amp_hist2d()
        self.data_ch_amp_3d.data={"image":[hist2d.T]}
        #self.data_ch_amp_3d.data={"image":[np.log(1+hist2d.T)]}


    def create_fig_ch_tot_3d(self):        
        ### Create 2d hist plot
        hist2d, chid_edges, tot_edges = self.data.get_ch_tot_hist2d()
        fig_ch_tot= figure(tools = self.tools_hist2d,
                           width = self.std_plot_width,
                           height = 300,
                           x_axis_label="Ch",
                           y_axis_label="ToT [samples]",
                           toolbar_location="above",
                           x_range=(chid_edges[0],chid_edges[-1]),
                           y_range=self.tot_range)
        palette = all_palettes["Plasma"][256]
        palette[0] = "#ffffff"
        plot = fig_ch_tot.image(image=[hist2d.T],
                                #image = [np.log(1+hist2d.T)], 
                                x=chid_edges[0], y=tot_edges[0], 
                                dw=chid_edges[-1]-chid_edges[0], 
                                dh=tot_edges[-1]-tot_edges[0], 
                                palette=palette)
        
        # Store figure
        self.fig_ch_tot_3d = fig_ch_tot
        self.data_ch_tot_3d = plot.data_source
        
        return fig_ch_tot

    def update_fig_ch_tot_3d(self):        
        ### Create 2d hist plot
        hist2d, chid_edges, tot_edges = self.data.get_ch_tot_hist2d()
        self.data_ch_tot_3d.data={"image":[hist2d.T]}
        #self.data_ch_amp_3d.data={"image":[np.log(1+hist2d.T)]}



        
    def create_fig_deltat(self):
        deltaT_hist, deltaT_edges = self.data.get_deltaT_hist()
        deltaT_edges=deltaT_edges*1000. # s to ms
        self.fig_deltat = figure(tools = self.tools_hist2d,
                            width = self.std_plot_width,
                            height = 300,
                            x_axis_label="DeltaT (ms)",
                            y_axis_label="Number of events",
                            toolbar_location="above",
                            x_range=(deltaT_edges[0],deltaT_edges[-1]),
                            y_range=(1,1.1*np.max(deltaT_hist)),
                            y_axis_type="log")
        plot = self.fig_deltat.quad(bottom=0, 
                                    left=deltaT_edges[:-1], 
                                    right=deltaT_edges[1:], 
                                    top=deltaT_hist, 
                                    alpha=1.0, **self.std_line_args)
        self.data_deltat = plot.data_source
        return self.fig_deltat

    def update_fig_deltat(self):
        deltaT_hist, deltaT_edges = self.data.get_deltaT_hist()
        self.data_deltat.data["top"]   =  deltaT_hist
        self.fig_deltat.y_range.end = 1.1*np.max(deltaT_hist)    
        
    def create_fig_amp_tot_scatter(self):
        self.fig_amp_tot_scatter = figure(tools = self.tools_hist2d,
                                          width = self.std_plot_width,
                                          height =self.std_plot_height,
                                          x_axis_label="Amplitude",
                                          y_axis_label="Time over threshold",
                                          toolbar_location="above",
                                          x_range=self.amp_range,
                                          y_range=self.tot_range)
        source_data = ColumnDataSource({"x":self.data.amps_s[:self.nevents_scatter]
                                        -self.data.peds_s[:self.nevents_scatter],
                                        "y":self.data.tots_s[:self.nevents_scatter],
                                        "ind":self.data.inds_s[:self.nevents_scatter],
                                        "line_color":self.data.ccolors_s[:self.nevents_scatter]})
        plot = self.fig_amp_tot_scatter.scatter("x","y",source=source_data,
                                                line_color="line_color",
                                                fill_color="white",
                                                fill_alpha=0.0,
                                                size = 7)
        self.data_amp_tot_scatter = plot.data_source
        
        # Add function when points are selected
        self.data_amp_tot_scatter.on_change('selected', self.select_amp_tot_scatter)

        
        return self.fig_amp_tot_scatter

    def update_fig_amp_tot_scatter(self):
        self.data_amp_tot_scatter.data={"x":
                                        self.data.amps_s[:self.nevents_scatter]
                                        -self.data.peds_s[:self.nevents_scatter],
                                        "y":
                                        self.data.tots_s[:self.nevents_scatter],
                                        "line_color":self.data.ccolors_s[:self.nevents_scatter],
                                        "ind":self.data.inds_s[:self.nevents_scatter]}

        
    def create_fig_wf(self):
        self.fig_wf = figure(tools = self.tools_hist2d,
                        width = self.std_plot_width,
                        height = 300,
                        x_axis_label="Time (us)",
                        y_axis_label="Value",
                        toolbar_location="above"
        )

        source_data = ColumnDataSource(
            {'xs':[np.linspace(0,499,500)/adc_rate_MHz],
             'ys':[np.sin(np.linspace(0,499,500)/adc_rate_MHz)],
             'line_color':["green"], 'fpga':[-1],
             'ch':[-1], 'time':[-1], 'amp':[-1], 'tot':[-1],
             'chx':[-1], 'chy':[-1], 'chz':[-1], 'ind':[-1]})

        plot = self.fig_wf.multi_line(xs='xs',ys='ys',
                                      source=source_data,
                                      line_color='line_color',
                                      line_width = 1.5)

        #plot = self.fig_wf.multi_line([np.linspace(0,499,500)/adc_rate_MHz], 
        #                                [np.sin(np.linspace(0,499,500)/adc_rate_MHz)], 
        #                                line_width = 1.5,
        #                                color=["green"])
        self.data_wf = plot.data_source
        
        return self.fig_wf

    def update_fig_wf(self,inds, ref_ind=None):
        if ref_ind == None:
            ref_ind = inds[0]

        wf_data={'xs':[], 'ys':[], 'line_color':[], 'fpga':[],
                 'ch':[], 'time':[], 'amp':[], 'tot':[],
                 'chx':[], 'chy':[], 'chz':[], 'ind':[]}
        
        print "Plotting wf", inds
        refTime=self.data.times[ref_ind]
        for ind in inds:
            timeShift=(self.data.times[ind]-refTime)*self.data.time_adc_s*1.e6 # us
            print "timeShift:", timeShift
            wf_data['xs'].append(np.linspace(0,len(self.data.wfs[ind])-1,
                                             len(self.data.wfs[ind]))/adc_rate_MHz+timeShift)
            wf_data['ys'].append(self.data.wfs[ind]-self.data.peds[ind])
            if (ind == ref_ind):
                wf_data['line_color'].append("red")
            else:
                 wf_data['line_color'].append("gray")
            wf_data['fpga'].append(self.data.fpgas[ind])
            wf_data['ch'].append(self.data.chs[ind])
            wf_data['time'].append(self.data.times[ind])
            wf_data['amp'].append(self.data.amps[ind]-self.data.peds[ind])
            wf_data['tot'].append(self.data.tots[ind])
            xyz = self.data.settings_xyz[(self.data.fpgas[ind],
                                          self.data.chs[ind])]
            wf_data['chx'].append(xyz[0])
            wf_data['chy'].append(xyz[1])
            wf_data['chz'].append(xyz[2])
            
            wf_data['ind'].append(ind)
            

        ### Column data source cannot be changed one by one
        ### Because column sizes must alway match
        self.data_wf.data = wf_data
        
    def create_table_wf(self):
        ### Create table containing wf information
        if (self.data_wf == None):
            ### Needs self.wf_data() to work
            create_fig_wf()
        wf_table_columns = [
            TableColumn(field="fpga", title="FPGA", width=50),
            TableColumn(field="ch", title="CH", width=50),
            TableColumn(field="chx", title="X", width=50),
            TableColumn(field="chy", title="Y", width=50),
            TableColumn(field="chz", title="Z", width=50),
            TableColumn(field="ind", title="Ind"),
            TableColumn(field="time", title="Time"),
            TableColumn(field="amp", title="Amp"),
            TableColumn(field="tot", title="ToT")
        ]
        self.table_wf = DataTable(source=self.data_wf, columns=wf_table_columns,
                                  width=self.std_plot_width, height=200)
        return self.table_wf
    
    def create_fig_tot_tot_scatter(self):
        self.fig_tot_tot_scatter = figure(tools = self.tools_hist2d,
                                          width = self.std_plot_width,
                                          height = self.std_plot_width,
                                          x_axis_label="Time over threshold",
                                          y_axis_label="Time over threshold",
                                          toolbar_location="above",
                                          x_range=self.tot_range,
                                          y_range=self.tot_range)
        
        # Create self.data_tot_tot_scatte and get data
        self.data_tot_tot_scatter = ColumnDataSource({})
        self.update_fig_tot_tot_scatter()

        # Create plot
        plot = self.fig_tot_tot_scatter.scatter(x="tot1",y="tot2",source=self.data_tot_tot_scatter,
                                                line_color="red",
                                                fill_color="white",
                                                fill_alpha=0.0,
                                                size = 7)
        # Add function when points are selected
        self.data_tot_tot_scatter.on_change('selected', self.select_tot_tot_scatter)

        
        return self.fig_tot_tot_scatter

    def update_fig_tot_tot_scatter(self):
        data_tot1=[]
        data_tot2=[]
        inds_all=[]
        cgroups=self.data.cgroups_s[self.data.ncoin_s>1] # Require coicindece
        cgroups=cgroups[:self.nevents_scatter] # Limit to desired number of events
        for cgroup in cgroups:
            inds,=np.where(self.data.cgroups == cgroup)
            inds_sorter=self.data.amps[inds].argsort()
            inds_all.append(inds[inds_sorter[-1]])
            data_tot1.append(self.data.tots[inds[inds_sorter[-1]]])
            data_tot2.append(self.data.tots[inds[inds_sorter[-2]]])
        inds_all=np.array(inds_all)
        data_tot1=np.array(data_tot1)
        data_tot2=np.array(data_tot2)
        self.data_tot_tot_scatter.data={"tot1":data_tot1,
                                        "tot2":data_tot2,
                                        "ind":inds_all}

    def create_table_cube(self):
        
        self.data_cube = ColumnDataSource({})
        self.update_table_cube()
       
        cube_table_columns = [
            TableColumn(field="x", title="X", width=50),
            TableColumn(field="y", title="Y", width=50),
            TableColumn(field="z", title="Z", width=50),
            TableColumn(field="nevents", title="Events"),
            TableColumn(field="rate", title="Rate"),
        ]
        self.table_cube = DataTable(source=self.data_cube, columns=cube_table_columns,
                                    width=self.std_plot_width, height=400)
        return self.table_cube


    def update_table_cube(self):
        ### Create table containing channel specific information
        print "Updating table: Number of events per cube"
        cube_nevents=self.data.get_cube_nevents()
        cube_data_dic={"x":[], "y":[],"z":[],"nevents":[],
                       "rate":[]}
        
        # Create "Any" rows
        xyzs = cube_nevents.keys()
        for xyz in xyzs:
            try:
                cube_nevents[(xyz[0],"Any","Any")]+=cube_nevents[xyz]
            except KeyError:
                cube_nevents[(xyz[0],"Any","Any")]=cube_nevents[xyz]
            try:
                cube_nevents[("Any",xyz[1],"Any")]+=cube_nevents[xyz]
            except KeyError:
                cube_nevents[("Any",xyz[1],"Any")]=cube_nevents[xyz]
            try:
                cube_nevents[("Any","Any",xyz[2])]+=cube_nevents[xyz]
            except KeyError:
                cube_nevents[("Any","Any",xyz[2])]=cube_nevents[xyz]
            try:
                cube_nevents[("Any","Any","Any")]+=cube_nevents[xyz]
            except KeyError:
                cube_nevents[("Any","Any","Any")]=cube_nevents[xyz]

                
        ### Form a dictionary of right shape    
        for xyz in sorted(cube_nevents):
            cube_data_dic["x"].append(xyz[0])
            cube_data_dic["y"].append(xyz[1])
            cube_data_dic["z"].append(xyz[2])
            cube_data_dic["nevents"].append(cube_nevents[xyz])
            cube_data_dic["rate"].append(1.*cube_nevents[xyz]/self.data.durationS)

        self.data_cube.data = cube_data_dic

    def create_figs_layer(self):

        ### First populate self.data_layer
        self.update_figs_layer()
        
        for z in self.data_layer:
            print "Z=",z
            source = self.data_layer[z]
            hover = HoverTool(tooltips=[("(x,y)", "($x, $y)"),
                                        ('Events','@nevents'),
                                        ('Rate','@rate')])
            if z=="sum":
                fig = figure(title="Z="+str(z), tools=[hover], toolbar_location=None,
                             x_range=map(str,np.unique(source.data["x"])[-1::-1]), # Inv axis
                             y_range=map(str,np.unique(source.data["y"])),
                             width = self.std_plot_width,
                             height = self.std_plot_width)
            else:
                fig = figure(title="Z="+str(z), tools=[hover], toolbar_location=None,
                             x_range=map(str,np.unique(source.data["x"])[-1::-1]), # Inv axis
                             y_range=map(str,np.unique(source.data["y"])),
                             width = self.std_plot_width/2,
                             height = self.std_plot_width/2)


            plot = fig.rect(x = "x",y = "y", width=1, height=1, source = source, color = "color")
            if z == "sum":
                ### Add neutron direction arrow
                fig.add_layout(Arrow(x_start='x_start', y_start='y_start',
                                     x_end='x_end', y_end='y_end',
                                     source=self.data_direction,
                                     end=VeeHead(size=35),
                                     line_color="black",
                                     line_width=4))
                
                ### Add neutron direction angle text
                fig.add_layout(LabelSet(x=2.5, y=2.5,
                                        source=self.data_direction,
                                        background_fill_color='white',
                                        text_align="center",
                                        text_baseline="middle",
                                        text_font_style="bold",
                                        text="angle_txt"))
                

            self.figs_layer[z]=fig

        return self.figs_layer

    def update_figs_layer(self):
        print "Reading cube data"
        cube_nevents=self.data.get_cube_nevents()
        print "Converting data to layer format"
        
        data_perz={}
        nevents_max=0
        nevents_min=1e12
        for xyz in sorted(cube_nevents):
            try:
                if cube_nevents[xyz]>0: # Broken channels not taken into account
                    nevents_min=min(nevents_min,cube_nevents[xyz])
                nevents_max=max(nevents_max,cube_nevents[xyz])
                data_perz[xyz[2]]["x"].append(str(xyz[0]))
                data_perz[xyz[2]]["y"].append(str(xyz[1]))
                data_perz[xyz[2]]["nevents"].append(cube_nevents[xyz])
                data_perz[xyz[2]]["rate"].append(1.*cube_nevents[xyz]/self.data.durationS)
            except KeyError:
                data_perz[xyz[2]]={"x":[],"y":[],"nevents":[],"rate":[]}
                data_perz[xyz[2]]["x"].append(str(xyz[0]))
                data_perz[xyz[2]]["y"].append(str(xyz[1]))
                data_perz[xyz[2]]["nevents"].append(cube_nevents[xyz])
                data_perz[xyz[2]]["rate"].append(1.*cube_nevents[xyz]/self.data.durationS)

        ### Sum (z==any)
        nevents_sum_max=0
        nevents_sum_min=1e12

        zs=data_perz.keys() # Notice: before adding the key "sum"
        # Initialize
        data_perz["sum"]={}
        data_perz["sum"]["x"]=data_perz[zs[0]]["x"]
        data_perz["sum"]["y"]=data_perz[zs[0]]["y"]
        data_perz["sum"]["nevents"]=len(data_perz[zs[0]]["x"])*[0]
        data_perz["sum"]["rate"]=len(data_perz[zs[0]]["x"])*[0]
        # Insert values
        for id_sum in range(len(data_perz["sum"]["x"])):
            x=data_perz["sum"]["x"][id_sum]
            y=data_perz["sum"]["y"][id_sum]
            for z in zs:
                for id_plane in  range(len(data_perz[z]["x"])):
                    if (data_perz[z]["x"][id_plane] == x and
                        data_perz[z]["y"][id_plane] == y):
                        data_perz["sum"]["nevents"][id_sum]+=data_perz[z]["nevents"][id_plane]
                        data_perz["sum"]["rate"][id_sum]+=data_perz[z]["rate"][id_plane]
            ### After adding data, find max
            if data_perz["sum"]["nevents"][id_sum] > 0: # Don't accept zero
                nevents_sum_min=min(nevents_sum_min,data_perz["sum"]["nevents"][id_sum])
                nevents_sum_max=max(nevents_sum_max,data_perz["sum"]["nevents"][id_sum])


                
        ### Add color to the dictionary 
        palette = np.array(all_palettes["Spectral"][11])
        palette[0] = "#ffffff"
        # Individual planes
        c_slope=9./(nevents_max-nevents_min)
        c_const=1-c_slope*nevents_min
        # Sum
        c_sum_slope=9./(nevents_sum_max-nevents_sum_min)
        c_sum_const=1-c_sum_slope*nevents_sum_min
        for z in data_perz:
            if z == "sum":
                color_ind=np.rint(c_sum_slope*np.array(data_perz[z]["nevents"])
                                  +c_sum_const).astype(int)
            else:
                color_ind=np.rint(c_slope*np.array(data_perz[z]["nevents"])
                                  +c_const).astype(int)
            color_ind[color_ind<0]=0
            data_perz[z]["color"] = palette[color_ind]
            
        for z in data_perz:
            ### Convert dictionary to bokeh data source
            try: # Updating existing data_layer variable
                self.data_layer[z].data =data_perz[z]
            except KeyError: # Creating new data layer variable
                self.data_layer[z]=ColumnDataSource(data_perz[z])

        ### Find direction of incoming neutrons and mark it with an arrow
        (phi,theta, const) = fit_profile(data_perz["sum"])
        x0=2.5
        y0=2.5
        corr=np.deg2rad(0.0)
        # Notice! Phi goes from 0 to pi (not -pi to pi)
        angle=phi+corr
        x_dev=1.5*np.cos(angle)
        y_dev=1.5*np.sin(angle)
        #print "ang:", np.rad2deg(angle),"cos:",np.cos(angle), "sin:",np.sin(angle)
        data_direction_dict=dict(x_start=[x0+x_dev],
                                 y_start=[y0-y_dev],
                                 x_end=[x0-x_dev],
                                 y_end=[y0+y_dev],
                                 angle_txt=['{0:.1f} deg'.format(-90.-np.rad2deg(angle))])
        if self.data_direction == None:
            self.data_direction = ColumnDataSource(data=data_direction_dict)
        else:
            self.data_direction.data = data_direction_dict

    def select_amp_tot_3d(self,attr, old, new):
        inds = np.array(new['1d']['indices'],dtype=int)
        print "Updating hist. Number of points selected:",len(inds)
        
        # Restrict to selected events
        self.data.select(inds) 
        
        print "Event rate:",
        print "{0:.3f}".format((1.*self.data.nEvents_s)/self.data.durationS), 
        print "Hz"

        if (self.fig_amp):
            self.update_fig_amp()
        if (self.fig_tot):
            self.update_fig_tot()
        if (self.fig_ch_amp_3d):
            self.update_fig_ch_amp_3d()
        if (self.fig_ch_tot_3d):
            self.update_fig_ch_tot_3d()
        if (self.table_ch):
            self.update_table_ch()
        if (self.fig_deltat):
            self.update_fig_deltat()
        if (self.fig_amp_tot_scatter):
            self.update_fig_amp_tot_scatter()
        if (self.fig_tot_tot_scatter):
            self.update_fig_tot_tot_scatter()
        if (self.table_cube):
            self.update_table_cube()
        if (len(self.figs_layer)>0):
            self.update_figs_layer()


    def select_amp_tot_scatter(self,attr, old, new):
        # Define functions for scatter points clicks.
        selected = np.array(new['1d']['indices'],dtype=int)
        if len(selected) > 0:
            selected = selected[0]
            ind=self.data_amp_tot_scatter.data["ind"][selected]
            ### Get all wf in the same coincidence group
            c_inds,=np.where(data.cgroups == data.cgroups[ind])
            self.update_fig_wf(c_inds,ref_ind=ind)


    def select_tot_tot_scatter(self,attr, old, new):
        # Define functions for scatter points clicks.
        selected = np.array(new['1d']['indices'],dtype=int)
        if len(selected) > 0:
            selected = selected[0]
            ind=self.data_tot_tot_scatter.data["ind"][selected]
            ### Get all wf in the same coincidence group
            c_inds,=np.where(data.cgroups == data.cgroups[ind])
            self.update_fig_wf(c_inds,ref_ind=ind)


############################ THE MAIN PROGRAM STARTS HERE ##############################
        
### Read in data file as an argument
parser = ArgumentParser(description="Plot ToT / max peak 2D histogram")
parser.add_argument("input_file_name",metavar="input_file_name",
                    help="input root file")
parser.add_argument("events_max",metavar="events_max", type=int,
                    help="Number of events to be read from the file")
args = parser.parse_args()

### Maximum number of events in scatter plot
events_max=args.events_max
scatter_max = 1000

### ADC sampling rate
adc_rate_MHz=30.

## Create data object
print "Creating data container"
data = Data_Container(args.input_file_name,0,events_max)

print "Creating plotter"
plotter = Plotter(data)
print "Creating figure Amp-ToT 3D"
fig_amp_tot_3d=plotter.create_fig_amp_tot_3d()
fig_amp = plotter.create_fig_amp()
fig_tot = plotter.create_fig_tot()
print "Creating figure Ch-Amp 3D"
fig_ch_amp_3d=plotter.create_fig_ch_amp_3d()
print "Creating figure Ch-ToT 3D"
fig_ch_tot_3d=plotter.create_fig_ch_tot_3d()
print "Creating table Ch"
table_ch = plotter.create_table_ch()
print "Creating figure DeltaT"
fig_deltat = plotter.create_fig_deltat()
fig_amp_tot_scatter = plotter.create_fig_amp_tot_scatter()
fig_wf = plotter.create_fig_wf()
table_wf = plotter.create_table_wf()
print "Creating ToT-ToT scatter plot"
fig_tot_tot_scatter = plotter.create_fig_tot_tot_scatter()
print "Creating cube table"
table_cube = plotter.create_table_cube()
print "Creating layer plots"
figs_layer = plotter.create_figs_layer()
print "Drawing plots"

# Add the plots to the html code.
layout = column(
    row(fig_amp_tot_3d, fig_tot),
    row(fig_amp),
    row(fig_ch_amp_3d),
    row(fig_ch_tot_3d),
    row(widgetbox(table_ch)),
    row(fig_deltat),
    row(fig_amp_tot_scatter),
    row(fig_wf),
    row(widgetbox(table_wf)),
    row(fig_tot_tot_scatter), # Causes weird Glypg warning but still works
    row(table_cube),
    row(figs_layer["sum"]),
    row(figs_layer[0],figs_layer[1]),
    row(figs_layer[2],figs_layer[3])
)

curdoc().add_root(layout)
curdoc().title = "Waveform Viewer"


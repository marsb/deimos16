import numpy as np
from scipy.optimize import curve_fit

def fit_profile(layer_hits):
    xy=np.array([map(float,layer_hits["x"]),map(float,layer_hits["y"])])
    nevents=np.array(layer_hits["nevents"])
    nevents_sigma=(nevents+1.0)**0.5
    popt,pcov=curve_fit(surface_plane,xy,nevents,p0=[1,1,1,10],sigma=nevents_sigma)

    r=(popt[0]**2+popt[1]**2+popt[2]**2)**0.5
    theta=np.arccos(popt[2]/r) # In rad
    phi=np.arctan2(popt[1], popt[0]) # Was np.arctan(popt[1]/popt[0])
    const= popt[3]/r
    return phi, theta, const

def surface_plane(xy,nx,ny,nz,p):
     z=(nx*xy[0]+ny*xy[1]+p)/nz
     return z
    

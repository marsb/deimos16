import numpy as np
import matplotlib.pyplot as plt
from argparse import ArgumentParser
from data_container import Data_Container
from directionFitter import DirectionFitter

def main():
    parser = ArgumentParser(description="Get direction accuracy")
    parser.add_argument("input_file_name",metavar="input_file_name",
                        help="input root file")
    parser.add_argument("datasets",metavar="datasets", type=int,
                        help="Number of data sets")
    parser.add_argument("neutrons",metavar="neutrons", type=float,
                        help="Mean number of neutrons per dataset")
    parser.add_argument("events",metavar="events", type=int,
                        help="Maximum number of root events to be read")
    parser.add_argument("tot_min",metavar="tot_min", type=int,
                        help="Minumum ToT value for accepted events")
    
    args = parser.parse_args()

    (angle_mean_deg, angle_std_deg, angles_fit)=get_direction_accuracy(args.input_file_name,
                                                                       args.tot_min,
                                                                       args.events,
                                                                       args.neutrons,
                                                                       args.datasets)

    
    print "Mean:", angle_mean_deg, "Std:", angle_std_deg, "deg"
    plt.hist(angles_fit,50)
    plt.show()

    
def get_direction_accuracy(input_file_name, tot, nevents, nneutrons, ndatasets):
    data=Data_Container(input_file_name,0,nevents)
    inds_selected=np.nonzero(data.tots>=tot)[0]
    data.select(inds_selected)
    cube_nevents=data.get_cube_nevents()

    data_n_neutrons=sum(cube_nevents.values())
    if data_n_neutrons < 10*nneutrons:
        print "Warning: Input data set may be too small!"
        print "Try a larger value for input argument events"

    data_meas=np.zeros((4,4,4))
    for xyz in sorted(cube_nevents):
        #print xyz,
        data_meas[xyz[0],xyz[1],xyz[2]]=cube_nevents[xyz]
    #print ""
    data_mean=1.*data_meas/data_n_neutrons*nneutrons

    directionFitter=DirectionFitter()
    fitted_angles=[]
    fit_failed=[]
    for i in range(ndatasets):
        if (i%100 == 0):
            print "Fitting dataset:", i
        
        data_simulated=np.random.poisson(data_mean)
        #print np.sum(data_simulated, axis=2)
        (phi,theta, const, sigma_phi,ratio) = directionFitter.fit(np.sum(data_simulated, axis=2))
        angle=phi
        angle_deg=np.rad2deg(-angle)-90.
        if theta==0.0 or phi==0.0:
            fit_failed.append(True)
        else:
            fit_failed.append(False)
        #if angle_deg < -180:
        #    angle_deg=360+angle_deg
        #if angle_deg>180:
        #    angle_deg=-180+angle_deg
        fitted_angles.append(angle_deg)
     
    fitted_angles=np.array(fitted_angles)
    fit_failed=np.array(fit_failed)
    angle_mean=np.mean(fitted_angles)
    angle_std=np.std(fitted_angles)
    #fitted_angles[fit_failed==True]=np.random.rand(np.count_nonzero(fit_failed))*380.+2*angle_mean_deg
    fitted_angles[fit_failed==True]=np.random.rand(np.count_nonzero(fit_failed))*360.-180.
    for i in range(10):
        angle_mean=np.mean(fitted_angles)
        fitted_angles[fitted_angles > angle_mean+180] = -360+fitted_angles[fitted_angles
                                                                           > angle_mean+180] 
        fitted_angles[fitted_angles < angle_mean-180] = 360+fitted_angles[fitted_angles
                                                                          < angle_mean-180] 

        
    angle_mean=np.mean(fitted_angles)
    angle_std=np.std(fitted_angles)

    return(angle_mean, angle_std, fitted_angles)
    
if __name__ == "__main__":
    main()

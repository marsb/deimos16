import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm

def main():

    mu_signal=np.array([5, 10, 20, 30, 40])
    mu_bg=100.0
    
    decission_thresholds=np.linspace(0.1,500,1000)

    fig=plt.figure("ROC", figsize=(8,6))
    ax1=fig.add_subplot(111)
    ax1.set_xlabel("False positive")
    ax1.set_ylabel("True positive")
    for s in mu_signal:
        true_positive=1-norm.cdf(decission_thresholds, loc=mu_bg+s, scale=(mu_bg+s)**0.5)
        false_positive=1-norm.cdf(decission_thresholds, loc=mu_bg, scale=(mu_bg)**0.5)
        label="B="+str(mu_bg)+" S="+str(s)
        plt.plot(false_positive, true_positive, label=label)
    
    plt.legend()
    plt.tight_layout()
    plt.savefig("roc.pdf")
    plt.savefig("roc.png")
    plt.show()

if __name__ == "__main__":
    main()

import numpy as np
import matplotlib.pyplot as plt
from argparse import ArgumentParser
from get_direction_accuracy import get_direction_accuracy

def main():
    parser = ArgumentParser(description="Plot direction accuracy")
    parser.add_argument("--input1",metavar="input1.root", help="input 1 root file")
    parser.add_argument("--input2",metavar="input2.root", help="input 2 root file") 
    parser.add_argument("--input3",metavar="input3.root", help="input 3 root file")
    parser.add_argument("--name1",metavar="name1.root", help="Name for data in file 1")
    parser.add_argument("--name2",metavar="name2.root", help="Name for data in file 2") 
    parser.add_argument("--name3",metavar="name3.root", help="Name for data in file 3")
    parser.add_argument("--neutrons",metavar="N", nargs="+", type=float,
                        help="Number of neutrons") 
    parser.add_argument("--scaler",metavar="x", type=float,
                        help="Scaler for time axis") 
    args = parser.parse_args()
    
    input_files={}
    if args.input1:
        input_files[args.name1]=args.input1
    if args.input2:
        input_files[args.name2]=args.input2
    if args.input3:
        input_files[args.name3]=args.input3

    stds={}
    means={}


    for name in input_files:
        print "Processing file:", input_files[name]
        stds[name]=[]
        means[name]=[]
        for nneutrons in args.neutrons:
            print "Mean number of neutrons:", nneutrons
            (angle_mean, angle_std, angles_fit)=get_direction_accuracy(input_files[name],
                                                                       40,
                                                                       int(max(10000,
                                                                               100*nneutrons)),
                                                                       nneutrons,
                                                                       1000)
        
            stds[name].append(angle_std)
            means[name].append(angle_mean)
            print "Mean:", angle_mean, "Std:", angle_std, "deg"

    fig=plt.figure("Direction accuracy", figsize=(6,4.5))
    ax_n=fig.add_subplot(111) # neutron axis
    ax_n.set_xlabel("Number of neutrons")
    ax_n.set_ylabel("Direction std (deg)")
    ax_n.set_yscale("log")
    ax_n.yaxis.set_ticks([1,2,5,10,20,30,40,50])
    ax_n.set_xscale("log")
    if args.scaler:
        ax_t=ax_n.twiny() # time axis
        ax_t.set_xscale("log")
        ax_t.set_xlabel("Measurement time (s)")
    neutrons=np.array(args.neutrons)
    for key in stds:
        ax_n.plot(neutrons,stds[key],label=key)
        if args.scaler:
            ax_t.plot(neutrons/args.scaler,stds[key])
    if args.scaler:
        ax_t.cla()
        ax_t.set_xscale("log")
        ax_t.yaxis.set_ticks([1,2,5,10,20,30,40,50])
        ax_t.set_xlabel("Measurement time (s)")
    plt.legend()
    plt.tight_layout()
    plt.savefig("direction_accuracy.pdf")
    plt.savefig("direction_accuracy.png")
    plt.show()

if __name__ == "__main__":
    main()

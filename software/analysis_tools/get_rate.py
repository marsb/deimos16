import numpy as np
import ROOT 
from argparse import ArgumentParser
import matplotlib.pyplot as plt




class DataContainer:

    def __init__(self,root_input_file_name,events_max,ped_max):
        self.nEvents = 0
        self.wfs, self.amps, self.tots  = np.array([]), np.array([]), np.array([])
        self.peds, self.nflags, self.chs = np.array([]), np.array([]), np.array([])
        self.times, self.storeTimes = np.array([]), np.array([])
        self.peds_dict={}
        self.types_dict={}
        
        # Connect to root file
        file_in=ROOT.TFile(root_input_file_name)
        # Get events tree
        events=file_in.Get("events")
        events.GetEntry(0)
        # Get settings tree
        settings=file_in.Get("settings")
        settings.GetEntry(0)

        # Get channel pedestals from settings tree
        for settingId in range(settings.GetEntries()):
            settings.GetEntry(settingId)
            self.peds_dict[settings.chId]=settings.pedestal
            self.types_dict[settings.chId]=settings.type
            
                        
        # Read data from events tree to python listst
        chs, amps, tots, peds, nflags, wfs, times, storeTimes = [], [], [], [], [], [], [], []
        events_id_range = range(min(events.GetEntries(),events_max))
        for event_id in events_id_range:
            events.GetEntry(event_id)
            ch = events.channel
            amp = events.peakValue
            tot = events.totValue
            ped = self.peds_dict[events.channel]
            nflag = events.neutronFlag
            wf = np.array(events.waveform)
            time = events.time
            storeTime = events.storeTime
            ### Accept only neutron channels
            if self.types_dict[events.channel] != 1:
                continue
            ### This part removes events with shift in baseline 
            wf_sort=np.sort(wf-ped)
            wf_min_mean=np.mean(wf_sort[:int(round(len(wf_sort)/5.))])
            if wf_min_mean>ped_max:
                continue
            chs.append(ch)
            amps.append(amp)
            tots.append(tot)
            peds.append(ped)
            nflags.append(nflag)
            wfs.append(wf)
            times.append(time)
            storeTimes.append(storeTime)
            self.nEvents+=1

        # Convert lists to numpy arrays
        self.chs = np.array(chs)
        self.wfs = np.array(wfs)
        self.amps = np.array(amps)
        self.tots = np.array(tots)
        self.peds = np.array(peds)
        self.nflags = np.array(nflags)
        self.times = np.array(times)
        self.storeTimes = np.array(storeTimes)

        self.time_adc_s=1.*(storeTimes[-1]-storeTimes[0])/(times[-1]-times[0])

        self.amps_no_ped_min = np.min(self.amps-self.peds)
        self.amps_no_ped_max = np.max(self.amps-self.peds)
        self.tots_min = np.min(self.tots)
        self.tots_max = np.max(self.tots)

        self.amps_no_ped_bins = int(round((self.amps_no_ped_max-self.amps_no_ped_min)/30.))
        self.tots_bins=int(round(self.tots_max-self.tots_min))
        
        self.durationS=np.max(self.storeTimes)-np.min(self.storeTimes)

        self.deltaT_range_s=(np.min(self.times[1:]-self.times[:-1])*self.time_adc_s,
                           np.max(self.times[1:]-self.times[:-1])*self.time_adc_s)

        # self.ch_data = ColumnDataSource(data=dict())

        # This is not the ADC sampling rate but clock for trigger time stamps
        # (100 MHz). The ADC sampling rate is 30 MHz.
        #print "ADC sampling rate:", 1.0e-6/self.time_adc_s, "MHz"
        print "Total number of events:", self.nEvents

    def get_rate_tot(self,tot_min):
        n_tots=np.count_nonzero(self.tots>=tot_min)
        return n_tots, 1.*n_tots/self.durationS

    def get_amp_hist(self,inds=[]):
        if len(inds)==0:
            amps_no_peds_hist, amps_no_peds_edges = np.histogram(self.amps-self.peds, bins=int(round(self.amps_no_ped_bins)), range=(self.amps_no_ped_min, self.amps_no_ped_max))
        else:
            amps_no_peds_hist, amps_no_peds_edges = np.histogram(self.amps[inds]-self.peds[inds], 
                                                                 bins=self.amps_no_ped_bins,
                                                                 range=(self.amps_no_ped_min,
                                                                        self.amps_no_ped_max))
        return amps_no_peds_hist, amps_no_peds_edges, 

    def get_tot_hist(self, inds=[]):
        if len(inds)==0:
            tots_hist, tots_edges = np.histogram(self.tots, bins=self.tots_bins,
                                                 range=(self.tots_min, self.tots_max))
        else:
            tots_hist, tots_edges = np.histogram(self.tots[inds], bins=self.tots_bins,
                                                 range=(self.tots_min, self.tots_max))
        return tots_hist, tots_edges

    def get_amp_tot_hist2d(self):
        hist2d, amps_no_peds_edges, tots_edges= np.histogram2d(self.amps-self.peds,self.tots,
                                                               bins=[self.amps_no_ped_bins,
                                                                     self.tots_bins],
                                                               range=((self.amps_no_ped_min,
                                                                       self.amps_no_ped_max),
                                                                      (self.tots_min,
                                                                       self.tots_max)))
        return hist2d, amps_no_peds_edges, tots_edges

    def get_deltaT_hist(self, inds=[]):
        if len(inds)==0:
            deltaT=self.times[1:]-self.times[:-1]
        else:
            inds.sort()
            times=self.times[inds]
            deltaT=times[1:]-times[:-1]

        deltaT_hist, deltaT_edges = np.histogram(deltaT*self.time_adc_s, bins=200,
                                                 range=self.deltaT_range_s)
        return deltaT_hist, deltaT_edges


    def get_ch_data(self,inds=[]):
        unique_chs=np.unique(self.chs)
        unique_chs.sort()
        nevents_chs=np.zeros(len(unique_chs))
        nneutrons_chs=np.zeros(len(unique_chs))
        if len(inds)==0:
            nflags=self.nflags
            chs=self.chs
        else:
            nflags=self.nflags[inds]
            chs=self.chs[inds]
        for id in range(len(unique_chs)):
            nflags_selected=nflags[chs==unique_chs[id]]
            nevents_chs[id]=len(nflags_selected)
            nneutrons_chs[id]=np.count_nonzero(nflags_selected==1)

        self.ch_data.data = {
            "ch" : unique_chs,
            "n_all" : nevents_chs,
            "n_neutron": nneutrons_chs
        }
        return self.ch_data


parser = ArgumentParser(description='Calcula neutron rate')
parser.add_argument("--tot", help="Minimum ToT value", type=int, default=60)
parser.add_argument("--pedestal", help="Cut for pedestal variation", type=float, default=0.0)
parser.add_argument('input_file_names', metavar='input.root', type=str, 
                    nargs='+', help='Input root files')
args=parser.parse_args()


for input_file_name in args.input_file_names:
    print "File", input_file_name
    data=DataContainer(input_file_name,10000000,args.pedestal)
    n, rate = data.get_rate_tot(args.tot)
    print "Neutrons:", n, "Rate:", "{0:.4f}".format(rate)



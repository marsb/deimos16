import numpy as np
from argparse import ArgumentParser
from data_container import Data_Container


def main():
    parser = ArgumentParser(description="Get training data set for machine learning studies")
    parser.add_argument("input_file_name",metavar="input_file_name",
                        help="input root file")
    parser.add_argument("datasets",metavar="datasets", type=int,
                        help="Number of data sets")
    parser.add_argument("neutrons",metavar="neutrons", type=int,
                        help="Mean number of neutrons per dataset")
    parser.add_argument("events",metavar="events", type=int,
                        help="Maximum number of root events to be read")
    parser.add_argument("tot_min",metavar="tot_min", type=int,
                        help="Minumum ToT value for accepted events")

    args = parser.parse_args()
    data=Data_Container(args.input_file_name,0,args.events)
    inds_selected=np.nonzero(data.tots>=args.tot_min)[0]
    data.select(inds_selected)
    cube_nevents=data.get_cube_nevents()

    data_n_neutrons=sum(cube_nevents.values())
    if data_n_neutrons < 10*args.neutrons:
        print "Warning: Input data set may be too small!"
        print "Try a larger value for input argument events"

    data_meas=[]
    for xyz in sorted(cube_nevents):
        #print xyz,
        data_meas.append(cube_nevents[xyz])
    #print ""
    data_mean=1.*np.array(data_meas)/data_n_neutrons*args.neutrons

    data_simulated=np.random.poisson(np.tile(data_mean,
                                             (args.datasets,1)))

    print "Saving output data array of size:", np.shape(data_simulated)
    np.save("trainingdata",data_simulated)
    #print data_simulated
    
if __name__ == "__main__":
    main()
